# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-keepattributes *Annotation*
-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }
-ignorewarnings
# Threeten backport
-dontwarn java.util.spi.LocaleServiceProvider
-dontwarn sun.util.calendar.*
-keep class org.threeten.bp.zone.*
-keepclassmembers,allowobfuscation class * {
    @com.google.gson.annotations.SerializedName <fields>;
  }
-keep,allowobfuscation @interface com.google.gson.annotations.SerializedName
-keep class com.app.samriddhi.** { *; }




# Payumoney start
-dontwarn com.mixpanel.**
-dontwarn org.apache.http.**
-dontwarn com.android.volley.toolbox.**

-keep class com.payumoney.core.entity.PaymentEntity{
*;
}
-keep class com.payumoney.core.entity.CardDetail{
*;
}
-keep class com.payumoney.core.entity.EmiTenure{
*;
}
-keep class com.payumoney.core.entity.EmiThreshold{
*;
}
-keep class com.payumoney.core.response.UserDetail{
*;
}
-keep class com.payumoney.core.entity.PayumoneyConvenienceFee{
    public *;
}
-keep class com.payumoney.core.PayUmoneyConstants{
*;
}
-keep class com.payumoney.core.PayUmoneySDK{
public *;
}
-keep interface com.payumoney.core.listener.APICallbackListener{
public *;
}
-keep interface * extends com.payumoney.core.listener.APICallbackListener{
public *;
}
-keep class com.payumoney.core.request.PaymentRequest{
public *;
}
-keep class com.payumoney.core.response.BinDetail{
public *;
}
-keep class com.payumoney.core.response.ErrorResponse{
public *;
}
-keep class com.payumoney.core.response.PaymentOptionDetails{
public *;
}
-keep class com.payumoney.core.utils.SdkHelper{
public *;
}
-keep class com.payumoney.core.response.PayUMoneyLoginResponse{
public *;
}
-keep class com.payumoney.core.response.NetBankingStatusResponse{
public *;
}
-keep class com.payumoney.core.PayUmoneySdkInitializer{
public static * ;
}
-keep class com.payumoney.core.PayUmoneySdkInitializer$PaymentParam{
*;
}
-keep class com.payumoney.core.PayUmoneySdkInitializer$PaymentParam$Builder{
*;
}
-keep class * implements android.os.Parcelable {
*;
}
-keep class * extends java.lang.Enum {
*;
}
-keep class com.payumoney.core.PayUmoneyConfig{
*;
}
-keep class com.payumoney.core.utils.SharedPrefsUtils{
*;
}
-keep class com.payumoney.core.utils.SharedPrefsUtils${
*;
}
-keep class com.payumoney.core.widget.ExpiryDate{
public *;
}
-keep class *{
public *;
public static * ;
}

-dontwarn android.support.**
-keep class android.support.v4.** { *; }
-keep interface android.support.v4.* { *; }
-keep class android.support.v7.* { *; }
-keep interface android.support.v7.* { *; }

-keep public class * extends android.view.View {
public <init>(android.content.Context);
public <init>(android.content.Context, android.util.AttributeSet);
public <init>(android.content.Context, android.util.AttributeSet, int);
public void set*(...);
}

-keep class * implements android.os.Parcelable {
public static final android.os.Parcelable$Creator *;
}

-keep class com.google.** { *; }
-keep class com.zl.reik.* { *; }
-keepattributes Annotation

-keepattributes Signature
-keepattributes Annotation
-keep class com.google.gson.stream.** { *; }
-keep class com.google.* {*;}
-keep class com.google.gson.examples.android.model.** { *; }
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer

-keep class android.support.annotation.Keep
-keep @android.support.annotation.Keep class *
-keepclassmembers @android.support.annotation.Keep class * {
*;
}
-keep class *{
public *;
public static * ;
}
# Payumoney end
