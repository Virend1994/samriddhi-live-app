package com.app.samriddhi.permission;

import java.util.ArrayList;

/**
 * Created by Radha  on 16/02/20.
 */


public interface PermissionResultCallback {
    void PermissionGranted(int request_code);

    void PartialPermissionGranted(int request_code, ArrayList<String> granted_permissions);

    void PermissionDenied(int request_code);

    void NeverAskAgain(int request_code);
}