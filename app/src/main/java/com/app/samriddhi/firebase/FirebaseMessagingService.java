package com.app.samriddhi.firebase;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.app.samriddhi.R;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.auth.LoginActivity;
import com.app.samriddhi.ui.activity.main.ui.AgentListActivity;
import com.app.samriddhi.ui.activity.main.ui.BillingActivity;
import com.app.samriddhi.ui.activity.main.ui.Grievance.HelpSupportStatus;
import com.app.samriddhi.ui.activity.main.ui.HomeActivity;
import com.app.samriddhi.ui.activity.main.ui.LedgerActivity;
import com.app.samriddhi.ui.activity.main.ui.NoOfCopiesActivity;
import com.app.samriddhi.ui.activity.main.ui.NotificationActivity;
import com.app.samriddhi.ui.activity.main.ui.ProfileActivity;
import com.app.samriddhi.ui.activity.main.ui.SalesOrgCityActivity;
import com.app.samriddhi.ui.activity.main.ui.UserCityActivity;
import com.app.samriddhi.ui.activity.main.ui.UserStateActivity;
import com.app.samriddhi.ui.activity.main.ui.WebviewUrl;
import com.app.samriddhi.ui.fragment.DashboardFragment;
import com.app.samriddhi.util.Constant;
import com.google.api.client.googleapis.notifications.NotificationUtils;
import com.google.firebase.messaging.RemoteMessage;
import com.iceteck.silicompressorr.videocompression.Config;

import java.util.Map;

public class FirebaseMessagingService extends com.google.firebase.messaging.FirebaseMessagingService {
    private static final String SCREEN_NAME = "screen_name";
    private static final String PROFILE = "Profile";
    private static final String LEDGER = "Ledger";
    private final String TAG = "FirebaseMessaging";


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        handleNotification(remoteMessage);

    }

    protected void handleNotification(RemoteMessage remoteMessage) {
        Log.e("awfdwqwfasfaf", "Payload" + " " + remoteMessage.getData());

        int getCount = PreferenceManager.getNotificationCount(this);

        int intstantCount = getCount+1;
        PreferenceManager.setNotificationCount(this, intstantCount);

        Log.e("awfdwqwfasfaf", "count" + " " + intstantCount+"");
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {

                //do stuff like remove view etc
                HomeActivity.txt_notification_count.setVisibility(View.VISIBLE);
                HomeActivity.txt_notification_count.setText(intstantCount + "");
            }
        });

        // Payload {message_info=Your complaint with Ref No #C3249/2 has been closed, message=Your complaint with Ref No #C3249/2 has been closed}
        //  Payload {notification_id=345, screen_name=Dashboard, url=, body=qwrqwrwrq      , type=1, title=sefewrwr, BP_Code=10011137}  /*Payload which is working*/

        Log.d(TAG, "handleNotification: " + remoteMessage.getData().get(SCREEN_NAME));
        String title = remoteMessage.getData().get("title");
        String message = remoteMessage.getData().get("body");
        generateNotification(this, message, title, remoteMessage.getData());
    }

    protected void showNotification(Context context, String message, Intent launchIntent, int id, String title) {
        int currentApiVersion = Build.VERSION.SDK_INT;
        int iUniqueId = (int) (System.currentTimeMillis() & 0xfffffff);
        if (TextUtils.isEmpty(title)) {
            title = context.getString(R.string.app_name);
        }
        String channel_id = getString(R.string.app_name);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification.Builder builder = new Notification.Builder(this);
        builder.setWhen(System.currentTimeMillis()).setContentTitle(title);
//        if (!TextUtils.isEmpty(subTitle))
//            builder.setSubText(subTitle);
        builder.setContentText(message).setSmallIcon(R.drawable.samriddhi_app_logo)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.appicon))
                .setContentIntent(PendingIntent.getActivity(context, iUniqueId, launchIntent, PendingIntent.FLAG_UPDATE_CURRENT)).setAutoCancel(true);


        if (currentApiVersion >= Build.VERSION_CODES.O) {
            Log.e("dsgsdgsd", "if");


            CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(channel_id, name, importance);
            notificationManager.createNotificationChannel(notificationChannel);
            builder.setChannelId(channel_id);

            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.notify(100, builder.build());
            playNotificationSound();


        } else {

            NotificationCompat.Builder builders =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.drawable.message_icon)
                            .setContentTitle(title)
                            .setContentText(message);

            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, launchIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT);
            builders.setContentIntent(contentIntent);

            // Add as notification
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            manager.notify(100, builders.build());
            playNotificationSound();
        }

    }


    // Playing notification sound
    public void playNotificationSound() {
        try {
            Uri alarmSound = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                    + "://" + this.getPackageName() + "/raw/notification");
            Ringtone r = RingtoneManager.getRingtone(this, alarmSound);
            r.play();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected void generateNotification(Context context, String message, String title, Map<String, String> data) {
        Intent launchIntent = null;
        if (PreferenceManager.getIsUserLoggedIn(this)) {

            Log.e("sfdsfsdfs", "User type" + " " + PreferenceManager.getPrefUserType(this));
            if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("AG")) {
                if (data.get(SCREEN_NAME).equalsIgnoreCase("billing")) {
                    launchIntent = new Intent(this, BillingActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                } else if (data.get(SCREEN_NAME).equalsIgnoreCase("Grievance")) {
                    launchIntent = new Intent(this, HelpSupportStatus.class)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    launchIntent.putExtra("profileShow", true);
                } else if (data.get(SCREEN_NAME).equalsIgnoreCase(PROFILE)) {
                    launchIntent = new Intent(this, ProfileActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    launchIntent.putExtra("profileShow", true);
                } else if (data.get(SCREEN_NAME).equalsIgnoreCase("Copies")) {
                    launchIntent = new Intent(this, NoOfCopiesActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                } else if (data.get(SCREEN_NAME).equalsIgnoreCase(LEDGER)) {
                    launchIntent = new Intent(this, LedgerActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                } else {
                    launchIntent = new Intent(this, HomeActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                }
            } else {
                if (data.get(SCREEN_NAME).equalsIgnoreCase("billing")) {
                    Log.e("billingScreen", "===");
                    if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("EX") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("CE") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("UE")) {
                        launchIntent = new Intent(this, AgentListActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardBill).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    } else if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("SH")) {
                        launchIntent = new Intent(this, UserCityActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardBill).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    } else if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("CO")) {
                        launchIntent = new Intent(this, UserStateActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardBill).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    }
                } else if (data.get(SCREEN_NAME).equalsIgnoreCase(PROFILE)) {
                    if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("EX") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("CE") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("UE")) {
                        launchIntent = new Intent(this, AgentListActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DrawerProfile).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    } else if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("SH")) {
                        launchIntent = new Intent(this, UserCityActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DrawerProfile).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    } else if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("CO")) {
                        launchIntent = new Intent(this, UserStateActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DrawerProfile).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    }
                } else if (data.get(SCREEN_NAME).equalsIgnoreCase(LEDGER)) {
                    if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("EX") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("CE") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("UE")) {
                        launchIntent = new Intent(this, AgentListActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardOutstanding).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    } else if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("SH")) {
                        launchIntent = new Intent(this, UserCityActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardOutstanding).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    } else if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("CO")) {
                        launchIntent = new Intent(this, UserStateActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardOutstanding).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    }
                } else if (data.get(SCREEN_NAME).equalsIgnoreCase("Copies")) {
                    Log.e("Copies Screen", "===");
                    if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("EX") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("CE") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("UE")) {
                        launchIntent = new Intent(this, AgentListActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardNoOfCopies).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    } else if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("SH")) {
                        launchIntent = new Intent(this, SalesOrgCityActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardNoOfCopies).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    } else if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("CO")) {
                        launchIntent = new Intent(this, UserStateActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardNoOfCopies).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    }
                } else {
                    launchIntent = new Intent(this, HomeActivity.class)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                }
            }
        } else {
            launchIntent = new Intent(this, LoginActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        }

        if (data.get("type").equalsIgnoreCase("20")) {

            // launchIntent = new Intent(this, LoginActivity.class);
            // launchIntent = new Intent(this, WebviewUrl.class)
            // launchIntent = new Intent(this, NotificationActivity.class)
            launchIntent = new Intent(this, HelpSupportStatus.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            int id = (int) (System.currentTimeMillis());
            launchIntent.putExtra("notificationId", data.get("notification_id"));
            launchIntent.putExtra("url", data.get("url"));
            launchIntent.putExtra("type", data.get("type"));
            showNotification(context, message, launchIntent, id, title);

        } else if (data.get("type").equalsIgnoreCase("3") || data.get("type").equalsIgnoreCase("2")) {

            // launchIntent = new Intent(this, LoginActivity.class);
            // launchIntent = new Intent(this, WebviewUrl.class)
            launchIntent = new Intent(this, NotificationActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            int id = (int) (System.currentTimeMillis());
            launchIntent.putExtra("notificationId", data.get("notification_id"));
            launchIntent.putExtra("url", data.get("url"));
            launchIntent.putExtra("type", data.get("type"));
            showNotification(context, message, launchIntent, id, title);

        } else {

            launchIntent = new Intent(this, HomeActivity.class)
                    .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            int id = (int) (System.currentTimeMillis());
            launchIntent.putExtra("notificationId", data.get("notification_id"));
            showNotification(context, message, launchIntent, id, title);
        }
    }

}
