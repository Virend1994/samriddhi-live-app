package com.app.samriddhi.binding;

import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.util.itemdecorator.DividerItemDecoration;
public class DataBindingAdapter {
    private static final String FONTS = "fonts/";
    @BindingAdapter({"bind:font"})
    public static void setFont(TextView textView, String fontName) {
        textView.setTypeface(Typeface.createFromAsset(textView.getContext().getAssets(), FONTS + fontName));
    }

    @BindingAdapter({"bind:et_font"})
    public static void setFont(EditText editText, String fontName) {
        editText.setTypeface(Typeface.createFromAsset(editText.getContext().getAssets(), FONTS + fontName));
    }

    @BindingAdapter({"bind:rb_font"})
    public static void setFont(RadioButton button, String fontName) {
        button.setTypeface(Typeface.createFromAsset(button.getContext().getAssets(), FONTS + fontName));
    }

    @BindingAdapter({"bind:cb_font"})
    public static void setFont(CheckBox button, String fontName) {
        button.setTypeface(Typeface.createFromAsset(button.getContext().getAssets(), FONTS + fontName));
    }

    @BindingAdapter({"bind:orientation", "bind:divider"})
    //set orientation Horizontal(0) or Vertical(1)
    public static void setDivider(RecyclerView recyclerView, int orientation, Drawable divider) {
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), orientation, divider));
    }




}
