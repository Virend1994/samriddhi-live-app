package com.app.samriddhi.api;

import com.app.samriddhi.api.response.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Query;

public interface ApiSapService {

    @GET("ZVO_SMRD_AGENT_VERIFY_SRV/AGNconfSet?$format=json&$filter=")
    Call<BaseResponse> getUserData(@Query("data") String url, @Header("Authorization") String basic);
}
