package com.app.samriddhi.api.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 13-09-2016.
 */
public class StringResponse extends BaseResponse {

    @SerializedName("data")
    public String object;
}
