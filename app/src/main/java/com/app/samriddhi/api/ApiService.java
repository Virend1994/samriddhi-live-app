package com.app.samriddhi.api;


import com.app.samriddhi.api.request.InvoiceRequest;
import com.app.samriddhi.api.response.BaseResponse;
import com.app.samriddhi.api.response.JsonArrayResponse;
import com.app.samriddhi.api.response.JsonObjectResponse;
import com.app.samriddhi.ui.model.ASDDaysModel;
import com.app.samriddhi.ui.model.AgencyRequestCIRReqSetModel;
import com.app.samriddhi.ui.model.AgencyRequestKYBPSetModel;
import com.app.samriddhi.ui.model.AgentChildResultModel;
import com.app.samriddhi.ui.model.AgentResultModel;
import com.app.samriddhi.ui.model.BankListModel;
import com.app.samriddhi.ui.model.BillOutAgentResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCDistWiseResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCResultModel;
import com.app.samriddhi.ui.model.BillingModel;
import com.app.samriddhi.ui.model.BillingOutstandingResultModel;
import com.app.samriddhi.ui.model.CIR_Order_Update;
import com.app.samriddhi.ui.model.CashCreditResultModel;
import com.app.samriddhi.ui.model.CityResultModel;
import com.app.samriddhi.ui.model.ContactUsData;
import com.app.samriddhi.ui.model.DashBoardData;
import com.app.samriddhi.ui.model.EditionModel;
import com.app.samriddhi.ui.model.GrievanceCategoryModel;
import com.app.samriddhi.ui.model.GrievanceCreateIncidentModel;
import com.app.samriddhi.ui.model.GrievanceModel;
import com.app.samriddhi.ui.model.GrievanceOpenItemListModel;
import com.app.samriddhi.ui.model.HintModel;
import com.app.samriddhi.ui.model.IncidentCommentData;
import com.app.samriddhi.ui.model.IncidentDataModel;
import com.app.samriddhi.ui.model.LanguageModel;
import com.app.samriddhi.ui.model.LedgerModel;
import com.app.samriddhi.ui.model.LogoutReqModel;
import com.app.samriddhi.ui.model.LogoutResultModel;
import com.app.samriddhi.ui.model.NotificationModel;
import com.app.samriddhi.ui.model.NumberOfCopiesModel;
import com.app.samriddhi.ui.model.PaymentModel;
import com.app.samriddhi.ui.model.ProposedAgencyModel;
import com.app.samriddhi.ui.model.RootModel;
import com.app.samriddhi.ui.model.SalesDistrictWiseResultModel;
import com.app.samriddhi.ui.model.SalesOrgCityResultModel;
import com.app.samriddhi.ui.model.ServerPortModel;
import com.app.samriddhi.ui.model.StateResultModel;
import com.app.samriddhi.ui.model.UserModel;
import com.app.samriddhi.ui.model.UserProfileModel;
import com.app.samriddhi.ui.model.UsersResultModel;

import java.util.ArrayList;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * Created by Radha Soni on 02-Mar-20.
 */
/**
 * Created by Bhupesh Sen on 19-nov-20.
 */

public interface ApiService {

    @GET("WebAPI_Get_SAP_PATH/{PRD}")
    Call<JsonObjectResponse<ServerPortModel>> getSapPort(@Path("PRD") String envName);

    @GET("User_Reg_Mob/{id}/{mobileno}")
    Call<JsonObjectResponse<UserModel>> getOtp(@Path("id") String agentId, @Path("mobileno") String mobileNumber);

    @POST("Verify")
    Call<BaseResponse> registerUser(@Body UserModel userData);

    @POST("user_login")
    Call<JsonObjectResponse<UserModel>> login(@Body UserModel userData);

    @GET("dashboard_data/{agent_id}")
    Call<DashBoardData> getDashboardCopiesData(@Path("agent_id") String agent_code);

    @GET("EditionList_data/{agent_id}")
    Call<JsonObjectResponse<EditionModel>> getEditionsList(@Path("agent_id") String agent_code);

    @GET("NoOfCopies_data/{BPCODE}/{AGTYPE}/{from_date}/{to_date}/{EditionCode}")
    Call<JsonObjectResponse<NumberOfCopiesModel>> getNumberOfCopies(@Path("BPCODE") String bp_code, @Path("AGTYPE") String agentType, @Path("from_date") String fromDate, @Path("to_date") String toDate, @Path("EditionCode") String editionCode);

    //    @GET("DBUser_Copies_Count/{BPCODE}/{A}/{from_date}/{to_date}")
    @GET("DBUser_Copies_Count_KEY/{BPCODE}/{A}/{from_date}/{to_date}/{search_key}")
    Call<JsonObjectResponse<AgentResultModel>> getAgentList(@Path("BPCODE") String bp_code, @Path("A") String a, @Path("from_date") String fromDate, @Path("to_date") String toDate, @Path("search_key") String searchKey);

    //@GET("DBUser_Copies_Count/{BPCODE}/{C}/{from_date}/{to_date}")
    @GET("DBUser_Copies_Count_KEY/{BPCODE}/{C}/{from_date}/{to_date}/{search_key}")
    Call<JsonObjectResponse<CityResultModel>> getCityList(@Path("BPCODE") String bp_code, @Path("C") String a, @Path("from_date") String fromDate, @Path("to_date") String toDate, @Path("search_key") String searchKey);

    @GET("todays-copy/api/all-in-one/{BPCODE}")
    Call<JsonObjectResponse<StateResultModel>> getStateList(@Path("BPCODE") String bp_code);

    @GET("User_Profile/{BPCODE}")
    Call<JsonObjectResponse<UserProfileModel>> getUserProfile(@Path("BPCODE") String bp_code);

    @GET("Bill_Month/{BPCODE}")
    Call<JsonObjectResponse<BillingModel>> getBillingData(@Path("BPCODE") String bp_code);

    @GET("Get_ContactUs/")
    Call<JsonArrayResponse<ContactUsData>> getContactUs();

    @GET("Ledger_data/{BPCODE}/{from_date}/{to_date}/{EditionCode}")
    Call<JsonObjectResponse<LedgerModel>> getLedger(@Path("BPCODE") String bp_code, @Path("from_date") String fromDate, @Path("to_date") String toDate, @Path("EditionCode") String editionCOde);

    @GET("Get_Hint/{language}")
    Call<JsonArrayResponse<HintModel>> getAppHints(@Path("language") String langCode);

    @POST("Profile_Update")
    Call<BaseResponse> submitProfileData(@Body UserProfileModel userData);

    @GET("User_Reg_Mob_Forgot/{mobileno}")
    Call<JsonObjectResponse<UserModel>> getOtpForForgotPassword(@Path("mobileno") String mobileNumber);


    // @GET("User_Reg_Mob_Forgot/{mobileNo}")
    @GET("agency/getOtp/{mobile_no}")
    Call<String> agencyOtpVerify(@Path("mobile_no") String mobileNo);

    @POST("Update_PWD")
    Call<BaseResponse> updatePassword(@Body UserModel userData);

    @POST("Update_PaymentStatus")
    Call<BaseResponse> updatePaymentStatus(@Body PaymentModel mData);

    ////Outstanding User type wise lists

    @GET("DBUser_Outstanding_Count_KEY/{BP_CODE}/{S}/*")
    Call<JsonObjectResponse<StateResultModel>> getOutStandingStateList(@Path("BP_CODE") String bp_code, @Path("S") String state);

    @GET("DBUser_Outstanding_Count_KEY/{BP_CODE}/{C}/{search_Key}")
    Call<JsonObjectResponse<CityResultModel>> getOutStandingCityList(@Path("BP_CODE") String bp_code, @Path("C") String city, @Path("search_Key") String stateId);

    @GET("DBUser_Outstanding_Count_KEY/{BP_CODE}/{A}/{search_Key}")
    Call<JsonObjectResponse<AgentResultModel>> getOutstandingAgentList(@Path("BP_CODE") String bp_code, @Path("A") String city, @Path("search_Key") String cityId);

    //////Billing User type wise lists

    @GET("billing/api/all-in-one/{BPCODE}")
    Call<JsonObjectResponse<BillingOutstandingResultModel>> getBillingStateList(@Path("BPCODE") String bp_code);

    @GET("billing/api/all-in-one/{BPCODE}")
    Call<JsonObjectResponse<CityResultModel>> getBillingCityList(@Path("BPCODE") String bp_code, @Query("s") String stateCode);

    @GET("DBUser_Billing_Count_KEY/{BP_CODE}/{A}/{from_date}/{to_date}/{search_key}")
    Call<JsonObjectResponse<AgentResultModel>> getBillingAgentList(@Path("BP_CODE") String bp_code, @Path("A") String state, @Path("from_date") String fromDate, @Path("to_date") String toDate, @Path("search_key") String search_key);

    @Streaming
    @GET
    Call<ResponseBody> downloadFileByUrl(@Url String fileUrl);

    @GET("GetAGChield/{BP_CODE}")
    Call<JsonObjectResponse<AgentChildResultModel>> getAgentChildData(@Path("BP_CODE") String bp_code);

    @GET("GetIncidentCategory/")
    Call<JsonArrayResponse<GrievanceModel>> getIncidentOptions();

    @GET("GetIncidentStatusType/")
    Call<JsonArrayResponse<GrievanceCategoryModel>> getIncidentCategoryType();

    @POST("CreateIncident")
    Call<BaseResponse> createGrievance(@Body GrievanceCreateIncidentModel mData);

    @GET("GetIncidentReportCATWISE/{BP_CODE}/{Incident_Status_Code}")
    Call<JsonArrayResponse<GrievanceOpenItemListModel>> getOpenIncident(@Path("BP_CODE") String bp_code, @Path("Incident_Status_Code") String statusCode);

    @GET("GetIncidentList/{BP_CODE}/{Status_CODE}/{CATEGORY_CODE}")
    Call<JsonObjectResponse<IncidentDataModel>> getSubIncidentData(@Path("BP_CODE") String bp_code, @Path("Status_CODE") String statusCode, @Path("CATEGORY_CODE") String categoryCode);

    @GET("GetIncidentComments/{incident_id}")
    Call<JsonArrayResponse<IncidentCommentData>> getIncidentComments(@Path("incident_id") String incident_id);

    @POST("UpdateFeedback")
    Call<BaseResponse> updateGrievanceFeedback(@Body GrievanceCreateIncidentModel feedbackData);

    @POST("UpdateIncidentComment")
    Call<BaseResponse> submitCommentOnIncident(@Body GrievanceCreateIncidentModel commentData);


    //////Grievance User type wise lists


    @GET("GETIncident_Count/{BP_CODE}/{S}/*")
    Call<JsonObjectResponse<StateResultModel>> getGrievanceStateList(@Path("BP_CODE") String bp_code, @Path("S") String state);

    @GET("GETIncident_Count/{BP_CODE}/{C}/{search_key}")
    Call<JsonObjectResponse<CityResultModel>> getGrievanceCityList(@Path("BP_CODE") String bp_code, @Path("C") String state, @Path("search_key") String search_key);

    @GET("GETIncident_Count/{BP_CODE}/{A}/{search_key}")
    Call<JsonObjectResponse<AgentResultModel>> getGrievanceAgentList(@Path("BP_CODE") String bp_code, @Path("A") String state, @Path("search_key") String search_key);

/////NumberOfCopies User Type List


    @GET("DBUser_Copies_Count_KEY/{BP_CODE}/{P}/{from_date}/{to_date}/{search_key}")
    Call<JsonObjectResponse<CashCreditResultModel>> getCrediCashData(@Path("BP_CODE") String bp_code, @Path("P") String a, @Path("from_date") String fromDate, @Path("to_date") String toDate, @Path("search_key") String searchKey);

    //////sales Group ////
    @GET("todays-copy/api/all-in-one/{BPCODE}")
    Call<JsonObjectResponse<UsersResultModel>> getSalesGroupUsers(@Path("BPCODE") String bp_code, @Query("s") String stateCode,@Query("u") String unitCode);

    //////sales Group ////
    @GET("billing/api/all-in-one/{BPCODE}")
    Call<JsonObjectResponse<BillOutCityUPCResultModel>> getBillOutCityUPC(@Path("BPCODE") String bp_code, @Query("s") String stateCode, @Query("u") String unitCode);

    ////Sales org City list
   // @GET("DBUser_Copies_Count_KEY/{BP_CODE}/{L}/{from_date}/{to_date}/{state_code}/{cashCreditCode}/{search_key}")
   // Call<JsonObjectResponse<SalesOrgCityResultModel>> getSalesOrgCity(@Path("BP_CODE") String bp_code, @Path("L") String a, @Path("from_date") String fromDate, @Path("to_date") String toDate, @Path("state_code") String stateCode, @Path("cashCreditCode") String cashCreditCode, @Path("search_key") String searchKey);


    ////Sales org City list
    @GET("todays-copy/api/all-in-one/{BPCODE}")
    Call<JsonObjectResponse<SalesOrgCityResultModel>> getSalesOrgCity(@Path("BPCODE") String bp_code, @Query("s") String stateCode);


    ////Sales org District wise list
    @GET("todays-copy/api/all-in-one/{BPCODE}")
    Call<JsonObjectResponse<SalesDistrictWiseResultModel>> getSalesDistrictWise(@Path("BPCODE") String bp_code, @Query("s") String stateCode, @Query("u") String unitCode, @Query("cc") String marketCode,@Query("cu") String cityUPC);

    @GET("billing/api/all-in-one/{BPCODE}")
    Call<JsonObjectResponse<BillOutCityUPCDistWiseResultModel>> getBillOutCityUPCDist(@Path("BPCODE") String bp_code, @Query("s") String stateCode, @Query("u") String unitCode, @Query("cc") String dChennal, @Query("cu") String cityUPC);


    ////Agent Wise list
    @GET("todays-copy/api/all-in-one/{BPCODE}")
    Call<JsonObjectResponse<AgentResultModel>> getAgentWiseList(@Path("BPCODE") String bp_code, @Query("s") String stateCode, @Query("u") String unitCode, @Query("cc") String marketCode,@Query("cu") String cityUPC,@Query("sd") String salseDist);

     ////Agent Wise list
    @GET("billing/api/all-in-one/{BPCODE}")
    Call<JsonObjectResponse<BillOutAgentResultModel>> getBillOutAgentWiseCopies(@Path("BPCODE") String bp_code, @Query("s") String stateCode, @Query("u") String unitCode, @Query("cc") String dChennal, @Query("cu") String cityUPC, @Query("sd") String salseDist);

    ////Agent Wise list
    @GET("todays-copy/api/all-in-one/{BPCODE}")
    Call<JsonObjectResponse<AgentResultModel>> getAgentList(@Path("BPCODE") String bp_code);


    ////Agent Wise list
    @GET("billing/api/all-in-one/{BPCODE}")
    Call<JsonObjectResponse<BillOutAgentResultModel>> getBillOutAgentList(@Path("BPCODE") String bp_code);



    //////Billing confirmation Api
    @POST("BillConfirm")
    Call<BaseResponse> confirmBill(@Body InvoiceRequest invoiceRequest);

    ////Bank list model
    @GET("Bank_List/")
    Call<JsonArrayResponse<BankListModel>> getBankList();

    @GET("Notification_List/{BP_CODE}")
    Call<JsonArrayResponse<NotificationModel>> getNotifications(@Path("BP_CODE") String bpCode);

    @GET("Notification_Read/{BP_CODE}")
    Call<BaseResponse> notificationRead(@Path("BP_CODE") String bpCode);

    /**
     * language change request
     */
    @POST("user_len/")
    Call<BaseResponse>languageChange(@Body LanguageModel languageModel);


    @GET("dashboard_data_IH/{agent_id}")
    Call<DashBoardData> getDashboardDataForInternalUsers(@Path("agent_id") String agent_code);

    @GET("Notification_Read_ID/{Notification_ID}")
    Call<BaseResponse>readNotifications(@Path("Notification_ID")String notificationId);




    //Start KYBP API
    @GET("agency/getUnit/{BPCODE}/{Statecode}")
    Call<String> getUnit(@Path("BPCODE") String bp_code, @Path("Statecode") String StateCode);

    @GET("agency/getCity/{BPCODE}/{Statecode}")
    Call<String> getCity(@Path("BPCODE") String bp_code, @Path("Statecode") String StateCode);

    @GET("agency/getState/{BPCODE}")
    Call<String> getStateMaster(@Path("BPCODE") String bp_code);

    @GET("agency/getAddressProof/{BPCODE}")
    Call<String> getAdressProof(@Path("BPCODE") String bp_code);


    @GET("agency/getRealtion/{BPCODE}")
    Call<String> getRelationShip(@Path("BPCODE") String bp_code);


    //end KYBP API


    //child education api
    @GET("agency/getEducation/{BPCODE}")
    Call<String> getEducation(@Path("BPCODE") String bp_code);



    //new Agency Request Api
    @GET("agency/agrequest")
    Call<String> getAgencyAllDetails();


    @GET("agency/getMaritialStatus/{BPCODE}")
    Call<String> getMaritial(@Path("BPCODE") String bp_code);


    @GET("agency/getNewsPaper/{BPCODE}")
    Call<String> getNewsPaper(@Path("BPCODE") String bp_code);



    @GET("agency/getCluster/{BPCODE}/{UnitID}")
    Call<String> getSalesCluster(@Path("BPCODE") String bp_code,@Path("UnitID") String StateCode);


    @GET("agency/getSalesOff/{BPCODE}/{UnitID}")
    Call<String> getSalesOffice(@Path("BPCODE") String bp_code,@Path("UnitID") String StateCode);


    @GET("agency/getDesignation/{BPCODE}")
    Call<String> getDesignation(@Path("BPCODE") String bp_code);


    @GET("agency/getDepartment/{BPCODE}")
    Call<String> getDepartment(@Path("BPCODE") String bp_code);


    @GET("agency/getCity/{BPCODE}/{Statecode}")
    Call<String> getDistrict(@Path("BPCODE") String bp_code,@Path("Statecode") String StateCode);

    @GET("agency/getTown/{BPCODE}")
    Call<String> getTown(@Path("BPCODE") String bp_code);


    @POST("agency/agrequestheader/")
    Call<String> addAgencyRequest(@Body RootModel userData);


    @GET("agency/getTownCity/{BPCODE}/{city_code}")
    Call<String> getTownCity(@Path("BPCODE") String bp_code,@Path("city_code") String city_code);


    @GET("agency/getLocation/{BPCODE}")
    Call<String> getLocation(@Path("BPCODE") String bp_code);


    @GET("agency/getLocationCity/{BPCODE}/{city_code}")
    Call<String> getLocationCity(@Path("BPCODE") String bp_code,@Path("city_code") String city_code);

    @GET("agency/TeamMaster/?")
    Call<String> getTeamMaster(@Query("state_id") String bp_code,@Query("unit_id") String unit_id,@Query("access_type") String access_type);

    @GET("agency/TeamMaster/?")
    Call<String> getSHTeamMaster(@Query("state_id") String unit_id,@Query("access_type") String access_type);

   // @GET("agency/TeamMaster/")
   // Call<String> getTeamMaster(@QueryMap Map<String,String> userData);

    @POST("agency/agrequest/")
    Call<RootModel> postAgencyData(@Body ArrayList<RootModel> userData);



    @GET("agency/getSalesDist/{bpcode}/{UnitID}")
    Call<String> getSalesDist(@Path("bpcode") String bp_code,@Path("UnitID") String Copies);



    @GET("agency/getlast7daysavg/{UserBPCODE}/{AGBPCODE}/{UnitCode}")
    Call<String> getAgencyDetails(@Path("UserBPCODE") String bp_code,@Path("AGBPCODE") String agbpcode,@Path("UnitCode") String UnitCode);


    @GET("bill/getBillConfirm/{Bill_No}")
    Call<String> getBillStatus(@Path("Bill_No") String billNo);


    @GET("agency/agrequestheader/?ordering=-ag_req_id")
    Call<String> getShowAgencyRequest();


    @GET("agency/agrequestheader/?")
    Call<String> getShowAgencyRequestoLocation(@Query("location_id") String unit_id, @Query("ordering") String ReqId);



    @GET("agency/agrequestheader/?")
    Call<String> getShowAgencyRequest(@Query("ordering") String ReqId,@Query("unit_id") String unit_id,@Query("location_id") String location_id);


    @GET("agency/agrequestheader/?")
    Call<String> getShowAgencyRequestUnit(@Query("ordering") String ReqId,@Query("unit_id") String unit_id);

    @GET("agency/getUnit/{BPCODE}")
    Call<String> getUnit(@Path("BPCODE") String bp_code);


    @POST("agency/agdetail/")
    Call<String> agDetail(@Body Map<String,Object> userData);


    @GET("agency/agdetail/?")
    Call<String> getAgDetail(@Query("ag_req_id") String ReqId);

    @Headers({"Content-Type: application/json"})
    @PUT("/agency/agkybp/{kybp_id}/")
    Call<String> updateKYPRequest(@Path("kybp_id") String bp_code,@Body AgencyRequestKYBPSetModel userData);

    @POST("/agency/agkybp/")
    Call<String> addKyBPForm(@Body AgencyRequestKYBPSetModel userData);

    @POST("/rawsap/CIR_Order_Update/")
    Call<String> addCIR_Order_Update(@Body ArrayList<CIR_Order_Update> cirListData);

    @Headers({"Content-Type: application/json"})
    @PUT("/rawsap/CIR_Order_Update/{id}/")
    Call<String> UpdateAddCIR_Order_Update(@Path("id") String id,@Body CIR_Order_Update cirListData);

    @GET("/agency/agkybp/?")
    Call<String> getKyBPForm(@Query("ag_detail_id") String ReqId);


    @GET("/agency/agsurvey/?")
    Call<String> getSURVEYFORM(@Query("ag_detail_id") String ReqId);



    @GET("/agency/agcir/?")
    Call<String> getCIRFORM(@Query("ag_detail_id") String ReqId,@Query("mainorjj") String mainorjj);
    @Headers({"Content-Type: application/json"})



    @PUT("/agency/agdetail/{ag_detail_id}/")
    Call<String> addUOHRemark(@Path("ag_detail_id") String bp_code,@Body ProposedAgencyModel userData);

    @GET("agency/getASDAMT/{bpcode}/{unitid}/{salesgrp}/{mainsalesoff}/{maincopies}/{jjsalesoff}/{jjcopies}")
    Call<String> getASDDetails(@Path("bpcode") String bp_code,@Path("unitid") String unitid,@Path("salesgrp") String salesgrp,@Path("mainsalesoff") String mainOffice,@Path("maincopies") int mainCopy,@Path("jjsalesoff") String jjOffice,@Path("jjcopies") int jjCopy);

    @POST("/agency/agcir/")
    Call<String> addCirRequest(@Body AgencyRequestCIRReqSetModel userData);

    @Headers({"Content-Type: application/json"})
    @PUT("/agency/agcir/{cirreq_id}/")
    Call<String> updateCirRequest(@Path("cirreq_id") String bp_code,@Body  AgencyRequestCIRReqSetModel userData);


    @Headers({"Content-Type: application/json"})
    @PUT("/agency/agdetail/{ag_detail_id}/")
    Call<String> agDelete(@Path("ag_detail_id") String bp_code,@Body Map<String,Object> userData);

    @POST("agency/agsurvey/")
    Call<String> addSurveyRequest(@Body Map<String,Object> userData);

    @Headers({"Content-Type: application/json"})
    @PUT("/agency/agsurvey/{survey_id}/")
    Call<String> updateSurveyRequest(@Path("survey_id") String bp_code,@Body Map<String,Object> userData);
    @GET("agency/agrequest/{ag_req_id}")
    Call<String> getRequestData(@Path("ag_req_id") String agredI);


    @GET("rawsap/OrderChangeReasonViewSet/")
    Call<String> getOrderChangeReasonViewSet();

    @GET("agency/getEdition/{BPCODE}")
    Call<String> getEdition(@Path("BPCODE") String bp_code);

    @GET("agency/getEdition/{BPCODE}/{PUBLICATION}?")
    Call<String> getEditionByUnitId(@Path("BPCODE") String bp_code,@Path("PUBLICATION") String PUBLICATION,@Query("unit_id") String ReqId);



    @GET("agency/getDroppingPoint/{BPCODE}/{route_id}")
    Call<String> getDroppingPoint(@Path("BPCODE") String bp_code,@Path("route_id") String route_id);

    @GET("agency/getPricegrp/{bpcode}/{Copies}/{UnitID}/{Sale_Type}/{Agency_Type}")
    Call<String> getASDPageGroup(@Path("bpcode") String bp_code,@Path("Copies") String Copies,@Path("UnitID") String unitID,
    @Path("Sale_Type")String Sale_Type,@Path("Agency_Type")String Agency_Type);

    @GET("agency/getRoute/{BPCODE}?")
    Call<String> getRouteByUnitId(@Path("BPCODE") String bp_code,@Query("unit_id") String ReqId);

    @POST("agency/getASDDays/{BPCODE}")
    Call<String> getASDDays(@Path("BPCODE") String bp_code,@Body ASDDaysModel userData);

    @GET("rawsap/ZVT_PORTAL_CIRViewSet/")
    Call<String> getOrderSupplyString(@QueryMap Map<String,String> userData);

    @Headers({"Content-Type: application/json"})
    @PUT("agency/agdetail/{ID}/")
    Call<String> agencyVerifyOtp(@Path("ID") String ID,@Body Map<String,Object> userData);


    @POST("logout")
    Call<LogoutResultModel> getLogoutStatus(@Body LogoutReqModel userData);

}

