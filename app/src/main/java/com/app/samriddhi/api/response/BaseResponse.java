package com.app.samriddhi.api.response;

import com.app.samriddhi.base.model.MetaInfo;
import com.app.samriddhi.util.StringUtility;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class BaseResponse {
    @SerializedName("code")
    @Expose
    public String replyCode;
    @SerializedName("reply")
    @Expose
    public String replyMsg;
    @SerializedName("id_exist")
    @Expose
    public Boolean idExist;
    @SerializedName("max_limit")
    @Expose
    public Integer maxLimit;
    @SerializedName("current_page")
    @Expose
    public Integer currentPage;
    @SerializedName("displayed_record")
    @Expose
    public Integer displayedRecord;
    @SerializedName("miss_count")
    @Expose
    public Integer missCount;

    @SerializedName("Outstanding")
    @Expose
    public String outStanding;

    @SerializedName("isedit")
    @Expose
    public Boolean isedit;

    @SerializedName("showprofile")
    @Expose
    public Boolean showProfile;

    @SerializedName("PayUMoney")
    @Expose
    public Boolean PayUMoney;
    @SerializedName("BankList")
    @Expose
    public Boolean BankList;
    @SerializedName("country_code")
    @Expose
    public String country_code;
    @SerializedName("country_name")
    @Expose
    public String country_name;
    @SerializedName("meta")
    @Expose
    public MetaInfo meta;
    @SerializedName("lastmonth_year_Bill_Amt")
    @Expose
    public String lastMonthBillAmt;
    @SerializedName("lastmonth_year")
    @Expose
    public String lastMonthYear;
    @SerializedName("AgencyName")
    @Expose
    public String AgencyName;

    public Boolean getShowProfile() {
        return showProfile;
    }

    public void setShowProfile(Boolean showProfile) {
        this.showProfile = showProfile;
    }

    public Boolean getIsedit() {
        return isedit;
    }

    public void setIsedit(Boolean isedit) {
        this.isedit = isedit;
    }

    public boolean isSuccess() {
        return StringUtility.validateString(replyCode) && replyCode.equals("success");
    }


}
