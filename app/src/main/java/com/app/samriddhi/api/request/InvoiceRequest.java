package com.app.samriddhi.api.request;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class InvoiceRequest {
    @SerializedName("Confirm_Status")
    @Expose
    private final int Confirm_Status;
    @SerializedName("BP_Code")
    @Expose
    private final String bPCode;
    @SerializedName("device_id")
    @Expose
    private final String deviceId;
    @SerializedName("Bill_Period")
    @Expose
    private final String billPeriod;
    @SerializedName("Bill_No")
    @Expose
    private final String billNo;
    @SerializedName("Confirm_Remark")
    @Expose
    private final String confirmRemark;
    @SerializedName("Confirm_By")
    @Expose
    private final String confirmBy;

    @SerializedName(("Bill_Amount"))
    @Expose
    private final String billAmount;
    @SerializedName("Bill_copies")
    @Expose
    private final String billCopies;
    @SerializedName("Confirm_On_Number ")
    @Expose
    private final String Confirm_On_Number ;




    public InvoiceRequest(String bPCode, String deviceId, String billPeriod, String billNo, String confirmRemark, String confirmBy, String billAmt, String billCopies,int Confirm_Status,String Confirm_On_Number) {
        this.bPCode = bPCode;
        this.deviceId = deviceId;
        this.billPeriod = billPeriod;
        this.billNo = billNo;
        this.confirmRemark = confirmRemark;
        this.confirmBy = confirmBy;
        this.billAmount = billAmt;
        this.billCopies = billCopies;
        this.Confirm_Status=Confirm_Status;
        this.Confirm_On_Number =Confirm_On_Number;
    }
}
