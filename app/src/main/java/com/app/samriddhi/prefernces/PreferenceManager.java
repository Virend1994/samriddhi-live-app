package com.app.samriddhi.prefernces;

import android.content.Context;
import android.content.SharedPreferences;

import com.app.samriddhi.util.SystemUtility;


/**
 * Created by radha soni on 28/02/2020.
 */

public class PreferenceManager {

    public static final String PREF_BASE_URL = "samriddhi_base_url";
    public static final String PREF_FCM_ID = "samriddhi_fcm_id";
    public static final String PREF_LANG = "samriddhi_app_lang";
    public static final String PREF_IS_LOGGED_IN = "samriddhi_is_logged_in";
    public static final String PREF_USER_DATA = "samriddhi_user_data";
    public static final String PREF_SERVER_PORT = "samriddhi_sap_port";
    public static final String PREF_SERVER_USER_NAME = "samriddhi_sap_username";
    public static final String PREF_SERVER_P = "samriddhi_sap_pwd";
    public static final String PREF_SERVER_TYPE = "samridhhi_sap_server_type";
    public static final String PREF_IS_LANG_SHOWN = "samriddhi_lang_dialog_shown";
    public static final String PREF_AGENT_NAME = "samriddhi_agent_name";
    public static final String PREF_AGENT_ID = "samriddhi_sgent_id";
    public static final String PREF_vkorg = "samriddhi_vkorg";
    public static final String PREF_vkbur = "samriddhi_vkbur";
    public static final String PREF_vkgrp = "samriddhi_vkgrp";
    public static final String PREF_USER_TYPE = "samriddhi_user_type";
    public static final String PREF_USER_OUT_STANDING = "samriddhi_user_outstanding";
    public static final String PREF_AGENT_PHONE = "samriddhi_agent_phone";
    public static final String USER_EMAIL = "user_email";
    public static final String USER_MOBILE = "user_mobile";
    public static final String PHONE_PATTERN = "^[987]\\d{9}$";
    public static final long MENU_DELAY = 300;
    public static final String PREF_USER_EMAIL = "samriddhi_user_email";
    public static final String PREF_USER_CITYUPC = "samriddhi_user_city_upc";
    public static final long PREF_DASHBOARD_SHOT = 1l;
    public static final long PREF_BILLING_SHOT = 12;
    public static final long PREF_LEDGER_SHOT = 13;
    public static final long PREF_NO_OF_COPIES_SHOT = 14;
    public static final long PREF_NO_OF_COPIES_FILTER_SHOT = 15;
    public static final long PREF_INVOICE_SHOT = 16;
    public static final String PREF_SHOW_PROFILE = "samriddhi_show_profile";
    public static final String PREF_AGENT_PARENT_FLOW = "samriddhi_agnet_flow";
    public static final String PREF_PARENT_AGENT_NAME = "samriddhi_parent_agent_name";
    public static final String PREF_PARENT_AGENT_ID = "samriddhi_parent_agent_id";
    public static final String PREF_PARENT_USER_TYPE = "samriddhi_parent_user_type";
    public static final String PREF_PARENT_EMAIL = "samriddhi_parent_email";
    public static final String PREF_PARENT_PHONE = "samriddhi_parent_phone";
    public static final String PREF_IS_SCREENSHOT = "samriddhi_is_sacreenshot";
    public static final String PREF_NOTIFICATION_COUNT = "samriddhi_notification_count";
    public static final String Menu_TYPE = "Menu_TYPE";
    public static final String Complaint_number = "Complaint_number";
    public static final String Complaint_Type = "Complaint_Type";
    public static String USER_DETAILS = "user_details";
    public static int selectedTheme = -1;
    private static SharedPreferences mSharedPreferences;
    ///PayUMoney shared refrences
    private final String dummyAmount = "10";//"10";
    private final String dummyEmail = "xyz@gmail.com";//"";//d.basak.db@gmail.com
    private final String productInfo = "product_info";// "product_info";
    private final String firstName = "firstname"; //"firstname";
    private boolean isOverrideResultScreen = true;

    public static SharedPreferences getSharedPreferences(Context context) {
        if (mSharedPreferences == null) {
            mSharedPreferences = android.preference.PreferenceManager.getDefaultSharedPreferences(context);
        }
        return mSharedPreferences;
    }


    public static void setComplaint_Type(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(Complaint_Type, data);
        editor.commit();
        SystemUtility.setAppLocale(context, data);
    }


    public static String getComplaint_Type(Context context) {
        return getSharedPreferences(context).getString(Complaint_Type, "");
    }

    public static void setComplaint_number(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(Complaint_number, data);
        editor.commit();
        SystemUtility.setAppLocale(context, data);
    }


    public static String getComplaint_number(Context context) {
        return getSharedPreferences(context).getString(Complaint_number, "");
    }


    public static void setPushRegId(Context context, String id) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_FCM_ID, id);
        editor.commit();
    }

    public static void setAppLang(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_LANG, data);
        editor.commit();
        SystemUtility.setAppLocale(context, data);
    }

    public static void setUserLoggedIn(Context context, boolean login) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(PREF_IS_LOGGED_IN, login);
        editor.commit();
    }


    public static void setLangDialogShown(Context context, boolean login) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(PREF_IS_LANG_SHOWN, login);
        editor.commit();
    }

    public static void setVkorg(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_vkorg, data);
        editor.commit();
    }


    public static void setAgencyMenuType(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(Menu_TYPE, data);
        editor.commit();
    }


    public static void setVkbur(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_vkbur, data);
        editor.commit();
    }

    public static void setvkgrp(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_vkgrp, data);
        editor.commit();
    }
    public static void setPrefAgentId(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_AGENT_ID, data);
        editor.commit();
    }

    public static void setAgentName(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_AGENT_NAME, data);
        editor.commit();
    }


    public static void setPrefUserType(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_USER_TYPE, data);
        editor.commit();
    }

    public static void setUserData(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_USER_DATA, data);
        editor.commit();
    }



    public static String getVkorg(Context context) {
        return getSharedPreferences(context).getString(PREF_vkorg, "");

    }

    public static String getMenu_TYPE(Context context) {
        return getSharedPreferences(context).getString(Menu_TYPE, "");

    }


    public static String getVkbur(Context context) {
        return getSharedPreferences(context).getString(PREF_vkbur, "");

    }

    public static String getvkgrp(Context context) {
        return getSharedPreferences(context).getString(PREF_vkgrp, "");
    }
    public static boolean getIsUserLoggedIn(Context context) {
        return getSharedPreferences(context).getBoolean(PREF_IS_LOGGED_IN, false);
    }

    public static boolean getIsLangDialogShown(Context context) {
        return getSharedPreferences(context).getBoolean(PREF_IS_LANG_SHOWN, false);
    }

    public static String getPushRegId(Context context) {
        return getSharedPreferences(context).getString(PREF_FCM_ID, "");
    }

    public static String getAppLang(Context context) {
        return getSharedPreferences(context).getString(PREF_LANG, "");
    }

    public static String getPrefUserType(Context context) {
        return getSharedPreferences(context).getString(PREF_USER_TYPE, "");
    }

    public static String getPrefAgentName(Context context) {
        return getSharedPreferences(context).getString(PREF_AGENT_NAME, "");
    }


    public static String getAgentId(Context context) {
        return getSharedPreferences(context).getString(PREF_AGENT_ID, "");
    }


    public static String getUserData(Context context) {
        return getSharedPreferences(context).getString(PREF_USER_DATA, "");
    }


    public static void clearAll(Context context) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.clear();
        editor.commit();

    }

    public static void setPrefServerPort(Context context, String id) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_SERVER_PORT, id);
        editor.commit();
    }

    public static void setPrefServerPwd(Context context, String id) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_SERVER_P, id);
        editor.commit();
    }

    public static void setPrefServerUserName(Context context, String id) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_SERVER_USER_NAME, id);
        editor.commit();
    }

    public static String getPrefServerPort(Context context) {
        return getSharedPreferences(context).getString(PREF_SERVER_PORT, "");
    }

    public static String getPrefServerUserName(Context context) {
        return getSharedPreferences(context).getString(PREF_SERVER_USER_NAME, "");
    }

    public static String getPrefServerPwd(Context context) {
        return getSharedPreferences(context).getString(PREF_SERVER_P, "");
    }

    public static void setUserOustStnading(Context context, String id) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_USER_OUT_STANDING, id);
        editor.commit();
    }

    public static String getUSerOutStanding(Context context) {
        return getSharedPreferences(context).getString(PREF_USER_OUT_STANDING, "");
    }

    public static void setUserEmail(Context context, String email) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_USER_EMAIL, email);
        editor.commit();
    }

    public static String getUserEmail(Context context) {
        return getSharedPreferences(context).getString(PREF_USER_EMAIL, "");
    }



    public static void setUserCityUpc(Context context, String email) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_USER_CITYUPC, email);
        editor.commit();
    }

    public static String getUserCityUpc(Context context) {
        return getSharedPreferences(context).getString(PREF_USER_CITYUPC, "");
    }

    public static void setUserPhone(Context context, String id) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_AGENT_PHONE, id);
        editor.commit();
    }

    public static String getUSerPhone(Context context) {
        return getSharedPreferences(context).getString(PREF_AGENT_PHONE, "");
    }

    public static void setShowUserProfile(Context context, boolean login) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(PREF_SHOW_PROFILE, login);
        editor.commit();
    }

    public static boolean getshowUserProfile(Context context) {
        return getSharedPreferences(context).getBoolean(PREF_SHOW_PROFILE, false);
    }

    public static void setPrefAgentParentValue(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_AGENT_PARENT_FLOW, data);
        editor.commit();
    }

    public static String getPrefAgentParentValue(Context context) {
        return getSharedPreferences(context).getString(PREF_AGENT_PARENT_FLOW, "");
    }

    public static void setParentAgentName(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_PARENT_AGENT_NAME, data);
        editor.commit();
    }

    public static String getParentAgentName(Context context) {
        return getSharedPreferences(context).getString(PREF_PARENT_AGENT_NAME, "");
    }

    public static void setParentAgentId(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_PARENT_AGENT_ID, data);
        editor.commit();
    }

    public static String getParentAgentId(Context context) {
        return getSharedPreferences(context).getString(PREF_PARENT_AGENT_ID, "");


    }

    public static void setParentUserType(Context context, String data) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_PARENT_USER_TYPE, data);
        editor.commit();
    }

    public static String getParentUserType(Context context) {
        return getSharedPreferences(context).getString(PREF_PARENT_USER_TYPE, "");


    }

    public static void setParentUserEmail(Context context, String email) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_PARENT_EMAIL, email);
        editor.commit();
    }

    public static String getParentUserEmail(Context context) {
        return getSharedPreferences(context).getString(PREF_PARENT_EMAIL, "");
    }

    public static void setParentUserPhone(Context context, String id) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_PARENT_PHONE, id);
        editor.commit();
    }

    public static String getParentUserPhone(Context context) {
        return getSharedPreferences(context).getString(PREF_PARENT_PHONE, "");
    }

    public static boolean getIsScreenshotAllowed(Context context) {
        return getSharedPreferences(context).getBoolean(PREF_IS_SCREENSHOT, false);
    }

    public static void setScreenshotAllowed(Context context, boolean login) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putBoolean(PREF_IS_SCREENSHOT, login);
        editor.commit();
    }

    public static void setNotificationCount(Context context, int count) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putInt(PREF_NOTIFICATION_COUNT, count);
        editor.commit();
    }

    public static int getNotificationCount(Context context) {

        return getSharedPreferences(context).getInt(PREF_NOTIFICATION_COUNT, 0);
    }

    public boolean isOverrideResultScreen() {
        return isOverrideResultScreen;
    }

    public void setOverrideResultScreen(boolean overrideResultScreen) {
        isOverrideResultScreen = overrideResultScreen;
    }

    public static void setBaseURL(Context context,String url) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.putString(PREF_BASE_URL, url);
        editor.apply();
    }

    public static String getBaseURL(Context context) {
        return getSharedPreferences(context).getString(PREF_BASE_URL, "http://live.dbsamriddhi.in/");
    }
    public static void clearURL(Context context) {
        SharedPreferences.Editor editor = getSharedPreferences(context).edit();
        editor.remove(PREF_BASE_URL);
        editor.apply();

    }

}
