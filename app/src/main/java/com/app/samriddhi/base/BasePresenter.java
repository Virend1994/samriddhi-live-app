package com.app.samriddhi.base;


import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.response.BaseResponse;
import com.app.samriddhi.ui.view.IView;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Radha Soni on 02/21/2020.
 */

public abstract class BasePresenter<I extends IView> {

    I iView;

    public BasePresenter() {
    }


    public I getView() {
        return iView;
    }

    public void setView(I iView) {
        this.iView = iView;
    }


    public boolean handleError(retrofit2.Response response) {
        if (response.code() == 203) {
            return handleError(((BaseResponse) response.body()), false);
        } else if (response.code() == 440) {
            getView().onTokenExpired();
            return true;
        } else if (response.errorBody() != null) {
            try {
                String error = response.errorBody().string();
                BaseResponse errorResponse = new Gson().fromJson(error, BaseResponse.class);
                return handleError(errorResponse, false);
            } catch (Exception e) {
                e.printStackTrace();
                getView().onError(null);
                return true;
            }
        } else {
            return handleError(((BaseResponse) response.body()), response.code() == 200);
        }
    }

    public boolean handleError(BaseResponse response, boolean success) {
        if (response != null && response.meta != null) {

            if (response.meta.getHasUpdate()) {
                if (response.meta.getForceUpdate())
                    getView().onForceUpdate();
                else
                    getView().onSoftUpdate();
                return true;
            }
        }
        if (success) {
            return false;
        } else {
            getView().onError(response != null ? response.replyMsg : null);
            return true;
        }

    }

    /**
     * update the notification read status
     * @param notificationId id of a notification
     */
    public void notificationRead(String notificationId) {
        SamriddhiApplication.getmInstance().getApiService().readNotifications( notificationId).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body() != null)
                            getView().onInfo(response.message());
                        else
                            getView().onInfo(response.body().replyMsg);
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onInfo(null);
            }
        });
    }


}
