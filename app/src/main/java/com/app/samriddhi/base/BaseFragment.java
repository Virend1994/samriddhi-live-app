package com.app.samriddhi.base;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationListener;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.app.samriddhi.R;
import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.response.BaseResponse;
import com.app.samriddhi.ui.activity.auth.LoginActivity;
import com.app.samriddhi.util.AlertUtility;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.StringUtility;
import com.bumptech.glide.Glide;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;

import retrofit2.Response;

public class BaseFragment extends Fragment {

    public static final int REQUEST_LOCATION = 1006;
    ProgressDialog progressDialog;

    public BaseFragment() {
        // will initialize later
    }

    public static <T extends BaseFragment> T getInstance(@Nullable Bundle args, Class<T> tClass) {
        try {
            T t = tClass.newInstance();
            t.setArguments(args);
            return t;
        } catch (java.lang.InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void replaceChildFragment(@IdRes int container, Fragment fragment) {
        getChildFragmentManager().beginTransaction().replace(container, fragment).commit();
    }

    public void replaceFragment(@IdRes final int container, final Fragment fragment) {
        final FrameLayout frameLayout = getActivity().findViewById(container);
        frameLayout.removeAllViewsInLayout();
        frameLayout.postDelayed(new Runnable() {
            @Override
            public void run() {
                getActivity().getSupportFragmentManager().beginTransaction().replace(frameLayout.getId(), fragment).addToBackStack(null).commitAllowingStateLoss();
                frameLayout.forceLayout();
            }
        }, 300);
    }

    public void setTitle(CharSequence title) {
        getActivity().setTitle(StringUtility.validateString(title.toString()) ? title : "");
    }

    public void setTitle(@StringRes int title) {
        getActivity().setTitle(title);
    }

    public void log(String message) {
        Log.d(getClass().getSimpleName(), message);
    }

    public SamriddhiApplication getApplicationClass() {
        return (SamriddhiApplication) getActivity().getApplication();
    }

    public SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(getActivity());
    }

    public boolean isCurrentUser(String id) {
        return getPreferences().getString(Constant.KEY_USER_ID, "").equals(id);
    }

    public void toast(final String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public void toast(@StringRes final int message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public ContentResolver getContentResolver() {
        return getContext().getContentResolver();
    }


    public void enableLoadingBar(boolean enable) {
        if (enable) {
                loadProgressBar(null, getString(R.string.loading), false);
        } else {
            dismissProgressBar();
        }
    }

    public void loadProgressBar(String title, String message, boolean cancellable) {
        if (progressDialog == null && !getActivity().isFinishing())
            progressDialog = ProgressDialog.show(getActivity(), null, null, false, cancellable);
        progressDialog.setContentView(R.layout.progress_bar);
        ImageView imgLoader = progressDialog.findViewById(R.id.img_loader);
        Glide.with(this).load(R.drawable.loader_latest).into(imgLoader);
       // Glide.with(this).load(R.drawable.loader_new).into(imgLoader);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

    }

    public void dismissProgressBar() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        progressDialog = null;
    }

    public AlertDialog.Builder getAlertDialogBuilder(String title, String message, boolean cancellable) {
        return new AlertDialog.Builder(getActivity(), R.style.AppTheme_AlertDialog)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(cancellable);
    }

    /*Pass null errorMessage to disable error view*/
    public void setFieldError(TextInputLayout tilField, String errorMessage) {
        if (tilField != null) {
            if (StringUtility.validateString(errorMessage)) {
                tilField.setError(errorMessage);
            } else {
                tilField.setErrorEnabled(false);
            }
        }
    }

    /*Pass null errorMessage to disable error view*/
    public void setFieldError(EditText edtField, String errorMessage) {
        if (edtField != null) {
            edtField.setError(errorMessage);
        }
    }

    public void onError(String reason) {
        if (getContext() == null) {
            return;
        }
        if (getUserVisibleHint()) {
            if (StringUtility.validateString(reason)) {
                AlertUtility.showFormAlert(getActivity(), getString(R.string.default_error), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
            } else {
                AlertUtility.showFormAlert(getActivity(), getString(R.string.default_error), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
            }
        }
    }

    public void onInfo(String message) {
        onInfo(message, false);
    }

    public void onInfo(String message, boolean finishOnOk) {

        AlertUtility.showFormAlert(getContext(), message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_LOCATION:
                if (resultCode == Activity.RESULT_CANCELED) {// The user was asked to change settings, but chose not to
                    getActivity().finish();
                }
                break;
            default:
                break;
        }
    }

    public boolean askListOfPermissions(String[] permissions, int requestCode) {
        boolean isOk = true;
        StringBuilder perNeed = new StringBuilder();
        for (String per : permissions) {
            if (!(ActivityCompat.checkSelfPermission(getActivity(), per) == PackageManager.PERMISSION_GRANTED)) {
                if (isOk)
                    isOk = false;
                perNeed.append(per);
                perNeed.append(",");
            }
        }

        if (isOk) {
            return true;
        }

        String reqPermissions = (perNeed.length() > 1 ? perNeed.substring(0, perNeed.length() - 1) : "");
        if (!reqPermissions.isEmpty()) {
            String[] arrPer = reqPermissions.split(",");
            ActivityCompat.requestPermissions(getActivity(), permissions, requestCode);
        }
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean isGranted = true;
        for (int grantResult : grantResults) {
            if (grantResult != PackageManager.PERMISSION_GRANTED) {
                isGranted = false;
                break;
            }
        }
        if (isGranted) {
            if (requestCode == REQUEST_LOCATION) {
                //initLocationListener(this.locationListener);
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    // ------------------------------ Current Location Code End --------------------------------//

    public void hideKeyBoard() {
        try {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getActivity().findViewById(android.R.id.content).getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected String getCurrentLanguage() {
        return getResources().getConfiguration().locale.getCountry();
    }

    public void handleError(BaseResponse response) {
        //todo handle error codes
        if (response != null) {
            try {
                onError(response.replyMsg);
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            onError(null);
        }
    }

    public void handleError(Response response) {
        //todo handle error codes
        if (response.code() == 203) {
            handleError(((BaseResponse) response.body()));
        } else if (response.code() == 440) {

        } else if (response.errorBody() != null) {
            try {
                String error = response.errorBody().string();
                BaseResponse errorResponse = new Gson().fromJson(error, BaseResponse.class);
                handleError(errorResponse);
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            onError(null);
        }
    }


    public void onError(String reason, final boolean finishOnOk) {
        if (!((getActivity())).isFinishing()) {
            if (StringUtility.validateString(reason)) {
                AlertUtility.showFormAlert(getActivity(), reason, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finish();
                    }
                });
            } else {
                AlertUtility.showFormAlert(getActivity(), getString(R.string.default_error), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        getActivity().finish();

                    }
                });
            }
        }
        enableLoadingBar(false);
    }

    public void onTokenExpired() {
        com.app.samriddhi.prefernces.PreferenceManager.clearAll(SamriddhiApplication.getmInstance());
        Intent intent = new Intent(SamriddhiApplication.getmInstance(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ActivityCompat.finishAffinity(getActivity());
        startActivity(intent);
        getActivity().finish();
    }


    public void onForceUpdate() {
    }

    public void onSoftUpdate() {
        // do something
    }
}


