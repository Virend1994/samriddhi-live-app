package com.app.samriddhi.base.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;

import java.util.ArrayList;

public class AppPendAdapter  extends RecyclerView.Adapter<AppPendAdapter.halfViewHolder> {

    private final ArrayList<String> srList;
    private final Context mcontext;



    public AppPendAdapter(Context context, ArrayList<String> srList) {
        this.mcontext=context;
        this.srList=srList;
    }

    @NonNull
    @Override
    public halfViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.app_pend_rec_list, parent, false);

        return new halfViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final halfViewHolder holder, final int position) {

        holder.open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if ( holder.linearLayout.getVisibility()==View.GONE)
                {
                    holder.linearLayout.setVisibility(View.VISIBLE);
                    holder.open.setImageResource(R.drawable.arrow_up);
                }
                else
                {
                    holder.linearLayout.setVisibility(View.GONE);
                    holder.open.setImageResource(R.drawable.arrow_down);
                }
            }
        });


        holder.attach.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });
    }

    @Override
    public int getItemCount() {
        return srList.size();
    }

    public class halfViewHolder extends RecyclerView.ViewHolder {

        LinearLayout linearLayout;
        ImageView open;
        TextView attach;
        public halfViewHolder(View view) {
            super(view);
            linearLayout=view.findViewById(R.id.btn_layout);
            open = view.findViewById(R.id.open);
            attach=view.findViewById(R.id.attach_pdf);
        }
    }

}