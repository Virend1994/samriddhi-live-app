package com.app.samriddhi.base.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;

import com.app.samriddhi.ui.model.CIR_Order_Update;
import com.app.samriddhi.ui.model.DropDownModel;
import com.app.samriddhi.ui.model.OrderSupplyModel;

import java.util.ArrayList;
import java.util.List;




public class OrderSupplyAdapter extends RecyclerView.Adapter<OrderSupplyAdapter.halfViewHolder> implements  DropdownMenuAdapter.OnMeneuClickListnser, Filterable {

    private ArrayList<OrderSupplyModel> srList;
    private final Context mcontext;

    public ArrayList<CIR_Order_Update> cirListData;
    private String type="";
    private final  OnClickEditListener onClickEditListener;
     ArrayList<CIR_Order_Update> CirListDataList;

    private final List<OrderSupplyModel> searchArray;

    public OrderSupplyAdapter(Context context, ArrayList<OrderSupplyModel> srList , OnClickEditListener onClickEditListener, ArrayList<CIR_Order_Update> cirListData, String type) {
        this.mcontext=context;
        this.srList=srList;
        this.onClickEditListener=onClickEditListener;
        this.searchArray=new ArrayList<>();
        this.type=type;
        this.searchArray.addAll(srList);
        this.cirListData=cirListData;

    }
    public interface OnClickEditListener{
        void onEdit(int CIRLength,OrderSupplyModel data,ArrayList<OrderSupplyModel> srtList,int pos);

    }

    @NonNull
    @Override
    public halfViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_supply_layout, parent, false);

        return new halfViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final halfViewHolder holder, final int position) {



        try {
            OrderSupplyModel data=srList.get(position);
            CIR_Order_Update listData=cirListData.get(position);
            holder.tvPva.setText(data.getPva());
            holder.tvCustomerName.setText(data.getCustName());
            holder.tvSoldToParty.setText(data.getSoldToParty());
            holder.tvGrossParty.setText(data.getProposedQty().toString());
            holder.tvShipToParty.setText(data.getShipToParty());

            holder.edtView.setOnClickListener(v->{
                onClickEditListener.onEdit(data.getCIROrderUpdate().length(),srList.get(position),srList,position);
            });



            if(data.getCIROrderUpdate().length()>0 ){

                holder.tvPva.setTextColor(Color.WHITE);
                holder.tvCustomerName.setTextColor(Color.WHITE);
                holder.tvSoldToParty.setTextColor(Color.WHITE);
                holder.tvGrossParty.setTextColor(Color.WHITE);
                holder.tvShipToParty.setTextColor(Color.WHITE);

                holder.tvPvaH.setTextColor(Color.WHITE);

                holder.tvSoldToPartyH.setTextColor(Color.WHITE);
                holder.tvGrossPartyH.setTextColor(Color.YELLOW);
                holder.tvShipToPartyH.setTextColor(Color.WHITE);





                holder.layoutBg.setBackgroundTintList(ContextCompat.getColorStateList(mcontext, R.color.gray));

            }else if(listData.getRemark().length()>0){
                holder.tvPva.setTextColor(Color.WHITE);
                holder.tvCustomerName.setTextColor(Color.WHITE);
                holder.tvSoldToParty.setTextColor(Color.WHITE);
                holder.tvGrossParty.setTextColor(Color.WHITE);
                holder.tvShipToParty.setTextColor(Color.WHITE);

                holder.tvPvaH.setTextColor(Color.WHITE);

                holder.tvSoldToPartyH.setTextColor(Color.WHITE);
                holder.tvGrossPartyH.setTextColor(Color.YELLOW);
                holder.tvShipToPartyH.setTextColor(Color.WHITE);





                holder.layoutBg.setBackgroundTintList(ContextCompat.getColorStateList(mcontext, R.color.light_gray));

            } else{
                holder.tvPva.setTextColor(Color.BLACK);
                holder.tvCustomerName.setTextColor(Color.BLACK);
                holder.tvSoldToParty.setTextColor(Color.BLACK);
                holder.tvGrossParty.setTextColor(Color.RED);
                holder.tvShipToParty.setTextColor(Color.BLACK);
                holder.layoutBg.setBackgroundResource(R.drawable.dw_card);
            }





            if(type.equalsIgnoreCase("Uoh") && data.getCIROrderUpdate().length()>0 ){
                holder.edtView.setVisibility(View.VISIBLE);
            }else{
                holder.edtView.setVisibility(View.GONE);
            }

            if(listData.getApproval_status()==1){
                holder.edtView.setVisibility(View.VISIBLE);
                holder.appImg.setVisibility(View.VISIBLE);
                holder.appImg.setBackgroundResource(R.drawable.approved);
            }else if(listData.getApproval_status()==2) {
                holder.edtView.setVisibility(View.VISIBLE);
                holder.appImg.setVisibility(View.VISIBLE);
                holder.appImg.setBackgroundResource(R.drawable.rejected);
            }else if(listData.getApproval_status()==0 && type.equalsIgnoreCase("Uoh") && data.getCIROrderUpdate().length()==0 ) {
                holder.edtView.setVisibility(View.GONE);
                holder.appImg.setVisibility(View.GONE);

            }else if(listData.getApproval_status()==0) {
                holder.edtView.setVisibility(View.VISIBLE);
                holder.appImg.setVisibility(View.GONE);

            }else if(listData.getApproval_status()==4 && type.equalsIgnoreCase("Ex")) {
                holder.edtView.setVisibility(View.VISIBLE);
                holder.appImg.setVisibility(View.GONE);

            }
        }catch (Exception e){
            Toast.makeText(mcontext,"Error"+e.getMessage(),Toast.LENGTH_LONG).show();
        }


    }

    private void removeItem(int position){
        srList.remove(position);
        //notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return srList.size();
    }

    @Override
    public void onOptionClick(DropDownModel liveTest) {

    }

    public class halfViewHolder extends RecyclerView.ViewHolder {

        TextView tvPva,tvCustomerName,tvSoldToParty,tvGrossParty,tvShipToParty;
        TextView tvItemCat,tvPvaH,tvmainjj,tvCustomerNameH,tvSoldToPartyH,tvGrossPartyH,tvShipToPartyH;

        LinearLayout edtView,layoutBg;
        ImageView appImg;

        public halfViewHolder(View view) {
            super(view);
            tvPva=view.findViewById(R.id.TvPva);
            tvItemCat=view.findViewById(R.id.tvItemCat);
            tvmainjj=view.findViewById(R.id.tvmainjj);

            edtView=view.findViewById(R.id.edtView);
            tvCustomerName=view.findViewById(R.id.tvCustomerName);
            tvShipToParty=view.findViewById(R.id.tvShipToParty);
            layoutBg=view.findViewById(R.id.layoutBg);
            tvSoldToParty=view.findViewById(R.id.tvSoldToParty);
            tvGrossParty=view.findViewById(R.id.tvGrossParty);
            appImg=view.findViewById(R.id.appImg);


            tvPvaH=view.findViewById(R.id.TvPvaH);

            tvShipToPartyH=view.findViewById(R.id.tvShipToPartyH);

            tvSoldToPartyH=view.findViewById(R.id.tvSoldToPartyH);
            tvGrossPartyH=view.findViewById(R.id.tvGrossPartyH);



        }
    }



    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String filterString = charSequence.toString().toLowerCase();
                //arraylist1.clear();
                FilterResults results = new FilterResults();
                int count = searchArray.size();
                final List<OrderSupplyModel> list = new ArrayList<>();
              //  CirListDataList = new ArrayList<>();

                OrderSupplyModel filterableString;
                //CIR_Order_Update filterableCirList;
                for (int i = 0; i < count; i++) {
                    filterableString = searchArray.get(i);
                  //  filterableCirList=searchCirListData.get(i);
                    if (searchArray.get(i).getCustName().toLowerCase().contains(filterString) || searchArray.get(i).getSoldToParty().toLowerCase().contains(filterString) || searchArray.get(i).getPva().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                      //  CirListDataList.add(filterableCirList);
                    }
                }

                results.values = list;
                results.count = list.size();

                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                srList = (ArrayList<OrderSupplyModel>) filterResults.values;
                 //cirListData = CirListDataList;
            notifyDataSetChanged();


            }
        };


    }

}