package com.app.samriddhi.base.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;

import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.model.NewAgencyModel;
import com.app.samriddhi.util.Globals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ShowRequestNewAgencyListAdapter extends RecyclerView.Adapter<ShowRequestNewAgencyListAdapter.halfViewHolder> implements Filterable {

    ArrayList<NewAgencyModel> allagencyList;
    ArrayList<NewAgencyModel> searchArray;
    private Context mcontext;
    private final Globals g = Globals.getInstance(mcontext);
    HashMap map;

    private final OnItemClickListnser onItemClickListnser;

    public ShowRequestNewAgencyListAdapter(Context context, ArrayList<NewAgencyModel> srList, OnItemClickListnser onItemClickListnser) {
        this.mcontext = context;
        this.allagencyList = srList;
        this.searchArray = new ArrayList<>();
        this.searchArray.addAll(srList);
        this.onItemClickListnser = onItemClickListnser;
    }

    @NonNull
    @Override
    public halfViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.agency_view_adapter_layout, parent, false);
        return new halfViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(@NonNull final halfViewHolder holder, final int position) {

        NewAgencyModel data = allagencyList.get(position);
        holder.TvRequestOne.setText("R. ID : " + data.getId());

        //holder.tvLocation.setText(data.getLocation());
        holder.tvLocation.setText(data.getTown());
        holder.tvUnit.setText(data.getUnit());

        holder.edtView.setOnClickListener(v -> {
            onItemClickListnser.onOptionClick(data);
        });

      /*  holder.linear_main.setOnClickListener(v -> {

            Log.e("sadad", data.getId());
            Log.e("sadad", PreferenceManager.getPrefUserType(mcontext));

            if (PreferenceManager.getPrefUserType(mcontext).equalsIgnoreCase("CO") || PreferenceManager.getPrefUserType(mcontext).equalsIgnoreCase("HO")) {
                onItemClickListnser.onApproveOnClick(data, "HO");
            }

            else if (PreferenceManager.getPrefUserType(mcontext).equalsIgnoreCase("SH")) {
                onItemClickListnser.onApproveOnClick(data, "SOH");
            } else if (PreferenceManager.getPrefUserType(mcontext).equalsIgnoreCase("UH")) {
                onItemClickListnser.onApproveOnClick(data, "UOH");
            }
            Log.e("sdfsfsfs", PreferenceManager.getPrefUserType(mcontext));

        });*/

        holder.linearApproval.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("wrewerwerwer", PreferenceManager.getPrefUserType(mcontext));
                // onItemClickListnser.onApproveOnClick(data, "SOH");
                onItemClickListnser.onApproveOnClick(data, PreferenceManager.getPrefUserType(mcontext));
            }
        });

        /*if (PreferenceManager.getPrefUserType(mcontext).equalsIgnoreCase("CO") || PreferenceManager.getPrefUserType(mcontext).equalsIgnoreCase("HO")) {
            if (data.getHOApproval().equals("1")) {
                holder.edtView.setVisibility(View.GONE);
                holder.linPending.setVisibility(View.GONE);
                holder.linApproved.setVisibility(View.VISIBLE);
            } else {
                holder.edtView.setVisibility(View.GONE);
                holder.linPending.setVisibility(View.VISIBLE);
                holder.linApproved.setVisibility(View.GONE);

            }
        }

        if (PreferenceManager.getPrefUserType(mcontext).equalsIgnoreCase("UH")) {
            if (data.getUOHApproval().equals("1")) {
                holder.edtView.setVisibility(View.GONE);
                holder.linPending.setVisibility(View.GONE);
                holder.linApproved.setVisibility(View.VISIBLE);
            } else {
                holder.edtView.setVisibility(View.GONE);
                holder.linPending.setVisibility(View.VISIBLE);
                holder.linApproved.setVisibility(View.GONE);

            }
        }


        if (PreferenceManager.getPrefUserType(mcontext).equalsIgnoreCase("SH")) {
            if (data.getSOHApproval().equals("1")) {
                holder.edtView.setVisibility(View.GONE);
                holder.linPending.setVisibility(View.GONE);
                holder.linApproved.setVisibility(View.VISIBLE);
            } else {
                holder.edtView.setVisibility(View.GONE);
                holder.linPending.setVisibility(View.VISIBLE);
                holder.linApproved.setVisibility(View.GONE);

            }
        }*/

       /* holder.approveView.setOnClickListener(v -> {
            onItemClickListnser.onApproveOnClick(data, PreferenceManager.getParentUserType(mcontext));
        });
        holder.approveViewHO.setOnClickListener(v -> {
            onItemClickListnser.onApproveOnClick(data, "HO");
        });
        holder.approveViewSoH.setOnClickListener(v -> {
            onItemClickListnser.onApproveOnClick(data, "SOH");
        });
*/
    }

    public interface OnItemClickListnser {
        void onOptionClick(NewAgencyModel liveTest);

        void onApproveOnClick(NewAgencyModel liveTest, String accessType);
    }


    @Override
    public int getItemCount() {
        return allagencyList.size();
    }

    public class halfViewHolder extends RecyclerView.ViewHolder {

        TextView TvRequestOne, tvLocation, tvUnit;
        ImageView edit_btn;

        LinearLayout edtView, approveView, approveViewSoH, approveViewHO, linear_main, linearApproval, linApproved, linPending;


        public halfViewHolder(View view) {
            super(view);

            linearApproval = view.findViewById(R.id.linearApproval);
            approveView = view.findViewById(R.id.approveView);
            approveViewSoH = view.findViewById(R.id.approveViewSoH);
            approveViewHO = view.findViewById(R.id.approveViewHO);
            edtView = view.findViewById(R.id.edtView);
            linear_main = view.findViewById(R.id.linear_main);
            tvLocation = view.findViewById(R.id.tvLocation);
            tvUnit = view.findViewById(R.id.tvUnit);
            TvRequestOne = view.findViewById(R.id.TvRequestOne);
            linApproved = view.findViewById(R.id.linApproved);
            linPending = view.findViewById(R.id.linPending);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String filterString = charSequence.toString().toLowerCase();
                //arraylist1.clear();
                FilterResults results = new FilterResults();
                int count = searchArray.size();
                final List<NewAgencyModel> list = new ArrayList<>();

                NewAgencyModel filterableString;

                for (int i = 0; i < count; i++) {
                    filterableString = searchArray.get(i);
                    if (searchArray.get(i).getUnit().toLowerCase().contains(filterString) || searchArray.get(i).getLocation().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                    }
                }

                results.values = list;
                results.count = list.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                allagencyList = (ArrayList<NewAgencyModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}