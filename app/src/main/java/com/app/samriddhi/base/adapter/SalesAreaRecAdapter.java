package com.app.samriddhi.base.adapter;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;

import java.util.ArrayList;

public class SalesAreaRecAdapter  extends RecyclerView.Adapter<SalesAreaRecAdapter.halfViewHolder> {

    private final ArrayList<String> srList;
    private final Context mcontext;



    public SalesAreaRecAdapter(Context context, ArrayList<String> srList) {
        this.mcontext=context;
        this.srList=srList;
    }

    @NonNull
    @Override
    public halfViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sales_area_rec_list, parent, false);

        return new halfViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final halfViewHolder holder, final int position) {

        holder.sales_quantity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                popupWindow(v);
            }
        });
    }

    public void popupWindow(View view) {
        LayoutInflater inflater = (LayoutInflater) view.getContext().getSystemService(view.getContext().LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_inc_dec, null);
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);


        ImageView img=popupView.findViewById(R.id.close_inc);
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                popupWindow.dismiss();
            }
        });



    }


    @Override
    public int getItemCount() {
        return srList.size();
    }

    public class halfViewHolder extends RecyclerView.ViewHolder {

        TextView sales_quantity;

        public halfViewHolder(View view) {
            super(view);

            sales_quantity=view.findViewById(R.id.sales_qty);

        }
    }



}