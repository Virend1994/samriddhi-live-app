package com.app.samriddhi.base.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;

import com.app.samriddhi.ui.model.VillageModel;
import com.app.samriddhi.util.Globals;

import java.util.ArrayList;

public class Village_Adapter extends RecyclerView.Adapter<Village_Adapter.halfViewHolder> {

    private final ArrayList<VillageModel> srList;
    private Context mcontext;
    private final Globals g = Globals.getInstance(mcontext);

    private final OnSetValueVillageListner onSetValueVillageListner;

    public Village_Adapter(Context context, ArrayList<VillageModel> srList, OnSetValueVillageListner onSetValueListner) {
        this.mcontext=context;
        this.srList=srList;
        this.onSetValueVillageListner=onSetValueListner;
    }



    @NonNull
    @Override
    public halfViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.village_rec_list, parent, false);

        return new halfViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final halfViewHolder holder, final int position) {


        holder.villagecopy.setText(String.valueOf(g.village_rec_datalist.get(position).getVillage_copy()));
        holder.villagename.setText(g.village_rec_datalist.get(position).getVillage_name());

        holder.deleteItem.setOnClickListener(v->{
            if(srList.size()>1){
                removeItem(position);
                onSetValueVillageListner.setVillageCopyTotal(g.village_rec_datalist);
            }
        });

        if(srList.size()==1){
            holder.deleteItem.setVisibility(View.GONE);
        }
    }

    private void removeItem(int position){
        srList.remove(position);
        //   notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return srList.size();
    }
    public interface OnSetValueVillageListner{
        void setVillageCopyTotal(ArrayList<VillageModel> srList);
    }
    public class halfViewHolder extends RecyclerView.ViewHolder {
        public EditText villagecopy,villagename;
        private final AppCompatImageView deleteItem;

        public halfViewHolder(View view) {
            super(view);
            villagecopy=view.findViewById(R.id.village_copy);
            villagename=view.findViewById(R.id.village_name);
            deleteItem=view.findViewById(R.id.deleteItem);

             villagecopy.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    if(s.length()>0)  {
                        g.village_rec_datalist.get(getAdapterPosition()).setVillage_copy(Integer.parseInt(villagecopy.getText().toString()));
                     onSetValueVillageListner.setVillageCopyTotal(g.village_rec_datalist);

                    }else{
                        g.village_rec_datalist.get(getAdapterPosition()).setVillage_copy(0);

                    }
                }
            });

             villagename.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(s.length()>0) {
                        g.village_rec_datalist.get(getAdapterPosition()).setVillage_name(villagename.getText().toString());

                    }
                    }
            });
        }
    }



}