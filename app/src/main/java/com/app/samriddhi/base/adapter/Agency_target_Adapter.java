package com.app.samriddhi.base.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;

import com.app.samriddhi.ui.model.TargetModelClass;
import com.app.samriddhi.util.Globals;

import java.util.ArrayList;

public class Agency_target_Adapter  extends RecyclerView.Adapter<Agency_target_Adapter.halfViewHolder> {

    private final ArrayList<TargetModelClass> srList;
    private Context mcontext;
    private final Globals g = Globals.getInstance(mcontext);

    private final OnSetValueListner onSetValueListner;



    public Agency_target_Adapter(Context context, ArrayList<TargetModelClass> srList ,OnSetValueListner onSetValueListner) {
        this.mcontext=context;
        this.srList=srList;
        this.onSetValueListner=onSetValueListner;
    }

    @NonNull
    @Override
    public Agency_target_Adapter.halfViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.agency_target_rec_list, parent, false);

        return new Agency_target_Adapter.halfViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final Agency_target_Adapter.halfViewHolder holder, final int position) {


        Log.e("dsgsdg","asfafasfas"+" "+g.survet_form_rec_datalist.get(position).getAgency_name());
        holder.agency_name.setText(g.survet_form_rec_datalist.get(position).getAgency_name());
        holder.agency_code.setText(g.survet_form_rec_datalist.get(position).getAgecy_code());
        holder.current_copy.setText(String.valueOf(g.survet_form_rec_datalist.get(position).getCur_copies()));


        if(g.reasonTypeName.equalsIgnoreCase("Intensification")){

            holder.m1.setEnabled(true);
            holder.m2.setEnabled(true);
            holder.m3.setEnabled(true);

           /* if(position==0){   *//*Ye pehle krwaya tha *//*

                holder.m1.setEnabled(false);
                holder.m2.setEnabled(false);
                holder.m3.setEnabled(false);
            }
            else {

                holder.m1.setEnabled(true);
                holder.m2.setEnabled(true);
                holder.m3.setEnabled(true);
            }
*/


            holder.current_copy.setEnabled(false);

            holder.m1.setText(String.valueOf(g.survet_form_rec_datalist.get(position).getMonth_one()));
            holder.m2.setText(String.valueOf(g.survet_form_rec_datalist.get(position).getMonth_two()));
            holder.m3.setText(String.valueOf(g.survet_form_rec_datalist.get(position).getMonth_three()));


        }else{
            if(g.survet_form_rec_datalist.get(position).getAgecy_code().equalsIgnoreCase("") || g.survet_form_rec_datalist.get(position).getAgecy_code().equalsIgnoreCase("null") || g.survet_form_rec_datalist.get(position).getAgecy_code().equalsIgnoreCase(null)){
                holder.current_copy.setEnabled(false);
                holder.m1.setText(String.valueOf(g.survet_form_rec_datalist.get(position).getMonth_one()));
                holder.m2.setText(String.valueOf(g.survet_form_rec_datalist.get(position).getMonth_two()));
                holder.m3.setText(String.valueOf(g.survet_form_rec_datalist.get(position).getMonth_three()));

            }else{
                holder.current_copy.setEnabled(false);
                holder.m1.setEnabled(false);
                holder.m2.setEnabled(false);
                holder.m3.setEnabled(false);

                holder.m1.setText(String.valueOf(g.survet_form_rec_datalist.get(position).getMonth_one()));
                holder.m2.setText(String.valueOf(g.survet_form_rec_datalist.get(position).getMonth_two()));
                holder.m3.setText(String.valueOf(g.survet_form_rec_datalist.get(position).getMonth_three()));

            }
        }


        holder.current_copy.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {


                if(s.length()>0) {
                    g.survet_form_rec_datalist.get(position).setCur_copies(Integer.parseInt(holder.current_copy.getText().toString()));
                    onSetValueListner.setTotalValue(g.survet_form_rec_datalist);
                }else{
                    g.survet_form_rec_datalist.get(position).setCur_copies(0);

                }


            }
        });
//
//
//
        holder.m1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>0) {
                    g.survet_form_rec_datalist.get(position).setMonth_one(Integer.parseInt(holder.m1.getText().toString()));
                    onSetValueListner.setTotalValue(g.survet_form_rec_datalist);
                }else{
                    g.survet_form_rec_datalist.get(position).setMonth_one(0);

                }

            }
        });
//
//
//
//
        holder.m2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>0) {
                    g.survet_form_rec_datalist.get(position).setMonth_two(Integer.parseInt(holder.m2.getText().toString()));

                    onSetValueListner.setTotalValue(g.survet_form_rec_datalist);
                }else{
                    g.survet_form_rec_datalist.get(position).setMonth_two(0);

                }
            }
        });
//
//
//
        holder.m3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(s.length()>0){
                    g.survet_form_rec_datalist.get(position).setMonth_three(Integer.parseInt(holder.m3.getText().toString()));

                    onSetValueListner.setTotalValue(g.survet_form_rec_datalist);
                }else{
                    g.survet_form_rec_datalist.get(position).setMonth_three(0);

                }

            }
        });

    }



    public  ArrayList<TargetModelClass> getSrList(){
        return srList;
    }
    public interface OnSetValueListner{
        void setTotalValue(ArrayList<TargetModelClass> srList);
    }

    @Override
    public int getItemCount() {
        return srList.size();
    }

    public class halfViewHolder extends RecyclerView.ViewHolder {
        public EditText m1,m2,m3,current_copy;


        TextView agency_name,agency_code;


        public halfViewHolder(View view) {
            super(view);

            agency_name=view.findViewById(R.id.ag_name_sf);
            agency_code=view.findViewById(R.id.ag_code_sf);
            current_copy=view.findViewById(R.id.cc_sf);
            m1=view.findViewById(R.id.month1_exit);
            m2=view.findViewById(R.id.month2_exit);
            m3=view.findViewById(R.id.month3_exit);

        }
    }






}