package com.app.samriddhi.base.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;

import java.util.ArrayList;

public class Agency_Copy_Shifted_resignation extends RecyclerView.Adapter<Agency_Copy_Shifted_resignation.halfViewHolder> {

    private final ArrayList<String> srList;
    private final Context mcontext;



    public Agency_Copy_Shifted_resignation(Context context, ArrayList<String> srList) {
        this.mcontext=context;
        this.srList=srList;
    }

    @NonNull
    @Override
    public halfViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.agency_copy_shifted_resignation_rec_list, parent, false);

        return new halfViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final halfViewHolder holder, final int position) {

    }



    @Override
    public int getItemCount() {
        return srList.size();
    }

    public class halfViewHolder extends RecyclerView.ViewHolder {
        public TextView survey,kybp,cir_rpt,otp;
        public ImageView open;
        public LinearLayout linearLayout;


        public halfViewHolder(View view) {
            super(view);


        }
    }



}