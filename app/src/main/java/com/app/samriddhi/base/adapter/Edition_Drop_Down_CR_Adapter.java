package com.app.samriddhi.base.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.prefernces.PreferenceManager;

import com.app.samriddhi.ui.model.DropDownModel;
import com.app.samriddhi.ui.model.EditionCRModel;
import com.app.samriddhi.util.Globals;
import com.app.samriddhi.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Edition_Drop_Down_CR_Adapter extends RecyclerView.Adapter<Edition_Drop_Down_CR_Adapter.halfViewHolder> implements  DropdownMenuAdapter.OnMeneuClickListnser{

    private final ArrayList<EditionCRModel> srList;
    private Context mcontext;
    public ArrayList<DropDownModel> EditionMasterArray;
    Globals g=Globals.getInstance(mcontext);
    public  final OnDropMenu onDropMenu;
    int dropPosition,getViewPosition;

    public Edition_Drop_Down_CR_Adapter(Context context, ArrayList<EditionCRModel> srList, OnDropMenu onDropMenu, int pos) {
        this.mcontext=context;

        this.srList=g.droppoint_rec_datalist.get(pos).getCirReqDropping_set();
        this.onDropMenu=onDropMenu;
        this.dropPosition=pos;
        EditionMasterArray=new ArrayList<>();
    }
    public interface OnDropMenu{
        void onDelete(int dropPosArray,int pos);
    }
    @NonNull
    @Override
    public halfViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.edition_cr_rec_list, parent, false);

        return new halfViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final halfViewHolder holder, final int position) {

        EditionCRModel data=srList.get(position);
        holder.edition.setText(data.getName());
        holder.noofcopy.setText(data.getCopy());

holder.deleteItem.setOnClickListener(v->{
    onDropMenu.onDelete(dropPosition,position);
});

if(srList.size()==1){
    holder.deleteItem.setVisibility(View.GONE);
}


    }






    @Override
    public int getItemCount() {
        return srList.size();
    }

    @Override
    public void onOptionClick(DropDownModel liveTest) {

        Util.hideDropDown();
         srList.get(getViewPosition).setPk(liveTest.getId());
        srList.get(getViewPosition).setName(liveTest.getDescription());

        notifyDataSetChanged();
    }

    public class halfViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView deleteItem;
        TextView edition,noofcopy;


        public halfViewHolder(View view) {
            super(view);
            deleteItem=view.findViewById(R.id.deleteItem);
            edition=view.findViewById(R.id.edition_spin);
            noofcopy=view.findViewById(R.id.edition_copy);


            noofcopy.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(s.length()>0){
                        srList.get(getAdapterPosition()).setCopy(noofcopy.getText().toString());
                    }

                }
            });




            getEdition();



        }


    }

    private void getEdition() {
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getEdition(PreferenceManager.getAgentId(mcontext));
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData=jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if(status.equalsIgnoreCase("Success")){

                            EditionMasterArray.clear();
                            for(int i=0;i<objData.length();i++){


                                JSONObject objStr=objData.getJSONObject(i);

                                DropDownModel dropDownModel=new DropDownModel();

                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("edition_name"));
                                EditionMasterArray.add(dropDownModel);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                else {
                    try{
                        Toast.makeText(mcontext, "error message"+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    }catch(IOException e){
                        Toast.makeText(mcontext, "error message"+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Toast.makeText(mcontext, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }


}