package com.app.samriddhi.base.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.app.samriddhi.R;
import com.app.samriddhi.util.Globals;


import java.util.ArrayList;
import java.util.HashMap;


public class ShowNewAgencyAdapter extends RecyclerView.Adapter<ShowNewAgencyAdapter.halfViewHolder> {

    ArrayList<HashMap> allagencyList = new ArrayList<HashMap>();
    private Context mcontext;
    private final Globals g = Globals.getInstance(mcontext);

    HashMap map;



    public ShowNewAgencyAdapter(Context context, ArrayList<HashMap> srList) {
        this.mcontext=context;
        this.allagencyList=srList;

    }

    @NonNull
    @Override
    public halfViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.show_new_agency_rec_list, parent, false);

        return new halfViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final halfViewHolder holder, final int position) {

        map = allagencyList.get(position);

        holder.agname.setText("Agency  "+ (position + 1));
        holder.unique_id.setText(map.get("Form_id").toString());

        holder.edit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {





            }
        });

    }



    @Override
    public int getItemCount() {
        return allagencyList.size();
    }

    public class halfViewHolder extends RecyclerView.ViewHolder {

        TextView agname,unique_id;
        ImageView edit_btn;

        public halfViewHolder(View view) {
            super(view);

            agname=view.findViewById(R.id.agency_name);
            unique_id=view.findViewById(R.id.unique_id);
            edit_btn=view.findViewById(R.id.edit);

        }
    }



}