package com.app.samriddhi.base.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.BR;
import com.app.samriddhi.R;
import com.app.samriddhi.ui.model.AgentListModel;

import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

public class AgencyDetailsShowAdapter extends RecyclerView.Adapter<AgencyDetailsShowAdapter.MyViewHolder> implements Filterable {

    private List<AgentListModel> dataList;
    private final OnRouteListClickListener onRouteListClickListener;
    static int  getPosition;
    private String screenName = "";
    private final List<AgentListModel> searchArray;
    public AgencyDetailsShowAdapter(List<AgentListModel> vehicleMasterList, OnRouteListClickListener onRouteListClickListener,String screenName) {
        this.dataList = vehicleMasterList;
        this.onRouteListClickListener = onRouteListClickListener;
        this.screenName = screenName;
        this.searchArray = new ArrayList<>();
        this.searchArray.addAll(vehicleMasterList);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), viewType, parent, false);
        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.getBinding().setVariable(BR.item, dataList.get(position));
        holder.getBinding().setVariable(BR.screenType, screenName);
        holder.getBinding().setVariable(BR.itemClickListener, onRouteListClickListener);
        holder.getBinding().setVariable(BR.index, position);
//      VehicleDriverListModel listData=vehicleMasterList.get(position);
//      holder.tvDriverName.setText(listData.getDriver_name());
//      holder.tvDriverNumber.setText(listData.getDriver_number());
//      holder.tvVehicleNumber.setText(listData.getVehicle_number());
//      holder.plantName.setText(listData.getPlantName());
//
//      holder.tvRouteCode.setText(listData.getVehicle_number());
      holder.itemView.setOnClickListener(v->{
            onRouteListClickListener.onItemClick(v,dataList.get(position));
         });

         holder.itemView.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 onRouteListClickListener.onItemClick(view,dataList.get(position));
             }
         });


      }
    void add(AgentListModel RouteVehicleMappingListModelClass) {
        this.dataList.add(RouteVehicleMappingListModelClass);
    }
   public void addAll(List<AgentListModel> RouteVehicleMappingListModelClasses) {
        this.dataList.addAll(RouteVehicleMappingListModelClasses);
    }

    @Override
    public int getItemViewType(int position) {
        return R.layout.item_agencies;
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        ViewDataBinding binding;

        public MyViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.executePendingBindings();
        }

        public ViewDataBinding getBinding() {
            return binding;
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String filterString = charSequence.toString().toLowerCase();
                //arraylist1.clear();
                FilterResults results = new FilterResults();
                int count = searchArray.size();
                final List<AgentListModel> list = new ArrayList<>();

                AgentListModel filterableString;

                for (int i = 0; i < count; i++) {
                    filterableString = searchArray.get(i);
                    if (searchArray.get(i).getSoldToPartyName().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                    }


                }

                results.values = list;
                results.count = list.size();

                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                dataList = (List<AgentListModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };


    }
    public interface OnRouteListClickListener{
        void onItemClick(View view,Object object);
    }
}

