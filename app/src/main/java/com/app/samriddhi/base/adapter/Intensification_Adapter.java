package com.app.samriddhi.base.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.prefernces.PreferenceManager;

import com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule.newRquestActivity;
import com.app.samriddhi.ui.model.AgencyRequestReasonSetModel;
import com.app.samriddhi.util.Globals;
import com.bumptech.glide.Glide;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Intensification_Adapter extends RecyclerView.Adapter<Intensification_Adapter.halfViewHolder> {

    private final ArrayList<String> srList;
    private Context mcontext;
    private final Globals g = Globals.getInstance(mcontext);

    private static AlertDialog alertDialog;
    ProgressDialog progressDialog;

    public Intensification_Adapter(Context context, ArrayList<String> srList) {
        this.mcontext = context;
        this.srList = srList;
    }

    @NonNull
    @Override
    public halfViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.intensification_rec_list, parent, false);

        return new halfViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final halfViewHolder holder, final int position) {

        holder.reason.setText(String.valueOf(g.intensification_rec_list.get(position).get("inten_reason")));

        holder.agcode.setText(String.valueOf(g.intensification_rec_list.get(position).get("inten_agcode")));
        holder.copy.setText(String.valueOf(g.intensification_rec_list.get(position).get("inten_copy")));


        holder.agcode.setOnClickListener(v -> {
            AlertDialog.Builder builder2 = new AlertDialog.Builder(mcontext);
            builder2.setCancelable(false);
            View view2 = LayoutInflater.from(mcontext).inflate(R.layout.enter_agency_code, null);
            Button btnSubmit = view2.findViewById(R.id.getAgencyBtn);
            ImageView closeImg = view2.findViewById(R.id.closeImg);
            EditText edtAgency = view2.findViewById(R.id.replaced_agency_code);
            btnSubmit.setOnClickListener(v1 -> {
                if (g.uniteCode.equalsIgnoreCase("")) {
                    alertMessage("Please select state and unit");
                    return;
                }
                for (int k = 0; k < g.intensification_rec_list.size(); k++) {
                    if (g.intensification_rec_list.get(k).get("inten_agcode").toString().equalsIgnoreCase(edtAgency.getText().toString())) {
                        alertMessage("Please Enter different Agency Code");
                        return;
                    }

                }

                getAgencyDetails(edtAgency.getText().toString(), position);

            });
            builder2.setView(view2);
            closeImg.setOnClickListener(v1 -> {
                alertDialog.cancel();
                alertDialog.dismiss();
            });

            alertDialog = builder2.create();
            alertDialog.show();
        });


        holder.deleteItem.setOnClickListener(v -> {

            if (srList.size() > 1) {
                removeItem(position);
            }

        });

        if (srList.size() == 1) {
            holder.deleteItem.setVisibility(View.GONE);
        }


        holder.reason.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                g.intensification_rec_list.get(position).put("inten_reason", holder.reason.getText().toString());
            }
        });

    }


    private void removeItem(int position) {
        srList.remove(position);
        //   notifyItemRemoved(position);
        notifyDataSetChanged();
        if (mcontext instanceof newRquestActivity) {
            ((newRquestActivity) mcontext).RemoveIntensification(position);
        }
    }


    private void alertMessage(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(mcontext)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })

                .show();
    }

    @Override
    public int getItemCount() {
        return srList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }


    public class halfViewHolder extends RecyclerView.ViewHolder {
        public TextView agcode, copy;
        public EditText reason;
        public AppCompatImageView deleteItem;

        public halfViewHolder(View view) {
            super(view);

            agcode = view.findViewById(R.id.inten_agcode);
            copy = view.findViewById(R.id.inten_copy);
            reason = view.findViewById(R.id.inten_reason);
            deleteItem = view.findViewById(R.id.deleteItem);


        }
    }


    public void getAgencyDetails(String agencyCode, int pos) {

        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getAgencyDetails(PreferenceManager.getAgentId(mcontext), "00" + agencyCode, g.uniteCode);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;
                    Log.e("newsdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());


                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {
                            JSONObject objData = jsonObject.getJSONObject("data");
                            AgencyRequestReasonSetModel model1 = new AgencyRequestReasonSetModel();

                            model1.setAg_code(agencyCode);
                            model1.setAg_copies(Integer.parseInt(objData.getString("AVGCOPY7Days")));
                            model1.setAg_reason("");
                            model1.setReason("");
                            model1.setAg_name(objData.getString("AGNAME"));
                            g.intensification_rec_list.get(pos).put("inten_agcode", agencyCode);

                            g.intensification_rec_list.get(pos).put("inten_copy", objData.getString("AVGCOPY7Days"));

                            g.intensification_rec_list.get(pos).put("inten_agency_name", objData.getString("AGNAME"));


                            notifyDataSetChanged();
//                            demoBinding.replacedAgencyCode.setText(objData.getString("AGNAME"));

                            //                    demoBinding.replacedAgencyCode.setText(objData.getString("AVGCOPY7Days"));

                            alertDialog.dismiss();
                            alertDialog.cancel();
                        }

                        if (status.equalsIgnoreCase("Failed")) {
                            alertDialog.cancel();
                            alertDialog.dismiss();
                            alertMessage(jsonObject.getString("msg"));
                            // Toast.makeText(newRquestActivity.this,"agency code is not map with this location please enter different agency code",Toast.LENGTH_LONG).show();


                        }
                        //                      enableLoadingBar(false);
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
                    enableLoadingBar(false);
                } else {
                    try {
                        Toast.makeText(mcontext, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(mcontext, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(mcontext, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });

    }

    public void enableLoadingBar(boolean enable) {
        if (enable) {
            loadProgressBar(null, null, false);
        } else {
            dismissProgressBar();
        }
    }

    public void loadProgressBar(String title, String message, boolean cancellable) {
        if (progressDialog == null)
            progressDialog = ProgressDialog.show(mcontext, null, null, false, cancellable);
        progressDialog.setContentView(R.layout.progress_bar);
        ImageView imgLoader = progressDialog.findViewById(R.id.img_loader);
        Glide.with(mcontext).load(R.drawable.loader_new).into(imgLoader);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

    }

    public void dismissProgressBar() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        progressDialog = null;
    }


}