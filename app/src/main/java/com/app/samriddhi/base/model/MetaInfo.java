package com.app.samriddhi.base.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class MetaInfo {

    @SerializedName("has_update")
    @Expose
    private Boolean hasUpdate;
    @SerializedName("force_update")
    @Expose
    private Boolean forceUpdate;
    @SerializedName("version")
    @Expose
    private String version;
    @SerializedName("link")
    @Expose
    private String link;

    public Boolean getHasUpdate() {
        return hasUpdate;
    }

    public void setHasUpdate(Boolean hasUpdate) {
        this.hasUpdate = hasUpdate;
    }

    public MetaInfo withHasUpdate(Boolean hasUpdate) {
        this.hasUpdate = hasUpdate;
        return this;
    }

    public Boolean getForceUpdate() {
        return forceUpdate;
    }

    public void setForceUpdate(Boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
    }

    public MetaInfo withForceUpdate(Boolean forceUpdate) {
        this.forceUpdate = forceUpdate;
        return this;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public MetaInfo withVersion(String version) {
        this.version = version;
        return this;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public MetaInfo withLink(String link) {
        this.link = link;
        return this;
    }

}
