package com.app.samriddhi.base;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.StringRes;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.app.samriddhi.BuildConfig;
import com.app.samriddhi.R;
import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.response.BaseResponse;
import com.app.samriddhi.permission.PermissionResultCallback;
import com.app.samriddhi.permission.PermissionUtils;
import com.app.samriddhi.ui.activity.auth.LoginActivity;
import com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule.KYBPActivity;
import com.app.samriddhi.util.AlertUtility;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.NetworkUtil;
import com.app.samriddhi.util.StringUtility;
import com.app.samriddhi.util.SystemUtility;
import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public abstract class BaseActivity extends AppCompatActivity implements PermissionResultCallback {
    public PermissionUtils permissionUtils;
    ProgressDialog progressDialog;
    ArrayList<String> storagePermissions = new ArrayList();
    /*LocationRequest mLocationRequest;*/
    private Snackbar snackbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        permissionUtils = new PermissionUtils(this);
        // setLocale();
        if (Build.VERSION.SDK_INT == 26) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }


    }

    public void setLocale() {
        Locale locale = getResources().getConfiguration().locale;
        Locale.setDefault(locale);
    }
    public void askStoragePermission(int requestCode) {
        storagePermissions.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        storagePermissions.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        storagePermissions.add(Manifest.permission.CAMERA);
        storagePermissions.add(Manifest.permission.READ_PHONE_STATE);
        storagePermissions.add(Manifest.permission.RECORD_AUDIO);
        permissionUtils.check_permission(storagePermissions, "Please allow Permissions to Continue", requestCode);
    }


    private void setAppLocale() {
        SystemUtility.setAppLocale(this, com.app.samriddhi.prefernces.PreferenceManager.getAppLang(this));
    }


    @Override
    protected void onResume() {
        super.onResume();
        setAppLocale();

        if (!NetworkUtil.isOnline(this)) {
            snackbar = Snackbar.make(findViewById(android.R.id.content), R.string.error_interent_message, Snackbar.LENGTH_INDEFINITE);
            snackbar.show();
        } else {
            if (snackbar != null) {
                snackbar.dismiss();
            }
        }
        NetworkUtil.addNetworkListener(new NetworkUtil.INetworkUtil() {
            @Override
            public void onNetworkChange(boolean available) {
                if (!available) {
                    snackbar = Snackbar.make(findViewById(android.R.id.content), R.string.error_interent_message, Snackbar.LENGTH_INDEFINITE);
                    snackbar.show();
                } else {
                    if (snackbar != null) {
                        snackbar.dismiss();
                    }
                }
            }
        });

    }


    public void startActivityAnimation(Context context, Class destinationClass, boolean addFlags) {
        if (addFlags)
            startActivity(new Intent(context, destinationClass).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        else
            startActivity(new Intent(context, destinationClass));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }


    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        dismissProgressBar();

    }

    //Get Shared Preferences
    public SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(this);
    }

    //Set Version on Screen
    public void setVersionOnScreen() {
        TextView tvSample = new TextView(this);
        tvSample.setTextColor(Color.RED);
        tvSample.setPadding(4, 4, 4, 4);
        tvSample.setGravity(Gravity.BOTTOM);
        tvSample.setText(BuildConfig.VERSION_NAME + "(" + BuildConfig.VERSION_CODE + ")");
        ((ViewGroup) findViewById(android.R.id.content)).addView(tvSample);
    }

    //Get Class Name
    public String getTag() {
        return getClass().getSimpleName();
    }

    //Log
    public void log(String message) {
        Log.d(getTag(), message);
    }

    //Alert Dialog
    public AlertDialog.Builder getAlertDialogBuilder(String title, String message, boolean cancellable) {
        return new AlertDialog.Builder(this, R.style.AppTheme_AlertDialog)
                .setTitle(title)
                .setMessage(message)
                .setCancelable(cancellable);
    }

    public void enableLoadingBar(boolean enable) {
        if (enable) {
            loadProgressBar(null, null, false);
        } else {
            dismissProgressBar();
        }
    }

    public void loadProgressBar(String title, String message, boolean cancellable) {
        if (progressDialog == null && !this.isFinishing())
            progressDialog = ProgressDialog.show(this, null, null, false, cancellable);
        progressDialog.setContentView(R.layout.progress_bar);
        ImageView imgLoader = progressDialog.findViewById(R.id.img_loader);
        // Glide.with(this).load(R.drawable.loader_new).into(imgLoader);
        Glide.with(this).load(R.drawable.loader_latest).into(imgLoader);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

    public void dismissProgressBar() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
        progressDialog = null;
    }


    public void onError(String reason) {
        onError(reason, false);
    }

    public void onError(String reason, final boolean finishOnOk) {


        if (!this.isFinishing()) {
            if (StringUtility.validateString(reason)) {
                AlertUtility.showFormAlert(this, reason, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                });
            } else {

                AlertUtility.showFormAlert(this, getString(R.string.default_error), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
            }
        }
        enableLoadingBar(false);
    }

    public void onInfo(String message) {
        onInfo(message, false);
    }

    public void BillonInfo(String message) {
        onInfo(message, true);
    }


    public void onInfo(String message, boolean finishOnOk) {
        if (!this.isFinishing())
        {
            AlertUtility.showFormAlert(this, message, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                }
            });
        }
    }

    /*Pass null errorMessage to disable error view*/
    public void setFieldError(TextInputLayout tilField, String errorMessage) {
        if (tilField != null) {
            if (StringUtility.validateString(errorMessage)) {
                tilField.setError(errorMessage);
            } else {
                tilField.setErrorEnabled(false);
            }
        }
    }

    public void hideKeyBoard() {
        try {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(findViewById(android.R.id.content).getWindowToken(), 0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void configureToolBar(@NonNull final Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().show();


    }

    public void configureToolBarBlack(@NonNull final Toolbar toolbar) {
        setSupportActionBar(toolbar);
        getSupportActionBar().show();
        for (int i = 0; i < toolbar.getChildCount(); i++) {
            View view = toolbar.getChildAt(i);
            if (view instanceof TextView) {
                TextView textView = (TextView) view;

            }
        }

    }


    public void handleBackButtonEvent(@NonNull final Toolbar toolbar) {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    protected String getCurrentLanguage() {
        return getResources().getConfiguration().locale.getLanguage();
    }


    public void toast(final String message) {
        Toast.makeText(BaseActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    public void toast(@StringRes final int message) {
        Toast.makeText(BaseActivity.this, message, Toast.LENGTH_SHORT).show();
    }

    public void replaceFragment(@IdRes int container, Fragment fragment) {
        replaceFragment(container, fragment, null);
    }

    public void replaceFragment(@IdRes final int container, final Fragment fragment, Bundle arguments) {
        if (arguments != null) {
            fragment.setArguments(arguments);
        }
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                getSupportFragmentManager().beginTransaction().replace(container, fragment).commitAllowingStateLoss();
            }
        }, 300);
    }

    public boolean isCurrentUser(String id) {
        return getPreferences().getString(Constant.KEY_USER_ID, "").equals(id);
    }

    public void snackBar(View view, @StringRes final int message) {
        Snackbar.make(view, message, Snackbar.LENGTH_SHORT).show();
    }

    public void snackBarLong(View view, @StringRes final int message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }

    public static void updateLanguage(Context ctx, String lang) {
        Configuration cfg = new Configuration();
        if (!TextUtils.isEmpty(lang))
            cfg.locale = new Locale(lang);
        else
            cfg.locale = Locale.getDefault();

        ctx.getResources().updateConfiguration(cfg, null);
    }


    public void onTokenExpired() {
        com.app.samriddhi.prefernces.PreferenceManager.clearAll(SamriddhiApplication.getmInstance());
        Intent intent = new Intent(SamriddhiApplication.getmInstance(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ActivityCompat.finishAffinity(this);
        startActivity(intent);
        finish();
    }

    public void onForceUpdate() {
    }

    public void onSoftUpdate() {
    }

    @Override
    public void PermissionGranted(int request_code) {
        Log.i("on BaseActivity", "=========== " + request_code);

    }

    @Override
    public void PartialPermissionGranted(int request_code, ArrayList<String> granted_permissions) {
        // will override
    }

    @Override
    public void PermissionDenied(int request_code) {
        // will override
    }

    @Override
    public void NeverAskAgain(int request_code) {
        askStoragePermission(202);
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (getIntent().hasExtra("notificationId")) {
            notificationRead(getIntent().getStringExtra("notificationId"));
        }
    }

    public void notificationRead(String notificationId) {
        SamriddhiApplication.getmInstance().getApiService().readNotifications(notificationId).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                Log.e("Respose from server", "===" + response.body().replyMsg);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
    }
}
