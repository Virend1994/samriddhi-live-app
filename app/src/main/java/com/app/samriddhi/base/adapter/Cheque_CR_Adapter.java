package com.app.samriddhi.base.adapter;

import android.app.DatePickerDialog;
import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;

import com.app.samriddhi.ui.model.CirReqChequeSetModel;
import com.app.samriddhi.util.Globals;

import java.util.ArrayList;
import java.util.Calendar;

public class Cheque_CR_Adapter  extends RecyclerView.Adapter<Cheque_CR_Adapter.halfViewHolder> {

    private final ArrayList<CirReqChequeSetModel> srList;
    private Context mcontext;
    private final Globals g = Globals.getInstance(mcontext);



    public Cheque_CR_Adapter(Context context, ArrayList<CirReqChequeSetModel> srList) {
        this.mcontext=context;
        this.srList=srList;
    }

    @NonNull
    @Override
    public halfViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cheque_cr_rec_list, parent, false);

        return new halfViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final halfViewHolder holder, final int position) {



        holder.amnt.setText(g.cheque_rec_datalist.get(position).getCheque_amt());
        holder.date.setText(g.cheque_rec_datalist.get(position).getCheque_date().toString());
        holder.amnt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                g.cheque_rec_datalist.get(position).setCheque_amt(holder.amnt.getText().toString());
            }
        });

        holder.date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int mYear,mMonth,mDay;
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(mcontext,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                holder.date.setText(year +"-"+(monthOfYear + 1) + "-" + dayOfMonth );
                                g.cheque_rec_datalist.get(position).setCheque_date(holder.date.getText().toString());


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();

            }
        });

        holder.deleteItem.setOnClickListener(v->{
            if(srList.size()>1){
                removeItem(position);
            }
        });

        if(srList.size()==1){
            holder.deleteItem.setVisibility(View.GONE);
        }
    }


    @Override
    public int getItemCount() {
        return srList.size();
    }
    private void removeItem(int position){
        srList.remove(position);
     //   notifyItemRemoved(position);
        notifyDataSetChanged();
    }
    public class halfViewHolder extends RecyclerView.ViewHolder {

        TextView date;
        EditText amnt;
        AppCompatImageView deleteItem;
        CardView viewCard;
        public halfViewHolder(View view) {
            super(view);
            deleteItem=view.findViewById(R.id.deleteItem);
            date=view.findViewById(R.id.date_cr);
            amnt=view.findViewById(R.id.cheque_amount);
        }
    }
}