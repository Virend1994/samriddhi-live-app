package com.app.samriddhi.base.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;

import com.app.samriddhi.SamriddhiApplication;

import com.app.samriddhi.prefernces.PreferenceManager;

import com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule.SubAgentModal;
import com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule.SunAgentAdapter;
import com.app.samriddhi.ui.activity.main.ui.Other.SubDetailAdapter;
import com.app.samriddhi.ui.model.CirReqDroppingSetModelAdpter;
import com.app.samriddhi.ui.model.DropDownModel;
import com.app.samriddhi.ui.model.EditionCRModel;
import com.app.samriddhi.util.Globals;
import com.app.samriddhi.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Drop_Point_Adapter extends RecyclerView.Adapter<Drop_Point_Adapter.halfViewHolder> implements  DropdownMenuAdapter.OnMeneuClickListnser, SunAgentAdapter.OnclickListener{

    private final ArrayList<CirReqDroppingSetModelAdpter> srList;
    private Context mcontext;
    public ArrayList<DropDownModel> dpMasterArray;
    public ArrayList<DropDownModel> routeMasterArray;
    public ArrayList<DropDownModel> EditionMasterArray;
    Globals g= Globals.getInstance(mcontext);
    //private ArrayList<EditionCRModel> edtList;
    public String dropSelectType="";
    Drop_Point_Adapter listenerContext;
    int getViewPosition;
   // Edition_CR_Adapter edition_cr_adapter;
    SunAgentAdapter edition_cr_adapter;

   ArrayList<EditionCRModel> editiondata_rec_datalist ;

    public Drop_Point_Adapter(Context context, ArrayList<CirReqDroppingSetModelAdpter> srList, ArrayList<DropDownModel> routeMasterArray,ArrayList<EditionCRModel> editiondata_rec_datalist) {
        this.mcontext=context;
        this.srList=srList;
        this.listenerContext=this;
        this.editiondata_rec_datalist = editiondata_rec_datalist;
       // this.edtList=new ArrayList<>();
        this.routeMasterArray=routeMasterArray;
        dpMasterArray=new ArrayList<>();
        EditionMasterArray=new ArrayList<>();

//        EditionCRModel obj=new EditionCRModel();
//        obj.setPk("");
//        obj.setName("");
//        obj.setCopy("");
        //edtList.add(obj);
    }

    @NonNull
    @Override
    public halfViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.drop_point_rec_list, parent, false);

        return new halfViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final halfViewHolder holder, final int position) {

    //  g.droppoint_rec_datalist.get(position).setCirReqDropping_set(edtList);


//        adapter_edi = new Edition_Drop_Down_CR_Adapter(mcontext, g.droppoint_rec_datalist.get(position).getCirReqDropping_set(),listenerContext,position);
//        holder.edition_rec_array.setAdapter(adapter_edi);
//        adapter_edi.notifyDataSetChanged();


        if (g.editiondata_rec_datalist.size()>0)
         {
            holder.agent_mapping.setText(g.droppoint_rec_datalist.get(position).getDroppingName());
            holder.agent_route.setText(g.droppoint_rec_datalist.get(position).getRouteName());
            holder.sub_agent_name.setText(g.droppoint_rec_datalist.get(position).getSub_agent_name());
            holder.noofcopy.setText(g.droppoint_rec_datalist.get(position).getNoofcopy());
            holder.ag_req.setText(g.droppoint_rec_datalist.get(position).getContact());
            holder.editionId.setText(g.droppoint_rec_datalist.get(position).getEditionName());
            holder.edt_location.setText(g.droppoint_rec_datalist.get(position).getLocation());
       }

        holder.agent_mapping.setOnClickListener(v->{
            getViewPosition=position;
            dropSelectType="dp";
            Util.showDropDown(dpMasterArray, "Select DropPoint", mcontext, this::onOptionClick);
        });

        holder.agent_route.setOnClickListener(v->{
            getViewPosition=position;
            dropSelectType="route";
            Util.showDropDown(routeMasterArray, "Select Route", mcontext, this::onOptionClick);
        });

        holder.editionId.setOnClickListener(v->{
            getViewPosition=position;
            dropSelectType="editionName";
            Util.showDropDown(g.SelectedEditionMasterArray, "Select Edition", mcontext, this::onOptionClick);
        });

     //  edition_cr_adapter = new Edition_CR_Adapter(mcontext, g.editiondata_rec_datalist, this);
        edition_cr_adapter = new SunAgentAdapter(mcontext, g.editiondata_subAgent_rec_datalist, this);
        holder.edition_rec_array.setAdapter(edition_cr_adapter);

        if(srList.size()==1){
            holder.deleteDropList.setVisibility(View.GONE);
        }

    }
    public ArrayList<CirReqDroppingSetModelAdpter> getSrList(){
        return  srList;
    }


    private void removeItem(int position){
        srList.remove(position);
        //notifyItemRemoved(position);
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        return srList.size();
    }

    @Override
    public void onOptionClick(DropDownModel liveTest) {
        if (dropSelectType.equalsIgnoreCase("dp"))
        {
            Util.hideDropDown();
           // srList.get(getViewPosition).setDroppingName(liveTest.getDescription());
          //  g.droppoint_rec_datalist.get(getViewPosition).setDroppingPk(liveTest.getId());
            g.droppoint_rec_datalist.get(getViewPosition).setDroppingPk(liveTest.getId());

            g.droppoint_rec_datalist.get(getViewPosition).setDroppingName(liveTest.getDescription());

        }
        if (dropSelectType.equalsIgnoreCase("route"))
        {
            Util.hideDropDown();

           // srList.get(getViewPosition).setRoutePk(liveTest.getId());
           // srList.get(getViewPosition).setRouteName(liveTest.getDescription());
            g.droppoint_rec_datalist.get(getViewPosition).setRoutePk(liveTest.getId());
            getDropPoint(liveTest.getId());
            g.droppoint_rec_datalist.get(getViewPosition).setRouteName(liveTest.getDescription());

        }
        if(dropSelectType.equalsIgnoreCase("editionName")){
            Util.hideDropDown();


            //srList.get(getViewPosition).setEditionPk(liveTest.getId());
           // srList.get(getViewPosition).setEditionName(liveTest.getDescription());
            g.droppoint_rec_datalist.get(getViewPosition).setEditionPk(liveTest.getId());
            g.droppoint_rec_datalist.get(getViewPosition).setEditionName(liveTest.getDescription());

        }
        notifyDataSetChanged();

    }

    @Override
    public void getSubAgentEditionList(ArrayList<SubAgentModal> EditionMasterArray) {

    }

   /* @Override
    public void getEditionList(ArrayList<EditionCRModel> EditionMasterArray) {

    }*/
//    @Override
//    public void onDelete(int adapterPos,int edtPos) {
//
//        srList.get(adapterPos).getCirReqDropping_set().remove(edtPos);
//        notifyDataSetChanged();
//
//
//    }

    public class halfViewHolder extends RecyclerView.ViewHolder {
        TextView agent_mapping,agent_route,addedition,editionId;
        EditText sub_agent_name,ag_req,noofcopy,edt_location;
        AppCompatImageView deleteDropList;
        RecyclerView edition_rec_array;


        public halfViewHolder(View view) {
            super(view);
            agent_mapping=view.findViewById(R.id.ag_mapping);
            sub_agent_name=view.findViewById(R.id.self_drop);
            ag_req=view.findViewById(R.id.contact_number);
            editionId=view.findViewById(R.id.editionId);
            addedition=view.findViewById(R.id.addedition);
            noofcopy=view.findViewById(R.id.map_copy);
            agent_route=view.findViewById(R.id.map_route);
            deleteDropList=view.findViewById(R.id.deleteDropList);
            edition_rec_array=view.findViewById(R.id.edition_rec_array);
            edt_location=view.findViewById(R.id.edt_location);
            edition_rec_array.setHasFixedSize(true);
            edition_rec_array.setLayoutManager(new LinearLayoutManager(mcontext, LinearLayoutManager.VERTICAL,false));


         deleteDropList.setOnClickListener(v->{
                if(srList.size()>1){
                    removeItem(getAdapterPosition());
                }
            });

            sub_agent_name.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    if(s.length()>0) {

                        g.droppoint_rec_datalist.get(getAdapterPosition()).setSub_agent_name(sub_agent_name.getText().toString());
                    }
   }
            });

         ag_req.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(s.length()>0) {
                        g.droppoint_rec_datalist.get(getAdapterPosition()).setContact(ag_req.getText().toString());
                    }

                }
            });

             noofcopy.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(s.length()>0){
                        g.droppoint_rec_datalist.get(getAdapterPosition()).setNoofcopy(noofcopy.getText().toString());

                    }
                }
            });



            edt_location.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(s.length()>0){
                        g.droppoint_rec_datalist.get(getAdapterPosition()).setLocation(edt_location.getText().toString());

                    }
                }
            });


            getEdition();
        }
    }

    private void getEdition() {
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getEditionByUnitId(PreferenceManager.getAgentId(mcontext),g.cirType,g.unitId);

        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData=jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if(status.equalsIgnoreCase("Success")){

                            EditionMasterArray.clear();
                            for(int i=0;i<objData.length();i++){


                                JSONObject objStr=objData.getJSONObject(i);

                                DropDownModel dropDownModel=new DropDownModel();

                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("edition_name"));
                                EditionMasterArray.add(dropDownModel);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                else {
                    try{
                        Toast.makeText(mcontext, "error message"+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    }catch(IOException e){
                        Toast.makeText(mcontext, "error message"+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Toast.makeText(mcontext, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void getDropPoint(String routeId) {

        // Calling JSON
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getDroppingPoint(PreferenceManager.getAgentId(mcontext),routeId);
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData=jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if(status.equalsIgnoreCase("Success")){
                            dpMasterArray.clear();
                            for(int i=0;i<objData.length();i++){


                                JSONObject objStr=objData.getJSONObject(i);

                                DropDownModel dropDownModel=new DropDownModel();
                                dropDownModel.setId(objStr.getString("pk"));

                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("dropping_name"));
                                dpMasterArray.add(dropDownModel);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                else {
                    try{
                        Toast.makeText(mcontext, "error message"+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    }catch(IOException e){
                        Toast.makeText(mcontext, "error message"+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Toast.makeText(mcontext, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }




}