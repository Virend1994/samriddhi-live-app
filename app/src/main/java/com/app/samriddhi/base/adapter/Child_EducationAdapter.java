package com.app.samriddhi.base.adapter;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.prefernces.PreferenceManager;

import com.app.samriddhi.ui.model.AgencyChildSet;
import com.app.samriddhi.ui.model.DropDownModel;
import com.app.samriddhi.util.Globals;
import com.app.samriddhi.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Child_EducationAdapter extends RecyclerView.Adapter<Child_EducationAdapter.halfViewHolder> implements  DropdownMenuAdapter.OnMeneuClickListnser{

    private ArrayList<AgencyChildSet> srLists;
    private Context mcontext;
    Globals g=Globals.getInstance(mcontext);
    private String child_gender_val="";
    int getViewPosition;
    List<String> genderList ;
    public ArrayList<DropDownModel> eduMaster;
    ArrayAdapter<String> dataAdapter;
    public String dropSelectType="";
    AlertDialog alertDialog;
    public Child_EducationAdapter(Context context, ArrayList<AgencyChildSet> srList) {


        this.mcontext=context;
        srLists=new ArrayList<>();
        this.srLists=srList;
        eduMaster=new ArrayList<>();
        genderList = new ArrayList<String>();
        genderList.add("Male");
        genderList.add("Female");
        genderList.add("Other");

    }

    @NonNull
    @Override
    public halfViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.child_rec_list, parent, false);

        return new halfViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final halfViewHolder holder, final int position) {




        if (g.child_rec_list.get(position).getChild_gender().equalsIgnoreCase("M"))
        {
            child_gender_val="M";
            Toast.makeText(mcontext,child_gender_val,Toast.LENGTH_SHORT).show();

            g.child_rec_list.get(position).setChild_gender(child_gender_val);
            holder.gender_kybp.setText("Male");
        }else if (g.child_rec_list.get(position).getChild_gender().equalsIgnoreCase("F"))
        {
            child_gender_val="F";
            Toast.makeText(mcontext,child_gender_val,Toast.LENGTH_SHORT).show();
            g.child_rec_list.get(position).setChild_gender(child_gender_val);
            holder.gender_kybp.setText("Female");
        }else if  (g.child_rec_list.get(position).getChild_gender().equalsIgnoreCase("O"))
        {
            child_gender_val="O";
            Toast.makeText(mcontext,child_gender_val,Toast.LENGTH_SHORT).show();
            g.child_rec_list.get(position).setChild_gender(child_gender_val);
            holder.gender_kybp.setText("Other");
        }

        holder.child_name_kybp.setText(g.child_rec_list.get(position).getChild_name());
        holder.child_dob_kybp.setText(g.child_rec_list.get(position).getChild_DOB());
        holder.child_edu_kybp.setText(g.child_rec_list.get(position).getChild_educationName());

//
//
        holder.child_name_kybp.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(s.length()>0){

                    g.child_rec_list.get(position).setChild_name(holder.child_name_kybp.getText().toString());

                }
            }
        });











        holder.child_dob_kybp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int mYear,mMonth,mDay;
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);




                    DatePickerDialog datePickerDialog = new DatePickerDialog(mcontext,
                            new DatePickerDialog.OnDateSetListener() {

                                @Override
                                public void onDateSet(DatePicker view, int year,
                                                      int monthOfYear, int dayOfMonth) {
                                    Log.e("agent Age", String.valueOf(g.agentAge));
                                    Log.e("child Age", String.valueOf(Util.getAge(mYear,mMonth,mDay)));

                                    if(Util.getAge(year,monthOfYear,dayOfMonth)>g.agentAge){
                                        alertMessage("child age sholud be less then agent age");
                                        return;

                                    }else {
                                             holder.child_dob_kybp.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);



                                        g.child_rec_list.get(position).setChild_DOB(holder.child_dob_kybp.getText().toString());

                                    }


                                    //   Toast.makeText(mcontext, holder.child_dob_kybp.getText().toString(),Toast.LENGTH_SHORT).show();

                                }
                            }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                datePickerDialog.show();

            }
        });










        holder.gender_kybp.setOnClickListener(v->{
            final CharSequence[] charSequence = genderList.toArray(new CharSequence[genderList.size()]);
            AlertDialog.Builder builder = new AlertDialog.Builder(mcontext);

            View view1 = LayoutInflater.from(mcontext).inflate(R.layout.spinnerlayout, null);


            builder.setView(view1);
            builder.setItems(charSequence, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    holder.gender_kybp.setText(charSequence[i]);

                    child_gender_val= genderList.get(i);
                    if (child_gender_val.equalsIgnoreCase("Male"))
                    {
                        child_gender_val="M";
                        Toast.makeText(mcontext,child_gender_val,Toast.LENGTH_SHORT).show();

                        g.child_rec_list.get(position).setChild_gender(child_gender_val);
                    }
                    if (child_gender_val.equalsIgnoreCase("Female"))
                    {
                        child_gender_val="F";
                        Toast.makeText(mcontext,child_gender_val,Toast.LENGTH_SHORT).show();
                        g.child_rec_list.get(position).setChild_gender(child_gender_val);
                    }
                    if (child_gender_val.equalsIgnoreCase("Other"))
                    {
                        child_gender_val="O";
                        Toast.makeText(mcontext,child_gender_val,Toast.LENGTH_SHORT).show();
                        g.child_rec_list.get(position).setChild_gender(child_gender_val);
                    }



                }
            });
            alertDialog = builder.create();
            alertDialog.show();


        });
        holder.child_edu_kybp.setOnClickListener(v->{
            getViewPosition=position;
            dropSelectType="education";
            Util OBJ=new Util();
            Util.showDropDown(eduMaster, "Select Education", mcontext, this::onOptionClick);


           // g.child_rec_list.get(position).put("c_edu", child_gender_val);
         //   holder.child_edu_kybp.setText(g.child_rec_list.get(getViewPosition).get("c_edu").toString());
        });



    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
    private void alertMessage(String message) {

        android.app.AlertDialog alertDialog = new android.app.AlertDialog.Builder(mcontext, R.style.AppTheme_AlertDialog)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })

                .show();
    }
    @Override
    public int getItemCount() {
        return srLists.size();
    }

    @Override
    public void onOptionClick(DropDownModel liveTest) {

        if (dropSelectType.equalsIgnoreCase("education"))
        {
            Util.hideDropDown();
            g.child_rec_list.get(getViewPosition).setChild_education(liveTest.getId());
            g.child_rec_list.get(getViewPosition).setChild_educationName(liveTest.getDescription());
            notifyDataSetChanged();

        }


    }

    public class halfViewHolder extends RecyclerView.ViewHolder {
        EditText child_name_kybp;
        TextView child_edu_kybp;
        TextView child_dob_kybp;
        TextView gender_kybp;

        public halfViewHolder(View view) {
            super(view);

            child_name_kybp=view.findViewById(R.id.child_name_kybp);
            gender_kybp=view.findViewById(R.id.gender_kybp);
            child_dob_kybp=view.findViewById(R.id.child_dob_kybp);
            child_edu_kybp=view.findViewById(R.id.child_edu_kybp);
           getEducation();
        }
    }



    public void getEducation() {
        // Calling JSON
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getEducation(PreferenceManager.getAgentId(mcontext));
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("getEducation==>>", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData=jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if(status.equalsIgnoreCase("Success")){
                            eduMaster.clear();
                            for(int i=0;i<objData.length();i++){


                                JSONObject objStr=objData.getJSONObject(i);

                                DropDownModel dropDownModel=new DropDownModel();
                                dropDownModel.setId(objStr.getString("pk"));

                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("Education_text"));
                                eduMaster.add(dropDownModel);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                else {
                    try{
                        Toast.makeText(mcontext, "error message"+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    }catch(IOException e){
                        Toast.makeText(mcontext, "error message"+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Toast.makeText(mcontext, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }


}