package com.app.samriddhi.base.adapter;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;

import com.app.samriddhi.ui.model.ReasonAdapterModel;
import com.app.samriddhi.util.Globals;

import java.util.ArrayList;

public class Reason_agency_adapter extends  RecyclerView.Adapter<Reason_agency_adapter.halfViewHolder> {

    private final ArrayList<ReasonAdapterModel> srList;
    private Context mcontext;
    private final Globals g = Globals.getInstance(mcontext);

    private  static AlertDialog alertDialog;
    ProgressDialog progressDialog;

    public Reason_agency_adapter(Context context, ArrayList<ReasonAdapterModel> srList) {
        this.mcontext=context;
        this.srList=srList;
    }

    @NonNull
    @Override
    public halfViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.agency_reason_view_data_adapter, parent, false);

        return new halfViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final halfViewHolder holder, final int position) {


        ReasonAdapterModel dataObj=srList.get(position);
        holder.reason.setText(dataObj.getAg_reason());

        holder.agcode.setText(dataObj.getAg_code());
        holder.copy.setText(dataObj.getAg_copies());
        holder.agent_name.setText(dataObj.getAg_name());
        holder.agencyName.setText(dataObj.getAg_name());
        holder.agencyLocation.setText(dataObj.getAgency_location());
        holder.asd.setText(dataObj.getAsd());
        holder.outstanding.setText(dataObj.getOutstanding());
        holder.unbilledAmount.setText(dataObj.getUnbilled_amount());

        holder.totalOutstanding.setText(dataObj.getTotal_outstanding());
        if(g.reasonTypeName.equalsIgnoreCase("Sub Agent Promotion")){
            holder.subAgentLayout.setVisibility(View.VISIBLE);
            holder.agencyTittle.setText(" Main Agent ");
            holder.subAgent_copies.setText(dataObj.getSubag_copies());
            holder.subAgent_code.setText(dataObj.getSubag_code());
        }else{
            holder.subAgentLayout.setVisibility(View.GONE);
        }

        if(g.reasonTypeName.equalsIgnoreCase("Replacement")){
            holder.llReplacement.setVisibility(View.VISIBLE);

        }else{
            holder.llReplacement.setVisibility(View.GONE);
        }




    }



    @Override
    public int getItemCount() {
        return srList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }





    public class halfViewHolder extends RecyclerView.ViewHolder {
        public TextView agcode,agencyTittle,copy,agent_name,reason,subAgent_code,subAgent_copies,agencyName,agencyLocation,asd,outstanding,unbilledAmount,totalOutstanding;
        LinearLayout subAgentLayout,llReplacement;


        public halfViewHolder(View view) {
            super(view);

            subAgent_code=view.findViewById(R.id.subAgent_code);
            subAgent_copies=view.findViewById(R.id.subAgent_copies);
            agcode=view.findViewById(R.id.inten_agcode);
            copy=view.findViewById(R.id.inten_copy);
            agencyTittle=view.findViewById(R.id.agencyTittle);
            reason=view.findViewById(R.id.inten_reason);
            subAgentLayout=view.findViewById(R.id.subAgentLayout);
            agent_name=view.findViewById(R.id.agent_name);
            agencyName=view.findViewById(R.id.tv_replaced_agency_name);
            agencyLocation=view.findViewById(R.id.tv_replaced_agency_location);
            asd=view.findViewById(R.id.tv_replaced_asd);
            outstanding=view.findViewById(R.id.tv_replaced_current_outstanding);
            unbilledAmount=view.findViewById(R.id.tv_replaced_unbilled_amount);
            totalOutstanding=view.findViewById(R.id.tv_replaced_total_outstanding);
            llReplacement=view.findViewById(R.id.ll_replacement_time);




        }
    }








}