package com.app.samriddhi.base.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;
import com.app.samriddhi.ui.model.DropDownModel;
import com.google.android.material.card.MaterialCardView;

import java.util.ArrayList;
import java.util.List;

public class DropdownMenuAdapter extends RecyclerView.Adapter<DropdownMenuAdapter.MyViewHolder> implements Filterable {

    private List<DropDownModel> menuClassList;
    private final OnMeneuClickListnser onMenuListClicklistener;
    static int getPosition;
    private int row_index;
    Context mContext;
    private final List<DropDownModel> searchArray;

    public DropdownMenuAdapter(ArrayList<DropDownModel> menuClassList, OnMeneuClickListnser onLiveTestClickListener, Context context) {
        this.menuClassList = menuClassList;
        this.onMenuListClicklistener = onLiveTestClickListener;
        this.searchArray = new ArrayList<>();
        this.searchArray.addAll(menuClassList);
        this.mContext = context;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = null;


        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.menuviewlayout, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        DropDownModel ticket = menuClassList.get(position);

        holder.nameText.setText(ticket.getDescription());
        //holder.number.setText(ticket.getTicketNumber());


        holder.openpanel.setOnClickListener(v -> {
            onMenuListClicklistener.onOptionClick(menuClassList.get(position));
            // notifyDataSetChanged();
        });




    }

    void add(DropDownModel ticket) {
        this.menuClassList.add(ticket);
    }

    public void addAll(List<DropDownModel> tickets) {
        this.menuClassList.addAll(tickets);
    }

    @Override
    public int getItemCount() {
        return menuClassList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView number, nameText;
        MaterialCardView openpanel;

        MyViewHolder(View view) {
            super(view);

            openpanel = view.findViewById(R.id.openpanel);
            nameText = view.findViewById(R.id.name);


        }
    }

    public interface OnMeneuClickListnser {
        void onOptionClick(DropDownModel listData);
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String filterString = charSequence.toString().toLowerCase();
                //arraylist1.clear();
                FilterResults results = new FilterResults();
                int count = searchArray.size();
                final List<DropDownModel> list = new ArrayList<>();

                DropDownModel filterableString;

                for (int i = 0; i < count; i++) {
                    filterableString = searchArray.get(i);
                    if (searchArray.get(i).getDescription().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                    }
                }

                results.values = list;
                results.count = list.size();

                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                menuClassList = (List<DropDownModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };


    }
}

