package com.app.samriddhi.base.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.StringRes;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ViewDataBinding;
import androidx.databinding.library.baseAdapters.BR;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;
import com.app.samriddhi.base.model.BaseModel;
import com.app.samriddhi.ui.model.AgentListModel;
import com.app.samriddhi.ui.model.BankListModel;
import com.app.samriddhi.ui.model.BillOutCityUPCDistWiseListModel;
import com.app.samriddhi.ui.model.BillOutCityUPCListModel;
import com.app.samriddhi.ui.model.BillingListModel;
import com.app.samriddhi.ui.model.BillingOutstandingListModel;
import com.app.samriddhi.ui.model.CityListModel;
import com.app.samriddhi.ui.model.CopiesData;
import com.app.samriddhi.ui.model.CreditCashModel;
import com.app.samriddhi.ui.model.GrievanceCategoryModel;
import com.app.samriddhi.ui.model.GrievanceModel;
import com.app.samriddhi.ui.model.GrievanceOpenItemListModel;
import com.app.samriddhi.ui.model.HintModel;
import com.app.samriddhi.ui.model.IncidentCommentData;
import com.app.samriddhi.ui.model.IncidentListModel;
import com.app.samriddhi.ui.model.LedgerTransModel;
import com.app.samriddhi.ui.model.NotificationModel;
import com.app.samriddhi.ui.model.SalesDistrictWiseListModel;
import com.app.samriddhi.ui.model.SalesOrgCityListModel;
import com.app.samriddhi.ui.model.SalesUserListModel;
import com.app.samriddhi.ui.model.StateListModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Admin on 01-09-2016.
 */
public class RecyclerViewArrayAdapter<T extends BaseModel>
        extends RecyclerView.Adapter<RecyclerViewArrayAdapter.MyViewHolder> {

    private ListSelectionMode selectionMode = ListSelectionMode.NONE;
    private List<T> mObjects;
    private final OnItemClickListener<T> onItemClickListener;
    private TextView emptyTextView;
    private int emptyViewText = R.string.default_empty_list_info;
    private Fragment fragment;
    private String screenName = "";
    public RecyclerViewArrayAdapter(final List<T> objects) {
        this(objects, null);
    }

    public RecyclerViewArrayAdapter(final List<T> objects, OnItemClickListener onItemClickListener) {
        this(objects, onItemClickListener, ListSelectionMode.NONE, "");
    }

    public RecyclerViewArrayAdapter(final List<T> objects, OnItemClickListener onItemClickListener, String screenName) {
        this(objects, onItemClickListener, ListSelectionMode.NONE, screenName);
    }


    public RecyclerViewArrayAdapter(final List<T> objects, OnItemClickListener onItemClickListener, ListSelectionMode selectionMode, String screenName) {
        mObjects = objects;
        this.selectionMode = selectionMode;
        this.onItemClickListener = onItemClickListener;
        this.screenName = screenName;
    }

    public List<T> getmObjects() {
        return mObjects;
    }

    public void setEmptyTextView(TextView emptyTextView, @StringRes int emptyViewText) {
        this.emptyTextView = emptyTextView;
        this.emptyViewText = emptyViewText;
    }

    public void setSelectionMode(ListSelectionMode selectionMode) {
        this.selectionMode = selectionMode;
    }

    public void setFragment(Fragment fragment) {
        this.fragment = fragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewDataBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), viewType, parent, false);

        return new MyViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.getBinding().setVariable(BR.item, getItem(position));
        holder.getBinding().setVariable(BR.itemClickListener, onItemClickListener);
        holder.getBinding().setVariable(BR._all, onItemClickListener);
        holder.getBinding().setVariable(BR.index, position);
        holder.getBinding().setVariable(BR.screenType, screenName);
        if (mObjects.get(position) instanceof StateListModel) {
            holder.binding.getRoot().findViewById(R.id.img_arrow_exp).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if( holder.binding.getRoot().findViewById(R.id.img_arrow_exp).getRotation()==360){
                        holder. binding.getRoot().findViewById(R.id.ll_childview).setVisibility(View.VISIBLE);
                        holder. binding.getRoot().findViewById(R.id.img_arrow_exp).setRotation(180);


                    }
                    else {
                        holder. binding.getRoot().findViewById(R.id.ll_childview).setVisibility(View.GONE);
                        holder. binding.getRoot().findViewById(R.id.img_arrow_exp).setRotation(360);
                    }

                }
            });

    }
        if (mObjects.get(position) instanceof SalesOrgCityListModel) {
            holder.binding.getRoot().findViewById(R.id.img_arrow_exp_city).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if( holder.binding.getRoot().findViewById(R.id.img_arrow_exp_city).getRotation()==360){
                        holder. binding.getRoot().findViewById(R.id.ll_childview_city).setVisibility(View.VISIBLE);
                        holder. binding.getRoot().findViewById(R.id.img_arrow_exp_city).setRotation(180);


                    }
                    else {
                        holder. binding.getRoot().findViewById(R.id.ll_childview_city).setVisibility(View.GONE);
                        holder. binding.getRoot().findViewById(R.id.img_arrow_exp_city).setRotation(360);
                    }

                }
            });

    }
        if (mObjects.get(position) instanceof SalesDistrictWiseListModel){
            holder.binding.getRoot().findViewById(R.id.img_arrow_exp_dis).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if( holder.binding.getRoot().findViewById(R.id.img_arrow_exp_dis).getRotation()==360){
                        holder. binding.getRoot().findViewById(R.id.ll_childview_exp_dis).setVisibility(View.VISIBLE);
                        holder. binding.getRoot().findViewById(R.id.img_arrow_exp_dis).setRotation(180);


                    }
                    else {
                        holder. binding.getRoot().findViewById(R.id.ll_childview_exp_dis).setVisibility(View.GONE);
                        holder. binding.getRoot().findViewById(R.id.img_arrow_exp_dis).setRotation(360);
                    }

                }
            });

    }
        if (mObjects.get(position) instanceof SalesUserListModel){
            holder.binding.getRoot().findViewById(R.id.img_arrow_exp_city_upc).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if( holder.binding.getRoot().findViewById(R.id.img_arrow_exp_city_upc).getRotation()==360){
                        holder. binding.getRoot().findViewById(R.id.ll_childview_city_upc).setVisibility(View.VISIBLE);
                        holder. binding.getRoot().findViewById(R.id.img_arrow_exp_city_upc).setRotation(180);


                    }
                    else {
                        holder. binding.getRoot().findViewById(R.id.ll_childview_city_upc).setVisibility(View.GONE);
                        holder. binding.getRoot().findViewById(R.id.img_arrow_exp_city_upc).setRotation(360);
                    }

                }
            });

    }


    }

    @Override
    public int getItemViewType(int position) {
        if (mObjects.get(position) instanceof CopiesData) {
            return R.layout.item_copies_data;
        } else if (mObjects.get(position) instanceof CityListModel) {
            return R.layout.item_cities_list;
        } else if (mObjects.get(position) instanceof AgentListModel) {
            return R.layout.item_agencies;
        }
        else if (mObjects.get(position) instanceof StateListModel) {
            return R.layout.item_state_list;
        }
        else if (mObjects.get(position) instanceof BillingOutstandingListModel) {
            return R.layout.item_bill_out_state_list;
        }

        else if (mObjects.get(position) instanceof BillOutCityUPCListModel) {
            return R.layout.item_bill_out_city_upc_list;
        }

        else if (mObjects.get(position) instanceof BillOutCityUPCDistWiseListModel) {
            return R.layout.item_bill_out_city_upc_dist_list;
        }
        else if (mObjects.get(position) instanceof BillingListModel) {
            return R.layout.item_billing;
        }/* else if (mObjects.get(position) instanceof LedgerTransModel) {
            return R.layout.item_ledger;
        }*/
        else if (mObjects.get(position) instanceof HintModel)
            return R.layout.item_hints;
        else if (mObjects.get(position) instanceof GrievanceModel) {
            return R.layout.item_grievance_options;
        } else if (mObjects.get(position) instanceof GrievanceCategoryModel) {
            return R.layout.item_category_type;
        } else if (mObjects.get(position) instanceof GrievanceOpenItemListModel) {
            return R.layout.item_open_complaints;
        } else if (mObjects.get(position) instanceof IncidentListModel) {
            return R.layout.item_sub_incident_list;
        } else if (mObjects.get(position) instanceof IncidentCommentData) {
            return R.layout.item_grievance_comments;
        } else if (mObjects.get(position) instanceof CreditCashModel) {
            return R.layout.item_cash_credit;
        } else if (mObjects.get(position) instanceof SalesUserListModel) {
            return R.layout.item_sales_group;
        } else if (mObjects.get(position) instanceof SalesOrgCityListModel) {
            return R.layout.item_sales_org_city;
        } else if (mObjects.get(position) instanceof SalesDistrictWiseListModel) {
            return R.layout.item_sales_district;
        } else if (mObjects.get(position) instanceof BankListModel) {
            return R.layout.item_banks;
        } else if (mObjects.get(position) instanceof NotificationModel)
            return R.layout.item_notifications;
        return -1;
    }

    /**
     * Adds the specified object at the end of the array.
     *
     * @param object The object to add at the end of the array.
     */
    public void add(final T object) {
        mObjects.add(object);
        notifyItemInserted(getItemCount() - 1);
    }

    public void addAll(final List<T> objects) {
        int posStart = mObjects.size();
        mObjects.addAll(objects);
        notifyItemRangeInserted(posStart, getItemCount() - 1);
    }

    /**
     * Remove all elements from the list.
     */
    public void clear() {
        final int size = getItemCount();
        mObjects.clear();
        notifyItemRangeRemoved(0, size);
    }


    @Override
    public int getItemCount() {
        if (emptyTextView != null) {
            if (mObjects.size() == 0) {
                emptyTextView.setVisibility(View.VISIBLE);
                emptyTextView.setText(emptyViewText);
            } else {
                emptyTextView.setVisibility(View.INVISIBLE);
            }
        }

        return mObjects.size();
    }

    public T getItem(final int position) {
        return mObjects.get(position);
    }

    public long getItemId(final int position) {
        return position;
    }

    /**
     * Returns the position of the specified item in the array.
     *
     * @param item The item to retrieve the position of.
     * @return The position of the specified item.
     */
    public int getPosition(final T item) {
        return mObjects.indexOf(item);
    }

    /**
     * Inserts the specified object at the specified index in the array.
     *
     * @param object The object to insert into the array.
     * @param index  The index at which the object must be inserted.
     */
    public void insert(final T object, int index) {
        mObjects.add(index, object);
        notifyItemInserted(index);
    }

    public void insert(final List<T> objects, int index) {
        int posStart = mObjects.size();
        mObjects.addAll(index, objects);
        notifyItemRangeInserted(posStart, getItemCount() - 1);
    }

    /**
     * Removes the specified object from the array.
     *
     * @param object The object to remove.
     */
    public void remove(T object) {
        final int position = getPosition(object);
        mObjects.remove(object);
        notifyItemRemoved(position);
    }

    /**
     * Sorts the content of this adapter using the specified comparator.
     *
     * @param comparator The comparator used to sort the objects contained in this adapter.
     */
    public void sort(Comparator<? super T> comparator) {
        Collections.sort(mObjects, comparator);
        notifyItemRangeChanged(0, getItemCount());
    }

    public void addItem(int position, T model) {
        mObjects.add(position, model);
        notifyItemInserted(position);
    }

    public void setItem(int position, T model) {
        mObjects.set(position, model);
        notifyItemChanged(position);
    }

    public void moveItem(int fromPosition, int toPosition) {
        final T model = mObjects.remove(fromPosition);
        mObjects.add(toPosition, model);
        notifyItemMoved(fromPosition, toPosition);
    }

    public void update(List<T> models, boolean replaceExisting) {
        /*removal of objects creates problem with pagination logic, so this is skipped.*/
//        applyAndAnimateRemovals(models);
        applyAndAnimateAdditions(models, replaceExisting);
        applyAndAnimateMovedItems(models);

        //Collections.sort(models);
    }

    public ArrayList<T> getSelectedItems() {
        ArrayList<T> selectedItems = new ArrayList<>();
        for (T mObject : mObjects) {
            if (mObject.isSelected()) {
                selectedItems.add(mObject);
            }
        }
        return selectedItems;
    }

    public void setSelectedItems(List<T> previouslySelectedItems) {
        for (T mObject : mObjects) {
            mObject.setSelected(previouslySelectedItems.contains(mObject));
        }
    }

    private void applyAndAnimateRemovals(List<T> newModels) {
        for (T mObject : mObjects) {
            if (!newModels.contains(mObject)) {
                remove(mObject);
            }
        }
    }

    private void applyAndAnimateAdditions(List<T> newModels, boolean replaceExisting) {
        for (int i = 0, count = newModels.size(); i < count; i++) {
            final T model = newModels.get(i);

            int index = mObjects.indexOf(model);
            if (!mObjects.contains(model)) {
                addItem(i, model);
            } else {
                if (replaceExisting) {
                    setItem(index, model);
                }
            }
        }
    }

    private void applyAndAnimateMovedItems(List<T> newModels) {
        for (int toPosition = newModels.size() - 1; toPosition >= 0; toPosition--) {
            final T model = newModels.get(toPosition);
            final int fromPosition = mObjects.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }
    }

    public void updateList(List<T> list) {
        mObjects = list;
        notifyDataSetChanged();
    }

    public interface OnItemClickListener<T> {
        void onItemClick(View view, T object);
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        ViewDataBinding binding;

        public MyViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
            this.binding.executePendingBindings();
        }

        public ViewDataBinding getBinding() {
            return binding;
        }
    }


}
