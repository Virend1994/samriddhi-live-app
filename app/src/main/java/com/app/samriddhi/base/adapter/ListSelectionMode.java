package com.app.samriddhi.base.adapter;

/**
 * Created by Admin on 27-12-2016.
 */

public enum ListSelectionMode {
    NONE, SINGLE, MULTIPLE
}
