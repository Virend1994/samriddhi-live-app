package com.app.samriddhi.base.adapter;

import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.R;

import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.model.ProposedAgencyModel;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.Globals;

import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.Credentials;


public class Perposed_Agency_Adapter extends RecyclerView.Adapter<Perposed_Agency_Adapter.halfViewHolder> {

    private final ArrayList<ProposedAgencyModel> srList;
    private Context mcontext;
    private final Globals g = Globals.getInstance(mcontext);

    private final OnclickListener onclickListener;


    public Perposed_Agency_Adapter(Context context, ArrayList<ProposedAgencyModel> srList, OnclickListener onclickListener) {
        this.mcontext = context;
        this.srList = srList;

        this.onclickListener = onclickListener;
    }

    @NonNull
    @Override
    public halfViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.perposed_agency_rec_list, parent, false);

        return new halfViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final halfViewHolder holder, final int position) {
        ProposedAgencyModel data = srList.get(position);
        Log.e("sdgsdgsgs", "DetailId" + " " + srList.get(position).getId());
        Log.e("sdgsdgsgs", "DetailId" + " " + srList.get(position).getAg_req_id());


        if (data.getAgencyRequestCIRReq_count() == 1) {
            if (data.getAgencyRequestSurvey_count() == 1) {
                if (data.getAgencyRequestKYBP_count() == 1) {
                    if (Integer.parseInt(g.jjSaleOfficeCopy) > 0) {
                        if (data.getAgencyRequestCIRReqJJ_count() == 1) {
                            holder.otp.setVisibility(View.VISIBLE);
                            holder.linearContract.setVisibility(View.VISIBLE);
                        } else {
                            holder.otp.setVisibility(View.GONE);
                            holder.linearContract.setVisibility(View.GONE);
                        }
                    } else {
                        holder.otp.setVisibility(View.VISIBLE);
                        holder.linearContract.setVisibility(View.VISIBLE);
                    }
                }
            }
        } else {

            holder.otp.setVisibility(View.GONE);
            holder.linearContract.setVisibility(View.GONE);
        }
        if (data.getOtp_validation().equals("1")) {

            holder.otp.setVisibility(View.GONE);
            holder.linearContract.setVisibility(View.VISIBLE);
        }

        holder.jj_cir.setOnClickListener(v -> {
            onclickListener.onItemClick("jj", data);
        });
        holder.survey.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                    if (g.count==0) {
                onclickListener.onItemClick("survey", data);
//                    }
//                    else
//                    {
//                        Toast.makeText(mcontext, "Survey Form already Submitted for this agency", Toast.LENGTH_SHORT).show();
//                    }
            }
        });


        holder.kybp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //Toast.makeText(mcontext,"Bhupesh ",Toast.LENGTH_SHORT).show();
                onclickListener.onItemClick("kybp", data);


            }
        });

        holder.cir_rpt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onclickListener.onItemClick("cir_rpt", data);

            }
        });


        holder.otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onclickListener.onItemClick("optMobile", data);
            }
        });


       /* if (position > 1) {
            holder.delete.setVisibility(View.VISIBLE);
        } else {
            holder.delete.setVisibility(View.INVISIBLE);
        }*/

        if (Integer.parseInt(g.jjSaleOfficeCopy) > 0) {
            holder.jj_cir.setVisibility(View.VISIBLE);
        } else {
            holder.jj_cir.setVisibility(View.GONE);
        }

        if (Integer.parseInt(g.mainSaleOfficeCopy) > 0) {
            holder.cir_rpt.setVisibility(View.VISIBLE);
        } else {
            holder.cir_rpt.setVisibility(View.GONE);
        }

        holder.tvAgentName.setText(srList.get(position).getAgency_name());

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(mcontext);
                builder.setMessage("Are you sure you want to delete this?")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                            public void onClick(final DialogInterface dialog, int which) {
                                Log.e("safaaasaffafa", data.getId());
                                Delete(data.getId(), "0", position);
                                dialog.dismiss();
                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();

                            }
                        })
                        .setIcon(R.drawable.ic_action_delete)
                        .show();

               /* if (position >= 2) {
                    srList.remove(position);
                    notifyDataSetChanged();
                }*/
            }
        });

        holder.open.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.linearLayout.getVisibility() == View.GONE) {
                    holder.linearLayout.setVisibility(View.VISIBLE);
                    holder.open.setImageResource(R.drawable.arrow_up);
                } else {
                    holder.linearLayout.setVisibility(View.GONE);
                    holder.open.setImageResource(R.drawable.arrow_down);
                }
            }
        });

        holder.cir_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // http://127.0.0.1:8000/dash/agreement_form_hi/147/
                // http://127.0.0.1:8000/dash/agreement_form_hi/147/?view=1
                String url = Constant.BASE_PORTAL_URL + "dash/agreement_form_hi/" + data.getRequestId() + "/";
                Log.e("fdggdgdgdgd", url);
                try {
                    Intent i = new Intent("android.intent.action.MAIN");
                    i.setComponent(ComponentName.unflattenFromString("com.android.chrome/com.android.chrome.Main"));
                    i.addCategory("android.intent.category.LAUNCHER");
                    i.setData(Uri.parse(url));
                    mcontext.startActivity(i);
                } catch (ActivityNotFoundException e) {
                    // Chrome is not installed
                    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    mcontext.startActivity(i);
                }
            }
        });
        holder.cir_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




                String url = Constant.BASE_PORTAL_URL + "dash/agreement_form_hi/" + data.getRequestId() + "/?view=1";
                Log.e("fdggdgdgdgd", url);
                try {
                    Intent i = new Intent("android.intent.action.MAIN");
                    i.setComponent(ComponentName.unflattenFromString("com.android.chrome/com.android.chrome.Main"));
                    i.addCategory("android.intent.category.LAUNCHER");
                    i.setData(Uri.parse(url));
                    mcontext.startActivity(i);
                } catch (ActivityNotFoundException e) {
                    // Chrome is not installed
                    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    mcontext.startActivity(i);
                }
            }
        });

        if (PreferenceManager.getPrefUserType(mcontext).equalsIgnoreCase("EX")
                || PreferenceManager.getPrefUserType(mcontext).equalsIgnoreCase("CE")
                || PreferenceManager.getPrefUserType(mcontext).equalsIgnoreCase("UE")
                || PreferenceManager.getPrefUserType(mcontext).equalsIgnoreCase("CI")
                || PreferenceManager.getPrefUserType(mcontext).equalsIgnoreCase("UH")) {


                 holder.cir_view.setVisibility(View.VISIBLE);
                 holder.cir_edit.setVisibility(View.VISIBLE);

        }

        if (PreferenceManager.getPrefUserType(mcontext).equalsIgnoreCase("HO")
                || PreferenceManager.getPrefUserType(mcontext).equalsIgnoreCase("SH")) {

            holder.cir_view.setVisibility(View.VISIBLE);
            holder.cir_edit.setVisibility(View.GONE);

        }

    }


    @Override
    public int getItemCount() {
        return srList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public class halfViewHolder extends RecyclerView.ViewHolder {
        public TextView survey, kybp, cir_rpt, jj_cir, otp, tvAgentName, cir_view, cir_edit;
        public ImageView open, delete;
        public LinearLayout linearLayout, linearContract;


        public halfViewHolder(View view) {
            super(view);
            tvAgentName = view.findViewById(R.id.tvAgentName);

            open = view.findViewById(R.id.open);
            delete = view.findViewById(R.id.delete_agency);
            linearLayout = view.findViewById(R.id.btn_layout);
            survey = view.findViewById(R.id.survey_form);
            jj_cir = view.findViewById(R.id.jj_cir);
            kybp = view.findViewById(R.id.kybp_form);
            cir_rpt = view.findViewById(R.id.cir_recipt);
            otp = view.findViewById(R.id.otp_veri);
            linearContract = view.findViewById(R.id.linearContract);
            cir_view = view.findViewById(R.id.cir_view);
            cir_edit = view.findViewById(R.id.cir_edit);


        }
    }

    public interface OnclickListener {
        void onItemClick(String formType, ProposedAgencyModel proposedAgencyModel);
    }


    public void Delete(String strId, String status, int position) {
        ProgressDialog progressDialog = new ProgressDialog(mcontext);
        progressDialog.setMessage("Please wait..");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(true);
        progressDialog.show();
        http:
//103.96.220.236/agency/changeAgStatus/518/0
        AndroidNetworking.get(Constant.BASE_PORTAL_URL + "agency/changeAgStatus/" + strId + "/" + status)
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            Log.e("sfsdfsfsf", response.toString());

                            if (response.has("message")) {
                                if (response.getString("message").equals("Agency Remove Successfully")) {
                                    Toast.makeText(mcontext, "Agency Removed", Toast.LENGTH_SHORT).show();

                                    srList.remove(position);
                                    notifyDataSetChanged();
                                    progressDialog.dismiss();

                                } else {
                                    progressDialog.dismiss();
                                    Toast.makeText(mcontext, "Something went wrong", Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                Toast.makeText(mcontext, response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            progressDialog.dismiss();
                            Log.e("sjdfhskfsd", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialog.dismiss();
                        Log.e("sjdfhskfsd", anError.getMessage());
                    }
                });

    }


}