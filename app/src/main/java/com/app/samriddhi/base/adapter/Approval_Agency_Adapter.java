package com.app.samriddhi.base.adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;


import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.R;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.DailyPoActivity;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.PendingOrdersActivity;
import com.app.samriddhi.ui.model.ProposedAgencyModel;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.Globals;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Credentials;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Approval_Agency_Adapter extends RecyclerView.Adapter<Approval_Agency_Adapter.halfViewHolder> {

    private final ArrayList<ProposedAgencyModel> srList;
    private Context mcontext;
    private final Globals g = Globals.getInstance(mcontext);
    boolean posValue = false;
    int positionValue = 0;
    int row_index = -1;
    private final OnclickListener onclickListener;

    public Approval_Agency_Adapter(Context context, ArrayList<ProposedAgencyModel> srList, OnclickListener onclickListener) {
        this.mcontext = context;
        this.srList = srList;

        this.onclickListener = onclickListener;
    }

    @NonNull
    @Override
    public halfViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.agency_view_layout_agent_adapter, parent, false);

        return new halfViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final halfViewHolder holder, final int position) {
        ProposedAgencyModel data = srList.get(position);

        Log.e("safaaasaffafa", data.getId());
       // Log.e("safaaasaffafa", data.getAg_detail_id());
        // Log.e("safaaasaffafa",data.getAg_req_id());



        int mainCopy = Integer.parseInt(data.getMain_copies());
        holder.agencyName.setText(data.getAgency_name());
        int jjCopy = Integer.parseInt(data.getJj_copies());
        if (mainCopy > 0) {
            holder.mainLayout.setVisibility(View.VISIBLE);
        } else {
            holder.mainLayout.setVisibility(View.GONE);
        }

        if (jjCopy > 0) {
            holder.jjLayout.setVisibility(View.VISIBLE);
        } else {
            holder.jjLayout.setVisibility(View.GONE);
        }


        try {

            holder.tvJjPo.setText(data.getJj_copies()+"-"+"Copies");
            holder.tvMainPo.setText(data.getMain_copies()+"-"+"Copies");

            Log.e("dfgdfgdfg","Here");

            for (int i = 0; i < data.getAgencyRequestCIRReq_set().length(); i++) {
                Log.e("dfgdfgdfg",i+"");

                if (i==0) {
                    Log.e("dfgdfgdfg","main");
                    holder.tvCollect.setText(data.getAgencyRequestCIRReq_set().getJSONObject(0).getString("asd_collected"));
                    holder.tvASDPro.setText(data.getAgencyRequestCIRReq_set().getJSONObject(0).getString("asd_as_per_norms"));
                    holder.tvCmPer.setText(data.getAgencyRequestCIRReq_set().getJSONObject(0).getString("commission_per"));
                    holder.tvCmAmt.setText(data.getAgencyRequestCIRReq_set().getJSONObject(0).getString("commission_amt"));
                    holder.tvMainRemark.setText(data.getAgencyRequestCIRReq_set().getJSONObject(0).getString("creation_comments"));

                    double CmPer=Double.parseDouble(data.getAgencyRequestCIRReq_set().getJSONObject(0).getString("commission_per"));
                    double CmAMT=Double.parseDouble(data.getAgencyRequestCIRReq_set().getJSONObject(0).getString("commission_amt"));
                    if(CmAMT>CmPer){

                        holder.tvCmAmt.setTextColor(Color.parseColor("#EF2112"));
                    }
                    double asdPerNorms= Double.parseDouble(data.getAgencyRequestCIRReq_set().getJSONObject(0).getString("asd_as_per_norms"));
                    double asdCollect= Double.parseDouble(data.getAgencyRequestCIRReq_set().getJSONObject(0).getString("asd_collected"));
                    if(asdCollect<asdPerNorms){

                        holder.tvCollect.setTextColor(Color.parseColor("#EF2112"));
                    }
                }

                else if (i==1) {
                    Log.e("dfgdfgdfg","jj");
                    holder.jjtvAsdCollect.setText(data.getAgencyRequestCIRReq_set().getJSONObject(1).getString("asd_collected"));
                    holder.jjtvASDPro.setText(data.getAgencyRequestCIRReq_set().getJSONObject(1).getString("asd_as_per_norms"));
                    holder.jjtvCmPer.setText(data.getAgencyRequestCIRReq_set().getJSONObject(1).getString("commission_per"));
                    holder.jjtvCmAmt.setText(data.getAgencyRequestCIRReq_set().getJSONObject(1).getString("commission_amt"));
                    holder.tvJJRemark.setText(data.getAgencyRequestCIRReq_set().getJSONObject(1).getString("creation_comments"));
                    double CmPer=Double.parseDouble(data.getAgencyRequestCIRReq_set().getJSONObject(1).getString("commission_per"));
                    double CmAMT=Double.parseDouble(data.getAgencyRequestCIRReq_set().getJSONObject(1).getString("commission_amt"));
                    if(CmAMT>
                            CmPer){

                        holder.jjtvCmAmt.setTextColor(Color.parseColor("#EF2112"));
                    }
                    double asdPerNorms= Double.parseDouble(data.getAgencyRequestCIRReq_set().getJSONObject(1).getString("asd_as_per_norms"));
                    double asdCollect= Double.parseDouble(data.getAgencyRequestCIRReq_set().getJSONObject(1).getString("asd_collected"));
                    if(asdCollect<asdPerNorms){

                        holder.jjtvAsdCollect.setTextColor(Color.parseColor("#EF2112"));
                    }
                }
                else {

                }
            }






        } catch (Exception e) {
            Log.e("Catch-Block", e.getMessage());
        }


       /* if (data.getUOHApproval().equalsIgnoreCase("1")) {
            holder.UOHLayout.setVisibility(View.VISIBLE);
            holder.tvUOHRemark.setText(data.getUOHRemark());

        } else {
            holder.agencyName.setText(data.getAgency_name());
            holder.UOHLayout.setVisibility(View.GONE);
        }

        if (data.getSOHApproval().equalsIgnoreCase("1")) {
            holder.SohLayout.setVisibility(View.VISIBLE);
            holder.tvSohRemark.setText(data.getSOHRemark());
        } else {
            holder.SohLayout.setVisibility(View.GONE);
        }

        if (data.getHOApproval().equalsIgnoreCase("1")) {
            holder.OHlayout.setVisibility(View.VISIBLE);
            holder.tvHoRemark.setText(data.getHORemark());
        } else {
            holder.OHlayout.setVisibility(View.GONE);
        }*/


        if (data.getUOHApproval().equalsIgnoreCase("1") && data.getSOHApproval().equalsIgnoreCase("1") && data.getHOApproval().equalsIgnoreCase("1")) {

            String text = "<font size='5' color=#95989A>(Proposed by UOH)</font> <font size='5' color=#ffbb33>(Proposed by SOH)</font> <font size='5' color=#00c851>(Proposed by HO)</font>";
            holder.approveStr.setText(Html.fromHtml(text));

        } else if (data.getSOHApproval().equalsIgnoreCase("1") && data.getHOApproval().equalsIgnoreCase("1")) {
            String text = "<font size='5' color=#ffbb33>(Proposed by SOH)</font> <font size='5' color=#00c851>(Proposed by HO)</font>";
            holder.approveStr.setText(Html.fromHtml(text));

        } else if (data.getUOHApproval().equalsIgnoreCase("1") && data.getHOApproval().equalsIgnoreCase("1")) {
            String text = "<font size='5' color=#95989A>(Proposed by UOH)</font> <font size='5' color=#00c851>(Proposed by HO)</font>";
            holder.approveStr.setText(Html.fromHtml(text));

        } else if (data.getUOHApproval().equalsIgnoreCase("1") && data.getSOHApproval().equalsIgnoreCase("1")) {
            String text = "<font size='5' color=#95989A>(Proposed by UOH)</font> <font size='5' color=#ffbb33>(Proposed by SOH)</font>";
            holder.approveStr.setText(Html.fromHtml(text));

        } else if (data.getSOHApproval().equalsIgnoreCase("1")) {
            String text = "<font size='5' color=#ffbb33>(Proposed by SOH)</font>";
            holder.approveStr.setText(Html.fromHtml(text));

        } else if (data.getUOHApproval().equalsIgnoreCase("1")) {
            String text = "<font  size='5' color=#95989A>(Proposed by UOH)</font>";
            holder.approveStr.setText(Html.fromHtml(text));

        } else if (data.getHOApproval().equalsIgnoreCase("1")) {
            String text = "<font size='5' color=#00c851>(Proposed by HO)</font>";
            holder.approveStr.setText(Html.fromHtml(text));

        }

        holder.checkboxImag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for (int l = 0; l < srList.size(); l++) {

                    //    data.setStatusCheck(true);
                    srList.get(l).setStatusCheck(l == position);
                }
                notifyDataSetChanged();

                onclickListener.onItemClick(srList.get(position), srList);
//                        if(data.isStatusCheck()){
//                            data.setStatusCheck(false);
//                        }else{
//                            data.setStatusCheck(true);
//                        }
            }
        });

        holder.checkboxImag.setOnClickListener(v -> {

            /* holder.clickLayout.setOnClickListener(v -> {*/

            Log.e("sfsdfsfs", srList.size() + "");


            for (int l = 0; l < srList.size(); l++) {

                //    data.setStatusCheck(true);
                srList.get(l).setStatusCheck(l == position);
            }
            notifyDataSetChanged();

            onclickListener.onItemClick(srList.get(position), srList);
//                        if(data.isStatusCheck()){
//                            data.setStatusCheck(false);
//                        }else{
//                            data.setStatusCheck(true);
//                        }


        });

        holder.editFrm.setOnClickListener(v -> {
            onclickListener.onEditClick(data);
        });

        holder.linDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder;
                builder = new AlertDialog.Builder(mcontext);
                builder.setMessage("Are you sure you want to delete this?")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                            public void onClick(final DialogInterface dialog, int which) {
                                 Log.e("safaaasaffafa",data.getId());
                                 Delete(data.getId(),"0",position);
                                 dialog.dismiss();

                            }
                        })
                        .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                dialog.dismiss();

                            }
                        })
                        .setIcon(R.drawable.ic_action_delete)
                        .show();

            }
        });


      /*  holder.checkboxImag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                row_index = position;
                notifyDataSetChanged();


            }
        });

        if (row_index == position) {

            holder.checkboxImag.setChecked(true);
        }
        else {

            holder.checkboxImag.setChecked(false);
        }*/

       if (data.isStatusCheck()) {
           holder.checkboxImag.setImageResource(R.drawable.tick);
           srList.get(position).setStatusCheck(true);
           onclickListener.onItemClick(srList.get(position), srList);
       }
       else {
            holder.checkboxImag.setImageResource(R.drawable.notick);
       }


        if(PreferenceManager.getPrefUserType(mcontext).equals("UH")){

            if(data.getUOHApproval().equals("1")){

              //  holder.linDelete.setVisibility(View.GONE);
                holder.editFrm.setVisibility(View.GONE);
            }
            else {
               // holder.linDelete.setVisibility(View.VISIBLE);
                holder.editFrm.setVisibility(View.VISIBLE);
            }


        }

       else if(PreferenceManager.getPrefUserType(mcontext).equals("SH")){

            if(data.getSOHApproval().equals("1")){

              //  holder.linDelete.setVisibility(View.GONE);
                holder.editFrm.setVisibility(View.GONE);
            }
            else {
              //  holder.linDelete.setVisibility(View.VISIBLE);
                holder.editFrm.setVisibility(View.VISIBLE);
            }

        }
       else {
            if(data.getHOApproval().equals("1")){
                holder.editFrm.setVisibility(View.GONE);
            }
            else {
                holder.editFrm.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return srList.size();
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }


    public class halfViewHolder extends RecyclerView.ViewHolder {
        public TextView agencyName, jjtvASDPro, jjtvAsdCollect, jjtvCmPer, jjtvCmAmt, approveStr, tvJjPo, tvMainPo, tvASDPro, tvCollect, tvCmPer, tvCmAmt, tvUOHRemark, tvSohRemark, tvHoRemark;
        public LinearLayout clickLayout, editFrm, mainLayout, jjLayout, UOHLayout, SohLayout, OHlayout, linDelete;
        //  public RadioButton checkboxImag;
        public ImageView checkboxImag;
        TextView tvMainRemark;
        TextView tvJJRemark;

        public halfViewHolder(View view) {
            super(view);
            tvCmAmt = view.findViewById(R.id.tvCmAmt);
            tvMainPo = view.findViewById(R.id.tvMainPo);
            tvJjPo = view.findViewById(R.id.tvJjPo);
            approveStr = view.findViewById(R.id.approveStr);
            UOHLayout = view.findViewById(R.id.UOHLayout);
            SohLayout = view.findViewById(R.id.SohLayout);
            OHlayout = view.findViewById(R.id.OHlayout);
            jjtvCmAmt = view.findViewById(R.id.jjtvCmAmt);
            jjtvASDPro = view.findViewById(R.id.jjtvASDPro);
            jjtvAsdCollect = view.findViewById(R.id.jjtvAsdCollect);
            jjtvCmPer = view.findViewById(R.id.jjtvCmPer);
            tvMainRemark = view.findViewById(R.id.tvMainRemark);
            tvJJRemark = view.findViewById(R.id.tvJJRemark);


            tvHoRemark = view.findViewById(R.id.tvHoRemark);
            tvCollect = view.findViewById(R.id.tvCollect);
            tvASDPro = view.findViewById(R.id.tvASDPro);
            tvSohRemark = view.findViewById(R.id.tvSohRemark);
            tvUOHRemark = view.findViewById(R.id.tvUOHRemark);

            editFrm = view.findViewById(R.id.editFrm);
            mainLayout = view.findViewById(R.id.mainLayout);
            jjLayout = view.findViewById(R.id.jjLayout);
            tvCmPer = view.findViewById(R.id.tvCmPer);
            agencyName = view.findViewById(R.id.agencyName);
            checkboxImag = view.findViewById(R.id.checkboxImag);
            clickLayout = view.findViewById(R.id.clickLayout);
            linDelete = view.findViewById(R.id.linDelete);
        }
    }

    public interface OnclickListener {
        void onItemClick(ProposedAgencyModel proposedAgencyModel, ArrayList<ProposedAgencyModel> srList);

        void onEditClick(ProposedAgencyModel proposedAgencyModel);

        void onDeleteClick(ProposedAgencyModel proposedAgencyModel);
    }

    public void Delete(String strId, String status, int position) {
        ProgressDialog progressDialog=new ProgressDialog(mcontext);
        progressDialog.setMessage("Please wait..");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(true);
        progressDialog.show();
        http://103.96.220.236/agency/changeAgStatus/518/0
        AndroidNetworking.get(Constant.BASE_PORTAL_URL+"agency/changeAgStatus/"+strId+"/"+status)
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            Log.e("sfsdfsfsf", response.toString());

                            if(response.has("message")){
                                if(response.getString("message").equals("Agency Remove Successfully")){

                                    srList.remove(position);
                                    notifyDataSetChanged();
                                    progressDialog.dismiss();
                                    Toast.makeText(mcontext, "Agency Removed", Toast.LENGTH_SHORT).show();

                                }
                                else {
                                    progressDialog.dismiss();
                                    Toast.makeText(mcontext, "Something went wrong", Toast.LENGTH_SHORT).show();
                                }

                            }
                            else {
                                Toast.makeText(mcontext, response.getString("reply"), Toast.LENGTH_SHORT).show();

                            }

                        } catch (Exception ex) {
                            progressDialog.dismiss();
                            Log.e("sjdfhskfsd", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialog.dismiss();
                        Log.e("sjdfhskfsd", anError.getMessage());
                    }
                });

    }

}
