package com.app.samriddhi.base.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;
import com.app.samriddhi.ui.model.RootModel;

import java.util.ArrayList;

public class Agency_new_perposal_Adapter extends RecyclerView.Adapter<Agency_new_perposal_Adapter.halfViewHolder> {

    private final ArrayList<RootModel> srList;
    private final Context mcontext;



    public Agency_new_perposal_Adapter(Context context, ArrayList<RootModel> srList) {
        this.mcontext=context;
        this.srList=srList;
    }

    @NonNull
    @Override
    public halfViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.agency_view_adapter_layout, parent, false);

        return new halfViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final halfViewHolder holder, final int position) {


    }



    @Override
    public int getItemCount() {
        return srList.size();
    }

    public class halfViewHolder extends RecyclerView.ViewHolder {


        public halfViewHolder(View view) {
            super(view);


        }
    }



}
