package com.app.samriddhi.base.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;
import com.app.samriddhi.ui.model.NewsPaperModel;
import com.google.android.material.card.MaterialCardView;

import java.util.ArrayList;
import java.util.List;

public class NewsPaperAdapterList extends RecyclerView.Adapter<NewsPaperAdapterList.MyViewHolder> implements Filterable {

    private ArrayList<NewsPaperModel> menuClassList;
    private final OnMeneuClickListnser onMenuListClicklistener;
    static int  getPosition;
    private int row_index;
    Context mContext;
    private final List<NewsPaperModel> searchArray;
    public NewsPaperAdapterList(ArrayList<NewsPaperModel> menuClassList, OnMeneuClickListnser onLiveTestClickListener, Context context) {
        this.menuClassList = menuClassList;
        this.onMenuListClicklistener = onLiveTestClickListener;
        this.searchArray=new ArrayList<>();
        this.searchArray.addAll(menuClassList);
        this.mContext=context;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = null;


    itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.checkbox_layout_list, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        NewsPaperModel dataValue=menuClassList.get(position);

        holder.nameText.setText(dataValue.getPaperName());



        holder.openpanel.setOnClickListener(v->{
            dataValue.setStatus(!dataValue.isStatus());

            notifyDataSetChanged();
            onMenuListClicklistener.onCheckValue(menuClassList);
        });

        if(dataValue.isStatus()){
            holder.checkboxImag.setImageResource(R.drawable.tick);

        }else{
            holder.checkboxImag.setImageResource(R.drawable.notick);
        }
        //holder.number.setText(ticket.getTicketNumber());


//        holder.openpanel.setOnClickListener(v -> {
//            onMenuListClicklistener.onOptionClick(menuClassList.get(position));
//           // notifyDataSetChanged();
//        });

      
      }
    void add(NewsPaperModel ticket) {
        this.menuClassList.add(ticket);
    }
   public void addAll(List<NewsPaperModel> tickets) {
        this.menuClassList.addAll(tickets);
    }

    @Override
    public int getItemCount() {
        return menuClassList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
          TextView nameText;
           MaterialCardView openpanel;
           ImageView checkboxImag;
        MyViewHolder(View view) {
            super(view);
            checkboxImag=view.findViewById(R.id.checkboxImag);

          openpanel=view.findViewById(R.id.openpanel);
            nameText=view.findViewById(R.id.tvNewsTittle);



        }
    }
    public interface OnMeneuClickListnser{
        void onCheckValue(ArrayList<NewsPaperModel> tickets);
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String filterString = charSequence.toString().toLowerCase();
                //arraylist1.clear();
                FilterResults results = new FilterResults();
                int count = searchArray.size();
                final List<NewsPaperModel> list = new ArrayList<>();

                NewsPaperModel filterableString;

                for (int i = 0; i < count; i++) {
                    filterableString = searchArray.get(i);
                    if (searchArray.get(i).getPaperName().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                    }
                }

                results.values = list;
                results.count = list.size();

                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                menuClassList = (ArrayList<NewsPaperModel>) filterResults.values;
                notifyDataSetChanged();
            }
        };


    }
}

