package com.app.samriddhi.base.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BillDataModal {
    @SerializedName("Bill_Period")
    @Expose
    public String bill_Period;

    @SerializedName("Bill_No")
    @Expose
    public String Bill_No;

    @SerializedName("BP_Code")
    @Expose
    public String BP_Code;

    @SerializedName("Confirm_By")
    @Expose
    public String Confirm_By;

    @SerializedName("Bill_copies")
    @Expose
    public String Bill_copies;

    @SerializedName("Bill_Amount")
    @Expose
    public String Bill_Amount;

    @SerializedName("Confirm_Status")
    @Expose
    public String Confirm_Status;

    @SerializedName("monat")
    @Expose
    public String monat;

    @SerializedName("gjahr")
    @Expose
    public String gjahr;


    public String getMonat() {
        return monat;
    }

    public void setMonat(String monat) {
        this.monat = monat;
    }

    public String getGjahr() {
        return gjahr;
    }

    public void setGjahr(String gjahr) {
        this.gjahr = gjahr;
    }

    public String getBill_Period() {
        return bill_Period;
    }

    public void setBill_Period(String bill_Period) {
        this.bill_Period = bill_Period;
    }

    public String getBill_No() {
        return Bill_No;
    }

    public void setBill_No(String bill_No) {
        Bill_No = bill_No;
    }

    public String getBP_Code() {
        return BP_Code;
    }

    public void setBP_Code(String BP_Code) {
        this.BP_Code = BP_Code;
    }

    public String getConfirm_By() {
        return Confirm_By;
    }

    public void setConfirm_By(String confirm_By) {
        Confirm_By = confirm_By;
    }

    public String getBill_copies() {
        return Bill_copies;
    }

    public void setBill_copies(String bill_copies) {
        Bill_copies = bill_copies;
    }

    public String getBill_Amount() {
        return Bill_Amount;
    }

    public void setBill_Amount(String bill_Amount) {
        Bill_Amount = bill_Amount;
    }

    public String getConfirm_Status() {
        return Confirm_Status;
    }

    public void setConfirm_Status(String confirm_Status) {
        Confirm_Status = confirm_Status;
    }
}
