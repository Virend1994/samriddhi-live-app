package com.app.samriddhi;


import android.content.Context;
import android.util.Log;

import com.app.samriddhi.api.ApiSapService;
import com.app.samriddhi.api.ApiService;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.util.AppBackgroundMonitor;
import com.app.samriddhi.util.AppEnvironment;
import com.app.samriddhi.util.Constant;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jakewharton.threetenabp.AndroidThreeTen;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.Cache;
import okhttp3.ConnectionPool;
import okhttp3.Credentials;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by Admin on 2/21/2018.
 */

public class SamriddhiApplication extends android.app.Application {
    static SamriddhiApplication mInstance;
    AppEnvironment appEnvironment;
    Gson gson;
    private ApiService apiService;

    public static SamriddhiApplication getmInstance() {
        return mInstance;
    }

    public static void setmInstance(SamriddhiApplication mInstance) {
        SamriddhiApplication.mInstance = mInstance;
    }

    public AppEnvironment getAppEnvironment() {
        return appEnvironment;
    }
    public void setAppEnvironment(AppEnvironment appEnvironment) {
        this.appEnvironment = appEnvironment;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        AndroidThreeTen.init(this);
        initApiService();
        appEnvironment = AppEnvironment.PRODUCTION;
        AppBackgroundMonitor.shared().setEnabled(true);
        //   PreferenceManager.clearURL(mInstance);

    }

    /**
     * initialize the Retrofit Api's
     */
    private void initApiService() {
        Log.d("FCM Token=", "initApiService: " + FirebaseInstanceId.getInstance().getToken());
        gson = new GsonBuilder().create();
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);


        File httpCacheDirectory = new File(getCacheDir(), "offlineCache");
        Cache cache = new Cache(httpCacheDirectory, Constant.CACHE_SIZE);


        OkHttpClient okHttpClient = UnsafeOkHttpClient.getUnsafeOkHttpClient();


       /* OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.cache(cache).
                connectTimeout(5, TimeUnit.MINUTES)
                .writeTimeout(5, TimeUnit.MINUTES)
                .readTimeout(5, TimeUnit.MINUTES)
                .connectionPool(new ConnectionPool(0, 5 * 60 * 1000, TimeUnit.SECONDS));
        if (BuildConfig.DEBUG) {
            httpClient.addInterceptor(httpLoggingInterceptor);
        }
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder().addHeader("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P)).build();
                return chain.proceed(request);
            }
        });*/

//        //Init retrofit
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.BASE_PORTAL_URL)
             //   .baseUrl(PreferenceManager.getBaseURL(mInstance))
                .client(okHttpClient)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        apiService = retrofit.create(ApiService.class);
    }

    public ApiService getApiService() {
        return apiService;
    }
}
