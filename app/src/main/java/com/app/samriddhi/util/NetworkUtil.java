package com.app.samriddhi.util;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.util.ArrayList;


public class NetworkUtil extends BroadcastReceiver {

    public static NetworkUtil networkUtil;
    static INetworkUtil _iNetworkUtilListener;
    static ArrayList<INetworkUtil> iNetworkUtilArrayList = new ArrayList<>();

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        // should check null because in air plan mode it will be null
        return netInfo != null && netInfo.isConnected();
    }

    public static void setNetworkListener(INetworkUtil iNetworkUtil) {
        _iNetworkUtilListener = iNetworkUtil;
    }

    public static void addNetworkListener(INetworkUtil iNetworkUtil) {
        if (!iNetworkUtilArrayList.contains(iNetworkUtil)) {
            iNetworkUtilArrayList.add(iNetworkUtil);
        }

    }

    public static NetworkUtil getInstance() {
        if (networkUtil == null)
            networkUtil = new NetworkUtil();
        return networkUtil;
    }

    public void removeNetworkListener(INetworkUtil iNetworkUtil) {
        iNetworkUtilArrayList.remove(iNetworkUtil);

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        for (int i = 0; i < iNetworkUtilArrayList.size(); i++) {
            iNetworkUtilArrayList.get(i).onNetworkChange(isOnline(context));
        }
    }

    public void onNetworkChange(INetworkUtil _iNetworkUtilListener) {
        NetworkUtil._iNetworkUtilListener = _iNetworkUtilListener;
    }

    public interface INetworkUtil {
        void onNetworkChange(boolean available);
    }

}
