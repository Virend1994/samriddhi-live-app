package com.app.samriddhi.util;

import android.content.Context;
import android.content.DialogInterface;
import android.widget.Button;
import android.widget.LinearLayout;
import androidx.appcompat.app.AlertDialog;

import com.app.samriddhi.R;

public class AlertUtility {

    public static AlertDialog alert;


    public static void showAlert(Context context, String message, DialogInterface.OnClickListener okListner) {
        if (alert != null && alert.isShowing())
            return;
        alert = new AlertDialog.Builder(context, R.style.AppTheme_AlertDialog)
                .setMessage(message).setTitle(context.getResources().getString(R.string.app_name))
                .setCancelable(false)
                .setPositiveButton(context.getResources().getString(R.string.str_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alert.dismiss();
                    }
                })
                .setNegativeButton(context.getResources().getString(R.string.ok), okListner)
                .create();
        alert.show();

        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(context.getResources().getColor(R.color.white_color));
        nbutton.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        pbutton.setTextColor(context.getResources().getColor(R.color.white_color));
        pbutton.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(10, 10, 10, 10);
        nbutton.setLayoutParams(params);
        pbutton.setLayoutParams(params);

    }


    public static void showAlertWithListener(Context context, String message, DialogInterface.OnClickListener okListner) {
        if (alert != null && alert.isShowing())
            return;
        alert = new AlertDialog.Builder(context, R.style.AppTheme_AlertDialog)
                .setMessage(message).setTitle(context.getResources().getString(R.string.app_name))
                .setCancelable(false)
                .setNegativeButton(context.getResources().getString(R.string.ok), okListner)
                .create();
        alert.show();

        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        nbutton.setTextColor(context.getResources().getColor(R.color.white_color));
        nbutton.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));

        pbutton.setTextColor(context.getResources().getColor(R.color.white_color));
        pbutton.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));

    }

    public static void showAlertDialog(Context context, String message) {
        if (alert != null && alert.isShowing())
            return;
        alert = new AlertDialog.Builder(context, R.style.AppTheme_AlertDialog)
                .setMessage(message)/*.setTitle(context.getResources().getString(R.string.app_name))*/
                .setCancelable(false)
                .setPositiveButton(context.getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alert.dismiss();
                    }
                })

                .create();
        alert.show();

        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        nbutton.setTextColor(context.getResources().getColor(R.color.white_color));
        nbutton.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));


        pbutton.setTextColor(context.getResources().getColor(R.color.white_color));
        pbutton.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(10, 10, 10, 10);
        nbutton.setLayoutParams(params);
        pbutton.setLayoutParams(params);


    }

    public static void showFormAlert(Context context, String message, DialogInterface.OnClickListener okListner) {
        if (alert != null && alert.isShowing())
            return;
        alert = new AlertDialog.Builder(context, R.style.AppTheme_AlertDialog)
                .setMessage(message)
                .setCancelable(false)

                .setNegativeButton(context.getResources().getString(R.string.ok), okListner)
                .create();
        alert.show();

        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(context.getResources().getColor(R.color.white_color));
        nbutton.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));


        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(10, 10, 10, 10);
        nbutton.setLayoutParams(params);


    }


    public static void showAlertDialogWithOpenOption(Context context, String message, DialogInterface.OnClickListener okListner) {
        if (alert != null && alert.isShowing())
            return;
        alert = new AlertDialog.Builder(context, R.style.AppTheme_AlertDialog)
                .setMessage(message).setTitle(context.getResources().getString(R.string.app_name))
                .setCancelable(false)
                .setNegativeButton(context.getResources().getString(R.string.str_open), okListner)
                .create();
        alert.show();

        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(context.getResources().getColor(R.color.white_color));
        nbutton.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(10, 10, 10, 10);
        nbutton.setLayoutParams(params);

    }


    public static void showAlertPermission(Context context, String message, DialogInterface.OnClickListener okListner) {
        if (alert != null && alert.isShowing())
            return;
        alert = new AlertDialog.Builder(context, R.style.AppTheme_AlertDialog)
                .setMessage(message).setTitle(context.getResources().getString(R.string.app_name))
                .setCancelable(false)
                .setPositiveButton(context.getResources().getString(R.string.ok), okListner)
                .setNegativeButton(context.getResources().getString(R.string.str_cancel), okListner)
                .create();
        alert.show();

        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        nbutton.setTextColor(context.getResources().getColor(R.color.white_color));
        nbutton.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(10, 10, 10, 10);
        nbutton.setLayoutParams(params);

    }

    public static void showAlertFileOpen(Context context, String message, DialogInterface.OnClickListener okListner) {
        if (alert != null && alert.isShowing())
            return;
        alert = new AlertDialog.Builder(context, R.style.AppTheme_AlertDialog)
                .setMessage(message).setTitle(context.getResources().getString(R.string.app_name))
                .setCancelable(false)
                .setNegativeButton(context.getResources().getString(R.string.str_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alert.dismiss();
                    }
                })
                .setPositiveButton(context.getResources().getString(R.string.str_open), okListner)
                .create();
        alert.show();

        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
        nbutton.setTextColor(context.getResources().getColor(R.color.white_color));
        nbutton.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));


        pbutton.setTextColor(context.getResources().getColor(R.color.white_color));
        pbutton.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(10, 10, 10, 10);
        nbutton.setLayoutParams(params);
        pbutton.setLayoutParams(params);


    }


    public static void showAlertWithListeners(Context context, String message, DialogInterface.OnClickListener okListner,DialogInterface.OnClickListener onCancelListener) {
        if (alert != null && alert.isShowing())
            return;
        alert = new AlertDialog.Builder(context, R.style.AppTheme_AlertDialog)
                .setMessage(message).setTitle(context.getResources().getString(R.string.app_name))
                .setCancelable(false)
                .setPositiveButton(context.getResources().getString(R.string.str_cancel), onCancelListener)
                .setNegativeButton(context.getResources().getString(R.string.ok), okListner)
                .create();
        alert.show();

        Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
        Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);

        nbutton.setTextColor(context.getResources().getColor(R.color.white_color));
        nbutton.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));


        pbutton.setTextColor(context.getResources().getColor(R.color.white_color));
        pbutton.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(10, 10, 10, 10);
        nbutton.setLayoutParams(params);
        pbutton.setLayoutParams(params);

    }

}
