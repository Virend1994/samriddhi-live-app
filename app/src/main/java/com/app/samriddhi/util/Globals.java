package com.app.samriddhi.util;


import android.annotation.SuppressLint;
import android.content.Context;

import com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule.SelfEditionModals;
import com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule.SubAgentModal;
import com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule.SunAgentAdapter;
import com.app.samriddhi.ui.model.AgencyChildSet;
import com.app.samriddhi.ui.model.AgencyRequestCIRReqSetModel;
import com.app.samriddhi.ui.model.AgencyRequestKYBPSetModel;
import com.app.samriddhi.ui.model.AgencyRequestSurveySetModel;
import com.app.samriddhi.ui.model.CirReqChequeSetModel;
import com.app.samriddhi.ui.model.CirReqDroppingSetModelAdpter;
import com.app.samriddhi.ui.model.DropDownModel;
import com.app.samriddhi.ui.model.EditionCRModel;
import com.app.samriddhi.ui.model.AgencyRequestReasonSetModel;
import com.app.samriddhi.ui.model.NewsPaperModel;
import com.app.samriddhi.ui.model.ReasonAdapterModel;
import com.app.samriddhi.ui.model.TargetModelClass;
import com.app.samriddhi.ui.model.VillageModel;


import java.util.ArrayList;
import java.util.HashMap;

@SuppressLint("TrulyRandom")
public class Globals
{
    private static Globals instance;
    public int count;
    public String selected_newspaper="";
    public String[] multi_selected_id;

    public Globals(Context context) {

    }

    public static synchronized Globals getInstance(Context context) {
        if (instance == null) {
            return instance = new Globals(context);
        } else
            return instance;
    }

    public ArrayList<ReasonAdapterModel> reasonAdapterList= new ArrayList<>();

    public ArrayList<TargetModelClass> survet_form_rec_datalist = new ArrayList<>();
    public ArrayList<VillageModel> village_rec_datalist = new ArrayList<>();
    public ArrayList<AgencyChildSet> child_rec_list = new ArrayList<>();
    public ArrayList<CirReqDroppingSetModelAdpter> droppoint_rec_datalist = new ArrayList<>();
    public ArrayList<EditionCRModel> editiondata_rec_datalist = new ArrayList<>();
    public ArrayList<SelfEditionModals> editiondata_self_rec_datalist = new ArrayList<>();
    public ArrayList<SubAgentModal> editiondata_subAgent_rec_datalist = new ArrayList<>();


    public ArrayList<EditionCRModel> route_editiondata_rec_datalist = new ArrayList<>();

    public ArrayList<CirReqChequeSetModel> cheque_rec_datalist = new ArrayList<>();
    public ArrayList<DropDownModel> SelectedEditionMasterArray=new ArrayList<>();
    public ArrayList<DropDownModel> localEditionMaster=new ArrayList<>();
    public ArrayList<HashMap> intensification_rec_list = new ArrayList<HashMap>();
    public ArrayList<HashMap> KybpdataList = new ArrayList<HashMap>();
    public ArrayList<HashMap> SurveyDataList = new ArrayList<HashMap>();
    public ArrayList<HashMap> Cir_reqDataList = new ArrayList<HashMap>();
    public ArrayList<HashMap> newAgencyCreationList = new ArrayList<HashMap>();
    public ArrayList<HashMap> newAgencyCreationIDList = new ArrayList<HashMap>();
    public ArrayList<NewsPaperModel> newsPaperNameMasterArray=new ArrayList<>();
    public ArrayList<DropDownModel> SalesDistArray=new ArrayList<>();
    public ArrayList<DropDownModel> educationMasterArray=new ArrayList<>();
    public ArrayList<DropDownModel> getMaritialArray=new ArrayList<>();
    public ArrayList<DropDownModel> designationMasterArray=new ArrayList<>();
    public ArrayList<DropDownModel> departmentMasterArray=new ArrayList<>();


    public ArrayList<AgencyRequestReasonSetModel> AgencyRequestReasonSetModel=new ArrayList<>();
    public ArrayList<AgencyRequestCIRReqSetModel> agencyRequestCIRReqSetModelList=new ArrayList<>();
    public ArrayList<AgencyRequestKYBPSetModel> agencyRequestKYBPSetList=new ArrayList<>();
    public ArrayList<AgencyRequestSurveySetModel> agencyRequestSurveySetModels=new ArrayList<>();
    public ArrayList<String> call_tyoe_code = new ArrayList<String>();
    public String Meeting_type_code="";
    public String[] splitedArr;

    public String radioBtnType="";
    public String state_main="";
    public String unit_main="";
    public String mainSaleGroup="";
    public String uniteCode="";

    public String unitId="";
    public String cirType="";
    public String mainSaleOffice="";
    public String jjSaleOffice="";


    public String mainSaleOfficeCopy="";
    public String jjSaleOfficeCopy="";

    public String form_id="";

    public String agentId="";
    public String requestId="";
    public String agencyNameStr="";
    public String agentNameStr="";
    public String agentMobileStr="";
    public int KYBPStatus=0;
    public int CIRStatus=0;
    public int ActionType=0;
    public int JJCIRStatus=0;
    public int SURVEYStatus=0;
    public double mainNoOfCopy=0;
    public double mainCirNoOfCopy=0;
    public int agentAge=0;
    public double jjCirNoOfCopy=0;
    public String reasonTypeName="";
    public String mainAgencyName="";
    public String Survey_Form="Survey_Form";

}

