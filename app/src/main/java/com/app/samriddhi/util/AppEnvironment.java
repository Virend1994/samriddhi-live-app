package com.app.samriddhi.util;


/**
 * Created by Radha Soni on 30/04/2020.
 */

public enum AppEnvironment {


    //Auth Header:- t+CSdxOJ4FyCjnuKp6TUmOj5y6Q23HRKIOCd+rsl7v0=
    SANDBOX {
        @Override
        public String merchant_Key() {
            return "hjBU1n55";
        }

        @Override
        public String merchant_ID() {
            return "7031236";
        }

        @Override
        public String furl() {
            return "https://www.payumoney.com/mobileapp/payumoney/failure.php";
        }

        @Override
        public String surl() {
            return "https://www.payumoney.com/mobileapp/payumoney/success.php";
        }

        @Override
        public String salt() {
            return "gowWUS93Jz";
        }

        @Override
        public boolean debug() {
            return true;
        }
    },
    PRODUCTION {
        @Override
        public String merchant_Key() {
            return "hjBU1n55";
        }

        @Override
        public String merchant_ID() {
            return "7031236";
        }

        @Override
        public String furl() {
            return "https://www.payumoney.com/mobileapp/payumoney/failure.php";
        }

        @Override
        public String surl() {
            return "https://www.payumoney.com/mobileapp/payumoney/success.php";
        }

        @Override
        public String salt() {
            return "gowWUS93Jz";
        }

        @Override
        public boolean debug() {
            return false;
        }
    };

    public abstract String merchant_Key();

    public abstract String merchant_ID();

    public abstract String furl();

    public abstract String surl();

    public abstract String salt();

    public abstract boolean debug();


}
