package com.app.samriddhi.util;

/*
 * Created by kipl104 on 04-Apr-17.
 */

public interface Constant {

    //Sap portal APi address
    // public static final String BASE_SAP_URL = "http://10.2.17.28:8001/sap/opu/odata/SAP/";

    //Portal API AddressL
    //public static final String BASE_PORTAL_URL = "http://dbadvt.com:8082/";
    // public static final String BASE_PORTAL_URL = "http://103.96.220.236/"; //test
    // public static final String BASE_PORTAL_URL = "https://ed3ac271b2e3.ngrok.io/"; //test

    // public static final String BASE_PORTAL_URL = "http://10.51.144.45:8000/"; //test
    public static final String BASE_PORTAL_URL = "http://dev.dbsamriddhi.in/"; //test
    public static final String Image_Url = "http://dev.dbsamriddhi.in/media/"; //test
    public static final String BASE_URL_Grievance = "http://dev.dbsamriddhi.com/"; //grievance production

   //  public static final String BASE_PORTAL_URL = "http://live.dbsamriddhi.in/"; //Production
    // public static final String Image_Url = "http://live.dbsamriddhi.in/media/"; //Production
    // public static final String BASE_URL_Grievance = "http://live_feedback.dbsamriddhi.com/"; //grievance production


    //  public static final String BASE_URL_Grievance = "http://feedback.dbsamriddhi.com/"; //grievance test
    // public static final String BASE_PORTAL_URL = "https://dbsamriddhi.com/"; /*Live*/
    public static final String SERVER_TYPE = "PRD";

    public static final String PORTAL_USER_NAME = "mappapi";


    public static final String PORTAL_P = "Asdf1245";


    public static final String KEY_USER_ID = "_id";
    public static final String PORTAL_USER_NAME_new = "mappapi";
    public static final String PORTAL_P_new = "Asdf1245";
    //Date time constants ex: yyyy-MM-dd HH:mm:ss
    public static final String DATE_FORMAT_MM_DD_YYYY = "MM-dd-yyyy";
    public static final String DATE_FORMAT_DD_MMM_YYYY = "dd MMM yyyy";
    public static final String DATE_FORMAT_MM_DD_YYYY_HH_MM = "MM-dd-yyyy hh:mm";
    public static final String DATE_FORMAT_MM_DD_YYYY_HH_MM_A = "MM-dd-yyyy hh:mm a";
    public static final String DATE_FORMAT_DD_MMM_YYYY_HH_MM_A = "dd MMM yyyy hh:mm a";
    public static final String DATE_FORMAT_HH_MM = "HH:MM";
    public static final String DATE_FORMAT_HH_MM_A = "hh:mm a";
    public static final String DATE_FORMAT_DEFAULT = DATE_FORMAT_MM_DD_YYYY;

    public static String SCREEN_NAME = "screenName";
    public static String DashBoardBill = "DashboardBill";
    public static String DashBoardOutstanding = "DashBoardOutstanding";
    public static String DashBoardNoOfCopies = "DashBoardNoOfCopies";
    //  public static String INVOICE_PDF_URL = "http://dbadvt.com:8082/GETBillPDF/"; test
    //public static String INVOICE_PDF_URL = "http://103.96.220.236/GETBillPDF/";// test
    public static String INVOICE_PDF_URL = "http://live.dbsamriddhi.in/GETBillPDF/";// test
    // public static String INVOICE_PDF_URL = "https://dbsamriddhi.com//GETBillPDF/";
    public static String DashBoardGrievance = "DashBoardGrievance";

    public static String DIR_INVOICE = "InvoiceData";
    public static String DIR_LEDGER = "LedgerData";
    public static String DIR_COPIES = "CopiesData";

    public static String DrawerProfile = "DrawerProfile";
    public static String OPENCOMPLAINT = "open";
    public static String CLOSECOMPLAINT = "close";
    public static String NEWCOMPLAINT = "new";
    public static String ITEMONLYONECHILD = "oneChild";
    public static final long CACHE_SIZE = 10 * 1024 * 1024; //10 MB
}
