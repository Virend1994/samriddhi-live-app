package com.app.samriddhi.util;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.app.samriddhi.R;
import com.app.samriddhi.ui.model.ExcelDataModel;
import com.app.samriddhi.ui.model.NewLedgerModel;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;


public class SystemUtility extends BroadcastReceiver {
    private static SystemUtility systemUtility;

    public static SystemUtility getInstance() {
        if (systemUtility == null)
            systemUtility = new SystemUtility();

        return systemUtility;
    }

    public static boolean appInstalledOrNot(Context context, String packageName) {
        if (context == null)
            throw new NullPointerException("Context must be provided");
        PackageManager pm = context.getPackageManager();
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public static String toTitleCase(String str) {
        if (str == null) {
            return null;
        }
        boolean space = true;
        StringBuilder builder = new StringBuilder(str);
        final int len = builder.length();

        for (int i = 0; i < len; ++i) {
            char c = builder.charAt(i);
            if (space) {
                if (!Character.isWhitespace(c)) {
                    // Convert to title case and switch out of whitespace mode.
                    builder.setCharAt(i, Character.toTitleCase(c));
                    space = false;
                }
            } else if (Character.isWhitespace(c)) {
                space = true;
            } else {
                builder.setCharAt(i, Character.toLowerCase(c));
            }
        }
        return builder.toString();
    }

    public static Bitmap drawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        try {
            if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
                bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
            } else {
                bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
            }

            Canvas canvas = new Canvas(bitmap);
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
            drawable.draw(canvas);
        } catch (Exception e) {
            e.printStackTrace();
            bitmap = null;
        }
        return bitmap;
    }

    public static Bitmap getCroppedBitmap(Bitmap bitmap) {
        int smallestSize = bitmap.getWidth() < bitmap.getHeight() ? bitmap.getWidth() : bitmap.getHeight();
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        // canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
        canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
                smallestSize / 2, paint);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);
        //Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
        //return _bmp;
        return output;
    }

    public static String getTempMediaDirectory(Context context) {
        String state = Environment.getExternalStorageState();
        File dir = null;
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            dir = context.getExternalCacheDir();
        } else {
            dir = context.getCacheDir();
        }

        if (dir != null && !dir.exists()) {
            dir.mkdirs();
        }
        if (dir.exists() && dir.isDirectory()) {
            return dir.getAbsolutePath();
        }
        return null;
    }

    public static void clearTempDirectory(Context context) {
        try {
            String dir = getTempMediaDirectory(context);
            if (StringUtility.validateString(dir)) {
                File file = new File(dir);
                if (file != null && file.exists()) {
                    deleteRecursive(file);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void deleteRecursive(File fileOrDirectory) {

        if (fileOrDirectory.isDirectory())
            for (File child : fileOrDirectory.listFiles())
                deleteRecursive(child);

//        fileOrDirectory.delete();

    }

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static int pxToDp(int px) {
        return (int) (px / Resources.getSystem().getDisplayMetrics().density);
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = listView.getPaddingTop() + listView.getPaddingBottom();

        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            if (listItem instanceof ViewGroup) {
                listItem.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            }

            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
    }

    public static String getPortalAuthToken() {
        String credentials = Constant.PORTAL_USER_NAME + ":" + Constant.PORTAL_P;
        Log.e("authToken======     ", "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP));
        return credentials;
    }

    public static void setAppLocale(Context context, String localeCode) {
        Resources resources = context.getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        Configuration config = resources.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLocale(new Locale(localeCode.toLowerCase()));
        } else {
            config.locale = new Locale(localeCode.toLowerCase());
        }
        resources.updateConfiguration(config, dm);
    }

    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static String getCurrentFormattedDate(Context context) {
        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        SimpleDateFormat month_date;
//        if (PreferenceManager.getAppLang(context).equalsIgnoreCase("Hi")) {
//            month_date = new SimpleDateFormat("MMMM", getDefaultLocale(context));
//        } else
        month_date = new SimpleDateFormat("MMMM", getDefaultLocale(context));
        return calendar.get(Calendar.DAY_OF_MONTH) + "-" + month_date.format(calendar.getTime()) + "-" + calendar.get(Calendar.YEAR);
    }

    public static String formatDecimalValues(float value) {
//        DecimalFormat decimalFormat = null;
//        try {
//            decimalFormat = new DecimalFormat("#.##");
//            Float.valueOf(decimalFormat.format(value));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        return String.valueOf((int) Math.ceil(value));
        //  return String.format("%.2f", value);
    }

    public static String formatTwoDecimal(String value) {
        DecimalFormat decimalFormat = new DecimalFormat("#.##");
        return (decimalFormat.format(value));
    }

    public static String getDate(String time) {
        if (time != null) {
            long timestamp = Long.parseLong(time);
            Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            cal.setTimeInMillis(timestamp * 1000);
            String date = DateFormat.format("yyyy-MM-dd", cal).toString();
            return date;
        } else return "";
    }

    public static String getCurrentMonthName() {
        return (String) android.text.format.DateFormat.format("MMMM", new Date());
    }

    public static String gtDashBoardPreviousMonth(Context context) {
        SimpleDateFormat format = new SimpleDateFormat("MMMM", getDefaultLocale(context));
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        return format.format(cal.getTime());

    }

    public static String getFormatedCalenderDay(Calendar cal) {
        return DateFormat.format("yyyy-MM-dd", cal).toString();
    }


    public static File convertJsonToExcel(ArrayList<ExcelDataModel> copiesModel) {
        try {
            Gson gson = new Gson();
            String listString = gson.toJson(
                    copiesModel,
                    new TypeToken<ArrayList<ExcelDataModel>>() {
                    }.getType());

            JSONArray array = new JSONArray(listString);
            String path = getStorage(Constant.DIR_COPIES);
            File dir = new File(path);
            if (!dir.exists())
                dir.mkdirs();
            File file = new File(dir, "Copies_data" + System.currentTimeMillis() + ".csv");
            // String csv = CDL.toString(array);
            String csv = array.toString();
            FileUtils.writeStringToFile(file, csv);
            return file;
        } catch (JSONException e) {
            Log.e("on json exception", "===== ");
            e.printStackTrace();
        } catch (IOException e) {
            Log.e("on Io exception", "===== ");
            e.printStackTrace();
        }
        return null;
    }


    //date formate with month name
    public static String getMonthFormattedDate(String time) {
        if (time != null && !time.isEmpty()) {
            long timestamp = 0;
            try {
                timestamp = Long.parseLong(time);
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(timestamp * 1000);
                String date = DateFormat.format("dd-MMM-yyyy", cal).toString();
                return date;
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            return time;
        }
        return "";
    }

    ///Ledger formatted date
    public static String getFormatedCalenderDate(Calendar cal) {
        return DateFormat.format("dd-MM-yyyy", cal).toString();
    }

    public static String getFormateDate(String time) {

        if (time != null) {
            long timestamp = Long.parseLong(time);
            Calendar cal = Calendar.getInstance(Locale.getDefault());
            cal.setTimeInMillis(timestamp * 1000);
            String date = DateFormat.format("dd-MM-yyyy", cal).toString();
            return date;
        } else return "";
    }

    public static String getLedgerFormateDate(String time) {

        if (time != null) {
            long timestamp = Long.parseLong(time);
            Calendar cal = Calendar.getInstance(Locale.getDefault());
            cal.setTimeInMillis(timestamp);
            String date = DateFormat.format("dd-MM-yyyy", cal).toString();
            return date;
        } else return "";
    }

    public static String getFormatDecimalValue(float value) {
        return String.valueOf((int) Math.ceil(value));
        // return String.format("%.2f", value);
    }


   /* public static File createLedgerPdf(Context context, List<LedgerTransModel> mData, String opBalance, String closeBal, String agentName, String fromDate, String toDate, Drawable drawableLogo,String agentId) {
        String path = getStorage(Constant.DIR_LEDGER);
        FileOutputStream fOut=null;
        try {
            File dir = new File(path);
            if (!dir.exists())
                dir.mkdirs();
            File file = new File(dir, "Ledger_" + System.currentTimeMillis() + ".pdf");
            fOut = new FileOutputStream(file);
            Document document = new Document();
            PdfWriter.getInstance(document, fOut);
            document.open();
            PdfPTable table = new PdfPTable(new float[]{1, .8f, 1, 1});
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(context.getResources().getString(R.string.str_ledger_tansaction_date));
            table.addCell(context.getResources().getString(R.string.str_ledger_type));
            table.addCell(context.getResources().getString(R.string.str_ledger_trans_amount));
            table.addCell(context.getResources().getString(R.string.str_ledger_transaction_reason));
//            table.addCell(context.getResources().getString(R.string.str_ledger_trans_desc));
//            table.addCell(context.getResources().getString(R.string.str_ledger_doc_number));
            table.setHeaderRows(1);
            PdfPCell[] cells = table.getRow(0).getCells();
            for (int j = 0; j < cells.length; j++) {
//                cells[j].setBackgroundColor(BaseColor.RED);
                cells[j].setPadding(15);
            }
            for (int i = 0; i < mData.size(); i++) {
                table.addCell(getFormateDate(mData.get(i).getBudat()));
                table.addCell(mData.get(i).getBlart());
                table.addCell(mData.get(i).getFormattedAmount());
                table.addCell(mData.get(i).getBezei());
//                table.addCell(mData.get(i).getXblnr());
            }

            try {

                BitmapDrawable bitDw = ((BitmapDrawable) drawableLogo);
                Bitmap bmp = bitDw.getBitmap();
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                Image image = Image.getInstance(stream.toByteArray());
                image.setAlignment(Image.MIDDLE);

                Paragraph paragraphImage = new Paragraph();
                paragraphImage.add(image);
                paragraphImage.setExtraParagraphSpace(16);
                paragraphImage.setAlignment(Element.ALIGN_CENTER);
                document.add(paragraphImage);
                // document.add(image);
            }
            catch (IOException e) {
                e.printStackTrace();
            }


            Paragraph paragraph = new Paragraph(context.getResources().getString(R.string.str_ledger_agency_name) + " : " + agentName);
            paragraph.setExtraParagraphSpace(16);
            paragraph.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraph);
            document.add(new Paragraph(context.getResources().getString(R.string.str_ledger_from) + " " + fromDate));
            document.add(new Paragraph(context.getResources().getString(R.string.str_ledger_to) + " " + toDate));
            document.add(new Paragraph(context.getString(R.string.str_ledger_opening_balance) + opBalance));
            document.add(new Paragraph(context.getString(R.string.str_ledger_closing_balance) + closeBal));
            table.setSpacingBefore(15);
            document.add(table);
            document.close();
            return file;
        } catch (DocumentException e) {
            Log.e("sfsaafafafa","DocumentException"+" "+e.getMessage());
        } catch (FileNotFoundException e) {
            Log.e("sfsaafafafa","FileNotFoundException"+" "+e.getMessage());
        }finally {
            if(fOut!=null){
                try {

                    fOut.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;

    }*/

    public static File createLedgerPdfTest(Context context, List<NewLedgerModel> mData, String opBalance, String closeBal, String agentName, String fromDate, String toDate, Drawable drawableLogo, String agentId, String strUnitName, String strUnitAdd) {
       // String path = getStorage(Constant.DIR_LEDGER);
        FileOutputStream fOut = null;
        Log.e("sdgsdgsdgs", agentId);
        try {
          //  File dir = new File(path);
          //  if (!dir.exists())
            //    dir.mkdirs();
            ContextWrapper cw = new ContextWrapper(context);
            File directory = cw.getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
            File file = new File(directory, System.currentTimeMillis() + ".pdf");

          //  File file = new File(dir, "Ledger_" + System.currentTimeMillis() + ".pdf");
            fOut = new FileOutputStream(file);
            Document document = new Document();
            PdfWriter.getInstance(document, fOut);
            document.open();

            PdfPTable table = new PdfPTable(new float[]{1, 1, 1, 1, 1});
            table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            table.addCell(context.getResources().getString(R.string.str_ledger_tansaction_date));
            table.addCell(context.getResources().getString(R.string.str_ledger_type));
            //table.addCell(context.getResources().getString(R.string.str_ledger_trans_amount));
            table.addCell(context.getResources().getString(R.string.str_debit));
            table.addCell(context.getResources().getString(R.string.str_credit));
            table.addCell(context.getResources().getString(R.string.str_balance));
            // table.addCell(context.getResources().getString(R.string.str_ledger_transaction_reason));
            table.setHeaderRows(1);

            PdfPCell[] cells = table.getRow(0).getCells();
            for (int j = 0; j < cells.length; j++) {
                // cells[j].setBackgroundColor(BaseColor.RED);
                cells[j].setPadding(15);
            }


            double totalDebit = 0, debit;
            double totalCredit = 0, credit;
            for (int i = 0; i < mData.size(); i++) {
                // table.addCell(getFormateDate(mData.get(i).getVDate()));
                table.addCell(mData.get(i).getVDate());
                table.addCell(mData.get(i).getNarration());
                table.addCell(mData.get(i).getDebit());
                debit = Double.parseDouble(mData.get(i).getDebit());
                totalDebit = totalDebit + debit;

                table.addCell(mData.get(i).getCredit());
                credit = Double.parseDouble(mData.get(i).getCredit());
                totalCredit = totalCredit + credit;
                table.addCell(mData.get(i).getBalance());
                //table.addCell(mData.get(i).getBezei());
                // table.addCell(mData.get(i).getBezei());
            }

            double balncDebit = totalDebit;
            balncDebit = Double.parseDouble(new DecimalFormat("##.##").format(balncDebit));

            double balncCredit = totalCredit;
            balncCredit = Double.parseDouble(new DecimalFormat("##.##").format(balncCredit));
            Log.e("sdsdfsdfsfs", balncDebit + "");
            Log.e("sdsdfsdfsfs", balncCredit + "");


            PdfPTable tableFooter = new PdfPTable(new float[]{1, 1, 1, 1, 1});
            tableFooter.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
            Font fontH1 = new Font(Font.FontFamily.UNDEFINED, 12, Font.BOLD);
            tableFooter.addCell("");
            tableFooter.addCell("");
            //table.addCell(context.getResources().getString(R.string.str_ledger_trans_amount));
            // tableFooter.addCell(new PdfPCell(new Phrase("1000",fontH1)));
            tableFooter.addCell(balncDebit + ""); /* To show debit total*/
            tableFooter.addCell(balncCredit+""); /*To show credit total*/
            tableFooter.addCell("");
            // table.addCell(context.getResources().getString(R.string.str_ledger_transaction_reason));
            tableFooter.setFooterRows(1);

            PdfPCell[] cellsFooter = tableFooter.getRow(0).getCells();
            for (int j = 0; j < cellsFooter.length; j++) {
                // cells[j].setBackgroundColor(BaseColor.RED);
                cellsFooter[j].setPadding(12);
            }

            try {

                BitmapDrawable bitDw = ((BitmapDrawable) drawableLogo);
                Bitmap bmp = bitDw.getBitmap();
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
                Image image = Image.getInstance(stream.toByteArray());
                image.setAlignment(Image.MIDDLE);

                Paragraph paragraphImage = new Paragraph();
                paragraphImage.add(image);
                paragraphImage.setExtraParagraphSpace(16);
                paragraphImage.setAlignment(Element.ALIGN_CENTER);
                document.add(paragraphImage);
                //document.add(Chunk.NEWLINE);
            } catch (IOException e) {
                e.printStackTrace();
            }

            Font fontUnit = new Font(Font.FontFamily.COURIER, 14, Font.BOLD);
            Paragraph paragraphUnit = new Paragraph(context.getResources().getString(R.string.office_address) +" :- "+strUnitName + "\n " + strUnitAdd, fontUnit);
            paragraphUnit.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraphUnit);

            document.add(Chunk.NEWLINE);
            Font fontParty = new Font(Font.FontFamily.COURIER, 18, Font.BOLD);
            Paragraph partyLedger = new Paragraph(context.getResources().getString(R.string.str_ledger_party), fontParty);
            partyLedger.setAlignment(Element.ALIGN_CENTER);
            document.add(partyLedger);


            Font fontDate = new Font(Font.FontFamily.COURIER, 15, Font.BOLD);
            Paragraph datePara = new Paragraph("From" + " " + fromDate + " " + "To" + " " + toDate, fontDate);
            datePara.setAlignment(Element.ALIGN_CENTER);
            document.add(datePara);

            // document.add(new Paragraph(context.getResources().getString(R.string.str_ledger_from) + " " + fromDate));
            // document.add(new Paragraph(context.getResources().getString(R.string.str_ledger_to) + " " + toDate));

            Font fontagentName = new Font(Font.FontFamily.COURIER, 14, Font.BOLD);
            Paragraph paragraphAgentName = new Paragraph(context.getResources().getString(R.string.str_ledger_agency_name) + agentId + "  " + agentName, fontagentName);
            paragraphAgentName.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraphAgentName);



            Font fontOpeningBal = new Font(Font.FontFamily.COURIER, 14, Font.UNDERLINE | Font.BOLD);
            Paragraph paragraphOpenBal = new Paragraph(context.getResources().getString(R.string.str_ledger_opening_balance) + "  " + opBalance, fontOpeningBal);
            paragraphOpenBal.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraphOpenBal);

            table.setSpacingBefore(15);
            document.add(table);
            document.add(tableFooter);
            document.add(Chunk.NEWLINE);
            document.add(Chunk.NEWLINE);
            Font fontClosingBal = new Font(Font.FontFamily.COURIER, 14, Font.UNDERLINE | Font.BOLD);
            Paragraph paragraphCloseBal = new Paragraph(agentId + " " + context.getResources().getString(R.string.str_ledger_closing_balance) + "  " + closeBal, fontClosingBal);
            paragraphCloseBal.setAlignment(Element.ALIGN_CENTER);
            document.add(paragraphCloseBal);

            document.add(Chunk.NEWLINE);


            /*Rectangle rect= new Rectangle(577,825,18,15); // you can resize rectangle
            rect.setBorder(Rectangle.BOX);
            rect.setBorderColor(BaseColor.BLACK);
            rect.setBorderWidth(2);
            document.add(rect);*/
            document.close();
            return file;
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (fOut != null) {
                try {

                    fOut.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return null;

    }

    public static String getStorage(String folderName) {
        String path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/Samriddhi/" + folderName;
        File dir = new File(path);
        if (!dir.exists())
            dir.mkdirs();
        return path;

    }

    public static String getTimestampForDate(String date) {
        if (date != null && !date.isEmpty()) {
            try {
                Date timestampDate = new SimpleDateFormat("dd-MMM-yyyy").parse(date);
                return String.valueOf(timestampDate.getTime() / 1000);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return date;
    }

    public static String getMothFormattedCalenderDay(Calendar cal) {
        return DateFormat.format("dd-MMM-yyyy", cal).toString();
    }

    public static Locale getDefaultLocale(Context context) {
        if (context != null) {
            return context.getResources().getConfiguration().locale;
        } else return Locale.getDefault();
    }

    //Without locale set default english for download copies excel
    public static String getMonthFormattedCopies(String time) {
        if (time != null && !time.isEmpty()) {
            long timestamp = 0;
            try {
                timestamp = Long.parseLong(time);
                Calendar cal = Calendar.getInstance();
                cal.setTimeInMillis(timestamp * 1000);
                String date = DateFormat.format("dd-MMM-yyyy", cal).toString();
                return date;
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
            return time;
        }
        return "";
    }

    public static int getFormatRoundOfValue(float value) {
        return (int) Math.ceil(value);
    }

    public static String getMonthLastDayFormatted() {
        Calendar toDateCal = Calendar.getInstance();
        toDateCal.add(Calendar.MONTH, -1);
        toDateCal.set(Calendar.DATE, toDateCal.getActualMaximum(Calendar.DAY_OF_MONTH));
        Date lastDayOfMonth = toDateCal.getTime();
        java.text.DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        return sdf.format(lastDayOfMonth);
    }

    public static String getFirstDateOfMonth() {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.MONTH, -1);
        c.set(Calendar.DAY_OF_MONTH, 1);
        java.text.DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        //System.out.println(df.format(c.getTime()));
        return sdf.format(c.getTime());
    }

    public static boolean getBillingInvoiceMonthCompare(String fkdat) {
        if (fkdat != null && !fkdat.isEmpty()) {
            Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
            Calendar today = Calendar.getInstance(TimeZone.getDefault());
            calendar.setTimeInMillis(Long.parseLong(fkdat) * 1000);
            int todayMonth = today.get(Calendar.MONTH);
            int billdateMonth = calendar.get(Calendar.MONTH);
            return !(today.get(Calendar.YEAR) == calendar.get(Calendar.YEAR) && (todayMonth + 1) == (billdateMonth + 1));
        }
        return false;
    }

    public static String getFormatOutstandingForBilling(float value) {
        return String.format("%.2f", value);
    }

    public static void clearNotification(Context context) {
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(getClass().getSimpleName(), "On Boot Completed");
        //TODO
    }

    public Intent generateShareIntent(String text) {
        return new Intent().setAction(Intent.ACTION_SEND).putExtra(Intent.EXTRA_TEXT, StringUtility.validateString(text) ? text : "null").setType("text/plain");
    }

    public Intent generateShareIntent(Uri uriToImage) {
        return new Intent().setAction(Intent.ACTION_SEND).putExtra(Intent.EXTRA_STREAM, uriToImage).setType("image/*");
    }

    public Intent generateShareIntent(ArrayList<Uri> imageUris) {
        return new Intent().setAction(Intent.ACTION_SEND_MULTIPLE).putExtra(Intent.EXTRA_STREAM, imageUris).setType("image/*");
    }

    public Intent generateNavigationIntent(String sLatitude, String sLongitude, String dLatitude, String dLongitude, String label) {
        String format = "geo:" + sLatitude + "," + sLongitude + "?q=" + dLatitude + "," + dLongitude + (StringUtility.validateString(label) ? "(" + label + ")" : "");
        Uri uri = Uri.parse(format);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        return intent;
    }

    public Intent generateSendEmailIntent(String strEmail, String subject, String body, Uri attachment) {
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setType("application/image");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{strEmail});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, body);
        emailIntent.putExtra(Intent.EXTRA_STREAM, attachment/*Uri.parse("file:///mnt/sdcard/Myimage.jpeg")*/);
        return emailIntent;
    }

    public boolean validateFilePath(String path) {
        return StringUtility.validateString(path) && validateFile(new File(path));
    }

    public boolean validateFile(File file) {
        return file.exists();
    }

    public String getRealPathFromURI(Context context, Uri contentUri) {
        if (validateFilePath(contentUri.getPath())) {
            return contentUri.getPath();
        }
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null,
                    null, null);
            if (cursor == null)
                return null;
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    public String generateFBHashKey(Context context) {
        // Add code to print out the key hash
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String key = Base64.encodeToString(md.digest(), Base64.DEFAULT);
                Log.d("FB KeyHash: ", key);
                return key;
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
        return null;
    }

    public String getMediaDirectory(Context context) {
        File dir = new File(Environment.getExternalStorageDirectory() + "/" + context.getApplicationInfo().name);
        if (dir != null && !dir.exists()) {
            dir.mkdirs();
        }
        if (dir.exists() && dir.isDirectory()) {
            return dir.getAbsolutePath();
        }
        return null;
    }
}
