package com.app.samriddhi.util;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.view.View;
import android.view.ViewTreeObserver;

import com.app.samriddhi.R;
import com.github.amlcurran.showcaseview.ShowcaseDrawer;

/**
 * Created by Radha Soni on 04/05/2020.
 */


public class CustonShowCaseViewCircle implements ShowcaseDrawer {

    private final Paint eraserPaint;
    private final Paint basicPaint;
    private final int eraseColour;
    private final RectF renderRect;
    protected int backgroundColour;
    private float width;
    private float height;

    public CustonShowCaseViewCircle(final View view, Resources resources) {
     /*   width = resources.getDimension(R.dimen.dimen_15dp);
        height = resources.getDimension(R.dimen.dimen_16dp);*/

        ViewTreeObserver observer = view.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            @Override
            public void onGlobalLayout() {
                // TODO Auto-generated method stub

                width = view.getWidth();
                height = view.getHeight();
                view.getViewTreeObserver().removeGlobalOnLayoutListener(
                        this);
            }
        });
        PorterDuffXfermode xfermode = new PorterDuffXfermode(PorterDuff.Mode.MULTIPLY);
        eraserPaint = new Paint();
        eraserPaint.setColor(resources.getColor(R.color.colorPrimary));
        eraserPaint.setAlpha(1);
        eraserPaint.setXfermode(xfermode);
        eraserPaint.setAntiAlias(true);
        eraseColour = resources.getColor(R.color.colorPrimaryDark);
        basicPaint = new Paint();
//        basicPaint.setColor(resources.getColor(R.color.google_red));
        renderRect = new RectF();
    }

    @Override
    public void setShowcaseColour(int color) {
        eraserPaint.setColor(color);
    }

    @Override
    public void drawShowcase(Bitmap buffer, float x, float y, float scaleMultiplier) {
        Canvas bufferCanvas = new Canvas(buffer);
        renderRect.left = x - width / 2f;
        renderRect.right = x + width / 2f;
        renderRect.top = y - height / 2f;
        renderRect.bottom = y + height / 2f;
        bufferCanvas.drawCircle(x, y, width / 2, eraserPaint);
    }

    @Override
    public int getShowcaseWidth() {
        return (int) width;
    }

    @Override
    public int getShowcaseHeight() {
        return (int) height;
    }

    @Override
    public float getBlockedRadius() {
        return width;
    }

    @Override
    public void setBackgroundColour(int backgroundColor) {
        // No-op, remove this from the API?
        this.backgroundColour = backgroundColor;
    }

    @Override
    public void erase(Bitmap bitmapBuffer) {
        bitmapBuffer.eraseColor(backgroundColour);
    }

    @Override
    public void drawToCanvas(Canvas canvas, Bitmap bitmapBuffer) {
        canvas.drawBitmap(bitmapBuffer, 0, 0, basicPaint);
    }


}
