package com.app.samriddhi.ui.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.StyleRes;
import androidx.fragment.app.DialogFragment;

import com.app.samriddhi.R;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.internal.ViewUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.Nullable;

public class ActionBottomDialogFragment extends BottomSheetDialogFragment
        implements View.OnClickListener {
    public static final String TAG = "ActionBottomDialog";
    private ItemClickListener mListener;
    CheckBox UnsoldCopies, tornCopies, BNR, shortCopies, Exchange;
    TextView toDate, fromDate;
    MaterialCardView card_view;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NORMAL, R.style.MyBottomSheetDialogTheme);
    }


    public static ActionBottomDialogFragment newInstance() {
        return new ActionBottomDialogFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        //getWindow().setLayout(500 /*our width*/, ViewGroup.LayoutParams.MATCH_PARENT);
        //getActivity().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        return inflater.inflate(R.layout.bottom_sheet_dialog, container, false);
    }


    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        view.findViewById(R.id.button).setOnClickListener(this);
        card_view = view.findViewById(R.id.card_view);
        UnsoldCopies = (CheckBox) view.findViewById(R.id.UnsoldCopies);
        tornCopies = (CheckBox) view.findViewById(R.id.tornCopies);
        BNR = (CheckBox) view.findViewById(R.id.BNR);
        shortCopies = (CheckBox) view.findViewById(R.id.shortCopies);
        Exchange = (CheckBox) view.findViewById(R.id.Exchange);
        toDate = (TextView) view.findViewById(R.id.tv_to_date);
        fromDate = (TextView) view.findViewById(R.id.tv_from_date);

        card_view.setBackgroundResource(R.drawable.card_view_background);


        fromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDateFromPicker(fromDate);
            }
        });
        toDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDateFromPicker(toDate);
            }
        });

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof ItemClickListener) {
            mListener = (ItemClickListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement ItemClickListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        StringBuilder result = new StringBuilder();
        result.append("\n");
        if (UnsoldCopies.isChecked()) {
            result.append(UnsoldCopies.getText() + "\n");
        }
        if (tornCopies.isChecked()) {
            result.append(tornCopies.getText() + "\n");
        }
        if (BNR.isChecked()) {
            result.append(BNR.getText() + "\n");
        }
        if (shortCopies.isChecked()) {
            result.append(shortCopies.getText() + "\n");
        }
        if (Exchange.isChecked()) {
            result.append(Exchange.getText() + "\n");
        }
        //Displaying the message on the toast
        try {
            if (compareDates(fromDate.getText().toString(), toDate.getText().toString())) {
                mListener.onItemClick(result.toString(), fromDate.getText().toString(), toDate.getText().toString());
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        dismiss();
    }

    public interface ItemClickListener {

        void onItemClick(String item, String fromDate, String toDate);
    }

    public void getDateFromPicker(TextView tv) {

        DatePickerDialog datePickerDialog;
        int year;
        int month;
        int dayOfMonth;
        Calendar calendar;

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        datePickerDialog = new DatePickerDialog(requireContext(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                        tv.setText(day + "/" + (month + 1) + "/" + year);
                    }
                }, year, month, dayOfMonth);
        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
        datePickerDialog.show();
    }

    public boolean compareDates(String date1string, String date2string) throws ParseException {
if(!date1string.isEmpty() || !date2string.isEmpty())
{
        Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(date1string);
        Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse(date2string);
        // if you already have date objects then skip 1
        //1

        //1

        //date object is having 3 methods namely after,before and equals for comparing
        //after() will return true if and only if date1 is after date 2
        if (date1.after(date2)) {
            System.out.println("Date1 is after Date2");
            Toast.makeText(requireContext(), "To date should be greater then from date", Toast.LENGTH_SHORT).show();
            return false;
        }

        //before() will return true if and only if date1 is before date2
        if (date1.before(date2)) {
            System.out.println("Date1 is before Date2");
            return true;
        }

        //equals() returns true if both the dates are equal
        if (date1.equals(date2)) {
            System.out.println("Date1 is equal Date2");
            return true;
        }

        if (date1.toString().isEmpty() && date2.toString().isEmpty()) {
            System.out.println("both date empty");
            return true;
        }

    }
        return true;}

}
