package com.app.samriddhi.ui.activity.main.ui.Grievance;

import android.annotation.SuppressLint;
import android.content.Context;
import android.preference.PreferenceActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.R;
import com.app.samriddhi.api.request.InvoiceRequest;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.Grievance.multipleimage.ImageAdapter;
import com.app.samriddhi.ui.activity.main.ui.InVoiceActivity;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.POModal.DailyPOModal;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.SystemUtility;
import com.bogdwellers.pinchtozoom.ImageMatrixTouchHandler;
import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class RequestDetailAdapter extends RecyclerView.Adapter<RequestDetailAdapter.MyViewHolder>{

    private final List<RequestDetailModal> requestDetailModals;
    private List<ImageModal> imageList;
    ImagaAdapter imageAdapter;
    Context context;
    int row_index = -1;
    public RequestDetailAdapter(List<RequestDetailModal> requestDetailModals, Context context) {
        this.requestDetailModals = requestDetailModals;
        this.context = context;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = null;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.complaint_detail_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder Viewholder, @SuppressLint("RecyclerView") int position) {
        RequestDetailModal listData = requestDetailModals.get(position);
        Viewholder.setIsRecyclable(false);
        if(PreferenceManager.getAppLang(context).equals("hi")) {

            Viewholder.txtDate.setText("नदिनांक");
            Viewholder.txtStatus.setText("स्थिति");
            Viewholder.txtCat.setText("प्रकार");
            Viewholder.txtSubCat.setText("संदर्भ के");
            Viewholder.txtDesc.setText("आपका विवरण");
            Viewholder.txtUploaded.setText("अपलोड की गई छवियां");
            Viewholder.txtHORemark.setText("संकल्प टिप्पणी");
        }


        if(PreferenceManager.getComplaint_Type(context).equals("c")){
            Viewholder.txtComplaintNumber.setText("Complaint Number - #"+listData.getNumber());
        }
        else if(PreferenceManager.getComplaint_Type(context).equals("q")){
            Viewholder.txtComplaintNumber.setText("Query Number - #"+listData.getNumber());
        }
        else if(PreferenceManager.getComplaint_Type(context).equals("s")){
            Viewholder.txtComplaintNumber.setText("Suggestion Number - #"+listData.getNumber());
        }
        else{
            Viewholder.txtComplaintNumber.setText("Request Number - #"+listData.getNumber());
        }

        Viewholder.tvDate.setText(listData.getDate());

        Viewholder.tvCatName.setText(listData.getType());
        Viewholder.tvSubCatName.setText(listData.getSub_cat_name());
        Viewholder.tvDescription.setText(listData.getDescription());
        if(listData.getStatus().equals("4")){
            Viewholder.tvStatus.setText("Closed");

            Viewholder.linearHoRemark.setVisibility(View.VISIBLE);
            Viewholder.tvHoRemark.setText(listData.getHo_remark());
            Viewholder.tvRemarkTime.setText("Closed At"+" -- "+listData.getHo_close_date());

        }
        else {
            Viewholder.linearHoRemark.setVisibility(View.GONE);
            Viewholder.tvStatus.setText("Pending");
        }


        if(listData.getImage()!="null"){

            Viewholder.relImage.setVisibility(View.VISIBLE);
            try {
                Log.e("dsgsdgsdg",listData.getImage());
                Glide.with(context).load(listData.getPath()+listData.getImage()).into(Viewholder.capImage);
                // Glide.with(context).load("https://image.shutterstock.com/image-photo/mountains-under-mist-morning-amazing-260nw-1725825019.jpg").into(Viewholder.capImage);
            } catch (Exception e) {
                Log.e("sfsdfsfsfsdf",e.getMessage());
            }

        }

        else {
            Log.e("asfasfasfa","else");
           Viewholder.relImage.setVisibility(View.GONE);

        }


        Viewholder.cardViewMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                row_index = position;
                notifyDataSetChanged();
            }
        });

        if (row_index == position) {


            Viewholder.linDetail.setVisibility(View.VISIBLE);

        } else {
            Viewholder.linDetail.setVisibility(View.GONE);
        }



        getImages(listData.getId(),Viewholder);




    }


    public void getImages(String id, MyViewHolder viewholder){

        AndroidNetworking.post(Constant.BASE_URL_Grievance+"agentservices/fetchCqrsImages")
                .addBodyParameter("cqrs_id", id)
                .setTag("Details")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            Log.e("sdgsdgsdg",response.toString());
                            imageList = new ArrayList<>();
                            JSONArray jsonArray = new JSONArray(response.getString("imagefile"));

                            for (int i = 0; i < jsonArray.length(); i++) {

                                ImageModal imageModal = new ImageModal();
                                JSONObject jsonObject=jsonArray.getJSONObject(i);
                                imageModal.setImageurl(response.getString("image_url"));
                                imageModal.setImage(jsonObject.getString("image"));
                                imageList.add(imageModal);
                            }
                            viewholder.recyclerImages.setHasFixedSize(true);
                            viewholder.recyclerImages.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true));
                            imageAdapter = new ImagaAdapter(imageList, context);
                            viewholder.recyclerImages.setAdapter(imageAdapter);

                        } catch (Exception ex) {
                           // loader.setVisibility(View.GONE);
                            Log.e("dfhdfdfhd", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                       // loader.setVisibility(View.GONE);
                        Log.e("dfhdfdfhd", anError.getMessage());

                    }
                });

    }





    @Override
    public int getItemCount() {
        return requestDetailModals.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtComplaintNumber, tvDate, tvStatus, tvCatName, tvSubCatName, tvDescription;
        ImageView capImage;
        MaterialCardView cardViewMain;
        LinearLayout linDetail;
        RelativeLayout relImage;
        RecyclerView recyclerImages;
        LinearLayout linearHoRemark;
        TextView tvRemarkTime;
        TextView tvHoRemark;
        TextView txtDate,txtStatus,txtUploaded,txtHORemark,txtDesc,txtCat,txtSubCat;


        MyViewHolder(View view) {
            super(view);

            txtSubCat = view.findViewById(R.id.txtSubCat);
            txtCat = view.findViewById(R.id.txtCat);
            txtDesc = view.findViewById(R.id.txtDesc);
            txtUploaded = view.findViewById(R.id.txtUploaded);
            txtStatus = view.findViewById(R.id.txtStatus);
            txtHORemark = view.findViewById(R.id.txtHORemark);
            txtDate = view.findViewById(R.id.txtDate);

            txtComplaintNumber = view.findViewById(R.id.txtComplaintNumber);
            tvDate = view.findViewById(R.id.tvDate);
            tvStatus = view.findViewById(R.id.tvStatus);
            tvCatName = view.findViewById(R.id.tvCatName);
            tvSubCatName = view.findViewById(R.id.tvSubCatName);
            tvDescription = view.findViewById(R.id.tvDescription);
            capImage = view.findViewById(R.id.capImage);
            cardViewMain = view.findViewById(R.id.cardViewMain);
            linDetail = view.findViewById(R.id.linDetail);
            relImage = view.findViewById(R.id.relImage);
            recyclerImages = view.findViewById(R.id.recyclerImages);
            linearHoRemark = view.findViewById(R.id.linearHoRemark);
            tvRemarkTime = view.findViewById(R.id.tvRemarkTime);
            tvHoRemark = view.findViewById(R.id.tvHoRemark);
        }
    }


}

