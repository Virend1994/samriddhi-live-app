package com.app.samriddhi.ui.activity.main.ui.AgencyClose;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.R;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule.KYBPActivity;
import com.app.samriddhi.ui.activity.main.ui.Grievance.RequestDetailAdapter;
import com.app.samriddhi.ui.activity.main.ui.Grievance.RequestDetailModal;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.Util;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.card.MaterialCardView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Credentials;

public class AgencyCloseDetailAdapter extends RecyclerView.Adapter<AgencyCloseDetailAdapter.MyViewHolder> {

    private final List<AgencyCloseStatusModal> requestDetailModals;
    Context context;
    int row_index = -1;
    int posINC = 0;
    Bitmap bitmap3 = null;

    public AgencyCloseDetailAdapter(List<AgencyCloseStatusModal> requestDetailModals, Context context) {
        this.requestDetailModals = requestDetailModals;
        this.context = context;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = null;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.closure_status_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder Viewholder, @SuppressLint("RecyclerView") int position) {
        AgencyCloseStatusModal listData = requestDetailModals.get(position);
        Viewholder.setIsRecyclable(false);
        Viewholder.tvStateName.setText(listData.getState_name());
        Viewholder.tvRequestDate.setText(listData.getRequest_date());
        Viewholder.tvRID.setText(listData.getId());
        Viewholder.tvUnit.setText(listData.getUnit());
        Viewholder.tvPlace.setText(listData.getUnit());
        Viewholder.tvClosureType.setText(listData.getClosure_type());
        Viewholder.tvAgencyCode.setText(listData.getAgency_code());
        Viewholder.tvAgencyName.setText(listData.getAgency_name());
        Viewholder.tvCopies.setText(listData.getCopies());
        Viewholder.tvStartDate.setText(listData.getStart_date());
        Viewholder.tvEndDate.setText(listData.getEnd_date());
        Viewholder.tvAsdNorms.setText(listData.getAsd());
        Viewholder.tvOutStandings.setText(listData.getOutstanding());
        Viewholder.tvTotalTime.setText(listData.getTotal_time());

        Viewholder.tvunbilled.setText(listData.getUnbilled_amount());
        Viewholder.tvTotalOutStandings.setText(listData.getTotal_ots());
        Viewholder.tvEstimated.setText(listData.getEst_bill_amt());
        Viewholder.tvBalanceAmount.setText(listData.getBalance_amount());
        Viewholder.tvAgencyLocation.setText(listData.getAgency_location());
        Viewholder.tvNewRemark.setText(listData.getRemark_new());

//        Viewholder.img_arrow_exp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if( Viewholder.img_arrow_exp.getRotation()==360){
//                    Viewholder.linDetail.setVisibility(View.VISIBLE);
//                    Viewholder.img_arrow_exp.setRotation(180);
//                    notifyDataSetChanged();
//
//
//                }
//                else {
//                    Viewholder.linDetail.setVisibility(View.GONE);
//                    Viewholder.img_arrow_exp.setRotation(360);
//                    notifyDataSetChanged();
//                }
//            }
//        });



        if (listData.getReason_id().equals("5")) {
            Viewholder.relativeNominee.setVisibility(View.VISIBLE);
            Viewholder.tvNomineeName.setText(listData.getNominee_name());
            Viewholder.tvRelationship.setText(listData.getNominee_relationship());

        } else {
            Viewholder.relativeNominee.setVisibility(View.GONE);
        }

        /* Access type UOH */
        Log.e("sfsdfsdfs", PreferenceManager.getPrefUserType(context));
        if (PreferenceManager.getPrefUserType(context).equals("UH")) {

            Viewholder.btnReject.setVisibility(View.GONE);

            Log.e("sfsdfsdfs", listData.getUoh_Staus());
            if (listData.getUoh_Staus().equals("0")) {
                Viewholder.relShowUOHRemark.setVisibility(View.GONE);
                Viewholder.btnlayout.setVisibility(View.VISIBLE);
                Viewholder.tvUOH.setText("Pending");
                Viewholder.tvUOH.setTextColor(Color.parseColor("#D51406"));
            } else if (listData.getUoh_Staus().equals("1")) {

                Viewholder.tvUOH.setText("Approved");
                Viewholder.tvUOH.setTextColor(Color.parseColor("#00c851"));

                Viewholder.relShowUOHRemark.setVisibility(View.VISIBLE);
                Viewholder.txtUOHName.setText(listData.getUOHName() + "-" + "UOH");
                Viewholder.txtUOHShowRemark.setText(listData.getUoh_remark());
                Viewholder.tvUOHRemarkTime.setText(listData.getUoh_updated_time());
                Viewholder.btnlayout.setVisibility(View.GONE);

            } else {
                Viewholder.relShowUOHRemark.setVisibility(View.VISIBLE);
                Viewholder.txtUOHName.setText(listData.getUOHName() + "-" + "UOH");
                Viewholder.txtUOHShowRemark.setText(listData.getUoh_remark());
                Viewholder.tvUOHRemarkTime.setText(listData.getUoh_updated_time());
                Viewholder.btnlayout.setVisibility(View.GONE);
                Viewholder.tvUOH.setText("Rejected");
                Viewholder.tvUOH.setTextColor(Color.parseColor("#D51406"));

            }

            /*Visible Soh Remark*/
            if (!listData.getSoh_status().equals("0")) {

                Viewholder.relSohRemark.setVisibility(View.VISIBLE);
                Viewholder.txtSohShowRemark.setText(listData.getSoh_remark());
                Viewholder.txtSohName.setText(listData.getSohName() + "-" + "SOH");
                Viewholder.tvSohRemarkTime.setText(listData.getSoh_updated_time());
                if (listData.getSoh_status().equals("1")) {

                    Viewholder.tvSohStatus.setText("Approved");
                    Viewholder.tvSohStatus.setTextColor(Color.parseColor("#00c851"));

                } else {
                    Viewholder.tvSohStatus.setText("Rejected");
                    Viewholder.tvSohStatus.setTextColor(Color.parseColor("#D51406"));

                }
            }

            /*Visible Ho Remark*/
            if (!listData.getHo_status().equals("0")) {

                Viewholder.relHoRemark.setVisibility(View.VISIBLE);
                Viewholder.txtHoShowRemark.setText(listData.getHo_remark());
                Viewholder.txtHoName.setText(listData.getHOName() + "-" + "HO");
                Viewholder.tvHoRemarkTime.setText(listData.getHo_update_time());
                if (listData.getHo_status().equals("1")) {

                    Viewholder.tvHoStatus.setText("Approved");
                    Viewholder.tvHoStatus.setTextColor(Color.parseColor("#00c851"));

                } else {
                    Viewholder.tvHoStatus.setText("Rejected");
                    Viewholder.tvHoStatus.setTextColor(Color.parseColor("#D51406"));

                }
            }

            /*Visible UFH Remark*/
            if (!listData.getUfh_status().equals("0")) {

                Viewholder.relUFHRemark.setVisibility(View.VISIBLE);
                Viewholder.txtUFHShowRemark.setText(listData.getUfh_remark());
                Viewholder.txtUFHName.setText(listData.getUFHName() + "-" + "UFH");
                Viewholder.tvUFHRemarkTime.setText(listData.getUfh_updated_time());
                if (listData.getUfh_status().equals("1")) {

                    Viewholder.tvUfh.setText("Approved");
                    Viewholder.tvUfh.setTextColor(Color.parseColor("#00c851"));

                } else {
                    Viewholder.tvUfh.setText("Rejected");
                    Viewholder.tvUfh.setTextColor(Color.parseColor("#D51406"));

                }
            }


            /*Visible CBR Remark*/
            if (!listData.getCbr_status().equals("0")) {

                Viewholder.relCBRRemark.setVisibility(View.VISIBLE);
                Viewholder.txtCBRShowRemark.setText(listData.getCbr_reamrk());
                Viewholder.txtCBRName.setText(listData.getCBRName() + "-" + "CBR");
                Viewholder.tvCBRRemarkTime.setText(listData.getCbr_updated_time());
                if (listData.getCbr_status().equals("1")) {

                    Viewholder.tvCbr.setText("Approved");
                    Viewholder.tvCbr.setTextColor(Color.parseColor("#00c851"));

                } else {
                    Viewholder.tvCbr.setText("Rejected");
                    Viewholder.tvCbr.setTextColor(Color.parseColor("#D51406"));

                }
            }
        }


        /*Access type SOH*/
        if (PreferenceManager.getPrefUserType(context).equals("SH")) {
            Viewholder.btnReject.setVisibility(View.VISIBLE);
            if (listData.getSoh_status().equals("0")) {

                Viewholder.relSohRemark.setVisibility(View.GONE);
                Viewholder.btnlayout.setVisibility(View.VISIBLE);
                Viewholder.tvSohStatus.setText("Pending");
                Viewholder.tvSohStatus.setTextColor(Color.parseColor("#D51406"));
            } else if (listData.getSoh_status().equals("1")) {

                Viewholder.tvSohStatus.setText("Approved");
                Viewholder.tvSohStatus.setTextColor(Color.parseColor("#00c851"));

                Viewholder.relSohRemark.setVisibility(View.VISIBLE);
                Viewholder.txtSohName.setText(listData.getSohName() + "-" + "SOH");
                Viewholder.txtSohShowRemark.setText(listData.getSoh_remark());
                Viewholder.tvSohRemarkTime.setText(listData.getSoh_updated_time());
                Viewholder.btnlayout.setVisibility(View.GONE);

            } else {

                Viewholder.relSohRemark.setVisibility(View.VISIBLE);
                Viewholder.txtSohName.setText(listData.getSohName() + "-" + "SOH");
                Viewholder.txtSohShowRemark.setText(listData.getSoh_remark());
                Viewholder.tvSohRemarkTime.setText(listData.getSoh_updated_time());
                Viewholder.btnlayout.setVisibility(View.GONE);


                Viewholder.tvSohStatus.setText("Rejected");
                Viewholder.tvSohStatus.setTextColor(Color.parseColor("#D51406"));

            }

            /*Visible UOH Remark*/
            if (!listData.getUoh_Staus().equals("0")) {


                Viewholder.relShowUOHRemark.setVisibility(View.VISIBLE);
                Viewholder.txtUOHName.setText(listData.getUOHName() + "-" + "UOH");
                Viewholder.txtUOHShowRemark.setText(listData.getUoh_remark());
                Viewholder.tvUOHRemarkTime.setText(listData.getUoh_updated_time());

                if (listData.getUoh_Staus().equals("1")) {

                    Viewholder.tvUOH.setText("Approved");
                    Viewholder.tvUOH.setTextColor(Color.parseColor("#00c851"));

                } else {
                    Viewholder.tvUOH.setText("Rejected");
                    Viewholder.tvUOH.setTextColor(Color.parseColor("#D51406"));

                }
            }

            /*Visible Ho Remark*/
            if (!listData.getHo_status().equals("0")) {

                Viewholder.relHoRemark.setVisibility(View.VISIBLE);
                Viewholder.txtHoShowRemark.setText(listData.getHo_remark());
                Viewholder.txtHoName.setText(listData.getHOName() + "-" + "HO");
                Viewholder.tvHoRemarkTime.setText(listData.getHo_update_time());
                if (listData.getHo_status().equals("1")) {

                    Viewholder.tvHoStatus.setText("Approved");
                    Viewholder.tvHoStatus.setTextColor(Color.parseColor("#00c851"));

                } else {
                    Viewholder.tvHoStatus.setText("Rejected");
                    Viewholder.tvHoStatus.setTextColor(Color.parseColor("#D51406"));

                }
            }

            /*Visible UFH Remark*/
            if (!listData.getUfh_status().equals("0")) {

                Viewholder.relUFHRemark.setVisibility(View.VISIBLE);
                Viewholder.txtUFHShowRemark.setText(listData.getUfh_remark());
                Viewholder.txtUFHName.setText(listData.getUFHName() + "-" + "UFH");
                Viewholder.tvUFHRemarkTime.setText(listData.getUfh_updated_time());
                if (listData.getUfh_status().equals("1")) {

                    Viewholder.tvUfh.setText("Approved");
                    Viewholder.tvUfh.setTextColor(Color.parseColor("#00c851"));

                } else {
                    Viewholder.tvUfh.setText("Rejected");
                    Viewholder.tvUfh.setTextColor(Color.parseColor("#D51406"));

                }
            }


            /*Visible CBR Remark*/
            if (!listData.getCbr_status().equals("0")) {

                Viewholder.relCBRRemark.setVisibility(View.VISIBLE);
                Viewholder.txtCBRShowRemark.setText(listData.getCbr_reamrk());
                Viewholder.txtCBRName.setText(listData.getCBRName() + "-" + "CBR");
                Viewholder.tvCBRRemarkTime.setText(listData.getCbr_updated_time());
                if (listData.getCbr_status().equals("1")) {

                    Viewholder.tvCbr.setText("Approved");
                    Viewholder.tvCbr.setTextColor(Color.parseColor("#00c851"));

                } else {
                    Viewholder.tvCbr.setText("Rejected");
                    Viewholder.tvCbr.setTextColor(Color.parseColor("#D51406"));

                }
            }
        }


        /*Access type HO*/
        if (PreferenceManager.getPrefUserType(context).equals("HO")) {
            Viewholder.btnReject.setVisibility(View.VISIBLE);
            if (listData.getHo_status().equals("0")) {

                Viewholder.relHoRemark.setVisibility(View.GONE);
                Viewholder.btnlayout.setVisibility(View.VISIBLE);
                Viewholder.tvHoStatus.setText("Pending");
                Viewholder.tvHoStatus.setTextColor(Color.parseColor("#D51406"));
            } else if (listData.getHo_status().equals("1")) {

                Viewholder.tvHoStatus.setText("Approved");
                Viewholder.tvHoStatus.setTextColor(Color.parseColor("#00c851"));

                Viewholder.relHoRemark.setVisibility(View.VISIBLE);
                Viewholder.txtHoName.setText(listData.getHOName() + "-" + "HO");
                Viewholder.txtHoShowRemark.setText(listData.getHo_remark());
                Viewholder.tvHoRemarkTime.setText(listData.getHo_update_time());
                Viewholder.btnlayout.setVisibility(View.GONE);

            } else {

                Viewholder.relHoRemark.setVisibility(View.VISIBLE);
                Viewholder.txtHoName.setText(listData.getHOName() + "-" + "HO");
                Viewholder.txtHoShowRemark.setText(listData.getHo_remark());
                Viewholder.tvHoRemarkTime.setText(listData.getHo_update_time());
                Viewholder.btnlayout.setVisibility(View.GONE);
                Viewholder.tvHoStatus.setText("Rejected");
                Viewholder.tvHoStatus.setTextColor(Color.parseColor("#D51406"));

            }

            /*Visible UOH Remark*/
            if (!listData.getUoh_Staus().equals("0")) {


                Viewholder.relShowUOHRemark.setVisibility(View.VISIBLE);
                Viewholder.txtUOHName.setText(listData.getUOHName() + "-" + "UOH");
                Viewholder.txtUOHShowRemark.setText(listData.getUoh_remark());
                Viewholder.tvUOHRemarkTime.setText(listData.getUoh_updated_time());

                if (listData.getUoh_Staus().equals("1")) {

                    Viewholder.tvUOH.setText("Approved");
                    Viewholder.tvUOH.setTextColor(Color.parseColor("#00c851"));

                } else {
                    Viewholder.tvUOH.setText("Rejected");
                    Viewholder.tvUOH.setTextColor(Color.parseColor("#D51406"));

                }
            }

            /*Visible Soh Remark*/
            if (!listData.getSoh_status().equals("0")) {

                Viewholder.relSohRemark.setVisibility(View.VISIBLE);
                Viewholder.txtSohShowRemark.setText(listData.getSoh_remark());
                Viewholder.txtSohName.setText(listData.getSohName() + "-" + "SOH");
                Viewholder.tvSohRemarkTime.setText(listData.getSoh_updated_time());
                if (listData.getSoh_status().equals("1")) {

                    Viewholder.tvSohStatus.setText("Approved");
                    Viewholder.tvSohStatus.setTextColor(Color.parseColor("#00c851"));

                } else {
                    Viewholder.tvSohStatus.setText("Rejected");
                    Viewholder.tvSohStatus.setTextColor(Color.parseColor("#D51406"));

                }
            }

            /*Visible UFH Remark*/
            if (!listData.getUfh_status().equals("0")) {

                Viewholder.relUFHRemark.setVisibility(View.VISIBLE);
                Viewholder.txtUFHShowRemark.setText(listData.getUfh_remark());
                Viewholder.txtUFHName.setText(listData.getUFHName() + "-" + "UFH");
                Viewholder.tvUFHRemarkTime.setText(listData.getUfh_updated_time());
                if (listData.getUfh_status().equals("1")) {

                    Viewholder.tvUfh.setText("Approved");
                    Viewholder.tvUfh.setTextColor(Color.parseColor("#00c851"));

                } else {
                    Viewholder.tvUfh.setText("Rejected");
                    Viewholder.tvUfh.setTextColor(Color.parseColor("#D51406"));

                }
            }


            /*Visible CBR Remark*/
            if (!listData.getCbr_status().equals("0")) {

                Viewholder.relCBRRemark.setVisibility(View.VISIBLE);
                Viewholder.txtCBRShowRemark.setText(listData.getCbr_reamrk());
                Viewholder.txtCBRName.setText(listData.getCBRName() + "-" + "CBR");
                Viewholder.tvCBRRemarkTime.setText(listData.getCbr_updated_time());
                if (listData.getCbr_status().equals("1")) {

                    Viewholder.tvCbr.setText("Approved");
                    Viewholder.tvCbr.setTextColor(Color.parseColor("#00c851"));

                } else {
                    Viewholder.tvCbr.setText("Rejected");
                    Viewholder.tvCbr.setTextColor(Color.parseColor("#D51406"));

                }
            }
        }

        /*Access type UFH*/
        if (PreferenceManager.getPrefUserType(context).equals("UFH")) {
            // Viewholder.btnReject.setVisibility(View.G);
            if (listData.getUfh_status().equals("0")) {

                Viewholder.relUFHRemark.setVisibility(View.GONE);
                Viewholder.btnlayout.setVisibility(View.VISIBLE);
                Viewholder.tvUfh.setText("Pending");
                Viewholder.tvUfh.setTextColor(Color.parseColor("#D51406"));
            } else if (listData.getUfh_status().equals("1")) {

                Viewholder.relUFHRemark.setVisibility(View.VISIBLE);
                Viewholder.txtUFHName.setText(listData.getUFHName() + "-" + "UFH");
                Viewholder.txtUFHShowRemark.setText(listData.getUfh_remark());
                Viewholder.tvUFHRemarkTime.setText(listData.getUfh_updated_time());
                Viewholder.btnlayout.setVisibility(View.GONE);
                Viewholder.tvUfh.setText("Approved");
                Viewholder.tvUfh.setTextColor(Color.parseColor("#00c851"));

            } else {

                Viewholder.relUFHRemark.setVisibility(View.VISIBLE);
                Viewholder.txtUFHName.setText(listData.getUFHName() + "-" + "UFH");
                Viewholder.txtUFHShowRemark.setText(listData.getUfh_remark());
                Viewholder.tvUFHRemarkTime.setText(listData.getUfh_updated_time());
                Viewholder.btnlayout.setVisibility(View.GONE);
                Viewholder.tvUfh.setText("Rejected");
                Viewholder.tvUfh.setTextColor(Color.parseColor("#D51406"));

            }

            /*Visible UOH Remark*/
            if (!listData.getUoh_Staus().equals("0")) {


                Viewholder.relShowUOHRemark.setVisibility(View.VISIBLE);
                Viewholder.txtUOHName.setText(listData.getUOHName() + "-" + "UOH");
                Viewholder.txtUOHShowRemark.setText(listData.getUoh_remark());
                Viewholder.tvUOHRemarkTime.setText(listData.getUoh_updated_time());

                if (listData.getUoh_Staus().equals("1")) {

                    Viewholder.tvUOH.setText("Approved");
                    Viewholder.tvUOH.setTextColor(Color.parseColor("#00c851"));

                } else {
                    Viewholder.tvUOH.setText("Rejected");
                    Viewholder.tvUOH.setTextColor(Color.parseColor("#D51406"));

                }
            }

            /*Visible Soh Remark*/
            if (!listData.getSoh_status().equals("0")) {

                Viewholder.relSohRemark.setVisibility(View.VISIBLE);
                Viewholder.txtSohShowRemark.setText(listData.getSoh_remark());
                Viewholder.txtSohName.setText(listData.getSohName() + "-" + "SOH");
                Viewholder.tvSohRemarkTime.setText(listData.getSoh_updated_time());
                if (listData.getSoh_status().equals("1")) {

                    Viewholder.tvSohStatus.setText("Approved");
                    Viewholder.tvSohStatus.setTextColor(Color.parseColor("#00c851"));

                } else {
                    Viewholder.tvSohStatus.setText("Rejected");
                    Viewholder.tvSohStatus.setTextColor(Color.parseColor("#D51406"));

                }
            }

            /*Visible Ho Remark*/
            if (!listData.getHo_status().equals("0")) {

                Viewholder.relHoRemark.setVisibility(View.VISIBLE);
                Viewholder.txtHoShowRemark.setText(listData.getHo_remark());
                Viewholder.txtHoName.setText(listData.getHOName() + "-" + "HO");
                Viewholder.tvHoRemarkTime.setText(listData.getHo_update_time());
                if (listData.getHo_status().equals("1")) {

                    Viewholder.tvHoStatus.setText("Approved");
                    Viewholder.tvHoStatus.setTextColor(Color.parseColor("#00c851"));

                } else {
                    Viewholder.tvHoStatus.setText("Rejected");
                    Viewholder.tvHoStatus.setTextColor(Color.parseColor("#D51406"));

                }
            }


            /*Visible CBR Remark*/
            if (!listData.getCbr_status().equals("0")) {

                Viewholder.relCBRRemark.setVisibility(View.VISIBLE);
                Viewholder.txtCBRShowRemark.setText(listData.getCbr_reamrk());
                Viewholder.txtCBRName.setText(listData.getCBRName() + "-" + "CBR");
                Viewholder.tvCBRRemarkTime.setText(listData.getCbr_updated_time());
                if (listData.getCbr_status().equals("1")) {

                    Viewholder.tvCbr.setText("Approved");
                    Viewholder.tvCbr.setTextColor(Color.parseColor("#00c851"));

                } else {
                    Viewholder.tvCbr.setText("Rejected");
                    Viewholder.tvCbr.setTextColor(Color.parseColor("#D51406"));

                }
            }
        }

        /*Access type CBR*/
        if (PreferenceManager.getPrefUserType(context).equals("CBR")) {

            if (listData.getCbr_status().equals("0")) {

                Viewholder.relCBRRemark.setVisibility(View.GONE);
                Viewholder.btnlayout.setVisibility(View.VISIBLE);
                Viewholder.tvCbr.setText("Pending");
                Viewholder.tvCbr.setTextColor(Color.parseColor("#D51406"));
            } else if (listData.getCbr_status().equals("1")) {


                Viewholder.relCBRRemark.setVisibility(View.VISIBLE);
                Viewholder.txtCBRName.setText(listData.getCBRName() + "-" + "CBR");
                Viewholder.txtCBRShowRemark.setText(listData.getCbr_reamrk());
                Viewholder.tvCBRRemarkTime.setText(listData.getUfh_updated_time());
                Viewholder.btnlayout.setVisibility(View.GONE);
                Viewholder.tvCbr.setText("Approved");
                Viewholder.tvCbr.setTextColor(Color.parseColor("#00c851"));

            } else {

                Viewholder.relCBRRemark.setVisibility(View.VISIBLE);
                Viewholder.txtCBRName.setText(listData.getCBRName() + "-" + "CBR");
                Viewholder.txtCBRShowRemark.setText(listData.getCbr_reamrk());
                Viewholder.tvCBRRemarkTime.setText(listData.getUfh_updated_time());
                Viewholder.btnlayout.setVisibility(View.GONE);

                Viewholder.tvCbr.setText("Rejected");
                Viewholder.tvCbr.setTextColor(Color.parseColor("#D51406"));

            }

            /*Visible UOH Remark*/
            if (!listData.getUoh_Staus().equals("0")) {


                Viewholder.relShowUOHRemark.setVisibility(View.VISIBLE);
                Viewholder.txtUOHName.setText(listData.getUOHName() + "-" + "UOH");
                Viewholder.txtUOHShowRemark.setText(listData.getUoh_remark());
                Viewholder.tvUOHRemarkTime.setText(listData.getUoh_updated_time());

                if (listData.getUoh_Staus().equals("1")) {

                    Viewholder.tvUOH.setText("Approved");
                    Viewholder.tvUOH.setTextColor(Color.parseColor("#00c851"));

                } else {
                    Viewholder.tvUOH.setText("Rejected");
                    Viewholder.tvUOH.setTextColor(Color.parseColor("#D51406"));

                }
            }

            /*Visible Soh Remark*/
            if (!listData.getSoh_status().equals("0")) {

                Viewholder.relSohRemark.setVisibility(View.VISIBLE);
                Viewholder.txtSohShowRemark.setText(listData.getSoh_remark());
                Viewholder.txtSohName.setText(listData.getSohName() + "-" + "SOH");
                Viewholder.tvSohRemarkTime.setText(listData.getSoh_updated_time());
                if (listData.getSoh_status().equals("1")) {

                    Viewholder.tvSohStatus.setText("Approved");
                    Viewholder.tvSohStatus.setTextColor(Color.parseColor("#00c851"));

                } else {
                    Viewholder.tvSohStatus.setText("Rejected");
                    Viewholder.tvSohStatus.setTextColor(Color.parseColor("#D51406"));

                }
            }

            /*Visible Ho Remark*/
            if (!listData.getHo_status().equals("0")) {

                Viewholder.relHoRemark.setVisibility(View.VISIBLE);
                Viewholder.txtHoShowRemark.setText(listData.getHo_remark());
                Viewholder.txtHoName.setText(listData.getHOName() + "-" + "HO");
                Viewholder.tvHoRemarkTime.setText(listData.getHo_update_time());
                if (listData.getHo_status().equals("1")) {

                    Viewholder.tvHoStatus.setText("Approved");
                    Viewholder.tvHoStatus.setTextColor(Color.parseColor("#00c851"));

                } else {
                    Viewholder.tvHoStatus.setText("Rejected");
                    Viewholder.tvHoStatus.setTextColor(Color.parseColor("#D51406"));

                }
            }


            /*Visible UFH Remark*/
            if (!listData.getUfh_status().equals("0")) {

                Viewholder.relUFHRemark.setVisibility(View.VISIBLE);
                Viewholder.txtUFHShowRemark.setText(listData.getUfh_remark());
                Viewholder.txtUFHName.setText(listData.getUFHName() + "-" + "UFH");
                Viewholder.tvUFHRemarkTime.setText(listData.getUfh_updated_time());
                if (listData.getUfh_status().equals("1")) {

                    Viewholder.tvUfh.setText("Approved");
                    Viewholder.tvUfh.setTextColor(Color.parseColor("#00c851"));

                } else {
                    Viewholder.tvUfh.setText("Rejected");
                    Viewholder.tvUfh.setTextColor(Color.parseColor("#D51406"));

                }
            }
        }


        Viewholder.btnShowNoofCopies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AppsContants.sharedpreferences = context.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                editor.putString(AppsContants.DetailStatus, "1");
                editor.putString(AppsContants.AgencyId, listData.getAgency_code());
                editor.commit();
                context.startActivity(new Intent(context, BillingDetailsActivity.class));
            }
        });


        Viewholder.btnShowDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppsContants.sharedpreferences = context.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                editor.putString(AppsContants.DetailStatus, "2");
                editor.putString(AppsContants.AgencyId, listData.getAgency_code());
                editor.commit();
                //context.startActivity(new Intent(context, BillingDetailsActivity.class));
                context.startActivity(new Intent(context, GetMonthDetailsActivity.class));
            }
        });


        Viewholder.btnApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String strRemark = Viewholder.editRemark.getText().toString();
                String status = "1";

                if (strRemark.equals("")) {
                    alertMessage("Remark cannot be empty");
                } else {

                    SubmitRemark(Viewholder, listData, status, strRemark);
                }

            }
        });


        Viewholder.btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String strRemark = Viewholder.editRemark.getText().toString();
                String status = "2";

                if (strRemark.equals("")) {
                    alertMessage("Remark cannot be empty");
                } else {

                    SubmitRemark(Viewholder, listData, status, strRemark);
                }

            }
        });


        Viewholder.btnViewAttachment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.e("sdfdfdsfsf", "Attachment" + " " + listData.getDocument());

                if (listData.getDocument().contains(".pdf")) {
                    Log.e("sdfdfdsfsf", "Attachment" + " " + "PDF");
                   // Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(PreferenceManager.getBaseURL1(context) + "media" + "/" + listData.getDocument()));
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(Constant.BASE_PORTAL_URL + "media" + "/" + listData.getDocument()));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.setPackage("com.android.chrome");
                    try {
                        context.startActivity(intent);
                    } catch (ActivityNotFoundException ex) {
                        // Chrome browser presumably not installed so allow user to choose instead
                        intent.setPackage(null);
                        context.startActivity(intent);
                    }
                } else if (!listData.getDocument().contains(".pdf")) {
                    if (listData.getDocument().equals("")) {

                        alertMessage("Attachment not found.");
                        return;
                    } else {
                        //Glide.with(context).asBitmap().load(PreferenceManager.getBaseURL1(context) + "media" + "/" + listData.getDocument()).into(new CustomTarget<Bitmap>() {
                        Glide.with(context).asBitmap().load(Constant.BASE_PORTAL_URL + "media" + "/" + listData.getDocument()).into(new CustomTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                // bankPassBookBitmap = Util.encodeImageToBase64(resource);
                                // bank_gal.setImageBitmap(resource);
                                bitmap3 = resource;

                                Log.e("sfsfas", "Image");
                                if (bitmap3 != null) {
                                    ShowPopup(bitmap3);
                                } else {
                                    Toast.makeText(context, "Attachment not found", Toast.LENGTH_SHORT).show();
                                }

                            }

                            @Override
                            public void onLoadCleared(@Nullable Drawable placeholder) {
                            }
                        });
                    }


                } else {
                    Log.e("sdfdfdsfsf", "Attachment" + " " + "not found");

                    alertMessage("Attachment not found.");
                    return;
                }
            }
        });
    }


    public void ShowPopup(Bitmap bitmap) {

        View popupView = null;

        popupView = LayoutInflater.from(context).inflate(R.layout.enlarge_image, null);
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0);

        ImageView download_Image = popupView.findViewById(R.id.download_Image);
        download_Image.setVisibility(View.VISIBLE);

        download_Image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                File pictureFile = getOutputMediaFile();
                if (bitmap == null) {
                    Log.e("Dsgsggsdgs", "Error creating media file, check storage permissions: ");// e.getMessage());
                    return;
                }
                try {
                    FileOutputStream fos = new FileOutputStream(pictureFile);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                    fos.close();
                    Toast.makeText(context, "Image saved in MyAppFilder under Storage", Toast.LENGTH_SHORT).show();

                    Log.e("Dsgsggsdgs", "File not found: " + pictureFile.toString());
                } catch (FileNotFoundException e) {
                    Log.e("Dsgsggsdgs", "File not found: " + e.getMessage());
                } catch (IOException e) {
                    Log.e("Dsgsggsdgs", "Error accessing file: " + e.getMessage());
                }

            }
        });

        ImageView closeImg = popupView.findViewById(R.id.closeImg);
        closeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });

        PhotoView imageView = popupView.findViewById(R.id.imageView);
        imageView.setImageBitmap(bitmap);

    }

    private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getPath()
                + "/MyAppFolder");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName = "Kybp_img" + timeStamp + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }


    private void alertMessage(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                }).show();
    }


    @Override
    public int getItemCount() {
        return requestDetailModals.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvUnit, tvUOH, tvPlace, tvSohStatus, tvHoStatus, tvUfh, tvCbr;
        TextView tvClosureType, tvAgencyCode, tvAgencyName, tvCopies, tvStartDate, tvEndDate, tvAsdNorms;
        TextView tvOutStandings, tvTotalTime, txtUOHName, txtUOHShowRemark, tvUOHRemarkTime;
        RelativeLayout relShowUOHRemark;
        ImageView img_arrow_exp;
        LinearLayout linDetail, btnlayout;
        TextView btnReject, btnApprove;
        EditText editRemark;
        RelativeLayout relSohRemark;
        TextView txtSohName, txtSohShowRemark, tvSohRemarkTime;

        RelativeLayout relHoRemark;
        TextView txtHoName, txtHoShowRemark, tvHoRemarkTime;

        RelativeLayout relUFHRemark;
        TextView txtUFHName, txtUFHShowRemark, tvUFHRemarkTime;

        RelativeLayout loader;
        RelativeLayout relCBRRemark;
        TextView txtCBRName, txtCBRShowRemark, tvCBRRemarkTime, tvStateName;
        MaterialButton btnViewAttachment;
        MaterialButton btnShowNoofCopies;
        MaterialButton btnShowDetails;
        RelativeLayout relativeNominee;
        TextView tvNomineeName;
        TextView tvRelationship;
        TextView tvunbilled;
        TextView tvTotalOutStandings;
        TextView tvEstimated;
        TextView tvBalanceAmount;
        TextView tvAgencyLocation;
        TextView tvNewRemark;
        TextView tvRequestDate;
        TextView tvRID;


        MyViewHolder(View view) {
            super(view);


            tvRequestDate = view.findViewById(R.id.tvRequestDate);
            tvRID = view.findViewById(R.id.tvRID);
            btnViewAttachment = view.findViewById(R.id.btnViewAttachment);
            tvNomineeName = view.findViewById(R.id.tvNomineeName);
            tvRelationship = view.findViewById(R.id.tvRelationship);
            relativeNominee = view.findViewById(R.id.relativeNominee);

            btnShowNoofCopies = view.findViewById(R.id.btnShowNoofCopies);
            btnShowDetails = view.findViewById(R.id.btnShowDetails);

            tvPlace = view.findViewById(R.id.tvPlace);
            img_arrow_exp = view.findViewById(R.id.img_arrow_exp);
            tvClosureType = view.findViewById(R.id.tvClosureType);
            tvAgencyCode = view.findViewById(R.id.tvAgencyCode);
            tvAgencyName = view.findViewById(R.id.tvAgencyName);
            tvCopies = view.findViewById(R.id.tvCopies);
            tvStartDate = view.findViewById(R.id.tvStartDate);
            tvEndDate = view.findViewById(R.id.tvEndDate);
            tvAsdNorms = view.findViewById(R.id.tvAsdNorms);
            tvOutStandings = view.findViewById(R.id.tvOutStandings);
            tvTotalTime = view.findViewById(R.id.tvTotalTime);
            tvStateName = view.findViewById(R.id.tvStateName);
            relShowUOHRemark = view.findViewById(R.id.relShowUOHRemark);
            txtUOHName = view.findViewById(R.id.txtUOHName);
            txtUOHShowRemark = view.findViewById(R.id.txtUOHShowRemark);
            tvUOHRemarkTime = view.findViewById(R.id.tvUOHRemarkTime);

            relSohRemark = view.findViewById(R.id.relSohRemark);
            txtSohName = view.findViewById(R.id.txtSohName);
            txtSohShowRemark = view.findViewById(R.id.txtSohShowRemark);
            tvSohRemarkTime = view.findViewById(R.id.tvSohRemarkTime);

            relHoRemark = view.findViewById(R.id.relHoRemark);
            txtHoName = view.findViewById(R.id.txtHoName);
            txtHoShowRemark = view.findViewById(R.id.txtHoShowRemark);
            tvHoRemarkTime = view.findViewById(R.id.tvHoRemarkTime);

            relUFHRemark = view.findViewById(R.id.relUFHRemark);
            txtUFHName = view.findViewById(R.id.txtUFHName);
            txtUFHShowRemark = view.findViewById(R.id.txtUFHShowRemark);
            tvUFHRemarkTime = view.findViewById(R.id.tvUFHRemarkTime);

            relCBRRemark = view.findViewById(R.id.relCBRRemark);
            txtCBRName = view.findViewById(R.id.txtCBRName);
            txtCBRShowRemark = view.findViewById(R.id.txtCBRShowRemark);
            tvCBRRemarkTime = view.findViewById(R.id.tvCBRRemarkTime);


            editRemark = view.findViewById(R.id.editRemark);
            btnApprove = view.findViewById(R.id.btnApprove);
            btnReject = view.findViewById(R.id.btnReject);
            btnlayout = view.findViewById(R.id.btnlayout);
            linDetail = view.findViewById(R.id.linDetail);


            tvUnit = view.findViewById(R.id.tvUnit);
            tvUOH = view.findViewById(R.id.tvUOH);
            tvSohStatus = view.findViewById(R.id.tvSohStatus);
            tvHoStatus = view.findViewById(R.id.tvHoStatus);
            tvUfh = view.findViewById(R.id.tvUfh);
            tvCbr = view.findViewById(R.id.tvCbr);
            loader = view.findViewById(R.id.loader);
            tvunbilled = view.findViewById(R.id.tvunbilled);
            tvTotalOutStandings = view.findViewById(R.id.tvTotalOutStandings);
            tvEstimated = view.findViewById(R.id.tvEstimated);
            tvBalanceAmount = view.findViewById(R.id.tvBalanceAmount);
            tvAgencyLocation = view.findViewById(R.id.tvAgencyLocation);
            tvNewRemark = view.findViewById(R.id.tvNewRemark);


        }
    }


    public void SubmitRemark(MyViewHolder viewholder, AgencyCloseStatusModal listData, String status, String remark) {

        viewholder.loader.setVisibility(View.VISIBLE);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("request_id", listData.getId());
            jsonObject.put("user_id", PreferenceManager.getAgentId(context));
            jsonObject.put("access_type_id", PreferenceManager.getPrefUserType(context));
            jsonObject.put("user_name", PreferenceManager.getPrefAgentName(context));
            jsonObject.put("status", status);
            jsonObject.put("remark", remark);
        } catch (JSONException e) {
            e.printStackTrace();
        }
//        AndroidNetworking.post(PreferenceManager.getBaseURL1(context)+ "dash/api/agency-closure-submit")
        AndroidNetworking.post(Constant.BASE_PORTAL_URL + "dash/api/agency-closure-submit")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .addJSONObjectBody(jsonObject)
                .setTag("Request")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            if (response.getString("message").equals("Agency Closure Request Update Successfully")) {
                                viewholder.loader.setVisibility(View.GONE);

                                if (context instanceof AgencyCloseStausActivity) {

                                    ((AgencyCloseStausActivity) context).getRequest();
                                    Toast.makeText(context, "Agency Closure Request Update Successfully", Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                viewholder.loader.setVisibility(View.GONE);
                                Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                            }


                            //binding.loader.setVisibility(View.GONE);
                        } catch (Exception ex) {
                            viewholder.loader.setVisibility(View.VISIBLE);
                            Log.e("rtrtrtr", ex.getMessage());
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        viewholder.loader.setVisibility(View.VISIBLE);
                        Log.e("rtrtrtr", anError.getMessage());
                    }
                });


    }
}

