package com.app.samriddhi.ui.activity.main.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.samriddhi.R;
import com.app.samriddhi.api.response.JsonObjectResponse;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.RecyclerViewArrayAdapter;
import com.app.samriddhi.databinding.ActivityUserTypeListBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.model.AgentResultModel;
import com.app.samriddhi.ui.model.BillOutAgentResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCDistWiseResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCResultModel;
import com.app.samriddhi.ui.model.BillingOutstandingResultModel;
import com.app.samriddhi.ui.model.CashCreditResultModel;
import com.app.samriddhi.ui.model.CityResultModel;
import com.app.samriddhi.ui.model.CreditCashModel;
import com.app.samriddhi.ui.model.SalesDistrictWiseResultModel;
import com.app.samriddhi.ui.model.SalesOrgCityResultModel;
import com.app.samriddhi.ui.model.StateResultModel;
import com.app.samriddhi.ui.model.UsersResultModel;
import com.app.samriddhi.ui.presenter.UserTypeCopiesDetailPresenter;
import com.app.samriddhi.ui.view.IUserTypeCopiesView;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.SystemUtility;

import java.util.Calendar;

public class CreditCashActivity extends BaseActivity implements IUserTypeCopiesView, RecyclerViewArrayAdapter.OnItemClickListener<CreditCashModel> {
    ActivityUserTypeListBinding mBinding;
    UserTypeCopiesDetailPresenter mPresenter;
    String screenName, stateCode = "";
    Calendar c = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_user_type_list);
        initViews();
    }

    /**
     * initialize nad bind views
     */
    private void initViews() {

        mBinding.toolbarCalender.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mBinding.toolbarCalender.rlNotification.setOnClickListener(v -> {
            startActivityAnimation(this, NotificationActivity.class, false);
        });

        if (getIntent() != null) {
            if (getIntent().hasExtra(Constant.SCREEN_NAME)) {
                screenName = getIntent().getStringExtra(Constant.SCREEN_NAME);
            }

            if (getIntent().hasExtra("stateCode")) {
                stateCode = getIntent().getStringExtra("stateCode");
            }
        }
        mBinding.setScrenName(screenName);
        mBinding.toolbarCalender.txtTittle.setText(getResources().getString(R.string.str_credit_cash_tittle) + " (" + getResources().getString(R.string.str_copies) + ")");
        mBinding.tvOnPage.setText("All States/"+stateCode);
        mBinding.llHeaderLables.setVisibility(View.GONE);
        mBinding.recycleList.setLayoutManager(new LinearLayoutManager(this));
        mPresenter = new UserTypeCopiesDetailPresenter();
        mPresenter.setView(this);
        mPresenter.getCashCreditData(PreferenceManager.getAgentId(this), "P", SystemUtility.getFormatedCalenderDay(c), SystemUtility.getFormatedCalenderDay(c), stateCode.isEmpty() ? "*" : stateCode);

    }

    @Override
    public void onCitySuccess(CityResultModel mCityModel) {
         // will override if require
    }

    @Override
    public void onStateSuccess(StateResultModel mStateModel) {
      // will override if require
    }

    @Override
    public void onStateSuccess(BillingOutstandingResultModel mStateModel) {

    }

    @Override
    public void onAgentListSuccess(AgentResultModel mAgentResultModel) {
        // will override if require
    }

    @Override
    public void onBillOutAgentListSuccess(BillOutAgentResultModel mAgentResultModel) {

    }

    @Override
    public void onCreditCashSuccess(CashCreditResultModel mResultsData) {
        mBinding.recycleList.setAdapter(new RecyclerViewArrayAdapter(mResultsData.getResults(), this, screenName));

    }

    @Override
    public void onSuccess(JsonObjectResponse<UsersResultModel> body) {
         // will override if require
    }

    @Override
    public void onBillOUtCityUPCSuccess(JsonObjectResponse<BillOutCityUPCResultModel> body) {

    }

    @Override
    public void onSalesOrgCitySuccess(SalesOrgCityResultModel mData) {
        // will override if require
    }

    @Override
    public void onSalesDistrictCopies(SalesDistrictWiseResultModel mResultData) {
        // will override if require
    }

    @Override
    public void onBillOutCityUPCDistSuccess(BillOutCityUPCDistWiseResultModel mResultData) {

    }


    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onItemClick(View view, CreditCashModel object) {
        if (screenName.equalsIgnoreCase(Constant.DashBoardNoOfCopies)) {
            startActivity(new Intent(this, SalesOrgCityActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardNoOfCopies).putExtra("Id", object.getVtweg()).putExtra("state_code", stateCode));
        }
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}
