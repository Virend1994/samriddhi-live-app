package com.app.samriddhi.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;
import com.app.samriddhi.ui.model.DrawerModel;

import java.util.ArrayList;

public class DrawerAdapter extends RecyclerView.Adapter<DrawerAdapter.MyViewHolder> {

    private final ItemClickListener mOnClickListener;
    String[] arrStrings;
    TypedArray arrDrawables;
    Context mContext;
    ArrayList<DrawerModel> arrData = new ArrayList<>();

    public DrawerAdapter(Context context, ArrayList<DrawerModel> arrDrawerData, ItemClickListener onClick) {
        this.arrData = arrDrawerData;
        this.mContext = context;
        this.mOnClickListener = onClick;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_drawer, parent, false);
        MyViewHolder vh = new MyViewHolder(v); // pass the view to View Holder
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        try {
            holder.textView.setText(arrData.get(position).item_name);
            holder.img_drawer_item.setImageResource(arrData.get(position).item_img);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return arrData.size();
    }

    public interface ItemClickListener {
        void onItemClick(int position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        public TextView textView;
        ImageView img_drawer_item;
        LinearLayout rl_drawer_item;

        public MyViewHolder(View v) {
            super(v);
            itemView.setOnClickListener(this);
            textView = v.findViewById(R.id.txt_item_name);
            img_drawer_item = v.findViewById(R.id.img_drawer_item);
            rl_drawer_item = v.findViewById(R.id.rl_drawer_item);
        }

        @Override
        public void onClick(View v) {
            mOnClickListener.onItemClick(getAdapterPosition());
        }
    }
}
