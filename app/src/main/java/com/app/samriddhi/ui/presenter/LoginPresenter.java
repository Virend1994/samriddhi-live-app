package com.app.samriddhi.ui.presenter;

import android.util.Log;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.response.JsonObjectResponse;
import com.app.samriddhi.base.BasePresenter;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.model.UserModel;
import com.app.samriddhi.ui.view.ILoginView;

import org.json.JSONArray;
import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter extends BasePresenter<ILoginView> {
    /**
     *  login the user in app
     * @param userModel User data
     */

    public void userLogin(UserModel userModel) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().login(userModel).enqueue(new Callback<JsonObjectResponse<UserModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<UserModel>> call, Response<JsonObjectResponse<UserModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null) {

                            PreferenceManager.setShowUserProfile(getView().getContext(), response.body().showProfile);
                            getView().onLoginSuccess(response.body().body);
                        } else
                            getView().onLoginSuccess(response.body().replyMsg);
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<UserModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }
}
