package com.app.samriddhi.ui.activity.main.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.samriddhi.R;
import com.app.samriddhi.api.response.JsonObjectResponse;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.RecyclerViewArrayAdapter;
import com.app.samriddhi.databinding.ActivityUserTypeListBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.model.AgentResultModel;
import com.app.samriddhi.ui.model.BillOutAgentResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCDistWiseResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCResultModel;
import com.app.samriddhi.ui.model.BillingOutstandingResultModel;
import com.app.samriddhi.ui.model.CashCreditResultModel;
import com.app.samriddhi.ui.model.CityResultModel;
import com.app.samriddhi.ui.model.SalesDistrictWiseResultModel;
import com.app.samriddhi.ui.model.SalesOrgCityResultModel;
import com.app.samriddhi.ui.model.SalesUserListModel;
import com.app.samriddhi.ui.model.StateResultModel;
import com.app.samriddhi.ui.model.UsersResultModel;
import com.app.samriddhi.ui.presenter.UserTypeCopiesDetailPresenter;
import com.app.samriddhi.ui.view.IUserTypeCopiesView;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.SystemUtility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class SalesGroupUsersActivity extends BaseActivity implements IUserTypeCopiesView, RecyclerViewArrayAdapter.OnItemClickListener<SalesUserListModel> {
    ActivityUserTypeListBinding mBinding;
    UserTypeCopiesDetailPresenter mPresenter;
    private static final String STATE_CODE="stateCode";
    private static final String STATE_NAME="stateName";
    private static final String UNIT_NAME="unitName";
    private static final String UNIT_CODE="unitCode";
    private static final String MARKET_CODE="MarketCode";
    private static final String CITY_UPC="cityUPC";
    String screenName, stateCode="", stateName="",unitCode="",unitName="";
    Calendar c = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_user_type_list);
        initializeView();
    }

    /**
     * initialize and bind views
     */

    private void initializeView() {
        mBinding.recycleList.setLayoutManager(new LinearLayoutManager(this));
        mBinding.toolbarCalender.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mBinding.toolbarCalender.rlNotification.setOnClickListener(v -> {
            startActivityAnimation(this, NotificationActivity.class, false);
        });

        if (getIntent() != null) {

            if (getIntent().hasExtra(Constant.SCREEN_NAME))
                screenName = getIntent().getStringExtra(Constant.SCREEN_NAME);
            if (getIntent().hasExtra(STATE_CODE))
                stateCode = getIntent().getStringExtra(STATE_CODE);
            if (getIntent().hasExtra(STATE_NAME))
                stateName = getIntent().getStringExtra(STATE_NAME);

            if (getIntent().hasExtra(UNIT_NAME))
                unitName = getIntent().getStringExtra(UNIT_NAME);
            if (getIntent().hasExtra(UNIT_CODE))
                unitCode = getIntent().getStringExtra(UNIT_CODE);
        }



        String date = new SimpleDateFormat("dd MMM, yyyy", Locale.getDefault()).format(new Date());
        mBinding.tvTodayDate.setText(date);

        mBinding.setScrenName(screenName);
        mBinding.toolbarCalender.txtTittle.setText(getResources().getString(R.string.str_sales_group_tittle) + " (" + getResources().getString(R.string.str_copies) + ")");
        mBinding.tvOnPage.setText("All States/"+stateName+"/"+unitName);
        if(stateCode.equals("*"))
        {
            mBinding.tvOnPage.setText("All Cities/"+unitName);
        }
        mPresenter = new UserTypeCopiesDetailPresenter();
        mPresenter.setView(this);
        mPresenter.getSalesGroups(PreferenceManager.getAgentId(this),  stateCode, unitCode);
    }

    @Override
    public void onCitySuccess(CityResultModel mCityModel) {
        //will override
    }

    @Override
    public void onStateSuccess(StateResultModel mStateModel) {
        //will override
    }

    @Override
    public void onStateSuccess(BillingOutstandingResultModel mStateModel) {

    }

    @Override
    public void onAgentListSuccess(AgentResultModel mAgentResultModel) {
        //will override
    }

    @Override
    public void onBillOutAgentListSuccess(BillOutAgentResultModel mAgentResultModel) {

    }

    @Override
    public void onCreditCashSuccess(CashCreditResultModel mResultsData) {
        //will override
    }

    @Override
    public void onSuccess(JsonObjectResponse<UsersResultModel> body) {
        if (body.body.getResults() != null) {
            new UsersResultModel().setResults(body.body.getResults());
            mBinding.recycleList.setAdapter(new RecyclerViewArrayAdapter(body.body.getResults(), this, screenName));
        }
    }

    @Override
    public void onBillOUtCityUPCSuccess(JsonObjectResponse<BillOutCityUPCResultModel> body) {

    }

    @Override
    public void onSalesOrgCitySuccess(SalesOrgCityResultModel mData) {
        //will override
    }

    @Override
    public void onSalesDistrictCopies(SalesDistrictWiseResultModel mResultData) {
        //will override
    }

    @Override
    public void onBillOutCityUPCDistSuccess(BillOutCityUPCDistWiseResultModel mResultData) {

    }


    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onItemClick(View view, SalesUserListModel object) {
        if (screenName.equalsIgnoreCase(Constant.DashBoardNoOfCopies)) {
            startActivity(new Intent(this, SalesDistrictWiseActivity.class).putExtra(STATE_CODE, stateCode).putExtra(STATE_NAME, stateName).putExtra(UNIT_NAME,unitName).putExtra(UNIT_CODE,unitCode).putExtra(MARKET_CODE,object.getMarketCode()).putExtra(CITY_UPC,object.getCityUPC()).putExtra(Constant.SCREEN_NAME, screenName));
        }
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}
