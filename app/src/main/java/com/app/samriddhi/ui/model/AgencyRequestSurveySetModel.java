package com.app.samriddhi.ui.model;

import java.io.Serializable;
import java.util.List;

public class AgencyRequestSurveySetModel implements Serializable {
    public List<AgencyRequestSurveyVillageSetModel> AgencyRequestSurveyVillage_set;
    public List<AgencyRequestSurveyTargetSetModel> AgencyRequestSurveyTarget_set;
    public int tot_copies;
    public int cash_copies;
    public int subagenct_copies;
    public int vendor_copies;
    public int railway_copies;
    public int self_copies;
    public int uoh_meeting_flag;
    public String uoh_meeting_mode;
    public String uoh_meeting_remark;
    public int SOH_meeting_flag;
    public String SOH_meeting_mode;
    public String SOH_meeting_remark;

    public List<AgencyRequestSurveyVillageSetModel> getAgencyRequestSurveyVillage_set() {
        return AgencyRequestSurveyVillage_set;
    }

    public void setAgencyRequestSurveyVillage_set(List<AgencyRequestSurveyVillageSetModel> agencyRequestSurveyVillage_set) {
        this.AgencyRequestSurveyVillage_set = agencyRequestSurveyVillage_set;
    }

    public List<AgencyRequestSurveyTargetSetModel> getAgencyRequestSurveyTarget_set() {
        return AgencyRequestSurveyTarget_set;
    }

    public void setAgencyRequestSurveyTarget_set(List<AgencyRequestSurveyTargetSetModel> agencyRequestSurveyTarget_set) {
        this.AgencyRequestSurveyTarget_set = agencyRequestSurveyTarget_set;
    }

    public int getTot_copies() {
        return tot_copies;
    }

    public void setTot_copies(int tot_copies) {
        this.tot_copies = tot_copies;
    }

    public int getCash_copies() {
        return cash_copies;
    }

    public void setCash_copies(int cash_copies) {
        this.cash_copies = cash_copies;
    }

    public int getSubagenct_copies() {
        return subagenct_copies;
    }

    public void setSubagenct_copies(int subagenct_copies) {
        this.subagenct_copies = subagenct_copies;
    }

    public int getVendor_copies() {
        return vendor_copies;
    }

    public void setVendor_copies(int vendor_copies) {
        this.vendor_copies = vendor_copies;
    }

    public int getRailway_copies() {
        return railway_copies;
    }

    public void setRailway_copies(int railway_copies) {
        this.railway_copies = railway_copies;
    }

    public int getSelf_copies() {
        return self_copies;
    }

    public void setSelf_copies(int self_copies) {
        this.self_copies = self_copies;
    }

    public int getuOH_meeting_flag() {
        return uoh_meeting_flag;
    }

    public void setuOH_meeting_flag(int uOH_meeting_flag) {
        this.uoh_meeting_flag = uOH_meeting_flag;
    }

    public String getuOH_meeting_mode() {
        return uoh_meeting_mode;
    }

    public void setuOH_meeting_mode(String uOH_meeting_mode) {
        this.uoh_meeting_mode = uOH_meeting_mode;
    }

    public String getUoh_meeting_remark() {
        return uoh_meeting_remark;
    }

    public void setUoh_meeting_remark(String uoh_meeting_remark) {
        this.uoh_meeting_remark = uoh_meeting_remark;
    }

    public int getsOH_meeting_flag() {
        return SOH_meeting_flag;
    }

    public void setsOH_meeting_flag(int sOH_meeting_flag) {
        this.SOH_meeting_flag = sOH_meeting_flag;
    }

    public String getsOH_meeting_mode() {
        return SOH_meeting_mode;
    }

    public void setsOH_meeting_mode(String sOH_meeting_mode) {
        this.SOH_meeting_mode = sOH_meeting_mode;
    }

    public String getsOH_meeting_remark() {
        return SOH_meeting_remark;
    }

    public void setsOH_meeting_remark(String sOH_meeting_remark) {
        this.SOH_meeting_remark = sOH_meeting_remark;
    }
}
