package com.app.samriddhi.ui.activity.main.ui.MisEntry;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.prefernces.PreferenceManager;

import com.app.samriddhi.util.AlertUtility;
import com.app.samriddhi.util.Constant;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;

public class MisEntryActivity extends BaseActivity {

    RecyclerView recyclerview;
    LinearLayoutManager linearLayoutManager;
    List<MisAgencyModals> sendMisList;
    List<MisAgencyModals> agencyModalsList;
    MisAgencyAdapter misAgencyAdapter;
    TextView txtShowSalePo, txtShowNPS, txtShowUnsold, txtShowFreeCopies, txtTotalAgencies, txtFresh, txtOld;
    EditText search;
    MaterialButton save_cr;

    JSONArray jsonArray;
    JSONObject objsend;
    String str = "";
    JSONObject jsonobjMain;
    boolean stop = false;

    LinearLayout linearBiharJharkhand;

    String strUserId = "";
    View viewFresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mis_entry);

        str = "Virend";

        strUserId = PreferenceManager.getAgentId(this);

        viewFresh = findViewById(R.id.viewFresh);
        txtOld = findViewById(R.id.txtOld);
        txtFresh = findViewById(R.id.txtFresh);
        txtTotalAgencies = findViewById(R.id.txtTotalAgencies);
        search = findViewById(R.id.search);
        txtShowFreeCopies = findViewById(R.id.txtShowFreeCopies);
        sendMisList = new ArrayList<>();

        txtShowUnsold = findViewById(R.id.txtShowUnsold);
        txtShowNPS = findViewById(R.id.txtShowNPS);
        txtShowSalePo = findViewById(R.id.txtShowSalePo);
        save_cr = findViewById(R.id.save_cr);
        linearBiharJharkhand = findViewById(R.id.linearBiharJharkhand);
        ImageView imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

        recyclerview = findViewById(R.id.recyclerview);
        recyclerview.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(linearLayoutManager);
        getAgenciesData();


        /*Save order work starts here*/

        save_cr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                search.setText("");
                SubmitMis();
            }
        });

        /*Save order work ends here*/
    }

    public void getAgenciesData() {


        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(3, TimeUnit.MINUTES)
                .writeTimeout(3, TimeUnit.MINUTES)
                .readTimeout(3, TimeUnit.MINUTES)
                .build();

        Log.e("jsbfksdbksbg", strUserId);
        save_cr.setVisibility(View.GONE);
        enableLoadingBar(true);
        AndroidNetworking.get(Constant.BASE_PORTAL_URL + "mis/api/report-submit/" + strUserId)
                // AndroidNetworking.get("https://0a75cff151fd.ngrok.io/mis/api/report-submit/"+strUserId)
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME_new, Constant.PORTAL_P_new))
                .setOkHttpClient(okHttpClient)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            int totalPo = Integer.parseInt(response.getString("TotalPO"));
                            int totalUnsold = Integer.parseInt(response.getString("TotalUnsold"));
                            int totalFreeCopies = Integer.parseInt(response.getString("Free_Copies"));

                            int totalNPS = totalPo - totalFreeCopies - totalUnsold;
                            txtShowFreeCopies.setText(totalFreeCopies + "");
                            txtShowSalePo.setText(totalPo + "");
                            txtShowNPS.setText(totalNPS + "");
                            txtShowUnsold.setText(totalUnsold + "");

                            JSONArray BrJhUnitList = new JSONArray(response.getString("BrJhUnitList"));
                            List<String> listUnit = new ArrayList<>();
                            for (int i = 0; i < BrJhUnitList.length(); i++) {
                                listUnit.add(BrJhUnitList.getString(i));
                            }
                            if (response.getString("success").equals("true")) {
                                agencyModalsList = new ArrayList<>();
                                JSONArray jsonArray = new JSONArray(response.getString("data"));

                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    MisAgencyModals misAgencyModals = new MisAgencyModals();
                                    linearBiharJharkhand.setVisibility(View.VISIBLE);
                                    if (listUnit.contains(jsonObject.getString("Center"))) {
                                        txtOld.setVisibility(View.VISIBLE);
                                        txtFresh.setVisibility(View.VISIBLE);
                                        viewFresh.setVisibility(View.VISIBLE);
                                        misAgencyModals.setFreshOldStatus("1"); /*Show fresh and old editText in adapter*/
                                    } else {
                                        txtOld.setVisibility(View.GONE);
                                        txtFresh.setVisibility(View.GONE);
                                        viewFresh.setVisibility(View.GONE);
                                        misAgencyModals.setFreshOldStatus("0");/*Do not show fresh and old editText in adapter*/
                                    }

                                    misAgencyModals.setCenter(jsonObject.getString("Center"));
                                    misAgencyModals.setAgency_name(jsonObject.getString("Agency_Name"));
                                    misAgencyModals.setAgency_code(jsonObject.getString("Agency_Code"));
                                    misAgencyModals.setExecutive_name(jsonObject.getString("Executive_Name"));
                                    misAgencyModals.setExecutive_Code(jsonObject.getString("Executive_Code"));
                                    misAgencyModals.setCity_UPC(jsonObject.getString("cityupc"));
                                    misAgencyModals.setMainJJ(jsonObject.getString("mainjj"));
                                    misAgencyModals.setEdition(jsonObject.getString("edition"));
                                    misAgencyModals.setEdition_code(jsonObject.getString("edition_code"));
                                    misAgencyModals.setVbeln(jsonObject.getString("vbeln"));
                                    misAgencyModals.setPosnr(jsonObject.getString("posnr"));
                                    misAgencyModals.setCash_Credit(jsonObject.getString("cashcredit"));
                                    misAgencyModals.setOrd_Date(jsonObject.getString("Ord_Date"));
                                    misAgencyModals.setPo(jsonObject.getString("PO"));
                                    misAgencyModals.setNps(jsonObject.getString("NPS"));
                                    misAgencyModals.setUnsold(jsonObject.getString("UNSOLD"));
                                    misAgencyModals.setFresh_unsold(jsonObject.getString("fresh_unsold"));
                                    misAgencyModals.setOld_unsold(jsonObject.getString("old_unsold"));
                                    misAgencyModals.setPstyv(jsonObject.getString("pstyv"));
                                    misAgencyModals.setId(jsonObject.getString("dailymis_id"));
                                    misAgencyModals.setCityupcraw(jsonObject.getString("cityupcraw"));
                                    misAgencyModals.setCashcreditraw(jsonObject.getString("cashcreditraw"));
                                    misAgencyModals.setOrd_Date_Int(jsonObject.getString("Ord_Date_Int"));
                                    misAgencyModals.setEditStatus("0");
                                    misAgencyModals.setTotalNPS(totalNPS + "");
                                    misAgencyModals.setTotalPO(totalPo + "");
                                    misAgencyModals.setTotalFreeCopies(totalFreeCopies + "");
                                    agencyModalsList.add(misAgencyModals);
                                }

                                setAdapter();
                            } else {
                                enableLoadingBar(false);
                                Toast.makeText(MisEntryActivity.this, response.getString("success"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            enableLoadingBar(false);
                            Log.e("sjdfhskfsd", e.getMessage());
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        enableLoadingBar(false);
                        Log.e("sjdfhskfsd", anError.getMessage());
                    }
                });
    }

    public void setAdapter() {
        misAgencyAdapter = new MisAgencyAdapter(agencyModalsList, MisEntryActivity.this);
        recyclerview.setAdapter(misAgencyAdapter);
        save_cr.setVisibility(View.VISIBLE);
        txtTotalAgencies.setText("Total Agencies" + " " + agencyModalsList.size() + "");
        enableLoadingBar(false);

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                String text = search.getText().toString().toLowerCase(Locale.getDefault());
                Log.e("bhs==>>", text);

                misAgencyAdapter.getFilter().filter(cs);
                misAgencyAdapter.notifyDataSetChanged();

                if (text.length() < 1) {

                    setAdapter();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    public void UpdateNpsUnsold() {
        int unsold = 0;
        int po = 0;
        int freeCopies = 0;
        sendMisList = misAgencyAdapter.getArrayData();
        if (sendMisList != null) {
            for (int i = 0; i < sendMisList.size(); i++) {
                po = Integer.parseInt(sendMisList.get(i).getTotalPO());
                freeCopies = Integer.parseInt(sendMisList.get(i).getTotalFreeCopies());
                unsold += Integer.parseInt(sendMisList.get(i).getUnsold());
            }

            int totalNPS = po - freeCopies - unsold;
            txtShowNPS.setText(totalNPS + "");
            txtShowUnsold.setText(unsold + "");


        }
    }


    public void SubmitMis() {
        sendMisList = misAgencyAdapter.getArrayData();
        if (sendMisList != null) {
            jsonobjMain = new JSONObject();
            jsonArray = new JSONArray();
            objsend = new JSONObject();
            try {
                //jsonobjMain.put("BP_Code", "28348");
                jsonobjMain.put("BP_Code", strUserId);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            for (int i = 0; i < sendMisList.size(); i++) {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("Center", sendMisList.get(i).getCenter());
                    jsonObject.put("Agency_Name", sendMisList.get(i).getAgency_name());
                    jsonObject.put("Agency_Code", sendMisList.get(i).getAgency_code());
                    if (sendMisList.get(i).getExecutive_Code() != "null") {
                        jsonObject.put("Executive_Code", sendMisList.get(i).getExecutive_Code());

                    } else {
                        jsonObject.put("Executive_Code", JSONObject.NULL);
                    }

                    jsonObject.put("Executive_Name", sendMisList.get(i).getExecutive_name());
                    jsonObject.put("cityupc", sendMisList.get(i).getCity_UPC());
                    jsonObject.put("mainjj", sendMisList.get(i).getMainJJ());
                    jsonObject.put("edition", sendMisList.get(i).getEdition());
                    jsonObject.put("edition_code", sendMisList.get(i).getEdition_code());
                    jsonObject.put("vbeln", sendMisList.get(i).getVbeln());
                    jsonObject.put("posnr", sendMisList.get(i).getPosnr());
                    jsonObject.put("cashcredit", sendMisList.get(i).getCash_Credit());
                    jsonObject.put("Ord_Date", sendMisList.get(i).getOrd_Date());
                    jsonObject.put("PO", sendMisList.get(i).getPo());
                    jsonObject.put("NPS", sendMisList.get(i).getNps());
                    jsonObject.put("UNSOLD", sendMisList.get(i).getUnsold());
                    jsonObject.put("fresh_unsold", sendMisList.get(i).getFresh_unsold());
                    jsonObject.put("old_unsold", sendMisList.get(i).getOld_unsold());
                    jsonObject.put("pstyv", sendMisList.get(i).getPstyv());
                    jsonObject.put("isChanged", sendMisList.get(i).getEditStatus());
                    jsonObject.put("cityupcraw", sendMisList.get(i).getCityupcraw());
                    jsonObject.put("cashcreditraw", sendMisList.get(i).getCashcreditraw());
                    jsonObject.put("Ord_Date_Int", sendMisList.get(i).getOrd_Date_Int());
                    if (sendMisList.get(i).getId() != "null") {

                        int id = Integer.parseInt(sendMisList.get(i).getId());
                        jsonObject.put("dailymis_id", id);

                    } else {
                        jsonObject.put("dailymis_id", JSONObject.NULL);
                    }
                    jsonArray.put(jsonObject);

                    int unsold = Integer.parseInt(sendMisList.get(i).getUnsold());
                    int po = Integer.parseInt(sendMisList.get(i).getPo());

                    if (unsold > po) {
                        stop = true;
                        alertMessage("Unsold cannot be greater than PO");
                        break;
                    } else {
                        stop = false;
                    }
                } catch (JSONException e) {

                }
            }
            try {
                jsonobjMain.putOpt("data", jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.e("segwtetwe", jsonobjMain.toString());
            Log.e("gfhfghfghgf", sendMisList.size() + "");

            try {
                File file = new File(Environment.getExternalStorageDirectory() + "/test.txt");

                if (!file.exists()) {
                    file.createNewFile();
                }
                FileWriter writer = new FileWriter(file);
                writer.append(jsonobjMain.toString());
                writer.flush();
                writer.close();
            } catch (IOException e) {
            }

            if (stop == false) {

                AlertUtility.showAlert(MisEntryActivity.this, "Are you sure want to update today's Mis?", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        UpdateMis(jsonobjMain);
                    }
                });
            }
        }
    }

    public void UpdateMis(JSONObject jsonobjMain) {

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(3, TimeUnit.MINUTES)
                .writeTimeout(3, TimeUnit.MINUTES)
                .readTimeout(3, TimeUnit.MINUTES)
                .build();

        AndroidNetworking.post(Constant.BASE_PORTAL_URL + "mis/api/report-submit/")
                //AndroidNetworking.post("https://0a75cff151fd.ngrok.io/mis/api/report-submit/")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME_new, Constant.PORTAL_P_new))
                .addJSONObjectBody(jsonobjMain)
                .setOkHttpClient(okHttpClient)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("erwrwrw", response.toString());

                            if (response.getString("success").equals("true")) {
                                enableLoadingBar(false);

                                Toast.makeText(MisEntryActivity.this, "Today's MIS Data inserted sucessfully.", Toast.LENGTH_SHORT).show();
                                //alertMessage("Today's MIS Data inserted sucessfully.");
                                getAgenciesData();
                            } else {
                                enableLoadingBar(false);
                                Toast.makeText(MisEntryActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {
                            enableLoadingBar(false);
                            Log.e("sjdfhskfsd", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        enableLoadingBar(false);
                        Log.e("sjdfhskfsd", anError.getErrorBody());
                    }
                });

    }

    private void alertMessage(String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(this, R.style.AppTheme_AlertDialog)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                }).show();
    }

    public void enableLoadingBar(boolean enable) {
        if (enable) {
            loadProgressBar(null, null, false);
        } else {
            dismissProgressBar();
        }
    }
}