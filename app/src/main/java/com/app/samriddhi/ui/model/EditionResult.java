package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EditionResult extends BaseModel {

    @SerializedName("NavSubagentSet")
    @Expose
    private NavSubagentSet navSubagentSet;
    @SerializedName("NavEdtnSet")
    @Expose
    private NavEdtnSet navEdtnSet;

    public NavSubagentSet getNavSubagentSet() {
        return navSubagentSet;
    }

    public void setNavSubagentSet(NavSubagentSet navSubagentSet) {
        this.navSubagentSet = navSubagentSet;
    }

    public NavEdtnSet getNavEdtnSet() {
        return navEdtnSet;
    }

    public void setNavEdtnSet(NavEdtnSet navEdtnSet) {
        this.navEdtnSet = navEdtnSet;
    }

}
