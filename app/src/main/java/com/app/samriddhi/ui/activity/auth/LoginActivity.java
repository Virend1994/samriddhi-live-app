package com.app.samriddhi.ui.activity.auth;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.databinding.ActivityLoginBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.AgentChildSelectionActivity;
import com.app.samriddhi.ui.activity.main.ui.HomeActivity;
import com.app.samriddhi.ui.activity.main.ui.ProfileActivity;
import com.app.samriddhi.ui.fragment.DashboardFragment;
import com.app.samriddhi.ui.model.UserModel;
import com.app.samriddhi.ui.presenter.LoginPresenter;
import com.app.samriddhi.ui.view.ILoginView;
import com.app.samriddhi.util.AlertUtility;
import com.app.samriddhi.util.StringUtility;
import com.app.samriddhi.util.SystemUtility;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class LoginActivity extends BaseActivity implements ILoginView {
    ActivityLoginBinding mLoginBinding;
    LoginPresenter mLoginPresenter;
    UserModel userData;
    String currentVersion = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        mLoginBinding.setActivity(this);
        userData = new UserModel();
        mLoginBinding.setUser(userData);
        mLoginPresenter = new LoginPresenter();
        mLoginPresenter.setView(this);

        try {
            currentVersion = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        mLoginBinding.txtAppVersion.setText("Version " + currentVersion + " " + "Copy Right (2021)");
    }

    /*Handles the button click on form*/
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_forgot_password:
                startActivityAnimation(this, ForgotPassword.class, false);
                break;
            case R.id.txt_signup:
                startActivityAnimation(this, SignUp.class, false);
                break;
            case R.id.btn_login:
                if (validateFields()) {
                    Log.e("DeviceToken=>>", "Token" + " " + FirebaseInstanceId.getInstance().getToken());
                    Log.e("settingId=>>", SystemUtility.getDeviceId(this));
                    Gson gson = new Gson();
                    String data = gson.toJson(userData);
                    Log.e("adasdasdad", data);
                    userData.setDevice_id(SystemUtility.getDeviceId(this));
                    userData.setDevice_token(FirebaseInstanceId.getInstance().getToken());
                    userData.setLanguage(PreferenceManager.getAppLang(this));
                    mLoginPresenter.userLogin(userData);
                }
            default:
                break;
        }
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    @Override
    protected void onStart() {

        super.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    /*////////Validate the login form fields///////////*/

    public boolean validateFields() {
        if (!StringUtility.validateString(mLoginBinding.edAgentCode.getText().toString())) {
            snackBar(mLoginBinding.edAgentCode, R.string.error_agent_id_empty);
            return false;
        }
        if (!StringUtility.validateString(mLoginBinding.edPassword.getText().toString())) {
            snackBar(mLoginBinding.edAgentCode, R.string.str_pwd_empty);
            return false;
        }
        return true;
    }

    /*User Login success on app method */
    @Override
    public void onLoginSuccess(UserModel userData) {
        DashboardFragment.strPopupDismiss="";
        try {
            PreferenceManager.setUserLoggedIn(this, true);
            PreferenceManager.setAgentName(this, userData.getUsermaster().get(0).getFields().getFirstName());
            PreferenceManager.setPrefAgentId(this, userData.getUsermaster().get(0).getFields().getUsername());
            /*Log*/
            PreferenceManager.setPrefUserType(this, userData.getUserdetail().get(0).getFields().getAccessType());

            if (userData.getUserSAPStatus() != null&&userData.getUserSAPStatus().getResults().size()!=0) {
                PreferenceManager.setVkbur(this, userData.getUserSAPStatus().getResults().get(0).getVkbur());
                PreferenceManager.setVkorg(this, userData.getUserSAPStatus().getResults().get(0).getVkorg());
                PreferenceManager.setvkgrp(this, userData.getUserSAPStatus().getResults().get(0).getVkgrp());
                PreferenceManager.setPrefAgentParentValue(this, userData.getUserSAPStatus().getResults().get(0).getParentFlag());
            }
            PreferenceManager.setUserPhone(this, userData.getUserSAPStatus().getResults().get(0).getMobile());



           // Log.e("sfdgsdggssg",userData.getUserSAPStatus().getResults().get(0).getMobile());
            PreferenceManager.setUserCityUpc(this, userData.getUserdetail().get(0).getFields().getAgent_city_upc());
            PreferenceManager.setUserEmail(this, userData.getUsermaster().get(0).getFields().getEmail());
           // PreferenceManager.setPrefAgentParentValue(this, userData.getUserSAPStatus().getResults().get(0).getParentFlag());

            if (PreferenceManager.getPrefAgentParentValue(this).equalsIgnoreCase("Y")) {
                PreferenceManager.setParentAgentName(this, userData.getUsermaster().get(0).getFields().getFirstName());
                PreferenceManager.setParentAgentId(this, userData.getUsermaster().get(0).getFields().getUsername());
                PreferenceManager.setParentUserType(this, userData.getUserdetail().get(0).getFields().getAccessType());
                PreferenceManager.setParentUserPhone(this, userData.getUserdetail().get(0).getFields().getMobileNo());
                PreferenceManager.setParentUserEmail(this, userData.getUsermaster().get(0).getFields().getEmail());
            }
            if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("AG")) {
                //Log.e("getUserSAPStatus", PreferenceManager.getPrefAgentParentValue(this));
                if (PreferenceManager.getPrefAgentParentValue(this).equalsIgnoreCase("")) {
                    Log.e("getUserSAPStatus", "1");
                    if (PreferenceManager.getshowUserProfile(this)) {
                        startActivity(new Intent(this, ProfileActivity.class).putExtra("profileShow", true).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                    }
                } else if (PreferenceManager.getPrefAgentParentValue(this).equalsIgnoreCase("Y")) {
                    Log.e("getUserSAPStatus", "2");
                    startActivity(new Intent(this, AgentChildSelectionActivity.class).putExtra("noBackHistory", true).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                } else {
                    Log.e("getUserSAPStatus", "3");
                    startActivityAnimation(this, HomeActivity.class, true);
                }


              /*  Log.e("getUserSAPStatus", "1");
                if (PreferenceManager.getshowUserProfile(this)) {
                    startActivity(new Intent(this, ProfileActivity.class).putExtra("profileShow", true).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                }

                else if (PreferenceManager.getPrefAgentParentValue(this).equalsIgnoreCase("Y")) {
                    Log.e("getUserSAPStatus", "2");
                    startActivity(new Intent(this, AgentChildSelectionActivity.class).putExtra("noBackHistory", true).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

                } else {
                    Log.e("getUserSAPStatus", "3");
                    startActivityAnimation(this, HomeActivity.class, true);
                }*/
            } else {
                Log.e("getUserSAPStatus", "4");
                startActivityAnimation(this, HomeActivity.class, true);
            }
            finish();

        } catch (Exception e) {
            Log.e("getUserSAPStatus", "Catch" + " " + e.getMessage());

        }
    }

    /*On Login success message shown for account activation*/

    @Override
    public void onLoginSuccess(String msg) {
        AlertUtility.showAlertWithListener(this, msg, null);
    }

    @Override
    public Context getContext() {
        return this;
    }
}
