package com.app.samriddhi.ui.view;

import com.app.samriddhi.api.response.JsonObjectResponse;
import com.app.samriddhi.ui.model.AgentResultModel;
import com.app.samriddhi.ui.model.BillOutAgentResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCDistWiseResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCResultModel;
import com.app.samriddhi.ui.model.BillingListModel;
import com.app.samriddhi.ui.model.BillingOutstandingListModel;
import com.app.samriddhi.ui.model.BillingOutstandingResultModel;
import com.app.samriddhi.ui.model.CashCreditResultModel;
import com.app.samriddhi.ui.model.CityResultModel;
import com.app.samriddhi.ui.model.SalesDistrictWiseResultModel;
import com.app.samriddhi.ui.model.SalesOrgCityResultModel;
import com.app.samriddhi.ui.model.StateResultModel;
import com.app.samriddhi.ui.model.UsersResultModel;

public interface IUserTypeCopiesView extends IView {
    void onCitySuccess(CityResultModel mCityModel);

    void onStateSuccess(StateResultModel mStateModel);

    void onStateSuccess(BillingOutstandingResultModel mStateModel);

    void onAgentListSuccess(AgentResultModel mAgentResultModel);
    void onBillOutAgentListSuccess(BillOutAgentResultModel mAgentResultModel);

    void onCreditCashSuccess(CashCreditResultModel mResultsData);

    void onSuccess(JsonObjectResponse<UsersResultModel> body);
    void onBillOUtCityUPCSuccess(JsonObjectResponse<BillOutCityUPCResultModel> body);

    void onSalesOrgCitySuccess(SalesOrgCityResultModel mData);

    void onSalesDistrictCopies(SalesDistrictWiseResultModel mResultData);

    void onBillOutCityUPCDistSuccess(BillOutCityUPCDistWiseResultModel mResultData);
}
