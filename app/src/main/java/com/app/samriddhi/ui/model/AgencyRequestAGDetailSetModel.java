package com.app.samriddhi.ui.model;

import java.util.List;

public class AgencyRequestAGDetailSetModel {
    public List<AgencyRequestSurveySetModel> AgencyRequestSurvey_set;
    public List<AgencyRequestKYBPSetModel> AgencyRequestKYBP_set;
    public List<AgencyRequestCIRReqSetModel> AgencyRequestCIRReq_set;
    public String agenct_name;
    public String agency_name;
    public String cluster_id;
    public String salesdist_id;
    public String custgrp;
    public String salesoff_id;
    public String price_group_id;

    public String getCustgrp() {
        return custgrp;
    }

    public void setCustgrp(String custgrp) {
        this.custgrp = custgrp;
    }

    public String getCluster_id() {
        return cluster_id;
    }

    public void setCluster_id(String cluster_id) {
        this.cluster_id = cluster_id;
    }

    public String getSalesdist_id() {
        return salesdist_id;
    }

    public void setSalesdist_id(String salesdist_id) {
        this.salesdist_id = salesdist_id;
    }

    public String getSalesoff_id() {
        return salesoff_id;
    }

    public void setSalesoff_id(String salesoff_id) {
        this.salesoff_id = salesoff_id;
    }

    public String getPrice_group_id() {
        return price_group_id;
    }

    public void setPrice_group_id(String price_group_id) {
        this.price_group_id = price_group_id;
    }

    public Object final_status;

    public List<AgencyRequestSurveySetModel> getAgencyRequestSurvey_set() {
        return AgencyRequestSurvey_set;
    }

    public void setAgencyRequestSurvey_set(List<AgencyRequestSurveySetModel> agencyRequestSurvey_set) {
        this.AgencyRequestSurvey_set = agencyRequestSurvey_set;
    }

    public List<AgencyRequestKYBPSetModel> getAgencyRequestKYBP_set() {
        return AgencyRequestKYBP_set;
    }

    public void setAgencyRequestKYBP_set(List<AgencyRequestKYBPSetModel> agencyRequestKYBP_set) {
        this.AgencyRequestKYBP_set = agencyRequestKYBP_set;
    }

    public List<AgencyRequestCIRReqSetModel> getAgencyRequestCIRReq_set() {
        return AgencyRequestCIRReq_set;
    }

    public void setAgencyRequestCIRReq_set(List<AgencyRequestCIRReqSetModel> agencyRequestCIRReq_set) {
        this.AgencyRequestCIRReq_set = agencyRequestCIRReq_set;
    }

    public String getAgenct_name() {
        return agenct_name;
    }

    public void setAgenct_name(String agenct_name) {
        this.agenct_name = agenct_name;
    }

    public String getAgency_name() {
        return agency_name;
    }

    public void setAgency_name(String agency_name) {
        this.agency_name = agency_name;
    }

    public Object getFinal_status() {
        return final_status;
    }

    public void setFinal_status(Object final_status) {
        this.final_status = final_status;
    }
}
