package com.app.samriddhi.ui.model;

import androidx.annotation.NonNull;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NavEditionList extends BaseModel {

    @SerializedName("Pva")
    @Expose
    private String pva;
    @SerializedName("EditionName")
    @Expose
    private String editionName;

    public String getPva() {
        return pva;
    }

    public void setPva(String pva) {
        this.pva = pva;
    }

    public String getEditionName() {
        return editionName;
    }

    public void setEditionName(String editionName) {
        this.editionName = editionName;
    }

    @NonNull
    @Override
    public String toString() {
        return editionName;
    }
}
