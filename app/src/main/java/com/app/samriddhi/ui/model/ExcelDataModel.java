package com.app.samriddhi.ui.model;

public class ExcelDataModel {
    private String Date;
    private String Number_Of_Copies;
    private String Agent_Code;

    public ExcelDataModel(String date, String noOfCopies, String agent_Code) {
        this.Date = date;
        this.Number_Of_Copies = noOfCopies;
        this.Agent_Code = agent_Code;
    }

    public ExcelDataModel() {

    }
}
