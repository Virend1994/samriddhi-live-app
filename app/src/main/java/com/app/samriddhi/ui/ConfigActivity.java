package com.app.samriddhi.ui;


import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.navigation.ui.AppBarConfiguration;

import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.databinding.ActivityConfigBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.HomeActivity;
import com.app.samriddhi.ui.activity.main.ui.LanguageActivity;


public class ConfigActivity extends BaseActivity {

    public ActivityConfigBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityConfigBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        setSupportActionBar(binding.toolbar);
        PreferenceManager.clearURL(ConfigActivity.this);
        binding.btnSaveLocalUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = binding.edtUrl.getText().toString();
                if (url.trim().equals("")) {
                    Toast.makeText(ConfigActivity.this, "Please enter local url", Toast.LENGTH_SHORT).show();
                } else {

                    PreferenceManager.setBaseURL(ConfigActivity.this, url);


                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            restart();
                        }
                    }, 500);

                }
            }
        });
        binding.btnSaveDevUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PreferenceManager.setBaseURL(ConfigActivity.this, "http://dev.dbsamriddhi.in/");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        restart();

                    }
                }, 500);
            }
        });

        binding.btnSaveLiveUrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                PreferenceManager.setBaseURL(ConfigActivity.this, "http://live.dbsamriddhi.in/");

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        restart();

                    }
                }, 500);
            }
        });


    }
    public void restart(){
        if (PreferenceManager.getIsUserLoggedIn(ConfigActivity.this)) {

            startActivityAnimation(ConfigActivity.this, HomeActivity.class, false);
        } else {
            startActivityAnimation(ConfigActivity.this, LanguageActivity.class, false);
        }
        System.exit(0);


    }

}