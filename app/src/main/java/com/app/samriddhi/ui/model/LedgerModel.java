package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LedgerModel extends BaseModel {
    @SerializedName("results")
    @Expose
    private List<LedgerListModel> results = null;

    public List<LedgerListModel> getResults() {
        return results;
    }

    public void setResults(List<LedgerListModel> results) {
        this.results = results;
    }
}
