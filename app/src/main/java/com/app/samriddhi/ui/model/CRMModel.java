package com.app.samriddhi.ui.model;

import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CRMModel extends BaseModel {
    public String brotherName;
    public String sisterName;
    public String fatherName;
    public String motherName;
    public String firstWifeName;
    @SerializedName("BpCode")
    @Expose
    private String bpCode = "";
    @SerializedName("Email")
    @Expose
    private String email = "";
    @SerializedName("Mobile1")
    @Expose
    private String mobile1 = "";
    @SerializedName("Owner")
    @Expose
    private String owner = "";
    @SerializedName("Gender")
    @Expose
    private String gender = "";
    @SerializedName("Mobile2")
    @Expose
    private String mobile2 = "";
    @SerializedName("MaritalStatus")
    @Expose
    private String maritalStatus = "";
    @SerializedName("EmergencyMob")
    @Expose
    private String emergencyMob = "";
    @SerializedName("State")
    @Expose
    private String state = "";
    @SerializedName("City")
    @Expose
    private String city = "";
    @SerializedName("Pincode")
    @Expose
    private String pincode = "";
    @SerializedName("Unit")
    @Expose
    private String unit = "";
    @SerializedName("Addr")
    @Expose
    private String addr = "";
    @SerializedName("Zcat")
    @Expose
    private String zcat = "";
    @SerializedName("Religion")
    @Expose
    private String religion = "";
    @SerializedName("HaveChild")
    @Expose
    private String haveChild = "";
    @SerializedName("NoChild")
    @Expose
    private String noChild = "";
    @SerializedName("RecPariMag")
    @Expose
    private String recPariMag = "";
    @SerializedName("CreditRate")
    @Expose
    private String creditRate = "";
    @SerializedName("BriAniGiftRec")
    @Expose
    private String briAniGiftRec = "";
    @SerializedName("FestiMailReciv")
    @Expose
    private String festiMailReciv = "";
    @SerializedName("RegInsurPolicy")
    @Expose
    private String regInsurPolicy = "";
    @SerializedName("PolicyNo")
    @Expose
    private String policyNo = "";
    @SerializedName("Aadhar")
    @Expose
    private String aadhar = "";
    @SerializedName("Pan")
    @Expose
    private String pan = "";
    @SerializedName("WorkingWithDb")
    @Expose
    private String workingWithDb = "";
    @SerializedName("Dob")
    @Expose
    private String dob = "";
    @SerializedName("MarrAnni")
    @Expose
    private String marrAnni = "";
    @SerializedName("MOTSet")
    @Expose
    private List<ProfileMotherModel> mOTSet = null;
    @SerializedName("BROSet")
    @Expose
    private List<ProfileBrotherDetail> bROSet = null;
    @SerializedName("SISSet")
    @Expose
    private List<ProfileSisterDetail> sISSet = null;
    @SerializedName("WIFESet")
    @Expose
    private List<ProfileWifeDetail> wIFESet = null;
    @SerializedName("KIDSet")
    @Expose
    private List<ProfileChildModel> kIDSet = null;
    @SerializedName("FATSet")
    @Expose
    private List<ProfileFatherModel> fatSet = null;
    public CRMModel() {
        this.mOTSet = new ArrayList<>();
        this.bROSet = new ArrayList<>();
        this.wIFESet = new ArrayList<>();
        this.kIDSet = new ArrayList<>();
        this.sISSet = new ArrayList<>();
        this.fatSet = new ArrayList<>();
        this.mOTSet.add(new ProfileMotherModel());
        this.bROSet.add(new ProfileBrotherDetail());
        this.sISSet.add(new ProfileSisterDetail());
        this.kIDSet.add(new ProfileChildModel());
        this.wIFESet.add(new ProfileWifeDetail());
        this.fatSet.add(new ProfileFatherModel());
    }

    public List<ProfileFatherModel> getFatSet() {
        return fatSet;
    }

    public void setFatSet(List<ProfileFatherModel> fatSet) {
        this.fatSet = fatSet;
    }

    public String getFatherName() {
        if (fatSet != null && fatSet.size() > 0) {
            fatherName = fatSet.get(0).getName();
        }
        return fatherName;
    }

    public void setFatherName(String fatherName) {
        if (fatSet != null && fatSet.size() > 0) {
            fatSet.get(0).setName(fatherName);
        }
        this.fatherName = fatherName;
    }


    public String getBrotherName() {
        if (bROSet != null && bROSet.size() > 0) {
            brotherName = bROSet.get(0).getName();
        }
        return brotherName;
    }

    public void setBrotherName(String brotherName) {
        if (bROSet != null && bROSet.size() > 0) {
            bROSet.get(0).setName(brotherName);
        }
        this.brotherName = brotherName;
    }

    public String getSisterName() {
        if (sISSet != null && sISSet.size() > 0) {
            sisterName = sISSet.get(0).getName();
        }
        return sisterName;
    }

    public void setSisterName(String sisterName) {
        if (sISSet != null && sISSet.size() > 0) {
            sISSet.get(0).setName(sisterName);
        }
        this.sisterName = sisterName;
    }


    public String getMotherName() {
        if (mOTSet != null && mOTSet.size() > 0) {
            motherName = mOTSet.get(0).getName();
        }
        return motherName;
    }

    public void setMotherName(String motherName) {
        if (mOTSet != null && mOTSet.size() > 0) {
            mOTSet.get(0).setName(motherName);
        }
        this.motherName = motherName;
    }

    public String getFirstWifeName() {
        if (wIFESet != null && wIFESet.size() > 0) {
            firstWifeName = wIFESet.get(0).getName();
        }
        return firstWifeName;
    }

    public void setFirstWifeName(String firstWifeName) {
        if (wIFESet != null && wIFESet.size() > 0) {
            wIFESet.get(0).setName(firstWifeName);
        }
        this.firstWifeName = firstWifeName;
    }

    @Bindable
    public String getBpCode() {
        return bpCode;
    }

    public void setBpCode(String bpCode) {
        this.bpCode = bpCode;
        notifyPropertyChanged(com.app.samriddhi.BR.bp_code);
    }

    @Bindable
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }

    @Bindable
    public String getMobile1() {
        return mobile1;
    }

    public void setMobile1(String mobile1) {
        this.mobile1 = mobile1;
        notifyPropertyChanged(BR.mobile1);
    }

    @Bindable
    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
        notifyPropertyChanged(BR.owner);
    }

    @Bindable
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
        notifyPropertyChanged(BR.gender);
    }

    @Bindable
    public String getMobile2() {
        return mobile2;
    }

    public void setMobile2(String mobile2) {
        this.mobile2 = mobile2;
        notifyPropertyChanged(BR.mobile2);
    }

    @Bindable
    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
        notifyPropertyChanged(BR.maritalStatus);
    }

    @Bindable
    public String getEmergencyMob() {
        return emergencyMob;
    }

    public void setEmergencyMob(String emergencyMob) {
        this.emergencyMob = emergencyMob;
        notifyPropertyChanged(BR.emergencyMob);
    }

    @Bindable
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
        notifyPropertyChanged(BR.state);
    }

    @Bindable
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
        notifyPropertyChanged(BR.city);
    }

    @Bindable
    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
        notifyPropertyChanged(BR.pincode);
    }

    @Bindable
    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
        notifyPropertyChanged(BR.unit);
    }

    @Bindable
    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
        notifyPropertyChanged(BR.addr);
    }

    @Bindable
    public String getZcat() {
        return zcat;
    }

    public void setZcat(String zcat) {
        this.zcat = zcat;
        notifyPropertyChanged(BR.zcat);
    }

    @Bindable
    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
        notifyPropertyChanged(BR.religion);
    }

    @Bindable
    public String getHaveChild() {
        return haveChild;
    }

    public void setHaveChild(String haveChild) {
        this.haveChild = haveChild;
        notifyPropertyChanged(BR.haveChild);
    }

    @Bindable
    public String getNoChild() {
        return noChild;
    }

    public void setNoChild(String noChild) {
        this.noChild = noChild;
        notifyPropertyChanged(BR.noChild);
    }

    @Bindable
    public String getRecPariMag() {
        return recPariMag;
    }

    public void setRecPariMag(String recPariMag) {
        this.recPariMag = recPariMag;
        notifyPropertyChanged(BR.recPariMag);
    }

    @Bindable
    public String getCreditRate() {
        return creditRate;
    }

    public void setCreditRate(String creditRate) {
        this.creditRate = creditRate;
        notifyPropertyChanged(BR.creditRate);
    }

    @Bindable
    public String getBriAniGiftRec() {
        return briAniGiftRec;
    }

    public void setBriAniGiftRec(String briAniGiftRec) {
        this.briAniGiftRec = briAniGiftRec;
        notifyPropertyChanged(BR.briAniGiftRec);
    }

    @Bindable
    public String getFestiMailReciv() {
        return festiMailReciv;
    }

    public void setFestiMailReciv(String festiMailReciv) {
        this.festiMailReciv = festiMailReciv;
        notifyPropertyChanged(BR.festiMailReciv);
    }

    @Bindable
    public String getRegInsurPolicy() {
        return regInsurPolicy;
    }

    public void setRegInsurPolicy(String regInsurPolicy) {
        this.regInsurPolicy = regInsurPolicy;
        notifyPropertyChanged(BR.regInsurPolicy);
    }

    @Bindable
    public String getPolicyNo() {
        return policyNo;
    }

    public void setPolicyNo(String policyNo) {
        this.policyNo = policyNo;
        notifyPropertyChanged(BR.policyNo);
    }

    @Bindable
    public String getAadhar() {
        return aadhar;
    }

    public void setAadhar(String aadhar) {
        this.aadhar = aadhar;
        notifyPropertyChanged(BR.aadhar);
    }

    @Bindable
    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
        notifyPropertyChanged(BR.pan);
    }

    @Bindable
    public String getWorkingWithDb() {
        return workingWithDb;
    }

    public void setWorkingWithDb(String workingWithDb) {
        this.workingWithDb = workingWithDb;
        notifyPropertyChanged(BR.workingWithDb);
    }

    @Bindable
    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
        notifyPropertyChanged(BR.dob);
    }

    @Bindable
    public String getMarrAnni() {
        return marrAnni;
    }

    public void setMarrAnni(String marrAnni) {
        this.marrAnni = marrAnni;
        notifyPropertyChanged(BR.marrAnni);
    }

    @Bindable
    public List<ProfileMotherModel> getMOTSet() {
        return mOTSet;
    }

    public void setMOTSet(List<ProfileMotherModel> mOTSet) {
        this.mOTSet = mOTSet;
        notifyPropertyChanged(BR.mOTSet);
    }

    @Bindable
    public List<ProfileBrotherDetail> getBROSet() {
        return bROSet;
    }

    public void setBROSet(List<ProfileBrotherDetail> bROSet) {
        this.bROSet = bROSet;
        notifyPropertyChanged(BR.bROSet);
    }

    @Bindable
    public List<ProfileSisterDetail> getSISSet() {
        return sISSet;
    }

    public void setSISSet(List<ProfileSisterDetail> sISSet) {
        this.sISSet = sISSet;
        notifyPropertyChanged(BR.sISSet);
    }

    @Bindable
    public List<ProfileWifeDetail> getWIFESet() {
        return wIFESet;
    }

    public void setWIFESet(List<ProfileWifeDetail> wIFESet) {
        this.wIFESet = wIFESet;
        notifyPropertyChanged(com.app.samriddhi.BR.wIFESet);
    }

    @Bindable
    public List<ProfileChildModel> getKIDSet() {
        return kIDSet;
    }

    public void setKIDSet(List<ProfileChildModel> kIDSet) {
        this.kIDSet = kIDSet;
        notifyPropertyChanged(com.app.samriddhi.BR.kIDSet);
    }


}
