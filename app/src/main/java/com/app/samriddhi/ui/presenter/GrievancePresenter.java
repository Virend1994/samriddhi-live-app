package com.app.samriddhi.ui.presenter;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.response.BaseResponse;
import com.app.samriddhi.api.response.JsonArrayResponse;
import com.app.samriddhi.api.response.JsonObjectResponse;
import com.app.samriddhi.base.BasePresenter;
import com.app.samriddhi.ui.model.GrievanceCategoryModel;
import com.app.samriddhi.ui.model.GrievanceCreateIncidentModel;
import com.app.samriddhi.ui.model.GrievanceModel;
import com.app.samriddhi.ui.model.GrievanceOpenItemListModel;
import com.app.samriddhi.ui.model.IncidentCommentData;
import com.app.samriddhi.ui.model.IncidentDataModel;
import com.app.samriddhi.ui.view.IGrievanceView;
import com.app.samriddhi.util.Constant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GrievancePresenter extends BasePresenter<IGrievanceView> {
    public void getIncidentOptions() {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getIncidentOptions().enqueue(new Callback<JsonArrayResponse<GrievanceModel>>() {
            @Override
            public void onResponse(Call<JsonArrayResponse<GrievanceModel>> call, Response<JsonArrayResponse<GrievanceModel>> response) {
                getView().enableLoadingBar(false);
                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().list != null)
                            getView().onCategoryGetSuccess(response.body().list);
                        else
                            getView().onError(response.body().replyMsg);
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonArrayResponse<GrievanceModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });

    }


    public void getIncidentCategory() {

        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getIncidentCategoryType().enqueue(new Callback<JsonArrayResponse<GrievanceCategoryModel>>() {
            @Override
            public void onResponse(Call<JsonArrayResponse<GrievanceCategoryModel>> call, Response<JsonArrayResponse<GrievanceCategoryModel>> response) {
                getView().enableLoadingBar(false);
                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().list != null)
                            getView().onGetIncidentStatus(response.body().list);
                        else
                            getView().onError("Category Fetch Failed");
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonArrayResponse<GrievanceCategoryModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError("Category Fetch Failed");
            }
        });
    }


    public void createIncidence(GrievanceCreateIncidentModel mData, String newcomplaint) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().createGrievance(mData).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                getView().enableLoadingBar(false);
                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body() != null)
                            getView().onDataSubmitSuccess(response.body().replyMsg, newcomplaint);
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }


    public void getOpenItems(String bpCode, String statusCode, String opencomplaint) {
        getView().enableLoadingBar(true);

        SamriddhiApplication.getmInstance().getApiService().getOpenIncident(bpCode, statusCode).enqueue(new Callback<JsonArrayResponse<GrievanceOpenItemListModel>>() {
            @Override
            public void onResponse(Call<JsonArrayResponse<GrievanceOpenItemListModel>> call, Response<JsonArrayResponse<GrievanceOpenItemListModel>> response) {
                getView().enableLoadingBar(false);
                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().list != null)
                            getView().onOpenListItemSuccess(response.body().list, opencomplaint);
                        else
                            getView().onError(response.body().replyMsg);
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonArrayResponse<GrievanceOpenItemListModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });

    }


    public void getSubIncident(String bp_code, String statusCode, String categoryCode, String complaintType) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getSubIncidentData(bp_code, statusCode, categoryCode).enqueue(new Callback<JsonObjectResponse<IncidentDataModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<IncidentDataModel>> call, Response<JsonObjectResponse<IncidentDataModel>> response) {
                getView().enableLoadingBar(false);
                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onIncidentSubItemsList(response.body().body, complaintType);
                        else
                            getView().onError(response.body().replyMsg);
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<IncidentDataModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }


    public void getIncidentComments(String strIncidentId) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getIncidentComments(strIncidentId).enqueue(new Callback<JsonArrayResponse<IncidentCommentData>>() {
            @Override
            public void onResponse(Call<JsonArrayResponse<IncidentCommentData>> call, Response<JsonArrayResponse<IncidentCommentData>> response) {
                getView().enableLoadingBar(false);
                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().list != null)
                            getView().onIncidentCommentsSuccess(response.body().list);
                        else
                            getView().onError(response.body().replyMsg);
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonArrayResponse<IncidentCommentData>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }


    public void submitFeedback(GrievanceCreateIncidentModel data) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().updateGrievanceFeedback(data).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.isSuccessful() && response.code() == 200) {
                            if (response.body() != null)
                                getView().onDataSubmitSuccess(response.body().replyMsg, Constant.CLOSECOMPLAINT);
                            //getView().onInfo(response.body().replyMsg);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }


    public void submitReplyOnIncident(GrievanceCreateIncidentModel data, String openComplaint) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().submitCommentOnIncident(data).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.isSuccessful() && response.code() == 200) {
                            if (response.body() != null)
                                getView().onDataSubmitSuccess(response.body().replyMsg, openComplaint);
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }
}
