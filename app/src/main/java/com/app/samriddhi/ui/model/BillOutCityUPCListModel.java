package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BillOutCityUPCListModel extends BaseModel {



    @SerializedName("UnitName")
    @Expose
    private String UnitName;


    @SerializedName("UnitCode")
    @Expose
    private String UnitCode;



    @SerializedName("CityUPC")
    @Expose
    private String CityUPC;



    @SerializedName("CashCredit")
    @Expose
    private String CashCredit;



    @SerializedName("Billing")
    @Expose
    private String Billing;



    @SerializedName("Outstanding")
    @Expose
    private String Outstanding;

    @SerializedName("DChannel")
    @Expose
    private String DChannel;


    public String getUnitName() {
        return UnitName;
    }

    public void setUnitName(String unitName) {
        UnitName = unitName;
    }

    public String getUnitCode() {
        return UnitCode;
    }

    public void setUnitCode(String unitCode) {
        UnitCode = unitCode;
    }

    public String getCityUPC() {
        return CityUPC;
    }

    public void setCityUPC(String cityUPC) {
        CityUPC = cityUPC;
    }

    public String getCashCredit() {
        return CashCredit;
    }

    public void setCashCredit(String cashCredit) {
        CashCredit = cashCredit;
    }

    public String getBilling() {
        return Billing;
    }

    public void setBilling(String billing) {
        Billing = billing;
    }

    public String getOutstanding() {
        return Outstanding;
    }

    public void setOutstanding(String outstanding) {
        Outstanding = outstanding;
    }

    public String getDChannel() {
        return DChannel;
    }

    public void setDChannel(String DChannel) {
        this.DChannel = DChannel;
    }
}
