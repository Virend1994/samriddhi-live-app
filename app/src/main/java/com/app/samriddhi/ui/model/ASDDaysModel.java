package com.app.samriddhi.ui.model;

import java.io.Serializable;
import java.util.ArrayList;

public class ASDDaysModel implements Serializable {
    int  jj_flag,copies;
    String asdcollected;
    String commission ;


    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    ArrayList<ASDDaysEdition> edition_list;

    public ArrayList<ASDDaysEdition> getEditionMasterArray() {
        return edition_list;
    }

    public void setEditionMasterArray(ArrayList<ASDDaysEdition> editionMasterArray) {
        edition_list = editionMasterArray;
    }



    public int getJj_flag() {
        return jj_flag;
    }

    public void setJj_flag(int jj_flag) {
        this.jj_flag = jj_flag;
    }

    public int getCopies() {
        return copies;
    }

    public void setCopies(int copies) {
        this.copies = copies;
    }

    public String getAsdcollected() {
        return asdcollected;
    }

    public void setAsdcollected(String asdcollected) {
        this.asdcollected = asdcollected;
    }


}
