package com.app.samriddhi.ui.fragment;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;

import android.app.Fragment;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;

import com.app.samriddhi.R;
import com.app.samriddhi.api.response.JsonObjectResponse;
import com.app.samriddhi.databinding.FragmentNoOfCopyGraphBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.model.AgentResultModel;
import com.app.samriddhi.ui.model.BillOutAgentResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCDistWiseResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCResultModel;
import com.app.samriddhi.ui.model.BillingOutstandingResultModel;
import com.app.samriddhi.ui.model.CashCreditResultModel;
import com.app.samriddhi.ui.model.CityResultModel;
import com.app.samriddhi.ui.model.SalesDistrictWiseResultModel;
import com.app.samriddhi.ui.model.SalesOrgCityListModel;
import com.app.samriddhi.ui.model.SalesOrgCityResultModel;
import com.app.samriddhi.ui.model.StateListModel;
import com.app.samriddhi.ui.model.StateResultModel;
import com.app.samriddhi.ui.model.UsersResultModel;
import com.app.samriddhi.ui.presenter.UserTypeCopiesDetailPresenter;
import com.app.samriddhi.ui.view.IUserTypeCopiesView;
import com.app.samriddhi.util.MyYAxisValueFormatter;
import com.bumptech.glide.Glide;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.XAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.ArrayList;
import java.util.List;

public class NoOfCopyGraphFragment extends Fragment implements IUserTypeCopiesView {
    FragmentNoOfCopyGraphBinding mBinding;
    UserTypeCopiesDetailPresenter mPresenter;
    ProgressDialog progressDialog;

    protected Typeface tfLight;

    public NoOfCopyGraphFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_no_of_copy_graph, container, false);
        View view = mBinding.getRoot();
        mPresenter = new UserTypeCopiesDetailPresenter();
        mPresenter.setView(this);
        tfLight = Typeface.createFromAsset(getActivity().getAssets(), "OpenSans-Bold.ttf");
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadProgressBar(null, getString(R.string.loading), false);
        mPresenter.getStateList(PreferenceManager.getAgentId(getActivity()));

    }

    @Override
    public void onCitySuccess(CityResultModel mCityModel) {


    }
    @Override
    public void onSalesOrgCitySuccess(SalesOrgCityResultModel mData) {
        dismissProgressBar();
        setUninGraphDAta(mData.getSalesOrgCityList());

    }


    private void setUninGraphDAta(List<SalesOrgCityListModel> results) {
        if (getActivity() != null && !getActivity().isFinishing()) {

            mBinding.distBarchart.setDoubleTapToZoomEnabled(false);
            mBinding.distBarchart.setPinchZoom(false);
            mBinding.distBarchart.setHovered(false);
            mBinding.distBarchart.setDrawGridBackground(false);

            XAxis xAxis = mBinding.distBarchart.getXAxis();

            xAxis.setDrawLabels(true);

            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
            xAxis.setTextSize(8f);
            xAxis.setTypeface(tfLight);
            xAxis.setDrawGridLines(true);

            YAxis yAxis = mBinding.distBarchart.getAxisRight();
            yAxis.setEnabled(false);
            yAxis.setDrawGridLines(true);


            YAxis yAxis1 = mBinding.distBarchart.getAxisLeft();
            yAxis1.setEnabled(false);
            yAxis1.setDrawGridLines(true);

            ArrayList<BarEntry> values = new ArrayList<>();

            ArrayList<String> arrLAbles = new ArrayList<>();

            ArrayList<SalesOrgCityListModel> arrMonth = new ArrayList<>();

            if (results.size() > 0) {

                //Collections.reverse(mCityModel);

                int sizeCount = results.size();

                for (int i = 0; i < sizeCount; i++) {

                    arrMonth.add(results.get(i));
                }
                // Collections.reverse(arrMonth);
                for (int i = 0; i < arrMonth.size(); i++) {
                    arrLAbles.add((arrMonth.get(i).getUnitCode()));
                    values.add(new BarEntry(Float.parseFloat(arrMonth.get(i).getTodayNPS()), i));
                }

                xAxis.setValueFormatter(new XAxisValueFormatter() {
                    @Override
                    public String getXValue(String original, int index, ViewPortHandler viewPortHandler) {
                        return arrLAbles.get(index);
                    }
                });

                BarDataSet barData = new BarDataSet(values, "City");
                BarData data = new BarData(arrLAbles, barData);
                barData.setValueTextSize(9f);
                barData.setValueTypeface(tfLight);
                MyYAxisValueFormatter f = new MyYAxisValueFormatter();
                data.setValueFormatter(f);
                barData.setColors(ColorTemplate.COLORFUL_COLORS);


                mBinding.distBarchart.animateY(1000);

                mBinding.distBarchart.setData(data);
            }
            mBinding.distBarchart.setDescription("");
        }
    }

    @Override
    public void onStateSuccess(StateResultModel mStateModel) {
        dismissProgressBar();
        setData(mStateModel.getResults());
        setSpinnerData(mStateModel.getResults());

    }

    @Override
    public void onStateSuccess(BillingOutstandingResultModel mStateModel) {



    }

    @Override
    public void onAgentListSuccess(AgentResultModel mAgentResultModel) {
        // will override if require
    }

    @Override
    public void onBillOutAgentListSuccess(BillOutAgentResultModel mAgentResultModel) {

    }

    @Override
    public void onCreditCashSuccess(CashCreditResultModel mResultsData) {
        // will override if require
    }

    @Override
    public void onSuccess(JsonObjectResponse<UsersResultModel> body) {
        // will override if require
    }

    @Override
    public void onBillOUtCityUPCSuccess(JsonObjectResponse<BillOutCityUPCResultModel> body) {

    }



    @Override
    public void onSalesDistrictCopies(SalesDistrictWiseResultModel mResultData) {
        // will override if require
    }

    @Override
    public void onBillOutCityUPCDistSuccess(BillOutCityUPCDistWiseResultModel mResultData) {

    }


    private void setData(List<StateListModel> results) {

        if (getActivity() != null && !getActivity().isFinishing()) {

            mBinding.barchartStateCopies.setDoubleTapToZoomEnabled(false);
            mBinding.barchartStateCopies.setPinchZoom(false);
            mBinding.barchartStateCopies.setHovered(false);
            mBinding.barchartStateCopies.setDrawGridBackground(false);

            XAxis xAxis = mBinding.barchartStateCopies.getXAxis();

            xAxis.setDrawLabels(true);

            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

            xAxis.setDrawGridLines(false);

            xAxis.setTextSize(8f);
            xAxis.setTypeface(tfLight);
            xAxis.setDrawGridLines(true);

            YAxis yAxis = mBinding.barchartStateCopies.getAxisRight();

            yAxis.setEnabled(false);

            YAxis yAxis1 = mBinding.barchartStateCopies.getAxisLeft();

            yAxis1.setEnabled(false);

            ArrayList<BarEntry> values = new ArrayList<>();

            ArrayList<String> arrLAbles = new ArrayList<>();

            ArrayList<StateListModel> arrMonth = new ArrayList<>();

            if (results.size() > 0) {

                //Collections.reverse(results);

                int sizeCount = results.size();

                for (int i = 0; i < sizeCount; i++) {

                    arrMonth.add(results.get(i));
                }
               // Collections.reverse(arrMonth);
                for (int i = 0; i < arrMonth.size(); i++) {
                    arrLAbles.add((arrMonth.get(i).getStateCode()));
                    Log.e("sdgjdbgskgkshkghsdg", arrMonth.get(i).getTodayNPS());
                    values.add(new BarEntry(Float.parseFloat(arrMonth.get(i).getTodayNPS()), i));
                }

                xAxis.setValueFormatter(new XAxisValueFormatter() {
                    @Override
                    public String getXValue(String original, int index, ViewPortHandler viewPortHandler) {
                        return arrLAbles.get(index);
                    }
                });

                BarDataSet barData = new BarDataSet(values, "States");
                BarData data = new BarData(arrLAbles, barData);
                barData.setValueTextSize(9f);
                barData.setValueTypeface(tfLight);
                MyYAxisValueFormatter f = new MyYAxisValueFormatter();
                data.setValueFormatter(f);
                barData.setColors(ColorTemplate.COLORFUL_COLORS);


                mBinding.barchartStateCopies.animateY(1000);

                mBinding.barchartStateCopies.setData(data);
            }
            mBinding.barchartStateCopies.setDescription("");
        }
    }

    private void setSpinnerData(List<StateListModel> results) {


      try {
          ArrayList<String> arrMonth = new ArrayList<>();
          if(!results.isEmpty())
          {
              if (results.size() > 0) {
                  int sizeCount = results.size();
                  for (int i = 0; i < sizeCount; i++) {

                      arrMonth.add(results.get(i).getStateCode());
                  }}

              ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, arrMonth);
              mBinding.spinner.setAdapter(adapter); // this will set list of values to spinner
              mBinding.spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                  @Override
                  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                      // Toast.makeText(getActivity(), ""+parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();

                      setDistBarChart(parent.getItemAtPosition(position).toString());


                  }

                  @Override
                  public void onNothingSelected(AdapterView<?> parent) {

                  }
              });
          }
      }
      catch (Exception e)
      {

      }


    }
    private void setDistBarChart(String stateID) {
        loadProgressBar(null, getString(R.string.loading), false);
        mPresenter.getSalesOrgCity(PreferenceManager.getAgentId(getActivity()), stateID);

    }

    @Override
    public void enableLoadingBar(boolean enable) {

    }

    @Override
    public void onError(String reason) {

    }

    @Override
    public void onInfo(String message) {

    }

    @Override
    public void onTokenExpired() {

    }

    @Override
    public void onForceUpdate() {

    }

    @Override
    public void onSoftUpdate() {

    }

    public void loadProgressBar(String title, String message, boolean cancellable) {
        if (progressDialog == null && !getActivity().isFinishing())
            progressDialog = ProgressDialog.show(getActivity(), null, null, false, cancellable);
        progressDialog.setContentView(R.layout.progress_bar);
        ImageView imgLoader = progressDialog.findViewById(R.id.img_loader);
        Glide.with(this).load(R.drawable.loader_latest).into(imgLoader);
        // Glide.with(this).load(R.drawable.loader_new).into(imgLoader);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

    }

    public void dismissProgressBar() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        } catch (Exception e)
        {

        }

    }


}