package com.app.samriddhi.ui.activity.main.ui.Ledger;

public class AgentDetailModal {

    public  String tran_date;
    public  String tran_type;
    public  String tran_amt;
    public  String tran_reason;
    public  String tran_id;
    public  String sno;


    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getTran_date() {
        return tran_date;
    }

    public void setTran_date(String tran_date) {
        this.tran_date = tran_date;
    }

    public String getTran_type() {
        return tran_type;
    }

    public void setTran_type(String tran_type) {
        this.tran_type = tran_type;
    }

    public String getTran_amt() {
        return tran_amt;
    }

    public void setTran_amt(String tran_amt) {
        this.tran_amt = tran_amt;
    }

    public String getTran_reason() {
        return tran_reason;
    }

    public void setTran_reason(String tran_reason) {
        this.tran_reason = tran_reason;
    }

    public String getTran_id() {
        return tran_id;
    }

    public void setTran_id(String tran_id) {
        this.tran_id = tran_id;
    }
}
