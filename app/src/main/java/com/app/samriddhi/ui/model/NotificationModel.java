package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationModel extends BaseModel {

    @SerializedName("Nitification_Id")
    @Expose
    private Integer nitificationId;
    @SerializedName("usermaster")
    @Expose
    private Integer usermaster;
    @SerializedName("Notification_Category")
    @Expose
    private String notificationCategory;
    @SerializedName("State")
    @Expose
    private Object state;
    @SerializedName("Notification_Leng")
    @Expose
    private String notificationLeng;
    @SerializedName("SalseOrg")
    @Expose
    private Object salseOrg;
    @SerializedName("BP_Code")
    @Expose
    private String bPCode;
    @SerializedName("Notification_Title")
    @Expose
    private String notificationTitle;
    @SerializedName("Notification_To")
    @Expose
    private String notificationTo;
    @SerializedName("Notification_body")
    @Expose
    private String notificationBody;
    @SerializedName("Sent_Status")
    @Expose
    private Integer sentStatus;
    @SerializedName("Sent_On")
    @Expose
    private String sentOn;
    @SerializedName("Read_Status")
    @Expose
    private Integer readStatus;
    @SerializedName("Read_On")
    @Expose
    private Object readOn;
    @SerializedName("googleapis_Response_Code")
    @Expose
    private String googleapisResponseCode;
    @SerializedName("googleapis_Response_Body")
    @Expose
    private String googleapisResponseBody;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("create_date")
    @Expose
    private String createDate;
    @SerializedName("update_date")
    @Expose
    private String updateDate;
    @SerializedName("create_user")
    @Expose
    private Object createUser;
    @SerializedName("update_user")
    @Expose
    private Object updateUser;

    @SerializedName("Screen_Name")
    @Expose
    private String Screen_Name;


    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("url")
    @Expose
    private String url;


    public String getbPCode() {
        return bPCode;
    }

    public void setbPCode(String bPCode) {
        this.bPCode = bPCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getScreen_Name() {
        return Screen_Name;
    }

    public void setScreen_Name(String screen_Name) {
        Screen_Name = screen_Name;
    }

    public Integer getNitificationId() {
        return nitificationId;
    }

    public void setNitificationId(Integer nitificationId) {
        this.nitificationId = nitificationId;
    }

    public Integer getUsermaster() {
        return usermaster;
    }

    public void setUsermaster(Integer usermaster) {
        this.usermaster = usermaster;
    }

    public String getNotificationCategory() {
        return notificationCategory;
    }

    public void setNotificationCategory(String notificationCategory) {
        this.notificationCategory = notificationCategory;
    }

    public Object getState() {
        return state;
    }

    public void setState(Object state) {
        this.state = state;
    }

    public String getNotificationLeng() {
        return notificationLeng;
    }

    public void setNotificationLeng(String notificationLeng) {
        this.notificationLeng = notificationLeng;
    }

    public Object getSalseOrg() {
        return salseOrg;
    }

    public void setSalseOrg(Object salseOrg) {
        this.salseOrg = salseOrg;
    }

    public String getBPCode() {
        return bPCode;
    }

    public void setBPCode(String bPCode) {
        this.bPCode = bPCode;
    }

    public String getNotificationTitle() {
        return notificationTitle;
    }

    public void setNotificationTitle(String notificationTitle) {
        this.notificationTitle = notificationTitle;
    }

    public String getNotificationTo() {
        return notificationTo;
    }

    public void setNotificationTo(String notificationTo) {
        this.notificationTo = notificationTo;
    }

    public String getNotificationBody() {
        return notificationBody;
    }

    public void setNotificationBody(String notificationBody) {
        this.notificationBody = notificationBody;
    }

    public Integer getSentStatus() {
        return sentStatus;
    }

    public void setSentStatus(Integer sentStatus) {
        this.sentStatus = sentStatus;
    }

    public String getSentOn() {
        return sentOn;
    }

    public void setSentOn(String sentOn) {
        this.sentOn = sentOn;
    }

    public Integer getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(Integer readStatus) {
        this.readStatus = readStatus;
    }

    public Object getReadOn() {
        return readOn;
    }

    public void setReadOn(Object readOn) {
        this.readOn = readOn;
    }

    public String getGoogleapisResponseCode() {
        return googleapisResponseCode;
    }

    public void setGoogleapisResponseCode(String googleapisResponseCode) {
        this.googleapisResponseCode = googleapisResponseCode;
    }

    public String getGoogleapisResponseBody() {
        return googleapisResponseBody;
    }

    public void setGoogleapisResponseBody(String googleapisResponseBody) {
        this.googleapisResponseBody = googleapisResponseBody;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public Object getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Object createUser) {
        this.createUser = createUser;
    }

    public Object getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Object updateUser) {
        this.updateUser = updateUser;
    }


}
