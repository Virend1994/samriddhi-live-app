package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SalesOrgCityResultModel extends BaseModel {

    @SerializedName("results")
    @Expose
    private List<SalesOrgCityListModel> salesOrgCityList = null;

    public List<SalesOrgCityListModel> getSalesOrgCityList() {
        return salesOrgCityList;
    }

    public void setSalesOrgCityList(List<SalesOrgCityListModel> salesOrgCityList) {
        this.salesOrgCityList = salesOrgCityList;
    }
}
