package com.app.samriddhi.ui.activity.main.ui.Other;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.app.samriddhi.R;

import java.util.ArrayList;
import java.util.List;

public class ShowDetailActivity extends AppCompatActivity {

    RecyclerView recyclerview;
    DetailAdapter detailAdapter;
    LinearLayoutManager linearLayoutManager;
    List<DetailModal>list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_detail);

        recyclerview=findViewById(R.id.recyclerview);
        linearLayoutManager=new LinearLayoutManager(this);
        recyclerview.setHasFixedSize(true);
        recyclerview.setLayoutManager(linearLayoutManager);
         list=new ArrayList<>();
        for (int i = 0; i <30 ; i++) {
            DetailModal detailModal=new DetailModal();
            detailModal.setName("Indore");
            list.add(detailModal);
        }

        detailAdapter=new DetailAdapter(list,ShowDetailActivity.this);
        recyclerview.setAdapter(detailAdapter);
    }
}