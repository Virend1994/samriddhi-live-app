package com.app.samriddhi.ui.view;

import com.app.samriddhi.ui.model.EditionModel;
import com.app.samriddhi.ui.model.NumberOfCopiesModel;

public interface ICopiesView extends IView {
    void onCopiesSucces(EditionModel editionModel);

    void onNumberOfCopiesSuccess(NumberOfCopiesModel nModel, boolean fromSubmit);

    void onError(String reason, boolean fromSubmit);


}
