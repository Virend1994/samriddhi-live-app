package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentModel extends BaseModel {
    @SerializedName("Bp_code")
    @Expose
    private String bpCode;
    @SerializedName("Payment_Amount")
    @Expose
    private String paymentAmount;
    @SerializedName("Payment_Request")
    @Expose
    private String paymentRequest;
    @SerializedName("Payment_Response")
    @Expose
    private String paymentResponse;
    @SerializedName("Payment_diviceID")
    @Expose
    private String paymentDiviceID;
    @SerializedName("Payment_Status")
    @Expose
    private String paymentStatus;

    public String getBpCode() {
        return bpCode;
    }

    public void setBpCode(String bpCode) {
        this.bpCode = bpCode;
    }

    public String getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getPaymentRequest() {
        return paymentRequest;
    }

    public void setPaymentRequest(String paymentRequest) {
        this.paymentRequest = paymentRequest;
    }

    public String getPaymentResponse() {
        return paymentResponse;
    }

    public void setPaymentResponse(String paymentResponse) {
        this.paymentResponse = paymentResponse;
    }

    public String getPaymentDiviceID() {
        return paymentDiviceID;
    }

    public void setPaymentDiviceID(String paymentDiviceID) {
        this.paymentDiviceID = paymentDiviceID;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

}
