package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BillOutAgentResultModel extends BaseModel {
    @SerializedName("results")
    @Expose
    private List<BillOutAgentListModel> results = null;

    public List<BillOutAgentListModel> getResults() {
        return results;
    }

    public void setResults(List<BillOutAgentListModel> results) {
        this.results = results;
    }
}
