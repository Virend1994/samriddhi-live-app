package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ContactUsModel extends BaseModel {
    @SerializedName("ContactUs_Top")
    @Expose
    private String contactUsTop;
    @SerializedName("ContactUs_Number")
    @Expose
    private String contactUsNumber;
    @SerializedName("ContactUs_EmailID")
    @Expose
    private String contactUsEmailID;
    @SerializedName("ContactUs_Bottom")
    @Expose
    private String contactUsBottom;
    @SerializedName("ContactUs_Status")
    @Expose
    private Integer contactUsStatus;

    public String getContactUsTop() {
        return contactUsTop;
    }

    public void setContactUsTop(String contactUsTop) {
        this.contactUsTop = contactUsTop;
    }

    public String getContactUsNumber() {
        return contactUsNumber;
    }

    public void setContactUsNumber(String contactUsNumber) {
        this.contactUsNumber = contactUsNumber;
    }

    public String getContactUsEmailID() {
        return contactUsEmailID;
    }

    public void setContactUsEmailID(String contactUsEmailID) {
        this.contactUsEmailID = contactUsEmailID;
    }

    public String getContactUsBottom() {
        return contactUsBottom;
    }

    public void setContactUsBottom(String contactUsBottom) {
        this.contactUsBottom = contactUsBottom;
    }

    public Integer getContactUsStatus() {
        return contactUsStatus;
    }

    public void setContactUsStatus(Integer contactUsStatus) {
        this.contactUsStatus = contactUsStatus;
    }
}
