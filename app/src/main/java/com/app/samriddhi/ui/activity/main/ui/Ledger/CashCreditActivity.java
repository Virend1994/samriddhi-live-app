package com.app.samriddhi.ui.activity.main.ui.Ledger;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AppsContants;
import com.app.samriddhi.util.Constant;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Connection;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import okhttp3.Credentials;

public class CashCreditActivity extends BaseActivity {


    RecyclerView recyclerview;
    LinearLayoutManager linearLayoutManager;
    List<LedgerModal> ledgerModalList;
    CashCreditAdapter citiesAdapter;
    TextView tvDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_credit);

        ImageView imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        tvDate = findViewById(R.id.tvDate);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        tvDate.setText(dateFormat.format(cal.getTime()));



        recyclerview = findViewById(R.id.recyclerview);
        recyclerview.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(CashCreditActivity.this);
        recyclerview.setLayoutManager(linearLayoutManager);
        getCityUpc();
    }


    public void startActivityAnimation(Context context, Class destinationClass, boolean addFlags) {
        if (addFlags)
            startActivity(new Intent(context, destinationClass).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        else startActivity(new Intent(context, destinationClass));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }


    public void getCityUpc() {
        ledgerModalList = new ArrayList<>();
        enableLoadingBar(true);

        String strUserId= PreferenceManager.getAgentId(CashCreditActivity.this);

        AppsContants.sharedpreferences =getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strStateCode = AppsContants.sharedpreferences.getString(AppsContants.stateCode, "");
        String strUnitCode = AppsContants.sharedpreferences.getString(AppsContants.unitCode, "");

        // System.out.println("APICALL"+ Constant.BASE_PORTAL_URL+"ledger/api/all-in-one/"+strUserId+"?"+"s="+strStateCode);
        //AndroidNetworking.get(Constant.BASE_PORTAL_URL+"ledger/api/all-in-one/091098?s=GJ&u=AHME&cu=1")
        AndroidNetworking.get(Constant.BASE_PORTAL_URL+"ledger/api/all-in-one/"+strUserId+"?"+"s="+strStateCode+"&u="+strUnitCode)
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getString("message").equals("Records are available.")){
                                JSONObject jsonObjectMain=new JSONObject(response.getString("data"));
                                JSONArray jsonArray=jsonObjectMain.getJSONArray("results");
                                int pos=0;
                                ledgerModalList = new ArrayList<>();
                                for (int i = 0; i <jsonArray.length(); i++) {
                                    pos++;
                                    LedgerModal ledgerModal = new LedgerModal();
                                    JSONObject jsonObject=jsonArray.getJSONObject(i);
                                    ledgerModal.setSerial_num(pos+"");
                                    ledgerModal.setCity_name(jsonObject.getString("UnitName"));
                                    ledgerModal.setCity_code(jsonObject.getString("Unitcode"));
                                    ledgerModal.setMarket_code(jsonObject.getString("MarketCode"));
                                    ledgerModal.setMarket(jsonObject.getString("Market"));
                                    ledgerModal.setCity_amt(jsonObject.getString("ledger"));
                                    ledgerModal.setCityupc(jsonObject.getString("CityUPC"));
                                    ledgerModalList.add(ledgerModal);
                                }
                                citiesAdapter=new CashCreditAdapter(ledgerModalList,CashCreditActivity.this);
                                recyclerview.setAdapter(citiesAdapter);
                                enableLoadingBar(false);
                            }

                        }
                        catch (Exception ex){
                            enableLoadingBar(false);
                            Log.e("Sdfsdfs",ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        enableLoadingBar(false);
                        Log.e("Sdfsdfs",anError.getErrorBody());

                    }
                });

    }
}