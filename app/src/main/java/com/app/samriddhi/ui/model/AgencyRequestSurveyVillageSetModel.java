package com.app.samriddhi.ui.model;

public class AgencyRequestSurveyVillageSetModel {
    public String village_name;
    public String getVillage_name() {
        return village_name;
    }
    public void setVillage_name(String village_name) {
        this.village_name = village_name;
    }
    public int getVillage_copies() {
        return village_copies;
    }
    public void setVillage_copies(int village_copies) {
        this.village_copies = village_copies;
    }
    public int village_copies;
}
