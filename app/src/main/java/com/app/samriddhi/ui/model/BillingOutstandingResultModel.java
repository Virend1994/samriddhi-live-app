package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BillingOutstandingResultModel extends BaseModel {
    @SerializedName("results")
    @Expose
    private List<BillingOutstandingListModel> results = null;

    public List<BillingOutstandingListModel> getResults() {
        return results;
    }

    public void setResults(List<BillingOutstandingListModel> results) {
        this.results = results;
    }
}
