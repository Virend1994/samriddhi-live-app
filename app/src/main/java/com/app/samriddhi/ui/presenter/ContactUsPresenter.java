package com.app.samriddhi.ui.presenter;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.response.JsonArrayResponse;
import com.app.samriddhi.base.BasePresenter;
import com.app.samriddhi.ui.model.ContactUsData;
import com.app.samriddhi.ui.view.IContactUsView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactUsPresenter extends BasePresenter<IContactUsView> {

    /**
     * provides the Db contact data.
     */
    public void getContactData() {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getContactUs().enqueue(new Callback<JsonArrayResponse<ContactUsData>>() {
            @Override
            public void onResponse(Call<JsonArrayResponse<ContactUsData>> call, Response<JsonArrayResponse<ContactUsData>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().list != null)
                            getView().onSuccess(response.body().list);

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonArrayResponse<ContactUsData>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

}
