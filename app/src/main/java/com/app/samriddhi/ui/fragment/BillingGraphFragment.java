package com.app.samriddhi.ui.fragment;

import android.app.ProgressDialog;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import android.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import androidx.databinding.DataBindingUtil;

import com.app.samriddhi.R;
import com.app.samriddhi.api.response.JsonObjectResponse;
import com.app.samriddhi.databinding.FragmentBillingGraphBinding;
import com.app.samriddhi.databinding.FragmentNoOfCopyGraphBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.model.AgentResultModel;
import com.app.samriddhi.ui.model.BillOutAgentResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCDistWiseResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCResultModel;
import com.app.samriddhi.ui.model.BillingOutstandingListModel;
import com.app.samriddhi.ui.model.BillingOutstandingResultModel;
import com.app.samriddhi.ui.model.CashCreditResultModel;
import com.app.samriddhi.ui.model.CityListModel;
import com.app.samriddhi.ui.model.CityResultModel;
import com.app.samriddhi.ui.model.SalesDistrictWiseResultModel;
import com.app.samriddhi.ui.model.SalesOrgCityListModel;
import com.app.samriddhi.ui.model.SalesOrgCityResultModel;
import com.app.samriddhi.ui.model.StateListModel;
import com.app.samriddhi.ui.model.StateResultModel;
import com.app.samriddhi.ui.model.UsersResultModel;
import com.app.samriddhi.ui.presenter.UserTypeCopiesDetailPresenter;
import com.app.samriddhi.ui.view.IUserTypeCopiesView;
import com.app.samriddhi.util.MyYAxisValueFormatter;
import com.bumptech.glide.Glide;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.XAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.ArrayList;
import java.util.List;


public class BillingGraphFragment extends Fragment implements IUserTypeCopiesView {
    FragmentBillingGraphBinding mBinding;
    UserTypeCopiesDetailPresenter mPresenter;
    ProgressDialog progressDialog;

    protected Typeface tfLight;

    public BillingGraphFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_billing_graph, container, false);
        View view = mBinding.getRoot();
        mPresenter = new UserTypeCopiesDetailPresenter();
        mPresenter.setView(this);
        tfLight = Typeface.createFromAsset(getActivity().getAssets(), "OpenSans-Bold.ttf");
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        loadProgressBar(null, getString(R.string.loading), false);
        mPresenter.getBillingStateList(PreferenceManager.getAgentId(getActivity()));

    }

    @Override
    public void onCitySuccess(CityResultModel mCityModel) {
        dismissProgressBar();
        setStateDistBilling(mCityModel.getResults());

    }

    @Override
    public void onStateSuccess(StateResultModel mStateModel) {

    }

    @Override
    public void onSalesOrgCitySuccess(SalesOrgCityResultModel mData) {


    }


    private void setStateDistBilling(List<CityListModel> results) {
        if (getActivity() != null && !getActivity().isFinishing()) {

            mBinding.barchartDistrictBilling.setDoubleTapToZoomEnabled(false);
            mBinding.barchartDistrictBilling.setPinchZoom(false);
            mBinding.barchartDistrictBilling.setHovered(false);
            mBinding.barchartDistrictBilling.setDrawGridBackground(false);

            XAxis xAxis = mBinding.barchartDistrictBilling.getXAxis();

            xAxis.setDrawLabels(true);

            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

            xAxis.setDrawGridLines(true);

            xAxis.setTextSize(8f);
            xAxis.setTypeface(tfLight);

            YAxis yAxis = mBinding.barchartDistrictBilling.getAxisRight();

            yAxis.setEnabled(false);

            YAxis yAxis1 = mBinding.barchartDistrictBilling.getAxisLeft();

            yAxis1.setEnabled(false);
            yAxis1.setTypeface(tfLight);


            ArrayList<BarEntry> values = new ArrayList<>();

            ArrayList<String> arrLAbles = new ArrayList<>();

            ArrayList<CityListModel> arrMonth = new ArrayList<>();

            if (results.size() > 0) {

                //Collections.reverse(mCityModel);

                int sizeCount = results.size();

                for (int i = 0; i < sizeCount; i++) {

                    arrMonth.add(results.get(i));
                }
                // Collections.reverse(arrMonth);
                for (int i = 0; i < arrMonth.size(); i++) {
                    arrLAbles.add((arrMonth.get(i).getUnitCode()));
                    values.add(new BarEntry(Float.parseFloat(arrMonth.get(i).getBilling()), i));
                }

                xAxis.setValueFormatter(new XAxisValueFormatter() {
                    @Override
                    public String getXValue(String original, int index, ViewPortHandler viewPortHandler) {
                        return arrLAbles.get(index);
                    }
                });

                BarDataSet barData = new BarDataSet(values, "City");
                BarData data = new BarData(arrLAbles, barData);
                barData.setValueTextSize(9f);
                barData.setValueTypeface(tfLight);
                MyYAxisValueFormatter f = new MyYAxisValueFormatter();
                data.setValueFormatter(f);
                barData.setColors(ColorTemplate.COLORFUL_COLORS);


                mBinding.barchartDistrictBilling.animateY(1000);

                mBinding.barchartDistrictBilling.setData(data);
            }
            mBinding.barchartDistrictBilling.setDescription("");
        }
    }

    @Override
    public void onStateSuccess(BillingOutstandingResultModel mStateModel) {
        dismissProgressBar();
        setData(mStateModel.getResults());
        setSpinnerData(mStateModel.getResults());


    }

    @Override
    public void onAgentListSuccess(AgentResultModel mAgentResultModel) {
        // will override if require
    }

    @Override
    public void onBillOutAgentListSuccess(BillOutAgentResultModel mAgentResultModel) {

    }

    @Override
    public void onCreditCashSuccess(CashCreditResultModel mResultsData) {
        // will override if require
    }

    @Override
    public void onSuccess(JsonObjectResponse<UsersResultModel> body) {
        // will override if require
    }

    @Override
    public void onBillOUtCityUPCSuccess(JsonObjectResponse<BillOutCityUPCResultModel> body) {

    }


    @Override
    public void onSalesDistrictCopies(SalesDistrictWiseResultModel mResultData) {
        // will override if require
    }

    @Override
    public void onBillOutCityUPCDistSuccess(BillOutCityUPCDistWiseResultModel mResultData) {

    }


    private void setData(List<BillingOutstandingListModel> results) {

        if (getActivity() != null && !getActivity().isFinishing()) {

            mBinding.barchartStateBilling.setDoubleTapToZoomEnabled(false);
            mBinding.barchartStateBilling.setPinchZoom(false);
            mBinding.barchartStateBilling.setHovered(false);
            mBinding.barchartStateBilling.setDrawGridBackground(false);

            XAxis xAxis = mBinding.barchartStateBilling.getXAxis();

            xAxis.setDrawLabels(true);

            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

            xAxis.setDrawGridLines(true);

            xAxis.setTextSize(8f);
            xAxis.setTypeface(tfLight);

            YAxis yAxis = mBinding.barchartStateBilling.getAxisRight();

            yAxis.setEnabled(false);

            YAxis yAxis1 = mBinding.barchartStateBilling.getAxisLeft();

            yAxis1.setEnabled(false);
            yAxis1.setTypeface(tfLight);

            ArrayList<BarEntry> values = new ArrayList<>();

            ArrayList<String> arrLAbles = new ArrayList<>();

            ArrayList<BillingOutstandingListModel> arrMonth = new ArrayList<>();

            if (results.size() > 0) {

                //Collections.reverse(results);

                int sizeCount = results.size();

                for (int i = 0; i < sizeCount; i++) {

                    arrMonth.add(results.get(i));
                }
                // Collections.reverse(arrMonth);
                for (int i = 0; i < arrMonth.size(); i++) {
                    arrLAbles.add((arrMonth.get(i).getStateCode()));
                   // Log.e("sdgjdbgskgkshkghsdg", arrMonth.get(i).getBilling());
                    values.add(new BarEntry(Float.parseFloat(arrMonth.get(i).getBilling()), i));
                }

                xAxis.setValueFormatter(new XAxisValueFormatter() {
                    @Override
                    public String getXValue(String original, int index, ViewPortHandler viewPortHandler) {
                        return arrLAbles.get(index);
                    }
                });

                BarDataSet barData = new BarDataSet(values, "States");
                BarData data = new BarData(arrLAbles, barData);
                barData.setValueTextSize(9f);
                barData.setValueTypeface(tfLight);

                MyYAxisValueFormatter f = new MyYAxisValueFormatter();
                data.setValueFormatter(f);
                barData.setColors(ColorTemplate.COLORFUL_COLORS);
                mBinding.barchartStateBilling.animateY(1000);
                mBinding.barchartStateBilling.setData(data);
            }

            mBinding.barchartStateBilling.setDescription("");
        }
    }

    private void setSpinnerData(List<BillingOutstandingListModel> results) {
      try {
          ArrayList<String> arrMonth = new ArrayList<>();
          if (!results.isEmpty()) {
              if (results.size() > 0) {
                  int sizeCount = results.size();
                  for (int i = 0; i < sizeCount; i++) {

                      arrMonth.add(results.get(i).getStateCode());
                  }
              }

              ArrayAdapter<String> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, arrMonth);
              mBinding.spinnerBilling.setAdapter(adapter); // this will set list of values to spinner
              mBinding.spinnerBilling.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                  @Override
                  public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                      // Toast.makeText(getActivity(), ""+parent.getItemAtPosition(position).toString(), Toast.LENGTH_SHORT).show();

                      setDistBarChart(parent.getItemAtPosition(position).toString());


                  }

                  @Override
                  public void onNothingSelected(AdapterView<?> parent) {

                  }
              });
          }
      }catch (Exception e)
      {}


    }

    private void setDistBarChart(String stateID) {
        loadProgressBar(null, getString(R.string.loading), false);
        mPresenter.getBillingCityList(PreferenceManager.getAgentId(getActivity()), stateID);


    }

    @Override
    public void enableLoadingBar(boolean enable) {

    }

    @Override
    public void onError(String reason) {

    }

    @Override
    public void onInfo(String message) {

    }

    @Override
    public void onTokenExpired() {

    }

    @Override
    public void onForceUpdate() {

    }

    @Override
    public void onSoftUpdate() {

    }

    public void loadProgressBar(String title, String message, boolean cancellable) {
        if (progressDialog == null && !getActivity().isFinishing())
            progressDialog = ProgressDialog.show(getActivity(), null, null, false, cancellable);
        progressDialog.setContentView(R.layout.progress_bar);
        ImageView imgLoader = progressDialog.findViewById(R.id.img_loader);
        Glide.with(this).load(R.drawable.loader_latest).into(imgLoader);
        // Glide.with(this).load(R.drawable.loader_new).into(imgLoader);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

    }

    public void dismissProgressBar() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            progressDialog = null;
        } catch (Exception e)
        {

        }

    }

}