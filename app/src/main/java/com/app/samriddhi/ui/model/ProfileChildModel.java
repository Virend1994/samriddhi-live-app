package com.app.samriddhi.ui.model;

import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileChildModel extends BaseModel {
    @SerializedName("BpCode")
    @Expose
    private String bpCode = "";
    @SerializedName("Whom")
    @Expose
    private String whom = "";
    @SerializedName("Name")
    @Expose
    private String name = "";
    @SerializedName("Education")
    @Expose
    private String education = "";
    @SerializedName("Email")
    @Expose
    private String email = "";
    @SerializedName("Mobile1")
    @Expose
    private String mobile1 = "";
    @SerializedName("Mobile2")
    @Expose
    private String mobile2 = "";
    @SerializedName("EmergencyMob")
    @Expose
    private String emergencyMob = "";
    @SerializedName("Dob")
    @Expose
    private String dob = "";
    @SerializedName("MarrAnni")
    @Expose
    private String marrAnni = "";

    @SerializedName("Gender")
    @Expose
    private String Gender = "";

    @Bindable
    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
        notifyPropertyChanged(BR.gender);
    }

    public String getBpCode() {
        return bpCode;
    }

    public void setBpCode(String bpCode) {
        this.bpCode = bpCode;
    }

    public String getWhom() {
        return whom;
    }

    public void setWhom(String whom) {
        this.whom = whom;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile1() {
        return mobile1;
    }

    public void setMobile1(String mobile1) {
        this.mobile1 = mobile1;
    }

    public String getMobile2() {
        return mobile2;
    }

    public void setMobile2(String mobile2) {
        this.mobile2 = mobile2;
    }

    public String getEmergencyMob() {
        return emergencyMob;
    }

    public void setEmergencyMob(String emergencyMob) {
        this.emergencyMob = emergencyMob;
    }

    @Bindable
    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
        notifyPropertyChanged(com.app.samriddhi.BR.dob);
    }

    @Bindable
    public String getMarrAnni() {
        return marrAnni;
    }

    public void setMarrAnni(String marrAnni) {
        this.marrAnni = marrAnni;
        notifyPropertyChanged(BR.marrAnni);
    }


}
