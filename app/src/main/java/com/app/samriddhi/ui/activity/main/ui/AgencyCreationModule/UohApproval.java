package com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.samriddhi.R;
import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.Approval_Agency_Adapter;
import com.app.samriddhi.databinding.AgencyApprovalUohViewLayoutBinding;
import com.app.samriddhi.prefernces.PreferenceManager;

import com.app.samriddhi.ui.model.NewAgencyModel;
import com.app.samriddhi.ui.model.ProposedAgencyModel;
import com.app.samriddhi.util.Globals;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UohApproval extends BaseActivity implements Approval_Agency_Adapter.OnclickListener {
    NewAgencyModel dataView;

    AgencyApprovalUohViewLayoutBinding mBinding;

    Context context;
    UohApproval contextListener;

    ProposedAgencyModel data;
    Globals g = Globals.getInstance(context);
    Approval_Agency_Adapter adapter;
    AlertDialog alertDialog;
    int count = 0;
    boolean uohRemarkStatus = false;
    ArrayList<ProposedAgencyModel> newSrList;
    private ArrayList<ProposedAgencyModel> srList;
    Intent i;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.agency_approval_soh_view_layout);

        mBinding = DataBindingUtil.setContentView(this, R.layout.agency_approval_uoh_view_layout);
        mBinding.toolbar.setTitle("UOH- Approval Matrix ");
        srList = new ArrayList<>();
        newSrList = new ArrayList<>();
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initView();
    }

    @Override
    protected void onResume() {
        getAgentData();
        super.onResume();
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    public void initView() {
        i = getIntent();
        context = this;
        contextListener = this;
        dataView = (NewAgencyModel) i.getSerializableExtra("MyData");
        mBinding.tvUnit.setText(dataView.getUnit());

        if (dataView.getUOHApproval().equalsIgnoreCase("1")) {
            mBinding.uohTv.setTextColor(Color.parseColor("#00c851"));
            mBinding.uohTv.setText("Submitted");
            uohRemarkStatus = true;
        }
        else {
            uohRemarkStatus = false;
            mBinding.uohTv.setText("");
        }

        if (dataView.getHOApproval().equalsIgnoreCase("1")) {
            mBinding.tvHo.setTextColor(Color.parseColor("#00c851"));
            mBinding.tvHo.setText("Approved");

        } else {
            mBinding.tvHo.setText("Not Approved");
        }
        if (dataView.getSOHApproval().equalsIgnoreCase("1")) {
            mBinding.tvSoh.setTextColor(Color.parseColor("#00c851"));
            mBinding.tvSoh.setText("Approved");
        } else {
            mBinding.tvSoh.setText("Not Approved");
        }

        if (dataView.getUHFApproval().equalsIgnoreCase("1")) {
            mBinding.tvUfh.setTextColor(Color.parseColor("#00c851"));
            mBinding.tvUfh.setText("Approved");
        } else {
            mBinding.tvUfh.setText("Not Approved");
        }

        if (dataView.getCBRApproval().equalsIgnoreCase("1")) {
            mBinding.tvCbr.setTextColor(Color.parseColor("#00c851"));
            mBinding.tvCbr.setText("Approved");
        } else {
            mBinding.tvCbr.setText("Not Approved");
        }

        if (dataView.getBPClosure().equalsIgnoreCase("1")) {
            mBinding.tvBo.setText("Approved");
        } else {
            mBinding.tvBo.setText("Not Approved");
        }

        if (dataView.getBPClosure().equalsIgnoreCase("1")) {
            mBinding.tvBo.setText("Approved");
        } else {
            mBinding.tvBo.setText("Not Approved");
        }



        if(dataView.getUOHApproval().equals("1")){

            mBinding.btnlayout.setVisibility(View.GONE);
        }
        else {
            mBinding.btnlayout.setVisibility(View.VISIBLE);

        }

        mBinding.tvPopulation.setText(dataView.getPopulation());
        mBinding.tvTotalCopies.setText(dataView.getNoMainCopy());
        mBinding.tvReason.setText(dataView.getReason());
        mBinding.tvState.setText(dataView.getState());
        mBinding.tvUnit.setText(dataView.getUnit());
        mBinding.unitName.setText(dataView.getUnit());
        mBinding.tvCity.setText(dataView.getDistrict());
        mBinding.tvUpcCity.setText(dataView.getCity_upc());
       // mBinding.tvlocationName.setText(dataView.getLocation());
        mBinding.tvlocationName.setText(dataView.getTown());
        mBinding.agencyDetailsRec.setHasFixedSize(true);
        mBinding.agencyDetailsRec.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));


        g.unit_main = dataView.getUnitId();
        if (dataView.getCity_upc().equals("City")) {

            g.mainSaleGroup = "CRL";
        } else {

            g.mainSaleGroup = "CRU";
        }
        g.unitId = dataView.getUnitId();
        if (dataView.getSalesOfficeNameMain().length() == 0) {
            g.mainSaleOffice = "";
        } else {
            g.mainSaleOffice = dataView.getSalesOfficeNameMain();
        }


        if (dataView.getSalesOfficeNameJJ().length() == 0) {
            g.jjSaleOffice = "";
        } else {
            g.jjSaleOffice = dataView.getSalesOfficeNameJJ();
        }


        if (dataView.getJanJagrati().length() == 0) {
            g.jjSaleOfficeCopy = "0";
        } else {
            g.jjSaleOfficeCopy = dataView.getJanJagrati();
        }


        if (dataView.getMain().length() == 0) {
            g.mainSaleOfficeCopy = "0";
        } else {
            g.mainSaleOfficeCopy = dataView.getMain();
        }

        mBinding.btnReject.setOnClickListener(v -> {


            // Toast.makeText(context, "Coming soon", Toast.LENGTH_SHORT).show();

            if (mBinding.remarkCr.getText().toString().length() == 0) {
                alertMessage("Please enter the remark");
                return;
            }
            newSrList.size();

            if(newSrList.size()==0){
                alertMessage("Please Select Agency");
                return;

            }
            enableLoadingBar(true);

            count = 0;
            for (int i = 0; i < newSrList.size(); i++) {

                if (newSrList.get(i).isStatusCheck()) {
                    ProposedAgencyModel objData = new ProposedAgencyModel();

                    objData.setAg_detail_id(newSrList.get(i).getId());
                    objData.setAg_req_id(newSrList.get(i).getRequestId());

                    objData.setAgency_name(newSrList.get(i).getAgency_name());

                    objData.setAgenct_name(newSrList.get(i).getAgenct_name());
                    objData.setUOHApproval("2");

                    objData.setUOHRemark(mBinding.remarkCr.getText().toString());
                  //  objData.setUOHUpdateBy(PreferenceManager.getAgentId(context));
                    objData.setUOHUpdateBy(PreferenceManager.getPrefAgentName(context));

                    objData.setAgencyRequestCIRReq_count(newSrList.get(i).getAgencyRequestCIRReq_count());

                    objData.setAgencyRequestKYBP_count(newSrList.get(i).getAgencyRequestKYBP_count());

                    objData.setAgencyRequestSurvey_count(newSrList.get(i).getAgencyRequestSurvey_count());

                    objData.setAgencyRequestCIRReqJJ_count(newSrList.get(i).getAgencyRequestCIRReqJJ_count());
                    addRemarkData(objData, newSrList.get(i).getId());
                    count++;

                    Log.e("isStatusCheck" + i, String.valueOf(newSrList.get(i).isStatusCheck()));
                } else {
                    Log.e("isStatusCheck" + i, String.valueOf(newSrList.get(i).isStatusCheck()));
                    ProposedAgencyModel objData = new ProposedAgencyModel();

                    objData.setAg_detail_id(newSrList.get(i).getId());
                    objData.setAg_req_id(newSrList.get(i).getRequestId());

                    objData.setAgency_name(newSrList.get(i).getAgency_name());

                    objData.setAgenct_name(newSrList.get(i).getAgenct_name());
                    objData.setUOHApproval("0");

                    objData.setUOHRemark(mBinding.remarkCr.getText().toString());
                   // objData.setUOHUpdateBy(PreferenceManager.getAgentId(context));
                    objData.setUOHUpdateBy(PreferenceManager.getPrefAgentName(context));

                    objData.setAgencyRequestCIRReq_count(newSrList.get(i).getAgencyRequestCIRReq_count());
                    objData.setAgencyRequestKYBP_count(newSrList.get(i).getAgencyRequestKYBP_count());
                    objData.setAgencyRequestSurvey_count(newSrList.get(i).getAgencyRequestSurvey_count());
                    objData.setAgencyRequestCIRReqJJ_count(newSrList.get(i).getAgencyRequestCIRReqJJ_count());
                    addRemarkData(objData, newSrList.get(i).getId());

                    count++;
                }

            }

            enableLoadingBar(false);
            finish();
        });


        mBinding.btnSubmit.setOnClickListener(v -> {


            // Toast.makeText(context, "Coming soon", Toast.LENGTH_SHORT).show();

            if (mBinding.remarkCr.getText().toString().length() == 0) {
                alertMessage("Please enter the remark");
                return;
            }

            newSrList.size();
            Log.e("sdfsdfsdfsd",newSrList.size()+"");

            if(newSrList.size()==0){
                alertMessage("Please Select Agency");
                return;

            }
            enableLoadingBar(true);

            count = 0;
            for (int i = 0; i < newSrList.size(); i++) {

                if (newSrList.get(i).isStatusCheck()) {
                    ProposedAgencyModel objData = new ProposedAgencyModel();

                    objData.setAg_detail_id(newSrList.get(i).getId());
                    objData.setAg_req_id(newSrList.get(i).getRequestId());

                    objData.setAgency_name(newSrList.get(i).getAgency_name());

                    objData.setAgenct_name(newSrList.get(i).getAgenct_name());
                    objData.setUOHApproval("1");

                    objData.setUOHRemark(mBinding.remarkCr.getText().toString());
                   // objData.setUOHUpdateBy(PreferenceManager.getAgentId(context));
                    objData.setUOHUpdateBy(PreferenceManager.getPrefAgentName(context));


                    objData.setAgencyRequestCIRReq_count(newSrList.get(i).getAgencyRequestCIRReq_count());

                    objData.setAgencyRequestKYBP_count(newSrList.get(i).getAgencyRequestKYBP_count());

                    objData.setAgencyRequestSurvey_count(newSrList.get(i).getAgencyRequestSurvey_count());

                    objData.setAgencyRequestCIRReqJJ_count(newSrList.get(i).getAgencyRequestCIRReqJJ_count());


                    Date c = Calendar.getInstance().getTime();
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                    String formattedDate = df.format(c);
                    objData.setUOHUpdateOn(formattedDate+" 00:00:00.000000");
                    objData.setUser_id(PreferenceManager.getAgentId(this));
                    addRemarkData(objData, newSrList.get(i).getId());
                    count++;

                    Log.e("isStatusCheck" + i, String.valueOf(newSrList.get(i).isStatusCheck()));
                } else {
                    Log.e("isStatusCheck" + i, String.valueOf(newSrList.get(i).isStatusCheck()));
                    ProposedAgencyModel objData = new ProposedAgencyModel();

                    objData.setAg_detail_id(newSrList.get(i).getId());
                    objData.setAg_req_id(newSrList.get(i).getRequestId());

                    objData.setAgency_name(newSrList.get(i).getAgency_name());

                    objData.setAgenct_name(newSrList.get(i).getAgenct_name());
                    objData.setUOHApproval("0");

                    objData.setUOHRemark(mBinding.remarkCr.getText().toString());
                  //  objData.setUOHUpdateBy(PreferenceManager.getAgentId(context));
                    objData.setUOHUpdateBy(PreferenceManager.getPrefAgentName(context));

                    objData.setAgencyRequestCIRReq_count(newSrList.get(i).getAgencyRequestCIRReq_count());
                    objData.setAgencyRequestKYBP_count(newSrList.get(i).getAgencyRequestKYBP_count());
                    objData.setAgencyRequestSurvey_count(newSrList.get(i).getAgencyRequestSurvey_count());
                    objData.setAgencyRequestCIRReqJJ_count(newSrList.get(i).getAgencyRequestCIRReqJJ_count());

                    Date c = Calendar.getInstance().getTime();
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                    String formattedDate = df.format(c);
                    objData.setUOHUpdateOn(formattedDate+" 00:00:00.000000");
                    objData.setUser_id(PreferenceManager.getAgentId(this));
                    addRemarkData(objData, newSrList.get(i).getId());
                    count++;
                }
            }

            enableLoadingBar(false);
            finish();
        });

    }

    private void alertMessage(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })

                .show();
    }

    public void getAgentData() {


        srList.clear();
        // enableLoadingBar(true);

        Log.e("dataShow", dataView.getId());

        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getRequestData(dataView.getId());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    enableLoadingBar(false);

                    assert response.body() != null;
                    try {


                        JSONObject requestData = new JSONObject(response.body());

                        JSONArray jsonArray = requestData.getJSONArray("AgencyRequestAGDetail_set");
                        mBinding.agencyCount.setText(String.valueOf(jsonArray.length()));

                        double totalAsd=0;
                        double totalAsdCollected=0;
                        double totalPDC=0;
                        double difference=0;

                        if (jsonArray.length() > 0) {


                            for (int i = 0; i < jsonArray.length(); i++) {

                                ProposedAgencyModel objData = new ProposedAgencyModel();
                                JSONObject str = jsonArray.getJSONObject(i);
                                objData.setId(str.getString("ag_detail_id"));
                                objData.setAgency_name(str.getString("agency_name"));
                                objData.setCreatedUser(requestData.getString("create_user"));
                                objData.setAgenct_name(str.getString("agenct_name"));
                                objData.setTotal_copies(requestData.getString("tot_copies"));
                                objData.setMobile(str.getString("ag_mono_otp"));
                                objData.setRequestId(str.getString("ag_req_id"));
                                objData.setJj_copies(requestData.getString("jj_copies"));
                                objData.setMain_copies(requestData.getString("main_copies"));
                                objData.setStatusCheck(false);
                                JSONArray AgencyRequestCIRReq_set = str.getJSONArray("AgencyRequestCIRReq_set");
                                objData.setAgencyRequestCIRReq_set(AgencyRequestCIRReq_set);

                                //JSONArray AgencyRequestKYBP_count=str.getJSONArray("AgencyRequestKYBP_set");

                                // JSONArray AgencyRequestSurvey_set=str.getJSONArray("AgencyRequestSurvey_set");

                                objData.setAgencyRequestCIRReq_count(Integer.parseInt(str.getString("AgencyRequestCIRReq_count")));
                                objData.setAgencyRequestKYBP_count(Integer.parseInt(str.getString("AgencyRequestKYBP_count")));
                                objData.setAgencyRequestSurvey_count(Integer.parseInt(str.getString("AgencyRequestSurvey_count")));
                                objData.setAgencyRequestCIRReqJJ_count(Integer.parseInt(str.getString("AgencyRequestCIRReqJJ_count")));
                                objData.setAgreement_form_yn(Integer.parseInt(str.getString("agreement_form_yn")));

                                objData.setUOHRemark(str.getString("UOHRemark"));
                                objData.setUOHApproval(str.getString("UOHApproval"));

                                objData.setSOHRemark(str.getString("SOHRemark"));
                                objData.setSOHApproval(str.getString("SOHApproval"));

                                objData.setHORemark(str.getString("HORemark"));
                                objData.setHOApproval(str.getString("HOApproval"));


                                if (str.getString("UOHApproval").equalsIgnoreCase("1")) {
                                    mBinding.linearUOH.setVisibility(View.VISIBLE);
                                    objData.setStatusCheck(true);
                                    mBinding.tvUOHRemark.setText(str.getString("UOHRemark"));
                                }



                                if (str.getString("SOHApproval").equalsIgnoreCase("1")) {
                                    mBinding.linearSOH.setVisibility(View.VISIBLE);
                                    objData.setStatusCheck(true);
                                    mBinding.tvSohRemark.setText(str.getString("SOHRemark"));
                                }



                                if (str.getString("HOApproval").equalsIgnoreCase("1")) {
                                    mBinding.linearHO.setVisibility(View.VISIBLE);
                                    mBinding.tvHoRemark.setText(str.getString("HORemark"));
                                }

                               /* if (str.getString("HOApproval").equalsIgnoreCase("1")) {
                                    objData.setStatusCheck(true);
                                } else {
                                    objData.setStatusCheck(false);
                                }
                                if (str.getString("SOHApproval").equalsIgnoreCase("1")) {
                                    objData.setStatusCheck(true);
                                } else {
                                    objData.setStatusCheck(false);
                                }*/


                                Log.e("AgencyR", AgencyRequestCIRReq_set.toString());
                                for (int k = 0; k < AgencyRequestCIRReq_set.length(); k++) {
                                    JSONObject ar = AgencyRequestCIRReq_set.getJSONObject(k);
                                    objData.setCommission_amt(ar.getString("commission_amt"));
                                    objData.setCommission_per(ar.getString("commission_per"));
                                    objData.setAsd_collected(ar.getString("asd_collected"));
                                    objData.setAsd_as_per_norms(ar.getString("asd_as_per_norms"));
                                    totalAsd+=Double.parseDouble(ar.getString("asd_as_per_norms"));
                                    totalAsdCollected+=Double.parseDouble(ar.getString("asd_collected"));
                                }

                                for (int j = 0; j <AgencyRequestCIRReq_set.length() ; j++) {
                                    JSONObject obj = AgencyRequestCIRReq_set.getJSONObject(j);
                                    JSONArray jsonArray1=obj.getJSONArray("CirReqCheque_set");
                                    for (int k = 0; k <jsonArray1.length() ; k++) {
                                        JSONObject jsonObjects=jsonArray1.getJSONObject(k);
                                        totalPDC+=Double.parseDouble(jsonObjects.getString("cheque_amt"));
                                    }
                                }
                                // JSONObject ARCIR = AgencyRequestCIRReq_set.getJSONObject(0);

                                // Log.e("AgencyRequestCIRReq_set",AgencyRequestCIRReq_set.toString());
                                //objData.setCommission_per(ARCIR.getString("commission_per"));
                                // Log.e("commission_per",ARCIR.getString("commission_per"));

//
//                                objData.setCommission_per(AgencyRequestCIRReq_set.getJSONObject(0).getString("commission_per"));
//                                objData.setCommission_amt(AgencyRequestCIRReq_set.getJSONObject(0).getString("commission_amt"));
//                                objData.setAsd_as_per_norms(AgencyRequestCIRReq_set.getJSONObject(0).getString("asd_as_per_norms"));
//                                objData.setAsd_collected(AgencyRequestCIRReq_set.getJSONObject(0).getString("asd_collected"));


//                                objData.setAgencyRequestCIRReq_count(str.getInt("AgencyRequestCIRReq_count"));
//
//                                objData.setAgencyRequestKYBP_count(str.getInt("AgencyRequestKYBP_count"));
//
//                                objData.setAgencyRequestSurvey_count(str.getInt("AgencyRequestSurvey_count"));
//
//
//


                                srList.add(objData);


                            }


                            Log.e("sdfsdfsdfssdfs",totalAsd+"");
                            Log.e("sdfsdfsdfssdfs",totalAsdCollected+"");
                            mBinding.txtTotal.setText(totalAsd+"");
                            mBinding.txtCollected.setText(totalAsdCollected+"");

                            mBinding.txtPDC.setText(totalPDC+"");
                            BigDecimal c = BigDecimal.valueOf(totalAsd).subtract(BigDecimal.valueOf(totalAsdCollected));
                            BigDecimal total=c.subtract(BigDecimal.valueOf(totalPDC));

                            mBinding.txtDifference.setText(total+"");

                            adapter = new Approval_Agency_Adapter(context, srList, contextListener);
                            mBinding.agencyDetailsRec.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                        } else {

                        }

                        enableLoadingBar(false);
                        //  enableLoadingBar(false);
                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }


    @Override
    public void onItemClick(ProposedAgencyModel proposedAgencyModel, ArrayList<ProposedAgencyModel> list) {

//        if(uohRemarkStatus){
//            alertMessage("You can edit now until soh not review your remark ");
//
//
//        }else{
        if (dataView.getSOHApproval().equalsIgnoreCase("1") || dataView.getSOHApproval().equalsIgnoreCase("2")) {
            alertMessage("You Can not edit Now ");
        } else {
            if (proposedAgencyModel.isStatusCheck()) {
                // mBinding.btnlayout.setVisibility(View.VISIBLE);
            } else {
                // mBinding.btnlayout.setVisibility(View.GONE);
            }
        }
//        }


        data = new ProposedAgencyModel();
        data = proposedAgencyModel;
        newSrList = list;
        // addRemarkData(proposedAgencyModel);

    }

    @Override
    public void onEditClick(ProposedAgencyModel proposedAgencyModel) {

        Intent intentttt = new Intent(context, ViewRequestData.class);
        intentttt.putExtra("MyData",dataView);
        getIntent().getSerializableExtra("MyData");
        startActivity(intentttt);


        /*AlertDialog.Builder builder = new AlertDialog.Builder(context);

        View view1 = LayoutInflater.from(context).inflate(R.layout.edit_agency_approval_matrix, null);

        TextView otp_veri = view1.findViewById(R.id.otp_veri);
        TextView tvAgentName = view1.findViewById(R.id.tvAgentName);

        TextView kybp_form = view1.findViewById(R.id.kybp_form);
        TextView survey_form = view1.findViewById(R.id.survey_form);
        TextView cir_recipt = view1.findViewById(R.id.cir_recipt);
        TextView jj_cir = view1.findViewById(R.id.jj_cir);

        ImageView imgCross = view1.findViewById(R.id.open);
        otp_veri.setVisibility(View.GONE);
        imgCross.setImageResource(R.drawable.cross);
        tvAgentName.setText(proposedAgencyModel.getAgency_name());

        if(proposedAgencyModel.getJj_copies().equals("0")){
            jj_cir.setVisibility(View.GONE);
        }
        else {
            jj_cir.setVisibility(View.VISIBLE);
        }

        survey_form.setOnClickListener(v -> {

            if (proposedAgencyModel.getAgencyRequestSurvey_count() > 0) {
                g.SURVEYStatus = 1;
            } else {
                g.SURVEYStatus = 0;
            }

            g.ActionType = 2;
            Intent intent = new Intent(context, SurveyFormActivity.class);
            g.agentId = proposedAgencyModel.getId();

            g.agentNameStr = proposedAgencyModel.getAgenct_name();
            g.agencyNameStr = proposedAgencyModel.getAgency_name();
            g.agentMobileStr = proposedAgencyModel.getMobile();
            g.requestId = dataView.getId();
            startActivity(intent);

        });

        kybp_form.setOnClickListener(v -> {
            if (proposedAgencyModel.getAgencyRequestKYBP_count() > 0) {
                g.KYBPStatus = 1;
            } else {
                g.KYBPStatus = 0;
            }
            g.ActionType = 2;

            Intent intent = new Intent(context, KYBPActivity.class);
            g.agentId = proposedAgencyModel.getId();
            g.agentMobileStr = proposedAgencyModel.getMobile();
            g.agentNameStr = proposedAgencyModel.getAgenct_name();
            g.agencyNameStr = proposedAgencyModel.getAgency_name();
            g.requestId = dataView.getId();
            startActivity(intent);

        });
        cir_recipt.setOnClickListener(v -> {

            if (proposedAgencyModel.getAgencyRequestCIRReq_count() > 0) {
                g.CIRStatus = 1;
            } else {
                g.CIRStatus = 0;
            }
            g.ActionType = 2;

            Log.e("sdfsdfsdf",proposedAgencyModel.getId());
            Log.e("sdfsdfsdf",dataView.getId());
            Intent intent = new Intent(context, Circulation_MainForm.class);
            g.agentId = proposedAgencyModel.getId();
            g.agentNameStr = proposedAgencyModel.getAgenct_name();
            g.agencyNameStr = proposedAgencyModel.getAgency_name();
            g.agentMobileStr = proposedAgencyModel.getMobile();
            g.requestId = dataView.getId();
            startActivity(intent);

        });


        jj_cir.setOnClickListener(v -> {

            if (proposedAgencyModel.getAgencyRequestCIRReqJJ_count() > 0) {
                g.JJCIRStatus = 1;
            } else {
                g.JJCIRStatus = 0;
            }
            g.ActionType = 2;

            Intent intent = new Intent(context, Circulation_JJForm.class);
            g.agentId = proposedAgencyModel.getId();
            g.agentNameStr = proposedAgencyModel.getAgenct_name();
            g.agencyNameStr = proposedAgencyModel.getAgency_name();
            g.agentMobileStr = proposedAgencyModel.getMobile();
            g.requestId = dataView.getId();
            startActivity(intent);

        });


        imgCross.setOnClickListener(v -> {
            alertDialog.dismiss();
            alertDialog.cancel();
        });
        builder.setView(view1);
        alertDialog = builder.create();
        alertDialog.show();*/
    }

    @Override
    public void onDeleteClick(ProposedAgencyModel listData) {

        Map<String, Object> userData = new HashMap<>();
        userData.put("ag_detail_id", data.getId());
        userData.put("ag_req_id", data.getRequestId());
        userData.put("agenct_name", data.getAgenct_name());
        userData.put("agency_name", data.getAgency_name());


        userData.put("status", "0");
        agDelete(data.getId(), userData);

    }

    public void agDelete(String getId, Map<String, Object> userData) {
        //  http://103.96.220.236/agency/agdetail/64/


        Call<String> call = SamriddhiApplication.getmInstance().getApiService().agDelete(data.getId(), userData);


        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;


                    Log.e("dataShow", response.toString());


                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        // Toast.makeText(context, jsonObject.toString(), Toast.LENGTH_SHORT).show();

                        // String message = jsonObject.getString("message");

                        Toast.makeText(context, "Successfully Deleted", Toast.LENGTH_SHORT).show();


                        getAgentData();


                        //  enableLoadingBar(false);
                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void addRemarkData(ProposedAgencyModel objData, String agId) {
        //  http://103.96.220.236/agency/agdetail/64/


        Gson gson = new Gson();
        String data1 = gson.toJson(objData);
        Log.e("cir", data1);
        Log.e("sfhasbas",agId);


        Call<String> call = SamriddhiApplication.getmInstance().getApiService().addUOHRemark(agId, objData);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("dataShow", response.toString());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        //Toast.makeText(context, jsonObject.toString(), Toast.LENGTH_SHORT).show();
                        // String message = jsonObject.getString("message");
                        Toast.makeText(context, "Your Request Order SuccessFully generated ", Toast.LENGTH_SHORT).show();
                        //  enableLoadingBar(false);
                    }
                    catch (Exception e) {
                        enableLoadingBar(false);
                        e.printStackTrace();
                    }

                } else {
                    try {
                        Log.e("drdfgdgdfgfd", "682");
                        //Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        Log.e("drdfgdgdfgfd", "686");
                        // Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                    enableLoadingBar(false);
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }


}
