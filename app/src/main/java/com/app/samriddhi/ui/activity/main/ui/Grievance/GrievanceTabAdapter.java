package com.app.samriddhi.ui.activity.main.ui.Grievance;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class GrievanceTabAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;



    public GrievanceTabAdapter(@NonNull FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs=NumOfTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment=null;
        switch (position){
            case 0:

                ComplaintFragment complaintFragment=new ComplaintFragment();
                return complaintFragment;
            case 1:
                QueryFragment queryFragment=new QueryFragment();
                return queryFragment;

            case 2:
                SuggestionFragment suggestionFragment=new SuggestionFragment();
                return suggestionFragment;

            case 3:
                RequestFragment requestFragment=new RequestFragment();
                return requestFragment;


        }

        return null;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
