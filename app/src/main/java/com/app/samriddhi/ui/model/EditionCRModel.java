package com.app.samriddhi.ui.model;

import java.io.Serializable;

public class EditionCRModel implements Serializable {

    String pk,name,copy,edition_code,status;


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getEdition_code() {
        return edition_code;
    }

    public void setEdition_code(String edition_code) {
        this.edition_code = edition_code;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCopy() {
        return copy;
    }

    public void setCopy(String copy) {
        this.copy = copy;
    }
}
