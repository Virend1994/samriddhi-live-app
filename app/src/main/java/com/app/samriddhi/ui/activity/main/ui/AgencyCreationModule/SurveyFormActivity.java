package com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.app.samriddhi.R;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.Agency_target_Adapter;
import com.app.samriddhi.base.adapter.Village_Adapter;

import com.app.samriddhi.databinding.SurveyFormBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AppsContants;
import com.app.samriddhi.ui.model.AgencyRequestSurveyTargetSetModel;
import com.app.samriddhi.ui.model.AgencyRequestSurveyVillageSetModel;
import com.app.samriddhi.ui.model.BankListModel;
import com.app.samriddhi.ui.model.RootModel;
import com.app.samriddhi.ui.model.TargetModelClass;
import com.app.samriddhi.ui.model.VillageModel;

import com.app.samriddhi.util.AlertUtility;
import com.app.samriddhi.util.Globals;
import com.google.android.material.button.MaterialButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SurveyFormActivity extends BaseActivity implements Agency_target_Adapter.OnSetValueListner, Village_Adapter.OnSetValueVillageListner {

    SurveyFormBinding surveyFormBinding;

    Context context;


    RootModel rootModel;
    ArrayList<AgencyRequestSurveyTargetSetModel> agencyRequestSurveyTargetSetModels;
    ArrayList<AgencyRequestSurveyVillageSetModel> agencyRequestSurveyVillageSetModel;
    private int[] layouts;
    private int currentPage = 0;

    SurveyFormActivity mContextAdapter;
    public ArrayList<String> s_no1 = new ArrayList<String>();
    public ArrayList<String> s_no2 = new ArrayList<String>();
    public ArrayList<String> s_no3 = new ArrayList<String>();
    public ArrayList<TargetModelClass> targetModelClassArrayList;
    RecyclerView villageRec, agency_target;
    TextView addVillage, addAgentDet;

    LinearLayout uohMeet;

    RadioButton uoh_meeting_yes_rb, uoh_meeting_no_rb, vc_rb, personal_rb, call_rb;
    TextView no_of_copy;
    TextView agent_name, agency_name;
    EditText self_copy, railway_copy, cc_sf, remark_sf, vendorcopy, subag_copy;
    MaterialButton save_survey_data;
    LinearLayout TvTotalLayout;
    TextView tvTotalData, targetType, tvOneMonth, tvThreeMonth, tvTwoMonth, newag_name_sf, newag_code_sf, newcc_sf, newmonth1_exit, newmonth2_exit, newmonth3_exit;
    Globals g = Globals.getInstance(context);

    RelativeLayout pettyrecLayout;
    String ag_name_rec = "";
    String ag_code_rec = "";
    String cc_rec = "";
    String m1_rec = "";
    String m2_rec = "";
    String survey_id = "";
    String m3_rec = "";
    String rep_ag_name = "";
    String rep_ag_code = "";
    String rep_cc = "";
    String rep_m1 = "";
    String rep_m2 = "";
    String rep_m3 = "";
    String uoh_meeting_yesno = "";
    String uoh_meeting_type = "";
    String village_name = "";
    int noCopies, monthOne, monthTwo, monthThree, totalCopyAmount;
    int village_copy;
    String vendor_copy = "";
    Village_Adapter adapter;
    static int formstatus = 0;

    AlertDialog alertDialog;
    int totalCount = 0;
    EditText cc_total, m1_total, m2_total, m3_total;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setContentView(R.layout.survey_form);
        rootModel = new RootModel();

        if (g.AgencyRequestReasonSetModel.size() > 0) {
            Log.e("rootModel", g.AgencyRequestReasonSetModel.toString());
        }
        this.mContextAdapter = this;
        targetModelClassArrayList = new ArrayList<>();
        agencyRequestSurveyTargetSetModels = new ArrayList<>();
        agencyRequestSurveyVillageSetModel = new ArrayList<>();

        surveyFormBinding = DataBindingUtil.setContentView(this, R.layout.survey_form);
        // agencyCopyBifurcationBinding= DataBindingUtil.setContentView(this,R.layout.agency_copy_bifurcation);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        context = this;


        initViews();
        formstatus = g.SURVEYStatus;
        if (formstatus == 1) {
            getSurVeyData();
        } else {

        }
        surveyFormBinding.toolbar.setTitle("Survey Form ");
        setSupportActionBar(surveyFormBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


    }

    @Override
    public boolean onSupportNavigateUp() {

        showBackAlert();
        return true;
    }

    public void showBackAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        View view1 = LayoutInflater.from(context).inflate(R.layout.back_alert_layout, null);

        MaterialButton btnNo = view1.findViewById(R.id.btnNo);
        MaterialButton btnYes = view1.findViewById(R.id.btnYes);

        btnYes.setOnClickListener(v -> {
            alertDialog.dismiss();
            alertDialog.cancel();
            finish();
        });

        btnNo.setOnClickListener(V -> {
            alertDialog.dismiss();
            alertDialog.cancel();
        });
        builder.setView(view1);
        alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onBackPressed() {
        showBackAlert();

    }

    private void initViews() {


        addBottomDots(currentPage);
        layouts = new int[]{
                R.layout.agency_copy_bifurcation,
                R.layout.agency_target_mont_wise
        };
        MyViewPagerAdapter myViewPagerAdapter = new MyViewPagerAdapter();
        surveyFormBinding.viewPager.setAdapter(myViewPagerAdapter);
        surveyFormBinding.viewPager.setOffscreenPageLimit(layouts.length);

        surveyFormBinding.viewPager.addOnPageChangeListener(pageChangeListener);


        // vgView();

    }

    public void vgView() {
        if (g.ActionType == 2) {
            self_copy.setEnabled(false);
            railway_copy.setEnabled(false);
            cc_sf.setEnabled(false);
            remark_sf.setEnabled(false);
            vendorcopy.setEnabled(false);
            subag_copy.setEnabled(false);
        }
    }

    private void addBottomDots(int currentPage) {
        this.currentPage = currentPage;
    }

    private final ViewPager.OnPageChangeListener pageChangeListener =
            new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageSelected(int position) {
                    addBottomDots(position);
                }

                @Override
                public void onPageScrolled(int arg0, float arg1, int arg2) {
                }

                @Override
                public void onPageScrollStateChanged(int arg0) {
                }
            };


    @Override
    public void setTotalValue(ArrayList<TargetModelClass> srList) {

        Log.e("srList=>>", srList.toString());
        monthOne = 0;
        monthTwo = 0;
        monthThree = 0;
        noCopies = 0;

        // Toast.makeText(this,srList.size(),Toast.LENGTH_SHORT).show();Expansion//Intensification//Sub Agent Promotion

        if (g.reasonTypeName.equalsIgnoreCase("Expansion")) {

            for (int l = 0; l < srList.size(); l++) {

                noCopies += srList.get(l).getCur_copies();


                monthOne += srList.get(l).getMonth_one();
                monthTwo += srList.get(l).getMonth_two();
                monthThree += srList.get(l).getMonth_three();


            }


        }
        if (g.reasonTypeName.equalsIgnoreCase("Replacement")) {

            for (int l = 0; l < srList.size(); l++) {


                monthOne += srList.get(l).getMonth_one();
                monthTwo += srList.get(l).getMonth_two();
                monthThree += srList.get(l).getMonth_three();


            }


        }
        if (g.reasonTypeName.equalsIgnoreCase("Sub Agent Promotion")) {

            int totalSubAgent = 0;
            for (int l = 0; l < srList.size(); l++) {

                totalSubAgent = srList.get(srList.size() - 1).getCur_copies();

                if (l == (srList.size() - 1)) {

                } else {
                    noCopies += srList.get(l).getCur_copies();
                }
                //  noCopies=srList.get(0).getCur_copies()-srList.get(1).getCur_copies();
                monthOne += srList.get(l).getMonth_one();
                monthTwo += srList.get(l).getMonth_two();
                monthThree += srList.get(l).getMonth_three();


            }
            noCopies = noCopies - totalSubAgent;
        }
        if (g.reasonTypeName.equalsIgnoreCase("Intensification")) {
            int totalSubAgent = 0;

            for (int l = 0; l < srList.size(); l++) {

                totalSubAgent = srList.get(srList.size() - 1).getCur_copies();

                if (l == (srList.size() - 1)) {

                } else {
                    noCopies += srList.get(l).getCur_copies();
                }

                monthOne += srList.get(l).getMonth_one();
                monthTwo += srList.get(l).getMonth_two();
                monthThree += srList.get(l).getMonth_three();


            }
            noCopies = noCopies - totalSubAgent;
        }


        //int value = (int)Math.round(g.mainNoOfCopy)+noCopies;

        tvTotalData.setText(String.valueOf(noCopies));
        tvOneMonth.setText(String.valueOf(monthOne));
        tvTwoMonth.setText(String.valueOf(monthTwo));
        tvThreeMonth.setText(String.valueOf(monthThree));
    }

    @Override
    public void setVillageCopyTotal(ArrayList<VillageModel> srList) {

        totalCopyAmount = 0;
        for (int l = 0; l < srList.size(); l++) {
            totalCopyAmount += srList.get(l).getVillage_copy();
        }


    }

    private class MyViewPagerAdapter extends PagerAdapter {
        MyViewPagerAdapter() {
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            View v = LayoutInflater.from(SurveyFormActivity.this)
                    .inflate(layouts[position], container, false);

            switch (position) {
                case 0:
                    villageRec = v.findViewById(R.id.village_rec);
                    addVillage = v.findViewById(R.id.add_village);
                    agent_name = v.findViewById(R.id.survey_agent_name);
                    agency_name = v.findViewById(R.id.survey_agency_name);
                    no_of_copy = v.findViewById(R.id.no_of_copy_sf);
                    cc_sf = v.findViewById(R.id.cash_counter_copies);
                    subag_copy = v.findViewById(R.id.sub_agent_copy);
                    railway_copy = v.findViewById(R.id.railway_copies);
                    self_copy = v.findViewById(R.id.self_copies);
                    remark_sf = v.findViewById(R.id.remark_survey);
                    vendorcopy = v.findViewById(R.id.ven_copies);


                    agent_name.setText(g.agentNameStr);

                    agency_name.setText(g.agencyNameStr);

                    Double newData = new Double(g.mainNoOfCopy);
                    int perValue = newData.intValue();

                    no_of_copy.setText(String.valueOf(perValue));

                    uohMeet = v.findViewById(R.id.uohMeet);
                    RadioGroup rg_meeting = v.findViewById(R.id.uoh_meeting_yesno_rg);
                    uoh_meeting_yes_rb = v.findViewById(R.id.uoh_meeting_yes_rb);
                    uoh_meeting_no_rb = v.findViewById(R.id.uoh_meeting_no_rb);

                    uohMeet.setVisibility(View.VISIBLE);
                    uoh_meeting_yesno = "1";

                    rg_meeting.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            switch (checkedId) {
                                case R.id.uoh_meeting_yes_rb:
                                    uohMeet.setVisibility(View.VISIBLE);
                                    uoh_meeting_yesno = "1";
                                    break;
                                case R.id.uoh_meeting_no_rb:
                                    uohMeet.setVisibility(View.GONE);
                                    uoh_meeting_yesno = "0";
                                    break;
                            }
                        }
                    });
                    vc_rb = v.findViewById(R.id.vc_rb);
                    personal_rb = v.findViewById(R.id.personal_rb);
                    call_rb = v.findViewById(R.id.call_rb);
                    RadioGroup rg = v.findViewById(R.id.uoh_meeting_rg);
                    uoh_meeting_type = "1";
                    rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            switch (checkedId) {
                                case R.id.vc_rb:
                                    uoh_meeting_type = "1";
                                    break;
                                case R.id.personal_rb:
                                    uoh_meeting_type = "2";
                                    break;
                                case R.id.call_rb:
                                    uoh_meeting_type = "3";
                                    break;
                            }
                        }
                    });
                    // demoBinding.rec.setLayoutManager(new LinearLayoutManager(this));
                    s_no1.clear();
                    s_no1.add("1");


                    g.village_rec_datalist.clear();

                    for (int i = 0; i < s_no1.size(); i++) {
                        VillageModel map = new VillageModel();
                        map.setVillage_copy(0);
                        map.setVillage_name("");
                        g.village_rec_datalist.add(map);
                    }

                    villageRec.setHasFixedSize(true);
                    villageRec.setNestedScrollingEnabled(true);
                    villageRec.setLayoutManager(new LinearLayoutManager(SurveyFormActivity.this));
                    adapter = new Village_Adapter(SurveyFormActivity.this, g.village_rec_datalist, mContextAdapter);
                    villageRec.setAdapter(adapter);
                    adapter.notifyDataSetChanged();

                    addVillage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            s_no1.add("2");
                            VillageModel map = new VillageModel();
                            map.setVillage_name("");
                            map.setVillage_copy(0);
                            g.village_rec_datalist.add(map);
                            adapter.notifyDataSetChanged();
                        }
                    });
                    break;
                    case 1:
                    agency_target = v.findViewById(R.id.pettyrec);
                    save_survey_data = v.findViewById(R.id.survey_save_data);
                    tvTotalData = v.findViewById(R.id.tvTotalData);
                    targetType = v.findViewById(R.id.targetType);
                    newag_name_sf = v.findViewById(R.id.newag_name_sf);
                    newag_code_sf = v.findViewById(R.id.newag_code_sf);
                    newcc_sf = v.findViewById(R.id.newcc_sf);
                    TvTotalLayout = v.findViewById(R.id.TvTotalLayout);
                    newmonth1_exit = v.findViewById(R.id.newmonth1_exit);
                    newmonth2_exit = v.findViewById(R.id.newmonth2_exit);
                    newmonth3_exit = v.findViewById(R.id.newmonth3_exit);
                    pettyrecLayout = v.findViewById(R.id.pettyrecLayout);
                    newag_name_sf.setText(String.valueOf(g.mainAgencyName));
                    newag_code_sf.setText(String.valueOf(g.mainNoOfCopy));


                    tvOneMonth = v.findViewById(R.id.tvOneMonth);
                    tvThreeMonth = v.findViewById(R.id.tvThreeMonth);
                    tvTwoMonth = v.findViewById(R.id.tvTwoMonth);

                    s_no3.clear();
                    s_no3.add("1");
                    s_no3.add("1");

                    g.survet_form_rec_datalist.clear();
                    g.AgencyRequestReasonSetModel.toString();

                    Log.e("datare", g.reasonAdapterList.toString());
                    targetType.setText(" ( " + g.reasonTypeName + " )");
                    if (g.reasonTypeName.equalsIgnoreCase("Replacement")) {

                        TvTotalLayout.setVisibility(View.GONE);
                        for (int k = 0; k < g.reasonAdapterList.size(); k++) {


                            TargetModelClass objData = new TargetModelClass();

                            objData.setAgency_name(g.reasonAdapterList.get(k).getAg_name());
                            objData.setAgecy_code(g.reasonAdapterList.get(k).getAg_code());
                            objData.setCur_copies(Integer.parseInt(g.reasonAdapterList.get(k).getAg_copies()));
                            totalCount += Integer.parseInt(g.reasonAdapterList.get(k).getAg_copies());

                            objData.setMonth_one(0);
                            objData.setMonth_two(0);
                            objData.setMonth_three(0);


                            g.survet_form_rec_datalist.add(objData);
//
//
                        }
                        TargetModelClass objData1 = new TargetModelClass();

                        objData1.setAgency_name(g.agencyNameStr);
                        objData1.setAgecy_code("");
                        int value = (int) Math.round(g.mainNoOfCopy);
                        totalCount += value;
                        objData1.setCur_copies(value);
                        objData1.setMonth_one(0);
                        objData1.setMonth_two(0);
                        objData1.setMonth_three(0);
                        g.survet_form_rec_datalist.add(objData1);

                    }
                    if (g.reasonTypeName.equalsIgnoreCase("Expansion")) {
                        TvTotalLayout.setVisibility(View.GONE);
                        pettyrecLayout.setVisibility(View.VISIBLE);
                        TargetModelClass objData1 = new TargetModelClass();

                        objData1.setAgency_name(g.agencyNameStr);
                        objData1.setAgecy_code("");
                        int value = (int) Math.round(g.mainNoOfCopy);
                        totalCount += value;
                        objData1.setCur_copies(value);

                        objData1.setMonth_one(0);
                        objData1.setMonth_two(0);
                        objData1.setMonth_three(0);
                        g.survet_form_rec_datalist.add(objData1);


                    }

                    if (g.reasonTypeName.equalsIgnoreCase("Intensification")) {

                        for (int k = 0; k < g.reasonAdapterList.size(); k++) {

                            HashMap map = new HashMap();
                            TargetModelClass objData = new TargetModelClass();

                            objData.setAgency_name(g.reasonAdapterList.get(k).getAg_name());
                            objData.setAgecy_code(g.reasonAdapterList.get(k).getAg_code());
                            objData.setCur_copies(Integer.parseInt(g.reasonAdapterList.get(k).getAg_copies()));
                            totalCount += Integer.parseInt(g.reasonAdapterList.get(k).getAg_copies());
                            objData.setMonth_one(0);
                            objData.setMonth_two(0);
                            objData.setMonth_three(0);
//                            map.put("agency_name", g.AgencyRequestReasonSetModel.get(k).ag_code);
//                            map.put("agency_code", g.AgencyRequestReasonSetModel.get(k).ag_code);
//                            map.put("cc",g.AgencyRequestReasonSetModel.get(k).ag_code);
//                            map.put("m1", "");
//                            map.put("m2", "");
//                            map.put("m3", "");

                            g.survet_form_rec_datalist.add(objData);


                        }
                        TargetModelClass objData1 = new TargetModelClass();

                        objData1.setAgency_name(g.agencyNameStr);
                        objData1.setAgecy_code("");
                        int value = (int) Math.round(g.mainNoOfCopy);
                        totalCount += value;
                        objData1.setCur_copies(value);

                        objData1.setMonth_one(0);
                        objData1.setMonth_two(0);
                        objData1.setMonth_three(0);
                        g.survet_form_rec_datalist.add(objData1);


                    }
                    Log.e("tatla data", String.valueOf(totalCount));
                    tvTotalData.setText(String.valueOf(totalCount));
                    if (g.reasonTypeName.equalsIgnoreCase("Sub Agent Promotion")) {

                        for (int k = 0; k < g.reasonAdapterList.size(); k++) {

                            HashMap map = new HashMap();
                            TargetModelClass objData = new TargetModelClass();

                            objData.setAgency_name(g.agencyNameStr);
                            objData.setAgecy_code(g.reasonAdapterList.get(k).getSubag_code());
                            objData.setCur_copies(Integer.parseInt(g.reasonAdapterList.get(k).getSubag_copies()));
                            totalCount += Integer.parseInt(g.reasonAdapterList.get(k).getAg_copies());
                            objData.setMonth_one(0);
                            objData.setMonth_two(0);
                            objData.setMonth_three(0);
//                            map.put("agency_name", g.AgencyRequestReasonSetModel.get(k).ag_code);
//                            map.put("agency_code", g.AgencyRequestReasonSetModel.get(k).ag_code);
//                            map.put("cc",g.AgencyRequestReasonSetModel.get(k).ag_code);
//                            map.put("m1", "");
//                            map.put("m2", "");
//                            map.put("m3", "");

                            g.survet_form_rec_datalist.add(objData);


                        }
                        TargetModelClass objData1 = new TargetModelClass();

                        objData1.setAgency_name(g.agencyNameStr);
                        objData1.setAgecy_code("");
                        int value = (int) Math.round(g.mainNoOfCopy);
                        totalCount += value;
                        objData1.setCur_copies(value);

                        objData1.setMonth_one(0);
                        objData1.setMonth_two(0);
                        objData1.setMonth_three(0);
                        g.survet_form_rec_datalist.add(objData1);


                    }


                    agency_target.setHasFixedSize(true);
                    agency_target.setNestedScrollingEnabled(true);
                    agency_target.setLayoutManager(new LinearLayoutManager(SurveyFormActivity.this));
                    Agency_target_Adapter adapter2 = new Agency_target_Adapter(SurveyFormActivity.this, g.survet_form_rec_datalist, mContextAdapter);
                    agency_target.setAdapter(adapter2);
                    adapter2.notifyDataSetChanged();

                    save_survey_data.setVisibility(View.GONE);


                    break;


                case 2:


                    break;
            }

            container.addView(v);
            return v;
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
            return view == obj;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, @NonNull Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    public void getSurVeyData() {

        Log.e("sdsfsfsfd", g.agentId);
        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getSURVEYFORM(g.agentId);
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                enableLoadingBar(false);
                if (response.isSuccessful()) {

                    assert response.body() != null;

                    try {
                        JSONArray jsonArray = new JSONArray(response.body());


                        if (jsonArray.length() > 0) {


                            Log.e("jsonArray", jsonArray.toString());
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject obj = jsonArray.getJSONObject(i);
                                survey_id = obj.getString("survey_id");
                                cc_sf.setText(obj.getString("cash_copies"));
                                subag_copy.setText(obj.getString("subagenct_copies"));
                                remark_sf.setText(obj.getString("uoh_meeting_remark"));
                                Double newData = new Double(obj.getString("tot_copies"));
                                int perValue = newData.intValue();

                                no_of_copy.setText(String.valueOf(perValue));

                                vendorcopy.setText(obj.getString("vendor_copies"));

                                railway_copy.setText(obj.getString("railway_copies"));

                                self_copy.setText(obj.getString("self_copies"));
                                self_copy.setText(obj.getString("self_copies"));
                                JSONArray surveyVillage = new JSONArray(obj.getString("AgencyRequestSurveyVillage_set"));

                                try {

                                    if (surveyVillage.length() > 0) {
                                        g.village_rec_datalist.clear();
                                    }

                                    Log.e("jsonArray", surveyVillage.toString());


                                    for (int k = 0; k < surveyVillage.length(); k++) {

                                        JSONObject str = surveyVillage.getJSONObject(k);
                                        VillageModel map = new VillageModel();
                                        map.setVillage_copy(str.getInt("village_copies"));
                                        map.setVillage_name(str.getString("village_name"));
                                        g.village_rec_datalist.add(map);
                                    }

                                    villageRec.setHasFixedSize(true);
                                    villageRec.setNestedScrollingEnabled(true);
                                    villageRec.setLayoutManager(new LinearLayoutManager(SurveyFormActivity.this));
                                    adapter = new Village_Adapter(SurveyFormActivity.this, g.village_rec_datalist, mContextAdapter);
                                    villageRec.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();

                                } catch (Exception e) {
                                    alertMessage(e.getMessage());
                                }
                                JSONArray targetData = new JSONArray(obj.getString("AgencyRequestSurveyTarget_set"));
                                totalCount = 0;
                                try {
                                    if (targetData.length() > 0) {
                                        g.survet_form_rec_datalist.clear();
                                    }

                                    Log.e("jsonArray", surveyVillage.toString());


                                    monthOne = 0;
                                    monthTwo = 0;
                                    monthThree = 0;
                                    noCopies = 0;
                                    int totalSubAgent = 0;
                                    for (int k = 0; k < targetData.length(); k++) {

                                        JSONObject str = targetData.getJSONObject(k);
                                        HashMap map = new HashMap();
                                        TargetModelClass objData = new TargetModelClass();

                                        objData.setAgency_name(str.getString("agency_name"));
                                        objData.setAgecy_code(str.getString("agecy_code"));
                                        objData.setCur_copies(str.getInt("cur_copies"));


//                                        if(g.reasonTypeName.equalsIgnoreCase("Expansion")) {
//
//
//
//                                                noCopies+=srList.get(l).getCur_copies();
//
//
//                                                monthOne+=srList.get(l).getMonth_one();
//                                                monthTwo+=srList.get(l).getMonth_two();
//                                                monthThree+=srList.get(l).getMonth_three();
//
//
//
//
//
//                                        }
//                                        if(g.reasonTypeName.equalsIgnoreCase("Replacement")) {
//
//
//
//
//                                                monthOne+=srList.get(l).getMonth_one();
//                                                monthTwo+=srList.get(l).getMonth_two();
//                                                monthThree+=srList.get(l).getMonth_three();
//
//
//
//
//
//                                        }
//                                        if(g.reasonTypeName.equalsIgnoreCase("Sub Agent Promotion")) {
//
//                                                noCopies=srList.get(0).getCur_copies()-srList.get(1).getCur_copies();
//                                                monthOne+=srList.get(l).getMonth_one();
//                                                monthTwo+=srList.get(l).getMonth_two();
//                                                monthThree+=srList.get(l).getMonth_three();
//
//
//
//
//                                        }
                                        if (g.reasonTypeName.equalsIgnoreCase("Sub Agent Promotion")) {


                                            JSONObject str1 = targetData.getJSONObject(targetData.length() - 1);


                                            totalSubAgent = str1.getInt("cur_copies");

                                            if (k == (targetData.length() - 1)) {

                                            } else {
                                                noCopies += Integer.valueOf(str.getInt("cur_copies"));
                                            }


                                            monthOne += Integer.valueOf(str.getInt("month_one"));
                                            monthTwo += Integer.valueOf(str.getInt("month_two"));
                                            monthThree += Integer.valueOf(str.getInt("month_three"));


                                        }
                                        if (g.reasonTypeName.equalsIgnoreCase("Intensification")) {


                                            JSONObject str1 = targetData.getJSONObject(targetData.length() - 1);


                                            totalSubAgent = str1.getInt("cur_copies");
                                            //  Toast.makeText(SurveyFormActivity.this,"Test Bhs"+totalSubAgent,Toast.LENGTH_LONG).show();


                                            if (k == (targetData.length() - 1)) {

                                            } else {
                                                noCopies += Integer.valueOf(str.getInt("cur_copies"));
                                                //Toast.makeText(SurveyFormActivity.this, "Test Bhs" + str.getInt("cur_copies"), Toast.LENGTH_LONG).show();

                                            }


                                            monthOne += Integer.valueOf(str.getInt("month_one"));
                                            monthTwo += Integer.valueOf(str.getInt("month_two"));
                                            monthThree += Integer.valueOf(str.getInt("month_three"));


                                        }

                                        //  noCopies+=Integer.valueOf(str.getInt("cur_copies"));
                                        //totalCount+=str.getInt("cur_copies");


                                        objData.setMonth_one(str.getInt("month_one"));
                                        objData.setMonth_two(str.getInt("month_two"));
                                        objData.setMonth_three(str.getInt("month_three"));


                                        g.survet_form_rec_datalist.add(objData);


                                    }

                                    noCopies = noCopies - totalSubAgent;
                                    // Toast.makeText(this,srList.size(),Toast.LENGTH_SHORT).show();Expansion//Intensification//Sub Agent Promotion


                                    //int value = (int)Math.round(g.mainNoOfCopy)+noCopies;

                                    tvTotalData.setText(String.valueOf(noCopies));
                                    tvOneMonth.setText(String.valueOf(monthOne));
                                    tvTwoMonth.setText(String.valueOf(monthTwo));
                                    tvThreeMonth.setText(String.valueOf(monthThree));

                                    agency_target.setHasFixedSize(true);
                                    agency_target.setNestedScrollingEnabled(true);
                                    agency_target.setLayoutManager(new LinearLayoutManager(SurveyFormActivity.this));
                                    Agency_target_Adapter adapter2 = new Agency_target_Adapter(SurveyFormActivity.this, g.survet_form_rec_datalist, mContextAdapter);
                                    agency_target.setAdapter(adapter2);
                                    adapter2.notifyDataSetChanged();

                                } catch (Exception e) {

                                    alertMessage(e.getMessage());

                                }

                                if (obj.getString("UOH_meeting_flag").equalsIgnoreCase("1")) {
                                    uohMeet.setVisibility(View.VISIBLE);
                                    uoh_meeting_yesno = "1";
                                    uoh_meeting_yes_rb.setChecked(true);
                                    if (obj.getString("UOH_meeting_mode").equalsIgnoreCase("1")) {
                                        vc_rb.setChecked(true);
                                    }
                                    if (obj.getString("UOH_meeting_mode").equalsIgnoreCase("2")) {
                                        personal_rb.setChecked(true);
                                    }
                                    if (obj.getString("UOH_meeting_mode").equalsIgnoreCase("3")) {
                                        call_rb.setChecked(true);
                                    }

                                    remark_sf.setText(obj.getString("uoh_meeting_remark"));

                                } else {
                                    uohMeet.setVisibility(View.GONE);
                                    uoh_meeting_no_rb.setChecked(true);
                                    uoh_meeting_yesno = "0";
                                }


                            }
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void addSurveyForm(ArrayList<AgencyRequestSurveyVillageSetModel> villageSetModels, ArrayList<AgencyRequestSurveyTargetSetModel> agencyRequestSurveyTargetSetModels) {
        enableLoadingBar(true);
        Map<String, Object> params = new HashMap<>();
        if (formstatus == 1) {
            params.put("survey_id", survey_id);
        }


      /*  for (int i = 0; i <agencyRequestSurveyTargetSetModels.size() ; i++) {
            if (i == 1) {

                if (agencyRequestSurveyTargetSetModels.get(i).getMonth_two() < agencyRequestSurveyTargetSetModels.get(i).getMonth_one()) {

                    alertMessage("Second Month copies should be greater than previous month");
                    enableLoadingBar(false);
                    return;

                } else if (agencyRequestSurveyTargetSetModels.get(i).getMonth_three() < agencyRequestSurveyTargetSetModels.get(i).getMonth_two()) {

                    alertMessage("Third Month copies should be greater than previous month");
                    enableLoadingBar(false);
                    return;

                }
            }
        }*/
        params.put("ag_detail_id", g.agentId);
        params.put("tot_copies", Integer.parseInt(no_of_copy.getText().toString()));
        if (cc_sf.getText().toString().length() == 0) {
            params.put("cash_copies", 0);
        } else {
            params.put("cash_copies", Integer.parseInt(cc_sf.getText().toString()));
        }
        if (subag_copy.getText().toString().length() == 0) {
            params.put("subagenct_copies", 0);
        } else {
            params.put("subagenct_copies", Integer.parseInt(subag_copy.getText().toString()));
        }


        if (vendorcopy.getText().toString().length() == 0) {
            params.put("vendor_copies", 0);
        } else {
            params.put("vendor_copies", Integer.parseInt(vendorcopy.getText().toString()));
        }


        if (railway_copy.getText().toString().length() == 0) {
            params.put("railway_copies", 0);
        } else {
            params.put("railway_copies", Integer.parseInt(railway_copy.getText().toString()));
        }


        if (self_copy.getText().toString().length() == 0) {
            params.put("self_copies", 0);
        } else {
            params.put("self_copies", Integer.parseInt(self_copy.getText().toString()));
        }


        params.put("UOH_meeting_flag", Integer.parseInt(uoh_meeting_yesno));

        if (uoh_meeting_type.length() == 0) {
            params.put("UOH_meeting_mode", 0);
        } else {
            params.put("UOH_meeting_mode", Integer.parseInt(uoh_meeting_type));
        }

        if (remark_sf.getText().toString().length() == 0) {
            params.put("uoh_meeting_remark", "0");
        } else {
            params.put("uoh_meeting_remark", remark_sf.getText().toString());
        }


        params.put("SOH_meeting_flag", 0);

        params.put("SOH_meeting_mode", "0");

        params.put("SOH_meeting_remark", "0");

        params.put("AgencyRequestSurveyVillage_set", villageSetModels);
        params.put("AgencyRequestSurveyTarget_set", agencyRequestSurveyTargetSetModels);

        Log.e("bhs", params.toString());
        Call<String> call;


        if (formstatus == 1) {
            params.put("survey_id", survey_id);
            call = SamriddhiApplication.getmInstance().getApiService().updateSurveyRequest(survey_id, params);
        } else {
            call = SamriddhiApplication.getmInstance().getApiService().addSurveyRequest(params);
        }


        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;


                    Log.e("dataShow", response.toString());


                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        // Toast.makeText(SurveyFormActivity.this, jsonObject.toString(), Toast.LENGTH_SHORT).show();

                        // String message = jsonObject.getString("message");


                        Toast.makeText(SurveyFormActivity.this, "Your Survey Data SuccessFully Saved", Toast.LENGTH_SHORT).show();

                        enableLoadingBar(false);
                        finish();


                        //  enableLoadingBar(false);
                    } catch (Exception e) {
                        enableLoadingBar(false);
                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(SurveyFormActivity.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(SurveyFormActivity.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                    enableLoadingBar(false);
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(SurveyFormActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });

    }


    public int getTotalAllCopy() {

        int cc_sfInt = 0;
        if (cc_sf.getText().toString().length() == 0) {
            cc_sfInt = 0;
        } else {
            cc_sfInt = Integer.parseInt(cc_sf.getText().toString());
        }


        int sub_agent_copyCount = 0;

        if (subag_copy.getText().toString().length() == 0) {
            sub_agent_copyCount = 0;
        } else {
            sub_agent_copyCount = Integer.parseInt(subag_copy.getText().toString());
        }

        int railway_copiesCount = 0;

        if (railway_copy.getText().toString().length() == 0) {
            railway_copiesCount = 0;
        } else {
            railway_copiesCount = Integer.parseInt(railway_copy.getText().toString());
        }


        int self_copyCount = 0;

        if (self_copy.getText().toString().length() == 0) {
            self_copyCount = 0;
        } else {
            self_copyCount = Integer.parseInt(self_copy.getText().toString());
        }


        int vendor = 0;

        if (vendorcopy.getText().toString().length() == 0) {
            vendor = 0;
        } else {
            vendor = Integer.parseInt(vendorcopy.getText().toString());
        }


        int totalCountCopy = totalCopyAmount + cc_sfInt + sub_agent_copyCount + railway_copiesCount + self_copyCount + vendor;
        Log.e("sdfsdfsdf", totalCountCopy + "");

        return totalCountCopy;
    }

    private void alertMessage(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })
                .show();
    }


    public void onclick(View view) {
        switch (view.getId()) {
            case R.id.btn_next:

                surveyFormBinding.viewPager.setCurrentItem(currentPage - 1);
                Log.e("survey" + currentPage, String.valueOf(currentPage));
                if (currentPage + 1 == 2) {
                    surveyFormBinding.btnPrevious.setText("SAVE");
                    currentPage = 0;
                }

                if (currentPage == 0) {
                    showBackAlert();
                }
                break;
            case R.id.btn_previous:


                if (currentPage + 1 == 2) {

                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                    String strUOHStatus = AppsContants.sharedpreferences.getString(AppsContants.UOHApproveStatus, "");
                    String SOHApproveStatus = AppsContants.sharedpreferences.getString(AppsContants.SOHApproveStatus, "");
                    String HoApproveStatus = AppsContants.sharedpreferences.getString(AppsContants.HoApproveStatus, "");


                   /*  if (PreferenceManager.getPrefUserType(this).equals("UH")) {
                        if (strUOHStatus.equals("1")) {
                            surveyFormBinding.btnPrevious.setVisibility(View.GONE);
                        } else {
                            surveyFormBinding.btnPrevious.setVisibility(View.VISIBLE);
                            surveyFormBinding.btnPrevious.setText("SAVE");
                        }
                    } else if (PreferenceManager.getPrefUserType(this).equals("SH")) {
                        if (SOHApproveStatus.equals("1")) {
                            surveyFormBinding.btnPrevious.setVisibility(View.GONE);
                        } else {
                            surveyFormBinding.btnPrevious.setVisibility(View.VISIBLE);
                            surveyFormBinding.btnPrevious.setText("SAVE");
                        }

                    } else if (PreferenceManager.getPrefUserType(this).equals("HO")) {
                        if (HoApproveStatus.equals("1")) {
                            surveyFormBinding.btnPrevious.setVisibility(View.GONE);
                        } else {
                            surveyFormBinding.btnPrevious.setVisibility(View.VISIBLE);
                            surveyFormBinding.btnPrevious.setText("SAVE");
                        }
                    }
*/
                   surveyFormBinding.btnPrevious.setText("SAVE");
                    currentPage = 0;
                }

                if (surveyFormBinding.btnPrevious.getText().equals("SAVE")) {

                    agencyRequestSurveyTargetSetModels.clear();
                    String agnt_name = agent_name.getText().toString();
                    String agcy_name = agency_name.getText().toString();
                    String nocpy = no_of_copy.getText().toString();
                    String cc = cc_sf.getText().toString();
                    String rc = railway_copy.getText().toString();
                    String sc = self_copy.getText().toString();
                    String remark = remark_sf.getText().toString();
                    String ven_cpy = vendorcopy.getText().toString();
                    String sub_agent_copy = subag_copy.getText().toString();
                    int lastArray = g.survet_form_rec_datalist.size() - 1;


                    Log.e("dgdssdsssg", g.survet_form_rec_datalist.size() + "");

                    for (int p = 0; p < g.survet_form_rec_datalist.size(); p++) {
                        AgencyRequestSurveyTargetSetModel obj = new AgencyRequestSurveyTargetSetModel();
                        obj.setAgency_name(g.survet_form_rec_datalist.get(p).getAgency_name());
                        obj.setAgecy_code(g.survet_form_rec_datalist.get(p).getAgecy_code());
                        obj.setCur_copies(g.survet_form_rec_datalist.get(p).getCur_copies());

                        if (g.reasonTypeName.equalsIgnoreCase("Replacement") && p == lastArray) {

                           /* if (g.survet_form_rec_datalist.get(p).getMonth_one() == 0) {
                                alertMessage("Please Enter the first month of copy");
                                return;
                            }
                            if (g.survet_form_rec_datalist.get(p).getMonth_two() == 0) {
                                alertMessage("Please Enter the second month of copy");
                                return;
                            }
                            if (g.survet_form_rec_datalist.get(p).getMonth_three() == 0) {
                                alertMessage("Please Enter the third month of copy");
                                return;
                            }*/


                            if (p == 1) {

                                if (g.survet_form_rec_datalist.get(p).getMonth_one() == 0) {
                                    alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the first month of copy");
                                    return;
                                }

                                if (g.survet_form_rec_datalist.get(p).getMonth_one() < g.survet_form_rec_datalist.get(p).getCur_copies()) {

                                    alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"First month copies should be greater than or equal to current copies");
                                    return;
                                }

                                if (g.survet_form_rec_datalist.get(p).getMonth_two() == 0) {
                                    alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the second month of copy");
                                    return;
                                }

                                if (g.survet_form_rec_datalist.get(p).getMonth_two() < g.survet_form_rec_datalist.get(p).getMonth_one()) {

                                    alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Second month copies should be greater than or equal to current copies");
                                    return;
                                }

                                if (g.survet_form_rec_datalist.get(p).getMonth_three() == 0) {
                                    alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the third month of copy");
                                    return;
                                }

                                if (g.survet_form_rec_datalist.get(p).getMonth_three() < g.survet_form_rec_datalist.get(p).getMonth_two()) {

                                    alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Third month copies should be greater than or equal to current copies");
                                    return;
                                }
                            }
                        }

                        if (g.reasonTypeName.equalsIgnoreCase("Sub Agent Promotion") && p == lastArray) {

                            if (p == 1) {
                                if (g.survet_form_rec_datalist.get(p).getMonth_one() == 0) {
                                    alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the first month of copy");
                                    return;
                                }

                                if (g.survet_form_rec_datalist.get(p).getMonth_one() < g.survet_form_rec_datalist.get(p).getCur_copies()) {

                                    alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"First month copies should be greater than or equal to current copies");
                                    return;
                                }

                                if (g.survet_form_rec_datalist.get(p).getMonth_two() == 0) {
                                    alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the second month of copy");
                                    return;
                                }

                                if (g.survet_form_rec_datalist.get(p).getMonth_two() < g.survet_form_rec_datalist.get(p).getMonth_one()) {

                                    alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Second month copies should be greater than or equal to current copies");
                                    return;
                                }

                                if (g.survet_form_rec_datalist.get(p).getMonth_three() == 0) {
                                    alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the third month of copy");
                                    return;
                                }

                                if (g.survet_form_rec_datalist.get(p).getMonth_three() < g.survet_form_rec_datalist.get(p).getMonth_two()) {

                                    alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Third month copies should be greater than or equal to current copies");
                                    return;
                                }
                            }

                        }

                        if (g.reasonTypeName.equalsIgnoreCase("Expansion")) {
                            /*if (g.survet_form_rec_datalist.get(p).getMonth_one() == 0) {
                                alertMessage("Please Enter the first month of copy");
                                return;
                            }
                            if (g.survet_form_rec_datalist.get(p).getMonth_two() == 0) {
                                alertMessage("Please Enter the second month of copy");
                                return;
                            }
                            if (g.survet_form_rec_datalist.get(p).getMonth_three() == 0) {
                                alertMessage("Please Enter the third month of copy");
                                return;
                            }*/


                            if (p == 0) {
                                if (g.survet_form_rec_datalist.get(p).getMonth_one() == 0) {
                                    alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the first month of copy");
                                    return;
                                }

                                if (g.survet_form_rec_datalist.get(p).getMonth_one() < g.survet_form_rec_datalist.get(p).getCur_copies()) {

                                    alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"First month copies should be greater than or equal to current copies");
                                    return;
                                }

                                if (g.survet_form_rec_datalist.get(p).getMonth_two() == 0) {
                                    alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the second month of copy");
                                    return;
                                }

                                if (g.survet_form_rec_datalist.get(p).getMonth_two() < g.survet_form_rec_datalist.get(p).getMonth_one()) {

                                    alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Second month copies should be greater than or equal to current copies");
                                    return;
                                }

                                if (g.survet_form_rec_datalist.get(p).getMonth_three() == 0) {
                                    alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the third month of copy");
                                    return;
                                }

                                if (g.survet_form_rec_datalist.get(p).getMonth_three() < g.survet_form_rec_datalist.get(p).getMonth_two()) {

                                    alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Third month copies should be greater than or equal to current copies");
                                    return;
                                }
                            }
                        }


                        if (g.reasonTypeName.equalsIgnoreCase("Intensification")) {

                            /*Size 2*/
                            if(g.survet_form_rec_datalist.size()==2){
                                Log.e("dfgjskjbdkgsd","Size"+"  2");
                                if(p==0){

                                    if (g.survet_form_rec_datalist.get(p).getMonth_one() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the first month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_one() > g.survet_form_rec_datalist.get(p).getCur_copies()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"First month copies should be less than or equal to current copies");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_two() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the second month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_two() < g.survet_form_rec_datalist.get(p).getMonth_one()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Second month copies should be greater than or equal to Month one");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_three() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the third month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_three() < g.survet_form_rec_datalist.get(p).getMonth_two()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Third month copies should be greater than or equal to Month two");
                                        return;
                                    }
                                }
                                if(p==1){

                                    if (g.survet_form_rec_datalist.get(p).getMonth_one() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the first month of copy");
                                        return;
                                    }
                                    if (g.survet_form_rec_datalist.get(p).getMonth_one() < g.survet_form_rec_datalist.get(p).getCur_copies()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"First month copies should be greater than or equal to current copies");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_two() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the second month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_two() < g.survet_form_rec_datalist.get(p).getCur_copies()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Second month copies should be greater than or equal to current copies");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_three() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the third month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_three() < g.survet_form_rec_datalist.get(p).getCur_copies()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Third month copies should be greater than or equal to current copies");
                                        return;
                                    }
                                }
                            }

                            /*Size 3*/
                            if(g.survet_form_rec_datalist.size()==3){

                                Log.e("dfgjskjbdkgsd","Size"+"  3");

                                if(p==0){

                                    if (g.survet_form_rec_datalist.get(p).getMonth_one() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the first month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_one() > g.survet_form_rec_datalist.get(p).getCur_copies()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"First month copies should be less than or equal to current copies");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_two() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the second month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_two() < g.survet_form_rec_datalist.get(p).getMonth_one()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Second month copies should be greater than or equal to Month one");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_three() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the third month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_three() < g.survet_form_rec_datalist.get(p).getMonth_two()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Third month copies should be greater than or equal to Month two");
                                        return;
                                    }
                                }



                                if(p==1){

                                    if (g.survet_form_rec_datalist.get(p).getMonth_one() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the first month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_one() > g.survet_form_rec_datalist.get(p).getCur_copies()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"First month copies should be less than or equal to current copies");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_two() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the second month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_two() < g.survet_form_rec_datalist.get(p).getMonth_one()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Second month copies should be greater than or equal to Month one");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_three() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the third month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_three() < g.survet_form_rec_datalist.get(p).getMonth_two()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Third month copies should be greater than or equal to Month two");
                                        return;
                                    }
                                }

                                if(p==2){

                                    if (g.survet_form_rec_datalist.get(p).getMonth_one() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the first month of copy");
                                        return;
                                    }
                                    if (g.survet_form_rec_datalist.get(p).getMonth_one() < g.survet_form_rec_datalist.get(p).getCur_copies()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"First month copies should be greater than or equal to current copies");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_two() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the second month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_two() < g.survet_form_rec_datalist.get(p).getCur_copies()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Second month copies should be greater than or equal to current copies");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_three() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the third month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_three() < g.survet_form_rec_datalist.get(p).getCur_copies()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Third month copies should be greater than or equal to current copies");
                                        return;
                                    }
                                }

                            }

                            /*Size 4*/

                            if(g.survet_form_rec_datalist.size()==4){
                                Log.e("dfgjskjbdkgsd","Size"+"  4");
                                if(p==0){
                                    if (g.survet_form_rec_datalist.get(p).getMonth_one() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the first month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_one() > g.survet_form_rec_datalist.get(p).getCur_copies()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"First month copies should be less than or equal to current copies");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_two() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the second month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_two() < g.survet_form_rec_datalist.get(p).getMonth_one()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Second month copies should be greater than or equal to Month one");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_three() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the third month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_three() < g.survet_form_rec_datalist.get(p).getMonth_two()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Third month copies should be greater than or equal to Month two");
                                        return;
                                    }
                                }



                                if(p==1){

                                    if (g.survet_form_rec_datalist.get(p).getMonth_one() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the first month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_one() > g.survet_form_rec_datalist.get(p).getCur_copies()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"First month copies should be less than or equal to current copies");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_two() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the second month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_two() < g.survet_form_rec_datalist.get(p).getMonth_one()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Second month copies should be greater than or equal to Month one");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_three() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the third month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_three() < g.survet_form_rec_datalist.get(p).getMonth_two()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Third month copies should be greater than or equal to Month two");
                                        return;
                                    }
                                }


                                if(p==2){

                                    if (g.survet_form_rec_datalist.get(p).getMonth_one() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the first month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_one() > g.survet_form_rec_datalist.get(p).getCur_copies()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"First month copies should be less than or equal to current copies");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_two() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the second month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_two() < g.survet_form_rec_datalist.get(p).getMonth_one()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Second month copies should be greater than or equal to Month one");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_three() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the third month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_three() < g.survet_form_rec_datalist.get(p).getMonth_two()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Third month copies should be greater than or equal to Month two");
                                        return;
                                    }
                                }

                                if(p==3){

                                    if (g.survet_form_rec_datalist.get(p).getMonth_one() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the first month of copy");
                                        return;
                                    }
                                    if (g.survet_form_rec_datalist.get(p).getMonth_one() < g.survet_form_rec_datalist.get(p).getCur_copies()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"First month copies should be greater than or equal to current copies");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_two() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the second month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_two() < g.survet_form_rec_datalist.get(p).getCur_copies()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Second month copies should be greater than or equal to current copies");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_three() == 0) {
                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Please Enter the third month of copy");
                                        return;
                                    }

                                    if (g.survet_form_rec_datalist.get(p).getMonth_three() < g.survet_form_rec_datalist.get(p).getCur_copies()) {

                                        alertMessage(g.survet_form_rec_datalist.get(p).getAgency_name()+" "+"Third month copies should be greater than or equal to current copies");
                                        return;
                                    }
                                }

                            }

                        }

                        obj.setMonth_one(g.survet_form_rec_datalist.get(p).getMonth_one());
                        obj.setMonth_two(g.survet_form_rec_datalist.get(p).getMonth_two());
                        obj.setMonth_three(g.survet_form_rec_datalist.get(p).getMonth_three());
                        agencyRequestSurveyTargetSetModels.add(obj);

                    }
//
//
                    for (int i = 0; i < g.village_rec_datalist.size(); i++) {
                        AgencyRequestSurveyVillageSetModel obj = new AgencyRequestSurveyVillageSetModel();
                        village_name = g.village_rec_datalist.get(i).getVillage_name();
                        village_copy = g.village_rec_datalist.get(i).getVillage_copy();

                        obj.setVillage_name(village_name);
                        obj.setVillage_copies(village_copy);

                        agencyRequestSurveyVillageSetModel.add(obj);

                    }


                    AlertUtility.showAlert(this, "Are you sure want to save the data..", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            addSurveyForm(agencyRequestSurveyVillageSetModel, agencyRequestSurveyTargetSetModels);

                        }
                    });

                }
                if (currentPage + 1 == 1) {


                    int tc = Integer.parseInt(no_of_copy.getText().toString());
                    if (getTotalAllCopy() > tc || getTotalAllCopy() < tc) {
                        alertMessage("Mis Match Copy No : " + getTotalAllCopy() + "\nActual Total No Of Copy  : " + g.mainNoOfCopy);
                    } else {
                        if (uoh_meeting_yesno.length() == 0) {
                            alertMessage("Do you have any meeting with UOH?");
                            return;
                        }

                        if (uoh_meeting_yesno.length() > 0 && uoh_meeting_yesno.equalsIgnoreCase("1")) {
                            if (uoh_meeting_type.length() == 0) {
                                alertMessage("Please select meeting mode");
                                return;
                            }
                            if (remark_sf.getText().toString().length() == 0) {
                                alertMessage("Please enter remark");
                                return;
                            }


                        }
                        if (remark_sf.getText().toString().length() == 0) {
                            alertMessage("Please Enter the remark");
                            return;
                        }
                        for (int i = 0; i < g.village_rec_datalist.size(); i++) {

                            if (g.village_rec_datalist.get(i).getVillage_name().length() == 0 && g.village_rec_datalist.get(i).getVillage_copy() != 0 && g.village_rec_datalist.get(i).getVillage_copy() > 0) {
                                alertMessage("Please enter the village name");
                                return;
                            } else {
                                village_name = g.village_rec_datalist.get(i).getVillage_name();
                                village_copy = g.village_rec_datalist.get(i).getVillage_copy();
                            }


                        }
                        surveyFormBinding.viewPager.setCurrentItem(currentPage + 1);
                    }

                }

                Log.e("survey" + currentPage, String.valueOf(currentPage));


                break;

            default:
                break;
        }
    }

    public void formCheck(int currentPage) {
        surveyFormBinding.viewPager.setCurrentItem(currentPage - 1);
    }
}