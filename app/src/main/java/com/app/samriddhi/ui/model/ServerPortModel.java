package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServerPortModel extends BaseModel {

    @SerializedName("screenshot")
    @Expose
    public boolean screenshot;
    @SerializedName("server_text")
    @Expose
    private String serverText;
    @SerializedName("server_host")
    @Expose
    private String serverHost;
    @SerializedName("server_username")
    @Expose
    private String serverUsername;
    @SerializedName("server_password")
    @Expose
    private String serverPassword;

    public String getServerText() {
        return serverText;
    }

    public void setServerText(String serverText) {
        this.serverText = serverText;
    }

    public String getServerHost() {
        return serverHost;
    }

    public void setServerHost(String serverHost) {
        this.serverHost = serverHost;
    }

    public String getServerUsername() {
        return serverUsername;
    }

    public void setServerUsername(String serverUsername) {
        this.serverUsername = serverUsername;
    }

    public String getServerPassword() {
        return serverPassword;
    }

    public void setServerPassword(String serverPassword) {
        this.serverPassword = serverPassword;
    }

}
