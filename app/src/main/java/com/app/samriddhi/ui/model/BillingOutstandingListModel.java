package com.app.samriddhi.ui.model;

import android.view.View;

import com.app.samriddhi.base.model.BaseModel;
import com.app.samriddhi.util.Constant;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BillingOutstandingListModel extends BaseModel {

    @SerializedName("StateName")
    @Expose
    private String StateName;


    @SerializedName("StateCode")
    @Expose
    private String StateCode;



    @SerializedName("BIllingMonth")
    @Expose
    private String BIllingMonth;



    @SerializedName("Billing")
    @Expose
    private String Billing;



    @SerializedName("Outstanding")
    @Expose
    private String Outstanding;

    public String getStateName() {
        return StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }

    public String getStateCode() {
        return StateCode;
    }

    public void setStateCode(String stateCode) {
        StateCode = stateCode;
    }

    public String getBIllingMonth() {
        return BIllingMonth;
    }

    public void setBIllingMonth(String BIllingMonth) {
        this.BIllingMonth = BIllingMonth;
    }

    public String getBilling() {
        return Billing;
    }

    public void setBilling(String billing) {
        Billing = billing;
    }

    public String getOutstanding() {
        return Outstanding;
    }

    public void setOutstanding(String outstanding) {
        Outstanding = outstanding;
    }
}
