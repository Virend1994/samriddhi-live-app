package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserSAPStatus extends BaseModel {

    @SerializedName("results")
    @Expose
    private List<SapResults> results = null;


    public List<SapResults> getResults() {
        return results;
    }

    public void setResults(List<SapResults> results) {
        this.results = results;
    }
}
