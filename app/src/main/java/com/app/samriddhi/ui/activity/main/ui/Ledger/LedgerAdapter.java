package com.app.samriddhi.ui.activity.main.ui.Ledger;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AppsContants;
import com.app.samriddhi.ui.activity.main.ui.LedgerActivity;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.DailyPoActivity;
import com.google.android.material.card.MaterialCardView;

import java.util.List;

public class LedgerAdapter extends RecyclerView.Adapter<LedgerAdapter.ViewHolder> {
    Context context;
    List<LedgerModal> dataAdapters;

    public LedgerAdapter(List<LedgerModal> getDataAdapter, Context context) {
        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;
    }

    @Override
    public LedgerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ledger_item_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, @SuppressLint("RecyclerView") int position) {

        LedgerModal dataAdapterOBJ = dataAdapters.get(position);
        Viewholder.setIsRecyclable(false);
        // Viewholder.tvEdtion.setText(dataAdapterOBJ.getEdi_name());
        Viewholder.txtSerial.setText(dataAdapterOBJ.getSerial_num());
        Viewholder.txtStateName.setText(dataAdapterOBJ.getState_name());
        Viewholder.txtAmount.setText(dataAdapterOBJ.getState_amt());
        Viewholder.cardViewMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof StatesActivity) {
                   // ((StatesActivity)context).startActivityAnimation(context, LedgerActivity.class, false);
                    AppsContants.sharedpreferences=context.getSharedPreferences(AppsContants.MyPREFERENCES,Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor=AppsContants.sharedpreferences.edit();
                    editor.putString(AppsContants.stateCode,dataAdapterOBJ.getState_code());
                    editor.commit();

                    ((StatesActivity)context).startActivityAnimation(context, CityActivity.class, false);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataAdapters.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtSerial,txtStateName,txtAmount;
        MaterialCardView cardViewMain;
        public ViewHolder(View itemView) {
            super(itemView);
            txtSerial = itemView.findViewById(R.id.txtSerial);
            txtStateName = itemView.findViewById(R.id.txtStateName);
            txtAmount = itemView.findViewById(R.id.txtAmount);
            cardViewMain = itemView.findViewById(R.id.cardViewMain);
        }
    }
}