package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;

public class NewLedgerModel {

    public String Kunnr;
    public String OpenBalance;
    public String VDate;
    public String Narration;
    public String Debit;
    public String Credit;
    public String Balance;


    public String getKunnr() {
        return Kunnr;
    }

    public void setKunnr(String kunnr) {
        Kunnr = kunnr;
    }

    public String getOpenBalance() {
        return OpenBalance;
    }

    public void setOpenBalance(String openBalance) {
        OpenBalance = openBalance;
    }

    public String getVDate() {
        return VDate;
    }

    public void setVDate(String VDate) {
        this.VDate = VDate;
    }

    public String getNarration() {
        return Narration;
    }

    public void setNarration(String narration) {
        Narration = narration;
    }

    public String getDebit() {
        return Debit;
    }

    public void setDebit(String debit) {
        Debit = debit;
    }

    public String getCredit() {
        return Credit;
    }

    public void setCredit(String credit) {
        Credit = credit;
    }

    public String getBalance() {
        return Balance;
    }

    public void setBalance(String balance) {
        Balance = balance;
    }
}
