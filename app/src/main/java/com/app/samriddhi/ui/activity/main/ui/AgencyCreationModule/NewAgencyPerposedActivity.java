package com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.samriddhi.R;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.Agency_new_perposal_Adapter;
import com.app.samriddhi.databinding.ActivityNewAgencyPerposedBinding;
import com.app.samriddhi.ui.model.RootModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewAgencyPerposedActivity extends BaseActivity {

    ActivityNewAgencyPerposedBinding agencyPerposedBinding;

    ArrayList<RootModel> arrayList;
    public ArrayList<String> s_no1 = new ArrayList<String>();

    Agency_new_perposal_Adapter adapter;
    Context context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_agency_perposed);
        context=this;
        arrayList=new ArrayList<>();

        agencyPerposedBinding= DataBindingUtil.setContentView(this, R.layout.activity_new_agency_perposed);
        initViews();
        getAgencyData();

    }

    private void initViews() {

        s_no1.clear();
        s_no1.add("1");
        s_no1.add("1");
        s_no1.add("1");


        agencyPerposedBinding.agencyPerposalRec.setHasFixedSize(true);
        agencyPerposedBinding.agencyPerposalRec.setNestedScrollingEnabled(true);
        agencyPerposedBinding.agencyPerposalRec.setLayoutManager(new LinearLayoutManager(getApplicationContext()));

    }



    public void getAgencyData(){

        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getAgencyAllDetails();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;


                    try {
                        JSONArray jsonObject=new JSONArray(response.body());

                        for(int i=0;i<jsonObject.length();i++){

                            JSONObject str=jsonObject.getJSONObject(i);

                            RootModel obj =new RootModel();
                            obj.setAg_req_id(str.getInt("ag_req_id"));
                            obj.setState_id(str.getInt("state_id"));
                            obj.setUnit_id(str.getInt("unit_id"));

                            obj.setCity_id(str.getInt("city_id"));
                            obj.setLocation_id(str.getInt("location_id"));
                            obj.setTown_id(str.getInt("town_id"));


                            obj.setCity_id(str.getInt("city_upc"));
                            obj.setReason(str.getString("reason"));
                            obj.setProposed_count(str.getInt("proposed_count"));

                           arrayList.add(obj);
                        }

                        Log.e("agecnyData", arrayList.toString());
                        adapter = new Agency_new_perposal_Adapter(context,arrayList);
                        agencyPerposedBinding.agencyPerposalRec.setAdapter(adapter);
                        adapter.notifyDataSetChanged();



                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
                    enableLoadingBar(false);
                } else {

                    try{
                        Toast.makeText(NewAgencyPerposedActivity.this, "error message"+response.errorBody().string(), Toast.LENGTH_SHORT).show();
                    }catch(IOException e){
                        Toast.makeText(NewAgencyPerposedActivity.this, "error message"+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                //  enableLoadingBar(false);

                enableLoadingBar(false);
                Toast.makeText(NewAgencyPerposedActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });

    }
}
