package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AgentChildResultModel extends BaseModel {
    @SerializedName("results")
    @Expose
    private List<AgentChildResultListModel> results = null;

    public List<AgentChildResultListModel> getResults() {
        return results;
    }

    public void setResults(List<AgentChildResultListModel> results) {
        this.results = results;
    }

}
