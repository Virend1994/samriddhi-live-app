package com.app.samriddhi.ui.activity.main.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.samriddhi.R;
import com.app.samriddhi.api.response.JsonObjectResponse;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.AgencyDetailsShowAdapter;
import com.app.samriddhi.base.adapter.BillOutAgencyDetailsShowAdapter;
import com.app.samriddhi.base.adapter.RecyclerViewArrayAdapter;
import com.app.samriddhi.databinding.ActivityUserTypeListBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.model.AgentListModel;
import com.app.samriddhi.ui.model.AgentResultModel;
import com.app.samriddhi.ui.model.BillOutAgentListModel;
import com.app.samriddhi.ui.model.BillOutAgentResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCDistWiseResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCResultModel;
import com.app.samriddhi.ui.model.BillingOutstandingResultModel;
import com.app.samriddhi.ui.model.CashCreditResultModel;
import com.app.samriddhi.ui.model.CityResultModel;
import com.app.samriddhi.ui.model.SalesDistrictWiseResultModel;
import com.app.samriddhi.ui.model.SalesOrgCityResultModel;
import com.app.samriddhi.ui.model.StateResultModel;
import com.app.samriddhi.ui.model.UsersResultModel;
import com.app.samriddhi.ui.presenter.UserTypeCopiesDetailPresenter;
import com.app.samriddhi.ui.view.IUserTypeCopiesView;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.SystemUtility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;


public class BillOutAgentListActivity extends BaseActivity implements IUserTypeCopiesView, RecyclerViewArrayAdapter.OnItemClickListener, BillOutAgencyDetailsShowAdapter.OnRouteListClickListener {
    ActivityUserTypeListBinding mBinding;
    UserTypeCopiesDetailPresenter mPresenter;
    Calendar c = Calendar.getInstance();
    private static final String STATE_CODE = "stateCode";
    private static final String STATE_NAME = "stateName";
    private static final String UNIT_NAME = "unitName";
    private static final String UNIT_CODE = "unitCode";
    private static final String D_CHANNEL="DChannel";
    private static final String CITY_UPC = "cityUPC";
    private static final String SALES_CODE = "SalesCode";
    private static final String SALES_DIST = "SalesDist";
    private static final String AGENT_ID = "agentId";
    private static final String AGENT_NAME = "agentName";
    String screenName, stateCode = "", stateName = "", unitCode = "", unitName = "", dChannel = "", salseCode = "", salesDist = "", cityUPC = "";
    BillOutAgencyDetailsShowAdapter agencyDetailsShowAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_user_type_list);
        initializeView();
    }

    /**
     * initalize all the views
     */
    private void initializeView() {


        mBinding.toolbarCalender.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mBinding.toolbarCalender.rlNotification.setOnClickListener(v -> {
            startActivityAnimation(this, NotificationActivity.class, false);
        });
        if (getIntent() != null) {
            if (getIntent().hasExtra(Constant.SCREEN_NAME))
                screenName = getIntent().getStringExtra(Constant.SCREEN_NAME);
            if (getIntent().hasExtra(STATE_CODE))
                stateCode = getIntent().getStringExtra(STATE_CODE);
            if (getIntent().hasExtra(STATE_NAME))
                stateName = getIntent().getStringExtra(STATE_NAME);

            if (getIntent().hasExtra(UNIT_NAME))
                unitName = getIntent().getStringExtra(UNIT_NAME);
            if (getIntent().hasExtra(UNIT_CODE))
                unitCode = getIntent().getStringExtra(UNIT_CODE);

            if (getIntent().hasExtra(D_CHANNEL))
                dChannel = getIntent().getStringExtra(D_CHANNEL);
            if (getIntent().hasExtra(CITY_UPC))
                cityUPC = getIntent().getStringExtra(CITY_UPC);

            if (getIntent().hasExtra(SALES_CODE))
                salseCode = getIntent().getStringExtra(SALES_CODE);

            if (getIntent().hasExtra(SALES_DIST))
                salesDist = getIntent().getStringExtra(SALES_DIST);
        }

        mBinding.setScrenName(screenName);
        mBinding.toolbarCalender.txtTittle.setText(getResources().getString(R.string.str_agent_list));

        String date = new SimpleDateFormat("dd MMM, yyyy", Locale.getDefault()).format(new Date());
        mBinding.tvTodayDate.setText(date);

        mPresenter = new UserTypeCopiesDetailPresenter();
        mPresenter.setView(this);
        mBinding.recycleList.setLayoutManager(new LinearLayoutManager(this));
        if (screenName.equalsIgnoreCase(Constant.DashBoardOutstanding)) {
            if (stateCode.isEmpty() || unitCode.isEmpty()) {
                mBinding.tvOnPage.setText("All Agent");
                 mBinding.tvHeaderOne.setText(R.string.str_outstanding);
            mBinding.tvHeaderTwo.setText(R.string.str_billing_tittle);
                mPresenter.getBillOutAgentList(PreferenceManager.getAgentId(this));

            } else {
                 mBinding.tvHeaderOne.setText(R.string.str_outstanding);
            mBinding.tvHeaderTwo.setText(R.string.str_billing_tittle);
                mBinding.tvOnPage.setText("All States/" + stateName + "/" + unitName + "/" + cityUPC + "/" + salesDist);
                mPresenter.getBillOutAgentWiseCopies(PreferenceManager.getAgentId(this), stateCode, unitCode, dChannel,cityUPC.toLowerCase(), salseCode);


            }

        }
        else if (screenName.equalsIgnoreCase(Constant.DashBoardBill)) {
            if (stateCode.isEmpty() || unitCode.isEmpty()) {
                mBinding.tvOnPage.setText("All Agent");
                 mBinding.tvHeaderOne.setText(R.string.str_outstanding);
            mBinding.tvHeaderTwo.setText(R.string.str_billing_tittle);
                mPresenter.getBillOutAgentList(PreferenceManager.getAgentId(this));

            } else {
                 mBinding.tvHeaderOne.setText(R.string.str_outstanding);
            mBinding.tvHeaderTwo.setText(R.string.str_billing_tittle);
                mBinding.tvOnPage.setText("All States/" + stateName + "/" + unitName + "/" + cityUPC + "/" + salesDist);
                mPresenter.getBillOutAgentWiseCopies(PreferenceManager.getAgentId(this),stateCode, unitCode, dChannel,cityUPC.toLowerCase(), salseCode);


            }

        }


        mBinding.searchLayout.setVisibility(View.VISIBLE);
        mBinding.searchEdt.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                String text = mBinding.searchEdt.getText().toString().toLowerCase(Locale.getDefault());

                agencyDetailsShowAdapter.getFilter().filter(cs);
                agencyDetailsShowAdapter.notifyDataSetChanged();

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });


    }

    @Override
    public void onCitySuccess(CityResultModel mCityModel) {
        //do something later
    }

    @Override
    public void onStateSuccess(StateResultModel mStateModel) {
        //do something later
    }

    @Override
    public void onStateSuccess(BillingOutstandingResultModel mStateModel) {

    }

    @Override
    public void onAgentListSuccess(AgentResultModel mAgentResultModel) {

    }

    @Override
    public void onBillOutAgentListSuccess(BillOutAgentResultModel mAgentResultModel) {
        agencyDetailsShowAdapter = new BillOutAgencyDetailsShowAdapter(mAgentResultModel.getResults(), this, screenName);
        mBinding.recycleList.setAdapter(agencyDetailsShowAdapter);
    }

    @Override
    public void onCreditCashSuccess(CashCreditResultModel mResultsData) {


        //do something later
    }

    @Override
    public void onSuccess(JsonObjectResponse<UsersResultModel> body) {

        //do something later
    }

    @Override
    public void onBillOUtCityUPCSuccess(JsonObjectResponse<BillOutCityUPCResultModel> body) {

    }

    @Override
    public void onSalesOrgCitySuccess(SalesOrgCityResultModel mData) {
        //do something later
    }

    @Override
    public void onSalesDistrictCopies(SalesDistrictWiseResultModel mResultData) {
        //do something later
    }

    @Override
    public void onBillOutCityUPCDistSuccess(BillOutCityUPCDistWiseResultModel mResultData) {

    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onItemClick(View view, Object object) {
        Log.e("ewiuryiwyrwiyr","1"+" "+((BillOutAgentListModel) object).getSoldToParty());

        if (object instanceof BillOutAgentListModel) {
            Log.e("ewiuryiwyrwiyr","2"+" "+ ((BillOutAgentListModel) object).getSoldToParty());

            if (screenName.equalsIgnoreCase(Constant.DrawerProfile)) {
                Log.e("ewiuryiwyrwiyr","3"+" "+ ((BillOutAgentListModel) object).getSoldToParty());

                startActivity(new Intent(this, ProfileActivity.class).putExtra(AGENT_ID, ((AgentListModel) object).getSoldToParty()));
            } else if (screenName.equalsIgnoreCase(Constant.DashBoardBill)) {
                Log.e("ewiuryiwyrwiyr","4"+" "+((BillOutAgentListModel) object).getSoldToParty());
                startActivity(new Intent(this, BillingActivity.class).putExtra(AGENT_ID, ((BillOutAgentListModel) object).getSoldToParty()));
            } else if (screenName.equalsIgnoreCase(Constant.DashBoardOutstanding)) {

                Log.e("ewiuryiwyrwiyr","5"+" "+((BillOutAgentListModel) object).getSoldToParty());

                startActivity(new Intent(this, LedgerActivity.class).putExtra(AGENT_ID, ((BillOutAgentListModel) object).getSoldToParty()
                ).putExtra(AGENT_NAME, ((BillOutAgentListModel) object).getSoldToPartyName()));

            } else if (screenName.equalsIgnoreCase(Constant.DashBoardNoOfCopies)) {
                Log.e("ewiuryiwyrwiyr", "6"+" "+((BillOutAgentListModel) object).getSoldToParty());

                startActivity(new Intent(this, NoOfCopiesActivity.class).putExtra(AGENT_ID, ((AgentListModel) object).getSoldToParty()
                ).putExtra(AGENT_NAME, ((AgentListModel) object).getSoldToPartyName()));

            } else if (screenName.equalsIgnoreCase(Constant.DashBoardGrievance)) {
                Log.e("ewiuryiwyrwiyr", "7"+" "+ ((BillOutAgentListModel) object).getSoldToParty());

                startActivity(new Intent(this, ActivityGrievance.class).putExtra(AGENT_ID, ((AgentListModel) object).getSoldToParty()
                ).putExtra(AGENT_NAME, ((AgentListModel) object).getSoldToPartyName()));

            }
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


}
