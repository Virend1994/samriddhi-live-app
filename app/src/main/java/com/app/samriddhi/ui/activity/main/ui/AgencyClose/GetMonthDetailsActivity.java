package com.app.samriddhi.ui.activity.main.ui.AgencyClose;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.ui.activity.main.ui.Grievance.multipleimage.ImageAdapter;
import com.app.samriddhi.util.Constant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.Credentials;

public class GetMonthDetailsActivity extends BaseActivity {
    RecyclerView recyclerview;
    LastThreeMonthsBillingAdapter lastThreeMonthsBillingAdapter;
    List<LastThreeMonthBillingModal> detailList;
    String strDetailStatus = "";
    String AgencyId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_month_details);

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strDetailStatus = AppsContants.sharedpreferences.getString(AppsContants.DetailStatus, "");
        AgencyId = AppsContants.sharedpreferences.getString(AppsContants.AgencyId, "");

        recyclerview = findViewById(R.id.recyclerview);
        ImageView imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        getBillingDetails();
    }

    public void getBillingDetails() {
        enableLoadingBar(true);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("agency_code", AgencyId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(Constant.BASE_PORTAL_URL + "dash/api/agency-billing-details")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .addJSONObjectBody(jsonObject)
                .setTag("Detail")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            Log.e("rtrtrtr", response.toString());
                            JSONArray jsonArray = new JSONArray(response.getString("billing_details"));
                            detailList = new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                LastThreeMonthBillingModal lastThreeMonthBillingModal = new LastThreeMonthBillingModal();
                                lastThreeMonthBillingModal.setMonth(jsonObject1.getString("month"));
                                lastThreeMonthBillingModal.setOpening_balance(jsonObject1.getString("opening_balance"));
                                lastThreeMonthBillingModal.setCopies(jsonObject1.getString("billing_copies"));
                                lastThreeMonthBillingModal.setBilling_amount(jsonObject1.getString("billing_amount"));
                                lastThreeMonthBillingModal.setUnbilling_amount(jsonObject1.getString("unbilling_amount"));
                                lastThreeMonthBillingModal.setNet_amount(jsonObject1.getString("net_amount"));
                                lastThreeMonthBillingModal.setPayment(jsonObject1.getString("payment"));
                                lastThreeMonthBillingModal.setClosing_balance(jsonObject1.getString("closing_balance"));
                                detailList.add(lastThreeMonthBillingModal);
                            }

                            recyclerview.setHasFixedSize(true);
                            LinearLayoutManager gridLayoutManager = new LinearLayoutManager(GetMonthDetailsActivity.this);
                            recyclerview.setLayoutManager(gridLayoutManager); // set LayoutManager to RecyclerView\
                            lastThreeMonthsBillingAdapter = new LastThreeMonthsBillingAdapter(detailList, GetMonthDetailsActivity.this);
                            recyclerview.setAdapter(lastThreeMonthsBillingAdapter);
                           dismissProgressBar();
                        } catch (Exception ex) {
                           dismissProgressBar();
                            Log.e("rtrtrtr", ex.getMessage());
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                       dismissProgressBar();
                        Log.e("rtrtrtr", anError.getMessage());
                    }
                });

    }
}