package com.app.samriddhi.ui.activity.main.ui;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.BuildConfig;
import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.databinding.ActivityLedgerBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AppsContants;
import com.app.samriddhi.ui.activity.main.ui.Ledger.ConfirmationLedgerActivity;
import com.app.samriddhi.ui.activity.main.ui.Ledger.NewLedgerAdapter;
import com.app.samriddhi.ui.model.LedgerModel;
import com.app.samriddhi.ui.model.NewLedgerModel;
import com.app.samriddhi.ui.presenter.LedgerPresenter;
import com.app.samriddhi.ui.view.ILedgerView;
import com.app.samriddhi.util.AlertUtility;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.CustonShowCaseViewCircle;
import com.app.samriddhi.util.StringUtility;
import com.app.samriddhi.util.SystemUtility;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.Credentials;

public class LedgerActivity extends BaseActivity implements ILedgerView {
    final Calendar fromDate = Calendar.getInstance(Locale.ENGLISH);
    final Calendar toDate = Calendar.getInstance(Locale.ENGLISH);
    ActivityLedgerBinding mBinding;
    LedgerPresenter mPresenter;
    String agentId = "", opBalance = "", closeBal = "", agencyName = "";
    // List<LedgerTransModel> mData = new ArrayList<>();
    List<NewLedgerModel> mData;
    ShowcaseView showcaseView;
    int showCount = 0;
    RelativeLayout.LayoutParams secondParams = null;
    File createdFile;
    String strAgencyId = "";
    private static final String AGENT_ID = "agentId";

    String AgencyLedgerAmt = "";
    String strFromDate = "";
    String strToDate = "";
    String dateFrom = "", dateTo = "", strYear = "", strFromToDate = "";

    List<NewLedgerModel> newLedgerModelList;
    String stropeningBal = "";
    String strAgentName = "";
    String strClosingBalnc = "";
    String strUserCityUpc = "";
    int strGetMonth = 0;
    String strDate1 = "", strDate2 = "";

    String strUnitName = "", strUnitAdd = "", strLogoStatus = "";
    /**
     * handle the From date selections
     */

    DatePickerDialog.OnDateSetListener fromDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub
            fromDate.set(Calendar.YEAR, year);
            fromDate.set(Calendar.MONTH, monthOfYear);
            fromDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            mBinding.edFromDate.setText(SystemUtility.getFormatedCalenderDate(fromDate));
            strFromDate = SystemUtility.getFormatedCalenderDate(fromDate);
        }
    };

    /**
     * select the ToDate selection
     */

    DatePickerDialog.OnDateSetListener toDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub
            toDate.set(Calendar.YEAR, year);
            toDate.set(Calendar.MONTH, monthOfYear);
            toDate.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            mBinding.edToDate.setText(SystemUtility.getFormatedCalenderDate(toDate));
            strToDate = SystemUtility.getFormatedCalenderDate(toDate);
        }

    };

    /**
     * handle the guidance buttons click events
     */


    View.OnClickListener coachClicks = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (showCount) {
                case 1:
                    showcaseView.hide();
                    showcaseView = new ShowcaseView.Builder(LedgerActivity.this)
                            .setTarget(new ViewTarget(R.id.linear_from, LedgerActivity.this))
                            .setContentTitle(getResources().getString(R.string.str_from_date_msg)).blockAllTouches()
                            .setStyle(R.style.CustomCoachMarksTheme)
                            .setShowcaseDrawer(new CustonShowCaseViewCircle(mBinding.linearFrom, getResources()))
                            .build();
                    showcaseView.forceTextPosition(ShowcaseView.BELOW_SHOWCASE);
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                    params.setMargins(15, 10, 10, 30);
                    showcaseView.setButtonPosition(params);
                    showcaseView.addView(getButton(), secondParams);
                    break;
                case 2:
                    showcaseView.hide();
                    showcaseView = new ShowcaseView.Builder(LedgerActivity.this)
                            .setTarget(new ViewTarget(R.id.linear_to_date, LedgerActivity.this))
                            .setContentTitle(getResources().getString(R.string.str_to_date_msg)).blockAllTouches()
                            .setStyle(R.style.CustomCoachMarksTheme)
                            .setShowcaseDrawer(new CustonShowCaseViewCircle(mBinding.linearToDate, getResources()))
                            .build();
                    break;
                default:
                    showcaseView.hide();
                    break;
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_ledger);
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        if (PreferenceManager.getPrefUserType(LedgerActivity.this).equals("AG")) {
            strUserCityUpc = PreferenceManager.getUserCityUpc(LedgerActivity.this);
            strAgencyId = PreferenceManager.getAgentId(LedgerActivity.this);

        } else {
            strUserCityUpc = AppsContants.sharedpreferences.getString(AppsContants.cityupc, "");
            strAgencyId = AppsContants.sharedpreferences.getString(AppsContants.AgencyId, "");
        }
        initViews();


        if (PreferenceManager.getPrefUserType(LedgerActivity.this).equals("AG")) {
            mBinding.btnConfirm.setText("Click here for Ledger Data");
            mBinding.btnConfirm.setVisibility(View.VISIBLE);
        } else {
            mBinding.btnConfirm.setVisibility(View.GONE);

        }


        if (getIntent() != null) {
            if (getIntent().hasExtra(AGENT_ID))
                strAgencyId = getIntent().getStringExtra(AGENT_ID);
        }
        //startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://live.dbsamriddhi.in/GETBillPDF/65309527")));

        Log.e("sfsdfsfsdfs", strUserCityUpc);
        Log.e("sfsdfsfsdfs", strAgencyId);

    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ed_from_date:
                DatePickerDialog datePickerDialog = new DatePickerDialog(LedgerActivity.this, fromDateListener, fromDate
                        .get(Calendar.YEAR), fromDate.get(Calendar.MONTH),
                        fromDate.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());

                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    Date dateObj = dateFormat.parse("01-01-2020");
                    Calendar calFutureHide = Calendar.getInstance();
                    calFutureHide.setTime(dateObj);
                    datePickerDialog.getDatePicker().setMinDate(calFutureHide.getTimeInMillis());
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                datePickerDialog.show();

                break;
            case R.id.ed_to_date:
                DatePickerDialog datePickerDialogTO = new DatePickerDialog(LedgerActivity.this, toDateListener, toDate
                        .get(Calendar.YEAR), toDate.get(Calendar.MONTH),
                        toDate.get(Calendar.DAY_OF_MONTH));
                datePickerDialogTO.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                    Date dateObj = dateFormat.parse("01-01-2020");
                    Calendar calFutureHide = Calendar.getInstance();
                    calFutureHide.setTime(dateObj);
                    datePickerDialogTO.getDatePicker().setMinDate(calFutureHide.getTimeInMillis());
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                datePickerDialogTO.show();
                break;
            case R.id.btn_submit_dates:
                if (validateFilter())
                    if (PreferenceManager.getPrefUserType(LedgerActivity.this).equals("AG")) {
                        getLedgerData(agentId.isEmpty() ? PreferenceManager.getAgentId(this) : agentId, SystemUtility.getFormatedCalenderDay(fromDate), SystemUtility.getFormatedCalenderDay(toDate), "*");
                    } else {
                        getLedgerData(strAgencyId, SystemUtility.getFormatedCalenderDay(fromDate), SystemUtility.getFormatedCalenderDay(toDate), "*");
                    }
                break;
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.rl_excel:
                askStoragePermission(201);
                break;
            default:
                break;
        }

    }

    /**
     * Download the generated ledger into pdf format
     */

    private void downloadPdfFile() {

        Drawable drawableLogo;

        if (strLogoStatus.equals("1")) {

            drawableLogo = getResources().getDrawable(R.drawable.ledger_logo);
        } else if (strLogoStatus.equals("2")) {

            drawableLogo = getResources().getDrawable(R.drawable.db_gujrati_ledger);
        }
        else {

            drawableLogo = getResources().getDrawable(R.drawable.db_marathi_ledger);
        }


        if (mData != null && mData.size() > 0) {
            // createdFile = SystemUtility.createLedgerPdf(this, mData, opBalance, closeBal, agencyName, mBinding.edFromDate.getText().toString(), mBinding.edToDate.getText().toString(),drawableLogo,strAgencyId);

            if (PreferenceManager.getPrefUserType(LedgerActivity.this).equals("AG")) {
                createdFile = SystemUtility.createLedgerPdfTest(this, mData, opBalance, closeBal, strAgentName, mBinding.edFromDate.getText().toString(), mBinding.edToDate.getText().toString(), drawableLogo, agentId.isEmpty() ? PreferenceManager.getAgentId(this) : agentId, strUnitName, strUnitAdd);
                //getLedgerData(agentId.isEmpty() ? PreferenceManager.getAgentId(this) : agentId, SystemUtility.getFormatedCalenderDay(fromDate), SystemUtility.getFormatedCalenderDay(toDate), "*");
            } else {
                createdFile = SystemUtility.createLedgerPdfTest(this, mData, opBalance, closeBal, strAgentName, mBinding.edFromDate.getText().toString(), mBinding.edToDate.getText().toString(), drawableLogo, strAgencyId, strUnitName, strUnitAdd);
            }

            loadProgressBar(null, "Please wait..", false);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    dismissProgressBar();
                    AlertUtility.showAlertFileOpen(LedgerActivity.this, String.format(getResources().getString(R.string.str_excel_download_success), " InternalStorage/Samriddhi/CopiesData "), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            openPdf();
                        }
                    });
                }
            }, 3000);
            //onInfo(String.format(getResources().getString(R.string.str_excel_download_success), " InternalStorage/Samriddhi/LedgerData "));
        }
    }


    /**
     * validate the ledger forms fields
     * fromdate
     * todate
     *
     * @return return the validation
     */
    private boolean validateFilter() {
        if (!StringUtility.validateString(mBinding.edFromDate.getText().toString())) {
            snackBar(mBinding.edFromDate, R.string.str_from_date_empty);
            return false;
        } else if (!StringUtility.validateString(mBinding.edToDate.getText().toString())) {
            snackBar(mBinding.edToDate, R.string.str_to_date_empty);
            return false;
        } else if (fromDate.compareTo(toDate) > 0) {
            snackBar(mBinding.edFromDate, R.string.str_date_compare_msg);
            return false;
        }
        return true;
    }


    @Override
    public void onSuccess(LedgerModel mLedgerData, String agencyName) {

        /* mBinding.btnConfirm.setVisibility(View.VISIBLE);
        this.agencyName = agencyName;
        mBinding.setIsdataAvailable(true);
        // closeBal = SystemUtility.getFormatDecimalValue(Float.parseFloat(mLedgerData.getResults().get(0).getClAmt()));
        closeBal = SystemUtility.getFormatDecimalValue(Float.parseFloat("266391.920"));
        opBalance = SystemUtility.getFormatDecimalValue(Float.parseFloat(mLedgerData.getResults().get(0).getOpAmt()));
        mBinding.setCloseBilling(closeBal);
        mBinding.setOpeningBilling(opBalance);
        if (mLedgerData.getResults().get(0).getTRNSet() != null && mLedgerData.getResults().get(0).getTRNSet().size() > 0) {
            mBinding.recycleInvoice.setAdapter(new RecyclerViewArrayAdapter(mLedgerData.getResults().get(0).getTRNSet()));
            mBinding.setIsTransactionData(true);
            mData = mLedgerData.getResults().get(0).getTRNSet();
        } else {
            mBinding.setIsTransactionData(false);
            mBinding.recycleInvoice.setAdapter(null);
            onError(getResources().getString(R.string.str_no_trans_found));
            mData = null;
        }

        strFromDate = SystemUtility.getFormatedCalenderDate(fromDate);
        strToDate = SystemUtility.getFormatedCalenderDate(toDate);


        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int intY = calendar.get(Calendar.YEAR);

                Log.e("sdgdsgsgdsgs", strFromDate);
                Log.e("sdgdsgsgdsgs", strToDate);

                strYear = String.valueOf(intY);
                String splitFrom[] = strFromDate.split("-");
                dateFrom = splitFrom[1].replaceFirst("^0+(?!$)", "");

                String splitTO[] = strToDate.split("-");
                dateTo = splitTO[1].replaceFirst("^0+(?!$)", "");
                strFromToDate = dateFrom + "to" + dateTo;

                getLedgerStatus(strYear, strFromToDate, strAgencyId);  *//*Check if ledger is confirmed or not*//*

            }
        });*/

    }


    /* ends from here*/
    @SuppressLint("NewApi")
    @Override
    public void PermissionGranted(int request_code) {
        downloadPdfFile();

    }

    @Override
    public void NeverAskAgain(int request_code) {
        // will override if require
    }


    @Override
    public void PermissionDenied(int request_code) {
        // will override if require
    }

    @Override
    public Context getContext() {
        return this;
    }

    /**
     * initialize and bind the views
     */
    private void initViews() {
        if (getIntent() != null && getIntent().hasExtra("agentId")) {
            agentId = getIntent().getStringExtra("agentId");
        }
        fromDate.set(Calendar.DAY_OF_MONTH, fromDate.getActualMinimum(Calendar.DAY_OF_MONTH));
        mPresenter = new LedgerPresenter();
        mPresenter.setView(this);
        mBinding.setIsdataAvailable(false);
        mBinding.recycleInvoice.setLayoutManager(new LinearLayoutManager(this));
        mBinding.toolbarLedger.imgBack.setOnClickListener(this::onClick);
        mBinding.toolbarLedger.txtTittle.setText(getResources().getString(R.string.str_ledger_tittle));
        mBinding.edFromDate.setText(SystemUtility.getFormatedCalenderDate(fromDate));
        mBinding.edToDate.setText(SystemUtility.getFormatedCalenderDate(toDate));
        mBinding.toolbarLedger.rlExcel.setOnClickListener(this::onClick);
        mBinding.toolbarLedger.imgExcel.setImageResource(R.drawable.img_pdf);
        // showCoachMarksOnScreen();

        // strFromDate = SystemUtility.getFormatedCalenderDate(fromDate);
        //strToDate = SystemUtility.getFormatedCalenderDate(toDate);
        mBinding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              //  Toast.makeText(LedgerActivity.this, "Coming Soon", Toast.LENGTH_SHORT).show();

               startActivity(new Intent(LedgerActivity.this, ConfirmationLedgerActivity.class));
               overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

            }
        });
    }

    /**
     * show guidance on the screen
     */

    private void showCoachMarksOnScreen() {
        showCount = 0;
        showcaseView = new ShowcaseView.Builder(this)
                .setTarget(new ViewTarget(R.id.rl_excel, this))
                .singleShot(PreferenceManager.PREF_LEDGER_SHOT)
                .setContentTitle(getResources().getString(R.string.str_download_pdf)).blockAllTouches()
                .setStyle(R.style.CustomCoachMarksTheme)
                .setShowcaseDrawer(new CustonShowCaseViewCircle(mBinding.toolbarLedger.rlExcel, getResources()))
                .build();
        showcaseView.forceTextPosition(ShowcaseView.BELOW_SHOWCASE);
        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        params.setMargins(15, 10, 10, 30);
        showcaseView.setButtonPosition(params);
        showcaseView.addView(getButton(), secondParams);

    }

    /**
     * get the guidance buttons
     * Next
     * Skip
     *
     * @return buttons return
     */
    public Button getButton() {
        final Button button = (Button) LayoutInflater.from(this).inflate(R.layout.showcase_custom_view_button, showcaseView, false);
        secondParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        secondParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        secondParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        secondParams.setMargins(15, 10, 10, 30);
        button.setOnClickListener(coachClicks);
        showCount++;
        return button;
    }

    /**
     * open the created ledger pdf file in device.
     */

    private void openPdf() {

        Log.e("sddssgs", "FILE" + " " + createdFile.toString());
        Uri path = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".fileprovider", createdFile);
        Log.e("create pdf uri path==>", "" + path);
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Log.e("sdgdsdgsg", e.getMessage());
            Toast.makeText(getApplicationContext(), "There is no any PDF Viewer", Toast.LENGTH_SHORT).show();
        }
    }

    private void getLedgerData(String s, String formatedCalenderDay, String formatedCalenderDay1, String s1) {
        Log.e("sdgsdggsdgsg", formatedCalenderDay);
        Log.e("sdgsdggsdgsg", formatedCalenderDay1);
        Log.e("sdgsdggsdgsg", s);
        enableLoadingBar(true);
        newLedgerModelList = new ArrayList<>();
        Log.e("LedgerURL", Constant.BASE_PORTAL_URL+"Ledger_data_new/" + strAgencyId + "/" + formatedCalenderDay + "/" + formatedCalenderDay1 + "/*");
        // AndroidNetworking.get("http://live.dbsamriddhi.in/Ledger_data_new/" + strAgencyId + "/"+formatedCalenderDay+"/"+formatedCalenderDay1+"/*")
        AndroidNetworking.get(Constant.BASE_PORTAL_URL+"Ledger_data_new/" + strAgencyId + "/" + formatedCalenderDay + "/" + formatedCalenderDay1 + "/*")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            enableLoadingBar(false);
                            if (response.getString("data") != null && !response.getString("data").isEmpty() && !response.getString("data").equals("null")) {
                                JSONObject objData = new JSONObject(response.getString("data"));
                                JSONArray jsonArray = objData.getJSONArray("results");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject objResult = jsonArray.getJSONObject(i);
                                    stropeningBal = objResult.getString("OpenBalance");
                                    strAgentName = objResult.getString("Name1");
                                    strUnitName = objResult.getString("unit_name");
                                    strUnitAdd = objResult.getString("unit_address");
                                    strLogoStatus = objResult.getString("state_flag");
                                    agencyName = strAgentName;

                                    JSONObject objLedgerItem = new JSONObject(objResult.getString("LedgerItemSet"));
                                    JSONArray jsonArrayresults = objLedgerItem.getJSONArray("results");
                                    for (int j = 0; j < jsonArrayresults.length(); j++) {
                                        JSONObject jsonObjectResult = jsonArrayresults.getJSONObject(j);
                                        NewLedgerModel newLedgerModel = new NewLedgerModel();

                                        newLedgerModel.setVDate(SystemUtility.getLedgerFormateDate(jsonObjectResult.getString("VDate")));
                                        if (jsonObjectResult.getString("Narration").contains("-")) {

                                            String strSplit[] = jsonObjectResult.getString("Narration").split("-");
                                            newLedgerModel.setNarration(strSplit[0]);

                                        } else {
                                            newLedgerModel.setNarration(jsonObjectResult.getString("Narration"));
                                        }

                                        double debit = Double.parseDouble(jsonObjectResult.getString("Debit"));
                                        debit = Double.parseDouble(new DecimalFormat("##.##").format(debit));
                                        newLedgerModel.setDebit(String.valueOf(debit));

                                        double credit = Double.parseDouble(jsonObjectResult.getString("Credit").replace("-", ""));
                                        credit = Double.parseDouble(new DecimalFormat("##.##").format(credit));
                                        newLedgerModel.setCredit(String.valueOf(credit));

                                        double balnc = Double.parseDouble(jsonObjectResult.getString("Balance"));
                                        balnc = Double.parseDouble(new DecimalFormat("##.##").format(balnc));
                                        newLedgerModel.setBalance(String.valueOf(balnc));

                                        strClosingBalnc = jsonObjectResult.getString("Balance");
                                        newLedgerModelList.add(newLedgerModel);
                                    }


                                    mBinding.setIsdataAvailable(true);

                                    double closingBalnc = Double.parseDouble(strClosingBalnc);
                                    closingBalnc = Double.parseDouble(new DecimalFormat("##.##").format(closingBalnc));
                                    mBinding.setCloseBilling(String.valueOf(closingBalnc));
                                    closeBal = String.valueOf(closingBalnc);

                                    // closeBal = SystemUtility.getFormatDecimalValue(Float.parseFloat(strClosingBalnc));
                                   // opBalance = SystemUtility.getFormatDecimalValue(Float.parseFloat(stropeningBal));

                                    double OpenBalnc = Double.parseDouble(stropeningBal);
                                    OpenBalnc = Double.parseDouble(new DecimalFormat("##.##").format(OpenBalnc));
                                    mBinding.setOpeningBilling(String.valueOf(OpenBalnc));
                                    opBalance = String.valueOf(OpenBalnc);


                                    //mBinding.setOpeningBilling(opBalance);

                                    mBinding.txtAgencyName.setText(strAgencyId+" :-"+strAgentName);

                                    if (newLedgerModelList != null && newLedgerModelList.size() > 0) {
                                        mBinding.recycleInvoice.setAdapter(new NewLedgerAdapter(newLedgerModelList, LedgerActivity.this));
                                        mBinding.setIsTransactionData(true);
                                        mData = newLedgerModelList;
                                    } else {
                                        mBinding.setIsTransactionData(false);
                                        mBinding.recycleInvoice.setAdapter(null);
                                        LedgerActivity.this.onError(getResources().getString(R.string.str_no_trans_found), false);
                                        mData = null;
                                    }


                                }
                            } else {
                                Toast.makeText(LedgerActivity.this, "Data not found", Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            enableLoadingBar(false);
                            Log.e("sdgsgsgssgs", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("sjdfhskfsd", anError.getMessage());
                        enableLoadingBar(false);
                    }
                });
    }


}
