package com.app.samriddhi.ui.activity.main.ui.AgencyClose;

import android.content.SharedPreferences;

public class AppsContants {

    public static SharedPreferences sharedpreferences;
    private static String myprefrencesKey;
    public static final String MyPREFERENCES = myprefrencesKey;

    public static final String DetailStatus = "DetailStatus";
    public static final String AgencyId = "AgencyId";
    public static final String AgencyName = "AgencyName";
    public static final String stateCode = "StateCode";
    public static final String unitCode = "unitCode";
    public static final String ImageStatus = "ImageStatus";
    public static final String StateId = "StateId";
    public static final String UnitId = "UnitId";
    public static final String cityupc = "cityupc";
    public static final String UOHApproveStatus = "UOHApproveStatus";
    public static final String SOHApproveStatus = "SOHApproveStatus";
    public static final String HoApproveStatus = "HoApproveStatus";
    public static final String StateFlag = "StateFlag";
    public static final String SalesMain = "SalesMain";
    public static final String SalesJJ = "SalesJJ";
    public static final String Reason = "Reason";
    public static final String MarketCode = "MarketCode";
    public static final String CityCode = "CityCode";

}