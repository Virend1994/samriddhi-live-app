package com.app.samriddhi.ui.activity.main.ui.OrderSupply.POModal;

public class DailyPOModal {

    public String agency_name;
    public String agency_id;
    public String edi_name;
    public String copies_type;
    public String sold_to_party;
    public String ship_to_party;
    public String item_cat;
    public String ref_qtd;
    public String sale_qtd;
    public String reason_qtd;
    public String inc_dsc;
    public String remark;
    public String base_copy;
    public String base_min;
    public String base_max;
    public String editedStatus;
    public String pick_date;
    public String pva;
    public String cust_name;
    public String city_name;
    public String posnr;
    public String pstyv;
    public String vbeln;
    public String proposed_qty;
    public String perincreased;
    public String is_approval;
    public String CIROrderId;
    public String checkedItem;
    public String OrderApproval;
    public String Error;
    public String scalabal_copies;
    public String quantityPerStatus;
    public String mainSaleTotal;
    public String DiffStatus;
    public String refTotal;
    public String StoreProposed;
    public String status;
    public String changeStatus;
    public String approvalINCDEC;
    public String TotalSaleJJ;
    public String totalRefJJ;

    public String TotalSaleMain;
    public String totalRefMain;

    public String TotalSaleFree;
    public String totalRefFree;

    public String TotalSaleCopies;
    public String totalRefCopies;
    public String rj_sh_flag;


    public String getRj_sh_flag() {
        return rj_sh_flag;
    }

    public void setRj_sh_flag(String rj_sh_flag) {
        this.rj_sh_flag = rj_sh_flag;
    }

    public String getTotalSaleJJ() {
        return TotalSaleJJ;
    }

    public void setTotalSaleJJ(String totalSaleJJ) {
        TotalSaleJJ = totalSaleJJ;
    }

    public String getTotalRefJJ() {
        return totalRefJJ;
    }

    public void setTotalRefJJ(String totalRefJJ) {
        this.totalRefJJ = totalRefJJ;
    }

    public String getTotalSaleMain() {
        return TotalSaleMain;
    }

    public void setTotalSaleMain(String totalSaleMain) {
        TotalSaleMain = totalSaleMain;
    }

    public String getTotalRefMain() {
        return totalRefMain;
    }

    public void setTotalRefMain(String totalRefMain) {
        this.totalRefMain = totalRefMain;
    }

    public String getTotalSaleFree() {
        return TotalSaleFree;
    }

    public void setTotalSaleFree(String totalSaleFree) {
        TotalSaleFree = totalSaleFree;
    }

    public String getTotalRefFree() {
        return totalRefFree;
    }

    public void setTotalRefFree(String totalRefFree) {
        this.totalRefFree = totalRefFree;
    }

    public String getTotalSaleCopies() {
        return TotalSaleCopies;
    }

    public void setTotalSaleCopies(String totalSaleCopies) {
        TotalSaleCopies = totalSaleCopies;
    }

    public String getTotalRefCopies() {
        return totalRefCopies;
    }

    public void setTotalRefCopies(String totalRefCopies) {
        this.totalRefCopies = totalRefCopies;
    }

    public String getApprovalINCDEC() {
        return approvalINCDEC;
    }

    public void setApprovalINCDEC(String approvalINCDEC) {
        this.approvalINCDEC = approvalINCDEC;
    }

    public String getChangeStatus() {
        return changeStatus;
    }

    public void setChangeStatus(String changeStatus) {
        this.changeStatus = changeStatus;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStoreProposed() {
        return StoreProposed;
    }

    public void setStoreProposed(String storeProposed) {
        StoreProposed = storeProposed;
    }

    public String getIncreament() {
        return increament;
    }

    public void setIncreament(String increament) {
        this.increament = increament;
    }

    public String increament;



    public String getRefTotal() {
        return refTotal;
    }

    public void setRefTotal(String refTotal) {
        this.refTotal = refTotal;
    }

    public String getDiffStatus() {
        return DiffStatus;
    }

    public void setDiffStatus(String diffStatus) {
        DiffStatus = diffStatus;
    }

    public String getMainSaleTotal() {
        return mainSaleTotal;
    }

    public void setMainSaleTotal(String mainSaleTotal) {
        this.mainSaleTotal = mainSaleTotal;
    }

    public String getQuantityPerStatus() {
        return quantityPerStatus;
    }

    public void setQuantityPerStatus(String quantityPerStatus) {
        this.quantityPerStatus = quantityPerStatus;
    }

    public String getScalabal_copies() {
        return scalabal_copies;
    }

    public void setScalabal_copies(String scalabal_copies) {
        this.scalabal_copies = scalabal_copies;
    }

    public String getError() {
        return Error;
    }

    public void setError(String error) {
        Error = error;
    }

    public String getOrderApproval() {
        return OrderApproval;
    }

    public void setOrderApproval(String orderApproval) {
        OrderApproval = orderApproval;
    }

    public String getCheckedItem() {
        return checkedItem;
    }

    public void setCheckedItem(String checkedItem) {
        this.checkedItem = checkedItem;
    }

    public String getCIROrderId() {
        return CIROrderId;
    }

    public void setCIROrderId(String CIROrderId) {
        this.CIROrderId = CIROrderId;
    }

    public String getVbeln() {
        return vbeln;
    }

    public void setVbeln(String vbeln) {
        this.vbeln = vbeln;
    }

    public String getIs_approval() {
        return is_approval;
    }

    public void setIs_approval(String is_approval) {
        this.is_approval = is_approval;
    }

    public String getPerincreased() {
        return perincreased;
    }

    public void setPerincreased(String perincreased) {
        this.perincreased = perincreased;
    }

    public String getProposed_qty() {
        return proposed_qty;
    }

    public void setProposed_qty(String proposed_qty) {
        this.proposed_qty = proposed_qty;
    }

    public String getPosnr() {
        return posnr;
    }

    public void setPosnr(String posnr) {
        this.posnr = posnr;
    }

    public String getPstyv() {
        return pstyv;
    }

    public void setPstyv(String pstyv) {
        this.pstyv = pstyv;
    }

    public String getCust_name() {
        return cust_name;
    }

    public void setCust_name(String cust_name) {
        this.cust_name = cust_name;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getPva() {
        return pva;
    }

    public void setPva(String pva) {
        this.pva = pva;
    }

    public String getReason_qty_change() {
        return reason_qty_change;
    }

    public void setReason_qty_change(String reason_qty_change) {
        this.reason_qty_change = reason_qty_change;
    }

    public String reason_qty_change;

    public String getPick_date() {
        return pick_date;
    }

    public void setPick_date(String pick_date) {
        this.pick_date = pick_date;
    }

    public String getEditedStatus() {
        return editedStatus;
    }

    public void setEditedStatus(String editedStatus) {
        this.editedStatus = editedStatus;
    }

    public String getBase_min() {
        return base_min;
    }

    public void setBase_min(String base_min) {
        this.base_min = base_min;
    }

    public String getBase_max() {
        return base_max;
    }

    public void setBase_max(String base_max) {
        this.base_max = base_max;
    }

    public String getBase_copy() {
        return base_copy;
    }

    public void setBase_copy(String base_copy) {
        this.base_copy = base_copy;
    }

    public String getSale_qtd() {
        return sale_qtd;
    }

    public void setSale_qtd(String sale_qtd) {
        this.sale_qtd = sale_qtd;
    }

    public String getCopies_type() {
        return copies_type;
    }

    public void setCopies_type(String copies_type) {
        this.copies_type = copies_type;
    }

    public String getAgency_name() {
        return agency_name;
    }

    public void setAgency_name(String agency_name) {
        this.agency_name = agency_name;
    }

    public String getAgency_id() {
        return agency_id;
    }

    public void setAgency_id(String agency_id) {
        this.agency_id = agency_id;
    }

    public String getEdi_name() {
        return edi_name;
    }

    public void setEdi_name(String edi_name) {
        this.edi_name = edi_name;
    }

    public String getSold_to_party() {
        return sold_to_party;
    }

    public void setSold_to_party(String sold_to_party) {
        this.sold_to_party = sold_to_party;
    }

    public String getShip_to_party() {
        return ship_to_party;
    }

    public void setShip_to_party(String ship_to_party) {
        this.ship_to_party = ship_to_party;
    }

    public String getItem_cat() {
        return item_cat;
    }

    public void setItem_cat(String item_cat) {
        this.item_cat = item_cat;
    }

    public String getRef_qtd() {
        return ref_qtd;
    }

    public void setRef_qtd(String ref_qtd) {
        this.ref_qtd = ref_qtd;
    }

    public String getReason_qtd() {
        return reason_qtd;
    }

    public void setReason_qtd(String reason_qtd) {
        this.reason_qtd = reason_qtd;
    }

    public String getInc_dsc() {
        return inc_dsc;
    }

    public void setInc_dsc(String inc_dsc) {
        this.inc_dsc = inc_dsc;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
