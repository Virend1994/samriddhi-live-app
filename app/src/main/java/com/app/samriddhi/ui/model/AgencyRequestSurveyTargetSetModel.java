package com.app.samriddhi.ui.model;

import java.io.Serializable;

public class AgencyRequestSurveyTargetSetModel implements Serializable {
    public String agency_name;
    public String agecy_code;
    public int cur_copies;
    public int month_one;
    public int month_two;
    public int month_three;

    public String getAgency_name() {
        return agency_name;
    }

    public void setAgency_name(String agency_name) {
        this.agency_name = agency_name;
    }

    public String getAgecy_code() {
        return agecy_code;
    }

    public void setAgecy_code(String agecy_code) {
        this.agecy_code = agecy_code;
    }

    public int getCur_copies() {
        return cur_copies;
    }

    public void setCur_copies(int cur_copies) {
        this.cur_copies = cur_copies;
    }

    public int getMonth_one() {
        return month_one;
    }

    public void setMonth_one(int month_one) {
        this.month_one = month_one;
    }

    public int getMonth_two() {
        return month_two;
    }

    public void setMonth_two(int month_two) {
        this.month_two = month_two;
    }

    public int getMonth_three() {
        return month_three;
    }

    public void setMonth_three(int month_three) {
        this.month_three = month_three;
    }


}
