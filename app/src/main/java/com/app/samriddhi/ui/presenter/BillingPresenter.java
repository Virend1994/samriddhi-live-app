package com.app.samriddhi.ui.presenter;

import com.app.samriddhi.R;
import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.response.BaseResponse;
import com.app.samriddhi.api.response.JsonObjectResponse;
import com.app.samriddhi.base.BasePresenter;
import com.app.samriddhi.ui.model.BillingModel;
import com.app.samriddhi.ui.model.PaymentModel;
import com.app.samriddhi.ui.view.IBillingView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BillingPresenter extends BasePresenter<IBillingView> {

    /**
     *  Provides the agent billing data
     * @param bp_code Agent Id
     */

    public void getBillData(String bp_code) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getBillingData(bp_code).enqueue(new Callback<JsonObjectResponse<BillingModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<BillingModel>> call, Response<JsonObjectResponse<BillingModel>> response) {
                getView().enableLoadingBar(false);
                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null) {
                            getView().onSuccess(response.body().body, response.body().outStanding, response.body().PayUMoney, response.body().BankList);
                        } else {
                            getView().onError(getView().getContext().getString(R.string.str_no_record_found));

                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<BillingModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

    /**
     *  update the payment status
     * @param mData payment request
     */

    public void updatePayment(PaymentModel mData) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().updatePaymentStatus(mData).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body() != null) {
                            getView().onInfo(response.body().replyMsg);
                        } else {
                            getView().onError(getView().getContext().getString(R.string.str_no_record_found));

                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

}
