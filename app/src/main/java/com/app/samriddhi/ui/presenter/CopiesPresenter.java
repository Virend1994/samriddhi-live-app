package com.app.samriddhi.ui.presenter;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.response.JsonObjectResponse;
import com.app.samriddhi.base.BasePresenter;
import com.app.samriddhi.ui.model.DashBoardData;
import com.app.samriddhi.ui.model.EditionModel;
import com.app.samriddhi.ui.model.NumberOfCopiesModel;
import com.app.samriddhi.ui.view.ICopiesView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CopiesPresenter extends BasePresenter<ICopiesView> {

    /**
     * gets the copies editions list
     * @param agentId BpCOde
     */
    public void getEditionList(String agentId) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getEditionsList(agentId).enqueue(new Callback<JsonObjectResponse<EditionModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<EditionModel>> call, Response<JsonObjectResponse<EditionModel>> response) {
                getView().enableLoadingBar(false);
                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onCopiesSucces(response.body().body);

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<EditionModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

    /**
     *  get the number of copies of an agent
     * @param agentId   logged in agentId
     * @param prefUserType DB user type
     * @param fromDate date from to filter
     * @param toDate date To to filter
     * @param editionCode copies edition code
     * @param fromSubmit boolean
     */

    public void getNumberOfCopies(String agentId, String prefUserType, String fromDate, String toDate, String editionCode, boolean fromSubmit) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getNumberOfCopies(agentId, prefUserType, fromDate, toDate, editionCode).enqueue(new Callback<JsonObjectResponse<NumberOfCopiesModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<NumberOfCopiesModel>> call, Response<JsonObjectResponse<NumberOfCopiesModel>> response) {
                getView().enableLoadingBar(false);
                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onNumberOfCopiesSuccess(response.body().body, fromSubmit);
                        else
                            getView().onError(response.body().replyMsg, fromSubmit);

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<NumberOfCopiesModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });


    }



}
