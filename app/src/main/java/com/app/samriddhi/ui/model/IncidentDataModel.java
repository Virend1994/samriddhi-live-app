package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class IncidentDataModel extends BaseModel {
    @SerializedName("Incidentlist")
    @Expose
    private List<IncidentListModel> incidentlist = null;

    @SerializedName("Feedback")
    @Expose
    private List<IncidentFeedback> feedback = null;

    public List<IncidentListModel> getIncidentlist() {
        return incidentlist;
    }

    public void setIncidentlist(List<IncidentListModel> incidentlist) {
        this.incidentlist = incidentlist;
    }

    public List<IncidentFeedback> getFeedback() {
        return feedback;
    }

    public void setFeedback(List<IncidentFeedback> feedback) {
        this.feedback = feedback;
    }
}
