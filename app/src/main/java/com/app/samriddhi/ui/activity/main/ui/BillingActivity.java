package com.app.samriddhi.ui.activity.main.ui;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.os.Build.VERSION.SDK_INT;
import static android.provider.Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.samriddhi.R;
import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.RecyclerViewArrayAdapter;
import com.app.samriddhi.databinding.ActivityBillingBinding;
import com.app.samriddhi.permission.PermissionUtils;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.model.BillingListModel;
import com.app.samriddhi.ui.model.BillingModel;
import com.app.samriddhi.ui.model.PaymentModel;
import com.app.samriddhi.ui.presenter.BillingPresenter;
import com.app.samriddhi.ui.view.IBillingView;
import com.app.samriddhi.util.AlertUtility;
import com.app.samriddhi.util.AppEnvironment;
import com.app.samriddhi.util.CustomShowCaseView;
import com.app.samriddhi.util.SystemUtility;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.payumoney.core.PayUmoneyConfig;
import com.payumoney.core.PayUmoneyConstants;
import com.payumoney.core.PayUmoneySdkInitializer;
import com.payumoney.core.entity.TransactionResponse;
import com.payumoney.sdkui.ui.utils.PayUmoneyFlowManager;
import com.payumoney.sdkui.ui.utils.ResultModel;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.TimeZone;

public class BillingActivity extends BaseActivity implements IBillingView, RecyclerViewArrayAdapter.OnItemClickListener<BillingListModel>, View.OnClickListener {
    ActivityBillingBinding mBillingBinding;
    BillingPresenter mPresenter;
    String agentId = "";
    BillingListModel mBillData;
    String productName;
    PaymentModel mPayMentData;
    String payableAmount = "", outStanding = "";
    ShowcaseView showcaseView;
    int showCount = 0;
    RelativeLayout.LayoutParams secondParams = null;
    BillingModel mBilModelData;
    Boolean payUMoneyEnabled, bankListEnabled;
    /*View.OnClickListener coachClicks = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (showCount) {
                case 1:
                    showcaseView.hide();
                    showcaseView = new ShowcaseView.Builder(BillingActivity.this)
                            .setTarget(new ViewTarget(R.id.btn_invoice, BillingActivity.this))
                            .setContentTitle(getResources().getString(R.string.str_click_view_invoice)).blockAllTouches()
                            .setStyle(R.style.CustomCoachMarksTheme)
                            .setShowcaseDrawer(new CustomShowCaseView((mBillingBinding.recycleBilling.getChildAt(0).getRootView().findViewById(R.id.btn_invoice)), getResources()))
                            .build();
                    showcaseView.forceTextPosition(ShowcaseView.BELOW_SHOWCASE);
                    break;

                default:
                    showcaseView.hide();
                    break;
            }
        }
    };*/


    private final boolean isDisableExitConfirmation = false;
    private PayUmoneySdkInitializer.PaymentParam mPaymentParams;

    /**
     * calculate the hash key
     * @param str payments request paramters
     * @return calculated hash key
     */
    public static String hashCal(String str) {
        byte[] hashseq = str.getBytes();
        StringBuilder hexString = new StringBuilder();
        try {
            MessageDigest algorithm = MessageDigest.getInstance("SHA-512");
            algorithm.reset();
            algorithm.update(hashseq);
            byte[] messageDigest = algorithm.digest();
            for (byte aMessageDigest : messageDigest) {
                String hex = Integer.toHexString(0xFF & aMessageDigest);
                if (hex.length() == 1) {
                    hexString.append("0");
                }
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException ignored) {
        }
        return hexString.toString();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBillingBinding = DataBindingUtil.setContentView(this, R.layout.activity_billing);
        setViews();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onItemClick(View view, BillingListModel object) {
        mBillData = object;
        permissionUtils = new PermissionUtils(BillingActivity.this);
        askStoragePermission(201);

    }

    /**
     * view the invoice on a bill
     */
    private void openInvoiceActivity() {

        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        Calendar today = Calendar.getInstance(TimeZone.getDefault());
        calendar.setTimeInMillis(Long.parseLong(mBillData.getFkdat()) * 1000);
        int todayMonth = today.get(Calendar.MONTH);
        int billdateMonth = calendar.get(Calendar.MONTH);

        if (!(today.get(Calendar.YEAR) == calendar.get(Calendar.YEAR) && (todayMonth + 1) == (billdateMonth + 1))) {
            startActivity(new Intent(this, InVoiceActivity.class)
                    .putExtra("bill_number", mBillData.getBillNo())
                    .putExtra("bill_period", mBillData.getFormattedMonth())
                    .putExtra("bill_amount", mBillData.getNetBilling())
                    .putExtra("copies", mBillData.getSoldCopies())
                    .putExtra("Monat", mBillData.getMonat())
                    .putExtra("Gjahr", mBillData.getGjahr())
                    .putExtra("PayAmt", mBillData.getPayAmt()));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    @SuppressLint("NewApi")
    @Override
    public void PermissionGranted(int request_code) {
        Log.e("on click", "==== ");
        openInvoiceActivity();
    }


    @Override
    public void onError(String reason) {
        super.onError(reason);
        mBillingBinding.setOutstanding("");

    }

    @Override
    public void onInfo(String message) {
        AlertUtility.showAlertWithListener(this, message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mPresenter.getBillData(agentId.isEmpty() ? PreferenceManager.getAgentId(BillingActivity.this) : agentId);
            }
        });
    }

    @Override
    public void onSuccess(BillingModel mData, String outStanding, Boolean payUMoney, Boolean bankList) {
        Collections.reverse(mData.getResults());
        mBillingBinding.setOutstanding(outStanding.isEmpty() ? "" : (SystemUtility.getFormatOutstandingForBilling(Float.parseFloat(outStanding))));
        payableAmount = mBillingBinding.getOutstanding();
        this.outStanding = mBillingBinding.getOutstanding();
        mBilModelData = mData;
        payUMoneyEnabled = payUMoney;
        bankListEnabled = bankList;
        mBillingBinding.recycleBilling.setAdapter(new RecyclerViewArrayAdapter(mData.getResults(), this));
        //showCoachMarksOnScreen();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_payable_amount:
                if (payUMoneyEnabled) {
                    if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("AG")) {
                        productName = "Agent billing";
                        int amount = (int) (Float.parseFloat(payableAmount));
                        showPaymentSelectionDialog(amount > 0);
                    }
                } else if (bankListEnabled) {
                    startActivityAnimation(this, BankListActivity.class, false);
                }
                break;
            default:
                break;

        }
    }

    /**
     * This function prepares the data for payment and launches payumoney plug n play sdk
     */
    private void launchPayUMoneyFlow() {
        PayUmoneyConfig payUmoneyConfig = PayUmoneyConfig.getInstance();
        payUmoneyConfig.disableExitConfirmation(isDisableExitConfirmation);
        PayUmoneySdkInitializer.PaymentParam.Builder builder = new PayUmoneySdkInitializer.PaymentParam.Builder();
        String txnId = PreferenceManager.getAgentId(this) + System.currentTimeMillis();
        String udf1 = PreferenceManager.getPrefAgentName(this);
        String udf2 = "";
        String udf3 = "";
        String udf4 = "";
        String udf5 = "";
        String udf6 = "";
        String udf7 = "";
        String udf8 = "";
        String udf9 = "";
        String udf10 = "";
        AppEnvironment appEnvironment = SamriddhiApplication.getmInstance().getAppEnvironment();
        builder.setAmount(payableAmount)
                .setTxnId(txnId)
                .setPhone(PreferenceManager.getUSerPhone(this))
                .setProductName(productName)
                .setFirstName("DBCORP" + PreferenceManager.getAgentId(this))
                .setEmail(PreferenceManager.getUserEmail(this).isEmpty() ? "payumoney-samridhi@dbcorp.in" : PreferenceManager.getUserEmail(this))
                .setsUrl(appEnvironment.surl())
                .setfUrl(appEnvironment.furl())
                .setUdf1(udf1)
                .setUdf2(udf2)
                .setUdf3(udf3)
                .setUdf4(udf4)
                .setUdf5(udf5)
                .setUdf6(udf6)
                .setUdf7(udf7)
                .setUdf8(udf8)
                .setUdf9(udf9)
                .setUdf10(udf10)
                .setIsDebug(appEnvironment.debug())
                .setKey(appEnvironment.merchant_Key())
                .setMerchantId(appEnvironment.merchant_ID());
        try {
            mPaymentParams = builder.build();

            /*
             * Hash should always be generated from your server side.
             * */
            //    generateHashFromServer(mPaymentParams);

            /*            *//**
             * Do not use below code when going live
             * Below code is provided to generate hash from sdk.
             * It is recommended to generate hash from server side only.
             * */
            mPaymentParams = calculateServerSideHashAndInitiatePayment1(mPaymentParams);
            PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams, this, R.style.AppTheme_pink, false);
        } catch (Exception e) {
            // some exception occurred
            Log.e("exception occured", "=== " + e.getMessage());
            //  Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();

        }
    }

    /**
     * Thus function calculates the hash for transaction
     *
     * @param paymentParam payment params of transaction
     * @return payment params along with calculated merchant hash
     */
    private PayUmoneySdkInitializer.PaymentParam calculateServerSideHashAndInitiatePayment1(final PayUmoneySdkInitializer.PaymentParam paymentParam) {

        StringBuilder stringBuilder = new StringBuilder();
        HashMap<String, String> params = paymentParam.getParams();
        stringBuilder.append(params.get(PayUmoneyConstants.KEY) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.TXNID) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.AMOUNT) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.PRODUCT_INFO) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.FIRSTNAME) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.EMAIL) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF1) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF2) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF3) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF4) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF5) + "||||||");

        AppEnvironment appEnvironment = SamriddhiApplication.getmInstance().getAppEnvironment();
        stringBuilder.append(appEnvironment.salt());

        String hash = hashCal(stringBuilder.toString());
        paymentParam.setMerchantHash(hash);

        return paymentParam;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result Code is -1 send from Payumoney activity
        if (requestCode == PayUmoneyFlowManager.REQUEST_CODE_PAYMENT && resultCode == RESULT_OK && data !=
                null) {
            TransactionResponse transactionResponse = data.getParcelableExtra(PayUmoneyFlowManager
                    .INTENT_EXTRA_TRANSACTION_RESPONSE);

            ResultModel resultModel = data.getParcelableExtra(PayUmoneyFlowManager.ARG_RESULT);
            // Check which object is non-null
            if (transactionResponse != null && transactionResponse.getPayuResponse() != null) {
                if (transactionResponse.getTransactionStatus().equals(TransactionResponse.TransactionStatus.SUCCESSFUL)) {
                    //Success Transaction
                    mPayMentData.setBpCode(PreferenceManager.getAgentId(this));
                    mPayMentData.setPaymentAmount(payableAmount);
                    mPayMentData.setPaymentDiviceID(SystemUtility.getDeviceId(this));
                    mPayMentData.setPaymentStatus("Success");
//                  new JSONObject(transactionResponse.getPayuResponse()).getJSONObject("result").getString("paymentId");
                    mPayMentData.setPaymentResponse(transactionResponse.getPayuResponse());
                    mPayMentData.setPaymentRequest(mPaymentParams.getParams().toString());
                    mPresenter.updatePayment(mPayMentData);
                } else {
                    //Failure Transaction
                    mPayMentData.setBpCode(PreferenceManager.getAgentId(this));
                    mPayMentData.setPaymentAmount(payableAmount);
                    mPayMentData.setPaymentDiviceID(SystemUtility.getDeviceId(this));
                    mPayMentData.setPaymentStatus("Failure");
                    mPayMentData.setPaymentResponse(transactionResponse.getPayuResponse());
                    mPayMentData.setPaymentRequest(mPaymentParams.getParams().toString());
                    mPresenter.updatePayment(mPayMentData);
                }
            } else if (resultModel != null && resultModel.getError() != null) {
                Log.d(this.getClass().getSimpleName(), "Error response : " + resultModel.getError().getTransactionResponse());
            } else {
                Log.d(this.getClass().getSimpleName(), "Both objects are null!");
            }

        }
        payableAmount = outStanding;
    }

    /**
     * show the dialog for payments
     * @param showPayNow handle the payment options
     */

    private void showPaymentSelectionDialog(boolean showPayNow) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        ViewGroup viewGroup = findViewById(android.R.id.content);
        View dialogView = LayoutInflater.from(this.getContext()).inflate(R.layout.alert_payment_options, viewGroup, false);
        builder.setView(dialogView);
        AlertDialog alertDialog = builder.create();
        if (showPayNow) {
            dialogView.findViewById(R.id.rd_full_pay).setEnabled(true);
            ((RadioButton) dialogView.findViewById(R.id.rd_full_pay)).setChecked(true);
            dialogView.findViewById(R.id.ed_amount).setVisibility(View.INVISIBLE);
        } else {
            dialogView.findViewById(R.id.rd_full_pay).setEnabled(false);
            ((RadioButton) dialogView.findViewById(R.id.rd_payment_other_option)).setChecked(true);
            dialogView.findViewById(R.id.ed_amount).setVisibility(View.VISIBLE);
            dialogView.findViewById(R.id.ed_amount).requestFocus();

        }
        ((RadioButton) dialogView.findViewById(R.id.rd_full_pay)).setText(getResources().getString(R.string.str_pay_total_amount) + "( " + payableAmount + " )");
        ((RadioGroup) dialogView.findViewById(R.id.radio_gp_payment)).setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.rd_full_pay:
                        dialogView.findViewById(R.id.ed_amount).setVisibility(View.INVISIBLE);
                        break;
                    case R.id.rd_payment_other_option:
                        dialogView.findViewById(R.id.ed_amount).setVisibility(View.VISIBLE);
                        dialogView.findViewById(R.id.ed_amount).requestFocus();
                        break;
                    default:
                        break;
                }
            }
        });
        dialogView.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });
        dialogView.findViewById(R.id.btn_pay_now).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int checkedRadioButtonId = ((RadioGroup) dialogView.findViewById(R.id.radio_gp_payment)).getCheckedRadioButtonId();
                if (checkedRadioButtonId == R.id.rd_full_pay) {
                    launchPayUMoneyFlow();
                    alertDialog.dismiss();
                } else if (checkedRadioButtonId == R.id.rd_payment_other_option) {
                    if (((EditText) dialogView.findViewById(R.id.ed_amount)).getText().toString().isEmpty()) {
                        ((EditText) dialogView.findViewById(R.id.ed_amount)).setError(getResources().getString(R.string.str_error_amount_empty));
                    } else {
                        payableAmount = ((EditText) dialogView.findViewById(R.id.ed_amount)).getText().toString();
                        launchPayUMoneyFlow();
                        alertDialog.dismiss();
                    }

                }
            }
        });
        alertDialog.show();
    }

    /**
     * initialize and bind the views
     */

    private void setViews() {
        mBillingBinding.setOutstanding("");
        mPresenter = new BillingPresenter();
        mPayMentData = new PaymentModel();
        mBillingBinding.recycleBilling.setLayoutManager(new LinearLayoutManager(this));
        mPresenter.setView(this);
        if (getIntent() != null && getIntent().hasExtra("agentId")) {
            agentId = getIntent().getStringExtra("agentId");
        }
        mPresenter.getBillData(agentId.isEmpty() ? PreferenceManager.getAgentId(this) : agentId);
        mBillingBinding.toolbarBilling.txtTittle.setText(getResources().getString(R.string.str_billing_tittle));
        mBillingBinding.toolbarBilling.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mBillingBinding.btnPayableAmount.setOnClickListener(this);
        mBillingBinding.toolbarBilling.rlNotification.setOnClickListener(v -> {
            startActivityAnimation(this, NotificationActivity.class, false);
        });

    }

    /**
     * Show the guidance on the screen
     */
   /* private void showCoachMarksOnScreen() {
        showCount = 0;
        showcaseView = new ShowcaseView.Builder(this)
                .setTarget(new ViewTarget(R.id.btn_payable_amount, this))
                .singleShot(PreferenceManager.PREF_BILLING_SHOT)
                .setContentTitle("Click here to pay.").blockAllTouches()
                .setStyle(R.style.CustomCoachMarksTheme)
                .setShowcaseDrawer(new CustomShowCaseView(mBillingBinding.btnPayableAmount, getResources()))
                .build();
        showcaseView.forceTextPosition(ShowcaseView.ABOVE_SHOWCASE);
        showcaseView.hideButton();
        if (mBilModelData.getResults() != null && mBilModelData.getResults().size() > 0)
            showcaseView.addView(getButton(), secondParams);
    }*/

    /**
     * provides the button on coach marks screen
     * @return the  button for guidance
     */
    /*public Button getButton() {
        final Button button = (Button) LayoutInflater.from(this).inflate(R.layout.showcase_custom_view_button, showcaseView, false);
        secondParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        secondParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        secondParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        secondParams.setMargins(15, 10, 10, 30);
        button.setOnClickListener(coachClicks);
        showCount++;
        return button;
    }*/

}
