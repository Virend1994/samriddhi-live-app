package com.app.samriddhi.ui.model;

public class AgencyOtherNewsPaperSet {
    public String newspaper_id;

    public String getNewspaper_id() {
        return newspaper_id;
    }

    public void setNewspaper_id(String newspaper_id) {
        this.newspaper_id = newspaper_id;
    }
}
