package com.app.samriddhi.ui.activity.main.ui.Grievance;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseFragment;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.Adapter.PoAdapter;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.POModal.DailyPOModal;
import com.app.samriddhi.ui.view.IView;
import com.app.samriddhi.util.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class OpenFragment extends BaseFragment {

    RecyclerView recyclerview;
    LinearLayoutManager linearLayoutManager;
    List<RequestDetailModal> listModal;
    RequestDetailAdapter requestDetailAdapter;
    TextView txtNotfound;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_open, container, false);
        txtNotfound = view.findViewById(R.id.txtNotfound);
        recyclerview = view.findViewById(R.id.recyclerview);
        recyclerview.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerview.setLayoutManager(linearLayoutManager);
        getRequests();
        return view;
    }

    public void getRequests() {
        enableLoadingBar(true);

        Log.e("dfgdfgdfgdg", PreferenceManager.getAgentId(getActivity()));
        Log.e("dfgdfgdfgdg", PreferenceManager.getComplaint_Type(getActivity()));


        AndroidNetworking.post(Constant.BASE_URL_Grievance + "agentservices/fetchCQRSbyType")
                .addBodyParameter("agent_id", PreferenceManager.getAgentId(getActivity()))
                .addBodyParameter("type", PreferenceManager.getComplaint_Type(getActivity()))
                .setTag("Details")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            listModal = new ArrayList<>();
                            Log.e("sdgsdgsdg",response.toString());
                            JSONArray jsonArray = new JSONArray(response.getString("details"));

                            for (int i = 0; i < jsonArray.length(); i++) {

                                RequestDetailModal requestDetailModal = new RequestDetailModal();
                                JSONObject jsonObject=jsonArray.getJSONObject(i);

                                if(!jsonObject.getString("status").equals("4")){
                                    requestDetailModal.setId(jsonObject.getString("id"));
                                    requestDetailModal.setNumber(jsonObject.getString("cqrs_code"));
                                    requestDetailModal.setStatus(jsonObject.getString("status"));
                                    requestDetailModal.setType(jsonObject.getString("categoryName"));
                                    requestDetailModal.setSub_cat_name(jsonObject.getString("subCategoryName"));
                                    requestDetailModal.setDate(jsonObject.getString("com_date"));
                                    requestDetailModal.setDescription(jsonObject.getString("details"));
                                    requestDetailModal.setHo_remark(jsonObject.getString("ho_remark"));
                                    requestDetailModal.setHo_close_date(jsonObject.getString("ho_close_date"));
                                    //requestDetailModal.setPath(response.getString("image_url"));
                                  //  requestDetailModal.setImage(jsonObject.getString("image"));
                                    listModal.add(requestDetailModal);

                                }
                            }

                            requestDetailAdapter = new RequestDetailAdapter(listModal, getActivity());
                            recyclerview.setAdapter(requestDetailAdapter);
                            dismissProgressBar();

                            if(listModal.size()==0){
                                txtNotfound.setVisibility(View.VISIBLE);
                            }
                            else {
                                txtNotfound.setVisibility(View.GONE);
                            }


                        } catch (Exception ex) {
                            dismissProgressBar();
                            Log.e("dfhdfdfhd", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dismissProgressBar();
                        Log.e("dfhdfdfhd", anError.getMessage());

                    }
                });

    }
}