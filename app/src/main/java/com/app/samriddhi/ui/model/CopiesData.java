package com.app.samriddhi.ui.model;

import android.os.Parcel;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CopiesData extends BaseModel {

    public static final Creator<CopiesData> CREATOR = new Creator<CopiesData>() {
        @Override
        public CopiesData createFromParcel(Parcel source) {
            return new CopiesData(source);
        }

        @Override
        public CopiesData[] newArray(int size) {
            return new CopiesData[size];
        }
    };
    @SerializedName("Partner")
    @Expose
    private String partner;
    @SerializedName("AgType")
    @Expose
    private String agType;
    @SerializedName("OrdDate")
    @Expose
    private String ordDate;
    @SerializedName("Pva")
    @Expose
    private String pva;
    @SerializedName("Rate")
    @Expose
    private String rate;
    @SerializedName("GrossCopy")
    @Expose
    private String grossCopy;
    @SerializedName("FreeCopy")
    @Expose
    private String freeCopy;
    @SerializedName("PaidCopy")
    @Expose
    private String paidCopy;
    @SerializedName("Zlao")
    @Expose
    private String zlao;
    @SerializedName("Zcoo")
    @Expose
    private String zcoo;

    public CopiesData() {
    }

    protected CopiesData(Parcel in) {
        super(in);
        this.partner = in.readString();
        this.agType = in.readString();
        this.ordDate = in.readString();
        this.pva = in.readString();
        this.rate = in.readString();
        this.grossCopy = in.readString();
        this.freeCopy = in.readString();
        this.paidCopy = in.readString();
        this.zlao = in.readString();
        this.zcoo = in.readString();
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public String getAgType() {
        return agType;
    }

    public void setAgType(String agType) {
        this.agType = agType;
    }

    public String getOrdDate() {
        return ordDate;
    }

    public void setOrdDate(String ordDate) {
        this.ordDate = ordDate;
    }

    public String getPva() {
        return pva;
    }

    public void setPva(String pva) {
        this.pva = pva;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getGrossCopy() {
        return grossCopy;
    }

    public void setGrossCopy(String grossCopy) {
        this.grossCopy = grossCopy;
    }

    public String getFreeCopy() {
        return freeCopy;
    }

    public void setFreeCopy(String freeCopy) {
        this.freeCopy = freeCopy;
    }

    public String getPaidCopy() {
        return paidCopy;
    }

    public void setPaidCopy(String paidCopy) {
        this.paidCopy = paidCopy;
    }

    public String getZlao() {
        return zlao;
    }

    public void setZlao(String zlao) {
        this.zlao = zlao;
    }

    public String getZcoo() {
        return zcoo;
    }

    public void setZcoo(String zcoo) {
        this.zcoo = zcoo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.partner);
        dest.writeString(this.agType);
        dest.writeString(this.ordDate);
        dest.writeString(this.pva);
        dest.writeString(this.rate);
        dest.writeString(this.grossCopy);
        dest.writeString(this.freeCopy);
        dest.writeString(this.paidCopy);
        dest.writeString(this.zlao);
        dest.writeString(this.zcoo);
    }
}
