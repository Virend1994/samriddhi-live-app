package com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;
import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.base.adapter.DropdownMenuAdapter;
import com.app.samriddhi.base.adapter.Edition_CR_Adapter;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.model.DropDownModel;
import com.app.samriddhi.ui.model.EditionCRModel;
import com.app.samriddhi.util.Globals;
import com.app.samriddhi.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SunAgentAdapter extends RecyclerView.Adapter<SunAgentAdapter.halfViewHolder> implements DropdownMenuAdapter.OnMeneuClickListnser {

    private final ArrayList<SubAgentModal> srList;
    private Context mcontext;
    public ArrayList<DropDownModel> EditionMasterArray;
    Globals g = Globals.getInstance(mcontext);
    final OnclickListener onclickListener;
    int getViewPosition;

    public interface OnclickListener {
        void getSubAgentEditionList(ArrayList<SubAgentModal> EditionMasterArray);
    }

    public SunAgentAdapter(Context context, ArrayList<SubAgentModal> srList, OnclickListener onclickListener) {
        this.mcontext = context;
        this.srList = srList;
        this.onclickListener = onclickListener;
        EditionMasterArray = new ArrayList<>();
    }

    @NonNull
    @Override
    public halfViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.sub_agent_edition_adapter, parent, false);

        return new halfViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final halfViewHolder holder, final int position) {

        Log.e("dgsgsdgsgs",position+"");

        holder.noofcopy.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                g.editiondata_subAgent_rec_datalist.get(position).setCopy(holder.noofcopy.getText().toString());

            }
        });

        holder.edition.setOnClickListener(v -> {
            getViewPosition = position;
            Util.showDropDown(EditionMasterArray, "Select Edition", mcontext, this::onOptionClick);
        });
        Log.e("sdsdgsdgs",g.editiondata_subAgent_rec_datalist.size()+"");
        Log.e("sdsdgsdgs",g.editiondata_subAgent_rec_datalist.get(position).getName());
        if (g.editiondata_subAgent_rec_datalist.size() > 0) {
            holder.edition.setText(g.editiondata_subAgent_rec_datalist.get(position).getName());
            holder.noofcopy.setText(g.editiondata_subAgent_rec_datalist.get(position).getCopy());
        }

        holder.deleteItem.setOnClickListener(v -> {

            if (srList.size() > 1) {
                removeItem(position);
            }

        });

        if (srList.size() == 1) {
            holder.deleteItem.setVisibility(View.GONE);
        }


    }


    private void removeItem(int position) {
        srList.remove(position);
        //   notifyItemRemoved(position);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return srList.size();
    }

    @Override
    public void onOptionClick(DropDownModel liveTest) {

        Util.hideDropDown();
        g.editiondata_subAgent_rec_datalist.get(getViewPosition).setPk(liveTest.getId());
        g.editiondata_subAgent_rec_datalist.get(getViewPosition).setName(liveTest.getDescription());
        g.editiondata_subAgent_rec_datalist.get(getViewPosition).setEdition_code(liveTest.getName());
        notifyDataSetChanged();
        onclickListener.getSubAgentEditionList(srList);
    }

    public class halfViewHolder extends RecyclerView.ViewHolder {
        AppCompatImageView deleteItem;
        TextView edition, noofcopy;

        public halfViewHolder(View view) {
            super(view);
            deleteItem = view.findViewById(R.id.deleteItem);
            edition = view.findViewById(R.id.edition_spin);
            noofcopy = view.findViewById(R.id.edition_copy);
            getEdition();
        }
    }

    private void getEdition() {
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getEditionByUnitId(PreferenceManager.getAgentId(mcontext), g.cirType, g.unitId);
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            EditionMasterArray.clear();
                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("edition_name"));
                                dropDownModel.setName(objStr.getJSONObject("fields").getString("edition_code"));

                                EditionMasterArray.add(dropDownModel);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(mcontext, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(mcontext, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Toast.makeText(mcontext, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }


}