package com.app.samriddhi.ui.model;

import java.io.Serializable;

public class ASDDaysEdition implements Serializable {
    public String edition_code;
    public String edition_copies;

    public String getEdition_code() {
        return edition_code;
    }

    public void setEdition_code(String edition_code) {
        this.edition_code = edition_code;
    }

    public String getEdition_copies() {
        return edition_copies;
    }

    public void setEdition_copies(String edition_copies) {
        this.edition_copies = edition_copies;
    }

}
