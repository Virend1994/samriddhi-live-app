package com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.samriddhi.R;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.Cheque_CR_Adapter;
import com.app.samriddhi.base.adapter.Drop_Point_Adapter;
import com.app.samriddhi.base.adapter.DropdownMenuAdapter;
import com.app.samriddhi.base.adapter.Edition_CR_Adapter;

import com.app.samriddhi.databinding.ActivityCirculationReciptBinding;
import com.app.samriddhi.prefernces.PreferenceManager;

import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AppsContants;
import com.app.samriddhi.ui.model.ASDDaysEdition;
import com.app.samriddhi.ui.model.ASDDaysModel;
import com.app.samriddhi.ui.model.AgencyRequestCIRReqSetModel;
import com.app.samriddhi.ui.model.BankListModel;
import com.app.samriddhi.ui.model.CirReqChequeSetModel;
import com.app.samriddhi.ui.model.CirReqDroppingSetModel;
import com.app.samriddhi.ui.model.CirReqDroppingSetModelAdpter;
import com.app.samriddhi.ui.model.CirReqEditionSetModel;
import com.app.samriddhi.ui.model.DropDownModel;
import com.app.samriddhi.ui.model.EditionCRModel;

import com.app.samriddhi.util.AlertUtility;
import com.app.samriddhi.util.Globals;
import com.app.samriddhi.util.Util;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Circulation_JJForm extends BaseActivity implements Edition_CR_Adapter.OnclickListener,SelfEditionAdapter.OnclickListener, DropdownMenuAdapter.OnMeneuClickListnser {


    ActivityCirculationReciptBinding circulationReciptBinding;
    String strcontactOne="";
    String strcontactTwo="";
    List<BothEditionModal> bothlist;
    String strType = "";
    public ArrayList<String> s_no1 = new ArrayList<String>();
    public ArrayList<String> edition = new ArrayList<String>();
    public ArrayList<String> cheque = new ArrayList<String>();
    Context context;
    Circulation_JJForm dropdownMenuAdapterClass;

    private String routetype = "";
    int editionCopy = 0;
    RadioButton post_dated_yes, post_dated_no;
    String cirreq_id = "";
    int dropCopy = 0;
    boolean perStatus = false;
    int asdDate = 0;
    Globals globals = Globals.getInstance(context);
    public ArrayList<DropDownModel> dpMasterArray;
    ArrayList<DropDownModel> centerExecutiveMasterArray;
    ArrayList<DropDownModel> uohMasterArray;
    ArrayList<DropDownModel> sohMasterArray;
    ArrayList<DropDownModel> EditionMasterArray;

    String strAsdColl="";
    ArrayList<CirReqEditionSetModel> cirReqEditionSetModelList;
    ArrayList<DropDownModel> routeMasterArray;
    Cheque_CR_Adapter adapter_cheque;
    ArrayList<CirReqChequeSetModel> cirReqChequeSetModelList;
    ArrayList<CirReqDroppingSetModel> cirReqDroppingSetModelList;
    ArrayList<DropDownModel> dropListRouteType;
    double asdTotal = 0;
    Double h = 0.0;
    int perValue = 0;
    double asdCopyRs = 0;
    SelfEditionAdapter adapter_edi_2;
    SunAgentAdapter adapter_edi_3;
    AlertDialog alertDialog;
    static int formstatus = 0;
    ArrayList<ASDDaysEdition> ASDDaysEditionlList;
    String cityupc = "", post_dated_yesno = "", asdfulfil_yesno = "", paymode = "", paymodeType = "", state_spin_val = "", unit_spin_val = "", edition_spin_val = "", location_spin_val = "", executive_name = "", editionName = "", editionId = "", executive_id = "", uoh_name = "", uoh_id = "", soh_name = "", soh_id = "",
            droing_point_mapping = "", droing_point_copy = "", droing_point_route = "", dp_val = "", route_spin_val = "";
    private String dropSelectType = "";
    Drop_Point_Adapter dropAdapter;
    Edition_CR_Adapter adapter_edi;
    String routeId = "";
    String strStateId = "";
    String strUnitID = "";
    String strJJSales = "",strReason="";
    String strSelfType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_circulation__recipt);
        circulationReciptBinding = DataBindingUtil.setContentView(this, R.layout.activity_circulation__recipt);
        context = this;


        this.dropdownMenuAdapterClass = this;
        globals.cirType = "JJ";

        dpMasterArray = new ArrayList<>();
        dropListRouteType = new ArrayList<>();
        DropDownModel obj = new DropDownModel();
        obj.setDescription("Self");
        dropListRouteType.add(obj);
        DropDownModel obj1 = new DropDownModel();
        obj1.setDescription("Sub Agent");
        dropListRouteType.add(obj1);
        DropDownModel obj2 = new DropDownModel();
        obj2.setDescription("Both");
        dropListRouteType.add(obj2);
        ASDDaysEditionlList = new ArrayList<>();
        centerExecutiveMasterArray = new ArrayList<>();
        uohMasterArray = new ArrayList<>();
        sohMasterArray = new ArrayList<>();
        cirReqEditionSetModelList = new ArrayList<>();
        routeMasterArray = new ArrayList<>();
        EditionMasterArray = new ArrayList<>();
        cirReqChequeSetModelList = new ArrayList<>();
        cirReqDroppingSetModelList = new ArrayList<>();

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, MODE_PRIVATE);
        strStateId = AppsContants.sharedpreferences.getString(AppsContants.StateId, "");
        strUnitID = AppsContants.sharedpreferences.getString(AppsContants.UnitId, "");
        strJJSales = AppsContants.sharedpreferences.getString(AppsContants.SalesJJ, "");
        strReason = AppsContants.sharedpreferences.getString(AppsContants.Reason, "");
        Log.e("dfgdfgdf","State_id"+" "+strStateId);
        Log.e("dfgdfgdf","Unit_id"+" "+strUnitID);



        getCenterExecutiveName();
        getUOHData();
        getSOHData();


        circulationReciptBinding.toolbar.setTitle("JJ Circulation Form ");
        setSupportActionBar(circulationReciptBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        formstatus = globals.JJCIRStatus;
        if (formstatus == 1) {
            globals.cheque_rec_datalist.clear();
            //Toast.makeText(this,"Yes",Toast.LENGTH_LONG).show();
            getCIRData();
        } else {
            // Toast.makeText(this,"no",Toast.LENGTH_LONG).show();
            globals.cheque_rec_datalist.clear();
            globals.SelectedEditionMasterArray.clear();
        }
        initViews();

        circulationReciptBinding.typeOfDrop.setOnClickListener(v -> {
            dropSelectType = "routetype";
            Util.showDropDown(dropListRouteType, "Select Droping Point", Circulation_JJForm.this, dropdownMenuAdapterClass);


        });

        circulationReciptBinding.getCovered.setOnClickListener(v -> {
            ASDDaysEditionlList.clear();
            int editionCopyCount = 0;

            for (int i = 0; i < globals.editiondata_rec_datalist.size(); i++) {
                ASDDaysEdition objCir = new ASDDaysEdition();

                // Log.e("ediPk"+i,g.editiondata_rec_datalist.get(i).get("ediPk").toString());


                if (globals.editiondata_rec_datalist.get(i).getPk().length() == 0) {
                    alertMessage("select edition");
                    return;
                }

                if (globals.editiondata_rec_datalist.get(i).getCopy().length() == 0) {
                    alertMessage("select edition copy");
                    return;
                }
                String edi_val = globals.editiondata_rec_datalist.get(i).getEdition_code();
                String copy_val = globals.editiondata_rec_datalist.get(i).getCopy();
                objCir.setEdition_copies(copy_val);
                objCir.setEdition_code(edi_val);
                editionCopyCount += Integer.parseInt(copy_val);
                ASDDaysEditionlList.add(objCir);

            }

            if (editionCopyCount > globals.jjCirNoOfCopy || editionCopyCount < globals.jjCirNoOfCopy) {
                alertMessage("Edition copy should not be greater than/less than of total no of copy : " + globals.jjCirNoOfCopy);
                return;
            }

            if (circulationReciptBinding.asdCollCr.getText().length() == 0) {
                alertMessage("Please enter asd collect");
                return;
            }
            ASDDaysModel objDays = new ASDDaysModel();
            objDays.setEditionMasterArray(ASDDaysEditionlList);
            objDays.setJj_flag(0);
            int IntValue = (int) Math.round(globals.jjCirNoOfCopy);
            objDays.setCopies(IntValue);
            objDays.setAsdcollected(circulationReciptBinding.asdCollCr.getText().toString());
            objDays.setCommission(circulationReciptBinding.commissionCr.getText().toString());
            getASDCovered(objDays);
        });



        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strUOHStatus = AppsContants.sharedpreferences.getString(AppsContants.UOHApproveStatus, "");
        String SOHApproveStatus = AppsContants.sharedpreferences.getString(AppsContants.SOHApproveStatus, "");
        String HoApproveStatus = AppsContants.sharedpreferences.getString(AppsContants.HoApproveStatus, "");

      /*  if (PreferenceManager.getPrefUserType(this).equals("UH")) { *//* Do not show to Agent *//*
            if (strUOHStatus.equals("1")) {
                circulationReciptBinding.saveCr.setVisibility(View.GONE);
            } else {
                circulationReciptBinding.saveCr.setVisibility(View.VISIBLE);
            }
        } else if (PreferenceManager.getPrefUserType(this).equals("SH")) { *//* Do not show to SOH *//*
            if (SOHApproveStatus.equals("1")) {
                circulationReciptBinding.saveCr.setVisibility(View.GONE);
            } else {
                circulationReciptBinding.saveCr.setVisibility(View.VISIBLE);
            }

        } else if (PreferenceManager.getPrefUserType(this).equals("HO")) { *//* Do not show to HO *//*
            if (HoApproveStatus.equals("1")) {
                circulationReciptBinding.saveCr.setVisibility(View.GONE);
            } else {
                circulationReciptBinding.saveCr.setVisibility(View.VISIBLE);
            }
        }*/
    }

    @Override
    public boolean onSupportNavigateUp() {


        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        View view1 = LayoutInflater.from(context).inflate(R.layout.back_alert_layout, null);

        MaterialButton btnNo = view1.findViewById(R.id.btnNo);
        MaterialButton btnYes = view1.findViewById(R.id.btnYes);

        btnYes.setOnClickListener(v -> {
            alertDialog.dismiss();
            alertDialog.cancel();
            finish();
        });

        btnNo.setOnClickListener(V -> {
            alertDialog.dismiss();
            alertDialog.cancel();
        });
        builder.setView(view1);
        alertDialog = builder.create();
        alertDialog.show();
        return true;
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        View view1 = LayoutInflater.from(context).inflate(R.layout.back_alert_layout, null);

        MaterialButton btnNo = view1.findViewById(R.id.btnNo);
        MaterialButton btnYes = view1.findViewById(R.id.btnYes);

        btnYes.setOnClickListener(v -> {
            alertDialog.dismiss();
            alertDialog.cancel();
            finish();
        });

        btnNo.setOnClickListener(V -> {
            alertDialog.dismiss();
            alertDialog.cancel();
        });
        builder.setView(view1);
        alertDialog = builder.create();
        alertDialog.show();

    }

    private void initViews() {
        getASD();
        getPageGroup();
        getEdition();
        // getDropPoint();
        getRoute();


        edition.clear();
        edition.add("1");

        globals.editiondata_rec_datalist.clear();
        globals.editiondata_self_rec_datalist.clear();
        globals.editiondata_subAgent_rec_datalist.clear();

        for (int i = 0; i < edition.size(); i++) {
            EditionCRModel map = new EditionCRModel();
            map.setPk("");
            map.setCopy("");
            map.setName("");
            map.setStatus("Add");
            globals.editiondata_rec_datalist.add(map);


            SelfEditionModals selfmap = new SelfEditionModals();
            selfmap.setPk("");
            selfmap.setCopy("");
            selfmap.setName("");
            globals.editiondata_self_rec_datalist.add(selfmap);


            SubAgentModal submap = new SubAgentModal();
            submap.setPk("");
            submap.setCopy("");
            submap.setName("");
            globals.editiondata_subAgent_rec_datalist.add(submap);
        }


        circulationReciptBinding.selfDrop.setText(globals.agencyNameStr);
        circulationReciptBinding.contactNumber.setText(globals.agentMobileStr);

        circulationReciptBinding.droopingPoint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dropSelectType = "dp";
                Util.showDropDown(dpMasterArray, "Select Droping Point", Circulation_JJForm.this, dropdownMenuAdapterClass);

            }
        });

        circulationReciptBinding.mapRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dropSelectType = "route";
                Util.showDropDown(routeMasterArray, "Select Route", Circulation_JJForm.this, dropdownMenuAdapterClass);

            }
        });


        circulationReciptBinding.editionRec.setHasFixedSize(true);
        circulationReciptBinding.editionRec.setNestedScrollingEnabled(true);
        circulationReciptBinding.editionRec.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter_edi = new Edition_CR_Adapter(Circulation_JJForm.this, globals.editiondata_rec_datalist, dropdownMenuAdapterClass);
        circulationReciptBinding.editionRec.setAdapter(adapter_edi);
        adapter_edi.notifyDataSetChanged();


        circulationReciptBinding.dropPointSelfRec.setHasFixedSize(true);
        circulationReciptBinding.dropPointSelfRec.setNestedScrollingEnabled(true);
        circulationReciptBinding.dropPointSelfRec.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter_edi_2 = new SelfEditionAdapter(Circulation_JJForm.this, globals.editiondata_self_rec_datalist, dropdownMenuAdapterClass);
        circulationReciptBinding.dropPointSelfRec.setAdapter(adapter_edi_2);
        adapter_edi_2.notifyDataSetChanged();

        circulationReciptBinding.addedition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edition.add("2");
                EditionCRModel map = new EditionCRModel();
                map.setPk("");
                map.setName("");
                map.setCopy("");
                map.setStatus("Add");
                globals.editiondata_rec_datalist.add(map);
                adapter_edi.notifyDataSetChanged();

                SelfEditionModals selfmap = new SelfEditionModals();
                selfmap.setPk("");
                selfmap.setCopy("");
                selfmap.setName("");
                globals.editiondata_self_rec_datalist.add(selfmap);
                adapter_edi_2.notifyDataSetChanged();


                SubAgentModal submap = new SubAgentModal();
                submap.setPk("");
                submap.setCopy("");
                submap.setName("");
                globals.editiondata_subAgent_rec_datalist.add(submap);
            }
        });

        circulationReciptBinding.tvEdition.setOnClickListener(v -> {
            dropSelectType = "edition";
            Util.showDropDown(globals.SelectedEditionMasterArray, "Select Edition", Circulation_JJForm.this, this::onOptionClick);

        });

        droppingPoint();
        cheque.clear();
        cheque.add("1");


        for (int i = 0; i < cheque.size(); i++) {
            CirReqChequeSetModel map = new CirReqChequeSetModel();

            map.setCheque_amt("");
            map.setCheque_date("");


            globals.cheque_rec_datalist.add(map);

        }




        circulationReciptBinding.chequeRec.setHasFixedSize(true);
        circulationReciptBinding.chequeRec.setNestedScrollingEnabled(true);
        circulationReciptBinding.chequeRec.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        adapter_cheque = new Cheque_CR_Adapter(Circulation_JJForm.this, globals.cheque_rec_datalist);
        circulationReciptBinding.chequeRec.setAdapter(adapter_cheque);
        adapter_cheque.notifyDataSetChanged();

        circulationReciptBinding.addCheque.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CirReqChequeSetModel map = new CirReqChequeSetModel();

                map.setCheque_amt("");
                map.setCheque_date("");


                globals.cheque_rec_datalist.add(map);
                adapter_cheque.notifyDataSetChanged();
            }
        });

        circulationReciptBinding.centerExeName.setOnClickListener(v -> {
            dropSelectType = "exe";
            Util.showDropDown(centerExecutiveMasterArray, "Select Center Executive Name", Circulation_JJForm.this, this::onOptionClick);
        });
        circulationReciptBinding.uohCr.setOnClickListener(v -> {
            dropSelectType = "uoh";
            Util.showDropDown(uohMasterArray, "Select UOH", Circulation_JJForm.this, this::onOptionClick);
        });
        circulationReciptBinding.sohCr.setOnClickListener(v -> {
            dropSelectType = "soh";
            Util.showDropDown(sohMasterArray, "Select SOH", Circulation_JJForm.this, this::onOptionClick);
        });

        asdDate = 2;
        post_dated_yesno = "0";
        circulationReciptBinding.postDatedRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.post_dated_yes:
                        post_dated_yesno = "1";
                        circulationReciptBinding.layoutViewASD.setVisibility(View.VISIBLE);
                        asdDate = 0;
                        break;
                    case R.id.post_dated_no:

                        post_dated_yesno = "0";
                        asdDate = 2;
                        circulationReciptBinding.layoutViewASD.setVisibility(View.GONE);
                        break;

                }
            }
        });


        circulationReciptBinding.dateCr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int mYear, mMonth, mDay;
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


                DatePickerDialog datePickerDialog = new DatePickerDialog(Circulation_JJForm.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {


                                String monthYear = "", DayOfMonth = "";
                                if (monthOfYear < 9) {
                                    monthYear = "0";
                                } else {
                                    monthYear = "";
                                }
                                if (dayOfMonth < 9) {
                                    DayOfMonth = "0";
                                } else {
                                    DayOfMonth = "";
                                }

                                circulationReciptBinding.dateCr.setText(year + "-" + monthYear + (monthOfYear + 1) + "-" + DayOfMonth + dayOfMonth);
                                //  ppr_rcv_date_value = String.format("%02d/%02d/%02d", dayOfMonth,monthOfYear + 1,year);


                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();

            }
        });

        paymode = "DD";
        paymodeType = "1";
        circulationReciptBinding.paymodeRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.dd:
                        paymode = "DD";
                        paymodeType = "1";
                        break;
                    case R.id.cheque:
                        paymode = "Cheque";
                        paymodeType = "2";
                        break;
                    case R.id.neft_rtgs:
                        paymode = "NEFT/RTGS";
                        paymodeType = "3";
                        break;
                }
            }
        });
        s_no1.clear();
        s_no1.add("1");

        // demoBinding.rec.setLayoutManager(new LinearLayoutManager(this));

        circulationReciptBinding.commissionCr.setOnClickListener(v -> {
            popupWindow1();
        });
        circulationReciptBinding.addroute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CirReqDroppingSetModelAdpter map = new CirReqDroppingSetModelAdpter();
                map.setDroppingName("");
                map.setRoutePk("");
                map.setDroppingPk("");
                map.setSelf_copy("");
                map.setContact("");
                map.setNoofcopy("");
                map.setRouteName("");
                map.setEditionPk("");
                map.setEditionName("");
                globals.droppoint_rec_datalist.add(map);
                dropAdapter.notifyDataSetChanged();
            }
        });

        circulationReciptBinding.asdCollCr.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {

                    double getasdColl = Double.parseDouble(circulationReciptBinding.asdCollCr.getText().toString());


                    if (getasdColl < asdTotal) {

                        asdDate = 1;

                        circulationReciptBinding.postCheckLayout.setVisibility(View.VISIBLE);
                    } else {
                        asdDate = 2;
                        circulationReciptBinding.postCheckLayout.setVisibility(View.GONE);
                    }
                    double divideAsd = 0;
                    if (asdCopyRs > 0) {
                        divideAsd = getasdColl / asdCopyRs;
                    } else {
                        divideAsd = 0;
                    }


                    circulationReciptBinding.tvScalableCopy.setText(String.valueOf(Math.round(divideAsd)));


                } else {
                    circulationReciptBinding.tvScalableCopy.setText("");
                    circulationReciptBinding.postCheckLayout.setVisibility(View.GONE);
                }


            }
        });

        circulationReciptBinding.saveCr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertUtility.showAlert(context, "Are you sure want to save Main Circulation Data", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        String noofcopy = "";
                        String date = circulationReciptBinding.dateCr.getText().toString().replace("T00:00:00", "");
                        String commision = circulationReciptBinding.commissionCr.getText().toString();
                        String asdNorm = circulationReciptBinding.asdNormsCr.getText().toString();
                        String asd_coll = circulationReciptBinding.asdCollCr.getText().toString();
                       // String asdcoverd = circulationReciptBinding.asdCovered.getText().toString();
                        String asdcoverd = circulationReciptBinding.tvCoveredCopy.getText().toString();
                        String remark = circulationReciptBinding.remarkCr.getText().toString();
                        String self_copi = circulationReciptBinding.selfDrop.getText().toString();
                        String conno = circulationReciptBinding.contactNumber.getText().toString();
                      //  String dp_copy = circulationReciptBinding.mapCopy.getText().toString();
                        String dp_copy = "0";

                        editionCopy = 0;

                        dropCopy = 0;
                        cirReqChequeSetModelList.clear();

                        cirReqEditionSetModelList.clear();

                        for (int i = 0; i < globals.editiondata_rec_datalist.size(); i++) {
                            CirReqEditionSetModel obj1 = new CirReqEditionSetModel();

                            // Log.e("ediPk"+i,g.editiondata_rec_datalist.get(i).get("ediPk").toString());


                            if (globals.editiondata_rec_datalist.get(i).getPk().length() == 0) {
                                alertMessage("select edition");
                                return;
                            }

                            if (globals.editiondata_rec_datalist.get(i).getCopy().length() == 0) {
                                alertMessage("select edition copy");
                                return;
                            }
                            String edi_val = globals.editiondata_rec_datalist.get(i).getPk();
                            String copy_val = globals.editiondata_rec_datalist.get(i).getCopy();
                            obj1.setEdition_copies(copy_val);
                            obj1.setEdition_id(edi_val);
                            editionCopy += Integer.parseInt(copy_val);
                            cirReqEditionSetModelList.add(obj1);

                        }
                      /*  if (editionCopy > g.jjCirNoOfCopy || editionCopy < g.jjCirNoOfCopy) {
                            alertMessage("Edition copy should not be grater then/less then of total no of copy : " + g.jjCirNoOfCopy);
                            return;
                        }*/
                        if (date.length() == 0 || date.equalsIgnoreCase("null")) {
                            alertMessage("Please select Start Date");
                            return;
                        }


                        if (circulationReciptBinding.typeOfDrop.getText().toString().length() == 0) {
                            alertMessage("Please Select the Route Type");
                            return;
                        }

                        if (circulationReciptBinding.remarkCr.getText().toString().length() == 0) {
                            alertMessage("Please Enter The Remark");
                            return;
                        }


                        if (executive_id.length() == 0) {
                            alertMessage("Please select executive");
                            return;
                        }

                        if (asd_coll.length() == 0) {
                            alertMessage(" Enter ASD Collect");
                            return;
                        }

                        if (asdDate == 1) {

                            if (post_dated_yesno.equalsIgnoreCase("")) {
                                alertMessage("Select Post Date yes or no");
                                return;
                            }

                        }
                        if (paymode.length() == 0) {
                            alertMessage("select payment mode ");
                            return;
                        }
                        if (post_dated_yesno.equalsIgnoreCase("1")) {
                            CirReqChequeSetModel obj1 = new CirReqChequeSetModel();

                            for (int i = 0; i < globals.cheque_rec_datalist.size(); i++) {

                                if (globals.cheque_rec_datalist.get(i).getCheque_date().toString().length() == 0 || globals.cheque_rec_datalist.get(i).getCheque_date().toString().equalsIgnoreCase("null")) {
                                    alertMessage("select cheque date");
                                    return;
                                }

                                if (globals.cheque_rec_datalist.get(i).getCheque_amt().length() == 0) {
                                    alertMessage("select amount ");
                                    return;
                                }
                                String date_ch = globals.cheque_rec_datalist.get(i).getCheque_date().toString().replace("T00:00:00", "");
                                String amnt_ch = globals.cheque_rec_datalist.get(i).getCheque_amt();
                                obj1.setCheque_date(date_ch + "T00:00:00");
                                obj1.setCheque_amt(amnt_ch);
                                obj1.setCheque_mode(paymode);
                                cirReqChequeSetModelList.add(obj1);

                            }
                        } else {
                            CirReqChequeSetModel obj1 = new CirReqChequeSetModel();
                            obj1.setCheque_date(null);
                            obj1.setCheque_amt("0");
                            obj1.setCheque_mode("");
                            cirReqChequeSetModelList.add(obj1);
                        }


                        ArrayList<CirReqDroppingSetModel> cirReqDroppingSetModelList = new ArrayList<>();


                        Log.e("daata", globals.droppoint_rec_datalist.toString());
                        if (routetype.equalsIgnoreCase("Self")) {


                            CirReqDroppingSetModel obj2=null;

                            if (circulationReciptBinding.droopingPoint.getText().toString().length() == 0) {
                                alertMessage("Please select your drop point");
                                return;
                            }




                            for (int i = 0; i < bothlist.size(); i++) {
                                int getFirstCopyId = 0;
                                int getSelfCopy = 0;
                                int getFirstCopy = 0;
                                int getSubCopy = 0;
                                int TotalCopies = 0;

                                getFirstCopyId = Integer.parseInt(bothlist.get(i).getEdition_id());
                                getFirstCopy = Integer.parseInt(bothlist.get(i).getEdition_copies());
                                Log.e("sdfsdfsd", getFirstCopy + "");
                                for (int j = 0; j < globals.editiondata_self_rec_datalist.size(); j++) {
                                    if (getFirstCopyId == Integer.parseInt(globals.editiondata_self_rec_datalist.get(j).getPk())) {
                                        getSelfCopy = Integer.parseInt(globals.editiondata_self_rec_datalist.get(j).getCopy());
                                        Log.e("sdfsdfsd", getSelfCopy + "");

                                        TotalCopies = getSelfCopy;
                                        Log.e("sdfsdfsd", TotalCopies + "");
                                        if (TotalCopies > getFirstCopy || TotalCopies < getFirstCopy) {
                                            alertMessage("Edition copies should not be greater");
                                            return;
                                        } else if (TotalCopies < getFirstCopy) {
                                            alertMessage("Edition copies should not be less");
                                            return;
                                        } else {

                                            obj2 = new CirReqDroppingSetModel();
                                            obj2.setDropping_id(dp_val);
                                            obj2.setSelf_copies(0);
                                            obj2.setSub_agent_name(circulationReciptBinding.selfDrop.getText().toString());
                                            obj2.setContact_no(conno);
                                            obj2.setSub_agent_copies("1");
                                            obj2.setRoute_id(route_spin_val);
                                            obj2.setEdition_idName(editionName);
                                            obj2.setEdition_id(globals.editiondata_self_rec_datalist.get(j).getPk());
                                            obj2.setEdition_copies(globals.editiondata_self_rec_datalist.get(j).getCopy());
                                            cirReqDroppingSetModelList.add(obj2);
                                        }
                                    }
                                    //  Log.e("sfdfsfd", "Self" + " " + g.editiondata_self_rec_datalist.get(i).getPk() + " " + g.editiondata_self_rec_datalist.get(i).getCopy());
                                }

                                //  Log.e("sfdfsfd", "First" + " " + bothlist.get(i).getEdition_id() + " " + bothlist.get(i).getEdition_copies());
                            }




                           // cirReqDroppingSetModelList.add(obj2);
                            dropCopy += Integer.parseInt(dp_copy);

                            if (circulationReciptBinding.tvEdition.length() == 0) {
                                alertMessage("Please Select your edition");
                                return;
                            }

                        }

                        else if (routetype.equalsIgnoreCase("Sub Agent")) {

                            strcontactOne = "";
                            strcontactTwo = "";
                            CirReqDroppingSetModel obj = null;


                            //  Log.e("sfdfsfd", "First" + " " + bothlist.get(i).getEdition_id() + " " + bothlist.get(i).getEdition_copies());

                            for (int k = 0; k < globals.editiondata_subAgent_rec_datalist.size(); k++) {


                                obj = new CirReqDroppingSetModel();
                                obj.setEdition_copies(globals.editiondata_subAgent_rec_datalist.get(k).getCopy());
                                obj.setEdition_id(globals.editiondata_subAgent_rec_datalist.get(k).getPk());
                                obj.setEdition_idName(globals.editiondata_subAgent_rec_datalist.get(k).getName());
                                obj.setSub_agent_copies("2");

                                for (int i = 0; i < globals.droppoint_rec_datalist.size(); i++) {

                                    Log.e("dfgdfgdgd", "Mobile" + " " + i + " " + globals.droppoint_rec_datalist.get(i).getContact());

                                    if (i == 0) {
                                        strcontactOne = globals.droppoint_rec_datalist.get(i).getContact();
                                    } else if (i == 1) {
                                        strcontactTwo = globals.droppoint_rec_datalist.get(i).getContact();
                                    }

                                    if (strcontactOne.equals(strcontactTwo)) {
                                        alertMessage("Sub agent's contact cannot be same ");
                                        return;
                                    }
                                    if (globals.droppoint_rec_datalist.get(i).getRoutePk().length() == 0) {
                                        alertMessage("Select sub agent Route Point");
                                        return;
                                    }


                                    if (globals.droppoint_rec_datalist.get(i).getSub_agent_name().length() == 0) {
                                        alertMessage("please enter sub agent name");
                                        return;
                                    }

                                    if (globals.droppoint_rec_datalist.get(i).getContact().length() == 0) {
                                        alertMessage("please enter sub agent number");
                                        return;
                                    }

                                    if (globals.droppoint_rec_datalist.get(i).getDroppingPk().length() == 0) {
                                        alertMessage("Select sub agent Drop Point");
                                        return;
                                    }

                                    String noofcp = globals.droppoint_rec_datalist.get(i).getNoofcopy();
                                    obj.setDropping_id(globals.droppoint_rec_datalist.get(i).getDroppingPk());
                                    obj.setSub_agent_name(globals.droppoint_rec_datalist.get(i).getSub_agent_name());
                                   // obj.setSub_agent_name("sub");

                                  /*  if (g.droppoint_rec_datalist.get(i).getEditionPk() != null && !g.droppoint_rec_datalist.get(i).getEditionPk().equals("null")) {

                                        obj.setEdition_id(g.droppoint_rec_datalist.get(i).getEditionPk());
                                    } else {

                                        obj.setEdition_id("");
                                    }*/


                                    //obj.setEdition_idName(g.droppoint_rec_datalist.get(i).getEditionName());
                                    obj.setAgent_location(globals.droppoint_rec_datalist.get(i).getLocation());
                                    obj.setSelf_copies(0);
                                    obj.setContact_no(globals.droppoint_rec_datalist.get(i).getContact());
                                    if (noofcp.length() != 0) {
                                        dropCopy += Integer.parseInt(noofcp);
                                    }


                                    obj.setRoute_id(globals.droppoint_rec_datalist.get(i).getRoutePk());
                                    cirReqDroppingSetModelList.add(obj);

                                    try {
                                        JSONObject jsonObject = new JSONObject();
                                        jsonObject.put("getNoofcopy", globals.droppoint_rec_datalist.get(i).getNoofcopy());
                                        jsonObject.put("getDroppingPk", globals.droppoint_rec_datalist.get(i).getDroppingPk());
                                        jsonObject.put("getSub_agent_name", globals.droppoint_rec_datalist.get(i).getSub_agent_name());
                                       // jsonObject.put("getSub_agent_name","sub");
                                        jsonObject.put("getEditionPk", globals.droppoint_rec_datalist.get(i).getEditionPk());
                                        jsonObject.put("getEditionName", globals.droppoint_rec_datalist.get(i).getEditionName());
                                        jsonObject.put("getLocation", globals.droppoint_rec_datalist.get(i).getLocation());
                                        jsonObject.put("getContact", globals.droppoint_rec_datalist.get(i).getContact());
                                        jsonObject.put("getRoutePk", globals.droppoint_rec_datalist.get(i).getRoutePk());
                                        Log.e("sdsdgsdgsdg", jsonObject.toString());
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                }
                            }


                            for (int s = 0; s < bothlist.size(); s++) {
                                int getFirstCopyId = 0;
                                int getSelfCopy = 0;
                                int getFirstCopy = 0;
                                int getSubCopy = 0;
                                int TotalCopies = 0;

                                getFirstCopyId = Integer.parseInt(bothlist.get(s).getEdition_id());
                                getFirstCopy = Integer.parseInt(bothlist.get(s).getEdition_copies());
                                Log.e("sdfsdfsd", getFirstCopy + "");

                                for (int k = 0; k < globals.editiondata_subAgent_rec_datalist.size(); k++) {
                                    if (getFirstCopyId == Integer.parseInt(globals.editiondata_subAgent_rec_datalist.get(k).getPk())) {
                                        getSubCopy = Integer.parseInt(globals.editiondata_subAgent_rec_datalist.get(k).getCopy());
                                        Log.e("sdfsdfsd", getSubCopy + "");
                                        TotalCopies = getSubCopy;
                                        Log.e("sdfsdfsd", TotalCopies + "");
                                        if (TotalCopies > getFirstCopy || TotalCopies < getFirstCopy) {
                                            alertMessage("Edition copies should not be greater");
                                            return;
                                        } else if (TotalCopies < getFirstCopy) {
                                            alertMessage("Edition Copies should not be less");
                                            return;
                                        } else {

                                        }

                                    }
                                }
                            }
                        }
                        else if (routetype.equalsIgnoreCase("Both")) {
                            strcontactOne = "";
                            strcontactTwo = "";

                          /*  if (circulationReciptBinding.mapRoute.getText().toString().length() == 0) {
                                alertMessage("Please select your route point");
                                return;
                            }*/


                           /* if (circulationReciptBinding.mapCopy.getText().toString().length() == 0) {
                                alertMessage("Please enter your copy");
                                return;
                            }*/


                            if (circulationReciptBinding.droopingPoint.getText().toString().length() == 0) {
                                alertMessage("Please select your drop point");
                                return;
                            }

                            if (circulationReciptBinding.tvEdition.length() == 0) {
                                alertMessage("Please Select your edition");
                                return;
                            }

                            /*Wait*/

                            for (int k = 0; k < globals.editiondata_subAgent_rec_datalist.size(); k++) {

                                CirReqDroppingSetModel obj = new CirReqDroppingSetModel();
                                obj.setEdition_id(globals.editiondata_subAgent_rec_datalist.get(k).getPk());
                                obj.setEdition_copies(globals.editiondata_subAgent_rec_datalist.get(k).getCopy());
                                obj.setEdition_idName(globals.editiondata_subAgent_rec_datalist.get(k).getName());
                                obj.setSub_agent_copies("2");
                                for (int i = 0; i < globals.droppoint_rec_datalist.size(); i++) {

                                    if (conno.equals(globals.droppoint_rec_datalist.get(i).getContact())) {
                                        alertMessage("Sub agent's contact cannot be same ");
                                        return;
                                    }


                                    if (globals.droppoint_rec_datalist.get(i).getDroppingPk().length() == 0) {
                                        alertMessage("Select sub agent Drop Point");
                                        return;
                                    }

                                    if (globals.droppoint_rec_datalist.get(i).getSub_agent_name().length() == 0) {
                                        alertMessage("please enter sub agent name");
                                        return;
                                    }

                                    if (globals.droppoint_rec_datalist.get(i).getRoutePk().length() == 0) {
                                        alertMessage("Select sub agent Route Point");
                                        return;
                                    }


                                    String noofcp = globals.droppoint_rec_datalist.get(i).getNoofcopy();
                                    obj.setDropping_id(globals.droppoint_rec_datalist.get(i).getDroppingPk());
                                   obj.setSub_agent_name(globals.droppoint_rec_datalist.get(i).getSub_agent_name());
                                    //obj.setSub_agent_name("sub");
                                   /* if (g.droppoint_rec_datalist.get(i).getEditionPk() != null && !g.droppoint_rec_datalist.get(i).getEditionPk().equals("null")) {


                                    } else {

                                        obj.setEdition_id("0");
                                    }*/

                                    // obj.setEdition_idName(g.droppoint_rec_datalist.get(i).getEditionName());
                                    obj.setAgent_location(globals.droppoint_rec_datalist.get(i).getLocation());
                                    obj.setSelf_copies(0);

                                    obj.setContact_no(globals.droppoint_rec_datalist.get(i).getContact());
                                    if (noofcp.length() != 0) {
                                        dropCopy += Integer.parseInt(noofcp);
                                    }
                               /* obj.setEdition_id(g.droppoint_rec_datalist.get(i).getEditionPk());
                                obj.setSub_agent_copies(g.droppoint_rec_datalist.get(i).getNoofcopy());*/
                                    obj.setRoute_id(globals.droppoint_rec_datalist.get(i).getRoutePk());


                                    cirReqDroppingSetModelList.add(obj);
                                }
                            }
                            CirReqDroppingSetModel obj2 = null;

                            for (int i = 0; i < bothlist.size(); i++) {

                                int topCopy = 0;
                                int topCopyId = 0;
                                int selfCopy = 0;
                                int selfId = 0;
                                int subAgentCopies = 0;
                                int subAgentId = 0;
                                int TotalCopies = 0;

                                topCopy = Integer.parseInt(bothlist.get(i).getEdition_copies());
                                topCopyId = Integer.parseInt(bothlist.get(i).getEdition_id());

                                for (int j = 0; j < globals.editiondata_self_rec_datalist.size(); j++) {

                                    if (topCopyId == Integer.parseInt(globals.editiondata_self_rec_datalist.get(j).getPk())) {

                                        obj2 = new CirReqDroppingSetModel();
                                        obj2.setDropping_id(dp_val);
                                        obj2.setSelf_copies(Integer.parseInt(dp_copy));
                                        obj2.setSub_agent_name(circulationReciptBinding.selfDrop.getText().toString());
                                        obj2.setContact_no(conno);
                                        obj2.setSub_agent_copies("1");
                                        obj2.setRoute_id(route_spin_val);
                                        obj2.setEdition_idName(editionName);
                                        obj2.setEdition_id(globals.editiondata_self_rec_datalist.get(j).getPk());
                                        obj2.setEdition_copies(globals.editiondata_self_rec_datalist.get(j).getCopy());
                                        cirReqDroppingSetModelList.add(obj2);

                                        selfId = Integer.parseInt(globals.editiondata_self_rec_datalist.get(j).getPk());
                                        selfCopy = Integer.parseInt(globals.editiondata_self_rec_datalist.get(j).getCopy());
                                        for (int k = 0; k < globals.editiondata_subAgent_rec_datalist.size(); k++) {

                                            if (selfId == Integer.parseInt(globals.editiondata_subAgent_rec_datalist.get(j).getPk())) {

                                                subAgentId = Integer.parseInt(globals.editiondata_subAgent_rec_datalist.get(j).getPk());
                                                subAgentCopies = Integer.parseInt(globals.editiondata_subAgent_rec_datalist.get(j).getCopy());


                                                TotalCopies = selfCopy + subAgentCopies;

                                                if (TotalCopies > topCopy) {
                                                    alertMessage("Edition Copies should not be greater");
                                                    return;
                                                } else if (TotalCopies < topCopy) {

                                                    alertMessage("Edition Copies should not be less");
                                                    return;
                                                } else {

                                                }
                                            }
                                        }

                                    }
                                }

                                Log.e("sfdfsfd", "Top" + " " + bothlist.get(i).getEdition_id() + " " + bothlist.get(i).getEdition_copies());
                            }

                            dropCopy += Integer.parseInt(dp_copy);
                        }


                       /* if (dropCopy > g.jjCirNoOfCopy || dropCopy < g.jjCirNoOfCopy) {
                            alertMessage("Drop copy should not be grater then/less then of total no of copy : " + g.jjCirNoOfCopy);
                            return;
                        }*/

                        AgencyRequestCIRReqSetModel abc = new AgencyRequestCIRReqSetModel();
                        abc.setCirReqEdition_set(cirReqEditionSetModelList);
                        if (asdDate == 2 || post_dated_yesno.equalsIgnoreCase("0")) {
                            // Toast.makeText(Circulation_JJForm.this,"Yes",Toast.LENGTH_LONG).show();
                            globals.cheque_rec_datalist.clear();
                            cirReqChequeSetModelList.clear();
                            abc.setCirReqCheque_set(cirReqChequeSetModelList);
                        } else {
                            abc.setCirReqCheque_set(cirReqChequeSetModelList);
                        }


                        if (circulationReciptBinding.remarkCr.getText().toString().length() == 0) {
                            alertMessage("Please Enter The Remark");
                            return;
                        }


                        abc.setCirReqDropping_set(cirReqDroppingSetModelList);
                        abc.setEdition_count(globals.editiondata_rec_datalist.size());

                        abc.setStart_date(date + "T00:00:00");

                        abc.setCommission_per(perValue);
                        abc.setCommission_amt(commision);



                        abc.setAsd_as_per_norms(asdNorm);
                        double asd_collValue = Double.parseDouble(asd_coll);

                        double asdNormValue = Double.parseDouble(asdNorm);
//                if(asd_collValue>asdNormValue){
//
//                }else{
//
//
//                }
//
//                if(editionCopy>g.mainNoOfCopy){
//
//                }else{
//
//                }
                        abc.setAsd_collected(asd_coll);

                        abc.setCheque_count(globals.cheque_rec_datalist.size());
                       // abc.setAsd_mode(paymodeType);
                        abc.setAsd_mode(paymode);
                        abc.setAsd_days(asdcoverd);
                        abc.setDropping_count(globals.droppoint_rec_datalist.size());
                        abc.setExecutive_name(executive_name);
                        abc.setExecutive_code(executive_id);
                        abc.setUOH_name(uoh_name);
                        abc.setUOH_code(uoh_id);
                        abc.setSOH_name(soh_name);
                        abc.setMainorjj("JJ");
                        //abc.setCopies_rate(asdCopyRs+"");
                        abc.setSOH_code(soh_id);
                        abc.setAg_detail_id(Integer.parseInt(globals.agentId));
                        abc.setCreation_comments(remark);

                        globals.agencyRequestCIRReqSetModelList.add(abc);


                        Gson gson = new Gson();
                        addCirForm(abc);
                        //  postData(rootModelList);
                        //    Toast.makeText(context, "dfdfgdgd", Toast.LENGTH_SHORT).show();
                        String data = gson.toJson(abc);
                        Log.e("agencyRequest", data);
                        // Toast.makeText(JJCirculation_Reciptbhs.this, "Data Save Successfully", Toast.LENGTH_SHORT).show();

                    }
                });
                //   finish();

            }
        });


    }

    public void getCIRData() {
        enableLoadingBar(true);

        globals.editiondata_self_rec_datalist.clear();
        globals.editiondata_subAgent_rec_datalist.clear();

        Log.e("agentId", globals.agentId);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getCIRFORM(globals.agentId, "JJ");
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                enableLoadingBar(false);
                if (response.isSuccessful()) {

                    assert response.body() != null;

                    try {
                        JSONArray jsonArray = new JSONArray(response.body());


                        if (jsonArray.length() > 0) {


                            Log.e("mainJj", jsonArray.toString());
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject obj = jsonArray.getJSONObject(i);
                                cirreq_id = obj.getString("cirreq_id");

                                circulationReciptBinding.dateCr.setText(obj.getString("start_date").replace("T00:00:00", ""));

                                circulationReciptBinding.commissionCr.setText(String.valueOf(Math.round(Float.valueOf(obj.getString("commission_amt")))));
                              //  circulationReciptBinding.commissionCr.setText(String.valueOf(Math.round(Float.valueOf(obj.getString("commission_per")))));
                                circulationReciptBinding.asdNormsCr.setText(String.valueOf(Math.round(Float.valueOf(obj.getString("asd_as_per_norms")))));
                                circulationReciptBinding.asdCollCr.setText(String.valueOf(Math.round(Float.valueOf(obj.getString("asd_collected")))));
                                circulationReciptBinding.centerExeName.setText(obj.getString("executive_name"));
                                circulationReciptBinding.tvCoveredCopy.setText(obj.getString("asd_days"));
                                circulationReciptBinding.remarkCr.setText(obj.getString("creation_comments"));
                                circulationReciptBinding.uohCr.setText(obj.getString("UOH_name"));
                                circulationReciptBinding.sohCr.setText(obj.getString("SOH_name"));

                                uoh_id = obj.getString("UOH_code");
                                uoh_name = obj.getString("UOH_name");
                                soh_name = obj.getString("SOH_name");
                                executive_name = obj.getString("executive_name");
                                soh_id = obj.getString("SOH_code");
                                executive_id = obj.getString("executive_code");
                                 strAsdColl = String.valueOf(Math.round(Float.valueOf(obj.getString("asd_collected"))));
                                asdTotal=Double.parseDouble(obj.getString("asd_as_per_norms"));
                               // asdCopyRs = Double.parseDouble(obj.getString("copies_rate"));


                                JSONArray CirReqEdition_setArray = new JSONArray(obj.getString("CirReqEdition_set"));
                                try {
                                    if (CirReqEdition_setArray.length() > 0) {
                                        globals.editiondata_rec_datalist.clear();
                                    }

                                    Log.e("jsonArray", CirReqEdition_setArray.toString());
                                    editionCopy = 0;
                                    globals.SelectedEditionMasterArray.clear();

                                    JSONArray droppoint_rec_datalist = new JSONArray(obj.getString("CirReqDropping_set"));
                                    int selfCount = 0;
                                    int dropCount = 0;
                                    if (droppoint_rec_datalist.length() > 0) {
                                        routetype = "";
                                        globals.droppoint_rec_datalist.clear();
                                        dropAdapter.notifyDataSetChanged();
                                    }

                                    for (int k = 0; k < droppoint_rec_datalist.length(); k++) {
                                        JSONObject dropingstr = droppoint_rec_datalist.getJSONObject(k);
                                        CirReqDroppingSetModelAdpter map = new CirReqDroppingSetModelAdpter();
                                      //  strType = dropingstr.getString("sub_agent_name");
                                        strType = dropingstr.getString("sub_agent_copies");

                                        if (dropingstr.getString("sub_agent_copies").equalsIgnoreCase("2")) { /*Sub-Agent*/
                                            if(globals.droppoint_rec_datalist.size()==0){

                                                Log.e("dsgsdgsdgsg", "Sub");
                                                dropCount++;
                                                Log.e("dropCount", String.valueOf(dropCount));
                                                map.setDroppingName(dropingstr.getString("dropping_idName"));
                                                map.setRoutePk(dropingstr.getString("route_id"));
                                                map.setDroppingPk(dropingstr.getString("dropping_id"));
                                                map.setSelf_copy(dropingstr.getString("self_copies"));
                                                map.setSub_agent_name(dropingstr.getString("sub_agent_name"));
                                                map.setContact(dropingstr.getString("contact_no"));
                                                map.setNoofcopy(dropingstr.getString("sub_agent_copies"));
                                                map.setRouteName(dropingstr.getString("route_idName"));
                                                map.setLocation(dropingstr.getString("agent_location"));
                                                map.setEditionName(dropingstr.getString("edition_idName"));
                                                map.setEditionPk(dropingstr.getString("edition_id"));
                                                globals.droppoint_rec_datalist.add(map);
                                            }

                                            SubAgentModal subAgentModal = new SubAgentModal();
                                            subAgentModal.setCopy(dropingstr.getString("edition_copies"));
                                            subAgentModal.setName(dropingstr.getString("edition_idName"));
                                            subAgentModal.setPk(dropingstr.getString("edition_id"));
                                            globals.editiondata_subAgent_rec_datalist.add(subAgentModal);
                                        }

                                        if (dropingstr.getString("sub_agent_copies").equalsIgnoreCase("1")) {  /*Self*/

                                            selfCount++;
                                            strSelfType = dropingstr.getString("sub_agent_copies");
                                            circulationReciptBinding.mapCopy.setText(dropingstr.getString("self_copies"));
                                            circulationReciptBinding.mapRoute.setText(dropingstr.getString("route_idName"));
                                            circulationReciptBinding.droopingPoint.setText(dropingstr.getString("dropping_idName"));
                                            circulationReciptBinding.selfDrop.setText(dropingstr.getString("sub_agent_name"));
                                            circulationReciptBinding.contactNumber.setText(dropingstr.getString("contact_no"));
                                            circulationReciptBinding.tvEdition.setText(dropingstr.getString("edition_idName"));


                                            dp_val = dropingstr.getString("dropping_id");
                                            route_spin_val = dropingstr.getString("route_id");
                                            editionId = dropingstr.getString("edition_id");

                                            SelfEditionModals selfEditionModals = new SelfEditionModals();
                                            selfEditionModals.setCopy(dropingstr.getString("edition_copies"));
                                            selfEditionModals.setName(dropingstr.getString("edition_idName"));
                                            selfEditionModals.setPk(dropingstr.getString("edition_id"));
                                            globals.editiondata_self_rec_datalist.add(selfEditionModals);


                                        } else {
                                            /*dropCount++;

                                            Log.e("dropCount", String.valueOf(dropCount));
                                            map.setDroppingName(dropingstr.getString("dropping_idName"));
                                            map.setRoutePk(dropingstr.getString("route_id"));
                                            map.setDroppingPk(dropingstr.getString("dropping_id"));
                                            map.setSelf_copy(dropingstr.getString("self_copies"));
                                            map.setSub_agent_name(dropingstr.getString("sub_agent_name"));
                                            map.setContact(dropingstr.getString("contact_no"));
                                            map.setNoofcopy(dropingstr.getString("sub_agent_copies"));
                                            map.setRouteName(dropingstr.getString("route_idName"));
                                            map.setLocation(dropingstr.getString("agent_location"));
                                            map.setEditionName(dropingstr.getString("edition_idName"));
                                            map.setEditionPk(dropingstr.getString("edition_id"));
                                            g.droppoint_rec_datalist.add(map);*/

                                        }


                                    }

                                    globals.editiondata_subAgent_rec_datalist.remove(0);
                                    Log.e("sdgsdgsgs", globals.droppoint_rec_datalist.size() + "");

                                    for (int k = 0; k < CirReqEdition_setArray.length(); k++) {
                                        JSONObject editionArray = CirReqEdition_setArray.getJSONObject(k);

                                        EditionCRModel obj1 = new EditionCRModel();
                                        obj1.setCopy(editionArray.getString("edition_copies"));
                                        obj1.setName(editionArray.getString("edition_idName"));
                                        obj1.setPk(editionArray.getString("edition_id"));
                                        obj1.setStatus("Update");
                                        globals.editiondata_rec_datalist.add(obj1);

                                        DropDownModel dropDownModel = new DropDownModel();
                                        dropDownModel.setId(editionArray.getString("edition_id"));
                                        dropDownModel.setDescription(editionArray.getString("edition_idName"));
                                        globals.SelectedEditionMasterArray.add(dropDownModel);
                                        editionCopy += Integer.parseInt(editionArray.getString("edition_copies"));


                                    }
                                    adapter_edi = new Edition_CR_Adapter(Circulation_JJForm.this, globals.editiondata_rec_datalist, dropdownMenuAdapterClass);
                                    circulationReciptBinding.editionRec.setAdapter(adapter_edi);
                                    adapter_edi.notifyDataSetChanged();

                                    if (strSelfType.equals("1")) { /*Self*/
                                        globals.editiondata_self_rec_datalist.remove(0);
                                        adapter_edi_2 = new SelfEditionAdapter(Circulation_JJForm.this, globals.editiondata_self_rec_datalist, dropdownMenuAdapterClass);
                                        circulationReciptBinding.dropPointSelfRec.setAdapter(adapter_edi_2);
                                        adapter_edi_2.notifyDataSetChanged();
                                    }

                                    Log.e("wetwerwew", "Sub"+" "+globals.editiondata_subAgent_rec_datalist.size() + "");
                                    Log.e("wetwerwew", "Self"+" "+globals.editiondata_self_rec_datalist.size() + "");



                                    if (dropCount > 0 && selfCount > 0) {
                                        routetype = "Both";
                                    } else if (dropCount == 0 && selfCount > 0) {
                                        routetype = "Self";
                                    } else if (selfCount == 0 && dropCount > 0) {
                                        routetype = "Sub Agent";

                                    }

                                    circulationReciptBinding.typeOfDrop.setText(routetype);
                                    if (routetype.equalsIgnoreCase("Self")) {
                                        bothlist = new ArrayList<>();
                                        for (int t = 0; t < globals.editiondata_rec_datalist.size(); t++) {
                                            BothEditionModal obj1 = new BothEditionModal();
                                            obj1.setEdition_id(globals.editiondata_rec_datalist.get(t).getPk());
                                            obj1.setEdition_copies(globals.editiondata_rec_datalist.get(t).getCopy());
                                            bothlist.add(obj1);
                                        }
                                        circulationReciptBinding.selfType.setVisibility(View.VISIBLE);
                                        circulationReciptBinding.addroute.setVisibility(View.GONE);
                                        circulationReciptBinding.dropPointCountRec.setVisibility(View.GONE);
                                        circulationReciptBinding.dropPointSelfRec.setVisibility(View.VISIBLE);
                                    } else if (routetype.equalsIgnoreCase("Sub Agent")) {

                                        bothlist = new ArrayList<>();
                                        for (int f = 0; f < globals.editiondata_rec_datalist.size(); f++) {
                                            BothEditionModal obj1 = new BothEditionModal();
                                            obj1.setEdition_id(globals.editiondata_rec_datalist.get(f).getPk());
                                            obj1.setEdition_copies(globals.editiondata_rec_datalist.get(f).getCopy());
                                            bothlist.add(obj1);
                                        }

                                        circulationReciptBinding.addroute.setVisibility(View.VISIBLE);
                                        circulationReciptBinding.selfType.setVisibility(View.GONE);
                                        circulationReciptBinding.dropPointCountRec.setVisibility(View.VISIBLE);
                                    } else if (routetype.equalsIgnoreCase("Both")) {

                                        bothlist = new ArrayList<>();
                                        for (int h = 0; h < globals.editiondata_rec_datalist.size(); h++) {
                                            BothEditionModal obj1 = new BothEditionModal();
                                            obj1.setEdition_id(globals.editiondata_rec_datalist.get(h).getPk());
                                            obj1.setEdition_copies(globals.editiondata_rec_datalist.get(h).getCopy());
                                            bothlist.add(obj1);
                                        }
                                        circulationReciptBinding.addroute.setVisibility(View.VISIBLE);
                                        circulationReciptBinding.selfType.setVisibility(View.VISIBLE);
                                        circulationReciptBinding.dropPointCountRec.setVisibility(View.VISIBLE);
                                        circulationReciptBinding.dropPointSelfRec.setVisibility(View.VISIBLE);

                                    }


                                    dropAdapter = new Drop_Point_Adapter(Circulation_JJForm.this, globals.droppoint_rec_datalist, routeMasterArray, globals.editiondata_rec_datalist);
                                    circulationReciptBinding.dropPointCountRec.setAdapter(dropAdapter);
                                    dropAdapter.notifyDataSetChanged();

                                    JSONArray CirReqCheque_set = new JSONArray(obj.getString("CirReqCheque_set"));

                                    if (obj.getString("asd_mode").equalsIgnoreCase("DD")) {

                                        circulationReciptBinding.dd.setChecked(true);

                                    } else if (obj.getString("asd_mode").equalsIgnoreCase("Cheque")) {
                                        circulationReciptBinding.cheque.setChecked(true);
                                    } else if (obj.getString("asd_mode").equalsIgnoreCase("NEFT/RTGS")) {
                                        circulationReciptBinding.neftRtgs.setChecked(true);
                                    }

                                    if (CirReqCheque_set.length() > 0) {
                                        globals.cheque_rec_datalist.clear();
                                        post_dated_yesno = "1";


                                        circulationReciptBinding.layoutViewASD.setVisibility(View.VISIBLE);
                                        circulationReciptBinding.postCheckLayout.setVisibility(View.VISIBLE);
                                        circulationReciptBinding.postDatedYes.setChecked(true);
                                        circulationReciptBinding.asdCovered.setText(obj.getString("asd_collected"));


                                        for (int k = 0; k < CirReqCheque_set.length(); k++) {
                                            JSONObject amtArray = CirReqCheque_set.getJSONObject(k);
                                            CirReqChequeSetModel obj1 = new CirReqChequeSetModel();

                                            // Log.e("ediPk"+i,g.editiondata_rec_datalist.get(i).get("ediPk").toString());


                                            obj1.setCheque_amt(amtArray.getString("cheque_amt"));
                                            obj1.setCheque_date(amtArray.getString("cheque_date").replace("T00:00:00", ""));

                                            globals.cheque_rec_datalist.add(obj1);

                                        }
                                        adapter_cheque = new Cheque_CR_Adapter(Circulation_JJForm.this, globals.cheque_rec_datalist);
                                        circulationReciptBinding.chequeRec.setAdapter(adapter_cheque);
                                        adapter_cheque.notifyDataSetChanged();

                                    } else {
                                        //  Toast.makeText(Circulation_JJForm.this,"No",Toast.LENGTH_LONG).show();


                                        post_dated_yesno = "0";

                                        circulationReciptBinding.layoutViewASD.setVisibility(View.GONE);
                                        circulationReciptBinding.postDatedNo.setChecked(true);
                                        circulationReciptBinding.postCheckLayout.setVisibility(View.VISIBLE);

                                    }


                                } catch (Exception e) {

                                    alertMessage(e.getMessage());
                                }
                            }
                        }
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void addCirForm(AgencyRequestCIRReqSetModel abc) {

        enableLoadingBar(true);

        Call<String> call;
        if (formstatus == 1) {
            call = SamriddhiApplication.getmInstance().getApiService().updateCirRequest(cirreq_id, abc);
        } else {
            call = SamriddhiApplication.getmInstance().getApiService().addCirRequest(abc);
        }

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;


                    Log.e("dataShow", response.toString());


                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        //  Toast.makeText(Circulation_JJForm.this, jsonObject.toString(), Toast.LENGTH_SHORT).show();

                        // String message = jsonObject.getString("message");


                        Toast.makeText(Circulation_JJForm.this, "Your  JJ Form Successfully Saved ", Toast.LENGTH_SHORT).show();

                        finish();


                        //  enableLoadingBar(false);
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    enableLoadingBar(false);
                } else {
                    try {
                        Toast.makeText(Circulation_JJForm.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();
                        enableLoadingBar(false);
                    } catch (IOException e) {
                        Toast.makeText(Circulation_JJForm.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();

                        enableLoadingBar(false);
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(Circulation_JJForm.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });

    }

    private void alertMessage(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })

                .show();
    }


    @Override
    public void onOptionClick(DropDownModel dataList) {
        if (dropSelectType.equalsIgnoreCase("exe")) {
            Util.hideDropDown();
            circulationReciptBinding.centerExeName.setText(dataList.getDescription());
            executive_name = dataList.getDescription();
            executive_id = dataList.getId();
        }


        if (dropSelectType.equalsIgnoreCase("edition")) {
            Util.hideDropDown();
            circulationReciptBinding.tvEdition.setText(dataList.getDescription());
            editionName = dataList.getDescription();
            editionId = dataList.getId();
        }

        if (dropSelectType.equalsIgnoreCase("route")) {
            Util.hideDropDown();
            circulationReciptBinding.mapRoute.setText(dataList.getDescription());
            route_spin_val = dataList.getId();
            getDropPoint(dataList.getId());
        }
        if (dropSelectType.equalsIgnoreCase("uoh")) {
            Util.hideDropDown();
            circulationReciptBinding.uohCr.setText(dataList.getDescription());
            uoh_name = dataList.getDescription();
            uoh_id = dataList.getId();
        }
        if (dropSelectType.equalsIgnoreCase("soh")) {
            Util.hideDropDown();
            circulationReciptBinding.sohCr.setText(dataList.getDescription());
            soh_name = dataList.getDescription();
            soh_id = dataList.getId();
        }
        if (dropSelectType.equalsIgnoreCase("dp")) {
            Util.hideDropDown();
            circulationReciptBinding.droopingPoint.setText(dataList.getDescription());
            dp_val = dataList.getId();
        }

        if (dropSelectType.equalsIgnoreCase("routetype")) {


            // Toast.makeText(this,"demo "+dataList.getDescription(),Toast.LENGTH_LONG).show();
            Util.hideDropDown();
            if (dataList.getDescription().equalsIgnoreCase("Self")) {

                bothlist = new ArrayList<>();
                for (int i = 0; i < globals.editiondata_rec_datalist.size(); i++) {
                    BothEditionModal obj1 = new BothEditionModal();
                    obj1.setEdition_id(globals.editiondata_rec_datalist.get(i).getPk());
                    obj1.setEdition_copies(globals.editiondata_rec_datalist.get(i).getCopy());
                    bothlist.add(obj1);
                }
                droppingPoint();

              /*  g.droppoint_rec_datalist.clear();
                dropAdapter = new Drop_Point_Adapter(Circulation_JJForm.this, g.droppoint_rec_datalist, routeMasterArray, g.editiondata_rec_datalist);
                circulationReciptBinding.dropPointCountRec.setAdapter(dropAdapter);
                dropAdapter.notifyDataSetChanged();*/

                circulationReciptBinding.selfType.setVisibility(View.VISIBLE);
                circulationReciptBinding.addroute.setVisibility(View.GONE);
                circulationReciptBinding.dropPointCountRec.setVisibility(View.GONE);
            } else if (dataList.getDescription().equalsIgnoreCase("Sub Agent")) {
                bothlist = new ArrayList<>();
                for (int i = 0; i < globals.editiondata_rec_datalist.size(); i++) {
                    BothEditionModal obj1 = new BothEditionModal();
                    obj1.setEdition_id(globals.editiondata_rec_datalist.get(i).getPk());
                    obj1.setEdition_copies(globals.editiondata_rec_datalist.get(i).getCopy());
                    bothlist.add(obj1);
                }

                droppingPoint();
                circulationReciptBinding.addroute.setVisibility(View.VISIBLE);
                circulationReciptBinding.selfType.setVisibility(View.GONE);
                circulationReciptBinding.dropPointCountRec.setVisibility(View.VISIBLE);
            } else if (dataList.getDescription().equalsIgnoreCase("Both")) {

                bothlist = new ArrayList<>();
                for (int i = 0; i < globals.editiondata_rec_datalist.size(); i++) {
                    BothEditionModal obj1 = new BothEditionModal();
                    obj1.setEdition_id(globals.editiondata_rec_datalist.get(i).getPk());
                    obj1.setEdition_copies(globals.editiondata_rec_datalist.get(i).getCopy());
                    bothlist.add(obj1);
                }


                droppingPoint();
                circulationReciptBinding.addroute.setVisibility(View.VISIBLE);
                circulationReciptBinding.selfType.setVisibility(View.VISIBLE);
                circulationReciptBinding.dropPointCountRec.setVisibility(View.VISIBLE);

            }

            routetype = dataList.getDescription();
            circulationReciptBinding.typeOfDrop.setText(dataList.getDescription());
        }

    }


    private void droppingPoint() {


        globals.droppoint_rec_datalist.clear();

        for (int i = 0; i < 1; i++) {
            //  EditionCRModel edtmap = new EditionCRModel();
            //  ArrayList<EditionCRModel> edtData=new ArrayList<>();


            CirReqDroppingSetModelAdpter map = new CirReqDroppingSetModelAdpter();
            map.setDroppingName("");
            map.setRoutePk("");
            map.setDroppingPk("");
            map.setSelf_copy("");
            map.setContact("");
            map.setNoofcopy("");
            map.setRouteName("");
            map.setEditionPk("");
            map.setEditionName("");


            globals.droppoint_rec_datalist.add(map);
        }

        circulationReciptBinding.dropPointCountRec.setHasFixedSize(true);
        circulationReciptBinding.dropPointCountRec.addItemDecoration(new DividerItemDecoration(getApplicationContext(),
                DividerItemDecoration.VERTICAL));
        circulationReciptBinding.dropPointCountRec.setNestedScrollingEnabled(true);
        circulationReciptBinding.dropPointCountRec.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        dropAdapter = new Drop_Point_Adapter(Circulation_JJForm.this, globals.droppoint_rec_datalist, routeMasterArray, globals.editiondata_rec_datalist);
        circulationReciptBinding.dropPointCountRec.setAdapter(dropAdapter);
        dropAdapter.notifyDataSetChanged();
    }

    private void getCenterExecutiveName() {
        enableLoadingBar(true);
        Map<String, String> data = new HashMap<>();
        data.put("state_id", "1");
        data.put("unit_id", "2");
        data.put("access_type", "EX");
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getTeamMaster(strStateId,strUnitID,"EX");
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                enableLoadingBar(false);
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONArray jsonObject = new JSONArray(response.body());


                        //     String status = jsonObject.getString("status");
                        if (jsonObject.length() > 0) {

                            centerExecutiveMasterArray.clear();

                            for (int i = 0; i < jsonObject.length(); i++) {


                                JSONObject objStr = jsonObject.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setDescription(objStr.getString("team_name"));
                                dropDownModel.setId(objStr.getString("team_id"));
                                centerExecutiveMasterArray.add(dropDownModel);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        enableLoadingBar(false);
                        Toast.makeText(Circulation_JJForm.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        enableLoadingBar(false);
                        Toast.makeText(Circulation_JJForm.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(Circulation_JJForm.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }

    private void getUOHData() {
        enableLoadingBar(true);
        Map<String, String> data = new HashMap<>();
        data.put("state_id", "1");
        data.put("unit_id", "2");
        data.put("access_type", "Ex");
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getTeamMaster(strStateId,strUnitID,"UH");   // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                enableLoadingBar(false);
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONArray jsonObject = new JSONArray(response.body());


                        //     String status = jsonObject.getString("status");
                        if (jsonObject.length() > 0) {

                            uohMasterArray.clear();

                            for (int i = 0; i < jsonObject.length(); i++) {


                                JSONObject objStr = jsonObject.getJSONObject(i);


                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setDescription(objStr.getString("team_name"));
                                dropDownModel.setId(objStr.getString("team_id"));
                                uohMasterArray.add(dropDownModel);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(Circulation_JJForm.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(Circulation_JJForm.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(Circulation_JJForm.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }


    private void getSOHData() {
        enableLoadingBar(true);
        Map<String, String> data = new HashMap<>();
        data.put("state_id", "1");
        data.put("unit_id", "2");
        data.put("access_type", "SH");
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getSHTeamMaster(strStateId,"SH");// Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    enableLoadingBar(false);
                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONArray jsonObject = new JSONArray(response.body());


                        //     String status = jsonObject.getString("status");
                        if (jsonObject.length() > 0) {

                            sohMasterArray.clear();

                            for (int i = 0; i < jsonObject.length(); i++) {


                                JSONObject objStr = jsonObject.getJSONObject(i);


                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setDescription(objStr.getString("team_name"));
                                dropDownModel.setId(objStr.getString("team_id"));
                                sohMasterArray.add(dropDownModel);
                            }
                        }

                    } catch (JSONException e) {
                        enableLoadingBar(false);
                        e.printStackTrace();
                    }

                } else {

                    try {
                        Toast.makeText(Circulation_JJForm.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(Circulation_JJForm.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(Circulation_JJForm.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void getDropPoint(String droppoint) {
        // Calling JSON
        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getDroppingPoint(PreferenceManager.getAgentId(context), droppoint);
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                enableLoadingBar(false);
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            dpMasterArray.clear();

                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("dropping_name"));
                                dropDownModel.setId(objStr.getString("pk"));
                                dpMasterArray.add(dropDownModel);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(Circulation_JJForm.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(Circulation_JJForm.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(Circulation_JJForm.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }


    public void getASD() {
        //g.unit_main;
        //g.mainSaleGroup;
        Log.e("getASD", PreferenceManager.getAgentId(context) + ":" + globals.unit_main + ":" + globals.mainSaleGroup + ":" + globals.mainSaleOffice + ":" + globals.mainSaleOfficeCopy + ":" + globals.jjSaleOffice + ":" + globals.jjSaleOfficeCopy);
        enableLoadingBar(true);
       Call<String> call = SamriddhiApplication.getmInstance().getApiService().getASDDetails(PreferenceManager.getAgentId(context), globals.unit_main, globals.mainSaleGroup, "0", 0, globals.jjSaleOffice, Integer.parseInt(globals.jjSaleOfficeCopy));
       //Call<String> call = SamriddhiApplication.getmInstance().getApiService().getASDDetails(PreferenceManager.getAgentId(context), g.unit_main, g.mainSaleGroup, g.mainSaleOffice, Integer.parseInt(g.mainSaleOfficeCopy), "\''", 0);
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                enableLoadingBar(false);
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("asd", response.body());


                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        // JSONArray objData=jsonObject.getJSONArray("data");
                        //String status = jsonObject.getString("status");
//                        if(status.equalsIgnoreCase("Success")){
//
//
//                            for(int i=0;i<objData.length();i++){

                        Double h = 0.0, copyRate = 0.0;
                       // copyRate = Double.parseDouble(jsonObject.getString("main_asd"));
                        copyRate = Double.parseDouble(jsonObject.getString("jj_asd"));
                        //  JSONObject objStr=objData.getJSONObject(i);
                        if (jsonObject.getString("asdamt").equalsIgnoreCase("")) {
                            h = 0.0;
                        } else {
                            h = Double.parseDouble(jsonObject.getString("asdamt"));
                        }
                        Log.e("asdamt:-", jsonObject.getString("asdamt"));
                        //circulationReciptBinding.asdNormsCr.setText(String.valueOf(h));
                        circulationReciptBinding.asdNormsCr.setText(String.valueOf(Math.round(h)));
                        Log.e("etrgdfgsd", "JJ" + " " + Math.round(h));

                        asdTotal = h;
                        asdCopyRs = Util.roundTwoDecimals(copyRate);

                        if (strAsdColl.length() > 0) {
                            double getasdColl = Double.parseDouble(circulationReciptBinding.asdCollCr.getText().toString());

                            if (getasdColl < asdTotal) {
                                asdDate = 1;
                                circulationReciptBinding.postCheckLayout.setVisibility(View.VISIBLE);
                            } else {
                                asdDate = 2;
                                circulationReciptBinding.postCheckLayout.setVisibility(View.GONE);
                            }
                            double divideAsd = 0;
                            if (asdCopyRs > 0) {
                                divideAsd = getasdColl / asdCopyRs;
                            } else {
                                divideAsd = 0;
                            }

                            circulationReciptBinding.tvScalableCopy.setText(String.valueOf(Math.round(divideAsd)));

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(Circulation_JJForm.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(Circulation_JJForm.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(Circulation_JJForm.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }


    private void getEdition() {
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getEditionByUnitId(PreferenceManager.getAgentId(context), "JJ", globals.unitId);

        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            EditionMasterArray.clear();
                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("edition_name"));
                                EditionMasterArray.add(dropDownModel);

                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }


    public void getPageGroup() {


        String val = String.format("%.0f", globals.mainNoOfCopy);
        //Log.e("getPageGroup", PreferenceManager.getAgentId(context) + ":" + val + ":" + globals.unit_main);
        Log.e("getPageGroup", PreferenceManager.getAgentId(context) + ":" + val + ":" + globals.unit_main+ ":" +strJJSales+ ":" +strReason);

        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getASDPageGroup(PreferenceManager.getAgentId(context), val, globals.unit_main,strJJSales,strReason);
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                enableLoadingBar(false);
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("getPageGroup", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {


                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                h = Double.parseDouble(objStr.getJSONObject("fields").getString("comm_per").replaceAll("\"", ""));


                                // Log.e("h:-",String.valueOf(asdTotal));
                            }
                            Double newData = new Double(h);
                            perValue = newData.intValue();

                            circulationReciptBinding.commissionCr.setText(String.valueOf(perValue));


                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(Circulation_JJForm.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(Circulation_JJForm.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(Circulation_JJForm.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }


    public void popupWindow1() {


        View popupView = null;


        popupView = LayoutInflater.from(Circulation_JJForm.this).inflate(R.layout.popup_commision, null);


        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0);


        TextView submit_button = popupView.findViewById(R.id.save_sampling);
        ImageView close = popupView.findViewById(R.id.close_sampling);
        EditText proposed = popupView.findViewById(R.id.tv_proposed_per);
        TextView tv_slab_per = popupView.findViewById(R.id.tv_slab_per);
        int commissionValue = 5;


        tv_slab_per.setText(String.valueOf(perValue));
        int getterValue = Integer.parseInt(tv_slab_per.getText().toString()) + 5;
        int lessValue = Integer.parseInt(tv_slab_per.getText().toString()) - 5;
        proposed.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    int pv = Integer.parseInt(proposed.getText().toString());
                    if (pv > getterValue) {
                        perStatus = true;
                        Toast.makeText(Circulation_JJForm.this, "Please enter correct percentage", Toast.LENGTH_SHORT).show();

                    } else if (pv < lessValue) {
                        perStatus = true;
                        Toast.makeText(Circulation_JJForm.this, "Please enter correct percentage", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                popupWindow.dismiss();


            }
        });

        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (proposed.getText().toString().equals("")) {
                    Toast.makeText(Circulation_JJForm.this, "Please enter proposed commission", Toast.LENGTH_SHORT).show();
                } else {
                    int acCommision = Integer.parseInt(tv_slab_per.getText().toString());
                    int prCommision = Integer.parseInt(proposed.getText().toString());

                    if (prCommision > acCommision) {
                        AlertUtility.showFormAlert(Circulation_JJForm.this, "Given commission is more then Allowed Commission, Special permission is required", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                popupWindow.dismiss();

                                circulationReciptBinding.commissionCr.setText(proposed.getText().toString());
                            }
                        });
                    } else {
                        popupWindow.dismiss();
                        circulationReciptBinding.commissionCr.setText(proposed.getText().toString());
                    }


                }


            }
        });
    }

    public void getRoute() {
        // Calling JSON
        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getRouteByUnitId(PreferenceManager.getAgentId(context), globals.unitId);
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                enableLoadingBar(false);
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            routeMasterArray.clear();

                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("route_name"));
                                dropDownModel.setId(objStr.getString("pk"));
                                routeMasterArray.add(dropDownModel);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(Circulation_JJForm.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(Circulation_JJForm.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(Circulation_JJForm.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    public void getSelfEditionList(ArrayList<SelfEditionModals> EditionMasterArray) {

        globals.SelectedEditionMasterArray.clear();
        for (int i = 0; i < EditionMasterArray.size(); i++) {

            DropDownModel dropDownModel = new DropDownModel();
            dropDownModel.setDescription(EditionMasterArray.get(i).getName());
            dropDownModel.setId(EditionMasterArray.get(i).getPk());
            globals.SelectedEditionMasterArray.add(dropDownModel);
        }


    }

    @Override
    public void getEditionList(ArrayList<EditionCRModel> EditionMasterArray) {

        globals.SelectedEditionMasterArray.clear();
        for (int i = 0; i < EditionMasterArray.size(); i++) {

            DropDownModel dropDownModel = new DropDownModel();
            dropDownModel.setDescription(EditionMasterArray.get(i).getName());
            dropDownModel.setId(EditionMasterArray.get(i).getPk());

            globals.SelectedEditionMasterArray.add(dropDownModel);


        }


    }

    public void getASDCovered(ASDDaysModel objDays) {

        Gson gson = new Gson();
        String data = gson.toJson(objDays);
        Log.e("objDays->", data);
        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getASDDays(PreferenceManager.getAgentId(context), objDays);
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                enableLoadingBar(false);
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        if (jsonObject.getString("status").equalsIgnoreCase("Success")) {
                            circulationReciptBinding.tvCoveredCopy.setText(jsonObject.getString("ASD_DAYS"));

                            circulationReciptBinding.asdCovered.setText(jsonObject.getString("ASD_DAYS"));

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(Circulation_JJForm.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(Circulation_JJForm.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(Circulation_JJForm.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }


}
