package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NavSubagentSet extends BaseModel {
    @SerializedName("results")
    @Expose
    private List<NavSubagentList> results = null;

    public List<NavSubagentList> getResults() {
        return results;
    }

    public void setResults(List<NavSubagentList> results) {
        this.results = results;
    }
}
