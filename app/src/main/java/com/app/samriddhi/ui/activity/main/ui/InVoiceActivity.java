package com.app.samriddhi.ui.activity.main.ui;

import android.Manifest;
import android.accounts.AccountManager;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.MediaStore;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.BuildConfig;
import com.app.samriddhi.R;
import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.request.InvoiceRequest;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.databinding.ActivityInvoiceBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AgencyCloseStausActivity;
import com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule.ViewRequestData;
import com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule.newRquestActivity;
import com.app.samriddhi.ui.fragment.DashboardFragment;
import com.app.samriddhi.ui.model.AgencyRequestReasonSetModel;
import com.app.samriddhi.ui.model.ProposedAgencyModel;
import com.app.samriddhi.ui.presenter.InVoicePresenter;
import com.app.samriddhi.ui.view.IView;
import com.app.samriddhi.util.AlertUtility;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.CustomShowCaseView;
import com.app.samriddhi.util.NetworkUtil;
import com.app.samriddhi.util.SystemUtility;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.github.barteksc.pdfviewer.listener.OnErrorListener;
import com.github.barteksc.pdfviewer.listener.OnLoadCompleteListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.GmailScopes;
import com.itextpdf.text.pdf.parser.Line;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import okhttp3.Credentials;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InVoiceActivity extends BaseActivity implements OnLoadCompleteListener, OnErrorListener, IView {
    private static final String TAG = InVoiceActivity.class.getSimpleName();
    ActivityInvoiceBinding mBinding;
    String Monat = "", Gjahr = "", PayAmt = "", billNumber = "", webUrl = "", billPeriod, billAmount = "", copies = "";
    BroadcastReceiver downloadReceiver;
    ShowcaseView showcaseView;
    int showCount = 0;
    InVoicePresenter mPresenter;
    File createdFile;
    InVoiceActivity mContext;
    PopupWindow popupWindow;
    String messageStatus = "", getOtp = "";
    String strReason = "";
    ImageView closeAlert;
    TextView tvMsg;
    MaterialButton sendFabButton, btnNo, btnYes, btnSubmit;
    EditText edtToAddress, edtSubject, edtMessage, edtAttachmentData, remark;
    LinearLayout btnMessageLayout, btnSubmitLayout, mailUi;
    EditText ed1, ed2, ed3, ed4;
    Toolbar toolbar;
    String strConfirmedContact = "";
    private static final int MAX_BUFFER_SIZE = 1024 * 1024;

    GoogleAccountCredential mCredential;
    ProgressDialog mProgress;
    private static final String PREF_ACCOUNT_NAME = "accountName";
    private static final String[] SCOPES = {
            GmailScopes.GMAIL_LABELS,
            GmailScopes.GMAIL_COMPOSE,
            GmailScopes.GMAIL_INSERT,
            GmailScopes.GMAIL_MODIFY,
            GmailScopes.GMAIL_READONLY,
            GmailScopes.MAIL_GOOGLE_COM
    };
    private InternetDetector internetDetector;
    private final int SELECT_PHOTO = 1;
    public String fileName = "";
    String strUserEmail = "";

    String strData = "Virend";
    String strOTPId = "", strOTP = "", strBpCode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_invoice);
        mContext = this;

        // Initializing Internet Checker
        internetDetector = new InternetDetector(getApplicationContext());

        // Initialize credentials and service object.
        mCredential = GoogleAccountCredential.usingOAuth2(
                getApplicationContext(), Arrays.asList(SCOPES))
                .setBackOff(new ExponentialBackOff());

        // Initializing Progress Dialog
        mProgress = new ProgressDialog(InVoiceActivity.this);
        mProgress.setMessage("Mail sending to company billconfirmation@dbcorp.in");
        initializeView();

        //SendSms();
    }


    /**
     * initialize and bind the views
     */
    private void initializeView() {


        TelephonyManager tMgr = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        //strConfirmedContact = tMgr.getLine1Number();
        strConfirmedContact = "";
       // Toast.makeText(InVoiceActivity.this, strConfirmedContact, Toast.LENGTH_SHORT).show();

        mBinding.toolbarInvoice.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mBinding.toolbarInvoice.rlNotification.setOnClickListener(v -> {
            startActivityAnimation(this, NotificationActivity.class, false);
        });
        mBinding.toolbarInvoice.txtTittle.setText(getResources().getString(R.string.str_invoice));
        if (getIntent() != null) {
            billNumber = getIntent().getStringExtra("bill_number");
            billPeriod = getIntent().getStringExtra("bill_period");
            Log.e("sfsdfsd", billPeriod);
            billAmount = getIntent().getStringExtra("bill_amount");
            PayAmt = getIntent().getStringExtra("PayAmt");
            Monat = getIntent().getStringExtra("Monat");
            Gjahr = getIntent().getStringExtra("Gjahr");
            copies = getIntent().getStringExtra("copies");
        }
        mPresenter = new InVoicePresenter();
        mPresenter.setView(this);

        webUrl = (Constant.INVOICE_PDF_URL + billNumber);
        downloadFromServer();
    }

    /**
     * download the invoice of a bill from server
     */
    private void downloadFromServer() {
        Log.e("dfgdfgdfgdfg", "Ok");
        Log.e("dfgdfgdfgdfg", "Data" + " " + Constant.INVOICE_PDF_URL + billNumber);
        enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().downloadFileByUrl(Constant.INVOICE_PDF_URL + billNumber).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    Log.e("dfgdfgdfgdfg", "If" + " " + response.toString());
                    //new AsyncFileDownload().execute(response.body());

                    final DownloadTask downloadTask = new DownloadTask(InVoiceActivity.this);
                    downloadTask.execute(Constant.INVOICE_PDF_URL + billNumber);

                } else {

                    Log.e("dfgdfgdfgdfg", "Else" + " " + response.toString());
                    enableLoadingBar(false);
                    onError(response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                enableLoadingBar(false);
                Log.e("dfgdfgdfgdfg", "OnFailure" + " " + t.getMessage());
            }
        });

    }

    /**
     * save the downloaded invoice in storage
     */

    private class DownloadTask extends AsyncTask<String, Integer, String> {

        private final Context context;


        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL(sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = connection.getContentLength();

                // download the file
                input = connection.getInputStream();

                ContextWrapper cw = new ContextWrapper(context);
                File directory = cw.getExternalFilesDir(SystemUtility.getStorage(Constant.DIR_INVOICE));
                createdFile = new File(directory, billNumber + ".pdf");

                //createdFile = new File(SystemUtility.getStorage(Constant.DIR_INVOICE), billNumber + ".pdf");
                Log.e("sdsdgsdgsg", "Path" + " " + createdFile.toString());
                output = new FileOutputStream(createdFile);

                byte[] data = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                Log.e("wefgiwegriwe", e.getMessage());
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            // Log.e("sdfsdfsds",s);
            // Log.e("sdfsdfsds","DONE");
            onLoadPDF();
        }
    }


    public void onLoadPDF() {

        enableLoadingBar(false);
        mBinding.pdfView.fromFile(createdFile).onError(InVoiceActivity.this).onLoad(InVoiceActivity.this).load();
        Log.e("sdfsdfsds", "DONE");
        //SendSms();
    }

    public void saveToDisk(ResponseBody body) {
        enableLoadingBar(false);
        mBinding.pdfView.fromFile(createdFile).onError(InVoiceActivity.this).onLoad(InVoiceActivity.this).load();

        createdFile = new File(SystemUtility.getStorage(Constant.DIR_INVOICE), billNumber + ".pdf");
        Log.e("sdgfsdgsgsd", createdFile.toString());

        try {
            FileOutputStream fos = new FileOutputStream(createdFile);
            fos.write(body.bytes());
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.e("erterteet", "File saved successfully!");

        if (createdFile.exists()) {
            createdFile.delete();
        }
        InputStream is = null;
        OutputStream os = null;
        try {

            Log.d(TAG, "File Size=" + body.contentLength());
            is = body.byteStream();
            os = new FileOutputStream(createdFile);

            byte[] data = new byte[4096];
            int count;
            int progress = 0;
            while ((count = is.read(data)) != -1) {
                os.write(data, 0, count);
                progress += count;
                Log.d(TAG, "Progress: " + progress + "/" + body.contentLength() + " >>>> " + (float) progress / body.contentLength());
            }
            os.flush();
            enableLoadingBar(false);
            mBinding.pdfView.fromFile(createdFile).onError(InVoiceActivity.this).onLoad(InVoiceActivity.this).load();
            Log.d(TAG, "File saved successfully!");


        } catch (IOException e) {
            Log.e("sdgfsdgsgsd", "Catch" + " " + e.getMessage());
            e.printStackTrace();
            Log.d(TAG, "Failed to save the file!");
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (IOException e) {
                    Log.e("sdgfsdgsgsd", "Finally" + " " + e.getMessage());
                    e.printStackTrace();
                }
            }
            if (os != null) {
                try {
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    public String createFile(String fileName) {
        String targetFolder = getApplicationContext().getFilesDir().getAbsolutePath() + "/mypdf/";
        File file = new File(targetFolder);
        file.mkdirs();
        // then create files under new folder like
        File myFile = new File(targetFolder, fileName);
        return myFile.getAbsolutePath();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void loadComplete(int nbPages) {
        if (downloadReceiver != null) {
            unregisterReceiver(downloadReceiver);
        }
        // showCoachMarksOnScreen();
    }

    @Override
    public void onError(Throwable t) {
        if (downloadReceiver != null) {
            unregisterReceiver(downloadReceiver);
        }
        // showCoachMarksOnScreen();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.rl_invoice:
                loadProgressBar(null, "Please wait..", false);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        dismissProgressBar();
                        AlertUtility.showAlertFileOpen(InVoiceActivity.this, String.format(getResources().getString(R.string.str_invoice_download_msg), " InternalStorage/Samriddhi/" + Constant.DIR_INVOICE + " "), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                openPdf();
                            }
                        });
                    }
                }, 3000);
                break;
            case R.id.txt_confirm_invoice:
                if (NetworkUtil.isOnline(this))
                    getBillConfirmCheck(billNumber);
                else {
                    AlertUtility.showAlertDialog(this, getResources().getString(R.string.error_interent_message));
                }
                break;

            default:
                break;

        }
    }

    public void getBillConfirmCheck(String billNumber) {
        Log.e("ewrwwerwrewe", billNumber);
        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getBillStatus(billNumber);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;
                    Log.e("ewrwwerwrewe", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("failed")) {
                            showAlert("failed");
                        } else {
                            JSONArray replyArray = jsonObject.getJSONArray("reply: ");
                            JSONObject reply = replyArray.getJSONObject(replyArray.length() - 1);

                            if (reply.getString("Confirm_Status").equalsIgnoreCase("1")) {

                                AlertUtility.showAlertDialog(InVoiceActivity.this, "You have already conformed this bill");
                            }

                            if (reply.getString("Confirm_Status").equalsIgnoreCase("2")) {

                                AlertUtility.showAlertDialog(InVoiceActivity.this, "You have already confirmed this bill after discussion.");
                            } else if (reply.getString("Confirm_Status").equalsIgnoreCase("0") || reply.getString("Confirm_Status").equalsIgnoreCase("3")) {




                                new AlertDialog.Builder(InVoiceActivity.this)
                                        .setTitle("Samriddhi")
                                        .setMessage("Your bill confirmation still pending")
                                        .setCancelable(true)
                                        .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                showAlert("pending");
                                            }
                                        }).show();


                               /* AlertUtility.showAlert(InVoiceActivity.this, "Your bill confirmation still pending", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        showAlert("pending");
                                    }
                                });*/
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                        AlertUtility.showAlertDialog(InVoiceActivity.this, "error message" + e.getMessage());

                    }
                    enableLoadingBar(false);
                } else {
                    try {

                        AlertUtility.showAlertDialog(InVoiceActivity.this, "error message" + response.errorBody().string());

                    } catch (IOException e) {
                        AlertUtility.showAlertDialog(InVoiceActivity.this, "error message" + response.errorBody().toString());
                        e.printStackTrace();
                    }
                    enableLoadingBar(false);
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                //  enableLoadingBar(false);
                enableLoadingBar(false);
                AlertUtility.showAlertDialog(InVoiceActivity.this, "Server Issue");
            }
        });
    }

    public void showAlert(String status) {
        strReason = "";

        View popupView = null;
        Log.e("sfsafasasfa", status);
        LinearLayout linearRadio;

        popupView = LayoutInflater.from(InVoiceActivity.this).inflate(R.layout.bill_confirmation_alert, null);


        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0);


        btnSubmitLayout = popupView.findViewById(R.id.btnSubmitLayout);
        btnMessageLayout = popupView.findViewById(R.id.btnMessageLayout);
        mailUi = popupView.findViewById(R.id.mailUi);
        btnNo = popupView.findViewById(R.id.btnNo);
        btnYes = popupView.findViewById(R.id.btnYes);
        MaterialButton btnSubmitWithNo = popupView.findViewById(R.id.btnSubmitWithNo);
        btnSubmit = popupView.findViewById(R.id.btnSubmit);
        tvMsg = popupView.findViewById(R.id.tvMsg);
        remark = popupView.findViewById(R.id.remark);
        closeAlert = popupView.findViewById(R.id.closeImg);
        sendFabButton = popupView.findViewById(R.id.send_mail);

        edtToAddress = popupView.findViewById(R.id.to_address);
        edtSubject = popupView.findViewById(R.id.subject);
        edtMessage = popupView.findViewById(R.id.body);
        edtAttachmentData = popupView.findViewById(R.id.attachmentData);
        edtToAddress.setVisibility(View.GONE);
        edtToAddress.setText("billconfirmation@dbcorp.in");
        edtSubject.setText("Bill conformation from " + PreferenceManager.getAgentId(InVoiceActivity.this) + "–" + PreferenceManager.getVkorg(InVoiceActivity.this));
        edtMessage.setText("“I hereby confirm the copies Billed, Payment Credits and Credit Note in the bill no " + billNumber + " dated " + Monat + "-" + Gjahr + " for Rs " + billAmount + " being in reconciliation with our records.”");

        linearRadio = popupView.findViewById(R.id.linearRadio);
        RadioButton radioCredit = popupView.findViewById(R.id.radioCredit);
        RadioButton radioDifference = popupView.findViewById(R.id.radioDifference);
        RadioButton radioPayment = popupView.findViewById(R.id.radioPayment);
        RadioButton radioDeposit = popupView.findViewById(R.id.radioDeposit);
        RadioButton radioOther = popupView.findViewById(R.id.radioOther);
        radioCredit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b == true) {
                    strReason = radioCredit.getText().toString();
                    radioDifference.setChecked(false);
                    radioPayment.setChecked(false);
                    radioDeposit.setChecked(false);
                    radioOther.setChecked(false);
                }
            }
        });

        radioDifference.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b == true) {
                    strReason = radioDifference.getText().toString();
                    radioCredit.setChecked(false);
                    radioPayment.setChecked(false);
                    radioDeposit.setChecked(false);
                    radioOther.setChecked(false);
                }
            }
        });

        radioPayment.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b == true) {
                    strReason = radioPayment.getText().toString();
                    radioCredit.setChecked(false);
                    radioDifference.setChecked(false);
                    radioDeposit.setChecked(false);
                    radioOther.setChecked(false);
                }
            }
        });

        radioDeposit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b == true) {
                    strReason = radioDeposit.getText().toString();
                    radioCredit.setChecked(false);
                    radioDifference.setChecked(false);
                    radioPayment.setChecked(false);
                    radioOther.setChecked(false);
                }
            }
        });

        radioOther.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b == true) {
                    strReason = radioOther.getText().toString();
                    radioCredit.setChecked(false);
                    radioDifference.setChecked(false);
                    radioPayment.setChecked(false);
                    radioDeposit.setChecked(false);
                }
            }
        });

        closeAlert.setOnClickListener(v -> {
            popupWindow.dismiss();
        });

        if (status.equals("pending")) {
            btnNo.setVisibility(View.GONE);
        }

        btnSubmit.setOnClickListener(v -> {
            if (remark.getText().toString().length() == 0) {
                alertMessage("Please enter the remark");
                return;
            }
//            if(remark.getText().toString().length()>15){
//                alertMessage("Enter remark character length should not be greater then 15");
//                return;
//            }
            popupWindow.dismiss();
            messageStatus = "No";
            edtMessage.setText("Agent Remark:- " + remark.getText().toString() + "\n“I hereby submit my concern as per remarks in the bill no " + billNumber + " dated " + Monat + "-" + Gjahr + " for Rs " + billAmount + "”");
            // getResultsFromApi(v);
            //SendSms();


            mProgress.show();
            if (messageStatus.equalsIgnoreCase("No")) {
                DashboardFragment.strPopupDismiss="1";
                mPresenter.confirmInvoice(new InvoiceRequest(PreferenceManager.getAgentId(mContext), SystemUtility.getDeviceId(InVoiceActivity.this),
                        billPeriod, billNumber, remark.getText().toString(), PreferenceManager.getPrefAgentName(mContext), billAmount, copies, 0,strConfirmedContact));

            } else if (messageStatus.equalsIgnoreCase("Yes")) {
                if (strReason.equals("")) {
                    DashboardFragment.strPopupDismiss="2";
                    mPresenter.confirmInvoice(new InvoiceRequest(PreferenceManager.getAgentId(mContext), SystemUtility.getDeviceId(InVoiceActivity.this),
                            billPeriod, billNumber, "Yes", PreferenceManager.getPrefAgentName(mContext), billAmount, copies, 1,strConfirmedContact));
                } else {
                    DashboardFragment.strPopupDismiss="2";
                    mPresenter.confirmInvoice(new InvoiceRequest(PreferenceManager.getAgentId(mContext), SystemUtility.getDeviceId(InVoiceActivity.this),
                            billPeriod, billNumber, strReason, PreferenceManager.getPrefAgentName(mContext), billAmount, copies, 2,strConfirmedContact));  /* 2 confirm bill after discussion*/
                }
            }
            popupWindow.dismiss();
            mProgress.dismiss();

        });


        btnSubmitWithNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (strReason.equals("")) {

                    Toast.makeText(mContext, "Select any reason.", Toast.LENGTH_SHORT).show();
                } else {

                    popupWindow.dismiss();
                    DashboardFragment.strPopupDismiss="1";
                    mPresenter.confirmInvoice(new InvoiceRequest(PreferenceManager.getAgentId(mContext), SystemUtility.getDeviceId(InVoiceActivity.this),
                            billPeriod, billNumber, strReason, PreferenceManager.getPrefAgentName(mContext), billAmount, copies, 0,strConfirmedContact));

                }

            }
        });
        btnNo.setOnClickListener(v -> {

            strReason = "";
            tvMsg.setText("Choose the option for not confirming the bill ");
            btnSubmitWithNo.setVisibility(View.VISIBLE);
            btnNo.setVisibility(View.GONE);
            btnYes.setVisibility(View.GONE);

            if (strReason.equals("")) {

                radioCredit.setText("Credit Note");
                radioDifference.setText("Copies Difference");
                radioPayment.setText("Payment Credits");
                radioDeposit.setText("Security Deposits");
                radioOther.setText("Any Other");

                linearRadio.setVisibility(View.VISIBLE);
            }

            // alertMessage("Please connect with the Circulation Manager and inform the details of");

            //billconfirmation@dbcorp.in

//            edtToAddress.setText("billconfirmation@dbcorp.in");
//            edtSubject.setText("Concern from "+PreferenceManager.getAgentId(InVoiceActivity.this)+"–"+PreferenceManager.getVkorg(InVoiceActivity.this));
//            edtMessage.setText("“I hereby submit my concern as per remarks in the bill no "+billNumber+" dated "+Monat +"-"+Gjahr+" for Rs "+billAmount+"”");
//
//            sendFabButton.setVisibility(View.GONE);
//            mailUi.setVisibility(View.VISIBLE);
//            btnMessageLayout.setVisibility(View.GONE);
//            btnSubmitLayout.setVisibility(View.VISIBLE);
//            tvMsg.setVisibility(View.GONE);
//            remark.setVisibility(View.VISIBLE);

        });

        btnYes.setOnClickListener(v -> {

            if (status.equals("pending")) {
                linearRadio.setVisibility(View.VISIBLE);
                radioCredit.setText("Credit not issue resolved");
                radioDifference.setText("Copies verified");
                radioPayment.setText("Payment issue resolved");
                radioDeposit.setText("Security deposit okay");
                radioOther.setText("Any Other");

                if (strReason.equals("")) {
                    Toast.makeText(mContext, "Select Reason", Toast.LENGTH_SHORT).show();
                } else {

                    linearRadio.setVisibility(View.GONE);
                    sendFabButton.setVisibility(View.VISIBLE);
                    btnMessageLayout.setVisibility(View.GONE);
                    btnSubmitLayout.setVisibility(View.GONE);
                    mailUi.setVisibility(View.VISIBLE);
                    tvMsg.setVisibility(View.GONE);
                    remark.setVisibility(View.GONE);
                }
            } else {

                sendFabButton.setVisibility(View.VISIBLE);
                btnMessageLayout.setVisibility(View.GONE);
                btnSubmitLayout.setVisibility(View.GONE);
                mailUi.setVisibility(View.VISIBLE);
                tvMsg.setVisibility(View.GONE);
                remark.setVisibility(View.GONE);
                linearRadio.setVisibility(View.GONE);
            }
           /* sendFabButton.setVisibility(View.VISIBLE);
            btnMessageLayout.setVisibility(View.GONE);
            btnSubmitLayout.setVisibility(View.GONE);
            mailUi.setVisibility(View.VISIBLE);
            tvMsg.setVisibility(View.GONE);
            remark.setVisibility(View.GONE);*/

        });

        sendFabButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                messageStatus = "Yes";
                // getResultsFromApi(view);
                // popupWindow.dismiss();

                // strUserEmail = mCredential.getSelectedAccountName();
                Log.e("sdfsdfsfsdfs", "DeviceID" + " " + SystemUtility.getDeviceId(InVoiceActivity.this));
                popupWindow.dismiss();
                popupOtpWindow();

               /* mProgress.show();
                if (messageStatus.equalsIgnoreCase("No")) {
                    mPresenter.confirmInvoice(new InvoiceRequest(PreferenceManager.getAgentId(mContext), SystemUtility.getDeviceId(InVoiceActivity.this),
                            billPeriod, billNumber, remark.getText().toString(), PreferenceManager.getPrefAgentName(mContext), billAmount, copies, 0));

                } else if (messageStatus.equalsIgnoreCase("Yes")) {
                    if (strReason.equals("")) {

                        mPresenter.confirmInvoice(new InvoiceRequest(PreferenceManager.getAgentId(mContext), SystemUtility.getDeviceId(InVoiceActivity.this),
                                billPeriod, billNumber, "Yes", PreferenceManager.getPrefAgentName(mContext), billAmount, copies, 1));
                    } else {

                        mPresenter.confirmInvoice(new InvoiceRequest(PreferenceManager.getAgentId(mContext), SystemUtility.getDeviceId(InVoiceActivity.this),
                                billPeriod, billNumber, strReason, PreferenceManager.getPrefAgentName(mContext), billAmount, copies, 2));  *//* 2 confirm bill after discussion*//*
                    }
                }
                popupWindow.dismiss();
                mProgress.dismiss();*/

            }

        });

    }

    private void alertMessage(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(this, R.style.AppTheme_AlertDialog)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })

                .show();
    }


    private void showMessage(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).show();
    }


    public void SendSms() {

        ProgressDialog progressDialog = new ProgressDialog(InVoiceActivity.this);
        progressDialog.setMessage("Please wait..");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();
        JSONObject jsonObjectMain;
        try {
            jsonObjectMain = new JSONObject();
            JSONObject jObj1 = new JSONObject();
            jObj1.put("email", "do-not-reply@dbcorp.in");
            jObj1.put("name", "Billing Confirmation");

            jsonObjectMain.putOpt("from", jObj1);
            jsonObjectMain.put("subject", "Bill conformation from " + PreferenceManager.getAgentId(InVoiceActivity.this) + "–" + PreferenceManager.getVkorg(InVoiceActivity.this));

            JSONArray jsonArrayContent = new JSONArray();
            JSONObject objectContent = new JSONObject();
            objectContent.put("type", "html");
            // objectContent.put("value", "Agent Remark:-" + remark.getText().toString() + "n“I hereby submit my concern as per remarks in the bill no" + billNumber + " dated " + Monat + "-" + Gjahr + " for Rs" + billAmount + "");
            objectContent.put("value", "Agent Remark:-" + "remark.getText().toString()" + "n“I hereby submit my concern as per remarks in the bill no" + billNumber + " dated " + Monat + "-" + Gjahr + " for Rs" + billAmount + "");
            jsonArrayContent.put(objectContent);

            jsonObjectMain.putOpt("content", jsonArrayContent);
            JSONArray jsonArrayPersonalize = new JSONArray();
            JSONObject jobjPersonalize = new JSONObject();

            JSONArray jsonArrayTo = new JSONArray();
            JSONObject jsonObjectTo = new JSONObject();
            //jsonObjectTo.put("email", "billconfirmation@dbcorp.in");
            jsonObjectTo.put("email", "virendmeena1122@gmail.com");
            jsonObjectTo.put("name", "SamriddhiApp");
            jsonArrayTo.put(jsonObjectTo);
            jobjPersonalize.put("to", jsonArrayTo);
            jsonArrayPersonalize.put(jobjPersonalize);
            jsonObjectMain.put("personalizations", jsonArrayPersonalize);
            Log.e("sgwtwyuwyew", jsonObjectMain.toString());

            AndroidNetworking.post("https://api.pepipost.com/v5/mail/send")
                    .addHeaders("api_key", "7c7c590ac0703ede953d497a2bb85e31")
                    .addHeaders("content-type", "application/json")
                    .addJSONObjectBody(jsonObjectMain)
                    .setTag("Sending SMS")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {

                            try {
                                Log.e("eywtwetwt", response.toString());

                                /*if (response.getString("status").equals("success")) {

                                    mPresenter.confirmInvoice(new InvoiceRequest(PreferenceManager.getAgentId(mContext), SystemUtility.getDeviceId(InVoiceActivity.this),
                                            billPeriod, billNumber, remark.getText().toString(), PreferenceManager.getPrefAgentName(mContext), billAmount, copies, 0));
                                    progressDialog.dismiss();
                                } else {

                                    mPresenter.confirmInvoice(new InvoiceRequest(PreferenceManager.getAgentId(mContext), SystemUtility.getDeviceId(InVoiceActivity.this),
                                            billPeriod, billNumber, "Yes", PreferenceManager.getPrefAgentName(mContext), billAmount, copies, 1));

                                    progressDialog.dismiss();
                                }*/

                            } catch (Exception ex) {
                                progressDialog.dismiss();
                                Log.e("dsgsgsgs", ex.getMessage());
                            }


                        }

                        @Override
                        public void onError(ANError anError) {
                            progressDialog.dismiss();

                        }
                    });


        } catch (Exception e) {
            Log.e("sgwtwyuwyew", e.getMessage());
        }
    }

    private void getResultsFromApi(View view) {


        if (!isGooglePlayServicesAvailable()) {
            acquireGooglePlayServices();
        } else if (mCredential.getSelectedAccountName() == null) {
            chooseAccount(view);
        } else if (!internetDetector.checkMobileInternetConn()) {
            showMessage(view, "No network connection available.");
        } else if (!Utils.isNotEmpty(edtToAddress)) {
            showMessage(view, "To address Required");
        } else if (!Utils.isNotEmpty(edtSubject)) {
            showMessage(view, "Subject Required");
        } else if (!Utils.isNotEmpty(edtMessage)) {
            showMessage(view, "Message Required");
        } else {
           // new MakeRequestTask(this, mCredential).execute();
            popupWindow.dismiss();
            popupOtpWindow();

//            strUserEmail=mCredential.getSelectedAccountName();
//            Log.e("sdfsdfsfsdfs",strUserEmail);
//
//
//            mProgress.show();
//            if (messageStatus.equalsIgnoreCase("No")) {
//                mPresenter.confirmInvoice(new InvoiceRequest(PreferenceManager.getAgentId(mContext), SystemUtility.getDeviceId(InVoiceActivity.this),
//                        billPeriod, billNumber, remark.getText().toString(), PreferenceManager.getPrefAgentName(mContext), billAmount, copies, 0,strUserEmail));
//
//            }
//            else if (messageStatus.equalsIgnoreCase("Yes")) {
//                if (strReason.equals("")) {
//
//                    mPresenter.confirmInvoice(new InvoiceRequest(PreferenceManager.getAgentId(mContext), SystemUtility.getDeviceId(InVoiceActivity.this),
//                            billPeriod, billNumber, "Yes", PreferenceManager.getPrefAgentName(mContext), billAmount, copies, 1,strUserEmail));
//                } else {
//
//                    mPresenter.confirmInvoice(new InvoiceRequest(PreferenceManager.getAgentId(mContext), SystemUtility.getDeviceId(InVoiceActivity.this),
//                            billPeriod, billNumber, strReason, PreferenceManager.getPrefAgentName(mContext), billAmount, copies, 2,strUserEmail));  *//* 2 confirm bill after discussion*//*
//                }
//            }
//            popupWindow.dismiss();
//            mProgress.dismiss();
        }
    }


    public void popupOtpWindow() {
        View popupView = null;
        //popupWindow.dismiss();

        popupView = LayoutInflater.from(InVoiceActivity.this).inflate(R.layout.bill_confirmation_otp, null);


        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0);


        TextView sendOtp = popupView.findViewById(R.id.sendOtp);
        Spannable wordOne = new SpannableString("Check OTP on this ");
        wordOne.setSpan(new ForegroundColorSpan(Color.BLACK), 0, wordOne.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        sendOtp.setText(wordOne);

        Spannable wordTwo = new SpannableString(PreferenceManager.getUSerPhone(InVoiceActivity.this));
        wordTwo.setSpan(new ForegroundColorSpan(Color.RED), 0, wordTwo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        sendOtp.append(wordTwo);

        Spannable wordThree = new SpannableString(" number");
        wordThree.setSpan(new ForegroundColorSpan(Color.BLACK), 0, wordThree.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        sendOtp.append(wordThree);


        LinearLayout linearResend = popupView.findViewById(R.id.linearResend);
        TextView sunmit_button = popupView.findViewById(R.id.otp_submit);
        ed1 = popupView.findViewById(R.id.et1);
        ed1.requestFocus();
        ed1.setFocusable(true);


        ed2 = popupView.findViewById(R.id.et2);
        ed3 = popupView.findViewById(R.id.et3);
        ed4 = popupView.findViewById(R.id.et4);
        ImageView img = popupView.findViewById(R.id.close);

        Log.e("dfjkjgdkgd", PreferenceManager.getUSerPhone(InVoiceActivity.this));

        generateOTP(PreferenceManager.getUSerPhone(InVoiceActivity.this));

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

        ed1.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {


            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

                if (s != null) {
                    ed2.requestFocus();
                }
            }
        });

        ed2.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {


            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

                if (s != null) {
                    ed3.requestFocus();
                }
            }
        });

        ed3.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {


            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

                if (s != null) {
                    ed4.requestFocus();
                }
            }
        });


        linearResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generateOTP(PreferenceManager.getUSerPhone(InVoiceActivity.this));
            }
        });
        sunmit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ed1.getText().toString().length() == 0) {
                    alertMessage("Please enter otp number ");
                    return;
                }
                if (ed2.getText().toString().length() == 0) {
                    alertMessage("Please enter otp number ");
                    return;
                }
                if (ed3.getText().toString().length() == 0) {
                    alertMessage("Please enter otp number ");
                    return;
                }
                if (ed4.getText().toString().length() == 0) {
                    alertMessage("Please enter otp number ");
                    return;
                }
                String enterOtp = ed1.getText().toString() + ed2.getText().toString() + ed3.getText().toString() + ed4.getText().toString();

                if (enterOtp.equalsIgnoreCase(strOTP)) {
                    VerifyOTP(enterOtp, PreferenceManager.getUSerPhone(InVoiceActivity.this));

                } else {
                    alertMessage("Invalid OTP");
                    return;
                }
            }
        });
    }


    public void generateOTP(String uSerPhone) {
        Log.e("sfsdfsdfsfsfs" + " Mobile", uSerPhone);
        enableLoadingBar(true);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("bill_number", billNumber);
            jsonObject.put("bp_code", PreferenceManager.getAgentId(InVoiceActivity.this));
            jsonObject.put("bill_period", billPeriod);
            Log.e("swgdgsgsg", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(Constant.BASE_PORTAL_URL+"v2/send-bill-conf-otp")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .addJSONObjectBody(jsonObject)
                .setTag("Request")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            Log.e("swgdgsgsg", response.toString());

                            if (response.getString("message").equals("OTP Sent successfully.")) {
                                enableLoadingBar(false);
                                JSONArray jsonArray = new JSONArray(response.getString("data"));
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    strOTPId = jsonObject1.getString("otp_id");
                                    strOTP = jsonObject1.getString("otp");
                                    strBpCode = jsonObject1.getString("bp_code");
                                }
                            }
                            else {
                                enableLoadingBar(false);
                                Toast.makeText(InVoiceActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {
                            Log.e("swgdgsgsg", ex.getMessage());
                            enableLoadingBar(false);
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        enableLoadingBar(false);
                    }
                });


    }

    public void VerifyOTP(String enterOtp, String uSerPhone) {


        /*  {
    "success": false,
    "message": "OTP not matched.!"
}  */



        /*  {
    "success": true,
    "message": "OTP verified successfully."
}  */
        enableLoadingBar(true);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("otp_id", strOTPId);
            jsonObject.put("otp", strOTP);
            jsonObject.put("bp_code", PreferenceManager.getAgentId(InVoiceActivity.this));
            Log.e("swgdgsgsg", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(Constant.BASE_PORTAL_URL+"v2/verify-bill-conf-otp")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .addJSONObjectBody(jsonObject)
                .setTag("Request")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            Log.e("swgdgsgsg", response.toString());

                            if (response.getString("message").equals("OTP verified successfully.")) {


                                if (messageStatus.equalsIgnoreCase("No")) {
                                    DashboardFragment.strPopupDismiss="1";
                                    mPresenter.confirmInvoice(new InvoiceRequest(PreferenceManager.getAgentId(mContext), SystemUtility.getDeviceId(InVoiceActivity.this),
                                            billPeriod, billNumber, remark.getText().toString(), PreferenceManager.getPrefAgentName(mContext), billAmount, copies, 0,strConfirmedContact));

                                } else if (messageStatus.equalsIgnoreCase("Yes")) {
                                    if (strReason.equals("")) {
                                        DashboardFragment.strPopupDismiss="2";
                                        mPresenter.confirmInvoice(new InvoiceRequest(PreferenceManager.getAgentId(mContext), SystemUtility.getDeviceId(InVoiceActivity.this),
                                                billPeriod, billNumber, "Yes", PreferenceManager.getPrefAgentName(mContext), billAmount, copies, 1,strConfirmedContact));
                                    } else {
                                        DashboardFragment.strPopupDismiss="2";
                                        mPresenter.confirmInvoice(new InvoiceRequest(PreferenceManager.getAgentId(mContext), SystemUtility.getDeviceId(InVoiceActivity.this),
                                                billPeriod, billNumber, strReason, PreferenceManager.getPrefAgentName(mContext), billAmount, copies, 2,strConfirmedContact));  /* 2 confirm bill after discussion*/
                                    }
                                }


                                enableLoadingBar(false);
                                popupWindow.dismiss();
                            }
                            else {
                                enableLoadingBar(false);
                                Toast.makeText(InVoiceActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {
                            Log.e("swgdgsgsg", ex.getMessage());
                            enableLoadingBar(false);
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        enableLoadingBar(false);
                    }
                });

    }


    // Method for Checking Google Play Service is Available
    private boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int connectionStatusCode = apiAvailability.isGooglePlayServicesAvailable(this);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    // Method to Show Info, If Google Play Service is Not Available.
    private void acquireGooglePlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int connectionStatusCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (apiAvailability.isUserResolvableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
        }
    }

    // Method for Google Play Services Error Info
    void showGooglePlayServicesAvailabilityErrorDialog(final int connectionStatusCode) {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        Dialog dialog = apiAvailability.getErrorDialog(
                InVoiceActivity.this,
                connectionStatusCode,
                Utils.REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }

    // Storing Mail ID using Shared Preferences
    private void chooseAccount(View view) {
        if (Utils.checkPermission(getApplicationContext(), Manifest.permission.GET_ACCOUNTS)) {
            String accountName = getPreferences(Context.MODE_PRIVATE).getString(PREF_ACCOUNT_NAME, null);
            if (accountName != null) {
                mCredential.setSelectedAccountName(accountName);
                getResultsFromApi(view);
            } else {
                // Start a dialog from which the user can choose an account
                startActivityForResult(mCredential.newChooseAccountIntent(), Utils.REQUEST_ACCOUNT_PICKER);
            }
        } else {
            ActivityCompat.requestPermissions(InVoiceActivity.this,
                    new String[]{Manifest.permission.GET_ACCOUNTS}, Utils.REQUEST_PERMISSION_GET_ACCOUNTS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Utils.REQUEST_PERMISSION_GET_ACCOUNTS:
                chooseAccount(sendFabButton);
                break;
            case SELECT_PHOTO:
                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
                photoPickerIntent.setType("image/*");
                startActivityForResult(photoPickerIntent, SELECT_PHOTO);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("dfgfdgdgd", " 881");
        switch (requestCode) {
            case Utils.REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK) {
                    showMessage(sendFabButton, "This app requires Google Play Services. Please install " +
                            "Google Play Services on your device and relaunch this app.");
                } else {
                    getResultsFromApi(sendFabButton);
                }
                break;
            case Utils.REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null && data.getExtras() != null) {
                    String accountName = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        SharedPreferences settings = getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(PREF_ACCOUNT_NAME, accountName);
                        editor.apply();
                        mCredential.setSelectedAccountName(accountName);
                        getResultsFromApi(sendFabButton);
                    }

                }

                break;
            case Utils.REQUEST_AUTHORIZATION:
                if (resultCode == RESULT_OK) {
                    getResultsFromApi(sendFabButton);
                }
                break;
            case SELECT_PHOTO:
                if (resultCode == RESULT_OK) {
                    final Uri imageUri = data.getData();
                    fileName = getPathFromURI(imageUri);
                    edtAttachmentData.setText(fileName);
                }
        }
    }

    public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getContentResolver().query(contentUri, proj, "", null, "");
        assert cursor != null;
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }

    // Async Task for sending Mail using GMail OAuth
    private class MakeRequestTask extends AsyncTask<Void, Void, String> {

        private com.google.api.services.gmail.Gmail mService = null;
        private Exception mLastError = null;
        private final View view = sendFabButton;
        private final InVoiceActivity activity;

        MakeRequestTask(InVoiceActivity activity, GoogleAccountCredential credential) {
            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new com.google.api.services.gmail.Gmail.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName(getResources().getString(R.string.app_name))
                    .build();
            this.activity = activity;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                return getDataFromApi();
            } catch (Exception e) {
                mLastError = e;
                cancel(true);
                return null;
            }
        }


        private String getDataFromApi() throws IOException {
            // getting Values for to Address, from Address, Subject and Body
            String user = "me";
            String to = Utils.getString(edtToAddress);
            String from = mCredential.getSelectedAccountName();
            String subject = Utils.getString(edtSubject);
            String body = Utils.getString(edtMessage);
            MimeMessage mimeMessage;
            String response = "";
            try {
                mimeMessage = createEmail(to, from, subject, body);
                response = sendMessage(mService, user, mimeMessage);
            } catch (MessagingException e) {
                e.printStackTrace();
            }
            return response;
        }

        // Method to send email
        private String sendMessage(Gmail service,
                                   String userId,
                                   MimeMessage email)
                throws MessagingException, IOException {
            com.google.api.services.gmail.model.Message message = createMessageWithEmail(email);
            // GMail's official method to send email with oauth2.0
            message = service.users().messages().send(userId, message).execute();

            System.out.println("Message id: " + message.getId());
            System.out.println(message.toPrettyString());
            return message.getId();
        }

        // Method to create email Params
        private MimeMessage createEmail(String to,
                                        String from,
                                        String subject,
                                        String bodyText) throws MessagingException {
            Properties props = new Properties();
            Session session = Session.getDefaultInstance(props, null);

            MimeMessage email = new MimeMessage(session);
            InternetAddress tAddress = new InternetAddress(to);
            InternetAddress fAddress = new InternetAddress(from);

            email.setFrom(fAddress);
            email.addRecipient(javax.mail.Message.RecipientType.TO, tAddress);
            email.setSubject(subject);

            // Create Multipart object and add MimeBodyPart objects to this object
            Multipart multipart = new MimeMultipart();

            // Changed for adding attachment and text
            // email.setText(bodyText);

            BodyPart textBody = new MimeBodyPart();
            textBody.setText(bodyText);
            multipart.addBodyPart(textBody);

            if (!(activity.fileName.equals(""))) {
                // Create new MimeBodyPart object and set DataHandler object to this object
                MimeBodyPart attachmentBody = new MimeBodyPart();
                String filename = activity.fileName; // change accordingly
                DataSource source = new FileDataSource(filename);
                attachmentBody.setDataHandler(new DataHandler(source));
                attachmentBody.setFileName(filename);
                multipart.addBodyPart(attachmentBody);
            }

            //Set the multipart object to the message object
            email.setContent(multipart);
            return email;
        }

        private com.google.api.services.gmail.model.Message createMessageWithEmail(MimeMessage email)
                throws MessagingException, IOException {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            email.writeTo(bytes);
            String encodedEmail = Base64.encodeBase64URLSafeString(bytes.toByteArray());
            com.google.api.services.gmail.model.Message message = new com.google.api.services.gmail.model.Message();
            message.setRaw(encodedEmail);
            return message;
        }

        @Override
        protected void onPreExecute() {
            mProgress.show();
        }

        @Override
        protected void onPostExecute(String output) {
            mProgress.hide();
            if (output == null || output.length() == 0) {
                showMessage(view, "No results returned.");
            } else {
                showMessage(view, output);

                if (messageStatus.equalsIgnoreCase("No")) {
                    DashboardFragment.strPopupDismiss="1";
                    mPresenter.confirmInvoice(new InvoiceRequest(PreferenceManager.getAgentId(mContext), SystemUtility.getDeviceId(InVoiceActivity.this),
                            billPeriod, billNumber, remark.getText().toString(), PreferenceManager.getPrefAgentName(mContext), billAmount, copies, 0,strConfirmedContact));

                } else if (messageStatus.equalsIgnoreCase("Yes")) {

                    if (strReason.equals("")) {
                        DashboardFragment.strPopupDismiss="2";
                        mPresenter.confirmInvoice(new InvoiceRequest(PreferenceManager.getAgentId(mContext), SystemUtility.getDeviceId(InVoiceActivity.this),
                                billPeriod, billNumber, "Yes", PreferenceManager.getPrefAgentName(mContext), billAmount, copies, 1,strConfirmedContact));
                    } else {
                        DashboardFragment.strPopupDismiss="2";
                        mPresenter.confirmInvoice(new InvoiceRequest(PreferenceManager.getAgentId(mContext), SystemUtility.getDeviceId(InVoiceActivity.this),
                                billPeriod, billNumber, strReason, PreferenceManager.getPrefAgentName(mContext), billAmount, copies, 2,strConfirmedContact));  /* 2 confirm bill after discussion*/
                    }
                }

                popupWindow.dismiss();
            }
        }

        @Override
        protected void onCancelled() {
            Log.e("sdfsfsfsfsdf", mLastError.getCause().getMessage());
            mProgress.hide();
            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            Utils.REQUEST_AUTHORIZATION);
                } else {
                    showMessage(view, "The following error occurred:\n" + mLastError);
                    Log.e("Error", mLastError + "");
                }
            } else {
                showMessage(view, "Request Cancelled.");
            }
        }
    }

    protected void sendEmail() {
        Log.i("Send email", "");
        String[] TO = {"sachin.chourasiya@dbcorp.in"};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Bill conformation from " + PreferenceManager.getAgentId(InVoiceActivity.this));
        emailIntent.putExtra(Intent.EXTRA_TEXT, "“I hereby confirm the copies Billed, Payment Credits and Credit Note in the Bill No " + billNumber + " dated " + Monat + "-" + Gjahr + " for Rs " + billAmount + " being in reconciliation with our records.”");
        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();
            Log.i("finish", "");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(InVoiceActivity.this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * open the downloaded pdf in phone
     */

    private void openPdf() {

        //SendSms();
        Uri path = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".fileprovider", createdFile);
        Log.e("create pdf uri path==>", "" + path);
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(getApplicationContext(),
                    "There is no any PDF Viewer",
                    Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * shows the guidance of the screen
     */
    /*private void showCoachMarksOnScreen() {
        showCount = 0;
        showcaseView = new ShowcaseView.Builder(this)
                .setTarget(new ViewTarget(R.id.rl_invoice, this))
                .singleShot(PreferenceManager.PREF_INVOICE_SHOT)
                .setContentTitle(getResources().getString(R.string.str_download_invoice_coach_msg)).blockAllTouches()
                .setStyle(R.style.CustomCoachMarksTheme)
                .setShowcaseDrawer(new CustomShowCaseView(mBinding.rlInvoice, getResources()))
                .build();
        showcaseView.forceTextPosition(ShowcaseView.ABOVE_SHOWCASE);

    }*/

    /**
     * background task to save the data into disk storage
     */

    public class AsyncFileDownload extends AsyncTask<ResponseBody, Void, Void> {

        @Override
        protected Void doInBackground(ResponseBody... responseBodies) {
            saveToDisk(responseBodies[0]);
            return null;
        }
    }
}
