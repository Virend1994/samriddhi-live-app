package com.app.samriddhi.ui.activity.main.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.samriddhi.R;
import com.app.samriddhi.api.response.JsonObjectResponse;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.RecyclerViewArrayAdapter;
import com.app.samriddhi.databinding.ActivityUserTypeListBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.model.AgentResultModel;
import com.app.samriddhi.ui.model.BillOutAgentResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCDistWiseListModel;
import com.app.samriddhi.ui.model.BillOutCityUPCDistWiseResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCResultModel;
import com.app.samriddhi.ui.model.BillingOutstandingResultModel;
import com.app.samriddhi.ui.model.CashCreditResultModel;
import com.app.samriddhi.ui.model.CityResultModel;
import com.app.samriddhi.ui.model.SalesDistrictWiseListModel;
import com.app.samriddhi.ui.model.SalesDistrictWiseResultModel;
import com.app.samriddhi.ui.model.SalesOrgCityResultModel;
import com.app.samriddhi.ui.model.StateResultModel;
import com.app.samriddhi.ui.model.UsersResultModel;
import com.app.samriddhi.ui.presenter.UserTypeCopiesDetailPresenter;
import com.app.samriddhi.ui.view.IUserTypeCopiesView;
import com.app.samriddhi.util.Constant;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class BillOutCityUPCDistActivity extends BaseActivity implements IUserTypeCopiesView, RecyclerViewArrayAdapter.OnItemClickListener<BillOutCityUPCDistWiseListModel> {
    ActivityUserTypeListBinding mBinding;
    UserTypeCopiesDetailPresenter mPresenter;
    private static final String STATE_CODE="stateCode";
    private static final String STATE_NAME="stateName";
    private static final String UNIT_NAME="unitName";
    private static final String UNIT_CODE="unitCode";
    private static final String D_CHANNEL="DChannel";
    private static final String CITY_UPC="cityUPC";
    private static final String SALES_CODE="SalesCode";
    private static final String SALES_DIST="SalesDist";
    String screenName, stateCode="", stateName="",unitCode="",unitName="",dChannel="",cityUPC="";
    Calendar c = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_user_type_list);
        initViews();
    }

    /**
     * initialize and bind views
     */

    private void initViews() {
        mBinding.toolbarCalender.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        mBinding.toolbarCalender.rlNotification.setOnClickListener(v -> {
            startActivityAnimation(this, NotificationActivity.class, false);
        });


        if (getIntent() != null) {
            if (getIntent().hasExtra(Constant.SCREEN_NAME))
                screenName = getIntent().getStringExtra(Constant.SCREEN_NAME);
            if (getIntent().hasExtra(STATE_CODE))
                stateCode = getIntent().getStringExtra(STATE_CODE);
            if (getIntent().hasExtra(STATE_NAME))
                stateName = getIntent().getStringExtra(STATE_NAME);

            if (getIntent().hasExtra(UNIT_NAME))
                unitName = getIntent().getStringExtra(UNIT_NAME);
            if (getIntent().hasExtra(UNIT_CODE))
                unitCode = getIntent().getStringExtra(UNIT_CODE);

            if (getIntent().hasExtra(D_CHANNEL))
                dChannel = getIntent().getStringExtra(D_CHANNEL);

            if (getIntent().hasExtra(CITY_UPC))
                cityUPC = getIntent().getStringExtra(CITY_UPC);
        }
        mBinding.setScrenName(screenName);
        mBinding.tvHeaderOne.setText(R.string.str_outstanding);
        mBinding.tvHeaderTwo.setText(R.string.str_billing_tittle);
        mBinding.toolbarCalender.txtTittle.setText(R.string.billing_and_outstanding);
        mBinding.tvOnPage.setText("All States/"+stateName+"/"+unitName+"/"+cityUPC);
        if(stateCode.equals(""))
        {
            mBinding.tvOnPage.setText("All Cities/"+unitName+"/"+cityUPC);
        }
        String date = new SimpleDateFormat("dd MMM, yyyy", Locale.getDefault()).format(new Date());
        mBinding.tvTodayDate.setText(date);
        mPresenter = new UserTypeCopiesDetailPresenter();
        mPresenter.setView(this);
        mBinding.recycleList.setLayoutManager(new LinearLayoutManager(this));

        Log.e("sdsdsfsggd",dChannel);
        Log.e("sdsdsfsggd",cityUPC);

        mPresenter.getBillOutCityUPCDist(PreferenceManager.getAgentId(this),  stateCode, unitCode,dChannel,cityUPC.toLowerCase());
    }

    @Override
    public void onCitySuccess(CityResultModel mCityModel) {
        // will override if require
    }

    @Override
    public void onStateSuccess(StateResultModel mStateModel) {
        // will override if require
    }

    @Override
    public void onStateSuccess(BillingOutstandingResultModel mStateModel) {

    }

    @Override
    public void onAgentListSuccess(AgentResultModel mAgentResultModel) {
        // will override if require
    }

    @Override
    public void onBillOutAgentListSuccess(BillOutAgentResultModel mAgentResultModel) {

    }

    @Override
    public void onCreditCashSuccess(CashCreditResultModel mResultsData) {
        // will override if require
    }

    @Override
    public void onSuccess(JsonObjectResponse<UsersResultModel> body) {
        // will override if require
    }

    @Override
    public void onBillOUtCityUPCSuccess(JsonObjectResponse<BillOutCityUPCResultModel> body) {

    }

    @Override
    public void onSalesOrgCitySuccess(SalesOrgCityResultModel mData) {
        // will override if require
    }

    @Override
    public void onSalesDistrictCopies(SalesDistrictWiseResultModel mResultData) {
    }

    @Override
    public void onBillOutCityUPCDistSuccess(BillOutCityUPCDistWiseResultModel mResultData) {
        mBinding.recycleList.setAdapter(new RecyclerViewArrayAdapter(mResultData.getSalesOrgCityList(), this, screenName));

    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onItemClick(View view, BillOutCityUPCDistWiseListModel object) {
        if (screenName.equalsIgnoreCase(Constant.DashBoardBill)) {
            if(object.getSalesDistCode()!=null)
            startActivity(new Intent(this, BillOutAgentListActivity.class).putExtra(STATE_CODE, stateCode).putExtra(STATE_NAME, stateName).putExtra(UNIT_NAME,unitName).putExtra(UNIT_CODE,unitCode).putExtra(D_CHANNEL,dChannel).putExtra(SALES_DIST,object.getSalesDistName()).putExtra(SALES_CODE,object.getSalesDistCode()).putExtra(CITY_UPC,cityUPC).putExtra(Constant.SCREEN_NAME, screenName));

        }
        else if (screenName.equalsIgnoreCase(Constant.DashBoardOutstanding)) {
            if(object.getSalesDistCode()!=null)
                startActivity(new Intent(this, BillOutAgentListActivity.class).putExtra(STATE_CODE, stateCode).putExtra(STATE_NAME, stateName).putExtra(UNIT_NAME,unitName).putExtra(UNIT_CODE,unitCode).putExtra(D_CHANNEL,dChannel).putExtra(SALES_DIST,object.getSalesDistName()).putExtra(SALES_CODE,object.getSalesDistCode()).putExtra(CITY_UPC,cityUPC).putExtra(Constant.SCREEN_NAME, screenName));

        }
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
}
