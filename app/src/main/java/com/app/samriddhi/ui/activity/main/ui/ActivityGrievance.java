package com.app.samriddhi.ui.activity.main.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.RecyclerViewArrayAdapter;
import com.app.samriddhi.databinding.ActivityGrievanceBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.model.GrievanceCategoryModel;
import com.app.samriddhi.ui.model.GrievanceCreateIncidentModel;
import com.app.samriddhi.ui.model.GrievanceModel;
import com.app.samriddhi.ui.model.GrievanceOpenItemListModel;
import com.app.samriddhi.ui.model.IncidentCommentData;
import com.app.samriddhi.ui.model.IncidentDataModel;
import com.app.samriddhi.ui.model.IncidentListModel;
import com.app.samriddhi.ui.presenter.GrievancePresenter;
import com.app.samriddhi.ui.view.IGrievanceView;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.StringUtility;
import com.app.samriddhi.util.SystemUtility;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ActivityGrievance extends BaseActivity implements IGrievanceView, RecyclerViewArrayAdapter.OnItemClickListener, View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    ActivityGrievanceBinding mBinding;
    GrievancePresenter mPresenter;
    List<GrievanceModel> arrOptionsData = new ArrayList<>();
    List<GrievanceCategoryModel> arrCategoryStatus = new ArrayList<>();
    String strAgentName = "", strNewComplaintId = "", strCategoryId = "", strOpenSelectedItemId = "", strSubComplaintId = "", agentId = "", strComplaintCloseOrOpen = "";
    List<GrievanceOpenItemListModel> arrOpenItems = new ArrayList<>();
    List<IncidentListModel> arrSubIncidents = new ArrayList<>();
    List<IncidentCommentData> arrComments = new ArrayList<>();
    boolean isOpen = false, isSatisfied = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_grievance);
        initView();
    }
      /*      initialize  all views and objects */

    private void initView() {
        mPresenter = new GrievancePresenter();
        mPresenter.setView(this);
        if (getIntent() != null && getIntent().hasExtra("agentId")) {
            agentId = getIntent().getStringExtra("agentId");
            strAgentName = getIntent().getStringExtra("agentName");
        } else {
            agentId = PreferenceManager.getAgentId(this);
            strAgentName = "";
        }
        mBinding.toolbarGrievance.txtTittle.setText(getResources().getString(R.string.str_grievance) + (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("AG") ? "" : ((strAgentName.isEmpty() ? "" : " (" + strAgentName + " )"))));
        mPresenter.getIncidentCategory();
        mBinding.toolbarGrievance.imgBack.setOnClickListener(this);
        mBinding.givenceNew.btnSubmitComplaint.setOnClickListener(this);
        mBinding.layoutOpenItems.btnReply.setOnClickListener(this);
        mBinding.givenceNew.recycleGrievanceOptions.setLayoutManager(new LinearLayoutManager(this));
        mBinding.layoutOpenItems.recycleOpenComplaints.setLayoutManager(new LinearLayoutManager(this));
        mBinding.layoutOpenItems.recycleOpenComplaintsSubItem.setLayoutManager(new LinearLayoutManager(this));
        mBinding.layoutOpenItems.recycleComments.setLayoutManager(new LinearLayoutManager(this));
        mBinding.layoutCloseItems.recycleCloseComplaints.setLayoutManager(new LinearLayoutManager(this));
        mBinding.layoutCloseItems.recycleCloseComplaintsSubItem.setLayoutManager(new LinearLayoutManager(this));
        mBinding.layoutCloseItems.recycleComments.setLayoutManager(new LinearLayoutManager(this));
        mBinding.layoutCloseItems.radioGpCloseItems.setOnCheckedChangeListener(this);
    }


/*Server side error message*/
    @Override
    public void onError(String reason) {
        if (reason != null && reason.equalsIgnoreCase("Category Fetch Failed")) {
            mPresenter.getIncidentOptions();
        } else {
            super.onError(reason);
        }
    }

    /*Handle the Speech to text button click */
    public void getSpeechInput(View view) {
        Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, 10);
        } else {
            Toast.makeText(this, "Your Device Don't Support Speech Input", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 10) {
            if (resultCode == RESULT_OK && data != null) {
                ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                Log.e("speech text code", "=== " + result.get(0));
                mBinding.givenceNew.txtSpechToText.setText(result.get(0));
            }
        }
    }

/* Get new Grievance category options from server */

    @Override
    public void onCategoryGetSuccess(List<GrievanceModel> mGrievData) {
        if (mGrievData != null && mGrievData.size() > 0) {
            arrOptionsData.clear();
            for (GrievanceModel grievData : mGrievData) {
                arrOptionsData.add(grievData);
            }
            mBinding.givenceNew.recycleGrievanceOptions.setAdapter(new RecyclerViewArrayAdapter(arrOptionsData, this));
        }
    }

    /* Grievance Status options from server like New ,Open etc.. */

    @Override
    public void onGetIncidentStatus(List<GrievanceCategoryModel> mGrievData) {
        mPresenter.getIncidentOptions();
        for (GrievanceCategoryModel grievData : mGrievData) {
            if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("AG")) {
                arrCategoryStatus.add(grievData);
            } else {
                if (grievData.getFields().getIncidentStatusText().equalsIgnoreCase("New")) {
                    continue;
                } else {
                    arrCategoryStatus.add(grievData);
                }
            }
        }
        if (arrCategoryStatus != null && arrCategoryStatus.size() > 0) {
            for (GrievanceCategoryModel data : arrCategoryStatus) {
                if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("AG")) {
                    if (data.getFields().getIncidentStatusText().equalsIgnoreCase("New")) {
                        data.getFields().setSelected(true);
                        setVisibility(data.getFields().getIncidentStatusText());
                    }
                } else {
                    if (data.getFields().getIncidentStatusText().equalsIgnoreCase("Open")) {
                        data.getFields().setSelected(true);
                        strCategoryId = String.valueOf(data.getPk());
                        setVisibility(data.getFields().getIncidentStatusText());

                    }
                }
            }
            mBinding.recycleStatus.setLayoutManager(new GridLayoutManager(this, arrCategoryStatus.size()));
            mBinding.recycleStatus.setAdapter(new RecyclerViewArrayAdapter(arrCategoryStatus, this));
        }
    }


    /* get all open items from server*/
    @Override
    public void onOpenListItemSuccess(List<GrievanceOpenItemListModel> arrData, String compliantType) {
        if (arrData != null && arrData.size() > 0) {
            arrOpenItems.clear();
            for (GrievanceOpenItemListModel data : arrData)
                arrOpenItems.add(data);
            if (compliantType.equalsIgnoreCase(Constant.OPENCOMPLAINT))
                mBinding.layoutOpenItems.recycleOpenComplaints.setAdapter(new RecyclerViewArrayAdapter(arrOpenItems, this));
            else
                mBinding.layoutCloseItems.recycleCloseComplaints.setAdapter(new RecyclerViewArrayAdapter(arrOpenItems, this));
        }
    }

    /*Get Sub incidents of a particular Incident*/
    @Override
    public void onIncidentSubItemsList(IncidentDataModel body, String complaintType) {
        if (body.getIncidentlist() != null && body.getIncidentlist().size() > 0) {
            arrSubIncidents.clear();
            arrSubIncidents = body.getIncidentlist();
            if (arrSubIncidents.size() == 1) {
                mBinding.layoutOpenItems.txtComplaintSubType.setVisibility(View.GONE);
                mBinding.layoutCloseItems.txtComplaintSubType.setVisibility(View.GONE);
                setDataForOneComplaintChild(arrSubIncidents);
            } else {

                if (complaintType.equalsIgnoreCase(Constant.OPENCOMPLAINT)) {
                    mBinding.layoutOpenItems.txtComplaintSubType.setVisibility(View.VISIBLE);
                    mBinding.layoutOpenItems.recycleOpenComplaintsSubItem.setAdapter(new RecyclerViewArrayAdapter(arrSubIncidents, this::onItemClick));
                } else {
                    mBinding.layoutCloseItems.txtComplaintSubType.setVisibility(View.VISIBLE);
                    mBinding.layoutCloseItems.recycleCloseComplaintsSubItem.setAdapter(new RecyclerViewArrayAdapter(arrSubIncidents, this::onItemClick));
                }
            }
        }
    }

    /* After getting Incidents comments from server */
    @Override
    public void onIncidentCommentsSuccess(List<IncidentCommentData> list) {
        //Incidents comments list
        if (list != null && list.size() > 0) {
            arrComments = list;
            if (isOpen)
                mBinding.layoutOpenItems.recycleComments.setAdapter(new RecyclerViewArrayAdapter(arrComments));
            else
                mBinding.layoutCloseItems.recycleComments.setAdapter(new RecyclerViewArrayAdapter(arrComments));
        }
    }

    @Override
    public void onDataSubmitSuccess(String message, String task) {
        if (task.equalsIgnoreCase(Constant.OPENCOMPLAINT)) {
            if (strComplaintCloseOrOpen.equalsIgnoreCase(getResources().getString(R.string.str_open))) {
                strComplaintCloseOrOpen = "";
                mBinding.layoutOpenItems.edReply.setText(null);
                arrComments.clear();
                mPresenter.getIncidentComments(strSubComplaintId);
            } else {
                setDataAfterCloseIssue();
            }
        } else if (task.equalsIgnoreCase(Constant.NEWCOMPLAINT)) {
            handleNewItemsView();
        } else if (task.equalsIgnoreCase(Constant.CLOSECOMPLAINT)) {
            handleCloseItemsView();
        }
        onInfo(message);
    }

    private void setDataAfterCloseIssue() {
        handleOpenView();
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onItemClick(View view, Object object) {
        if (object instanceof GrievanceModel) {
            for (int i = 0; i < arrOptionsData.size(); i++) {
                if (((GrievanceModel) object).getFields().incidentCategoryText.equalsIgnoreCase(arrOptionsData.get(i).getFields().getIncidentCategoryText())) {
                    arrOptionsData.get(i).getFields().setSelected(true);
                    strNewComplaintId = String.valueOf(arrOptionsData.get(i).getPk());
                } else
                    arrOptionsData.get(i).getFields().setSelected(false);
            }
        } else if (object instanceof GrievanceCategoryModel) {
            for (int i = 0; i < arrCategoryStatus.size(); i++) {

                if (((GrievanceCategoryModel) object).getFields().getIncidentStatusText().equalsIgnoreCase(arrCategoryStatus.get(i).getFields().getIncidentStatusText())) {
                    arrCategoryStatus.get(i).getFields().setSelected(true);
                    strCategoryId = String.valueOf(arrCategoryStatus.get(i).getPk());
                }
                else {
                    arrCategoryStatus.get(i).getFields().setSelected(false);
                }
            }
            setVisibility(((GrievanceCategoryModel) object).getFields().getIncidentStatusText());
        } else if (object instanceof GrievanceOpenItemListModel) {
            for (int i = 0; i < arrOpenItems.size(); i++) {
                if (((GrievanceOpenItemListModel) object).getIncidentCategoryText().equalsIgnoreCase(arrOpenItems.get(i).getIncidentCategoryText())) {
                    arrOpenItems.get(i).setSelected(true);
                    strOpenSelectedItemId = String.valueOf(arrOpenItems.get(i).getIncidentCategoryCode());
                } else
                    arrOpenItems.get(i).setSelected(false);
            }
            setDataOnIncidentClick();
            mPresenter.getSubIncident(agentId, strCategoryId, strOpenSelectedItemId, isOpen ? Constant.OPENCOMPLAINT : Constant.CLOSECOMPLAINT);
        } else if (object instanceof IncidentListModel) {
            for (int i = 0; i < arrSubIncidents.size(); i++) {
                if (((IncidentListModel) object).getPk() == (arrSubIncidents.get(i).getPk())) {
                    arrSubIncidents.get(i).getFields().setSelected(true);
                    strSubComplaintId = String.valueOf(arrSubIncidents.get(i).getPk());
                } else {
                    arrSubIncidents.get(i).getFields().setSelected(false);
                }
            }
            onIncidentClick(((IncidentListModel) object).getFields().getIncidentText());
        }
    }
    private void setDataForOneComplaintChild(List<IncidentListModel> arrSubIncidents) {
        strSubComplaintId = String.valueOf(arrSubIncidents.get(0).getPk());
        if (isOpen) {
            if (mBinding.layoutOpenItems.recycleOpenComplaintsSubItem.getAdapter() != null) {
                mBinding.layoutOpenItems.recycleOpenComplaintsSubItem.getAdapter().notifyDataSetChanged();
            }
        } else {
            if (mBinding.layoutCloseItems.recycleCloseComplaintsSubItem.getAdapter() != null) {
                mBinding.layoutCloseItems.recycleCloseComplaintsSubItem.getAdapter().notifyDataSetChanged();
            }
        }
        onIncidentClick(arrSubIncidents.get(0).getFields().getIncidentText());
    }

    /**
     *
     * @param incidentText Describe the particular incident like:- Support etcc.
     */

    private void onIncidentClick(String incidentText) {
        clearComments();

        if (isOpen) {

            mBinding.layoutOpenItems.txtComments.setVisibility(View.VISIBLE);
            mBinding.layoutOpenItems.txtComments.setText(incidentText);
            if (!PreferenceManager.getPrefUserType(this).equalsIgnoreCase("AG")) {
                strComplaintCloseOrOpen = "";
                mBinding.layoutOpenItems.linearReply.setVisibility(View.VISIBLE);
                ((RadioButton) findViewById(R.id.rd_open)).setChecked(false);
                ((RadioButton) findViewById(R.id.rd_close)).setChecked(false);
                mBinding.layoutOpenItems.edReply.setText("");
            }
        } else {
            mBinding.layoutCloseItems.txtComments.setVisibility(View.VISIBLE);
            mBinding.layoutCloseItems.txtComments.setText(incidentText);
            if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("AG")) {
                mBinding.layoutCloseItems.radioGpCloseItems.setVisibility(View.VISIBLE);
                mBinding.layoutCloseItems.btnSubmitFeedback.setVisibility(View.VISIBLE);
            }

        }
        mPresenter.getIncidentComments(strSubComplaintId);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back:
                super.onBackPressed();
                break;
            case R.id.btn_submit_complaint:
                if (isValidate())
                    mPresenter.createIncidence(new GrievanceCreateIncidentModel(agentId, SystemUtility.getDeviceId(this), strNewComplaintId, strCategoryId, mBinding.givenceNew.txtSpechToText.getText().toString()), Constant.NEWCOMPLAINT);
                break;
            case R.id.btn_submit_feedback:
                if (isValidFieldsCloses()) {
                    mPresenter.submitFeedback(new GrievanceCreateIncidentModel(agentId, SystemUtility.getDeviceId(this), strCategoryId, mBinding.layoutCloseItems.txtComments.getText().toString(), strSubComplaintId, isSatisfied ? "1" : "0"));
                }
                break;

            case R.id.btn_reply:
                if (validateCommentReply()) {
                    mPresenter.submitReplyOnIncident(new GrievanceCreateIncidentModel(PreferenceManager.getAgentId(this), SystemUtility.getDeviceId(this), strComplaintCloseOrOpen.equalsIgnoreCase(getResources().getString(R.string.str_open)) ? "2" : "3", strSubComplaintId, mBinding.layoutOpenItems.edReply.getText().toString(), true), Constant.OPENCOMPLAINT);
                }
                break;
            default:
                break;
        }
    }
    /**
     * This method is validate the comments fields
     * @return
     */

    private boolean validateCommentReply() {
        if (strComplaintCloseOrOpen.isEmpty()) {
            onInfo(getResources().getString(R.string.str_error_select_feedback_type));
            return false;
        } else if (!StringUtility.validateString(mBinding.layoutOpenItems.edReply.getText().toString())) {
            onInfo(getResources().getString(R.string.str_reply_msg_empty));
            return false;
        }
        return true;
    }

    /**
     * On close tab validate the fields before submit on server
     * @return
     */
    private boolean isValidFieldsCloses() {
        if (strSubComplaintId.isEmpty()) {
            onInfo(getResources().getString(R.string.str_sub_complaint_not_selected));
            return false;
        } else
            return true;
    }

    /**
     * This method validate fields before submitting the New complaint
     * @return
     */
    private boolean isValidate() {
        if (strNewComplaintId.isEmpty()) {
            onInfo(getResources().getString(R.string.str_error_select_complaint_type));
            return false;
        } else if (!StringUtility.validateString(mBinding.givenceNew.txtSpechToText.getText().toString())) {
            onInfo(getResources().getString(R.string.str_error_complaint_message));
            return false;
        } else
            return true;
    }

    /**
     * Handle the Tabs Items visibility
     * New Close open
     */
    private void setVisibility(String incidentStatusText) {
        hideKeyBoard();
        if (incidentStatusText.equalsIgnoreCase("New")) {
            handleNewItemsView();
        } else if (incidentStatusText.equalsIgnoreCase("Open")) {
            handleOpenView();
        } else if (incidentStatusText.equalsIgnoreCase("Close")) {
            handleCloseItemsView();
        }
    }

    /**
     * Set the New Complaints Items
     */
    private void handleNewItemsView() {
        strNewComplaintId = "";
        mBinding.givenceNew.grievanceNewItem.setVisibility(View.VISIBLE);
        mBinding.layoutCloseItems.linearCloseItems.setVisibility(View.GONE);
        mBinding.layoutOpenItems.linearOpenItems.setVisibility(View.GONE);
        mPresenter.getIncidentOptions();
        mBinding.givenceNew.txtSpechToText.setText("");
    }

    /**
     * Set the CLose Complaints items
     */

    private void handleCloseItemsView() {
        isOpen = false;
        strSubComplaintId = "";
        
        mBinding.givenceNew.grievanceNewItem.setVisibility(View.GONE);
        mBinding.layoutCloseItems.linearCloseItems.setVisibility(View.VISIBLE);
        mBinding.layoutOpenItems.linearOpenItems.setVisibility(View.GONE);
        mBinding.layoutCloseItems.txtComments.setVisibility(View.GONE);
        mBinding.layoutCloseItems.radioGpCloseItems.setVisibility(View.GONE);
        mBinding.layoutCloseItems.btnSubmitFeedback.setVisibility(View.GONE);
        arrSubIncidents.clear();
        clearComments();
        if (mBinding.layoutCloseItems.recycleCloseComplaintsSubItem.getAdapter() != null)
            mBinding.layoutCloseItems.recycleCloseComplaintsSubItem.getAdapter().notifyDataSetChanged();
        mBinding.layoutCloseItems.txtComplaintSubType.setVisibility(View.GONE);
        mPresenter.getOpenItems(agentId, strCategoryId, Constant.CLOSECOMPLAINT);

    }

    /**
     * Set the Open compaints Items view
     */

    private void handleOpenView() {
        strComplaintCloseOrOpen = "";
        mBinding.layoutOpenItems.rdGpReplyType.setOnCheckedChangeListener(null);
        mBinding.layoutOpenItems.rdGpReplyType.clearCheck();
        mBinding.givenceNew.grievanceNewItem.setVisibility(View.GONE);
        mBinding.layoutCloseItems.linearCloseItems.setVisibility(View.GONE);
        mBinding.layoutOpenItems.linearOpenItems.setVisibility(View.VISIBLE);
        mBinding.layoutOpenItems.linearReply.setVisibility(View.GONE);
        arrSubIncidents.clear();
        mBinding.layoutOpenItems.rdGpReplyType.setOnCheckedChangeListener(this::onCheckedChanged);
        isOpen = true;
        if (mBinding.layoutOpenItems.recycleOpenComplaintsSubItem.getAdapter() != null)
            mBinding.layoutOpenItems.recycleOpenComplaintsSubItem.getAdapter().notifyDataSetChanged();
        clearComments();
        mBinding.layoutOpenItems.txtComplaintSubType.setVisibility(View.GONE);
        mBinding.layoutOpenItems.txtComments.setVisibility(View.GONE);
        mPresenter.getOpenItems(agentId, strCategoryId, Constant.OPENCOMPLAINT);

    }

    /**
     * Bind the Open and Close items comments data.
     */

    private void setDataOnIncidentClick() {
        arrComments.clear();
        if (isOpen) {
            mBinding.layoutOpenItems.linearReply.setVisibility(View.GONE);
            mBinding.layoutOpenItems.txtComments.setVisibility(View.GONE);
            if (mBinding.layoutOpenItems.recycleComments.getAdapter() != null)
                mBinding.layoutOpenItems.recycleComments.getAdapter().notifyDataSetChanged();
        } else {
            if (mBinding.layoutCloseItems.recycleComments.getAdapter() != null)
                mBinding.layoutCloseItems.recycleComments.getAdapter().notifyDataSetChanged();
            mBinding.layoutCloseItems.txtComments.setVisibility(View.GONE);
        }


    }

    /**
     * This method is used for clear the comments on Open and Close
     * Tabs
     */

    private void clearComments() {
        arrComments.clear();
        if (isOpen) {
            if (mBinding.layoutOpenItems.recycleComments.getAdapter() != null)
                mBinding.layoutOpenItems.recycleComments.getAdapter().notifyDataSetChanged();
        } else {
            if (mBinding.layoutCloseItems.recycleComments.getAdapter() != null)
                mBinding.layoutCloseItems.recycleComments.getAdapter().notifyDataSetChanged();
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.rd_satisfied:
                isSatisfied = true;
                ((RadioButton) findViewById(R.id.rd_unsatisfied)).setChecked(false);
                ((RadioButton) findViewById(R.id.rd_satisfied)).setChecked(true);
                break;
            case R.id.rd_unsatisfied:
                isSatisfied = false;
                ((RadioButton) findViewById(R.id.rd_unsatisfied)).setChecked(true);
                ((RadioButton) findViewById(R.id.rd_satisfied)).setChecked(false);
                break;

            case R.id.rd_open:
                ((RadioButton) findViewById(R.id.rd_open)).setChecked(true);
                ((RadioButton) findViewById(R.id.rd_close)).setChecked(false);
                strComplaintCloseOrOpen = getResources().getString(R.string.str_open);
                break;
            case R.id.rd_close:
                ((RadioButton) findViewById(R.id.rd_open)).setChecked(false);
                ((RadioButton) findViewById(R.id.rd_close)).setChecked(true);
                strComplaintCloseOrOpen = getResources().getString(R.string.str_close);
                break;
            default:
                break;
        }
    }
}