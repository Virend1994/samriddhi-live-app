package com.app.samriddhi.ui.model;

import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserModel extends BaseModel {
    @SerializedName("UserSAPStatus")
    @Expose
    public UserSAPStatus userSAPStatus;
    @SerializedName("mobileotpsent")
    @Expose
    private String mobileotpsent;
    @SerializedName("mobileno")
    @Expose
    private String mobileno;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("DeviceTokenId")
    @Expose
    private String device_token;
    @SerializedName("usermaster")
    @Expose
    private List<Usermaster> usermaster = null;
    @SerializedName("userdetail")
    @Expose
    private List<Userdetail> userdetail = null;

    @SerializedName("user_info")
    @Expose
    private List<AgencyModel> user_info = null;


    private String device_id;
    @SerializedName("Bp_code")
    @Expose
    private String bp_code;

    public UserSAPStatus getUserSAPStatus() {
        return userSAPStatus;
    }

    public void setUserSAPStatus(UserSAPStatus userSAPStatus) {
        this.userSAPStatus = userSAPStatus;
    }







    @SerializedName("Password")
    @Expose

    private String password;
    private Boolean isOtpVerified;

    @SerializedName("Leng")
    @Expose
    private String language;


    public List<AgencyModel> getUser_info() {
        return user_info;
    }

    public void setUser_info(List<AgencyModel> user_info) {
        this.user_info = user_info;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public List<Usermaster> getUsermaster() {
        return usermaster;
    }

    public void setUsermaster(List<Usermaster> usermaster) {
        this.usermaster = usermaster;
    }

    public List<Userdetail> getUserdetail() {
        return userdetail;
    }

    public void setUserdetail(List<Userdetail> userdetail) {
        this.userdetail = userdetail;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    @Bindable
    public String getBp_code() {
        return bp_code;
    }

    public void setBp_code(String bp_code) {
        this.bp_code = bp_code;
        notifyPropertyChanged(androidx.databinding.library.baseAdapters.BR.bp_code);
    }

    @Bindable
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
        notifyPropertyChanged(androidx.databinding.library.baseAdapters.BR.password);
    }

    @Bindable
    public Boolean getOtpVerified() {
        return isOtpVerified;
    }

    public void setOtpVerified(Boolean otpVerified) {
        isOtpVerified = otpVerified;
        notifyPropertyChanged(BR.otpVerified);
    }

    @Bindable
    public String getMobileotpsent() {
        return mobileotpsent;
    }

    public void setMobileotpsent(String mobileotpsent) {
        this.mobileotpsent = mobileotpsent;
        notifyPropertyChanged(androidx.databinding.library.baseAdapters.BR.mobileotpsent);
    }

    @Bindable
    public String getMobileno() {
        return mobileno;
    }

    public void setMobileno(String mobileno) {
        this.mobileno = mobileno;
        notifyPropertyChanged(androidx.databinding.library.baseAdapters.BR.mobileno);
    }

    @Bindable
    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
        notifyPropertyChanged(androidx.databinding.library.baseAdapters.BR.otp);
    }


    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}