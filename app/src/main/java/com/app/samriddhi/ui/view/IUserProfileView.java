package com.app.samriddhi.ui.view;

import com.app.samriddhi.ui.model.UserProfileModel;

public interface IUserProfileView extends IView {
    void onProfileSuccess(UserProfileModel mUserProfileData, Boolean isedit);
}
