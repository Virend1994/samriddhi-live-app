package com.app.samriddhi.ui.activity.main.ui.Grievance;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.HomeActivity;
import com.app.samriddhi.util.Constant;
import com.google.android.material.card.MaterialCardView;

import org.json.JSONObject;

public class HelpSupportStatus extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_support_status);

        ImageView imgBack=findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        MaterialCardView card_complaint=findViewById(R.id.card_complaint);
        TextView txtCountComplaint=findViewById(R.id.txtCountComplaint);
        card_complaint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferenceManager.setComplaint_Type(HelpSupportStatus.this, "c");
                startActivityAnimation(HelpSupportStatus.this,RequestDetailTabActivity.class, false);

            }
        });

        MaterialCardView card_query=findViewById(R.id.card_query);
        TextView txtCountQuery=findViewById(R.id.txtCountQuery);
        card_query.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferenceManager.setComplaint_Type(HelpSupportStatus.this, "q");
                startActivityAnimation(HelpSupportStatus.this,RequestDetailTabActivity.class, false);
            }
        });

        MaterialCardView card_suggestion=findViewById(R.id.card_suggestion);
        TextView txtCountSuggesion=findViewById(R.id.txtCountSuggesion);

        card_suggestion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferenceManager.setComplaint_Type(HelpSupportStatus.this, "s");
                startActivityAnimation(HelpSupportStatus.this,RequestDetailTabActivity.class, false);

            }
        });

        MaterialCardView card_request=findViewById(R.id.card_request);
        TextView txtCountRequest=findViewById(R.id.txtCountRequest);
        card_request.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PreferenceManager.setComplaint_Type(HelpSupportStatus.this, "r");
                startActivityAnimation(HelpSupportStatus.this,RequestDetailTabActivity.class, false);
            }
        });

        enableLoadingBar(true);
        card_complaint.setClickable(false);
        card_query.setClickable(false);
        card_request.setClickable(false);
        card_suggestion.setClickable(false);

        AndroidNetworking.post(Constant.BASE_URL_Grievance + "agentservices/getAgentRecords")
                .addBodyParameter("agent_id",PreferenceManager.getAgentId(HelpSupportStatus.this))
                .setTag("Counts")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            if(response.getString("status").equals("success")){
                                txtCountComplaint.setText(response.getString("total_c"));
                                txtCountQuery.setText(response.getString("total_q"));
                                txtCountRequest.setText(response.getString("total_r"));
                                txtCountSuggesion.setText(response.getString("total_s"));
                                dismissProgressBar();

                                card_complaint.setClickable(true);
                                card_query.setClickable(true);
                                card_request.setClickable(true);
                                card_suggestion.setClickable(true);
                            }
                        }
                        catch (Exception ex){
                            dismissProgressBar();
                            Log.e("fdhdfhd",ex.getMessage());
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        dismissProgressBar();
                        Log.e("fdhdfhd",anError.getMessage());
                    }
                });


    }



    public void startActivityAnimation(Context context, Class destinationClass, boolean addFlags) {

        if (addFlags)
            startActivity(new Intent(context, destinationClass).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        else
            startActivity(new Intent(context, destinationClass));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }


}