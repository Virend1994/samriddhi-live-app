package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IncidentFields extends BaseModel {
    @SerializedName("Incident_Cat")
    @Expose
    private String incidentCat;
    @SerializedName("Incident_Status")
    @Expose
    private String incidentStatus;
    @SerializedName("Incident_Text")
    @Expose
    private String incidentText;
    @SerializedName("Incident_VKORG")
    @Expose
    private Object incidentVKORG;
    @SerializedName("Incident_BP_CODE")
    @Expose
    private String incidentBPCODE;
    @SerializedName("Incident_Last_Feedback")
    @Expose
    private Object incidentLastFeedback;
    @SerializedName("Incident_Last_Feedback_By")
    @Expose
    private Object incidentLastFeedbackBy;
    @SerializedName("Incident_Last_Feedback_On")
    @Expose
    private String incidentLastFeedbackOn;
    @SerializedName("Incident_Last_Comment")
    @Expose
    private String incidentLastComment;
    @SerializedName("Incident_Last_Comment_By")
    @Expose
    private String incidentLastCommentBy;
    @SerializedName("Incident_Last_Comment_On")
    @Expose
    private String incidentLastCommentOn;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("create_date")
    @Expose
    private String createDate;
    @SerializedName("update_date")
    @Expose
    private String updateDate;
    @SerializedName("create_user")
    @Expose
    private String createUser;
    @SerializedName("update_user")
    @Expose
    private String updateUser;

    public String getIncidentCat() {
        return incidentCat;
    }

    public void setIncidentCat(String incidentCat) {
        this.incidentCat = incidentCat;
    }

    public String getIncidentStatus() {
        return incidentStatus;
    }

    public void setIncidentStatus(String incidentStatus) {
        this.incidentStatus = incidentStatus;
    }

    public String getIncidentText() {
        return incidentText;
    }

    public void setIncidentText(String incidentText) {
        this.incidentText = incidentText;
    }

    public Object getIncidentVKORG() {
        return incidentVKORG;
    }

    public void setIncidentVKORG(Object incidentVKORG) {
        this.incidentVKORG = incidentVKORG;
    }

    public String getIncidentBPCODE() {
        return incidentBPCODE;
    }

    public void setIncidentBPCODE(String incidentBPCODE) {
        this.incidentBPCODE = incidentBPCODE;
    }

    public Object getIncidentLastFeedback() {
        return incidentLastFeedback;
    }

    public void setIncidentLastFeedback(Object incidentLastFeedback) {
        this.incidentLastFeedback = incidentLastFeedback;
    }

    public Object getIncidentLastFeedbackBy() {
        return incidentLastFeedbackBy;
    }

    public void setIncidentLastFeedbackBy(Object incidentLastFeedbackBy) {
        this.incidentLastFeedbackBy = incidentLastFeedbackBy;
    }

    public String getIncidentLastFeedbackOn() {
        return incidentLastFeedbackOn;
    }

    public void setIncidentLastFeedbackOn(String incidentLastFeedbackOn) {
        this.incidentLastFeedbackOn = incidentLastFeedbackOn;
    }

    public String getIncidentLastComment() {
        return incidentLastComment;
    }

    public void setIncidentLastComment(String incidentLastComment) {
        this.incidentLastComment = incidentLastComment;
    }

    public String getIncidentLastCommentBy() {
        return incidentLastCommentBy;
    }

    public void setIncidentLastCommentBy(String incidentLastCommentBy) {
        this.incidentLastCommentBy = incidentLastCommentBy;
    }

    public String getIncidentLastCommentOn() {
        return incidentLastCommentOn;
    }

    public void setIncidentLastCommentOn(String incidentLastCommentOn) {
        this.incidentLastCommentOn = incidentLastCommentOn;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }
}
