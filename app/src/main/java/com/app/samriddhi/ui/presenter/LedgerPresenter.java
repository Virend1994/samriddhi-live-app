package com.app.samriddhi.ui.presenter;

import android.util.Log;

import com.app.samriddhi.R;
import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.response.JsonObjectResponse;
import com.app.samriddhi.base.BasePresenter;
import com.app.samriddhi.ui.model.LedgerModel;
import com.app.samriddhi.ui.view.ILedgerView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LedgerPresenter extends BasePresenter<ILedgerView> {

    /**
     * users ledger data
     * @param agentCode logged in user id
     * @param fromDate from date
     * @param toDate todate
     * @param docType document type
     */

    public void getLedgerData(String agentCode, String fromDate, String toDate, String docType) {
        Log.e("dgdffgdfgd",agentCode);
        Log.e("dgdffgdfgd",fromDate);
        Log.e("dgdffgdfgd",toDate);
        Log.e("dgdffgdfgd",docType);

        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getLedger(agentCode, fromDate, toDate, docType).enqueue(new Callback<JsonObjectResponse<LedgerModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<LedgerModel>> call, Response<JsonObjectResponse<LedgerModel>> response) {
                getView().enableLoadingBar(false);
                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null) {
                            getView().onSuccess(response.body().body, response.body().AgencyName);
                        } else
                            getView().onError(getView().getContext().getString(R.string.str_no_trans_found));

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<LedgerModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });

    }
}
