package com.app.samriddhi.ui.activity.main.ui.AgencyClose;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.R;
import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.Agency_Copy_Shifted_resignation;
import com.app.samriddhi.databinding.ActivityAgencyCloseBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule.KYBPActivity;
import com.app.samriddhi.ui.activity.main.ui.Grievance.SubmitRequestPopup;
import com.app.samriddhi.ui.model.DropDownModel;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.Util;
import com.bumptech.glide.Glide;
import com.codekidlabs.storagechooser.StorageChooser;
import com.github.chrisbanes.photoview.PhotoView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.Credentials;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AgencyCloseActivity extends BaseActivity {

    ActivityAgencyCloseBinding agencyCloseBinding;
    String strAgencyStartDate = "";
    String strAgencyCloseDate = "";
    String strAgencyCode = "";
    int strReasonId = 1;
    Calendar myCalendar;
    DatePickerDialog datePickerDialog;
    StorageChooser chooser;
    private static final String IMAGE_DIRECTORY = "/directory";
    String mCapturedFilePath = "";
    private final int GALLERY = 1;
    private final int CAMERA = 2;
    private final int MyFILE = 3;

    File ImageFile;
    String strImage = "";
    Bitmap bitmap;
    private String dropSelectType = "";
    private String Nominee_relation = "";
    private String nominee_name = "";
    private String agency_name = "";
    private String txtAgencyCloseDate = "";
    public ArrayList<DropDownModel> relationMasterArray;
    private static final int MAX_BUFFER_SIZE = 1024 * 1024;
    String currentDate = "";

    String strEstimaedAmt="",strNewOutstanding="",strBalanceAmt="",strUnbilledAmount="",strCurrentOutStanding="",strAsd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_agency_close);
        agencyCloseBinding = DataBindingUtil.setContentView(this, R.layout.activity_agency_close);
        initeView();
        getRelationShip();

        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        currentDate = dateFormat.format(tomorrow);




    }


    public void initeView() {
        relationMasterArray = new ArrayList<>();
        agencyCloseBinding.tvNomineeName.setOnClickListener(vi -> {
            dropSelectType = "nomi_name";
            Util.showDropDown(relationMasterArray, "Select Nominee Relation", AgencyCloseActivity.this, this::onOptionClick);

        });

        agencyCloseBinding.imageCut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                agencyCloseBinding.relImageView.setVisibility(View.GONE);
                agencyCloseBinding.txtDocumentUpload.setVisibility(View.VISIBLE);
                agencyCloseBinding.txtDocumentUpload.setText("Select file");
                strImage = "";
            }
        });
        agencyCloseBinding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        agencyCloseBinding.agResignationRb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b == true) {
                    strReasonId = 1;
                    agencyCloseBinding.closeOutRb.setChecked(false);
                    agencyCloseBinding.closeNonPerRb.setChecked(false);
                    agencyCloseBinding.closeIssueRb.setChecked(false);
                    agencyCloseBinding.closeDeathRb.setChecked(false);
                    agencyCloseBinding.linearNominee.setVisibility(View.GONE);
                    agencyCloseBinding.txtUploadType.setText("Resignation Upload");
                }
            }
        });

        agencyCloseBinding.closeOutRb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b == true) {

                    strReasonId = 2;
                    agencyCloseBinding.agResignationRb.setChecked(false);
                    agencyCloseBinding.closeIssueRb.setChecked(false);
                    agencyCloseBinding.closeDeathRb.setChecked(false);
                    agencyCloseBinding.closeNonPerRb.setChecked(false);
                    agencyCloseBinding.linearNominee.setVisibility(View.GONE);
                    agencyCloseBinding.txtUploadType.setText("Document Upload");
                }
            }
        });

        agencyCloseBinding.closeNonPerRb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b == true) {

                    strReasonId = 3;
                    agencyCloseBinding.agResignationRb.setChecked(false);
                    agencyCloseBinding.closeIssueRb.setChecked(false);
                    agencyCloseBinding.closeDeathRb.setChecked(false);
                    agencyCloseBinding.closeOutRb.setChecked(false);
                    agencyCloseBinding.linearNominee.setVisibility(View.GONE);
                    agencyCloseBinding.txtUploadType.setText("Document Upload");
                }
            }
        });

        agencyCloseBinding.closeIssueRb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b == true) {

                    strReasonId = 4;
                    agencyCloseBinding.agResignationRb.setChecked(false);
                    agencyCloseBinding.closeNonPerRb.setChecked(false);
                    agencyCloseBinding.closeDeathRb.setChecked(false);
                    agencyCloseBinding.closeOutRb.setChecked(false);
                    agencyCloseBinding.linearNominee.setVisibility(View.GONE);
                    agencyCloseBinding.txtUploadType.setText("Document Upload");
                }
            }
        });


        agencyCloseBinding.closeDeathRb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

                if (b == true) {

                    strReasonId = 5;
                    agencyCloseBinding.agResignationRb.setChecked(false);
                    agencyCloseBinding.closeNonPerRb.setChecked(false);
                    agencyCloseBinding.closeIssueRb.setChecked(false);
                    agencyCloseBinding.closeOutRb.setChecked(false);
                    agencyCloseBinding.linearNominee.setVisibility(View.VISIBLE);
                    agencyCloseBinding.txtUploadType.setText("Document Upload");
                }
            }
        });

        agencyCloseBinding.editAgentCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                strAgencyCode = String.valueOf(editable);

                if (strAgencyCode.length() == 8) {

                    AgencyDetail(strAgencyCode);
                }

            }
        });


        myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener dateTO = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                // TODO Auto-generated method stub

                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                setCloseDate();
            }

        };
        LinearLayout linearCloseDate = findViewById(R.id.linearCloseDate);
        linearCloseDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                datePickerDialog = new DatePickerDialog(AgencyCloseActivity.this, dateTO, myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();

            }
        });


        agencyCloseBinding.btnShowNoofCopies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                editor.putString(AppsContants.DetailStatus, "1");
                editor.putString(AppsContants.AgencyId, strAgencyCode);
                editor.commit();
                startActivity(new Intent(AgencyCloseActivity.this, BillingDetailsActivity.class));
            }
        });


        agencyCloseBinding.btnShowDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                editor.putString(AppsContants.DetailStatus, "2");
                editor.putString(AppsContants.AgencyId, strAgencyCode);
                editor.commit();
                startActivity(new Intent(AgencyCloseActivity.this, GetMonthDetailsActivity.class));
            }
        });

        agencyCloseBinding.btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                nominee_name = agencyCloseBinding.nomineeName.getText().toString();
                agency_name = agencyCloseBinding.txtAgentName.getText().toString();
                txtAgencyCloseDate = agencyCloseBinding.txtAgencyCloseDate.getText().toString();
                if (agency_name.equals("")) {
                    Toast.makeText(AgencyCloseActivity.this, "Enter Agency Code", Toast.LENGTH_SHORT).show();

                } else if (txtAgencyCloseDate.equals("")) {
                    Toast.makeText(AgencyCloseActivity.this, "Select closing date", Toast.LENGTH_SHORT).show();
                } else if (agencyCloseBinding.editRemark.getText().toString().length() == 0) {
                    Toast.makeText(AgencyCloseActivity.this, "Enter Remark", Toast.LENGTH_SHORT).show();
                } else {

                    if (strReasonId == 5) {
                        if (Nominee_relation.equals("")) {
                            alertMessage("Please select nominee type");
                            return;
                        } else if (nominee_name.equals("")) {
                            alertMessage("Please enter nominee name");
                            return;
                        } else if (strImage.equals("")) {
                            Toast.makeText(AgencyCloseActivity.this, "Select File", Toast.LENGTH_SHORT).show();
                        } else {

                            SubmitClosureRequest();
                        }

                    } else if (strReasonId == 1 || strReasonId == 5) {
                        if (strImage.equals("")) {
                            Toast.makeText(AgencyCloseActivity.this, "Select File", Toast.LENGTH_SHORT).show();

                        } else {
                            SubmitClosureRequest();

                        }
                    } else {
                        SubmitClosureRequest();

                    }


                }
            }
        });


        agencyCloseBinding.txtDocumentUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPictureDialog();

            }
        });

        chooser = new StorageChooser.Builder()
                .withActivity(AgencyCloseActivity.this)
                .withFragmentManager(getFragmentManager())
                .withMemoryBar(true)
                .build();


        chooser.setOnSelectListener(new StorageChooser.OnSelectListener() {
            @Override
            public void onSelect(String path) {
                Log.e("SELECTED_PATH", path);
            }
        });


        agencyCloseBinding.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (bitmap != null) {
                    showAlert();
                }

            }
        });
    }

    private void onOptionClick(DropDownModel dataList) {
        if (dropSelectType.equalsIgnoreCase("nomi_name")) {
            Util.hideDropDown();
            agencyCloseBinding.tvNomineeName.setText(dataList.getDescription());
            Nominee_relation = dataList.getDescription();

        }
    }


    private void alertMessage(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(this, R.style.AppTheme_AlertDialog)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })

                .show();
    }


    public void getRelationShip() {
        // Calling JSON
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getRelationShip(PreferenceManager.getAgentId(AgencyCloseActivity.this));
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                if (response.isSuccessful()) {


                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            relationMasterArray.clear();
                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("relation"));
                                dropDownModel.setId(objStr.getString("pk"));
                                relationMasterArray.add(dropDownModel);
                            }
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(AgencyCloseActivity.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(AgencyCloseActivity.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Toast.makeText(AgencyCloseActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });

    }


    public void showAlert() {


        View popupView = null;

        popupView = LayoutInflater.from(AgencyCloseActivity.this).inflate(R.layout.enlarge_image, null);
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0);

        ImageView closeImg = popupView.findViewById(R.id.closeImg);
        closeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                popupWindow.dismiss();
            }
        });

        PhotoView imageView = popupView.findViewById(R.id.imageView);
        imageView.setImageBitmap(bitmap);

    }


    public void showPictureDialog() {

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(AgencyCloseActivity.this);
        builder.setTitle("Select Action");
        String[] pictureDialogItems = {"Select photo from gallery", "Capture image from camera", "Select from storage"};

        builder.setItems(pictureDialogItems, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                switch (which) {

                    case 0:
                        choosePhotoFromGallery();
                        break;

                    case 1:
                        captureFromCamera();
                        break;

                    case 2:
                        mCapturedFilePath = createFile(getFilename());
                        Intent intent = new Intent();
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        intent.setType("application/pdf");
                        startActivityForResult(intent, MyFILE);
                        break;
                }

            }
        });

        builder.show();
    }


    public void choosePhotoFromGallery() {

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY);
    }


    public void captureFromCamera() {

        Intent intent_2 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent_2, CAMERA);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                strImage = contentURI.toString();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
                    agencyCloseBinding.imageView.setImageBitmap(bitmap);
                    agencyCloseBinding.relImageView.setVisibility(View.VISIBLE);
                    agencyCloseBinding.txtDocumentUpload.setVisibility(View.GONE);
                    Log.e("Gallery Path", path);
                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(AgencyCloseActivity.this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            bitmap = (Bitmap) data.getExtras().get("data");
            Uri contentURI = data.getData();
            strImage = contentURI.toString();
            agencyCloseBinding.imageView.setImageBitmap(bitmap);
            agencyCloseBinding.relImageView.setVisibility(View.VISIBLE);
            agencyCloseBinding.txtDocumentUpload.setVisibility(View.GONE);
            saveImage(bitmap);
            //Toast.makeText(AgencyCloseActivity.this, "Image Saved!", Toast.LENGTH_SHORT).show();
        } else if (requestCode == MyFILE) {
            Log.e("sdgsdgsdg", "Else");
            Uri uri = data.getData();
            String uriString = uri.toString();
            strImage = uri.toString();
            File myFile = new File(uriString);
            String path = myFile.getAbsolutePath();
            String displayName = null;
            createFile(mCapturedFilePath);
            writeToFile(uri, mCapturedFilePath);

            if (uriString.startsWith("content://")) {
                Cursor cursor = null;
                try {
                    cursor = getContentResolver().query(uri, null, null, null, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                        Log.e("sdgsdgsdg", "If" + " " + strImage);
                        Log.e("sdgsdgsdg", "If" + " " + displayName);
                        agencyCloseBinding.txtDocumentUpload.setText(displayName);
                        agencyCloseBinding.txtDocumentUpload.setVisibility(View.VISIBLE);
                        agencyCloseBinding.relImageView.setVisibility(View.GONE);
                    }
                } finally {
                    cursor.close();
                }
            } else if (uriString.startsWith("file://")) {
                displayName = myFile.getName();
                Log.e("sdgsdgsdg", "Else" + " " + displayName);
                agencyCloseBinding.txtDocumentUpload.setText(displayName);
                agencyCloseBinding.txtDocumentUpload.setVisibility(View.VISIBLE);
                agencyCloseBinding.relImageView.setVisibility(View.GONE);
            }
        }


    }


    public String createFile(String fileName) {
        String targetFolder = getApplicationContext().getFilesDir().getAbsolutePath() + "/mypdf/";
        File file = new File(targetFolder);
        file.mkdirs();
        // then create files under new folder like
        File myFile = new File(targetFolder, fileName);
        return myFile.getAbsolutePath();
    }

    public void writeToFile(Uri uri, String filePath) {
        try {
            InputStream inputStream = getContentResolver().openInputStream(uri);
            if (inputStream != null) {
                File destination = new File(filePath);
                ImageFile = destination;
                Log.e("sdffdsfffsdf", destination.toString());
                int bytesAvailable = inputStream.available();
                int bufferSize = Math.min(bytesAvailable, MAX_BUFFER_SIZE);
                byte[] buffers = new byte[bufferSize];
                FileOutputStream outputStream = new FileOutputStream(destination);
                int read = inputStream.read(buffers);
                while (read != -1) {
                    outputStream.write(buffers, 0, read);
                    read = inputStream.read(buffers);
                }
                inputStream.close();
                outputStream.close();
            }
        } catch (IOException exception) {
            Log.e("dfsfasfa", exception.getMessage());
        }
    }

    private String getFilename() {
        return "PROFILE_IMG" + System.currentTimeMillis() + ".pdf";
    }


    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);

        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }
        try {
            ImageFile = new File(wallpaperDirectory, Calendar.getInstance().getTimeInMillis() + ".jpg");
            ImageFile.createNewFile();
            strImage = ImageFile.toString();

            FileOutputStream fo = new FileOutputStream(ImageFile);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(AgencyCloseActivity.this,
                    new String[]{ImageFile.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("fvbcbv", "File Saved::---&gt;" + ImageFile.getAbsolutePath());
            agencyCloseBinding.txtDocumentUpload.setText(ImageFile.getName());
            return ImageFile.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }


    private void setCloseDate() {

        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        strAgencyCloseDate = sdf.format(myCalendar.getTime());
        agencyCloseBinding.txtAgencyCloseDate.setText(strAgencyCloseDate);
        getTimeDifference(strAgencyStartDate, strAgencyCloseDate);
        GetDateDIff(currentDate, strAgencyCloseDate);
    }


    public void GetDateDIff(String currentDate, String strAgencyCloseDate) {
        Log.e("newLog", currentDate);
        Log.e("newLog", strAgencyCloseDate);
        int todayPlusOne=0;
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd").withLocale(Locale.US);
        LocalDate startDate = LocalDate.parse(currentDate, formatter);

        LocalDate endDate = LocalDate.parse(strAgencyCloseDate, formatter);
        Period period = Period.between(startDate, endDate);
        String strDays = String.valueOf(period.getDays());
        Log.e("newLog", strDays);


        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String todayAsString = dateFormat.format(today);
        String[] strSplit =todayAsString.split("-");
        int todayDate= Integer.parseInt(strSplit[2]);

        Calendar date = Calendar.getInstance();
        String dayToday = android.text.format.DateFormat.format("EEEE", date).toString();
        Log.e("dgsdgsdgs",dayToday);

        if(dayToday.equals("Saturday")){

            todayPlusOne=todayDate+2;
        }
        else{

            todayPlusOne=todayDate+1;
        }

        /*   check if Saturday */

        int unbilledAmt= Integer.parseInt(strUnbilledAmount);
        int diff= Integer.parseInt(strDays);
        Log.e("sgdgsgsgsgsg", unbilledAmt+"");
        Log.e("sgdgsgsgsgsg", todayPlusOne+"");

        /* divide unbilled amount from cureent date + 1*/

        int getDividedValue=unbilledAmt/todayPlusOne;

        Log.e("sgdgsgsgsgsg", getDividedValue+"");

        /* To get emtimated amount */
        /* diff x getDividedValue*/
        int getEstimatedAmount=getDividedValue*diff;
        strEstimaedAmt= String.valueOf(getEstimatedAmount);
        agencyCloseBinding.txtEstimatedAmount.setText(strEstimaedAmt);
        int currentOutstanding= Integer.parseInt(strCurrentOutStanding);

        int totalOutsanding=currentOutstanding+unbilledAmt+getEstimatedAmount;

        agencyCloseBinding.txtNewoutstanding.setText(totalOutsanding+"");
        int asd= Integer.parseInt(strAsd);
        int balAMT=totalOutsanding-asd;
        agencyCloseBinding.txtBalance.setText(balAMT+"");


    }


    public void getTimeDifference(String strAgencyStartDate, String strAgencyCloseDate) {

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd").withLocale(Locale.US);
        LocalDate startDate = LocalDate.parse(strAgencyStartDate, formatter);
        LocalDate endDate = LocalDate.parse(strAgencyCloseDate, formatter);
        Period period = Period.between(startDate, endDate);
        String strYear = period.getYears() + " " + "Years";
        String strMonths = period.getMonths() + " " + "Months";
        String strDays = period.getDays() + " " + "Days";
        Log.e("sdgsdgsd", strYear + strMonths + strDays);
        agencyCloseBinding.txtTotalTime.setText(strYear + " " + strMonths + " " + strDays);
    }

    public void AgencyDetail(String strCode) {
        enableLoadingBar(true);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("agency_code", strCode);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(Constant.BASE_PORTAL_URL + "dash/api/agency-details")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .addJSONObjectBody(jsonObject)
                .setTag("Detail")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {

                            JSONObject jsonObject = new JSONObject(response.getString("agency_details"));
                            agencyCloseBinding.txtAgentName.setText(jsonObject.getString("Agentname"));
                            agencyCloseBinding.txtAgencyName.setText(jsonObject.getString("Agencyname"));
                            agencyCloseBinding.txtAgencyLocation.setText(jsonObject.getString("agency_location"));
                            agencyCloseBinding.txtUnbilledAmount.setText(jsonObject.getString("unbilled_amount"));
                            strUnbilledAmount=jsonObject.getString("unbilled_amount");
                            if (jsonObject.getString("NoCopies").contains(".")) {
                                String[] strSplit = jsonObject.getString("NoCopies").split("\\.");
                                agencyCloseBinding.txtAgencyCopies.setText(strSplit[0]);
                            } else {
                                agencyCloseBinding.txtAgencyCopies.setText(jsonObject.getString("NoCopies"));
                            }

                            agencyCloseBinding.txtAgencyStartDate.setText(jsonObject.getString("Startdate"));
                            agencyCloseBinding.txtAsd.setText(jsonObject.getString("Asd"));
                            agencyCloseBinding.txtTotalOutstandings.setText(jsonObject.getString("Outstanding"));
                            strCurrentOutStanding=jsonObject.getString("Outstanding");
                            strAsd=jsonObject.getString("Asd");

                            strAgencyStartDate = jsonObject.getString("Startdate");

                            dismissProgressBar();
                        } catch (Exception ex) {
                            dismissProgressBar();
                            Log.e("rtrtrtr", ex.getMessage());
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        dismissProgressBar();
                        Log.e("rtrtrtr", anError.getMessage());
                    }
                });

    }


    public void SubmitClosureRequest() {

        // Log.e("sddfsfsdfsd","File"+" "+ImageFile.toString());
        enableLoadingBar(true);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("agency_code", strAgencyCode);
            jsonObject.put("create_user_id", PreferenceManager.getAgentId(AgencyCloseActivity.this));
            jsonObject.put("access_type_id", PreferenceManager.getPrefUserType(AgencyCloseActivity.this));
            jsonObject.put("agency_name", agencyCloseBinding.txtAgencyName.getText().toString());
            jsonObject.put("agency_reason_select", strReasonId);
            jsonObject.put("agency_start_date", agencyCloseBinding.txtAgencyStartDate.getText().toString());
            jsonObject.put("agent_name", agencyCloseBinding.txtAgentName.getText().toString());
            jsonObject.put("asd", agencyCloseBinding.txtAsd.getText().toString());
            jsonObject.put("closing_date", agencyCloseBinding.txtAgencyCloseDate.getText().toString());
            jsonObject.put("no_of_copies", agencyCloseBinding.txtAgencyCopies.getText().toString());
            jsonObject.put("reason", agencyCloseBinding.editRemark.getText().toString());
            jsonObject.put("resignation_upload", "");
            jsonObject.put("total_outstanding", agencyCloseBinding.txtTotalOutstandings.getText().toString());
            jsonObject.put("total_time_spent_with_us", agencyCloseBinding.txtTotalTime.getText().toString());
            jsonObject.put("nominee_name", nominee_name);
            jsonObject.put("nominee_relationship", Nominee_relation);

            jsonObject.put("estd_bill_amt", agencyCloseBinding.txtEstimatedAmount.getText().toString());
            jsonObject.put("total_ots", agencyCloseBinding.txtNewoutstanding.getText().toString());
            jsonObject.put("unbilled_amount", agencyCloseBinding.txtUnbilledAmount.getText().toString());
            jsonObject.put("balance_amount", agencyCloseBinding.txtBalance.getText().toString());
            jsonObject.put("agency_location", agencyCloseBinding.txtAgencyLocation.getText().toString());
            jsonObject.put("remark", agencyCloseBinding.editAmtRemark.getText().toString());
            Log.e("sdfsdfsdfsf", jsonObject.toString());

        }
        catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(Constant.BASE_PORTAL_URL + "dash/api/agency-closure-update")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .addJSONObjectBody(jsonObject)
                .setTag("Detail")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            Log.e("sdfsdfsfdsfsf", response.toString());
                            //  {"message":"Agency Closure Request Create Successfully","status_code":1,"id":7}
                            if (response.getString("message").equals("Agency Closure Request Create Successfully")) {

                                Log.e("sdfsdfsfdsfsf", "DONE");
                                Log.e("sfafasfafa", response.getString("id"));
                                AndroidNetworking.upload(Constant.BASE_PORTAL_URL + "dash/api/agency-closure-file-upload/" + response.getString("id"))
                                        .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                                        .addMultipartFile("image", ImageFile)
                                        .setTag("Upload")
                                        .setPriority(Priority.HIGH)
                                        .build()
                                        .getAsJSONObject(new JSONObjectRequestListener() {
                                            @Override
                                            public void onResponse(JSONObject response) {
                                                dismissProgressBar();
                                                Log.e("sdfsdfsfdsfsf", response.toString());

                                                try {

                                                    if (response.getString("reply").equals("File Uploaded Successfully")) {
                                                        startActivity(new Intent(AgencyCloseActivity.this, AgencyCloseStausActivity.class));
                                                        //Toast.makeText(AgencyCloseActivity.this, "Request submitted successfully", Toast.LENGTH_SHORT).show();
                                                        finish();
                                                    } else {

                                                        Toast.makeText(AgencyCloseActivity.this, response.getString("reply"), Toast.LENGTH_SHORT).show();
                                                    }
                                                } catch (Exception ex) {
                                                    // Log.e("safafaf", anError.getMessage());
                                                    dismissProgressBar();
                                                    Log.e("sdfsdfsfdsfsf", ex.getMessage());
                                                }


                                            }

                                            @Override
                                            public void onError(ANError anError) {
                                                Log.e("sdfsdfsfdsfsf", anError.getMessage());
                                                dismissProgressBar();
                                                // Log.e("dfgdfgdfg", anError.getMessage());

                                            }
                                        });


                            } else {
                                dismissProgressBar();
                                if (response.getString("message").equals("Agency Request Already Excit")) {
                                    alertMessage("Request already exists.");
                                    return;
                                }
                            }

                        } catch (Exception ex) {
                            dismissProgressBar();
                            Log.e("rtrtrtr", ex.getMessage());
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        dismissProgressBar();
                        Log.e("rtrtrtr", anError.getMessage());
                    }
                });
    }
}
