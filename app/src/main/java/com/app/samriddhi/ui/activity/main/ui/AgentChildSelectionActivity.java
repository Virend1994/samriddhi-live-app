package com.app.samriddhi.ui.activity.main.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.appcompat.app.AlertDialog;

import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.model.AgentChildResultListModel;
import com.app.samriddhi.ui.model.AgentChildResultModel;
import com.app.samriddhi.ui.presenter.AgentChildDataPresenter;
import com.app.samriddhi.ui.view.IAgentChildView;

import java.util.ArrayList;
import java.util.List;

public class AgentChildSelectionActivity extends BaseActivity implements IAgentChildView {
    AgentChildDataPresenter mPresenter;
    ArrayList<String> arrCode = new ArrayList<>();
    boolean noBackHistory = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_selection);
        initView();
    }

    /**
     * Initialize the views
     */
    private void initView() {
        mPresenter = new AgentChildDataPresenter();
        mPresenter.setView(this);
        if (getIntent() != null && getIntent().hasExtra("noBackHistory")) {
            noBackHistory = getIntent().getBooleanExtra("noBackHistory", false);
        }
        mPresenter.getAgentChildData(PreferenceManager.getParentAgentId(this));
    }

    @Override
    public void onSuccess(AgentChildResultModel agentChildData) {
        showAgentOptions(agentChildData.getResults());
    }

    /**
     * This method shown the Agents multiple child's on dialog
     * @param results list of agents child
     */
    private void showAgentOptions(List<AgentChildResultListModel> results) {
        results.add(0, new AgentChildResultListModel(PreferenceManager.getParentUserType(this), PreferenceManager.getParentUserEmail(this),
                PreferenceManager.getParentAgentId(this), PreferenceManager.getParentUserPhone(this), PreferenceManager.getParentAgentName(this)));

        for (AgentChildResultListModel data : results) {
            arrCode.add(data.getPartner() + " ( " + data.getName1() + " )");
        }

        AlertDialog alertDialog = new AlertDialog.Builder(this, R.style.AlertDialogSignleChoice)
                .setTitle(getResources().getString(R.string.str_agent_child_select))
                .setCancelable(false)
                .setSingleChoiceItems(arrCode.toArray(new String[arrCode.size()]), 0, null)
                .setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setPrefDataForChild(((AlertDialog) dialog).getListView().getCheckedItemPosition(), results, dialog);
                    }
                })
                .setNegativeButton(getResources().getString(R.string.str_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (noBackHistory) {
                            startActivityAnimation(AgentChildSelectionActivity.this, HomeActivity.class, true);
                        }
                        AgentChildSelectionActivity.this.finish();
                    }
                })
                .create();
        alertDialog.show();


        Button nbutton = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
        Button pbutton = alertDialog.getButton(DialogInterface.BUTTON_POSITIVE);
        nbutton.setTextColor(getResources().getColor(R.color.white_color));
        nbutton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));


        pbutton.setTextColor(getResources().getColor(R.color.white_color));
        pbutton.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        params.setMargins(10, 0, 20, 0);
        nbutton.setLayoutParams(params);
        pbutton.setLayoutParams(params);
    }

    /**
     * This method save the Parent agent selected child data
     * @param position define the item position
     * @param results list of agents child
     * @param dialog alertDialog object
     */

    private void setPrefDataForChild(int position, List<AgentChildResultListModel> results, DialogInterface dialog) {
        PreferenceManager.setAgentName(this, results.get(position).getName1());
        PreferenceManager.setPrefAgentId(this, results.get(position).getPartner());
        PreferenceManager.setPrefUserType(this, results.get(position).getType());
        PreferenceManager.setUserPhone(this, results.get(position).getMobile());
        PreferenceManager.setUserEmail(this, results.get(position).getEmail());
        dialog.dismiss();
        startActivityAnimation(this, HomeActivity.class, false);

        finish();
    }

    @Override
    public Context getContext() {
        return this;
    }

}
