package com.app.samriddhi.ui.activity.main.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.samriddhi.R;
import com.app.samriddhi.api.response.JsonObjectResponse;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.RecyclerViewArrayAdapter;
import com.app.samriddhi.base.model.BaseModel;
import com.app.samriddhi.databinding.ActivityUserTypeListBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.fragment.ActionBottomDialogFragment;
import com.app.samriddhi.ui.model.AgentResultModel;
import com.app.samriddhi.ui.model.BillOutAgentResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCDistWiseResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCResultModel;
import com.app.samriddhi.ui.model.BillingOutstandingListModel;
import com.app.samriddhi.ui.model.BillingOutstandingResultModel;
import com.app.samriddhi.ui.model.CashCreditResultModel;
import com.app.samriddhi.ui.model.CityResultModel;
import com.app.samriddhi.ui.model.SalesDistrictWiseResultModel;
import com.app.samriddhi.ui.model.SalesOrgCityResultModel;
import com.app.samriddhi.ui.model.StateListModel;
import com.app.samriddhi.ui.model.StateResultModel;
import com.app.samriddhi.ui.model.UsersResultModel;
import com.app.samriddhi.ui.presenter.UserTypeCopiesDetailPresenter;
import com.app.samriddhi.ui.view.IUserTypeCopiesView;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.SystemUtility;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class UserStateActivity extends BaseActivity implements IUserTypeCopiesView, RecyclerViewArrayAdapter.OnItemClickListener<BaseModel>, ActionBottomDialogFragment.ItemClickListener {
    private final String STATE_CODE = "stateCode";
    private final String STATE_NAME = "stateName";
    ActivityUserTypeListBinding mBinding;
    UserTypeCopiesDetailPresenter mPresenter;
    Calendar c = Calendar.getInstance();


    String screenName = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_user_type_list);
        initializeView();
    }

    /**
     * initialize and bind views
     */

    private void initializeView() {
        mBinding.toolbarCalender.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mBinding.toolbarCalender.rlNotification.setOnClickListener(v -> {
            startActivityAnimation(this, NotificationActivity.class, false);
        });
        mBinding.imgFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showBottomSheetDialog();
            }
        });
        if (getIntent() != null) {
            if (getIntent().hasExtra(Constant.SCREEN_NAME)) {
                screenName = getIntent().getStringExtra(Constant.SCREEN_NAME);
            }

        }
        mBinding.setScrenName(screenName);
        if (screenName.equalsIgnoreCase(Constant.DashBoardNoOfCopies)) {
            mBinding.toolbarCalender.txtTittle.setText(getResources().getString(R.string.str_state_list) + " (" + getResources().getString(R.string.str_copies) + ")");
        } else
            mBinding.toolbarCalender.txtTittle.setText(getResources().getString(R.string.str_state_list));

        mPresenter = new UserTypeCopiesDetailPresenter();
        mPresenter.setView(this);
        mBinding.recycleList.setLayoutManager(new LinearLayoutManager(this));
        mBinding.tvOnPage.setText("All States");

        String date = new SimpleDateFormat("dd MMM, yyyy", Locale.getDefault()).format(new Date());
        mBinding.tvTodayDate.setText(date);

        if (screenName.equalsIgnoreCase(Constant.DashBoardOutstanding)) {

            mBinding.tvHeaderOne.setText(R.string.str_outstanding);
            mBinding.tvHeaderTwo.setText(R.string.str_billing_tittle);
            mBinding.toolbarCalender.txtTittle.setText(R.string.billing_and_outstanding);

            mPresenter.getBillingStateList(PreferenceManager.getAgentId(this));
          //  mPresenter.getOutstandingStateList(PreferenceManager.getAgentId(this), "S");
        }

        else if (screenName.equalsIgnoreCase(Constant.DashBoardBill)) {
            mBinding.tvHeaderOne.setText(R.string.str_outstanding);
            mBinding.tvHeaderTwo.setText(R.string.str_billing_tittle);
            mBinding.toolbarCalender.txtTittle.setText(R.string.billing_and_outstanding);
            mPresenter.getBillingStateList(PreferenceManager.getAgentId(this));

        }

        else if (screenName.equalsIgnoreCase(Constant.DashBoardGrievance)) {
            mPresenter.getGrievanceStateList(PreferenceManager.getAgentId(this), "S");
        } else
            mPresenter.getStateList(PreferenceManager.getAgentId(this));
    }

    @Override
    public void onCitySuccess(CityResultModel mCityModel) {
        // will override if require
    }

    @Override
    public void onStateSuccess(StateResultModel mStateModel) {
        mBinding.recycleList.setAdapter(new RecyclerViewArrayAdapter(mStateModel.getResults(), this, screenName));

    }

    @Override
    public void onStateSuccess(BillingOutstandingResultModel mStateModel) {
        mBinding.recycleList.setAdapter(new RecyclerViewArrayAdapter(mStateModel.getResults(), this, screenName));


    }

    @Override
    public void onAgentListSuccess(AgentResultModel mAgentResultModel) {
        // will override if require
    }

    @Override
    public void onBillOutAgentListSuccess(BillOutAgentResultModel mAgentResultModel) {

    }

    @Override
    public void onCreditCashSuccess(CashCreditResultModel mResultsData) {
        // will override if require
    }

    @Override
    public void onSuccess(JsonObjectResponse<UsersResultModel> body) {
        // will override if require
    }

    @Override
    public void onBillOUtCityUPCSuccess(JsonObjectResponse<BillOutCityUPCResultModel> body) {

    }

    @Override
    public void onSalesOrgCitySuccess(SalesOrgCityResultModel mData) {
        // will override if require
    }

    @Override
    public void onSalesDistrictCopies(SalesDistrictWiseResultModel mResultData) {
        // will override if require
    }

    @Override
    public void onBillOutCityUPCDistSuccess(BillOutCityUPCDistWiseResultModel mResultData) {

    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onItemClick(View view, BaseModel object) {
        if(object instanceof BillingOutstandingListModel)
        {
            if (screenName.equalsIgnoreCase(Constant.DashBoardBill)) {
                startActivity(new Intent(this, UserCityActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardBill).putExtra(STATE_CODE, ((BillingOutstandingListModel) object).getStateCode()).putExtra(STATE_NAME, ((BillingOutstandingListModel) object).getStateName()));
            }
            else if(screenName.equalsIgnoreCase(Constant.DashBoardOutstanding)) {
                startActivity(new Intent(this, UserCityActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardOutstanding).putExtra(STATE_CODE, ((BillingOutstandingListModel) object).getStateCode()).putExtra(STATE_NAME, ((BillingOutstandingListModel) object).getStateName()));
            }
        }
        else if(object instanceof StateListModel)
        {
            if (screenName.equalsIgnoreCase(Constant.DashBoardOutstanding))
                startActivity(new Intent(this, UserCityActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardOutstanding).putExtra(STATE_CODE, ((StateListModel) object).getStateCode()).putExtra(STATE_NAME, ((StateListModel) object).getStateName()));
            else if (screenName.equalsIgnoreCase(Constant.DashBoardNoOfCopies)) {
                startActivity(new Intent(this, SalesOrgCityActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardNoOfCopies).putExtra(STATE_CODE, ((StateListModel) object).getStateCode()).putExtra(STATE_NAME, ((StateListModel) object).getStateName()));

                //  startActivity(new Intent(this, UserCityActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardNoOfCopies).putExtra("stateCode", object.getState()));
            } else if (screenName.equalsIgnoreCase(Constant.DrawerProfile)) {
                startActivity(new Intent(this, UserCityActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DrawerProfile).putExtra(STATE_CODE, ((StateListModel) object).getStateCode()));
            } else if (screenName.equalsIgnoreCase(Constant.DashBoardGrievance)) {
                startActivity(new Intent(this, UserCityActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardGrievance).putExtra(STATE_CODE, ((StateListModel) object).getStateCode()));
            } else
                startActivityAnimation(this, AgentListActivity.class, false);
        }


        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }
    public void showBottomSheetDialog() {
        ActionBottomDialogFragment addPhotoBottomDialogFragment =
                ActionBottomDialogFragment.newInstance();
        addPhotoBottomDialogFragment.show(getSupportFragmentManager(),
                ActionBottomDialogFragment.TAG);
    }
    @Override public void onItemClick(String item,String fromDate,String toDate) {
        Toast.makeText(this, "Selected item is " + item+"From Date : "+fromDate+"\n"+"To Date : "+toDate, Toast.LENGTH_SHORT).show();
    }


//    private void showBottomSheetDialog() {
//
//        final BottomSheetDialog bottomSheetDialog = new BottomSheetDialog(this);
//        bottomSheetDialog.setContentView(R.layout.bottom_sheet_dialog);
//
//        LinearLayout copy = bottomSheetDialog.findViewById(R.id.copyLinearLayout);
//        LinearLayout share = bottomSheetDialog.findViewById(R.id.shareLinearLayout);
//        LinearLayout download = bottomSheetDialog.findViewById(R.id.download);
//        LinearLayout delete = bottomSheetDialog.findViewById(R.id.delete);
//
//        bottomSheetDialog.show();
//    }
}
