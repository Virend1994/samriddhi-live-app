package com.app.samriddhi.ui.activity.main.ui.Ledger;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.BuildConfig;
import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.databinding.ActivityConfirmationLedgerBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AppsContants;
import com.app.samriddhi.ui.model.NewLedgerModel;
import com.app.samriddhi.util.AlertUtility;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.SystemUtility;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.Credentials;

public class ConfirmationLedgerActivity extends BaseActivity {
    ActivityConfirmationLedgerBinding mBinding;
    List<NewLedgerModel> mData;
    File createdFile;
    String strAgencyId = "";
    String strUserCityUpc = "";
    String stropeningBal = "";
    String strAgentName = "";
    String strClosingBalnc = "";
    int strGetMonth = 0;
    String strDate1 = "", agentId = "", strDate2 = "", strFromToDate = "", strYear = "", agencyName = "", opBalance = "", closeBal = "";
    List<NewLedgerModel> newLedgerModelList;
    LinearLayout linear_city, linear_upc;
    RadioButton radio_city_jan_june, radio_city_jul_dec;
    RadioButton radio_upc_dec_may, radio_upc_jun_nov;
    String strConfirmYear = "";
    PopupWindow popupWindow;
    EditText ed1, ed2, ed3, ed4;
    String strOTPId = "", strOTP = "", strBpCode = "";
    String strConfirmLedgerStatus = "";
    String strUnitName = "", strUnitAdd = "", strLogoStatus = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_confirmation_ledger);
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);

        Calendar calendar = Calendar.getInstance();
        int intY = calendar.get(Calendar.YEAR);
        strYear = String.valueOf(intY);

        if (PreferenceManager.getPrefUserType(ConfirmationLedgerActivity.this).equals("AG")) {
            strUserCityUpc = PreferenceManager.getUserCityUpc(ConfirmationLedgerActivity.this);
            strAgencyId = PreferenceManager.getAgentId(ConfirmationLedgerActivity.this);

        } else {
            strUserCityUpc = AppsContants.sharedpreferences.getString(AppsContants.cityupc, "");
            strAgencyId = AppsContants.sharedpreferences.getString(AppsContants.AgencyId, "");
        }


        linear_city = findViewById(R.id.linear_city);
        linear_upc = findViewById(R.id.linear_upc);
        radio_city_jan_june = findViewById(R.id.radio_city_jan_june);
        radio_city_jul_dec = findViewById(R.id.radio_city_jul_dec);

        radio_upc_dec_may = findViewById(R.id.radio_upc_dec_may);
        radio_upc_jun_nov = findViewById(R.id.radio_upc_jun_nov);

       /* if (strUserCityUpc.equals("CRL") || strUserCityUpc.equals("CITY") || strUserCityUpc.equals("City")) {
            linear_city.setVisibility(View.VISIBLE);
            linear_upc.setVisibility(View.GONE);

        } else {
            linear_city.setVisibility(View.GONE);
            linear_upc.setVisibility(View.VISIBLE);
        }*/

        Log.e("asfasfasfafasfa", PreferenceManager.getUSerPhone(ConfirmationLedgerActivity.this));
        radio_city_jan_june.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {

                    String strSplit[] = radio_city_jan_june.getText().toString().split("-");
                    strConfirmYear = strSplit[4];
                    Log.e("sdgsdgsgs", strConfirmYear);

                    radio_city_jul_dec.setChecked(false);
                    radio_upc_dec_may.setChecked(false);
                    radio_upc_jun_nov.setChecked(false);
                    strFromToDate = "1to6";

                    strDate1 = strConfirmYear + "-01-01";
                    strDate2 = strConfirmYear + "-06-30";
                    if (PreferenceManager.getPrefUserType(ConfirmationLedgerActivity.this).equals("AG")) {
                        getPeriodLedgerData(strDate1, strDate2);
                    } else {
                        getPeriodLedgerData(strDate1, strDate2);
                    }

                }
            }
        });

        radio_city_jul_dec.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {

                    String strSplit[] = radio_city_jul_dec.getText().toString().split("-");
                    strConfirmYear = strSplit[4];
                    Log.e("sdgsdgsgs", strConfirmYear);

                    radio_city_jan_june.setChecked(false);
                    radio_upc_dec_may.setChecked(false);
                    radio_upc_jun_nov.setChecked(false);
                    strFromToDate = "7to12";
                   /* int getYear = Integer.parseInt(strYear);
                    int getYearMinusOner = getYear - 1;
                    strYear = String.valueOf(getYearMinusOner);*/

                   // strDate1 = strYear + "-07-01";
                   // strDate2 = strYear + "-12-31";

                    strDate1 = strConfirmYear + "-07-01";
                    strDate2 = strConfirmYear + "-12-31";
                    if (PreferenceManager.getPrefUserType(ConfirmationLedgerActivity.this).equals("AG")) {
                        getPeriodLedgerData(strDate1, strDate2);
                    } else {
                        getPeriodLedgerData(strDate1, strDate2);
                    }

                }
            }
        });


        radio_upc_dec_may.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    String strSplit[] = radio_upc_dec_may.getText().toString().split("-");
                    strConfirmYear = strSplit[4];
                    Log.e("sdgsdgsgs", strConfirmYear);

                    radio_city_jan_june.setChecked(false);
                    radio_city_jul_dec.setChecked(false);
                    radio_upc_jun_nov.setChecked(false);
                    strFromToDate = "12to5";

                    Calendar calendar = Calendar.getInstance();
                    int intY = calendar.get(Calendar.YEAR);
                    strYear = String.valueOf(intY);
                    String strYearMay = String.valueOf(intY);


                    int getYear = Integer.parseInt(strYear);
                    int getYearMinusOner = getYear - 1;
                    strYear = String.valueOf(getYearMinusOner);

                    strDate1 = strYear + "-12-01";
                    strDate2 = strYearMay + "-05-31";

                    if (PreferenceManager.getPrefUserType(ConfirmationLedgerActivity.this).equals("AG")) {
                        getPeriodLedgerData(strDate1, strDate2);
                    } else {
                        getPeriodLedgerData(strDate1, strDate2);
                    }
                }
            }
        });


        radio_upc_jun_nov.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {
                    String strSplit1[] = radio_upc_jun_nov.getText().toString().split("-");
                    strConfirmYear = strSplit1[4];
                    Log.e("sdgsdgsgs", strConfirmYear);
                    radio_city_jan_june.setChecked(false);
                    radio_city_jul_dec.setChecked(false);
                    radio_upc_dec_may.setChecked(false);
                    strFromToDate = "6to11";

                    Calendar calendar = Calendar.getInstance();
                    int intY = calendar.get(Calendar.YEAR);
                    strYear = String.valueOf(intY);

                    Date c = Calendar.getInstance().getTime();
                    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
                    String formattedDate = df.format(c);
                    String strSplit[] = formattedDate.split("-");
                    strGetMonth = Integer.parseInt(strSplit[1]);


                    strDate1 = strYear + "-06-01";
                    strDate2 = strYear + "-11-30";
                    if (PreferenceManager.getPrefUserType(ConfirmationLedgerActivity.this).equals("AG")) {
                        getPeriodLedgerData(strDate1, strDate2);
                    } else {
                        getPeriodLedgerData(strDate1, strDate2);
                    }
                }
            }
        });


        mBinding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });


        mBinding.txtConfirmInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPopup(strYear, strFromToDate, strAgencyId); /* Confirm*/

            }
        });


        mBinding.rlInvoice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                downloadPdfFile();
            }
        });

        mBinding.linearPDF.setVisibility(View.GONE);
        Log.e("CheckingLog", strUserCityUpc);


        if (PreferenceManager.getPrefUserType(ConfirmationLedgerActivity.this).equals("AG")) {
            mBinding.txtConfirmInvoice.setVisibility(View.VISIBLE);
        } else {
            mBinding.txtConfirmInvoice.setVisibility(View.GONE);
        }

        getRadioButtonDates();

    }


    public void downloadPdfFile() {


        Drawable drawableLogo = getResources().getDrawable(R.drawable.ledger_logo);
        if (mData != null && mData.size() > 0) {
            // createdFile = SystemUtility.createLedgerPdf(this, mData, opBalance, closeBal, agencyName, mBinding.edFromDate.getText().toString(), mBinding.edToDate.getText().toString(),drawableLogo,strAgencyId);

            if (PreferenceManager.getPrefUserType(ConfirmationLedgerActivity.this).equals("AG")) {
                createdFile = SystemUtility.createLedgerPdfTest(this, mData, opBalance, closeBal, strAgentName, strDate1, strDate2, drawableLogo, agentId.isEmpty() ? PreferenceManager.getAgentId(this) : agentId, strUnitName, strUnitAdd);
                //getLedgerData(agentId.isEmpty() ? PreferenceManager.getAgentId(this) : agentId, SystemUtility.getFormatedCalenderDay(fromDate), SystemUtility.getFormatedCalenderDay(toDate), "*");
            } else {
                createdFile = SystemUtility.createLedgerPdfTest(this, mData, opBalance, closeBal, strAgentName, strDate1, strDate2, drawableLogo, agentId.isEmpty() ? PreferenceManager.getAgentId(this) : agentId, strUnitName, strUnitAdd);
            }

            loadProgressBar(null, "Please wait..", false);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    dismissProgressBar();
                    AlertUtility.showAlertFileOpen(ConfirmationLedgerActivity.this, String.format(getResources().getString(R.string.str_excel_download_success), " InternalStorage/Samriddhi/CopiesData "), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            openPdf();
                        }
                    });
                }
            }, 3000);
            //onInfo(String.format(getResources().getString(R.string.str_excel_download_success), " InternalStorage/Samriddhi/LedgerData "));
        }


    }


    private void openPdf() {

        Log.e("sddssgs", "FILE" + " " + createdFile.toString());
        Uri path = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".fileprovider", createdFile);
        Log.e("create pdf uri path==>", "" + path);
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(path, "application/pdf");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            Log.e("sdgdsdgsg", e.getMessage());
            Toast.makeText(getApplicationContext(), "There is no any PDF Viewer", Toast.LENGTH_SHORT).show();
        }
    }


    public void getLedgerStatus(String strYear, String strFromToDate, String strAgencyId) {

        enableLoadingBar(true);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("bp_code", strAgencyId);
            jsonObject.put("from_to", strFromToDate);
            jsonObject.put("year", strConfirmYear);
            Log.e("wrwetwtwetwetwetwe", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }


        /*   {"bp_code":"10011137","from_to":"12to5","year":"2020"}    */

        AndroidNetworking.post(Constant.BASE_PORTAL_URL + "ledger/getLedgerConfirm")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("wrwetwtwetwetwetwe", response.toString());
                        try {

                            enableLoadingBar(false);

                            if (response.getString("confirmation_flag").equals("true")) {
                                // showPopup(strYear, strFromToDate, strAgencyId); /* Confirm*/
                                mBinding.txtConfirmInvoice.setVisibility(View.VISIBLE);
                            } else {
                                mBinding.txtConfirmInvoice.setVisibility(View.GONE);
                                alertMessage("You have already confirm this Ledger."); /*Already Confirmed*/
                            }
                        } catch (JSONException e) {
                            enableLoadingBar(false);
                            alertMessage(e.getMessage());
                            Log.e("uehkushkghd", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        enableLoadingBar(false);
                        alertMessage("Something went wrong");
                        //  Log.e("uehkushkghd", anError.getErrorBody());
                        Log.e("uehkushkghd", anError.getMessage());

                    }
                });
    }


    private void alertMessage(String message) {

        AlertUtility.showFormAlert(ConfirmationLedgerActivity.this, message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
    }

    private void showPopup(String strYear, String strFromToDate, String strAgencyId) {

        AlertDialog.Builder alert = new AlertDialog.Builder(ConfirmationLedgerActivity.this);
        alert.setMessage("Are you sure you want to confirm the ledger ?")
                .setPositiveButton("Confirm", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {
                        popupOtpWindow("1", strYear, strFromToDate, strAgencyId);
                        //ConfirmLedger("1", strYear, strFromToDate, strAgencyId); /*Api call to confirm ledger*/
                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                popupOtpWindow("0", strYear, strFromToDate, strAgencyId);
                //ConfirmLedger("0", strYear, strFromToDate, strAgencyId); /*Api call to cancel ledger*/
            }
        });

        AlertDialog alert1 = alert.create();
        alert1.show();
        //alert1.setCancelable(false);
    }


    public void popupOtpWindow(String status, String strYear, String strFromToDate, String strAgencyId) {


        View popupView = null;
        //popupWindow.dismiss();

        popupView = LayoutInflater.from(ConfirmationLedgerActivity.this).inflate(R.layout.bill_confirmation_otp, null);


        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0);


        TextView sendOtp = popupView.findViewById(R.id.sendOtp);
        Spannable wordOne = new SpannableString("Check OTP on this ");
        wordOne.setSpan(new ForegroundColorSpan(Color.BLACK), 0, wordOne.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        sendOtp.setText(wordOne);

        Spannable wordTwo = new SpannableString(PreferenceManager.getUSerPhone(ConfirmationLedgerActivity.this));
        wordTwo.setSpan(new ForegroundColorSpan(Color.RED), 0, wordTwo.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        sendOtp.append(wordTwo);

        Spannable wordThree = new SpannableString(" number");
        wordThree.setSpan(new ForegroundColorSpan(Color.BLACK), 0, wordThree.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        sendOtp.append(wordThree);


        LinearLayout linearResend = popupView.findViewById(R.id.linearResend);
        TextView sunmit_button = popupView.findViewById(R.id.otp_submit);
        ed1 = popupView.findViewById(R.id.et1);
        ed1.requestFocus();
        ed1.setFocusable(true);


        ed2 = popupView.findViewById(R.id.et2);
        ed3 = popupView.findViewById(R.id.et3);
        ed4 = popupView.findViewById(R.id.et4);
        ImageView img = popupView.findViewById(R.id.close);

        Log.e("dfjkjgdkgd", PreferenceManager.getUSerPhone(ConfirmationLedgerActivity.this));

        generateOTP(PreferenceManager.getUSerPhone(ConfirmationLedgerActivity.this));

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

        ed1.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {


            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

                if (s != null) {
                    ed2.requestFocus();
                }
            }
        });

        ed2.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {


            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

                if (s != null) {
                    ed3.requestFocus();
                }
            }
        });

        ed3.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {


            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

                if (s != null) {
                    ed4.requestFocus();
                }
            }
        });


        linearResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                generateOTP(PreferenceManager.getUSerPhone(ConfirmationLedgerActivity.this));
            }
        });
        sunmit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ed1.getText().toString().length() == 0) {
                    alertMessage("Please enter otp number ");
                    return;
                }
                if (ed2.getText().toString().length() == 0) {
                    alertMessage("Please enter otp number ");
                    return;
                }
                if (ed3.getText().toString().length() == 0) {
                    alertMessage("Please enter otp number ");
                    return;
                }
                if (ed4.getText().toString().length() == 0) {
                    alertMessage("Please enter otp number ");
                    return;
                }
                String enterOtp = ed1.getText().toString() + ed2.getText().toString() + ed3.getText().toString() + ed4.getText().toString();
                strOTP = enterOtp;
                VerifyOTP(status, strYear, strFromToDate, strAgencyId);
            }
        });
    }


    public void generateOTP(String uSerPhone) {
        Log.e("sfsdfsdfsfsfs" + " Mobile", uSerPhone);
        enableLoadingBar(true);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", PreferenceManager.getAgentId(ConfirmationLedgerActivity.this));
            jsonObject.put("otp_for", "LedgerConfirmation");
            Log.e("swgdgsgsg", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(Constant.BASE_PORTAL_URL + "otp/api/send")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .addJSONObjectBody(jsonObject)
                .setTag("Request")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            Log.e("swgdgsgsg", response.toString());

                            if (response.getString("message").equals("OTP Sent successfully & It is valid for 1 hour.")) {
                                enableLoadingBar(false);
                                JSONObject jsonObject1 = new JSONObject(response.getString("data"));
                                strOTPId = jsonObject1.getString("otp_id");
                                strBpCode = jsonObject1.getString("user_id");
                                Toast.makeText(ConfirmationLedgerActivity.this, "OTP send Successfully", Toast.LENGTH_SHORT).show();
                            } else {
                                enableLoadingBar(false);
                                alertMessage(response.getString("message"));
                                return;
                            }
                        } catch (Exception ex) {
                            Log.e("swgdgsgsg", ex.getMessage());
                            enableLoadingBar(false);
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        enableLoadingBar(false);
                    }
                });


    }

    public void VerifyOTP(String status, String strYear, String strFromToDate, String strAgencyId) {

        enableLoadingBar(true);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("otp_id", strOTPId);
            jsonObject.put("otp", strOTP);
            jsonObject.put("user_id", PreferenceManager.getAgentId(ConfirmationLedgerActivity.this));
            Log.e("swgdgsgsg", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(Constant.BASE_PORTAL_URL + "otp/api/verify")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .addJSONObjectBody(jsonObject)
                .setTag("Request")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            Log.e("swgdgsgsg", response.toString());

                            if (response.getString("message").equals("OTP verified successfully.")) {


                                ConfirmLedger(status, strYear, strFromToDate, strAgencyId); /*Api call to confirm ledger*/

                                enableLoadingBar(false);
                                popupWindow.dismiss();
                            } else {
                                enableLoadingBar(false);
                                alertMessage("Invalid OTP");
                                return;
                            }
                        } catch (Exception ex) {
                            Log.e("swgdgsgsg", ex.getMessage());
                            enableLoadingBar(false);
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        enableLoadingBar(false);
                    }
                });

    }


    public void ConfirmLedger(String status, String strYear, String strFromToDate, String strAgencyId) {

        int confirmstatus = Integer.parseInt(status);
        enableLoadingBar(true);
        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put("bp_code", strAgencyId);
            jsonObject.put("from_to", strFromToDate);
            // jsonObject.put("year", strYear);
            jsonObject.put("year", strConfirmYear);

            if (strUserCityUpc.equals("CRL") || strUserCityUpc.equals("CITY") || strUserCityUpc.equals("City")) {
                jsonObject.put("city_upc", "City");

            } else {
                jsonObject.put("city_upc", "UPC");
            }

            jsonObject.put("ledger_amount", Double.parseDouble(strClosingBalnc));  /*closing balance need to put here*/
            jsonObject.put("device_id", SystemUtility.getDeviceId(ConfirmationLedgerActivity.this));
            jsonObject.put("confirm_status", confirmstatus);
            Log.e("uehkushkghd", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(Constant.BASE_PORTAL_URL + "ledger/LedgerConfirm")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            enableLoadingBar(false);
                            Log.e("asfasfasfafaf", response.toString());
                            if (response.getString("success").equals("true")) {
                                alertMessage(response.getString("message"));
                                mBinding.txtConfirmInvoice.setVisibility(View.GONE);
                            } else {
                                alertMessage(response.getString("message"));
                            }
                        } catch (JSONException e) {
                            enableLoadingBar(false);
                            alertMessage(e.getMessage());
                            Log.e("uehkushkghd", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        enableLoadingBar(false);
                        alertMessage("Something went wrong");
                        Log.e("uehkushkghd", anError.getErrorBody());

                    }
                });
    }

    private void getPeriodLedgerData(String formatedCalenderDay, String formatedCalenderDay1) {
        Log.e("sgdgsgsgsg", formatedCalenderDay);
        Log.e("sgdgsgsgsg", formatedCalenderDay1);
        createdFile = null;

        enableLoadingBar(true);
        newLedgerModelList = new ArrayList<>();
        Log.e("LedgerURL", Constant.BASE_PORTAL_URL + "Ledger_data_new/" + strAgencyId + "/" + formatedCalenderDay + "/" + formatedCalenderDay1 + "/*");
        AndroidNetworking.get(Constant.BASE_PORTAL_URL + "Ledger_data_new/" + strAgencyId + "/" + formatedCalenderDay + "/" + formatedCalenderDay1 + "/*")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            enableLoadingBar(false);
                            Log.e("sgdgsgsgsg", response.toString());
                            if (response.getString("data") != null && !response.getString("data").isEmpty() && !response.getString("data").equals("null")) {
                                JSONObject objData = new JSONObject(response.getString("data"));
                                JSONArray jsonArray = objData.getJSONArray("results");
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject objResult = jsonArray.getJSONObject(i);
                                    stropeningBal = objResult.getString("OpenBalance");
                                    strAgentName = objResult.getString("Name1");

                                    strUnitName = objResult.getString("unit_name");
                                    strUnitAdd = objResult.getString("unit_address");
                                    strLogoStatus = objResult.getString("state_flag");

                                    agencyName = strAgentName;

                                    JSONObject objLedgerItem = new JSONObject(objResult.getString("LedgerItemSet"));
                                    JSONArray jsonArrayresults = objLedgerItem.getJSONArray("results");
                                    for (int j = 0; j < jsonArrayresults.length(); j++) {
                                        JSONObject jsonObjectResult = jsonArrayresults.getJSONObject(j);
                                        NewLedgerModel newLedgerModel = new NewLedgerModel();
                                        newLedgerModel.setVDate(SystemUtility.getLedgerFormateDate(jsonObjectResult.getString("VDate")));
                                        if (jsonObjectResult.getString("Narration").contains("-")) {

                                            String strSplit[] = jsonObjectResult.getString("Narration").split("-");
                                            newLedgerModel.setNarration(strSplit[0]);

                                        } else {
                                            newLedgerModel.setNarration(jsonObjectResult.getString("Narration"));
                                        }

                                        double debit = Double.parseDouble(jsonObjectResult.getString("Debit"));
                                        debit = Double.parseDouble(new DecimalFormat("##.##").format(debit));
                                        newLedgerModel.setDebit(String.valueOf(debit));

                                        double credit = Double.parseDouble(jsonObjectResult.getString("Credit").replace("-", ""));
                                        credit = Double.parseDouble(new DecimalFormat("##.##").format(credit));
                                        newLedgerModel.setCredit(String.valueOf(credit));

                                        double balnc = Double.parseDouble(jsonObjectResult.getString("Balance"));
                                        balnc = Double.parseDouble(new DecimalFormat("##.##").format(balnc));
                                        newLedgerModel.setBalance(String.valueOf(balnc));

                                        strClosingBalnc = jsonObjectResult.getString("Balance");
                                        newLedgerModelList.add(newLedgerModel);
                                    }


                                    // closeBal = SystemUtility.getFormatDecimalValue(Float.parseFloat(strClosingBalnc));
                                    //  opBalance = SystemUtility.getFormatDecimalValue(Float.parseFloat(stropeningBal));

                                    double closingBalnc = Double.parseDouble(strClosingBalnc);
                                    closingBalnc = Double.parseDouble(new DecimalFormat("##.##").format(closingBalnc));
                                    mBinding.setCloseBilling(String.valueOf(closingBalnc));
                                    closeBal = String.valueOf(closingBalnc);


                                    double OpenBalnc = Double.parseDouble(stropeningBal);
                                    OpenBalnc = Double.parseDouble(new DecimalFormat("##.##").format(OpenBalnc));
                                    mBinding.setOpeningBilling(String.valueOf(OpenBalnc));
                                    opBalance = String.valueOf(OpenBalnc);

                                    if (newLedgerModelList != null && newLedgerModelList.size() > 0) {
                                        mData = newLedgerModelList;
                                        mBinding.linearPDF.setVisibility(View.VISIBLE);
                                        onLoadPDF();
                                    } else {
                                        ConfirmationLedgerActivity.this.onInfo(getResources().getString(R.string.str_no_trans_found));
                                        mData = null;
                                        mBinding.linearPDF.setVisibility(View.GONE);
                                    }

                                }
                            } else {
                                Toast.makeText(ConfirmationLedgerActivity.this, "Data not found", Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            enableLoadingBar(false);
                            Log.e("sdgsgsgssgs", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("sjdfhskfsd", anError.getMessage());
                        enableLoadingBar(false);
                    }
                });
    }


    public void onLoadPDF() {
        Drawable drawableLogo;

        if (strLogoStatus.equals("1")) {
            drawableLogo = getResources().getDrawable(R.drawable.ledger_logo);
        } else if (strLogoStatus.equals("2")) {
            drawableLogo = getResources().getDrawable(R.drawable.db_gujrati_ledger);
        }
        else {
            drawableLogo = getResources().getDrawable(R.drawable.db_marathi_ledger);
        }


        if (mData != null && mData.size() > 0) {

            // createdFile = SystemUtility.createLedgerPdf(this, mData, opBalance, closeBal, agencyName, mBinding.edFromDate.getText().toString(), mBinding.edToDate.getText().toString(),drawableLogo,strAgencyId);

            if (PreferenceManager.getPrefUserType(ConfirmationLedgerActivity.this).equals("AG")) {

                createdFile = SystemUtility.createLedgerPdfTest(this, mData, opBalance, closeBal, strAgentName, strDate1, strDate2, drawableLogo, agentId.isEmpty() ? PreferenceManager.getAgentId(this) : agentId, strUnitName, strUnitAdd);
                //getLedgerData(agentId.isEmpty() ? PreferenceManager.getAgentId(this) : agentId, SystemUtility.getFormatedCalenderDay(fromDate), SystemUtility.getFormatedCalenderDay(toDate), "*");
            } else {

                createdFile = SystemUtility.createLedgerPdfTest(this, mData, opBalance, closeBal, strAgentName, strDate1, strDate2, drawableLogo, strAgencyId, strUnitName, strUnitAdd);
            }

            Log.e("sdfsdfsds", createdFile.toString());
            mBinding.pdfView.fromFile(createdFile).load();

            if (PreferenceManager.getPrefUserType(ConfirmationLedgerActivity.this).equals("AG")) {
                getLedgerStatus(strYear, strFromToDate, strAgencyId);
            }
        }
    }


    private void getRadioButtonDates() {
        enableLoadingBar(true);
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault());
        String strcurrentDate = df.format(c);
        String AgenctType = "";
        if (strUserCityUpc.equals("CRL") || strUserCityUpc.equals("CITY") || strUserCityUpc.equals("City")) {
            AgenctType = "City";

        } else {
            AgenctType = "UPC";
        }

        Log.e("etewetwtwtwtwtwt", strcurrentDate);
        Log.e("etewetwtwtwtwtwt", AgenctType);

        AndroidNetworking.get(Constant.BASE_PORTAL_URL + "ledger/api/ledger-year-label/" + strcurrentDate + "/" + AgenctType)
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            enableLoadingBar(false);
                            Log.e("etewetwtwtwtwtwt", response.toString());
                            if (response.getString("data") != null && !response.getString("data").isEmpty() && !response.getString("data").equals("null")) {
                                JSONArray jsonArray = new JSONArray(response.getString("data"));

                                if (strUserCityUpc.equals("CRL") || strUserCityUpc.equals("CITY") || strUserCityUpc.equals("City")) {
                                    linear_city.setVisibility(View.VISIBLE);
                                    linear_upc.setVisibility(View.GONE);
                                    Log.e("sgdgsdgsdgs", jsonArray.length() + "");

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject objResult = jsonArray.getJSONObject(i);
                                        if (i == 0) {
                                            radio_city_jul_dec.setText(objResult.getString("from") + " To " + objResult.getString("to"));
                                        } else {
                                            radio_city_jan_june.setText(objResult.getString("from") + " To " + objResult.getString("to"));
                                        }

                                    }

                                } else {
                                    Log.e("sgdgsdgsdgs", jsonArray.length() + "");
                                    linear_city.setVisibility(View.GONE);
                                    linear_upc.setVisibility(View.VISIBLE);

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject objResult = jsonArray.getJSONObject(i);
                                        if (i == 0) {

                                            radio_upc_dec_may.setText(objResult.getString("from") + " To " + objResult.getString("to"));
                                        } else {
                                            radio_upc_jun_nov.setText(objResult.getString("from") + " To " + objResult.getString("to"));
                                        }

                                    }

                                }


                            } else {
                                Toast.makeText(ConfirmationLedgerActivity.this, "Ledger Dates not found", Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            enableLoadingBar(false);
                            Log.e("etewetwtwtwtwtwt", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("etewetwtwtwtwtwt", anError.getMessage());
                        enableLoadingBar(false);
                    }
                });
    }
}