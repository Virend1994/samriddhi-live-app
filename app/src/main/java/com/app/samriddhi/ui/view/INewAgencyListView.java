package com.app.samriddhi.ui.view;

import com.app.samriddhi.ui.model.BankListModel;

import java.util.List;

public interface INewAgencyListView extends IView {

    void onSuccess(List<BankListModel> list);
}
