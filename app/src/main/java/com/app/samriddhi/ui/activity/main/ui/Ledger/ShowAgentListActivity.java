package com.app.samriddhi.ui.activity.main.ui.Ledger;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AppsContants;
import com.app.samriddhi.ui.activity.main.ui.LedgerActivity;
import com.app.samriddhi.util.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.Credentials;

public class ShowAgentListActivity extends BaseActivity {

    RecyclerView recyclerview;
    LinearLayoutManager linearLayoutManager;
    List<LedgerModal> ledgerModalList;
    AgentListAdapter agentListAdapter;
    TextView tvDate;
    int pos=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_list);

        ImageView imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        tvDate = findViewById(R.id.tvDate);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        tvDate.setText(dateFormat.format(cal.getTime()));

        recyclerview = findViewById(R.id.recyclerview);
        recyclerview.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(ShowAgentListActivity.this);
        recyclerview.setLayoutManager(linearLayoutManager);
        getAgencyList();


    }

    public void startActivityAnimation(Context context, Class destinationClass, boolean addFlags) {
        if (addFlags)
            startActivity(new Intent(context, destinationClass).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        else
            startActivity(new Intent(context, destinationClass));
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }


    public void getAgencyList() {
        ledgerModalList = new ArrayList<>();
        enableLoadingBar(true);

        String strUserId= PreferenceManager.getAgentId(ShowAgentListActivity.this);
        AppsContants.sharedpreferences =getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strStateCode = AppsContants.sharedpreferences.getString(AppsContants.stateCode, "");
        String strUnitCode = AppsContants.sharedpreferences.getString(AppsContants.unitCode, "");
        String strCityUpc = AppsContants.sharedpreferences.getString(AppsContants.cityupc, "");
        String strMarketCode = AppsContants.sharedpreferences.getString(AppsContants.MarketCode, "");
        String strCityCode = AppsContants.sharedpreferences.getString(AppsContants.CityCode, "");

        System.out.println("APICALL"+ Constant.BASE_PORTAL_URL+"ledger/api/all-in-one/"+strUserId+"?"+"s="+strStateCode+"&u="+strUnitCode+"&cu="+strMarketCode+"&cc="+strCityUpc.toLowerCase()+"&sd="+strCityCode.toUpperCase());

        String strUrl="";

        if (PreferenceManager.getPrefUserType(ShowAgentListActivity.this).equals("EX") || PreferenceManager.getPrefUserType(ShowAgentListActivity.this).equals("CI")) {
            strUrl=Constant.BASE_PORTAL_URL+"ledger/api/all-in-one/"+strUserId;
        } else {
            strUrl=Constant.BASE_PORTAL_URL+"ledger/api/all-in-one/"+strUserId+"?"+"s="+strStateCode+"&u="+strUnitCode+"&cu="+strMarketCode+"&cc="+strCityUpc.toLowerCase()+"&sd="+strCityCode.toUpperCase();
        }

        Log.e("afagasfaga",strUrl);
        AndroidNetworking.get(strUrl)
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (PreferenceManager.getPrefUserType(ShowAgentListActivity.this).equals("EX") || PreferenceManager.getPrefUserType(ShowAgentListActivity.this).equals("CI")) {
                                if(response.getString("reply").equals("Records are available.")){
                                    JSONObject jsonObjectMain=new JSONObject(response.getString("data"));
                                    JSONArray jsonArray=jsonObjectMain.getJSONArray("results");
                                    int pos=0;
                                    ledgerModalList = new ArrayList<>();
                                    for (int i = 0; i <jsonArray.length(); i++) {
                                        pos++;
                                        LedgerModal ledgerModal = new LedgerModal();
                                        JSONObject jsonObject=jsonArray.getJSONObject(i);
                                        ledgerModal.setSerial_num(pos+"");
                                        ledgerModal.setAgent_code(jsonObject.getString("SoldToParty"));
                                        ledgerModal.setAgent_name(jsonObject.getString("SoldToPartyName"));
                                        ledgerModal.setAgent_amt(jsonObject.getString("ledger"));
                                        ledgerModalList.add(ledgerModal);
                                    }
                                    agentListAdapter=new AgentListAdapter(ledgerModalList,ShowAgentListActivity.this);
                                    recyclerview.setAdapter(agentListAdapter);
                                    enableLoadingBar(false);
                                }
                            }
                            else {
                                if(response.getString("message").equals("Records are available.")){
                                    JSONObject jsonObjectMain=new JSONObject(response.getString("data"));
                                    JSONArray jsonArray=jsonObjectMain.getJSONArray("results");
                                    int pos=0;
                                    ledgerModalList = new ArrayList<>();
                                    for (int i = 0; i <jsonArray.length(); i++) {
                                        pos++;
                                        LedgerModal ledgerModal = new LedgerModal();
                                        JSONObject jsonObject=jsonArray.getJSONObject(i);
                                        ledgerModal.setSerial_num(pos+"");
                                        ledgerModal.setAgent_code(jsonObject.getString("SoldToParty"));
                                        ledgerModal.setAgent_name(jsonObject.getString("SoldToPartyName"));
                                        ledgerModal.setAgent_amt(jsonObject.getString("ledger"));
                                        ledgerModalList.add(ledgerModal);
                                    }
                                    agentListAdapter=new AgentListAdapter(ledgerModalList,ShowAgentListActivity.this);
                                    recyclerview.setAdapter(agentListAdapter);
                                    enableLoadingBar(false);
                                }

                            }


                        }
                        catch (Exception ex){
                            enableLoadingBar(false);
                            Log.e("Sdfsdfs",ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        enableLoadingBar(false);
                        Log.e("Sdfsdfs",anError.getErrorBody());

                    }
                });
    }
}