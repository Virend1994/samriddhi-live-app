package com.app.samriddhi.ui.activity.main.ui.Grievance;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.samriddhi.R;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.google.android.material.tabs.TabLayout;

public class RequestDetailTabActivity extends AppCompatActivity {
    TabLayout tablayout;
    ViewPager viewPager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_detail_tab);
        ImageView imgBack=findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tablayout=findViewById(R.id.tablayout);
        viewPager=findViewById(R.id.viewPager);

        tablayout.addTab(tablayout.newTab().setText(R.string.open));
        tablayout.addTab(tablayout.newTab().setText(R.string.close));

        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText(R.string.open);
        tablayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText(R.string.close);
        tablayout.getTabAt(1).setCustomView(tabTwo);


        final RequestDetailTabAdapter adapterTablayout = new RequestDetailTabAdapter(this.getSupportFragmentManager(), tablayout.getTabCount());
        viewPager.setAdapter(adapterTablayout);

        TextView txtCategories=findViewById(R.id.txtCategories);


        if(PreferenceManager.getAppLang(RequestDetailTabActivity.this).equals("hi")) {

            tabOne.setText("शिकायतं");
            tabTwo.setText("सवाल");
            txtCategories.setText("विवरण");

        }


        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tablayout));
        tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
}