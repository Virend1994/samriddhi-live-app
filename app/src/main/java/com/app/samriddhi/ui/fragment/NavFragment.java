package com.app.samriddhi.ui.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.BaseFragment;
import com.app.samriddhi.databinding.FragmentNavBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.DrawerAdapter;
import com.app.samriddhi.ui.activity.auth.LoginActivity;
import com.app.samriddhi.ui.activity.main.ui.ActivityGrievance;
import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AgencyCloseActivity;
import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AgencyCloseStausActivity;
import com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule.ShowRequestListActivity;
import com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule.newRquestActivity;
import com.app.samriddhi.ui.activity.main.ui.AgentChildSelectionActivity;
import com.app.samriddhi.ui.activity.main.ui.AgentListActivity;
import com.app.samriddhi.ui.activity.main.ui.BillOutAgentListActivity;
import com.app.samriddhi.ui.activity.main.ui.BillingActivity;
import com.app.samriddhi.ui.activity.main.ui.ContactUsActivity;
import com.app.samriddhi.ui.activity.main.ui.GraphActivity;
import com.app.samriddhi.ui.activity.main.ui.Grievance.GrievanceTabActivity;
import com.app.samriddhi.ui.activity.main.ui.Grievance.HelpSupportStatus;
import com.app.samriddhi.ui.activity.main.ui.Grievance.RequestDetailTabActivity;
import com.app.samriddhi.ui.activity.main.ui.Grievance.SubmitRequestPopup;
import com.app.samriddhi.ui.activity.main.ui.HomeActivity;
import com.app.samriddhi.ui.activity.main.ui.LanguageActivity;
import com.app.samriddhi.ui.activity.main.ui.Ledger.ShowAgentListActivity;
import com.app.samriddhi.ui.activity.main.ui.Ledger.StatesActivity;
import com.app.samriddhi.ui.activity.main.ui.LedgerActivity;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.DailyPoActivity;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.OrderSupply;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.PendingOrdersActivity;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.UohOrderSupply;
import com.app.samriddhi.ui.activity.main.ui.ProfileActivity;
import com.app.samriddhi.ui.activity.main.ui.UserCityActivity;
import com.app.samriddhi.ui.activity.main.ui.UserStateActivity;
import com.app.samriddhi.ui.model.DrawerModel;
import com.app.samriddhi.ui.model.LogoutReqModel;
import com.app.samriddhi.ui.model.LogoutResultModel;
import com.app.samriddhi.ui.presenter.UserLogoutPresenter;
import com.app.samriddhi.ui.presenter.UserTypeCopiesDetailPresenter;
import com.app.samriddhi.ui.view.IUserLogoutView;
import com.app.samriddhi.util.AlertUtility;
import com.app.samriddhi.util.Constant;
import com.google.gson.Gson;

import java.util.ArrayList;

public class NavFragment extends BaseFragment implements DrawerAdapter.ItemClickListener, IUserLogoutView {
    FragmentNavBinding mBinding;
    ArrayList<DrawerModel> arrDrawerData = new ArrayList<>();
    String[] arrItems = new String[]{};
    UserLogoutPresenter mPresenter;

    public static RecyclerView recycle_nav;

    public NavFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_nav, container, false);
        mBinding.recycleNav.setLayoutManager(new LinearLayoutManager(getActivity()));
        initDrawer();
        mBinding.setFragment(this);
        mPresenter = new UserLogoutPresenter();
        mPresenter.setView(this);
        return mBinding.getRoot();
    }

    /**
     * initialize drawer menus
     */
    private void initDrawer() {
        recycle_nav = mBinding.getRoot().findViewById(R.id.recycle_nav);
        String currentVersion = "";
        mBinding.txtLocation.setText("Access Type - " + PreferenceManager.getPrefUserType(getActivity()));

        try {
            currentVersion = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        mBinding.txtVersion.setText("Version " + currentVersion + " " + "Copy Right (2021)");
        arrItems = (getActivity().getResources().getStringArray(R.array.arr_nav_items));
        TypedArray imgs = getResources().obtainTypedArray(R.array.arr_nav_drawables);
        for (int i = 0; i < arrItems.length; i++) {

            Log.e("sdgsdgsg", arrItems[i]);
            if (arrItems[i].equalsIgnoreCase(getString(R.string.agent_selection))) {
                if (PreferenceManager.getPrefAgentParentValue(getActivity()).equalsIgnoreCase("Y")) {

                    if (PreferenceManager.getPrefUserType(getActivity()).equals("EX")
                            || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CE")
                            || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UE")
                            || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CI")) { /* Do not show to executive */

                        if (!arrItems[i].equals("Approval/Pending") && !arrItems[i].equals("Help/Support")
                                && !arrItems[i].equals("Help Support Status") && !arrItems[i].equals("New Agency Proposed") && !arrItems[i].equals(getString(R.string.str_graph))) {
                            arrDrawerData.add(new DrawerModel(arrItems[i], imgs.getResourceId(i, -1)));
                        }
                    } else if (PreferenceManager.getPrefUserType(getActivity()).equals("AG")) { /* Do not show to Agent */
                        if (!arrItems[i].equals("Approval/Pending") && !arrItems[i].equals("New Agency Proposed")
                                && !arrItems[i].equals("New Agency Request") && !arrItems[i].equals("Agency Close Request")
                                && !arrItems[i].equals("Daily Supply and Hold")
                                && !arrItems[i].equals("Pending Agency")
                                && !arrItems[i].equals(getString(R.string.str_graph))) {
                            arrDrawerData.add(new DrawerModel(arrItems[i], imgs.getResourceId(i, -1)));
                        }
                    } else if (PreferenceManager.getPrefUserType(getActivity()).equals("UH")) { /* Do not show to Agent */
                        if (!arrItems[i].equals("Help/Support") && !arrItems[i].equals("Help Support Status")) {
                            arrDrawerData.add(new DrawerModel(arrItems[i], imgs.getResourceId(i, -1)));
                        }
                    } else if (PreferenceManager.getPrefUserType(getActivity()).equals("SH")) { /* Do not show to Agent */
                        if (!arrItems[i].equals("New Agency Request") && !arrItems[i].equals("Pending Agency") &&
                                !arrItems[i].equals("Help/Support") && !arrItems[i].equals("Help Support Status")
                                && !arrItems[i].equals("Daily Supply and Hold")
                                && !arrItems[i].equals("Approval/Pending")) {
                            arrDrawerData.add(new DrawerModel(arrItems[i], imgs.getResourceId(i, -1)));
                        }
                    } else if (PreferenceManager.getPrefUserType(getActivity()).equals("HO") || PreferenceManager.getPrefUserType(getActivity()).equals("CO")) { /* Do not show to Agent */
                        if (!arrItems[i].equals("New Agency Request") && !arrItems[i].equals("Pending Agency") &&
                                !arrItems[i].equals("Help/Support") && !arrItems[i].equals("Help Support Status")
                                && !arrItems[i].equals("Daily Supply and Hold")
                                && !arrItems[i].equals("Approval/Pending")) {
                            arrDrawerData.add(new DrawerModel(arrItems[i], imgs.getResourceId(i, -1)));
                        }
                    } else if (PreferenceManager.getPrefUserType(getActivity()).equals("UF")) { /* Do not show to Agent */
                        if (!arrItems[i].equals("Approval/Pending")
                                && !arrItems[i].equals("New Agency Proposed")
                                && !arrItems[i].equals("New Agency Request")
                                && !arrItems[i].equals("Agency Close Request")
                                && !arrItems[i].equals("Daily Supply and Hold")
                                && !arrItems[i].equals("Pending Agency")
                                && !arrItems[i].equals("Approval/Pending")
                                && !arrItems[i].equals("Ledger")
                                && !arrItems[i].equals(getString(R.string.str_graph))
                                && !arrItems[i].equals("Help/Support") && !arrItems[i].equals("Help Support Status")) {
                            arrDrawerData.add(new DrawerModel(arrItems[i], imgs.getResourceId(i, -1)));
                        }
                    } else if (PreferenceManager.getPrefUserType(getActivity()).equals("CB")) { /* Do not show to Agent */
                        if (!arrItems[i].equals("Approval/Pending")
                                && !arrItems[i].equals("New Agency Proposed")
                                && !arrItems[i].equals("New Agency Request")
                                && !arrItems[i].equals("Agency Close Request")
                                && !arrItems[i].equals("Daily Supply and Hold")
                                && !arrItems[i].equals("Pending Agency")
                                && !arrItems[i].equals("Approval/Pending")
                                && !arrItems[i].equals("Ledger")
                                && !arrItems[i].equals(getString(R.string.str_graph))
                                && !arrItems[i].equals("Help/Support") && !arrItems[i].equals("Help Support Status")) {
                            arrDrawerData.add(new DrawerModel(arrItems[i], imgs.getResourceId(i, -1)));
                        }
                    } else {
                        arrDrawerData.add(new DrawerModel(arrItems[i], imgs.getResourceId(i, -1)));
                    }
                    Log.e("sdgsdgsg", "If");
                    //arrDrawerData.add(new DrawerModel(arrItems[i], imgs.getResourceId(i, -1)));
                } else {
                    continue;
                }
            } else {

                Log.e("sdgsdgsg", "Else");
                //arrDrawerData.add(new DrawerModel(arrItems[i], imgs.getResourceId(i, -1)));
                if (PreferenceManager.getPrefUserType(getActivity()).equals("EX")
                        || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CE")
                        || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UE")
                        || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CI")) { /* Do not show to executive */

                    if (!arrItems[i].equals("Approval/Pending") && !arrItems[i].equals("Help/Support")
                            && !arrItems[i].equals("Help Support Status") && !arrItems[i].equals("New Agency Proposed") && !arrItems[i].equals(getString(R.string.str_graph))) {
                        arrDrawerData.add(new DrawerModel(arrItems[i], imgs.getResourceId(i, -1)));
                    }
                } else if (PreferenceManager.getPrefUserType(getActivity()).equals("AG")) { /* Do not show to Agent */
                    if (!arrItems[i].equals("Approval/Pending") && !arrItems[i].equals("New Agency Proposed")
                            && !arrItems[i].equals("New Agency Request") && !arrItems[i].equals("Agency Close Request")
                            && !arrItems[i].equals("Daily Supply and Hold")
                            && !arrItems[i].equals(getString(R.string.str_graph))
                            && !arrItems[i].equals("Pending Agency")) {
                        arrDrawerData.add(new DrawerModel(arrItems[i], imgs.getResourceId(i, -1)));
                    }
                } else if (PreferenceManager.getPrefUserType(getActivity()).equals("UH")) { /* Do not show to Agent */
                    if (!arrItems[i].equals("Help/Support") && !arrItems[i].equals("Help Support Status")) {
                        arrDrawerData.add(new DrawerModel(arrItems[i], imgs.getResourceId(i, -1)));
                    }
                } else if (PreferenceManager.getPrefUserType(getActivity()).equals("SH")) { /* Do not show to Agent */
                    if (!arrItems[i].equals("New Agency Request") && !arrItems[i].equals("Pending Agency") &&
                            !arrItems[i].equals("Help/Support") && !arrItems[i].equals("Help Support Status")
                            && !arrItems[i].equals("Daily Supply and Hold")
                            && !arrItems[i].equals("Approval/Pending")) {
                        arrDrawerData.add(new DrawerModel(arrItems[i], imgs.getResourceId(i, -1)));
                    }
                } else if (PreferenceManager.getPrefUserType(getActivity()).equals("HO") || PreferenceManager.getPrefUserType(getActivity()).equals("CO")) { /* Do not show to Agent */
                    if (!arrItems[i].equals("New Agency Request")
                            && !arrItems[i].equals("Pending Agency") &&
                            !arrItems[i].equals("Help/Support") && !arrItems[i].equals("Help Support Status")
                            && !arrItems[i].equals("Daily Supply and Hold")
                            && !arrItems[i].equals("Approval/Pending")) {
                        arrDrawerData.add(new DrawerModel(arrItems[i], imgs.getResourceId(i, -1)));
                    }
                } else if (PreferenceManager.getPrefUserType(getActivity()).equals("UF")) { /* Do not show to Agent */
                    if (!arrItems[i].equals("Approval/Pending")
                            && !arrItems[i].equals("New Agency Proposed")
                            && !arrItems[i].equals("New Agency Request")
                            && !arrItems[i].equals("Agency Close Request")
                            && !arrItems[i].equals("Daily Supply and Hold")
                            && !arrItems[i].equals("Pending Agency")
                            && !arrItems[i].equals("Approval/Pending")
                            && !arrItems[i].equals("Ledger")
                            && !arrItems[i].equals(getString(R.string.str_graph))
                            && !arrItems[i].equals("Help/Support") && !arrItems[i].equals("Help Support Status")) {
                        arrDrawerData.add(new DrawerModel(arrItems[i], imgs.getResourceId(i, -1)));
                    }
                } else if (PreferenceManager.getPrefUserType(getActivity()).equals("CB")) { /* Do not show to Agent */
                    if (!arrItems[i].equals("Approval/Pending")
                            && !arrItems[i].equals("New Agency Proposed")
                            && !arrItems[i].equals("New Agency Request")
                            && !arrItems[i].equals("Agency Close Request")
                            && !arrItems[i].equals("Daily Supply and Hold")

                            && !arrItems[i].equals(getString(R.string.str_graph))
                            && !arrItems[i].equals("Pending Agency")
                            && !arrItems[i].equals("Approval/Pending")
                            && !arrItems[i].equals("Ledger")
                            && !arrItems[i].equals("Help/Support") && !arrItems[i].equals("Help Support Status")) {
                        arrDrawerData.add(new DrawerModel(arrItems[i], imgs.getResourceId(i, -1)));
                    }
                } else {
                    arrDrawerData.add(new DrawerModel(arrItems[i], imgs.getResourceId(i, -1)));
                }
            }
        }
        mBinding.recycleNav.setAdapter(new DrawerAdapter(getActivity(), arrDrawerData, this));
    }

    /**
     * handles the item click
     *
     * @param view
     */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.linear_close_drawer:
                ((HomeActivity) getActivity()).closeDrawer();
                break;
            case R.id.txt_logout:
                AlertUtility.showAlert(getActivity(), getResources().getString(R.string.str_logout_mag), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        LogoutReqModel logoutReqModel = new LogoutReqModel();
                        logoutReqModel.flag = 1;
                        logoutReqModel.user_id = PreferenceManager.getAgentId(getActivity());

                        Gson gson = new Gson();
                        String data1 = gson.toJson(logoutReqModel);
                        Log.e("asfgasgagasgaagasg", data1);

                        mPresenter.getLogoutStatus(logoutReqModel);

                    }
                });
                break;
            default:
                break;
        }
    }


    @Override
    public void onItemClick(int position) {
        if ((arrDrawerData.get(position).item_name).equalsIgnoreCase(getResources().getString(R.string.str_profile))) {
            showProfileAccordingToUser();

        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.agent_selection))) {
            ((HomeActivity) getActivity()).closeDrawer();
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), AgentChildSelectionActivity.class, false);

        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_dashboard))) {
            ((HomeActivity) getActivity()).closeDrawer();
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_billing_drawer))) {

            ((HomeActivity) getActivity()).closeDrawer();
            openBillingScreen();
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_outstanding_drawer))) {

            ((HomeActivity) getActivity()).closeDrawer();
            openOutstandingScreen();
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_ledger))) {
            ((HomeActivity) getActivity()).closeDrawer();
            openLedger();
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_language))) {
            ((HomeActivity) getActivity()).closeDrawer();
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), LanguageActivity.class, false);
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_contact_us))) {
            ((HomeActivity) getActivity()).closeDrawer();
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), ContactUsActivity.class, false);
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_open_agency))) {
            ((HomeActivity) getActivity()).closeDrawer();
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), newRquestActivity.class, false);
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_close_agency))) {
            ((HomeActivity) getActivity()).closeDrawer();
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), AgencyCloseStausActivity.class, false);
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_new_agency_perposed))) {
            ((HomeActivity) getActivity()).closeDrawer();

            PreferenceManager.setAgencyMenuType(getActivity(), "Proposed");
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), ShowRequestListActivity.class, false);
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_pending_agency))) {
            ((HomeActivity) getActivity()).closeDrawer();
            PreferenceManager.setAgencyMenuType(getActivity(), "Pending");
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), ShowRequestListActivity.class, false);
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_daily_sup_and_hold))) {
            ((HomeActivity) getActivity()).closeDrawer();
            // ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), OrderSupply.class, false);
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), DailyPoActivity.class, false);
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_approval))) {
            ((HomeActivity) getActivity()).closeDrawer();
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), PendingOrdersActivity.class, false);
            // ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), UohOrderSupply.class, false);
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_grievance))) {
            //  ((HomeActivity) getActivity()).closeDrawer();
            //   openGrievanceFlow();

            ((HomeActivity) getActivity()).closeDrawer();
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), GrievanceTabActivity.class, false);
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_grievance_status))) {
            ((HomeActivity) getActivity()).closeDrawer();
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), HelpSupportStatus.class, false);
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_graph))) {
            ((HomeActivity) getActivity()).closeDrawer();
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), GraphActivity.class, false);
        } else {
            onInfo(getResources().getString(R.string.coming_soon));
        }
    }

    /**
     * open the grievance screens flow
     */

    private void openGrievanceFlow() {
        if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("AG"))
            ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), ActivityGrievance.class, false);
        else {
            if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("EX") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CE")
                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UE") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CI")) {
                startActivityWithParams(AgentListActivity.class, Constant.DashBoardGrievance);
            } else if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("SH")) {
                startActivityWithParams(UserCityActivity.class, Constant.DashBoardGrievance);
            } else if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CO") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("HO")) {
                startActivityWithParams(UserStateActivity.class, Constant.DashBoardGrievance);
            }
        }
    }

    /**
     * open the billing screen for all type of users
     */

    private void openBillingScreen() {

        if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("AG"))
            ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), BillingActivity.class, false);
        else {
            if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("EX")
                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CE")
                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UE")
                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CI")) {
                startActivityWithParams(BillOutAgentListActivity.class, Constant.DashBoardBill);
            } else if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CO") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("HO") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("SH") || (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UH"))) {
                startActivityWithParams(UserStateActivity.class, Constant.DashBoardBill);
            }
        }
    }

    private void openOutstandingScreen() {

        if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("AG"))
            ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), LedgerActivity.class, false);
        else {
            if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("EX")
                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CE")
                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UE")
                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CI")) {
                startActivityWithParams(BillOutAgentListActivity.class, Constant.DashBoardOutstanding);
            } else if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CO") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("HO") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("SH") || (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UH"))) {
                startActivityWithParams(UserStateActivity.class, Constant.DashBoardOutstanding);
            }
        }
    }

    /**
     * show the ledger screen  for all type of users
     */

    private void openLedger() {

        if (PreferenceManager.getPrefUserType(getActivity()).equals("AG")) {
            ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), LedgerActivity.class, false);
        }
        else if (PreferenceManager.getPrefUserType(getActivity()).equals("EX") || PreferenceManager.getPrefUserType(getActivity()).equals("CI")) {
            ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), ShowAgentListActivity.class, false);
        } else {

            ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), StatesActivity.class, false);
        }



        /*if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("AG"))
            ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), LedgerActivity.class, false);
        else {
            if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("EX")
                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CE")
                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UE")
                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CI")) {
                startActivityWithParams(AgentListActivity.class, Constant.DashBoardOutstanding);
            } else if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("SH") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UH")) {
                startActivityWithParams(UserCityActivity.class, Constant.DashBoardOutstanding);
            } else if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CO") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("HO")) {
                startActivityWithParams(UserStateActivity.class, Constant.DashBoardOutstanding);
            }
        }*/
    }

    /**
     * open the profile screen for all type of users
     */

    private void showProfileAccordingToUser() {
        ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), ProfileActivity.class, false);

//        if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("AG"))
//            ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), ProfileActivity.class, false);
//        else {
//            if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("EX")
//                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CE")
//                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UE")
//                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CI")) {
//                startActivityWithParams(AgentListActivity.class, Constant.DrawerProfile);
//            }
//            else if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CO")||PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("HO")|| PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("SH") || (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UH"))) {
//                startActivityWithParams(UserStateActivity.class, Constant.DrawerProfile);
//            }
//        }

        ((HomeActivity) getActivity()).closeDrawer();
    }


    /**
     * launch the intent for screens flow with animations
     *
     * @param destinationClass where to go in app
     * @param clickFieldName   screen name
     */

    private void startActivityWithParams(Class destinationClass, String clickFieldName) {
        startActivity(new Intent(getActivity(), destinationClass).putExtra(Constant.SCREEN_NAME, clickFieldName));
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    public void onLogoutSuccess(LogoutResultModel logoutResultModel) {
        if (logoutResultModel.message.equals("success")) {
            String language = PreferenceManager.getAppLang(getActivity());
            PreferenceManager.clearAll(getActivity());
            PreferenceManager.setAppLang(getActivity(), language);
            startActivity(new Intent(getActivity(), LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            getActivity().finish();
        } else {
            Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_SHORT).show();
            String language = PreferenceManager.getAppLang(getActivity());
            PreferenceManager.clearAll(getActivity());
            PreferenceManager.setAppLang(getActivity(), language);
            startActivity(new Intent(getActivity(), LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            getActivity().finish();
        }


    }
}
