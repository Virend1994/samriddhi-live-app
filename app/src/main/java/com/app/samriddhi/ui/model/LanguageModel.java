package com.app.samriddhi.ui.model;

import androidx.databinding.BaseObservable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LanguageModel extends BaseObservable {
    @SerializedName("Bp_code")
    @Expose
    private String bpCode;
    @SerializedName("device_id")
    @Expose
    private String deviceId;

    public LanguageModel(String bpCode, String deviceId, String leng, String deviceTokenId) {
        this.bpCode = bpCode;
        this.deviceId = deviceId;
        this.leng = leng;
        this.deviceTokenId = deviceTokenId;
    }


    @SerializedName("Leng")
    @Expose
    private String leng;
    @SerializedName("DeviceTokenId")
    @Expose
    private String deviceTokenId;

    public String getBpCode() {
        return bpCode;
    }

    public void setBpCode(String bpCode) {
        this.bpCode = bpCode;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getLeng() {
        return leng;
    }

    public void setLeng(String leng) {
        this.leng = leng;
    }

    public String getDeviceTokenId() {
        return deviceTokenId;
    }

    public void setDeviceTokenId(String deviceTokenId) {
        this.deviceTokenId = deviceTokenId;
    }

}
