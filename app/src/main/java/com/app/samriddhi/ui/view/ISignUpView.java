package com.app.samriddhi.ui.view;

import com.app.samriddhi.ui.model.UserModel;

public interface ISignUpView extends IView {
    void onSuccess(UserModel userModel);

    void onRegisterSuccess(String msg);
}
