package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AgencyModel  extends BaseModel {

    @SerializedName("Name")
    @Expose

    private String Name;
    @SerializedName("BP_Code")

    @Expose
    private String BP_Code;
    public String getName() {
        return Name;
    }
    public void setName(String name) {
        Name = name;
    }
    public String getBP_Code() {
        return BP_Code;
    }
    public void setBP_Code(String BP_Code) {
        this.BP_Code = BP_Code;
    }
}
