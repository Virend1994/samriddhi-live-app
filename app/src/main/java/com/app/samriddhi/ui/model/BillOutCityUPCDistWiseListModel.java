package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BillOutCityUPCDistWiseListModel extends BaseModel {

    @SerializedName("SalesDistName")
    @Expose
    private String SalesDistName;


    @SerializedName("SalesDistCode")
    @Expose
    private String SalesDistCode;



    @SerializedName("Billing")
    @Expose
    private String Billing;



    @SerializedName("Outstanding")
    @Expose
    private String Outstanding;


    public String getSalesDistName() {
        return SalesDistName;
    }

    public void setSalesDistName(String salesDistName) {
        SalesDistName = salesDistName;
    }

    public String getSalesDistCode() {
        return SalesDistCode;
    }

    public void setSalesDistCode(String salesDistCode) {
        SalesDistCode = salesDistCode;
    }

    public String getBilling() {
        return Billing;
    }

    public void setBilling(String billing) {
        Billing = billing;
    }

    public String getOutstanding() {
        return Outstanding;
    }

    public void setOutstanding(String outstanding) {
        Outstanding = outstanding;
    }
}
