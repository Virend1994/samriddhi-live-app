package com.app.samriddhi.ui.activity.auth;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;

import androidx.databinding.DataBindingUtil;

import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.databinding.ActivityForgotPasswordBinding;
import com.app.samriddhi.ui.model.UserModel;
import com.app.samriddhi.ui.presenter.SignUpPresenter;
import com.app.samriddhi.ui.view.ISignUpView;
import com.app.samriddhi.util.AlertUtility;
import com.app.samriddhi.util.StringUtility;
import com.app.samriddhi.util.SystemUtility;
import com.google.gson.Gson;

public class ForgotPassword extends BaseActivity implements TextWatcher, ISignUpView {
    ActivityForgotPasswordBinding mBinding;
    SignUpPresenter mSignUpPresenter;
    UserModel userModel;
    String strAgencyCode = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_forgot_password);
        mBinding.setActivity(this);
        mBinding.edOtp.addTextChangedListener(this);
        mSignUpPresenter = new SignUpPresenter();
        mSignUpPresenter.setView(this);

        mBinding.radioBtnAgency1.setChecked(true);
        mBinding.radioBtnAgency1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {

                    mBinding.radioBtnAgency2.setChecked(false);
                    mBinding.radioBtnAgency3.setChecked(false);

                    strAgencyCode = userModel.getUser_info().get(0).getBP_Code();
                    Log.e("sdfsdfsd", "Bpcode 52" + " " + userModel.getUser_info().get(0).getBP_Code());


                }
            }
        });

        mBinding.radioBtnAgency2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {

                    mBinding.radioBtnAgency1.setChecked(false);
                    mBinding.radioBtnAgency3.setChecked(false);
                    strAgencyCode = userModel.getUser_info().get(1).getBP_Code();
                    Log.e("sdfsdfsd", "Bpcode 67" + " " + userModel.getUser_info().get(1).getBP_Code());

                }
            }
        });

        mBinding.radioBtnAgency3.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b == true) {

                    mBinding.radioBtnAgency2.setChecked(false);
                    mBinding.radioBtnAgency1.setChecked(false);
                    strAgencyCode = userModel.getUser_info().get(2).getBP_Code();
                    Log.e("sdfsdfsd", "Bpcode 81" + " " + userModel.getUser_info().get(2).getBP_Code());
                }
            }
        });
    }


    /* handle buttons click on Form */

    public void onItemClick(View view) {
        switch (view.getId()) {
            case R.id.txt_login_forgot:
                onBackPressed();
                break;
            case R.id.btn_send_otp:
                /*if (!StringUtility.validateString(mBinding.txtAgentId.getText().toString())) {
                    snackBar(mBinding.txtAgentId, R.string.error_agent_id_empty);
                    return;
                } else */
                if (!StringUtility.validateString(mBinding.edMobileNumber.getText().toString())) {
                    snackBar(mBinding.edMobileNumber, R.string.str_mobile_no_empty);
                    return;
                } else if (!StringUtility.validateMobileNumber(mBinding.edMobileNumber)) {
                    snackBar(mBinding.edMobileNumber, R.string.str_mobile_no_validate_error);
                    return;
                } else {
                    mSignUpPresenter.getOtpForForgot(mBinding.edMobileNumber.getText().toString());
                }
                break;

            case R.id.btn_forgot_submit:
                if (isValidAllFields()) {
                    userModel.setDevice_id(SystemUtility.getDeviceId(this));
                    userModel.setBp_code(strAgencyCode);
                    Gson gson = new Gson();
                    String data = gson.toJson(userModel);
                    Log.e("wfwwefwef", data);
                    mSignUpPresenter.submitPassword(userModel);
                }
                break;
            default:
                break;
        }

    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        mBinding.edOtp.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mobile, 0, 0, 0);
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        mBinding.edOtp.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mobile, 0, 0, 0);
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s.length() == 4) {
            if (s.toString().equals(userModel.getOtp())) {
                mBinding.edOtp.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mobile, 0, R.drawable.check, 0);
                mBinding.edOtp.setEnabled(false);
                userModel.setOtpVerified(true);
                mBinding.edMobileNumber.setEnabled(false);
            } else {
                userModel.setOtpVerified(false);

                mBinding.edOtp.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mobile, 0, R.drawable.cross, 0);
            }
        } else {
            mBinding.edOtp.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mobile, 0, 0, 0);
        }
    }

    /*Mobile otp verified success*/
    @Override
    public void onSuccess(UserModel userModel) {
        if (userModel != null) {
            this.userModel = userModel;
            mBinding.setUser(userModel);
            mBinding.radioBtnAgency1.setChecked(true);
            strAgencyCode = userModel.getUser_info().get(0).getBP_Code();
            Log.e("sdfsdfsd", "Bpcode" + " " + userModel.getUser_info().get(0).getBP_Code());

            if (userModel.getUser_info().size() == 1) {

                mBinding.radioBtnAgency1.setVisibility(View.VISIBLE);
                mBinding.radioBtnAgency1.setText(userModel.getUser_info().get(0).getName()+"-"+userModel.getUser_info().get(0).getBP_Code());

                mBinding.radioBtnAgency2.setVisibility(View.GONE);

                mBinding.radioBtnAgency3.setVisibility(View.GONE);
            } else if (userModel.getUser_info().size() == 2) {

                mBinding.radioBtnAgency1.setVisibility(View.VISIBLE);
                mBinding.radioBtnAgency1.setText(userModel.getUser_info().get(0).getName()+"-"+userModel.getUser_info().get(0).getBP_Code());

                mBinding.radioBtnAgency2.setVisibility(View.VISIBLE);
                mBinding.radioBtnAgency2.setText(userModel.getUser_info().get(1).getName()+"-"+userModel.getUser_info().get(1).getBP_Code());

                mBinding.radioBtnAgency3.setVisibility(View.GONE);
            } else {
                mBinding.radioBtnAgency1.setVisibility(View.VISIBLE);
                mBinding.radioBtnAgency1.setText(userModel.getUser_info().get(0).getName()+"-"+userModel.getUser_info().get(0).getBP_Code());

                mBinding.radioBtnAgency2.setVisibility(View.VISIBLE);
                mBinding.radioBtnAgency2.setText(userModel.getUser_info().get(1).getName()+"-"+userModel.getUser_info().get(1).getBP_Code());

                mBinding.radioBtnAgency3.setVisibility(View.VISIBLE);
                mBinding.radioBtnAgency3.setText(userModel.getUser_info().get(2).getName()+"-"+userModel.getUser_info().get(2).getBP_Code());
            }


        }

    }

    /*After updating the password on server successfully */
    @Override
    public void onRegisterSuccess(String msg) {
        AlertUtility.showAlertWithListener(this, msg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                startActivity(new Intent(ForgotPassword.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });
    }

    @Override
    public Context getContext() {
        return this;
    }
    /*Validate all fields of Forgot password form*/
    private boolean isValidAllFields() {
        if (!StringUtility.validateEditText(mBinding.edPassword)) {
            snackBar(mBinding.edPassword, R.string.str_pwd_empty);
            return false;
        } else if (mBinding.edPassword.getText().toString().length() < 6) {
            snackBar(mBinding.edPassword, R.string.str_password_length);
            return false;
        } else if (!StringUtility.validateEditText(mBinding.edConfirmPwd)) {
            snackBar(mBinding.edConfirmPwd, R.string.str_confirm_pwd_empty);
            return false;
        } else if (!(mBinding.edPassword.getText().toString()).equals(mBinding.edConfirmPwd.getText().toString())) {
            snackBar(mBinding.edPassword, R.string.str_pwd_not_match);
            return false;
        } else {
            userModel.setPassword(mBinding.edPassword.getText().toString());
            return true;
        }
    }
}
