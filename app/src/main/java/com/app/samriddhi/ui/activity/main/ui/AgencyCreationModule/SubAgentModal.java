package com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule;

import java.io.Serializable;

public class SubAgentModal implements Serializable {

    String pk,name,copy,edition_code;

    public String getEdition_code() {
        return edition_code;
    }

    public void setEdition_code(String edition_code) {
        this.edition_code = edition_code;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCopy() {
        return copy;
    }

    public void setCopy(String copy) {
        this.copy = copy;
    }
}
