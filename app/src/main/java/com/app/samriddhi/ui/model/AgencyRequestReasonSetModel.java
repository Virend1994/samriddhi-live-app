package com.app.samriddhi.ui.model;

public class AgencyRequestReasonSetModel {
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getAg_code() {
        return ag_code;
    }

    public void setAg_code(String ag_code) {
        this.ag_code = ag_code;
    }

    public int getAg_copies() {
        return ag_copies;
    }

    public void setAg_copies(int ag_copies) {
        this.ag_copies = ag_copies;
    }

    public String getAg_reason() {
        return ag_reason;
    }

    public void setAg_reason(String ag_reason) {
        this.ag_reason = ag_reason;
    }

    public String reason;
    public String ag_code;
    public int ag_copies,subag_copies;
    public String ag_reason;
    public String ag_name;
    public String subag_code;
    public String asd;
    public String outstanding;
    public String unbilled_amount;
    public String agency_location;

    public String getAsd() {
        return asd;
    }

    public void setAsd(String asd) {
        this.asd = asd;
    }

    public String getOutstanding() {
        return outstanding;
    }

    public void setOutstanding(String outstanding) {
        this.outstanding = outstanding;
    }

    public String getUnbilled_amount() {
        return unbilled_amount;
    }

    public void setUnbilled_amount(String unbilled_amount) {
        this.unbilled_amount = unbilled_amount;
    }

    public String getAgency_location() {
        return agency_location;
    }

    public void setAgency_location(String agency_location) {
        this.agency_location = agency_location;
    }

    public String getTotal_outstanding() {
        return total_outstanding;
    }

    public void setTotal_outstanding(String total_outstanding) {
        this.total_outstanding = total_outstanding;
    }

    public String total_outstanding;

    public String getSubag_code() {
        return subag_code;
    }

    public void setSubag_code(String subag_code) {
        this.subag_code = subag_code;
    }

    public int getSubag_copies() {
        return subag_copies;
    }

    public void setSubag_copies(int subag_copies) {
        this.subag_copies = subag_copies;
    }

    public String getAg_name() {
        return ag_name;
    }

    public void setAg_name(String ag_name) {
        this.ag_name = ag_name;
    }
}
