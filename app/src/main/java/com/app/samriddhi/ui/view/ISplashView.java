package com.app.samriddhi.ui.view;

import com.app.samriddhi.ui.model.ServerPortModel;

public interface ISplashView extends IView {

    void onSuccess(ServerPortModel body);

}
