package com.app.samriddhi.ui.model;

public class AgencyChildSet {
    public String getChild_name() {
        return child_name;
    }

    public void setChild_name(String child_name) {
        this.child_name = child_name;
    }

    public String getChild_DOB() {
        return child_DOB;
    }

    public void setChild_DOB(String child_DOB) {
        this.child_DOB = child_DOB;
    }

    public String getChild_education() {
        return child_education;
    }

    public void setChild_education(String child_education) {
        this.child_education = child_education;
    }
    public Object getAg_child_id() {
        return ag_child_id;
    }

    public void setAg_child_id(Object ag_child_id) {
        this.ag_child_id = ag_child_id;
    }
    public String getChild_gender() {
        return child_gender;
    }

    public void setChild_gender(String child_gender) {
        this.child_gender = child_gender;
    }

    public String child_name;
    public String child_DOB;
    public String child_education;
    public  Object ag_child_id;
    public String child_educationName;

    public String getChild_educationName() {
        return child_educationName;
    }

    public void setChild_educationName(String child_educationName) {
        this.child_educationName = child_educationName;
    }

    public String child_gender;
}
