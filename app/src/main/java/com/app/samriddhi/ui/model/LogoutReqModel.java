package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LogoutReqModel extends BaseModel {
    @SerializedName("flag")
    @Expose
    public int flag;

    @SerializedName("user_id")
    @Expose
    public String user_id;

}
