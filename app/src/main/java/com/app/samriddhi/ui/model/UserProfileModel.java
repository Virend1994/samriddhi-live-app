package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserProfileModel extends BaseModel {
    @SerializedName("usermaster")
    @Expose
    private List<Usermaster> usermaster = null;
    @SerializedName("userdetail")
    @Expose
    private List<Userdetail> userdetail = null;
    @SerializedName("usercrm")
    @Expose
    private UserCrmModel usercrm;
    @SerializedName("EducationList")
    @Expose
    private List<String> educationList = null;
    public Object pan_copy;
    public Object user_image;

    public Object getPan_copy() {
        return pan_copy;
    }

    public void setPan_copy(Object pan_copy) {
        this.pan_copy = pan_copy;
    }

    public Object getUser_image() {
        return user_image;
    }

    public void setUser_image(Object user_image) {
        this.user_image = user_image;
    }

    public List<String> getEducationList() {
        return educationList;
    }

    public void setEducationList(List<String> educationList) {
        this.educationList = educationList;
    }

    public List<Usermaster> getUsermaster() {
        return usermaster;
    }

    public void setUsermaster(List<Usermaster> usermaster) {
        this.usermaster = usermaster;
    }

    public List<Userdetail> getUserdetail() {
        return userdetail;
    }

    public void setUserdetail(List<Userdetail> userdetail) {
        this.userdetail = userdetail;
    }

    public UserCrmModel getUsercrm() {
        return usercrm;
    }

    public void setUsercrm(UserCrmModel usercrm) {
        this.usercrm = usercrm;
    }
}
