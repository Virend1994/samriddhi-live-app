package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.app.samriddhi.base.model.BillDataModal;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DashBoardData extends BaseModel {
    @SerializedName("HintCount")
    @Expose
    public String HintCount;

    @SerializedName("state_flag")
    @Expose
    public String state_flag;

    @SerializedName("data")
    @Expose
    private DashBoardModel data;
    @SerializedName("lastmonth_year")
    @Expose
    private String lastmonthYear;
    @SerializedName("lastmonth_year_Bill_Amt")
    @Expose
    private String lastmonthYearBillAmt;
    @SerializedName("YTDData")
    @Expose
    private YearDataModel yTDData;
    @SerializedName("MTDData")
    @Expose
    private MonthDataModel mTDData;
    @SerializedName("reply")
    @Expose
    private String reply;
    @SerializedName("Open_count")
    @Expose
    private Integer openCount;
    @SerializedName("Closed_Count")
    @Expose
    private Integer closedCount;
    @SerializedName("Noti_Count")
    @Expose
    private Integer notification_Count;

    @SerializedName("isLastBillConfirmed")
    @Expose
    public String isLastBillConfirmed;


    @SerializedName("billingData")
    @Expose
    private ArrayList<BillDataModal> billDataModals = null;


    public String getIsLastBillConfirmed() {
        return isLastBillConfirmed;
    }

    public void setIsLastBillConfirmed(String isLastBillConfirmed) {
        this.isLastBillConfirmed = isLastBillConfirmed;
    }

    public ArrayList<BillDataModal> getBillDataModals() {
        return billDataModals;
    }

    public void setBillDataModals(ArrayList<BillDataModal> billDataModals) {
        this.billDataModals = billDataModals;
    }

    public String getState_flag() {
        return state_flag;
    }

    public void setState_flag(String state_flag) {
        this.state_flag = state_flag;
    }

    public YearDataModel getyTDData() {
        return yTDData;
    }

    public void setyTDData(YearDataModel yTDData) {
        this.yTDData = yTDData;
    }

    public MonthDataModel getmTDData() {
        return mTDData;
    }

    public void setmTDData(MonthDataModel mTDData) {
        this.mTDData = mTDData;
    }

    public Integer getNotification_Count() {
        return notification_Count;
    }

    public void setNotification_Count(Integer notification_Count) {
        this.notification_Count = notification_Count;
    }

    public Integer getOpenCount() {
        return openCount;
    }

    public void setOpenCount(Integer openCount) {
        this.openCount = openCount;
    }

    public Integer getClosedCount() {
        return closedCount;
    }

    public void setClosedCount(Integer closedCount) {
        this.closedCount = closedCount;
    }

    public DashBoardModel getData() {
        return data;
    }

    public void setData(DashBoardModel data) {
        this.data = data;
    }

    public String getLastmonthYear() {
        return lastmonthYear;
    }

    public void setLastmonthYear(String lastmonthYear) {
        this.lastmonthYear = lastmonthYear;
    }

    public String getLastmonthYearBillAmt() {
        return lastmonthYearBillAmt;
    }

    public void setLastmonthYearBillAmt(String lastmonthYearBillAmt) {
        this.lastmonthYearBillAmt = lastmonthYearBillAmt;
    }

    public YearDataModel getYTDData() {
        return yTDData;
    }

    public void setYTDData(YearDataModel yTDData) {
        this.yTDData = yTDData;
    }

    public MonthDataModel getMTDData() {
        return mTDData;
    }

    public void setMTDData(MonthDataModel mTDData) {
        this.mTDData = mTDData;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getHintCount() {
        return HintCount;
    }

    public void setHintCount(String hintCount) {
        HintCount = hintCount;
    }

}
