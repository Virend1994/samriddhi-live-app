package com.app.samriddhi.ui.activity.main.ui

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.content.FileProvider
import androidx.core.view.children
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import com.app.samriddhi.R
import com.app.samriddhi.base.BaseActivity
import com.app.samriddhi.databinding.ActivityNoOfCopiesBinding
import com.app.samriddhi.prefernces.PreferenceManager
import com.app.samriddhi.ui.model.*
import com.app.samriddhi.ui.presenter.CopiesPresenter
import com.app.samriddhi.ui.presenter.DashboardPresenter
import com.app.samriddhi.ui.view.ICopiesView
import com.app.samriddhi.ui.view.IDashboardView
import com.app.samriddhi.util.*
import com.github.amlcurran.showcaseview.ShowcaseView
import com.github.amlcurran.showcaseview.targets.ViewTarget
import com.kizitonwose.calendarview.model.CalendarDay
import com.kizitonwose.calendarview.model.CalendarMonth
import com.kizitonwose.calendarview.model.DayOwner
import com.kizitonwose.calendarview.ui.DayBinder
import com.kizitonwose.calendarview.ui.MonthHeaderFooterBinder
import com.kizitonwose.calendarview.ui.ViewContainer
import kotlinx.android.synthetic.main.activity_no_of_copies.*
import kotlinx.android.synthetic.main.activity_no_of_copies.view.*
import kotlinx.android.synthetic.main.calendar_day_legend.*
import kotlinx.android.synthetic.main.calender_header.view.*
import kotlinx.android.synthetic.main.calender_row_layout.view.*
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.custom_toolbar.view.*
import org.threeten.bp.LocalDate
import org.threeten.bp.YearMonth
import java.io.File
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class NoOfCopiesActivity : BaseActivity(), ICopiesView, AdapterView.OnItemSelectedListener, IDashboardView {
    override fun onSuccess(dashBoardModel: DashBoardData, lastMonthBillAmt: String, hintCount: String, openCount: Int, closeCount: Int) {
        binding.layoutAgentDetail.item = dashBoardModel.data
        binding.layoutAgentDetail.billing = if (dashBoardModel.lastmonthYearBillAmt.isEmpty()) "" else SystemUtility.formatDecimalValues(java.lang.Float.parseFloat(dashBoardModel.lastmonthYearBillAmt)).toString()
        //   binding.layoutAgentDetail.billing = dashBoardModel.lastmonthYearBillAmt

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent!!.id) {
            R.id.spinner_editions -> {
                if (position != 0) {
                    if (!spinner_total_copies.selectedItem.equals(resources.getString(R.string.str_total_copies))) {
                        mPresenter.getNumberOfCopies(editionData.results.get(0).navSubagentSet.results.get(spinner_total_copies.selectedItemPosition - 1).shipToParty, "SA", today.minusMonths(1).withDayOfMonth(1).toString(), today.withDayOfMonth(today.lengthOfMonth()).toString()
                                , editionData.results.get(0).navEdtnSet.results.get(spinner_editions.selectedItemPosition - 1).pva, false)
                    } else {
                        mPresenter.getNumberOfCopies(if (PreferenceManager.getPrefUserType(this) == "AG") PreferenceManager.getAgentId(this) else intent.getStringExtra("agentId"), "AG", today.minusMonths(1).withDayOfMonth(1).toString(), today.withDayOfMonth(today.lengthOfMonth()).toString()
                                , editionData.results.get(0).navEdtnSet.results.get(spinner_editions.selectedItemPosition - 1).pva, false)
                    }
                } else {
                    if (position == 0) {
                        if (spinner_total_copies.selectedItem.equals(resources.getString(R.string.str_total_copies))) {
                            mPresenter.getNumberOfCopies(if (PreferenceManager.getPrefUserType(this) == "AG") PreferenceManager.getAgentId(this) else intent.getStringExtra("agentId"), "AG", today.minusMonths(1).withDayOfMonth(1).toString(), today.withDayOfMonth(today.lengthOfMonth()).toString()
                                    , "*", false)
                        } else {
                            mPresenter.getNumberOfCopies(editionData.results.get(0).navSubagentSet.results.get(spinner_total_copies.selectedItemPosition - 1).shipToParty, "SA", today.minusMonths(1).withDayOfMonth(1).toString(), today.withDayOfMonth(today.lengthOfMonth()).toString()
                                    , "*", false)
                        }

                    }
                }
            }
            R.id.spinner_total_copies -> {
                if (position == 0) {
                    if (spinner_editions.selectedItem.equals(resources.getString(R.string.str_all_editions))) {
                        mPresenter.getNumberOfCopies(if (PreferenceManager.getPrefUserType(this) == "AG") PreferenceManager.getAgentId(this) else intent.getStringExtra("agentId"), "AG", today.minusMonths(1).withDayOfMonth(1).toString(), today.withDayOfMonth(today.lengthOfMonth()).toString()
                                , "*", false)
                    } else {

                        mPresenter.getNumberOfCopies(if (PreferenceManager.getPrefUserType(this) == "AG") PreferenceManager.getAgentId(this) else intent.getStringExtra("agentId"), "AG", today.minusMonths(1).withDayOfMonth(1).toString(), today.withDayOfMonth(today.lengthOfMonth()).toString()
                                , editionData.results.get(0).navEdtnSet.results.get(spinner_editions.selectedItemPosition - 1).pva, false)
                    }
                } else {
                    if (spinner_editions.selectedItem.equals(resources.getString(R.string.str_all_editions))) {
                        mPresenter.getNumberOfCopies(editionData.results.get(0).navSubagentSet.results.get(spinner_total_copies.selectedItemPosition - 1).shipToParty, "SA", today.minusMonths(1).withDayOfMonth(1).toString(), today.withDayOfMonth(today.lengthOfMonth()).toString()
                                , "*", false)
                    } else {

                        mPresenter.getNumberOfCopies(editionData.results.get(0).navSubagentSet.results.get(spinner_total_copies.selectedItemPosition - 1).shipToParty, "SA", today.minusMonths(1).withDayOfMonth(1).toString(), today.withDayOfMonth(today.lengthOfMonth()).toString()
                                , editionData.results.get(0).navEdtnSet.results.get(spinner_editions.selectedItemPosition - 1).pva, false)

                    }
                }

            }

        }
    }

    override fun onNumberOfCopiesSuccess(nModel: NumberOfCopiesModel, fromSubmit: Boolean) {
        copiesModel = nModel
        for (result in copiesModel.results) {
            copiesMap.put(SystemUtility.getDate(result.ordDate), result.paidCopy)
        }
        if (fromSubmit) {
            Log.e("selected item", "====== " + spinner_total_copies.selectedItemPosition)
            startActivity(Intent(this, NoOfCopiesFilterActivity::class.java).putExtra("copiesData", nModel).putExtra("selectedName", spinner_total_copies.selectedItem.toString()))
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        } else
            initViews()
    }

    override fun getContext(): Context {
        return this
    }

    override fun onLogoutSuccess(logoutResultModel: LogoutResultModel?) {
    }

    override fun onCopiesSucces(editionModel: EditionModel?) {
        if (editionModel != null) {
            editionData = editionModel
            val arrEdition = ArrayList<String>()
            val arrCopies = ArrayList<String>()
            arrEdition.add(0, resources.getString(R.string.str_all_editions))
            arrCopies.add(0, resources.getString(R.string.str_total_copies))
            for (data in editionModel.results.get(0).navEdtnSet.results) {
                arrEdition.add(data.toString())
            }
            for (copies in editionModel.results.get(0).navSubagentSet.results) {
                arrCopies.add(copies.toString())
            }
            spinner_editions.adapter = ArrayAdapter(this, R.layout.simple_list_item_1, arrEdition)
            spinner_total_copies.adapter = ArrayAdapter(this, R.layout.simple_list_item_1, arrCopies)
        }
        spinner_editions.onItemSelectedListener = this
        spinner_total_copies.onItemSelectedListener = this
    }

    val mPresenter = CopiesPresenter()
    val mAgentPresenter = DashboardPresenter()
    private var selectedDate: LocalDate? = null
    private val today = LocalDate.now()
    val daysOfWeek = daysOfWeekFromLocale()
    var copiesModel = NumberOfCopiesModel()
    var copiesMap = HashMap<String, String>()
    var editionData = EditionModel()
    var cal = Calendar.getInstance()
    var fromDate = Date()
    var toDate = Date()
    var arrData = ArrayList<ExcelDataModel>()
    var excelDataModel = ExcelDataModel()
    private lateinit var binding: ActivityNoOfCopiesBinding

    lateinit var showcaseView: ShowcaseView
    internal var showCount = 0
    lateinit var secondParams: RelativeLayout.LayoutParams


    val fromDateSetListener = object : DatePickerDialog.OnDateSetListener {
        override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                               dayOfMonth: Int) {
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            fromDate = cal.time
            ed_from_date.text = SystemUtility.getFormatedCalenderDay(cal)
        }
    }

    val toDateSetListener = object : DatePickerDialog.OnDateSetListener {
        override fun onDateSet(view: DatePicker, year: Int, monthOfYear: Int,
                               dayOfMonth: Int) {
            cal.set(Calendar.YEAR, year)
            cal.set(Calendar.MONTH, monthOfYear)
            cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            toDate = cal.time
            ed_to_date.text = SystemUtility.getFormatedCalenderDay(cal)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_no_of_copies)
        mPresenter.view = this
        mAgentPresenter.view = this

        setViews()
    }

    /**
     * initialize and bind views
     */
    private fun setViews() {
        img_back.setOnClickListener {
            onBackPressed()
        }

        rl_notification.setOnClickListener { startActivityAnimation(this, NotificationActivity::class.java, false) }

        if (intent != null && intent.hasExtra("agentName")) {
            binding.layoutAgentDetail.txtAgencyNames.text = intent.getStringExtra("agentName")
            binding.layoutAgentDetail.txtAgenciesCode.text = intent.getStringExtra("agentId")
        }

        mPresenter.getEditionList(if (PreferenceManager.getPrefUserType(this) == "AG") PreferenceManager.getAgentId(this) else intent.getStringExtra("agentId"))
        if (!PreferenceManager.getPrefUserType(this).equals("AG")) {
            mAgentPresenter.getInternalUsersDashBoardData(intent.getStringExtra("agentId"))
            toolbar_calender.toolbar_calender.txt_tittle.text = resources.getString(R.string.str_agent_detail)
        } else {
            toolbar_calender.toolbar_calender.txt_tittle.text = resources.getString(R.string.title_no_of_copies)
        }
        toolbar_calender.rl_excel.visibility = View.VISIBLE

        ed_from_date.setOnClickListener {
            showDatePicker(fromDateSetListener)
        }
        ed_to_date.setOnClickListener {
            showDatePicker(toDateSetListener)
        }
        btn_submit_filter.setOnClickListener {
            if (validateFilter()) {
                if (spinner_total_copies.selectedItem.equals(resources.getString(R.string.str_total_copies))) {
                    mPresenter.getNumberOfCopies(if (PreferenceManager.getPrefUserType(this) == "AG") PreferenceManager.getAgentId(this) else intent.getStringExtra("agentId"), "AG", ed_from_date.text.toString(), ed_to_date.text.toString(), "*", true)

                } else {
                    mPresenter.getNumberOfCopies(editionData.results.get(0).navSubagentSet.results.get(spinner_total_copies.selectedItemPosition - 1).shipToParty, "SA", ed_from_date.text.toString(), ed_to_date.text.toString(), "*", true)

                }
            }

        }
        binding.layoutAgentDetail.rlProfile.setOnClickListener {
            startActivity(Intent(this, ProfileActivity::class.java).putExtra("agentId", intent.getStringExtra("agentId")))
        }

        binding.toolbarCalender.imgExcel.setOnClickListener {
            askStoragePermission(201)

        }
       // showCoachMarksOnScreen()
    }

    /**
     * validate the form fields for filter the copies
     */
    private fun validateFilter(): Boolean {
        if (!StringUtility.validateString(ed_from_date.text.toString())) {
            snackBar(ed_from_date, R.string.str_from_date_empty)
            return false
        } else if (!StringUtility.validateString(ed_to_date.text.toString())) {
            snackBar(ed_to_date, R.string.str_to_date_empty)
            return false
        } else if (fromDate.compareTo(toDate) > 0) {
            snackBar(ed_from_date, R.string.str_date_compare_msg)
            return false
        }
        return true
    }


    private fun downloadFile() {
        loadProgressBar(null, "Please wait..", false)

        if (copiesModel != null) {

            for (result in copiesModel.results) {
                excelDataModel = ExcelDataModel(SystemUtility.getMonthFormattedCopies(result.ordDate), result.paidCopy, result.partner)
                arrData.add(excelDataModel)
            }
            var file: File? = null
            if (arrData != null && arrData.size > 0) {

                file = SystemUtility.convertJsonToExcel(arrData)
            }

            val finalFile = file
            val handler = Handler()
            handler.postDelayed({
                dismissProgressBar()
                AlertUtility.showAlertFileOpen(this@NoOfCopiesActivity, String.format(resources.getString(R.string.str_excel_download_success), " InternalStorage/Samriddhi/CopiesData ")) { dialog, which -> openFile(FileProvider.getUriForFile(this@NoOfCopiesActivity, "com.app.samriddhi.fileprovider", finalFile!!)) }
                //  onInfo(String.format(getResources().getString(R.string.str_excel_download_success), " InternalStorage/Samriddhi/CopiesData "));
            }, 3000)


        }
    }

    private fun openFile(uri: Uri) {
        Log.e("create pdf uri path==>", "" + uri)
        try {
            val intent = Intent(Intent.ACTION_VIEW)
            intent.setDataAndType(uri, "application/vnd.ms-excel")
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            startActivity(intent)

        } catch (e: ActivityNotFoundException) {
            Toast.makeText(applicationContext,
                    "There is no any PDF Viewer",
                    Toast.LENGTH_SHORT).show()
        }

    }

    private fun showDatePicker(toDateSetListener: DatePickerDialog.OnDateSetListener) {
        val datePickerDialog = DatePickerDialog(this, toDateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH))
        datePickerDialog.datePicker.maxDate = System.currentTimeMillis()
        datePickerDialog.show()
    }

    private fun initViews() {
        if (legendLayout
                != null) {
            legendLayout.children.forEachIndexed { index, view ->
                (view as TextView).apply {
                    text = daysOfWeek[index].name.first().toString()
                    setTextColorRes(R.color.white_color)
                }
            }
        }

        calender_view.setup(YearMonth.now().minusMonths(1), YearMonth.now(), daysOfWeek.first())
        (calender_view.layoutManager as LinearLayoutManager).reverseLayout = true
        class DayViewContainer(view: View) : ViewContainer(view) {
            // Will be set when this container is bound. See the dayBinder.
            lateinit var day: CalendarDay // Will be set when this container is bound.
            val textView = view.exTwoDayText
            val layout = view.calender_row
            val textCopies = view.txt_copies_count

            init {
                view.setOnClickListener {
                    if (day.owner == DayOwner.THIS_MONTH) {
                        if (selectedDate != day.date) {
                            val oldDate = selectedDate
                            selectedDate = day.date
                            calender_view.notifyDateChanged(day.date)
                            oldDate?.let { calender_view.notifyDateChanged(it) }
                        }
                    }
                }
            }

        }
        calender_view.dayBinder = object : DayBinder<DayViewContainer> {
            override fun create(view: View) = DayViewContainer(view)
            override fun bind(container: DayViewContainer, day: CalendarDay) {
                val textView = container.textView
                val textCopies = container.textCopies
                container.day = day
                val layout = container.layout
                textView.text = day.date.dayOfMonth.toString()

                if (copiesMap != null && !copiesMap.isEmpty()) {
                    textCopies.text = copiesMap.get(day.date.toString())
                }


                if (day.owner == DayOwner.THIS_MONTH) {
                    textView.makeVisible()
                    textCopies.makeVisible()
                    when (day.date) {
                        today -> {
                            textView.setTextColorRes(R.color.white_color)
                            textCopies.setTextColorRes(R.color.white_color)
                            layout.setBackgroundResource(R.drawable.rounded_red_background)
                        }
                        else -> {
                            textView.setTextColorRes(R.color.black_color)
                            textCopies.setTextColorRes(R.color.home_item_border)
                            layout.background = null
                        }
                    }
                } else {
                    textView.makeVisible()
                    textView.setTextColorRes(R.color.home_item_border)
                    textCopies.makeInVisible()
                }
            }
        }
        class MonthViewContainer(view: View) : ViewContainer(view) {
            val textView = view.exTwoHeaderText
        }
        calender_view.monthHeaderBinder = object : MonthHeaderFooterBinder<MonthViewContainer> {
            override fun create(view: View) = MonthViewContainer(view)
            override fun bind(container: MonthViewContainer, month: CalendarMonth) {
                @SuppressLint("SetTextI18n") // Concatenation warning for `setText` call.
                container.textView.text = "${month.yearMonth.month.name.toLowerCase().capitalize()} ${month.year}"
            }
        }
    }

    override fun onError(reason: String?, fromButon: Boolean) {
        if (!fromButon) {
            copiesMap.clear()
            initViews()
        }
        AlertUtility.showAlertDialog(this, resources.getString(R.string.str_no_result_found))
    }


    override fun NeverAskAgain(request_code: Int) {
      // will override if require
    }

    override fun PermissionGranted(request_code: Int) {
        downloadFile()
    }

    /**
     * permission denied by user
     */
    override fun PermissionDenied(request_code: Int) {
        // will override if require
    }

    /**
     * shows the guidance on the screen
     */

    private fun showCoachMarksOnScreen() {
        showCount = 0
        showcaseView = ShowcaseView.Builder(this)
                .setTarget(ViewTarget(R.id.rl_excel, this))
                .singleShot(PreferenceManager.PREF_NO_OF_COPIES_SHOT)
                .setContentTitle(resources.getString(R.string.str_download_excel_msg)).blockAllTouches()
                .setStyle(R.style.CustomCoachMarksTheme)
                .setShowcaseDrawer(CustonShowCaseViewCircle(binding.toolbarCalender.rlExcel, resources))
                .build()
        showcaseView.forceTextPosition(ShowcaseView.BELOW_SHOWCASE)
        val params = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
        params.addRule(RelativeLayout.ALIGN_PARENT_LEFT)
        params.setMargins(15, 10, 10, 30)
        showcaseView.setButtonPosition(params)
        showcaseView.addView(getButton(), secondParams)

    }

    /**
     * return the button for guidance screen
     */

    fun getButton(): Button {
        val button = LayoutInflater.from(this).inflate(R.layout.showcase_custom_view_button, showcaseView, false) as Button
        secondParams = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        secondParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
        secondParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT)
        secondParams.setMargins(15, 10, 10, 30)
        button.setOnClickListener(coachClicks)
        showCount++
        return button
    }

    /**
     * handles the coach marks buttons click
     */

    internal var coachClicks: View.OnClickListener = View.OnClickListener {
        when (showCount) {
            1 -> {
                showcaseView.hide()
                showcaseView = ShowcaseView.Builder(this)
                        .setTarget(ViewTarget(R.id.spinner_editions, this))
                        .setContentTitle(resources.getString(R.string.str_edition_selection_msg)).blockAllTouches()
                        .setStyle(R.style.CustomCoachMarksTheme)
                        .setShowcaseDrawer(CustomShowCaseView(binding.spinnerEditions, resources))
                        .build()
                showcaseView.forceTextPosition(ShowcaseView.BELOW_SHOWCASE)
                val params = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
                params.addRule(RelativeLayout.ALIGN_PARENT_LEFT)
                params.setMargins(15, 10, 10, 30)
                showcaseView.setButtonPosition(params)
                showcaseView.addView(getButton(), secondParams)
            }
            2 -> {
                showcaseView.hide()
                showcaseView = ShowcaseView.Builder(this)
                        .setTarget(ViewTarget(R.id.spinner_total_copies, this))
                        .setContentTitle(resources.getString(R.string.str_total_copies_selection_msg)).blockAllTouches()
                        .setStyle(R.style.CustomCoachMarksTheme)
                        .setShowcaseDrawer(CustomShowCaseView(binding.spinnerTotalCopies, resources))
                        .build()
                showcaseView.forceTextPosition(ShowcaseView.BELOW_SHOWCASE)
                val params = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
                params.addRule(RelativeLayout.ALIGN_PARENT_LEFT)
                params.setMargins(15, 10, 10, 30)
                showcaseView.setButtonPosition(params)
                showcaseView.addView(getButton(), secondParams)
            }
            3 -> {
                showcaseView.hide()
                binding.scrollCalender.fullScroll(View.FOCUS_DOWN)
                showcaseView = ShowcaseView.Builder(this)
                        .setTarget(ViewTarget(R.id.ed_from_date, this))
                        .setContentTitle(resources.getString(R.string.str_from_date_msg)).blockAllTouches()
                        .setStyle(R.style.CustomCoachMarksTheme)
                        .setShowcaseDrawer(CustomShowCaseView(binding.edToDate, resources))
                        .build()
                val params = RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM)
                params.addRule(RelativeLayout.ALIGN_PARENT_LEFT)
                params.setMargins(15, 10, 10, 30)
                showcaseView.setButtonPosition(params)
                showcaseView.addView(getButton(), secondParams)
            }
            4 -> {
                showcaseView.hide()
                showcaseView = ShowcaseView.Builder(this)
                        .setTarget(ViewTarget(R.id.ed_to_date, this))
                        .setContentTitle(resources.getString(R.string.str_to_date_msg)).blockAllTouches()
                        .setStyle(R.style.CustomCoachMarksTheme)
                        .setShowcaseDrawer(CustomShowCaseView(binding.edToDate, resources))
                        .build()
            }

            else -> showcaseView.hide()
        }
    }

}