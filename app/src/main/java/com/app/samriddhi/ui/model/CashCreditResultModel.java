package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CashCreditResultModel extends BaseModel {
    @SerializedName("results")
    @Expose
    private List<CreditCashModel> results = null;

    public List<CreditCashModel> getResults() {
        return results;
    }

    public void setResults(List<CreditCashModel> results) {
        this.results = results;
    }
}
