package com.app.samriddhi.ui.view;

import com.app.samriddhi.ui.model.UserModel;

public interface ILoginView extends IView {
    void onLoginSuccess(UserModel userData);

    void onLoginSuccess(String msg);
}
