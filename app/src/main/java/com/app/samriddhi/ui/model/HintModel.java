package com.app.samriddhi.ui.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HintModel extends BaseModel implements Parcelable {
    public static final Creator<HintModel> CREATOR = new Creator<HintModel>() {
        @Override
        public HintModel createFromParcel(Parcel source) {
            return new HintModel(source);
        }

        @Override
        public HintModel[] newArray(int size) {
            return new HintModel[size];
        }
    };
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("pk")
    @Expose
    private Integer pk;
    @SerializedName("fields")
    @Expose
    private HintFieldsModel fields;

    public HintModel() {
    }

    protected HintModel(Parcel in) {
        super(in);
        this.model = in.readString();
        this.pk = (Integer) in.readValue(Integer.class.getClassLoader());
        this.fields = in.readParcelable(HintFieldsModel.class.getClassLoader());
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }

    public HintFieldsModel getFields() {
        return fields;
    }

    public void setFields(HintFieldsModel fields) {
        this.fields = fields;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeString(this.model);
        dest.writeValue(this.pk);
        dest.writeParcelable(this.fields, flags);
    }
}