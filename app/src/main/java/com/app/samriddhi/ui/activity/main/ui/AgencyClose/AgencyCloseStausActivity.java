package com.app.samriddhi.ui.activity.main.ui.AgencyClose;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.R;
import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.DropdownMenuAdapter;
import com.app.samriddhi.base.adapter.ShowRequestNewAgencyListAdapter;
import com.app.samriddhi.databinding.ActivityAgencyCloseBinding;
import com.app.samriddhi.databinding.ActivityAgencyCloseStausBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.auth.SplashActivity;
import com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule.ShowRequestListActivity;
import com.app.samriddhi.ui.activity.main.ui.Grievance.RequestDetailAdapter;
import com.app.samriddhi.ui.activity.main.ui.Grievance.RequestDetailModal;
import com.app.samriddhi.ui.activity.main.ui.HomeActivity;
import com.app.samriddhi.ui.activity.main.ui.LanguageActivity;
import com.app.samriddhi.ui.model.DropDownModel;
import com.app.samriddhi.ui.view.IView;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Credentials;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AgencyCloseStausActivity extends BaseActivity implements DropdownMenuAdapter.OnMeneuClickListnser {

    private ArrayList<DropDownModel> stateMaster;
    private ArrayList<DropDownModel> unitMaster;

    private String dropSelectType = "";
    private String unitId = "", stateId = "";

    private AgencyCloseStausActivity context;


    ActivityAgencyCloseStausBinding binding;
    LinearLayoutManager linearLayoutManager;
    List<AgencyCloseStatusModal> listModal;
    AgencyCloseDetailAdapter requestDetailAdapter;
    int number = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.activity_agency_close_staus);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_agency_close_staus);
        this.context = this;
        stateMaster = new ArrayList<>();
        unitMaster = new ArrayList<>();


        binding.tvState.setOnClickListener(v -> {
            dropSelectType = "state";
            Util.showDropDown(stateMaster, "Select State", context, this::onOptionClick);
        });


        binding.tvUnit.setOnClickListener(v -> {
            dropSelectType = "unit";
            Util.showDropDown(unitMaster, "Select unit", context, this::onOptionClick);
        });
        initView();
        getState();
    }

    public void initView() {
        binding.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });


        if (PreferenceManager.getPrefUserType(AgencyCloseStausActivity.this).equals("UH")) {

            binding.relAddRequest.setVisibility(View.VISIBLE);
        } else if (PreferenceManager.getPrefUserType(this).equals("EX")
                || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("CE")
                || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("UE")
                || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("CI")) {

            binding.relAddRequest.setVisibility(View.VISIBLE);
        } else {
            binding.relAddRequest.setVisibility(View.GONE);
        }

        binding.relAddRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(AgencyCloseStausActivity.this, AgencyCloseActivity.class));
            }
        });

        binding.recyclerview.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(AgencyCloseStausActivity.this);
        binding.recyclerview.setLayoutManager(linearLayoutManager);

        getRequest();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // getRequest();
    }

    public void getState() {
        stateMaster.clear();
        // Calling JSON10011137MP
        //enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getStateMaster(PreferenceManager.getAgentId(this));
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;


                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        Log.e("sgfsdgsdgs", jsonObject.toString());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("state_name"));

                                // dropDownModel.setName(objStr.getJSONObject("fields").getString("jj_flag"));

                                stateMaster.add(dropDownModel);

                            }
                        }

                    } catch (JSONException e) {
                        Log.e("sdgsdgsdg", e.getMessage());
                    }
                    //   enableLoadingBar(false);
                } else {

                    try {
                        Toast.makeText(AgencyCloseStausActivity.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        Toast.makeText(AgencyCloseStausActivity.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {

                Toast.makeText(AgencyCloseStausActivity.this, R.string.err_ifsc_code_wrong, Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void getUnit(String stateId) {
        unitMaster.clear();
        Log.e("wetwetwt", "Unit");
      /*  AndroidNetworking.post("http://dev.dbsamriddhi.in/agency/getUnit/"+PreferenceManager.getAgentId(this)+"/"+stateId)
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("wetwetwt", response.toString());
                            JSONArray jsonArray = new JSONArray(response.getString("data"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject objStr = jsonArray.getJSONObject(i);
                                JSONObject jsonObject=new JSONObject(objStr.getString("fields"));
                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(jsonObject.getString("unit_name"));
                                dropDownModel.setName(jsonObject.getString("jj_flag"));
                                unitMaster.add(dropDownModel);

                            }
                        } catch (Exception ex) {
                            Log.e("wetwetwt", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        Log.e("wetwetwt", anError.getMessage());
                    }
                });*/

        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getUnit(PreferenceManager.getAgentId(context), stateId);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("unit_name"));

                                dropDownModel.setName(objStr.getJSONObject("fields").getString("jj_flag"));

                                unitMaster.add(dropDownModel);

                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //   enableLoadingBar(false);
                } else {

                    try {
                        Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {

                Toast.makeText(context, R.string.err_ifsc_code_wrong, Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void getRequest() {

        enableLoadingBar(true);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("user_id", PreferenceManager.getAgentId(AgencyCloseStausActivity.this));
            jsonObject.put("access_type_id", PreferenceManager.getPrefUserType(AgencyCloseStausActivity.this));
            jsonObject.put("state", stateId);
            jsonObject.put("unit", unitId);
            Log.e("asdasdada", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

       // AndroidNetworking.post(PreferenceManager.getBaseURL1(this) + "dash/api/agency-closure-status-data")
        AndroidNetworking.post(Constant.BASE_PORTAL_URL + "dash/api/agency-closure-status-data")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .addJSONObjectBody(jsonObject)
                .setTag("Request")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {


                        Log.e("rtrtrtr", response.toString());

                        try {
                            listModal = new ArrayList<>();
                            JSONArray jsonArray = new JSONArray(response.getString("results"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                AgencyCloseStatusModal agencyCloseStatusModal = new AgencyCloseStatusModal();
                                if (PreferenceManager.getPrefUserType(AgencyCloseStausActivity.this).equals("HO")) {

                                    if (!jsonObject.getString("SOHApproval").equals("2")) {  /*Approved by SOH*/

                                        agencyCloseStatusModal.setId(jsonObject.getString("id"));
                                        agencyCloseStatusModal.setUnit(jsonObject.getString("unit_name"));
                                        agencyCloseStatusModal.setState_name(jsonObject.getString("state_name"));
                                        agencyCloseStatusModal.setClosure_type(jsonObject.getString("reason_name"));
                                        agencyCloseStatusModal.setAgency_code(jsonObject.getString("agency_code"));
                                        agencyCloseStatusModal.setAgency_name(jsonObject.getString("agency_name"));
                                        agencyCloseStatusModal.setAgent_name(jsonObject.getString("agent_name"));
                                        agencyCloseStatusModal.setCopies(jsonObject.getString("no_of_copies"));
                                        agencyCloseStatusModal.setStart_date(jsonObject.getString("agency_start_date"));
                                        agencyCloseStatusModal.setEnd_date(jsonObject.getString("closing_date"));
                                        agencyCloseStatusModal.setTotal_time(jsonObject.getString("total_time_spent_with_us"));
                                        agencyCloseStatusModal.setAsd(jsonObject.getString("asd"));
                                        agencyCloseStatusModal.setOutstanding(jsonObject.getString("total_outstanding"));
                                        agencyCloseStatusModal.setDocument(jsonObject.getString("resignation_upload"));
                                        agencyCloseStatusModal.setReason_id(jsonObject.getString("reason_id"));
                                        agencyCloseStatusModal.setNominee_name(jsonObject.getString("nominee_name"));
                                        agencyCloseStatusModal.setNominee_relationship(jsonObject.getString("nominee_relationship"));
                                        agencyCloseStatusModal.setAgency_remark(jsonObject.getString("reason"));

                                        agencyCloseStatusModal.setCreate_date(jsonObject.getString("create_date"));
                                        agencyCloseStatusModal.setCreated_by(jsonObject.getString("create_user"));
                                        agencyCloseStatusModal.setStatus(jsonObject.getString("status"));

                                        agencyCloseStatusModal.setUoh_Staus(jsonObject.getString("UOHApproval"));
                                        agencyCloseStatusModal.setUoh_remark(jsonObject.getString("UOHRemark"));
                                        agencyCloseStatusModal.setUOHName(jsonObject.getString("UOHUpdateBy"));
                                        agencyCloseStatusModal.setUoh_updated_time(jsonObject.getString("UOHUpdateOn"));

                                        agencyCloseStatusModal.setSoh_status(jsonObject.getString("SOHApproval"));
                                        agencyCloseStatusModal.setSoh_remark(jsonObject.getString("SOHRemark"));
                                        agencyCloseStatusModal.setSohName(jsonObject.getString("SOHUpdateBy"));
                                        agencyCloseStatusModal.setSoh_updated_time(jsonObject.getString("SOHUpdateOn"));

                                        agencyCloseStatusModal.setHo_status(jsonObject.getString("HOApproval"));
                                        agencyCloseStatusModal.setHOName(jsonObject.getString("HOUpdateBy"));
                                        agencyCloseStatusModal.setHo_remark(jsonObject.getString("HORemark"));
                                        agencyCloseStatusModal.setHo_update_time(jsonObject.getString("HOUpdateOn"));

                                        agencyCloseStatusModal.setUfh_status(jsonObject.getString("UFHApproval"));
                                        agencyCloseStatusModal.setUfh_remark(jsonObject.getString("UFHRemark"));
                                        agencyCloseStatusModal.setUFHName(jsonObject.getString("UFHUpdateBy"));
                                        agencyCloseStatusModal.setUfh_updated_time(jsonObject.getString("UFHUpdateOn"));


                                        agencyCloseStatusModal.setCbr_status(jsonObject.getString("CBRApproval"));
                                        agencyCloseStatusModal.setCbr_reamrk(jsonObject.getString("CBRRemark"));
                                        agencyCloseStatusModal.setCBRName(jsonObject.getString("CBRUpdateBy"));
                                        agencyCloseStatusModal.setCbr_updated_time(jsonObject.getString("CBRUpdateOn"));

                                        agencyCloseStatusModal.setEst_bill_amt(jsonObject.getString("est_bill_amt"));
                                        agencyCloseStatusModal.setTotal_ots(jsonObject.getString("total_ots"));
                                        agencyCloseStatusModal.setUnbilled_amount(jsonObject.getString("unbilled_amount"));
                                        agencyCloseStatusModal.setRemark_new(jsonObject.getString("remark"));
                                        agencyCloseStatusModal.setBalance_amount(jsonObject.getString("balance_amount"));
                                        agencyCloseStatusModal.setAgency_location(jsonObject.getString("agency_location"));
                                        agencyCloseStatusModal.setRequest_date(jsonObject.getString("request_date"));

                                        listModal.add(agencyCloseStatusModal);
                                    }
                                } else {
                                    agencyCloseStatusModal.setId(jsonObject.getString("id"));
                                    agencyCloseStatusModal.setUnit(jsonObject.getString("unit_name"));
                                    agencyCloseStatusModal.setState_name(jsonObject.getString("state_name"));

                                    agencyCloseStatusModal.setClosure_type(jsonObject.getString("reason_name"));
                                    agencyCloseStatusModal.setAgency_code(jsonObject.getString("agency_code"));
                                    agencyCloseStatusModal.setAgency_name(jsonObject.getString("agency_name"));
                                    agencyCloseStatusModal.setAgent_name(jsonObject.getString("agent_name"));
                                    agencyCloseStatusModal.setCopies(jsonObject.getString("no_of_copies"));
                                    agencyCloseStatusModal.setStart_date(jsonObject.getString("agency_start_date"));
                                    agencyCloseStatusModal.setEnd_date(jsonObject.getString("closing_date"));
                                    agencyCloseStatusModal.setTotal_time(jsonObject.getString("total_time_spent_with_us"));
                                    agencyCloseStatusModal.setAsd(jsonObject.getString("asd"));
                                    agencyCloseStatusModal.setOutstanding(jsonObject.getString("total_outstanding"));
                                    agencyCloseStatusModal.setDocument(jsonObject.getString("resignation_upload"));
                                    agencyCloseStatusModal.setReason_id(jsonObject.getString("reason_id"));
                                    agencyCloseStatusModal.setNominee_name(jsonObject.getString("nominee_name"));
                                    agencyCloseStatusModal.setNominee_relationship(jsonObject.getString("nominee_relationship"));
                                    agencyCloseStatusModal.setAgency_remark(jsonObject.getString("reason"));

                                    agencyCloseStatusModal.setCreate_date(jsonObject.getString("create_date"));
                                    agencyCloseStatusModal.setCreated_by(jsonObject.getString("create_user"));
                                    agencyCloseStatusModal.setStatus(jsonObject.getString("status"));

                                    agencyCloseStatusModal.setUoh_Staus(jsonObject.getString("UOHApproval"));
                                    agencyCloseStatusModal.setUoh_remark(jsonObject.getString("UOHRemark"));
                                    agencyCloseStatusModal.setUOHName(jsonObject.getString("UOHUpdateBy"));
                                    agencyCloseStatusModal.setUoh_updated_time(jsonObject.getString("UOHUpdateOn"));

                                    agencyCloseStatusModal.setSoh_status(jsonObject.getString("SOHApproval"));
                                    agencyCloseStatusModal.setSoh_remark(jsonObject.getString("SOHRemark"));
                                    agencyCloseStatusModal.setSohName(jsonObject.getString("SOHUpdateBy"));
                                    agencyCloseStatusModal.setSoh_updated_time(jsonObject.getString("SOHUpdateOn"));

                                    agencyCloseStatusModal.setHo_status(jsonObject.getString("HOApproval"));
                                    agencyCloseStatusModal.setHOName(jsonObject.getString("HOUpdateBy"));
                                    agencyCloseStatusModal.setHo_remark(jsonObject.getString("HORemark"));
                                    agencyCloseStatusModal.setHo_update_time(jsonObject.getString("HOUpdateOn"));

                                    agencyCloseStatusModal.setUfh_status(jsonObject.getString("UFHApproval"));
                                    agencyCloseStatusModal.setUfh_remark(jsonObject.getString("UFHRemark"));
                                    agencyCloseStatusModal.setUFHName(jsonObject.getString("UFHUpdateBy"));
                                    agencyCloseStatusModal.setUfh_updated_time(jsonObject.getString("UFHUpdateOn"));

                                    agencyCloseStatusModal.setCbr_status(jsonObject.getString("CBRApproval"));
                                    agencyCloseStatusModal.setCbr_reamrk(jsonObject.getString("CBRRemark"));
                                    agencyCloseStatusModal.setCBRName(jsonObject.getString("CBRUpdateBy"));
                                    agencyCloseStatusModal.setCbr_updated_time(jsonObject.getString("CBRUpdateOn"));

                                    agencyCloseStatusModal.setEst_bill_amt(jsonObject.getString("est_bill_amt"));
                                    agencyCloseStatusModal.setTotal_ots(jsonObject.getString("total_ots"));
                                    agencyCloseStatusModal.setUnbilled_amount(jsonObject.getString("unbilled_amount"));
                                    agencyCloseStatusModal.setRemark_new(jsonObject.getString("remark"));
                                    agencyCloseStatusModal.setBalance_amount(jsonObject.getString("balance_amount"));
                                    agencyCloseStatusModal.setAgency_location(jsonObject.getString("agency_location"));
                                    agencyCloseStatusModal.setRequest_date(jsonObject.getString("request_date"));
                                    listModal.add(agencyCloseStatusModal);
                                }

                            }


                            requestDetailAdapter = new AgencyCloseDetailAdapter(listModal, AgencyCloseStausActivity.this);
                            binding.recyclerview.setHasFixedSize(true);
                            binding.recyclerview.setAdapter(requestDetailAdapter);

                            enableLoadingBar(false);
                            if (listModal.size() == 0) {
                                binding.txtNotfound.setVisibility(View.VISIBLE);
                            } else {
                                binding.txtNotfound.setVisibility(View.GONE);
                            }
                        } catch (Exception ex) {
                            enableLoadingBar(false);
                            Log.e("rtrtrtr", ex.getMessage());
                        }

                    }


                    @Override
                    public void onError(ANError anError) {
                        enableLoadingBar(false);
                        Log.e("rtrtrtr", anError.getMessage());
                    }
                });

    }


    public void enableLoadingBar(boolean enable) {
        if (enable) {
            loadProgressBar(null, null, false);
        } else {
            dismissProgressBar();
        }
    }

    @Override
    public void onOptionClick(DropDownModel data) {
        if (dropSelectType.equalsIgnoreCase("state")) {

            stateId = data.getId();
            binding.tvState.setText(data.getDescription());
            getUnit(stateId);
            unitId="";
            binding.tvUnit.setText("Unit");
            getRequest();

        }

        if (dropSelectType.equalsIgnoreCase("unit")) {

            unitId = data.getId();
            binding.tvUnit.setText(data.getDescription());
            Log.e("Vuduwehr", unitId);

            getRequest();
        }

        Util.hideDropDown();
    }
}