package com.app.samriddhi.ui.presenter;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.response.JsonArrayResponse;
import com.app.samriddhi.base.BasePresenter;
import com.app.samriddhi.ui.model.BankListModel;
import com.app.samriddhi.ui.view.IBankListView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BankListPresenter extends BasePresenter<IBankListView> {

    /**
     * provides the banks list for netbanking
     */
    public void getBankList() {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getBankList().enqueue(new Callback<JsonArrayResponse<BankListModel>>() {
            @Override
            public void onResponse(Call<JsonArrayResponse<BankListModel>> call, Response<JsonArrayResponse<BankListModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body() != null)
                            getView().onSuccess(response.body().list);
                        else
                            getView().onError(response.body().replyMsg);
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonArrayResponse<BankListModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }
}
