package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class UserCrmModel extends BaseModel {
    @SerializedName("results")
    @Expose
    private List<CRMModel> results = null;

    public UserCrmModel() {
        this.results = new ArrayList<CRMModel>();
        this.results.add(new CRMModel());
    }

    public List<CRMModel> getResults() {
        return results;
    }

    public void setResults(List<CRMModel> results) {
        this.results = results;
    }
}
