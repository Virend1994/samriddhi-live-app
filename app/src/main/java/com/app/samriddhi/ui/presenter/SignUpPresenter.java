package com.app.samriddhi.ui.presenter;

import android.util.Log;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.response.BaseResponse;
import com.app.samriddhi.api.response.JsonObjectResponse;
import com.app.samriddhi.base.BasePresenter;
import com.app.samriddhi.ui.model.UserModel;
import com.app.samriddhi.ui.view.ISignUpView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUpPresenter extends BasePresenter<ISignUpView> {

    /**
     * Get the otp on registered phone number
     * @param agentId  entered  agent id
     * @param mobileNumber user entered mobile number
     */
    public void getOtp(String agentId, String mobileNumber) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getOtp(agentId, mobileNumber).enqueue(new Callback<JsonObjectResponse<UserModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<UserModel>> call, Response<JsonObjectResponse<UserModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onSuccess(response.body().body);
                        else
                            getView().onRegisterSuccess(response.body().replyMsg);
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<UserModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

    /**
     * register the user in app
     * @param userModel users data
     */

    public void registerUser(UserModel userModel) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().registerUser(userModel).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        Log.e("on Success", "==== " + response.body().replyMsg);
                        getView().onRegisterSuccess(response.body().replyMsg);
                        //   getView().onSuccess(response.body().replyMsg);
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });

    }

    /**
     * get otp on user phone in forgot password case
     * @param mobileNumber user registered mobile  number
     */

    public void getOtpForForgot(String mobileNumber) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getOtpForForgotPassword(mobileNumber).enqueue(new Callback<JsonObjectResponse<UserModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<UserModel>> call, Response<JsonObjectResponse<UserModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onSuccess(response.body().body);
                        else
                            getView().onRegisterSuccess(response.body().replyMsg);
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<UserModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

    /**
     * submit the changed password on server
     * @param userModel users data
     */
    public void submitPassword(UserModel userModel) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().updatePassword(userModel).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        Log.e("on Success", "==== " + response.body().replyMsg);
                        getView().onRegisterSuccess(response.body().replyMsg);

                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }
}
