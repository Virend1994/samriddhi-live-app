package com.app.samriddhi.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.BaseFragment;
import com.app.samriddhi.databinding.FragmentDashboardBinding;
import com.app.samriddhi.events.CoachMarksEvent;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.DrawerAdapter;
import com.app.samriddhi.ui.activity.auth.LoginActivity;
import com.app.samriddhi.ui.activity.main.ui.ActivityGrievance;
import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AgencyCloseStausActivity;
import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AppsContants;
import com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule.ShowRequestListActivity;
import com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule.newRquestActivity;
import com.app.samriddhi.ui.activity.main.ui.AgentChildSelectionActivity;
import com.app.samriddhi.ui.activity.main.ui.AgentListActivity;
import com.app.samriddhi.ui.activity.main.ui.BillOutAgentListActivity;
import com.app.samriddhi.ui.activity.main.ui.BillingActivity;
import com.app.samriddhi.ui.activity.main.ui.ContactUsActivity;
import com.app.samriddhi.ui.activity.main.ui.GraphActivity;
import com.app.samriddhi.ui.activity.main.ui.Grievance.GrievanceTabActivity;
import com.app.samriddhi.ui.activity.main.ui.Grievance.HelpSupportStatus;
import com.app.samriddhi.ui.activity.main.ui.HomeActivity;
import com.app.samriddhi.ui.activity.main.ui.InVoiceActivity;
import com.app.samriddhi.ui.activity.main.ui.LanguageActivity;
import com.app.samriddhi.ui.activity.main.ui.LedgerActivity;
import com.app.samriddhi.ui.activity.main.ui.MisEntry.MisEntryActivity;
import com.app.samriddhi.ui.activity.main.ui.NoOfCopiesActivity;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.DailyPoActivity;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.PendingOrdersActivity;
import com.app.samriddhi.ui.activity.main.ui.ProfileActivity;
import com.app.samriddhi.ui.activity.main.ui.SalesOrgCityActivity;
import com.app.samriddhi.ui.activity.main.ui.UserCityActivity;
import com.app.samriddhi.ui.activity.main.ui.UserStateActivity;
import com.app.samriddhi.ui.model.DashBoardData;
import com.app.samriddhi.ui.model.DashBoardModel;
import com.app.samriddhi.ui.model.DrawerModel;
import com.app.samriddhi.ui.model.DropDownModel;
import com.app.samriddhi.ui.model.LogoutReqModel;
import com.app.samriddhi.ui.model.LogoutResultModel;
import com.app.samriddhi.ui.model.MonthListModel;
import com.app.samriddhi.ui.presenter.DashboardPresenter;
import com.app.samriddhi.ui.presenter.UserLogoutPresenter;
import com.app.samriddhi.ui.view.IDashboardView;
import com.app.samriddhi.ui.view.IUserLogoutView;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.CustomShowCaseView;
import com.app.samriddhi.util.CustonShowCaseViewCircle;
import com.app.samriddhi.util.MyYAxisValueFormatter;
import com.app.samriddhi.util.SystemUtility;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.formatter.XAxisValueFormatter;
import com.github.mikephil.charting.utils.ViewPortHandler;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallState;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import org.apache.harmony.awt.datatransfer.TextFlavor;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;

import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
import static android.os.Build.VERSION.SDK_INT;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import okhttp3.Credentials;

public class DashboardFragment extends BaseFragment implements IDashboardView, IUserLogoutView, View.OnClickListener, DrawerAdapter.ItemClickListener {
    public static final int APP_UPDATE_REQUEST_CODE = 404;
    FragmentDashboardBinding mBinding;
    DashboardPresenter mPresenter;
    DashBoardModel dashBoardModel;
    ShowcaseView showcaseView;
    int showCount = 0;
    RelativeLayout.LayoutParams secondParams = null;
    String currentVersion = "";
    String[] arrItems = new String[]{};
    ArrayList<DrawerModel> arrDrawerData;
    Dialog dialog;
    public static String strPopupDismiss = "";

    /**
     * Guidance buttons click
     */
   /* View.OnClickListener coachClicks = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (showCount) {
                case 1:
                    showcaseView.hide();
                    showcaseView = new ShowcaseView.Builder(getActivity())
                            .setTarget(new ViewTarget(R.id.rl_today_copies, getActivity()))
                            .setContentTitle(getResources().getString(R.string.str_no_copies_coach_msg)).blockAllTouches()
                            .setStyle(R.style.CustomCoachMarksTheme)
                            .setShowcaseDrawer(new CustomShowCaseView(mBinding.rlTodayCopies, getResources()))
                            .build();
                    showcaseView.forceTextPosition(ShowcaseView.BELOW_SHOWCASE);
                    RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                    params.setMargins(15, 10, 10, 30);
                    showcaseView.setButtonPosition(params);
                    showcaseView.addView(getButton(), secondParams);
                    break;

                    case 2:
                    showcaseView.hide();
                    showcaseView = new ShowcaseView.Builder(getActivity())
                            .setTarget(new ViewTarget(R.id.linear_bil, getActivity()))
                            .setContentTitle(getResources().getString(R.string.str_view_billing)).blockAllTouches()
                            .setStyle(R.style.CustomCoachMarksTheme)
                            .setShowcaseDrawer(new CustomShowCaseView(mBinding.layoutBilling.linearBil, getResources()))
                            .build();
                    showcaseView.forceTextPosition(ShowcaseView.BELOW_SHOWCASE);
                    RelativeLayout.LayoutParams params1 = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    params1.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
                    params1.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
                    params1.setMargins(15, 10, 10, 30);
                    showcaseView.setButtonPosition(params1);
                    showcaseView.addView(getButton(), secondParams);
                    break;

                case 3:
                    showcaseView.hide();
                    showcaseView = new ShowcaseView.Builder(getActivity())
                            .setTarget(new ViewTarget(R.id.linear_outstanding, getActivity()))
                            .setContentTitle(getResources().getString(R.string.str_view_ledger)).blockAllTouches()
                            .setStyle(R.style.CustomCoachMarksTheme)
                            .setShowcaseDrawer(new CustomShowCaseView(mBinding.layoutBilling.linearOutstanding, getResources()))
                            .build();
                    showcaseView.forceTextPosition(ShowcaseView.BELOW_SHOWCASE);

                    break;
                default:
                    showcaseView.hide();
                    break;
            }
        }
    };*/
    private AppUpdateManager appUpdateManager;
    private InstallStateUpdatedListener installStateUpdatedListener;
    UserLogoutPresenter mPresenterLogout;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_dashboard, container, false);
        mBinding.setFragment(this);


        if (PreferenceManager.getPrefUserType(getActivity()).equals("AG")) { /* Do not show to executive */
            mBinding.rlCopyTrends.setVisibility(View.VISIBLE);
            mBinding.linearAgency.setVisibility(View.GONE);
            mBinding.rlReports.setVisibility(View.GONE);
            mBinding.rlPedingAgency.setVisibility(View.GONE);
            mBinding.rlDailySupply.setVisibility(View.GONE);
            mBinding.linearHoSoh.setVisibility(View.GONE);

        } else if (PreferenceManager.getPrefUserType(getActivity()).equals("UF") || PreferenceManager.getPrefUserType(getActivity()).equals("CB")) { /* Do not show to executive */
            mBinding.rlCopyTrends.setVisibility(View.VISIBLE);
            mBinding.linearAgency.setVisibility(View.GONE);
            mBinding.rlReports.setVisibility(View.GONE);
            mBinding.rlPedingAgency.setVisibility(View.GONE);
            mBinding.rlDailySupply.setVisibility(View.GONE);
            mBinding.linearHoSoh.setVisibility(View.GONE);
            mBinding.rlGrivence.setVisibility(View.GONE);

        } else if (PreferenceManager.getPrefUserType(getActivity()).equals("SH")) { /* Do not show to SOH */
            mBinding.rlGrivence.setVisibility(View.GONE);
            mBinding.linearAgency.setVisibility(View.GONE);
            mBinding.rlReports.setVisibility(View.GONE);
            mBinding.rlPedingAgency.setVisibility(View.GONE);
            mBinding.rlDailySupply.setVisibility(View.GONE);
            mBinding.linearHoSoh.setVisibility(View.VISIBLE);
            // mBinding.rlCopyTrends.setVisibility(View.GONE);


        } else if (PreferenceManager.getPrefUserType(getActivity()).equals("HO") || PreferenceManager.getPrefUserType(getActivity()).equals("CO")) { /* Do not show to HO */
            mBinding.rlGrivence.setVisibility(View.GONE);
            mBinding.linearAgency.setVisibility(View.GONE);
            mBinding.rlReports.setVisibility(View.GONE);
            mBinding.rlPedingAgency.setVisibility(View.GONE);
            mBinding.rlDailySupply.setVisibility(View.GONE);
            mBinding.linearHoSoh.setVisibility(View.VISIBLE);
            //  mBinding.rlCopyTrends.setVisibility(View.GONE);
        } else if (PreferenceManager.getPrefUserType(getActivity()).equals("UH")
                || PreferenceManager.getPrefUserType(getActivity()).equals("EX")
                || PreferenceManager.getPrefUserType(getActivity()).equals("CI")
                || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CE")
                || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UE")) { /* Do not show to Agent */

            mBinding.rlReports.setVisibility(View.VISIBLE);
            mBinding.linearAgency.setVisibility(View.VISIBLE);
            mBinding.rlGrivence.setVisibility(View.GONE);
            mBinding.rlPedingAgency.setVisibility(View.VISIBLE);
            mBinding.rlDailySupply.setVisibility(View.VISIBLE);
            mBinding.linearHoSoh.setVisibility(View.GONE);
            // mBinding.rlCopyTrends.setVisibility(View.GONE);
        } else {

            mBinding.rlAgencyRequest.setVisibility(View.VISIBLE);
            //  mBinding.rlCopyTrends.setVisibility(View.GONE);
        }

        return mBinding.getRoot();
    }

    /**
     * initialize and bind views
     */
    private void initViews() {
        Log.e("ZDSvsdfdsfsf", PreferenceManager.getPrefUserType(getActivity()));
        mPresenter = new DashboardPresenter();
        mPresenter.setView(this);
        mBinding.rlTodayCopies.setOnClickListener(this);
        mBinding.layoutBilling.linearBil.setOnClickListener(this);
        mBinding.layoutBilling.linearOutstanding.setOnClickListener(this);
        mBinding.rlGrivence.setOnClickListener(this);
        mBinding.rlReports.setOnClickListener(this);
        mBinding.rlPedingAgency.setOnClickListener(this);

        mBinding.rlCloseAgency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // startActivity(new Intent(getActivity(), AgencyCloseStausActivity.class));
                ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), AgencyCloseStausActivity.class, false);

            }
        });

        mBinding.rlProposedAgency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManager.setAgencyMenuType(getActivity(), "Proposed");
                ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), ShowRequestListActivity.class, false);
            }
        });

        mBinding.rlPedingAgency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreferenceManager.setAgencyMenuType(getActivity(), "Pending");
                ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), ShowRequestListActivity.class, false);

            }
        });
        mBinding.rlDailySupply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), DailyPoActivity.class, false);
            }
        });
        mBinding.rlAgencyRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), newRquestActivity.class, false);
            }
        });



        mBinding.swipeRefresh.setColorSchemeColors(Color.RED, Color.RED, Color.RED);
        mBinding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.getNoOfCopies(PreferenceManager.getAgentId(getActivity()));
                mBinding.swipeRefresh.setRefreshing(false);

            }
        });
        //  showCoachMarksOnScreen();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(strPopupDismiss.equals("1")||strPopupDismiss.equals("2")){
            dialog.dismiss();
        }
        // mPresenter.getNoOfCopies(PreferenceManager.getAgentId(getActivity()));
        //checkNewAppVersionState();

        //   checkIfLoggedInOtherDevice();

        /*  if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }*/
        /*new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                showCoachMarksOnScreen();
            }
        }, 2000);*/
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        //checkForAppUpdate();
        initViews();
        arrDrawerData = new ArrayList<>();
        //TODO: comment if dont want loader on dashboard
        mPresenter.getNoOfCopies(PreferenceManager.getAgentId(getActivity()));

        /*checkNewAppVersionState();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }*/
    }

    @Override
    public Context getContext() {
        return getActivity();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.rl_today_copies:
                if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("AG")) {
                    Log.e("sdgsdgsg", "1");
                    ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), NoOfCopiesActivity.class, false);
                } else if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("EX")
                        || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CE")
                        || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UE")
                        || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CI")) {
                    Log.e("sdgsdgsg", "2");
                    startActivityWithParams(AgentListActivity.class, Constant.DashBoardNoOfCopies);
                }
//                    else if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UH")) {
//                        Log.e("sdgsdgsg", "3");
//                        startActivityWithParams(SalesOrgCityActivity.class, Constant.DashBoardNoOfCopies);
//                    }
                else if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CO") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("HO") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("SH") || (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UH"))) {
                    Log.e("sdgsdgsg", "4");
                    startActivityWithParams(UserStateActivity.class, Constant.DashBoardNoOfCopies);

                }
                break;
            case R.id.linear_bil:

                if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("AG"))
                    ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), BillingActivity.class, false);
                else {
                    if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("EX")
                            || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CE")
                            || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UE")
                            || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CI")) {
                        startActivityWithParams(BillOutAgentListActivity.class, Constant.DashBoardBill);
                    } else if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CO") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("HO") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("SH") || (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UH"))) {
                        startActivityWithParams(UserStateActivity.class, Constant.DashBoardBill);
                    }
                }
                break;
            case R.id.linear_outstanding:
                if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("AG"))
                    ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), LedgerActivity.class, false);
                else {
                    if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("EX") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CE")
                            || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UE")
                            || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CI")) {
                        startActivityWithParams(BillOutAgentListActivity.class, Constant.DashBoardOutstanding);
                    } else if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CO") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("HO") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("SH") || (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UH"))) {
                        startActivityWithParams(UserStateActivity.class, Constant.DashBoardOutstanding);
                    }
                }
                break;
            case R.id.rl_grivence:

                ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), GrievanceTabActivity.class, false);

//                 if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("AG"))
//                    ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), ActivityGrievance.class, false);
//                else {
//                    if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("EX") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CE") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UE")) {
//                        startActivityWithParams(AgentListActivity.class, Constant.DashBoardGrievance);
//                    } else if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("SH")) {
//                        startActivityWithParams(UserCityActivity.class, Constant.DashBoardGrievance);
//                    } else if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CO")) {
//                        startActivityWithParams(UserStateActivity.class, Constant.DashBoardGrievance);
//                    }
//                }
                //   onInfo(getResources().getString(R.string.coming_soon));

                break;
            case R.id.rl_reports:
                //onInfo(getResources().getString(R.string.coming_soon));
                //   Toast.makeText(getActivity(), "Coming Soon", Toast.LENGTH_SHORT).show();
                ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), MisEntryActivity.class, false);

                break;
            default:
                break;

        }
    }

    /**
     * launch the intent with animations
     *
     * @param destinationClass from where to go
     * @param clickFieldName   clicked item screen name
     */
    private void startActivityWithParams(Class destinationClass, String clickFieldName) {
        startActivity(new Intent(getActivity(), destinationClass).putExtra(Constant.SCREEN_NAME, clickFieldName));
        getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    /**
     * set Chart data
     *
     * @param monthData copies monthly chart data
     */
    private void setData(ArrayList<MonthListModel> monthData) {

        if (getActivity() != null && !getActivity().isFinishing()) {

            mBinding.barChart.setDoubleTapToZoomEnabled(false);
            mBinding.barChart.setPinchZoom(false);
            mBinding.barChart.setHovered(false);
            mBinding.barChart.setDrawGridBackground(false);

            XAxis xAxis = mBinding.barChart.getXAxis();

            xAxis.setDrawLabels(true);

            xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

            xAxis.setDrawGridLines(false);

            xAxis.setTextSize(8f);

            YAxis yAxis = mBinding.barChart.getAxisRight();

            yAxis.setEnabled(false);

            YAxis yAxis1 = mBinding.barChart.getAxisLeft();

            yAxis1.setEnabled(false);

            ArrayList<BarEntry> values = new ArrayList<>();

            ArrayList<String> arrLAbles = new ArrayList<>();

            ArrayList<MonthListModel> arrMonth = new ArrayList<>();

            if (monthData.size() > 0) {

                Collections.reverse(monthData);

                int sizeCount = monthData.size() > 7 ? 7 : monthData.size();

                for (int i = 0; i < sizeCount; i++) {

                    arrMonth.add(monthData.get(i));
                }
                Collections.reverse(arrMonth);
                for (int i = 0; i < arrMonth.size(); i++) {
                    arrLAbles.add((arrMonth.get(i).getFormattedMonth()));
                    values.add(new BarEntry(Float.parseFloat(arrMonth.get(i).getSoldCopies()), i));
                }

                xAxis.setValueFormatter(new XAxisValueFormatter() {
                    @Override
                    public String getXValue(String original, int index, ViewPortHandler viewPortHandler) {
                        return arrLAbles.get(index);
                    }
                });

                BarDataSet barData = new BarDataSet(values, "Average copies");
                BarData data = new BarData(arrLAbles, barData);
                barData.setValueTextSize(10);
                MyYAxisValueFormatter f = new MyYAxisValueFormatter();
                data.setValueFormatter(f);
                barData.setHighLightColor(getContext().getResources().getColor(R.color.colorPrimary));
                barData.setColor(getContext().getResources().getColor(R.color.colorPrimary));

                mBinding.barChart.animateY(1000);

                mBinding.barChart.setData(data);
            }
            mBinding.barChart.setDescription("");
        }
    }


    @Override
    public void onItemClick(int position) {

        if ((arrDrawerData.get(position).item_name).equalsIgnoreCase(getResources().getString(R.string.str_profile))) {
            showProfileAccordingToUser();

        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.agent_selection))) {
            ((HomeActivity) getActivity()).closeDrawer();
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), AgentChildSelectionActivity.class, false);

        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_dashboard))) {
            ((HomeActivity) getActivity()).closeDrawer();
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_billing_drawer))) {

            ((HomeActivity) getActivity()).closeDrawer();
            openBillingScreen();
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_outstanding_drawer))) {

            ((HomeActivity) getActivity()).closeDrawer();
            openOutstandingScreen();
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_ledger))) {
            ((HomeActivity) getActivity()).closeDrawer();
            openLedger();
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_language))) {
            ((HomeActivity) getActivity()).closeDrawer();
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), LanguageActivity.class, false);
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_contact_us))) {
            ((HomeActivity) getActivity()).closeDrawer();
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), ContactUsActivity.class, false);
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_open_agency))) {
            ((HomeActivity) getActivity()).closeDrawer();
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), newRquestActivity.class, false);
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_close_agency))) {
            ((HomeActivity) getActivity()).closeDrawer();
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), AgencyCloseStausActivity.class, false);
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_new_agency_perposed))) {
            ((HomeActivity) getActivity()).closeDrawer();

            PreferenceManager.setAgencyMenuType(getActivity(), "Proposed");
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), ShowRequestListActivity.class, false);
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_pending_agency))) {
            ((HomeActivity) getActivity()).closeDrawer();
            PreferenceManager.setAgencyMenuType(getActivity(), "Pending");
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), ShowRequestListActivity.class, false);
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_daily_sup_and_hold))) {
            ((HomeActivity) getActivity()).closeDrawer();
            // ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), OrderSupply.class, false);
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), DailyPoActivity.class, false);
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_approval))) {
            ((HomeActivity) getActivity()).closeDrawer();
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), PendingOrdersActivity.class, false);
            // ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), UohOrderSupply.class, false);
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_grievance))) {
            //  ((HomeActivity) getActivity()).closeDrawer();
            //   openGrievanceFlow();

            ((HomeActivity) getActivity()).closeDrawer();
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), GrievanceTabActivity.class, false);
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_grievance_status))) {
            ((HomeActivity) getActivity()).closeDrawer();
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), HelpSupportStatus.class, false);
        } else if (arrDrawerData.get(position).item_name.equalsIgnoreCase(getResources().getString(R.string.str_graph))) {
            ((HomeActivity) getActivity()).closeDrawer();
            ((HomeActivity) getActivity()).startActivityAnimation(getActivity(), GraphActivity.class, false);
        } else {
            onInfo(getResources().getString(R.string.coming_soon));
        }

    }

    /**
     * app update check
     */
    private void openOutstandingScreen() {

        if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("AG"))
            ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), LedgerActivity.class, false);
        else {
            if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("EX")
                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CE")
                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UE")
                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CI")) {
                startActivityWithParams(BillOutAgentListActivity.class, Constant.DashBoardOutstanding);
            } else if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CO") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("HO") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("SH") || (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UH"))) {
                startActivityWithParams(UserStateActivity.class, Constant.DashBoardOutstanding);
            }
        }
    }

    private void openBillingScreen() {

        if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("AG"))
            ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), BillingActivity.class, false);
        else {
            if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("EX")
                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CE")
                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UE")
                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CI")) {
                startActivityWithParams(AgentListActivity.class, Constant.DashBoardBill);
            } else if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("SH") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UH")) {
                startActivityWithParams(UserCityActivity.class, Constant.DashBoardBill);
            } else if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CO") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("HO")) {
                startActivityWithParams(UserStateActivity.class, Constant.DashBoardBill);
            }
        }
    }

    /**
     * show the ledger screen  for all type of users
     */

    private void openLedger() {
        // ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), StatesActivity.class, false);
        if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("AG"))
            ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), LedgerActivity.class, false);
        else {
            if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("EX")
                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CE") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UE") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CI")) {
                startActivityWithParams(AgentListActivity.class, Constant.DashBoardOutstanding);
            } else if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("SH") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UH")) {
                startActivityWithParams(UserCityActivity.class, Constant.DashBoardOutstanding);
            } else if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CO") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("HO")) {
                startActivityWithParams(UserStateActivity.class, Constant.DashBoardOutstanding);
            }
        }
    }


    private void showProfileAccordingToUser() {
//        if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("AG"))
//            ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), ProfileActivity.class, false);
//        else {
//            if (PreferenceManager.getPrefUserType(getActivity()).equals("EX")
//                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CE")
//                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UE")
//                    || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CI")) {
//
//                startActivityWithParams(AgentListActivity.class, Constant.DrawerProfile);
//            } else if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("SH") || PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("UH")) {
//                startActivityWithParams(UserCityActivity.class, Constant.DrawerProfile);
//
//            } else if (PreferenceManager.getPrefUserType(getActivity()).equalsIgnoreCase("CO")) {
//                startActivityWithParams(UserStateActivity.class, Constant.DrawerProfile);
//            }
//        }
        ((BaseActivity) getActivity()).startActivityAnimation(getActivity(), ProfileActivity.class, false);

        ((HomeActivity) getActivity()).closeDrawer();
    }

    @Override
    public void onLogoutSuccess(LogoutResultModel logoutResultModel) {
        if (logoutResultModel.message.equals("success")) {
            String language = PreferenceManager.getAppLang(getActivity());
            PreferenceManager.clearAll(getActivity());
            PreferenceManager.setAppLang(getActivity(), language);
            startActivity(new Intent(getActivity(), LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
            getActivity().finish();
        }

    }

    class GetVersionCode extends AsyncTask<Void, String, String> {
        long time = System.currentTimeMillis();

        @Override
        protected String doInBackground(Void... voids) {
            String newVersion = null;
            try {
                Document document = Jsoup.connect("https://play.google.com/store/apps/details?id=com.app.samriddhi" + "&" + time + "")
                        .timeout(30000)
                        .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                        .referrer("http://www.google.com")
                        .get();
                if (document != null) {
                    Elements element = document.getElementsContainingOwnText("Current Version");
                    for (Element ele : element) {
                        if (ele.siblingElements() != null) {
                            Elements sibElemets = ele.siblingElements();
                            for (Element sibElemet : sibElemets) {
                                newVersion = sibElemet.text();
                            }
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return newVersion;

        }

        @Override

        protected void onPostExecute(String onlineVersion) {
            super.onPostExecute(onlineVersion);
            //Log.e("dfhgjdfgdf", onlineVersion);
            //Log.e("dfhgjdfgdf", currentVersion);

            if (onlineVersion != null && !onlineVersion.isEmpty()) {
                if (!currentVersion.equals(onlineVersion)) {
                    //show anything
                    Log.e("dfhgjdfgdf", "Update App");

                    final Dialog dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.update_app_alert);
                    TextView txDescription = dialog.findViewById(R.id.txDescription);
                    txDescription.setText("Update" + " " + onlineVersion + " " + "is available to download.Downloading the latest update you will get the latest features,improvements and fixed bugs.");
                    TextView btnUpdate = dialog.findViewById(R.id.btnUpdate);
                    btnUpdate.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.app.samriddhi"));
                            startActivity(intent);
                        }
                    });
                    dialog.show();
                } else {


                    Log.e("dfhgjdfgdf", "Updated+");
                }
            }

            Log.d("update", "Current version " + currentVersion + "playstore version " + onlineVersion);
        }
    }

    private void checkForAppUpdate() {
        // Creates instance of the manager.
        appUpdateManager = AppUpdateManagerFactory.create(getActivity());

        // Returns an intent object that you use to check for an update.
        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();

        // Create a listener to track request state updates.
        installStateUpdatedListener = new InstallStateUpdatedListener() {
            @Override
            public void onStateUpdate(InstallState installState) {

                // Show module progress, log state, or install the update.
                if (installState.installStatus() == InstallStatus.DOWNLOADED)
                    // After the update is downloaded, show a notification
                    // and request user confirmation to restart the app.
                    unregisterInstallStateUpdListener();
            }
        };

        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                // Request the update.
                if (appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {

                    // Before starting an update, register a listener for updates.
                    appUpdateManager.registerListener(installStateUpdatedListener);
                    // Start an update.
                    startAppUpdateFlexible(appUpdateInfo);
                } else if (appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)) {
                    // Start an update.
                    startAppUpdateImmediate(appUpdateInfo);
                }

            }
        });
    }

    /**
     * App immediate update method
     *
     * @param appUpdateInfo appUpdated Info
     */

    private void startAppUpdateImmediate(AppUpdateInfo appUpdateInfo) {
        try {
            appUpdateManager.startUpdateFlowForResult(
                    appUpdateInfo,
                    AppUpdateType.IMMEDIATE,
                    // The current activity making the update request.
                    getActivity(),
                    // Include a request code to later monitor this update request.
                    APP_UPDATE_REQUEST_CODE);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, final int resultCode, Intent intent) {
        //  super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == APP_UPDATE_REQUEST_CODE) {
            if (resultCode != RESULT_OK) { //RESULT_OK / RESULT_CANCELED / RESULT_IN_APP_UPDATE_FAILED
                Log.d("Update flow failed ", "! Result code:  " + resultCode);
                // If the update is cancelled or fails,
                // you can request to start the update again.
                unregisterInstallStateUpdListener();
            }
        }
    }

    /**
     * app flexible update popup method
     *
     * @param appUpdateInfo appupdate info
     */
    private void startAppUpdateFlexible(AppUpdateInfo appUpdateInfo) {
        try {
            appUpdateManager.startUpdateFlowForResult(
                    appUpdateInfo,
                    AppUpdateType.FLEXIBLE,
                    // The current activity making the update request.
                    getActivity(),
                    // Include a request code to later monitor this update request.
                    APP_UPDATE_REQUEST_CODE);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
            unregisterInstallStateUpdListener();
        }
    }

    /**
     * this is for check the app update on play store
     */

    private void checkNewAppVersionState() {
        appUpdateManager
                .getAppUpdateInfo()
                .addOnSuccessListener(
                        appUpdateInfo -> {
                            //FLEXIBLE:
                            // If the update is downloaded but not installed,
                            // notify the user to complete the update.
                            if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                                unregisterInstallStateUpdListener();
                            }
                            //IMMEDIATE:
                            if (appUpdateInfo.updateAvailability()
                                    == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                                // If an in-app update is already running, resume the update.
                                startAppUpdateImmediate(appUpdateInfo);
                            }
                        });

    }

    /**
     * unregister the app update listener
     */

    private void unregisterInstallStateUpdListener() {
        if (appUpdateManager != null && installStateUpdatedListener != null)
            appUpdateManager.unregisterListener(installStateUpdatedListener);
    }

    @Override
    public void onSuccess(DashBoardData dashBoardModel, String lastMonthBillAmt, String hintCount, Integer openCount, Integer closeCount) {
        if (dashBoardModel != null) {

            this.dashBoardModel = dashBoardModel.getData();
            mBinding.setItem(this.dashBoardModel);
            mBinding.layoutBilling.setItem(this.dashBoardModel);
            mBinding.txtOpenCount.setText("" + openCount + " " + getResources().getString(R.string.str_open));
            mBinding.txtCloseCount.setText("" + closeCount + " " + getResources().getString(R.string.str_close));
            mBinding.layoutBilling.setBilling(lastMonthBillAmt.isEmpty() ? "" : lastMonthBillAmt);
            //   mBinding.layoutBilling.setBilling(lastMonthBillAmt.isEmpty() ? "" : SystemUtility.formatDecimalValues(Float.parseFloat(lastMonthBillAmt)));




            /* To show daily po for SH only in Rajasthan */
            Log.e("GetFlat" + " " + "State_Flag" + " ", dashBoardModel.getState_flag());
            AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, MODE_PRIVATE);
            SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
            editor.putString(AppsContants.StateFlag, dashBoardModel.getState_flag());
            editor.commit();

            if (PreferenceManager.getPrefUserType(getActivity()).equals("SH")) {
                if (dashBoardModel.getState_flag().equals("1")) {   /* 1 only when rajasthan SH will login */
                    arrItems = (getActivity().getResources().getStringArray(R.array.arr_nav_items));
                    TypedArray imgs = getResources().obtainTypedArray(R.array.arr_nav_drawables);
                    for (int i = 0; i < arrItems.length; i++) {
                        if (!arrItems[i].equals("New Agency Request") && !arrItems[i].equals("Pending Agency") &&
                                !arrItems[i].equals("Help/Support") && !arrItems[i].equals("Help Support Status")
                                && !arrItems[i].equals("Daily Supply and Hold")) {
                            arrDrawerData.add(new DrawerModel(arrItems[i], imgs.getResourceId(i, -1)));
                        }
                    }
                    NavFragment.recycle_nav.setAdapter(new DrawerAdapter(getActivity(), arrDrawerData, this));
                }
            }
            try {
                PreferenceManager.setUserOustStnading(getActivity(), dashBoardModel.getData().getClaculatedOutStanding());
                PreferenceManager.setNotificationCount(getActivity(), dashBoardModel.getNotification_Count());
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (dashBoardModel.getMTDData().getMTDdata() != null) {
                mBinding.txtNoDataFound.setVisibility(View.GONE);
                mBinding.barChart.setVisibility(View.VISIBLE);
                setData(dashBoardModel.getMTDData().getMTDdata());
            } else {
                mBinding.txtNoDataFound.setVisibility(View.VISIBLE);
                mBinding.barChart.setVisibility(View.GONE);
            }
            ((HomeActivity) getActivity()).setHintCount(hintCount);

            if (PreferenceManager.getPrefUserType(getActivity()).equals("AG")) {

                if (dashBoardModel.getIsLastBillConfirmed().equals("false")) {
                    dialog = new Dialog(getActivity());
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.custum_popup);

                    Button dialog_cancel = (Button) dialog.findViewById(R.id.dialog_cancel);
                    dialog_cancel.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    });

                    Button dialog_ok = (Button) dialog.findViewById(R.id.dialog_ok);
                    dialog_ok.setOnClickListener(new View.OnClickListener() {
                        @SuppressLint("NewApi")
                        @Override
                        public void onClick(View v) {

                            strPopupDismiss="3";
                            startActivity(new Intent(getActivity(), InVoiceActivity.class)
                                    .putExtra("bill_number", dashBoardModel.getBillDataModals().get(0).getBill_No())
                                    .putExtra("bill_period", dashBoardModel.getBillDataModals().get(0).getBill_Period())
                                    .putExtra("bill_amount", dashBoardModel.getBillDataModals().get(0).getBill_Amount())
                                    .putExtra("copies", dashBoardModel.getBillDataModals().get(0).getBill_copies())
                                    .putExtra("Monat", dashBoardModel.getBillDataModals().get(0).getMonat())
                                    .putExtra("Gjahr", dashBoardModel.getBillDataModals().get(0).getGjahr())
                                    .putExtra("PayAmt", dashBoardModel.getBillDataModals().get(0).getBill_Amount()));
                            getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            //dialog.dismiss();


                        }
                    });

                    dialog.show();
                }
            }


            try {
                currentVersion = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
            new GetVersionCode().execute();

        }

    }

    /**
     * return the button for guidance
     * @return button
     */

  /*  public Button getButton() {
        final Button button = (Button) LayoutInflater.from(getActivity()).inflate(R.layout.showcase_custom_view_button, showcaseView, false);
        secondParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        secondParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        secondParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        secondParams.setMargins(15, 10, 10, 30);
        button.setOnClickListener(coachClicks);
        showCount++;
        return button;
    }*/

    /**
     * show the guidance on screen
     */

   /* private void showCoachMarksOnScreen() {
        try {
            showCount = 0;
            showcaseView = new ShowcaseView.Builder(getActivity())
                    .setTarget(new ViewTarget(R.id.txt_hint_counts, getActivity()))
                    .singleShot(PreferenceManager.PREF_DASHBOARD_SHOT)
                    .setContentTitle(getResources().getString(R.string.str_app_guide)).blockAllTouches()
                    .setStyle(R.style.CustomCoachMarksTheme)
                    .setShowcaseDrawer(new CustonShowCaseViewCircle(((HomeActivity) getActivity()).getHintId(), getResources()))
                    .build();
            showcaseView.forceTextPosition(ShowcaseView.BELOW_SHOWCASE);
            RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
            params.setMargins(15, 10, 10, 30);
            showcaseView.setButtonPosition(params);
            showcaseView.addView(getButton(), secondParams);
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }

    }*/

   /* @Subscribe(threadMode = ThreadMode.MAIN)
    public void onCoachMarks(CoachMarksEvent event) {
        showCoachMarksOnScreen();
    }*/
    @Override
    public void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }


    public void checkIfLoggedInOtherDevice() {
        Log.e("DeviceToken=>>", "Token" + " " + SystemUtility.getDeviceId(getActivity()));
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("device_id", SystemUtility.getDeviceId(getActivity()));
            jsonObject.put("BPCODE", PreferenceManager.getAgentId(getActivity()));

            Log.e("sfasfasaasfafa", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post(Constant.BASE_PORTAL_URL + "logoutStatusCheck/")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            Log.e("sfasfasaasfafa", response.toString());

                            if (response.getString("login_status").equals("true")) {
                                Log.e("sfasfasaasfafa", "True");

                                final Dialog dialog = new Dialog(getActivity());
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setCancelable(false);
                                dialog.setContentView(R.layout.update_app_alert);
                                TextView txDescription = dialog.findViewById(R.id.txDescription);
                                txDescription.setText(response.getString("message"));
                                TextView btnUpdate = dialog.findViewById(R.id.btnUpdate);
                                btnUpdate.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {

                                        LogoutReqModel logoutReqModel = new LogoutReqModel();
                                        logoutReqModel.flag = 1;
                                        logoutReqModel.user_id = PreferenceManager.getAgentId(getActivity());

                                        Gson gson = new Gson();
                                        String data1 = gson.toJson(logoutReqModel);
                                        Log.e("asfgasgagasgaagasg", data1);

                                        mPresenter.getLogoutStatus(logoutReqModel);
                                    }
                                });
                                dialog.show();
                            } else {
                                Log.e("sfasfasaasfafa", "False");


                            }

                        } catch (Exception ex) {
                            Log.e("sfasfasaasfafa", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        Log.e("sfasfasaasfafa", anError.getMessage());
                    }
                });

    }


}

