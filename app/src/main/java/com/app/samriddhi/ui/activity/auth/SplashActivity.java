package com.app.samriddhi.ui.activity.auth;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;

import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.ConfigActivity;
import com.app.samriddhi.ui.activity.main.ui.HomeActivity;
import com.app.samriddhi.ui.activity.main.ui.LanguageActivity;
import com.app.samriddhi.ui.model.ServerPortModel;
import com.app.samriddhi.ui.presenter.SplashPresenter;
import com.app.samriddhi.ui.view.ISplashView;

public class SplashActivity extends BaseActivity implements ISplashView {
    SplashPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        mPresenter = new SplashPresenter();
        mPresenter.setView(this);
        mPresenter.getSapPortal();

    }

    @Override
    public Context getContext() {
        return this;
    }


    /*Successfully connected to SAP server*/
    @Override
    public void onSuccess(ServerPortModel body) {
        if (body != null) {
            PreferenceManager.setPrefServerPort(this, body.getServerHost());
            PreferenceManager.setPrefServerPwd(this, body.getServerPassword());
            PreferenceManager.setPrefServerUserName(this, body.getServerUsername());
            PreferenceManager.setScreenshotAllowed(this, body.screenshot);
        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (PreferenceManager.getIsUserLoggedIn(SplashActivity.this)) {
                    startActivityAnimation(SplashActivity.this, HomeActivity.class, false);
                   // startActivityAnimation(SplashActivity.this, ConfigActivity.class, false);

                } else
                    startActivityAnimation(SplashActivity.this, LanguageActivity.class, false);
             //   startActivityAnimation(SplashActivity.this, ConfigActivity.class, false);

                finish();
            }
        }, 2000);


    }

    /* When not able to connect to SAP server get error from server*/
    @Override
    public void onError(String reason) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (PreferenceManager.getIsUserLoggedIn(SplashActivity.this)) {
                    startActivityAnimation(SplashActivity.this, HomeActivity.class, false);
                    //startActivityAnimation(SplashActivity.this, ConfigActivity.class, false);
                } else {

                    startActivityAnimation(SplashActivity.this, LanguageActivity.class, false);
                    //startActivityAnimation(SplashActivity.this, ConfigActivity.class, false);

                }
                finish();
            }
        }, 2000);

    }
}
