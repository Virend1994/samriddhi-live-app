package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class StateResultModel extends BaseModel {
    @SerializedName("results")
    @Expose
    private List<StateListModel> results = null;

    public List<StateListModel> getResults() {
        return results;
    }

    public void setResults(List<StateListModel> results) {
        this.results = results;
    }
}
