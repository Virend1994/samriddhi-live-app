package com.app.samriddhi.ui.model;

public class CirReqDroppingSetModel {
    public String dropping_id;
    public String route_id;
    public String contact_no;
    public String sub_agent_code;
    public String sub_agent_name;
    public String sub_agent_contact_no;
    public String sub_agent_copies;
    public String agent_location;


    public String getAgent_location() {
        return agent_location;
    }

    public void setAgent_location(String agent_location) {
        this.agent_location = agent_location;
    }



    public String edition_id,edition_idName,edition_copies;
    public int self_copies;

    public String getEdition_id() {
        return edition_id;
    }

    public void setEdition_id(String edition_id) {
        this.edition_id = edition_id;
    }

    public String getEdition_idName() {
        return edition_idName;
    }

    public void setEdition_idName(String edition_idName) {
        this.edition_idName = edition_idName;
    }

    public String getEdition_copies() {
        return edition_copies;
    }

    public void setEdition_copies(String edition_copies) {
        this.edition_copies = edition_copies;
    }

    public String getDropping_id() {
        return dropping_id;
    }

    public void setDropping_id(String dropping_id) {
        this.dropping_id = dropping_id;
    }

    public String getRoute_id() {
        return route_id;
    }

    public void setRoute_id(String route_id) {
        this.route_id = route_id;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getSub_agent_code() {
        return sub_agent_code;
    }

    public void setSub_agent_code(String sub_agent_code) {
        this.sub_agent_code = sub_agent_code;
    }

    public String getSub_agent_name() {
        return sub_agent_name;
    }

    public void setSub_agent_name(String sub_agent_name) {
        this.sub_agent_name = sub_agent_name;
    }

    public String getSub_agent_contact_no() {
        return sub_agent_contact_no;
    }

    public void setSub_agent_contact_no(String sub_agent_contact_no) {
        this.sub_agent_contact_no = sub_agent_contact_no;
    }

    public String getSub_agent_copies() {
        return sub_agent_copies;
    }

    public void setSub_agent_copies(String sub_agent_copies) {
        this.sub_agent_copies = sub_agent_copies;
    }

    public int getSelf_copies() {
        return self_copies;
    }

    public void setSelf_copies(int self_copies) {
        this.self_copies = self_copies;
    }
}
