package com.app.samriddhi.ui.activity.main.ui;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.core.content.FileProvider;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.RecyclerViewArrayAdapter;
import com.app.samriddhi.databinding.ActivityFilterCopiesBinding;
import com.app.samriddhi.permission.PermissionUtils;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.model.CopiesData;
import com.app.samriddhi.ui.model.ExcelDataModel;
import com.app.samriddhi.ui.model.NumberOfCopiesModel;
import com.app.samriddhi.util.AlertUtility;
import com.app.samriddhi.util.CustonShowCaseViewCircle;
import com.app.samriddhi.util.SystemUtility;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

import java.io.File;
import java.util.ArrayList;

public class NoOfCopiesFilterActivity extends BaseActivity implements View.OnClickListener {
    ActivityFilterCopiesBinding mBinding;
    NumberOfCopiesModel mCopiesData;
    String agentName = "";
    ArrayList<ExcelDataModel> arrData = new ArrayList<>();
    PermissionUtils permissionUtils;
    ShowcaseView showcaseView;
    int showCount = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_filter_copies);
        if (getIntent() != null && getIntent().hasExtra("copiesData")) {
            mCopiesData = (getIntent().getParcelableExtra("copiesData"));
            agentName = getIntent().getStringExtra("selectedName");
        }
        initView();
    }

    /**
     * initialize and bind views
     */
    private void initView() {
        mBinding.toolbarFilter.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mBinding.toolbarFilter.rlExcel.setVisibility(View.VISIBLE);
        mBinding.toolbarFilter.imgExcel.setOnClickListener(this);
        mBinding.toolbarFilter.txtTittle.setText(getResources().getString(R.string.str_circulation_copies_tittle) + " " + agentName);
        mBinding.recycleData.setLayoutManager(new LinearLayoutManager(this));
        mBinding.recycleData.setAdapter(new RecyclerViewArrayAdapter(mCopiesData.getResults()));
        permissionUtils = new PermissionUtils(this);
        //showCoachMarksOnScreen();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_excel:
                askStoragePermission(201);
                break;
            default:
                break;
        }
    }


    @Override
    public void NeverAskAgain(int request_code) {
       //will override
    }

    @Override
    public void PermissionGranted(int request_code) {
        downloadFile();
    }

    @Override
    public void PermissionDenied(int request_code) {
        //will override
    }

    /**
     * download the filter copies data in PDF format
     */
    private void downloadFile() {
        if (mCopiesData.getResults() != null) {
            File file = null;
            loadProgressBar(null, "Please wait..", false);
            for (CopiesData result : mCopiesData.getResults()) {
                ExcelDataModel excelDataModel = new ExcelDataModel(SystemUtility.getMonthFormattedCopies(result.getOrdDate()), result.getPaidCopy(), result.getPartner());
                arrData.add(excelDataModel);
            }
            if (arrData != null && arrData.size() > 0) {
                file = SystemUtility.convertJsonToExcel(arrData);
            }
            File finalFile = file;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    dismissProgressBar();
                    AlertUtility.showAlertFileOpen(NoOfCopiesFilterActivity.this, String.format(getResources().getString(R.string.str_excel_download_success), " InternalStorage/Samriddhi/CopiesData "), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            openFile(FileProvider.getUriForFile(NoOfCopiesFilterActivity.this, "com.app.samriddhi.fileprovider", finalFile));

                        }
                    });
                    //onInfo(String.format(getResources().getString(R.string.str_excel_download_success), " InternalStorage/Samriddhi/CopiesData "));
                }
            }, 3000);

        }
    }

    /**
     * guidance on screen
     */

    private void showCoachMarksOnScreen() {
        showCount = 0;
        showcaseView = new ShowcaseView.Builder(this)
                .setTarget(new ViewTarget(R.id.rl_excel, this))
                .singleShot(PreferenceManager.PREF_NO_OF_COPIES_FILTER_SHOT)
                .setContentTitle(getResources().getString(R.string.str_download_pdf)).blockAllTouches()
                .setStyle(R.style.CustomCoachMarksTheme)
                .setShowcaseDrawer(new CustonShowCaseViewCircle(mBinding.toolbarFilter.rlExcel, getResources()))
                .build();
        showcaseView.forceTextPosition(ShowcaseView.BELOW_SHOWCASE);

    }

    /**
     * open the created pdf file
     * @param uri  pdf file uri
     */
    private void openFile(Uri uri) {
        Log.e("create pdf uri path==>", "" + uri);
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(uri, "application/vnd.ms-excel");
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            startActivity(intent);

        } catch (ActivityNotFoundException e) {
            Toast.makeText(getApplicationContext(),
                    "There is no any PDF Viewer",
                    Toast.LENGTH_SHORT).show();
        }
    }
}
