package com.app.samriddhi.ui.model;

import android.os.Parcel;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HintFieldsModel extends BaseModel {

    public static final Creator<HintFieldsModel> CREATOR = new Creator<HintFieldsModel>() {
        @Override
        public HintFieldsModel createFromParcel(Parcel source) {
            return new HintFieldsModel(source);
        }

        @Override
        public HintFieldsModel[] newArray(int size) {
            return new HintFieldsModel[size];
        }
    };
    @SerializedName("Hint_Srno")
    @Expose
    private Integer hintSrno;
    @SerializedName("Hint_Text")
    @Expose
    private String hintText;
    @SerializedName("Hint_Status")
    @Expose
    private Integer hintStatus;

    public HintFieldsModel() {
    }

    protected HintFieldsModel(Parcel in) {
        super(in);
        this.hintSrno = (Integer) in.readValue(Integer.class.getClassLoader());
        this.hintText = in.readString();
        this.hintStatus = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public Integer getHintSrno() {
        return hintSrno;
    }

    public void setHintSrno(Integer hintSrno) {
        this.hintSrno = hintSrno;
    }

    public String getHintText() {
        return hintText;
    }

    public void setHintText(String hintText) {
        this.hintText = hintText;
    }

    public Integer getHintStatus() {
        return hintStatus;
    }

    public void setHintStatus(Integer hintStatus) {
        this.hintStatus = hintStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeValue(this.hintSrno);
        dest.writeString(this.hintText);
        dest.writeValue(this.hintStatus);
    }
}
