package com.app.samriddhi.ui.activity.main.ui.Grievance.multipleimage;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;
import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;

import java.io.File;
import java.util.List;

public class GiftAdapter extends RecyclerView.Adapter<GiftAdapter.MyViewHolder> {
    private ImageView cross;
    private final List<String> arrayList;
    Context c;
    String s;
    String first = "";

    String first1 = "http://ruparnatechnology.com/rental/image/";

    public GiftAdapter(Context c, List<String> mDetials123, String s) {
        this.arrayList = mDetials123;
        this.c = c;
        this.s = s;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView giftprofileimg;
        public View cross;


        public MyViewHolder(View view) {
            super(view);


            giftprofileimg = view.findViewById(R.id.giftprofileimg);
            cross = view.findViewById(R.id.cross);


        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.giftitem, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        if (s.equals("0")) {

            Log.e("sfsdfsdfs", "If");
            holder.giftprofileimg.setImageURI(Uri.parse(new File(arrayList.get(position)).toString()));
        } else {
            Log.e("sfsdfsdfs","else");
            Glide.with(c).load(arrayList.get(position)).into(holder.giftprofileimg);
        }
        holder.cross.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  Toast.makeText(c, ""+position, Toast.LENGTH_SHORT).show();
                removeAt(position);
            }
        });


        holder.giftprofileimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showAlert(Uri.parse(new File(arrayList.get(position)).toString()));
            }
        });

        //Picasso.with(c).load(arrayList.get(position)).into(holder.giftprofileimg);


    }



    public void showAlert(Uri parse) {


        View popupView = null;
        Log.e("sfsafasasfa",parse.toString());

        popupView = LayoutInflater.from(c).inflate(R.layout.enlarge_image, null);

        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0);

        ImageView closeImg = popupView.findViewById(R.id.closeImg);
        closeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });

        PhotoView imageView = popupView.findViewById(R.id.imageView);
        imageView.setImageURI(parse);
       // Glide.with(c).load(status).into(imageView);



    }


    public void removeAt(int position) {
        arrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, arrayList.size());
    }


    public int getItemCount() {
        return (null != arrayList ? arrayList.size() : 0);

    }
}
