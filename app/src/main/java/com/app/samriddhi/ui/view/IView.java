package com.app.samriddhi.ui.view;

import android.content.Context;

public interface IView {

    /*Required to implement for Activity
     * Not required to implement for Fragment
     * */
    Context getContext();

    /*implemented in BaseActivity and BaseFragment*/
    void enableLoadingBar(boolean enable);

    /*implemented in BaseActivity and BaseFragment*/
    void onError(String reason);

    /*implemented in BaseActivity and BaseFragment*/
    void onInfo(String message);


    /*implemented in BaseActivity and BaseFragment*/
    void onTokenExpired();

    /*implemented in BaseActivity and BaseFragment*/
    void onForceUpdate();

    /*implemented in BaseActivity and BaseFragment*/
    void onSoftUpdate();
}
