package com.app.samriddhi.ui.view;

import com.app.samriddhi.ui.model.BillingModel;

public interface IBillingView extends IView {
    void onSuccess(BillingModel mData, String outStanding, Boolean payUMoney, Boolean bankList);
}
