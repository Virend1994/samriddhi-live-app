package com.app.samriddhi.ui.model;

public class VillageModel {

    String village_name;
    int village_copy;

    public String getVillage_name() {
        return village_name;
    }

    public void setVillage_name(String village_name) {
        this.village_name = village_name;
    }

    public int getVillage_copy() {
        return village_copy;
    }

    public void setVillage_copy(int village_copy) {
        this.village_copy = village_copy;
    }
}
