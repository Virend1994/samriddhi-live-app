package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.app.samriddhi.util.SystemUtility;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LedgerTransModel extends BaseModel {

    @SerializedName("Kunnr")
    @Expose
    private String kunnr;

    @SerializedName("VDate")
    @Expose
    private String budat;

    @SerializedName("Narration")
    @Expose
    private String xblnr;


    @SerializedName("Debit")
    @Expose
    private String Debit;

    @SerializedName("Credit")
    @Expose
    private String Credit;

    @SerializedName("Balance")
    @Expose
    private String Balance;

    public String getKunnr() {
        return kunnr;
    }

    public void setKunnr(String kunnr) {
        this.kunnr = kunnr;
    }

    public String getBudat() {
        return budat;
    }

    public void setBudat(String budat) {
        this.budat = budat;
    }



    public String getXblnr() {
        return xblnr;
    }

    public void setXblnr(String xblnr) {
        this.xblnr = xblnr;
    }
    public String getFormattedAmount() {
        if (Debit.isEmpty())
            return "";
        else
            return SystemUtility.getFormatDecimalValue(Float.parseFloat(Debit));
        // return String.valueOf(SystemUtility.formatDecimalValues(Float.parseFloat(dmshb)));
    }
    public String getFormattedCredit() {
        if (Credit.isEmpty())
            return "";
        else
            return SystemUtility.getFormatDecimalValue(Float.parseFloat(Credit));
        // return String.valueOf(SystemUtility.formatDecimalValues(Float.parseFloat(dmshb)));
    }

    public String getFormattedBalance() {
        if (Balance.isEmpty())
            return "";
        else
            return SystemUtility.getFormatDecimalValue(Float.parseFloat(Balance));
        // return String.valueOf(SystemUtility.formatDecimalValues(Float.parseFloat(dmshb)));
    }



    public void setDebit(String debit) {
        Debit = debit;
    }


    public void setCredit(String credit) {
        Credit = credit;
    }


    public void setBalance(String balance) {
        Balance = balance;
    }
}
