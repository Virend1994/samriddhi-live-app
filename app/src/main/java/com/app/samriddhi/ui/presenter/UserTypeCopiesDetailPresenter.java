package com.app.samriddhi.ui.presenter;

import android.util.Log;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.response.JsonObjectResponse;
import com.app.samriddhi.base.BasePresenter;
import com.app.samriddhi.ui.model.AgentResultModel;
import com.app.samriddhi.ui.model.BillOutAgentResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCDistWiseResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCResultModel;
import com.app.samriddhi.ui.model.BillingOutstandingResultModel;
import com.app.samriddhi.ui.model.CashCreditResultModel;
import com.app.samriddhi.ui.model.CityResultModel;
import com.app.samriddhi.ui.model.SalesDistrictWiseResultModel;
import com.app.samriddhi.ui.model.SalesOrgCityResultModel;
import com.app.samriddhi.ui.model.StateResultModel;
import com.app.samriddhi.ui.model.UsersResultModel;
import com.app.samriddhi.ui.view.IUserTypeCopiesView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserTypeCopiesDetailPresenter extends BasePresenter<IUserTypeCopiesView> {

    /**
     * provides the all agents list
     * @param agentId internal user id
     * @param a type
     * @param fromDate from date
     * @param toDate todate
     * @param searchKey  city ya state code
     */
    public void getAgentList(String agentId, String a, String fromDate, String toDate, String searchKey) {
        getView().enableLoadingBar(true);

        Log.e("sdfsdgdfg","Agent_id"+" "+agentId);
        Log.e("sdfsdgdfg","A"+" "+a);
        Log.e("sdfsdgdfg","fromDate"+" "+fromDate);
        Log.e("sdfsdgdfg","toDate"+" "+toDate);
        Log.e("sdfsdgdfg","searchKey"+" "+searchKey);
        SamriddhiApplication.getmInstance().getApiService().getAgentList(agentId, a, fromDate, toDate, searchKey).enqueue(new Callback<JsonObjectResponse<AgentResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<AgentResultModel>> call, Response<JsonObjectResponse<AgentResultModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onAgentListSuccess(response.body().body);

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<AgentResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

    /**
     * get the states list
     * @param agentId internal user id
     */

    public void getStateList(String agentId) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getStateList(agentId).enqueue(new Callback<JsonObjectResponse<StateResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<StateResultModel>> call, Response<JsonObjectResponse<StateResultModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onStateSuccess(response.body().body);

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<StateResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

    /**
     * get the internal users city list
     * @param agentId internal user id
     * @param c type
     * @param fromDate from date
     * @param toDate todate
     * @param searchKey key state or city code
     */
    public void getCityList(String agentId, String c, String fromDate, String toDate, String searchKey) {

        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getCityList(agentId, c, fromDate, toDate, searchKey).enqueue(new Callback<JsonObjectResponse<CityResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<CityResultModel>> call, Response<JsonObjectResponse<CityResultModel>> response) {
                getView().enableLoadingBar(false);
                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onCitySuccess(response.body().body);

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<CityResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

    /**
     * get the state list for outstandings
     * @param agentId logged in user id
     * @param s type State
     */

    public void getOutstandingStateList(String agentId, String s) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getOutStandingStateList(agentId, s).enqueue(new Callback<JsonObjectResponse<StateResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<StateResultModel>> call, Response<JsonObjectResponse<StateResultModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onStateSuccess(response.body().body);

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<StateResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

    /**
     * outstanding city list
     * @param agentId logged in user id
     * @param c type City
     * @param searchKey key state or city
     */

    public void getOutStandingCityList(String agentId, String c, String searchKey) {

        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getOutStandingCityList(agentId, c, searchKey).enqueue(new Callback<JsonObjectResponse<CityResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<CityResultModel>> call, Response<JsonObjectResponse<CityResultModel>> response) {
                getView().enableLoadingBar(false);
                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onCitySuccess(response.body().body);

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<CityResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

    /**
     * get the outstanding Agents list
     * @param agentId logged in user id
     * @param a type Agent
     * @param searchKey state or city code
     */

    public void getOutStandingAgentList(String agentId, String a, String searchKey) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getOutstandingAgentList(agentId, a, searchKey).enqueue(new Callback<JsonObjectResponse<AgentResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<AgentResultModel>> call, Response<JsonObjectResponse<AgentResultModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onAgentListSuccess(response.body().body);

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<AgentResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

    /**
     * get the billings state list
     * @param agentId logged in user id
     */


    public void getBillingStateList(String agentId) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getBillingStateList(agentId).enqueue(new Callback<JsonObjectResponse<BillingOutstandingResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<BillingOutstandingResultModel>> call, Response<JsonObjectResponse<BillingOutstandingResultModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onStateSuccess(response.body().body);

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<BillingOutstandingResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

    /**
     * get billings city list
     * @param agentId logged in user id
     * @param stateCode state code
     */
    public void getBillingCityList(String agentId,  String stateCode) {

        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getBillingCityList(agentId,stateCode).enqueue(new Callback<JsonObjectResponse<CityResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<CityResultModel>> call, Response<JsonObjectResponse<CityResultModel>> response) {
                getView().enableLoadingBar(false);
                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onCitySuccess(response.body().body);

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<CityResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }


    /**
     * get all the agents list according to filter
     * @param agentId  logged in user id
     * @param a
     * @param fromDate from date
     * @param toDate Todate
     * @param searchKey state or city code
     */
    public void getBillingAgentList(String agentId, String a, String fromDate, String toDate, String searchKey) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getBillingAgentList(agentId, a, fromDate, toDate, searchKey).enqueue(new Callback<JsonObjectResponse<AgentResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<AgentResultModel>> call, Response<JsonObjectResponse<AgentResultModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onAgentListSuccess(response.body().body);

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<AgentResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }


    /**
     * grivance state lists
     * @param agentId logged in user id
     * @param s type State
     */
    public void getGrievanceStateList(String agentId, String s) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getGrievanceStateList(agentId, s).enqueue(new Callback<JsonObjectResponse<StateResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<StateResultModel>> call, Response<JsonObjectResponse<StateResultModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onStateSuccess(response.body().body);

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<StateResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }


    /**
     * Get all the city list according to filter
     * @param agentId logged in user id
     * @param c type City
     * @param searchKey state ya city code
     */
    public void getGrievanceCityList(String agentId, String c, String searchKey) {

        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getGrievanceCityList(agentId, c, searchKey).enqueue(new Callback<JsonObjectResponse<CityResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<CityResultModel>> call, Response<JsonObjectResponse<CityResultModel>> response) {
                getView().enableLoadingBar(false);
                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onCitySuccess(response.body().body);

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<CityResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }


    /**
     * get the agents list
     * @param agentId logg ed in User id
     * @param a type Agent
     * @param searchKey State ya city code
     */
    public void getGrievanceAgentList(String agentId, String a, String searchKey) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getGrievanceAgentList(agentId, a, searchKey).enqueue(new Callback<JsonObjectResponse<AgentResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<AgentResultModel>> call, Response<JsonObjectResponse<AgentResultModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onAgentListSuccess(response.body().body);

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<AgentResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

////Number of copies flow////////

    /**
     * get the credit or cash data lists
     * @param agentId logged in user id
     * @param a Type Agent
     * @param fromDate from date
     * @param toDate Todate
     * @param searchKey state or city code
     */

    public void getCashCreditData(String agentId, String a, String fromDate, String toDate, String searchKey) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getCrediCashData(agentId, a, fromDate, toDate, searchKey).enqueue(new Callback<JsonObjectResponse<CashCreditResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<CashCreditResultModel>> call, Response<JsonObjectResponse<CashCreditResultModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onCreditCashSuccess(response.body().body);

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<CashCreditResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

    /**
     * get the Sale group list
     * @param agentId logged in user id
     * @param stateCode state code
     * @param unitCode unit code
     */

    public void getSalesGroups(String agentId, String stateCode, String unitCode) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getSalesGroupUsers(agentId,stateCode, unitCode).enqueue(new Callback<JsonObjectResponse<UsersResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<UsersResultModel>> call, Response<JsonObjectResponse<UsersResultModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onSuccess(response.body());

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<UsersResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

    public void getBillOutCityUPC(String agentId, String stateCode, String unitCode) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getBillOutCityUPC(agentId,stateCode, unitCode).enqueue(new Callback<JsonObjectResponse<BillOutCityUPCResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<BillOutCityUPCResultModel>> call, Response<JsonObjectResponse<BillOutCityUPCResultModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onBillOUtCityUPCSuccess(response.body());

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<BillOutCityUPCResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

    /**
     * get the sales organizations list
     * @param agentId logged in user id
     * @param stateCode  stateCOde
     */

    public void getSalesOrgCity(String agentId, String stateCode) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getSalesOrgCity(agentId, stateCode).enqueue(new Callback<JsonObjectResponse<SalesOrgCityResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<SalesOrgCityResultModel>> call, Response<JsonObjectResponse<SalesOrgCityResultModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onSalesOrgCitySuccess(response.body().body);

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<SalesOrgCityResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

    /**
     * get the district list
     * @param agentId logged in user id
     * @param stateCode stateCode
     * @param unitCode  unit Code
     * @param marketCode market Code
     * @param cityUPC cityUPC code
     */
    public void getSalesDistrictWise(String agentId,String stateCode,String unitCode,String marketCode,String cityUPC) {
        getView().enableLoadingBar(true);

        SamriddhiApplication.getmInstance().getApiService().getSalesDistrictWise(agentId, stateCode, unitCode, marketCode,cityUPC).enqueue(new Callback<JsonObjectResponse<SalesDistrictWiseResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<SalesDistrictWiseResultModel>> call, Response<JsonObjectResponse<SalesDistrictWiseResultModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onSalesDistrictCopies(response.body().body);

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<SalesDistrictWiseResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }
    public void getBillOutCityUPCDist(String agentId,String stateCode,String unitCode,String dChannel,String cityUPC) {
        getView().enableLoadingBar(true);

        SamriddhiApplication.getmInstance().getApiService().getBillOutCityUPCDist(agentId, stateCode, unitCode, dChannel,cityUPC).enqueue(new Callback<JsonObjectResponse<BillOutCityUPCDistWiseResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<BillOutCityUPCDistWiseResultModel>> call, Response<JsonObjectResponse<BillOutCityUPCDistWiseResultModel>> response) {

                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onBillOutCityUPCDistSuccess(response.body().body);

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<BillOutCityUPCDistWiseResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

    /**
     * get the number of copies agent wise
     * @param agentId logged in user id
     * @param stateCode stateCode
     * @param unitCode  unit Code
     * @param marketCode market Code
     * @param cityUPC cityUPC code
     * @param salesCode salse code
     */
    public void getAgentWiseCopies(String agentId,  String stateCode, String unitCode, String marketCode,String cityUPC, String salesCode) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getAgentWiseList(agentId, stateCode, unitCode, marketCode,cityUPC, salesCode).enqueue(new Callback<JsonObjectResponse<AgentResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<AgentResultModel>> call, Response<JsonObjectResponse<AgentResultModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onAgentListSuccess(response.body().body);

                    }
                }
            }
            @Override
            public void onFailure(Call<JsonObjectResponse<AgentResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

    public void getBillOutAgentWiseCopies(String agentId,  String stateCode, String unitCode, String dChannel,String cityUPC, String salesCode) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getBillOutAgentWiseCopies(agentId, stateCode, unitCode, dChannel, cityUPC,salesCode).enqueue(new Callback<JsonObjectResponse<BillOutAgentResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<BillOutAgentResultModel>> call, Response<JsonObjectResponse<BillOutAgentResultModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onBillOutAgentListSuccess(response.body().body);

                    }
                }
            }
            @Override
            public void onFailure(Call<JsonObjectResponse<BillOutAgentResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }


    public void getAgentList(String agentId) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getAgentList(agentId).enqueue(new Callback<JsonObjectResponse<AgentResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<AgentResultModel>> call, Response<JsonObjectResponse<AgentResultModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onAgentListSuccess(response.body().body);

                    }
                }
            }
            @Override
            public void onFailure(Call<JsonObjectResponse<AgentResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

    public void getBillOutAgentList(String agentId) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getBillOutAgentList(agentId).enqueue(new Callback<JsonObjectResponse<BillOutAgentResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<BillOutAgentResultModel>> call, Response<JsonObjectResponse<BillOutAgentResultModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onBillOutAgentListSuccess(response.body().body);

                    }
                }
            }
            @Override
            public void onFailure(Call<JsonObjectResponse<BillOutAgentResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }
}
