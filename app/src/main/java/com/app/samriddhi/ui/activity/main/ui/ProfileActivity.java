package com.app.samriddhi.ui.activity.main.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.Spinner;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.databinding.FragmentProfileBinding;
import com.app.samriddhi.databinding.ItemChildInfoBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.model.CRMModel;
import com.app.samriddhi.ui.model.ProfileBrotherDetail;
import com.app.samriddhi.ui.model.ProfileChildModel;
import com.app.samriddhi.ui.model.ProfileFatherModel;
import com.app.samriddhi.ui.model.ProfileMotherModel;
import com.app.samriddhi.ui.model.ProfileSisterDetail;
import com.app.samriddhi.ui.model.UserCrmModel;
import com.app.samriddhi.ui.model.UserProfileModel;
import com.app.samriddhi.ui.presenter.UserProfilePresenter;
import com.app.samriddhi.ui.view.IUserProfileView;
import com.app.samriddhi.util.AlertUtility;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.FileUtil;
import com.app.samriddhi.util.StringUtility;
import com.app.samriddhi.util.SystemUtility;
import com.app.samriddhi.util.Util;
import com.github.chrisbanes.photoview.PhotoView;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.gson.Gson;
import com.iceteck.silicompressorr.SiliCompressor;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ProfileActivity extends BaseActivity implements View.OnClickListener, IUserProfileView, AdapterView.OnItemSelectedListener {
    FragmentProfileBinding mProfileBinding;
    UserProfilePresenter mPresenter;
    String agentId = "";
    UserProfileModel userData;
    ItemChildInfoBinding mBinding;
    ArrayList<ItemChildInfoBinding> arrChildBinding = new ArrayList<>();
    ArrayList<ProfileChildModel> arrChildData = new ArrayList<>();
    ArrayList<String> arrGender = new ArrayList<>();
    boolean isEditable = false, fromFirstTime = false;
    String panCardBitmap = "",strUserImage="";
    String strImage="";
    File fileUser,filePan;
    String status="";

    Uri selectedImage;



    private Bitmap bitmap;
    private File destination = null;
    private InputStream inputStreamImg;
    private String imgPath = null;

    ArrayList<Integer> arrNoOfChild = new ArrayList<>();
    Calendar agentCal = Calendar.getInstance(), spouseCal = Calendar.getInstance(), sisCal = Calendar.getInstance(),
            motherCal = Calendar.getInstance(), brotherCal = Calendar.getInstance(), fatherCal = Calendar.getInstance(),
            marriageAnniCal = Calendar.getInstance(), childCal = Calendar.getInstance();
    DatePickerDialog.OnDateSetListener spouseDateListner = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            spouseCal.set(Calendar.YEAR, year);
            spouseCal.set(Calendar.MONTH, monthOfYear);
            spouseCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            mProfileBinding.edSpouseDob.setText(SystemUtility.getMothFormattedCalenderDay(spouseCal));
        }

    };
    DatePickerDialog.OnDateSetListener agentDobListner = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            agentCal.set(Calendar.YEAR, year);
            agentCal.set(Calendar.MONTH, monthOfYear);
            agentCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            mProfileBinding.edAgentDob.setText(SystemUtility.getMothFormattedCalenderDay(agentCal));
        }

    };
    DatePickerDialog.OnDateSetListener fatherDob = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            fatherCal.set(Calendar.YEAR, year);
            fatherCal.set(Calendar.MONTH, monthOfYear);
            fatherCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            mProfileBinding.edFatherDob.setText(SystemUtility.getMothFormattedCalenderDay(fatherCal));
        }

    };
    DatePickerDialog.OnDateSetListener agentMotherDob = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            motherCal.set(Calendar.YEAR, year);
            motherCal.set(Calendar.MONTH, monthOfYear);
            motherCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            mProfileBinding.edMotherDob.setText(SystemUtility.getMothFormattedCalenderDay(motherCal));
        }

    };
    DatePickerDialog.OnDateSetListener agetntSisterDob = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            sisCal.set(Calendar.YEAR, year);
            sisCal.set(Calendar.MONTH, monthOfYear);
            sisCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            mProfileBinding.edSisterDob.setText(SystemUtility.getMothFormattedCalenderDay(sisCal));
        }

    };
    DatePickerDialog.OnDateSetListener brotherDob = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            // TODO Auto-generated method stub
            brotherCal.set(Calendar.YEAR, year);
            brotherCal.set(Calendar.MONTH, monthOfYear);
            brotherCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            mProfileBinding.edBrotherDate.setText(SystemUtility.getMothFormattedCalenderDay(brotherCal));
        }

    };
    DatePickerDialog.OnDateSetListener marriageAnniListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            // TODO Auto-generated method stub
            marriageAnniCal.set(Calendar.YEAR, year);
            marriageAnniCal.set(Calendar.MONTH, month);
            marriageAnniCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            mProfileBinding.edMarrigaeDate.setText(SystemUtility.getMothFormattedCalenderDay(marriageAnniCal));

        }
    };
    DatePickerDialog.OnDateSetListener onChildDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {

            int pos = (int) view.getTag();

            // TODO Auto-generated method stub
            childCal.set(Calendar.YEAR, year);
            childCal.set(Calendar.MONTH, month);
            childCal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            arrChildBinding.get(pos).edChildDob.setText(SystemUtility.getMothFormattedCalenderDay(childCal));

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mProfileBinding = DataBindingUtil.setContentView(this, R.layout.fragment_profile);
        mProfileBinding.toolbarProfile.imgBack.setOnClickListener(this);
        initView();

        mProfileBinding.btnProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                status="1"; /*user image*/
                selectImage();
            }
        });

        mProfileBinding.btnPanImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                status="2"; /*Pan image*/
                selectImage();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (mProfileBinding.toolbarProfile.txtProfileEdit.getText().equals(getResources().getString(R.string.str_editing))){
            AlertUtility.showAlertWithListeners(this, getResources().getString(R.string.profile_save_msg), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();


                }
            }, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    handleBack();

                }
            });
        }
        else
        {
        handleBack();
        }
    }

    private  void handleBack(){
        if (PreferenceManager.getPrefAgentParentValue(this).equalsIgnoreCase("Y")) {
            startActivity(new Intent(this, AgentChildSelectionActivity.class).putExtra("noBackHistory", true));
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            finish();
        } else {
            if (fromFirstTime) {
                startActivityAnimation(this, HomeActivity.class, true);
                finish();
            } else
                super.onBackPressed();
        }
    }



    private void selectImage() {

      /*  ImagePicker.Companion.with(ProfileActivity.this)//Crop image(Optional), Check Customization for more option
                .compress(1024)
                .galleryOnly()
                .maxResultSize(1080, 1080)//Crop square image, its same as crop(1f, 1f)//Final image size will be less than 1 MB(Optional)
                .start(0);*/

        final CharSequence[] options = {"Choose from Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(ProfileActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
               /* if (options[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File f = new File(Environment.getExternalStorageDirectory(), "temp.jpg");
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                    startActivityForResult(intent, 1);


                }*/

                if (options[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(intent, 2);

                } else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 1) {
            try {
                Log.e("Activity", "Pick from Camera::>>> ");
                 selectedImage = data.getData();
                if(status.equals("1")){    /* for camera to set in user image*/
                    bitmap = (Bitmap) data.getExtras().get("data");
                    Bitmap bitmaps=getResizedBitmap(bitmap, 500);
                    strUserImage = Util.encodeImageToBase64(bitmaps);
                    mProfileBinding.imgUserImage.setImageBitmap(bitmap);

                    mProfileBinding.imgUserImage.setVisibility(View.VISIBLE);
                    mProfileBinding.imgUserGet.setVisibility(View.GONE);
                    userData.setUser_image("data:image/jpeg;base64," + strUserImage);
                    mProfileBinding.imgUserImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            showAlert(selectedImage);
                        }
                    });

                }
                else {  /* for camera to set in pan image*/

                    bitmap = (Bitmap) data.getExtras().get("data");
                    Bitmap bitmaps=getResizedBitmap(bitmap, 500);
                    panCardBitmap = Util.encodeImageToBase64(bitmaps);
                    mProfileBinding.imgPan.setImageBitmap(bitmap);
                    mProfileBinding.imgPan.setVisibility(View.VISIBLE);
                    mProfileBinding.imgPanGet.setVisibility(View.GONE);
                    userData.setPan_copy("data:image/jpeg;base64," + panCardBitmap);
                    mProfileBinding.imgPan.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            showAlert(selectedImage);
                        }
                    });
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == 2) {
              selectedImage = null;
            try {
                selectedImage = data.getData();
            } catch (Exception e) {
                e.printStackTrace();
            }
            Log.e("Activity", "Pick from Gallery::>>> ");
            try {
               if(status.equals("1")){

                   bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                   Bitmap bitmaps=getResizedBitmap(bitmap, 500);
                   strUserImage = Util.encodeImageToBase64(bitmaps);
                   imgPath = getRealPathFromURI(selectedImage);
                   destination = new File(imgPath.toString());
                   mProfileBinding.imgUserImage.setImageBitmap(bitmap);
                   mProfileBinding.imgUserImage.setVisibility(View.VISIBLE);
                   mProfileBinding.imgUserGet.setVisibility(View.GONE);
                   userData.setUser_image("data:image/jpeg;base64," + strUserImage);
                   mProfileBinding.imgUserImage.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View view) {

                           showAlert(selectedImage);
                       }
                   });
               }
               else {
                   bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImage);
                   Bitmap bitmaps=getResizedBitmap(bitmap, 500);
                   panCardBitmap = Util.encodeImageToBase64(bitmaps);
                   imgPath = getRealPathFromURI(selectedImage);
                   destination = new File(imgPath.toString());
                   mProfileBinding.imgPan.setImageBitmap(bitmap);
                   mProfileBinding.imgPan.setVisibility(View.VISIBLE);
                   mProfileBinding.imgPanGet.setVisibility(View.GONE);
                   userData.setPan_copy("data:image/jpeg;base64," + panCardBitmap);
                   mProfileBinding.imgPan.setOnClickListener(new View.OnClickListener() {
                       @Override
                       public void onClick(View view) {

                           showAlert(selectedImage);
                       }
                   });
               }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

       /* if (status.equals("1") ) {
            File f = new File(Environment.getExternalStorageDirectory().toString());
            strImage = String.valueOf(f);
            for (File temp : f.listFiles()) {
                if (temp.getName().equals("temp.jpg")) {
                    f = temp;
                    break;
                }
            }



            try {
                Bitmap bitmap;
                BitmapFactory.Options bitmapOptions = new BitmapFactory.Options();

                bitmap = BitmapFactory.decodeFile(f.getAbsolutePath(),
                        bitmapOptions);
                // obj.setPan_copy("data:image/jpeg;base64," + panCardBitmap);
                String filePath = ImagePicker.Companion.getFilePath(data);
                Bitmap selectedImage = SiliCompressor.with(ProfileActivity.this).getCompressBitmap(filePath);


                if(status.equals("1")){
                    Uri compressUri = getPickImageResultUri(this, data);
                    strUserImage = Util.encodeImageToBase64(selectedImage);
                    userData.setUser_image("data:image/jpeg;base64," + strUserImage);
                   // mProfileBinding.imgUserImage.setImageBitmap(strUserImage);
                    mProfileBinding.imgUserImage.setImageURI(compressUri);
                    mProfileBinding.imgUserGet.setVisibility(View.GONE);
                    mProfileBinding.imgUserImage.setVisibility(View.VISIBLE);
                    mProfileBinding.imgUserImage.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            showAlert(compressUri);
                        }
                    });
                }
                else {
                    Uri compressUri = getPickImageResultUri(this, data);
                    panCardBitmap = Util.encodeImageToBase64(selectedImage);
                    userData.setPan_copy("data:image/jpeg;base64," + panCardBitmap);
                   // mProfileBinding.imgPan.setImageBitmap(bitmap);
                    mProfileBinding.imgPan.setImageURI(compressUri);
                    mProfileBinding.imgPanGet.setVisibility(View.GONE);
                    mProfileBinding.imgPan.setVisibility(View.VISIBLE);
                    mProfileBinding.imgPan.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            showAlert(compressUri);
                        }
                    });
                }


                String path = Environment
                        .getExternalStorageDirectory()
                        + File.separator
                        + "Phoenix" + File.separator + "default";
                f.delete();
                OutputStream outFile = null;
                fileUser = new File(path, System.currentTimeMillis() + ".jpg");
                filePan = new File(path, System.currentTimeMillis() + ".jpg");
                try {

                    if(status.equals("1")){

                        outFile = new FileOutputStream(fileUser);
                    }
                    else {

                        outFile = new FileOutputStream(filePan);
                    }

                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outFile);
                    outFile.flush();
                    outFile.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (status.equals("2")) {
            if (resultCode == Activity.RESULT_OK) {

                Uri uri = data.getData();
                strImage = String.valueOf(uri);*//*no use just cheking if image has selected or not*//*



                String filePath = ImagePicker.Companion.getFilePath(data);
                try {
                    Bitmap selectedImage = SiliCompressor.with(ProfileActivity.this).getCompressBitmap(filePath);
                    if(status.equals("1")){
                        Log.e("sdfsdfsfsdfdsf","If User Image");
                        Uri compressUri = getPickImageResultUri(this, data);
                        strUserImage = Util.encodeImageToBase64(selectedImage);
                        userData.setUser_image("data:image/jpeg;base64," + strUserImage);
                        fileUser = FileUtil.from(ProfileActivity.this, uri);
                       // mProfileBinding.imgUserImage.setImageBitmap(myBitmap);
                        mProfileBinding.imgUserImage.setImageURI(compressUri);
                        mProfileBinding.imgUserGet.setVisibility(View.GONE);
                        mProfileBinding.imgUserImage.setVisibility(View.VISIBLE);
                        mProfileBinding.imgUserImage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                showAlert(compressUri);
                            }
                        });
                    }
                    else {
                        Log.e("sdfsdfsfsdfdsf","else User Image");
                        Uri compressUri = getPickImageResultUri(this, data);
                        panCardBitmap = Util.encodeImageToBase64(selectedImage);
                        userData.setPan_copy("data:image/jpeg;base64," + panCardBitmap);
                        filePan = FileUtil.from(ProfileActivity.this, uri);
                        Bitmap myBitmap = BitmapFactory.decodeFile(filePan.getAbsolutePath());
                       // mProfileBinding.imgPan.setImageBitmap(myBitmap);
                        mProfileBinding.imgPan.setImageURI(compressUri);
                        mProfileBinding.imgPanGet.setVisibility(View.GONE);
                        mProfileBinding.imgPan.setVisibility(View.VISIBLE);
                        mProfileBinding.imgPan.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                showAlert(compressUri);
                            }
                        });
                    }

                } catch (IOException e) {

                    e.printStackTrace();
                }
            }
        }*/
    }



    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Audio.Media.DATA};
        Cursor cursor = managedQuery(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void showAlert(Uri parse) {


        View popupView = null;
        Log.e("sfsafasasfa",parse.toString());

        popupView = LayoutInflater.from(ProfileActivity.this).inflate(R.layout.enlarge_image, null);

        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0);

        ImageView closeImg = popupView.findViewById(R.id.closeImg);
        closeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                popupWindow.dismiss();
            }
        });

        PhotoView imageView = popupView.findViewById(R.id.imageView);
        imageView.setImageURI(parse);
        // Glide.with(c).load(status).into(imageView);
    }

    public static Uri getPickImageResultUri(@NonNull Context context, @Nullable Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);

        }
        return isCamera || data.getData() == null ? getCaptureImageOutputUri(context) : data.getData();
    }

    public static Uri getCaptureImageOutputUri(@NonNull Context context) {
        Uri outputFileUri = null;
        File getImage = context.getExternalCacheDir();
        if (getImage != null) {

            Log.e("demobhsurl", getImage.getPath());
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));

        }
        return outputFileUri;
    }

    @Override
    public Context getContext() {
        return this;
    }

    /**
     *  validate the form fields
     * @return
     */

    private boolean isValidateFields() {
        if (!StringUtility.validateEditText(mProfileBinding.edFlat)) {
            mProfileBinding.edFlat.setError(getResources().getString(R.string.str_empty_flat_no));
            return false;
        } else if (!StringUtility.validateEditText(mProfileBinding.edCity)) {
            mProfileBinding.edCity.setError(getResources().getString(R.string.str_city_empty));
            return false;
        } else if (!StringUtility.validateEditText(mProfileBinding.edState)) {
            mProfileBinding.edState.setError(getResources().getString(R.string.str_state_empty));
            return false;
        } else if (!StringUtility.validateEditText(mProfileBinding.edPincode)) {
            mProfileBinding.edPincode.setError(getResources().getString(R.string.str_empty_pincode));
            return false;
        } else if (!StringUtility.validateEditText(mProfileBinding.edAdharNo)) {
            mProfileBinding.edAdharNo.setError(getResources().getString(R.string.str_empty_adhar_no));
            return false;
        } else if (!StringUtility.validateEditText(mProfileBinding.edPanCardNo)) {
            mProfileBinding.edPanCardNo.setError(getResources().getString(R.string.str_empty_pan_card_no));
            return false;
        } else if (!StringUtility.validateEditText(mProfileBinding.edMobileNumber)) {
            mProfileBinding.edMobileNumber.setError(getResources().getString(R.string.str_mobile_no_empty));
            return false;
        } else if (!StringUtility.validateEditText(mProfileBinding.edAgentDob)) {
            mProfileBinding.edAgentDob.setError(getResources().getString(R.string.str_empty_agent_dob));
            return false;
        } else if (!StringUtility.validateEditText(mProfileBinding.edMaritalStatus)) {
            mProfileBinding.edMaritalStatus.setError(getResources().getString(R.string.str_empty_marital_status));
            return false;
        } else if (!StringUtility.validateEditText(mProfileBinding.edSpouseName)) {
            mProfileBinding.edSpouseName.setError(getResources().getString(R.string.str_empty_spouse_name));
            return false;
        } else if (!StringUtility.validateEditText(mProfileBinding.edSpouseDob)) {
            mProfileBinding.edSpouseDob.setError(getResources().getString(R.string.str_spouse_dob_empty));
            return false;
        } else if (!StringUtility.validateEditText(mProfileBinding.edMarrigaeDate)) {
            mProfileBinding.edMarrigaeDate.setError(getResources().getString(R.string.str_marriage_date_empty));
            return false;
        } else if (!StringUtility.validateEditText(mProfileBinding.edFatherName)) {
            mProfileBinding.edFatherName.setError(getResources().getString(R.string.str_father_name_empty));
            return false;
        } else if (!StringUtility.validateEditText(mProfileBinding.edFatherDob)) {
            mProfileBinding.edFatherDob.setError(getResources().getString(R.string.str_father_dob_empty));
            return false;
        } else if (!StringUtility.validateEditText(mProfileBinding.edMotherName)) {
            mProfileBinding.edMotherName.setError(getResources().getString(R.string.str_mother_name_empty));
            return false;
        } else if (!StringUtility.validateEditText(mProfileBinding.edMotherDob)) {
            mProfileBinding.edMotherDob.setError(getResources().getString(R.string.str_mother_dob_empty));
            return false;
        } else if (!StringUtility.validateEditText(mProfileBinding.edBrotherName)) {
            mProfileBinding.edBrotherName.setError(getResources().getString(R.string.str_brother_name_empty));
            return false;
        } else if (!StringUtility.validateEditText(mProfileBinding.edBrotherDate)) {
            mProfileBinding.edBrotherDate.setError(getResources().getString(R.string.str_brother_date_empty));
            return false;
        } else if (!StringUtility.validateEditText(mProfileBinding.edSisterName)) {
            mProfileBinding.edSisterName.setError(getResources().getString(R.string.str_sister_name_empty));

            return false;
        } else if (!StringUtility.validateEditText(mProfileBinding.edSisterDob)) {
            mProfileBinding.edSisterDob.setError(getResources().getString(R.string.str_sister_dob_empty));
            return false;
        } else
            return true;
    }

    /**
     * initialize and bind Views
     */
    private void initView() {
        mProfileBinding.toolbarProfile.txtTittle.setText(getResources().getString(R.string.str_profile_tittle));
        mProfileBinding.toolbarProfile.rlNotification.setOnClickListener(v -> {
            startActivityAnimation(this, NotificationActivity.class, false);
        });

        if (getIntent() != null) {
            if (getIntent().hasExtra("agentId")) {
                agentId = getIntent().getStringExtra("agentId");
            } else if (getIntent().hasExtra("profileShow"))
                fromFirstTime = getIntent().getBooleanExtra("profileShow", false);
        }


        for (int i = 1; i < 6; i++) {
            arrNoOfChild.add(i);
        }
        arrGender.add(getResources().getString(R.string.str_male));
        arrGender.add(getResources().getString(R.string.str_female));
        mProfileBinding.spinnerChildNo.setAdapter(new ArrayAdapter<Integer>(this, R.layout.simple_list_item_1, arrNoOfChild));
        mProfileBinding.spinnerChildNo.setOnItemSelectedListener(this);
        mPresenter = new UserProfilePresenter();
        mPresenter.setView(this);
        mPresenter.getUserProfile(agentId.isEmpty() ? PreferenceManager.getAgentId(this) : agentId);
        mProfileBinding.toolbarProfile.txtProfileEdit.setOnClickListener(this::onClick);
    }

    /**
     * handles the form buttons click
     * @param view form view
     */

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit_profile:
                setProfileData();
                break;
            case R.id.img_back:
                onBackPressed();
                break;
            case R.id.txt_profile_edit:
                mProfileBinding.setIsEditable(isEditable);
                for (int i = 0; i < arrChildBinding.size(); i++) {
                    arrChildBinding.get(i).setIsEditable(isEditable);
                }
                mProfileBinding.edFlat.requestFocus();
                mProfileBinding.toolbarProfile.txtProfileEdit.setText(getResources().getString(R.string.str_editing));
                break;
            case R.id.ed_agent_dob:
                DatePickerDialog datePickerDialog = new DatePickerDialog(ProfileActivity.this, agentDobListner, agentCal
                        .get(Calendar.YEAR), agentCal.get(Calendar.MONTH),
                        agentCal.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
                datePickerDialog.show();
                break;
            case R.id.ed_mother_dob:
                DatePickerDialog pickerDialog = new DatePickerDialog(ProfileActivity.this, agentMotherDob, motherCal
                        .get(Calendar.YEAR), motherCal.get(Calendar.MONTH),
                        motherCal.get(Calendar.DAY_OF_MONTH));
                pickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
                pickerDialog.show();
                break;
            case R.id.ed_brother_date:
                DatePickerDialog datePickerBrother = new DatePickerDialog(ProfileActivity.this, brotherDob, brotherCal
                        .get(Calendar.YEAR), brotherCal.get(Calendar.MONTH),
                        brotherCal.get(Calendar.DAY_OF_MONTH));
                datePickerBrother.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
                datePickerBrother.show();
                break;
            case R.id.ed_sister_dob:
                DatePickerDialog datePickerSister = new DatePickerDialog(ProfileActivity.this, agetntSisterDob, sisCal
                        .get(Calendar.YEAR), sisCal.get(Calendar.MONTH),
                        sisCal.get(Calendar.DAY_OF_MONTH));
                datePickerSister.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
                datePickerSister.show();
                break;
            case R.id.ed_father_dob:
                DatePickerDialog datePickerFather = new DatePickerDialog(ProfileActivity.this, fatherDob, fatherCal
                        .get(Calendar.YEAR), fatherCal.get(Calendar.MONTH),
                        fatherCal.get(Calendar.DAY_OF_MONTH));
                datePickerFather.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
                datePickerFather.show();
                break;
            case R.id.ed_marrigae_date:
                DatePickerDialog datePickerAnniversary = new DatePickerDialog(ProfileActivity.this, marriageAnniListener, marriageAnniCal
                        .get(Calendar.YEAR), marriageAnniCal.get(Calendar.MONTH),
                        marriageAnniCal.get(Calendar.DAY_OF_MONTH));
                datePickerAnniversary.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
                datePickerAnniversary.show();
                break;
            case R.id.ed_spouse_dob:
                DatePickerDialog datePickerSpouse = new DatePickerDialog(ProfileActivity.this, spouseDateListner, spouseCal
                        .get(Calendar.YEAR), spouseCal.get(Calendar.MONTH),
                        spouseCal.get(Calendar.DAY_OF_MONTH));
                datePickerSpouse.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
                datePickerSpouse.show();
                break;
            default:
                break;

        }
    }

    /**
     * set profile data on views
     */

    private void setProfileData() {

        if (userData != null && userData.getUsercrm() != null) {

            userData.getUsercrm().getResults().get(0).setDob(SystemUtility.getTimestampForDate(mProfileBinding.edAgentDob.getText().toString()));
            CRMModel crmModel = userData.getUsercrm().getResults().get(0);
            //Brother date of birth set
            if (crmModel.getBROSet() != null && crmModel.getBROSet().size() > 0) {
                crmModel.getBROSet().get(0).setDob(SystemUtility.getTimestampForDate(mProfileBinding.edBrotherDate.getText().toString()));
            }
            else {
                crmModel.getBROSet().clear();
                crmModel.getBROSet().add(new ProfileBrotherDetail());
                crmModel.setBrotherName(mProfileBinding.edBrotherName.getText().toString());
                crmModel.getBROSet().get(0).setDob(SystemUtility.getTimestampForDate(mProfileBinding.edBrotherDate.getText().toString()));
            }

            ////Mother DOB set
            if (crmModel.getMOTSet() != null && crmModel.getMOTSet().size() > 0) {
                userData.getUsercrm().getResults().get(0).getMOTSet().get(0).setDob(SystemUtility.getTimestampForDate(mProfileBinding.edMotherDob.getText().toString()));

            } else {
                crmModel.getMOTSet().clear();
                crmModel.getMOTSet().add(new ProfileMotherModel());
                crmModel.setMotherName(mProfileBinding.edMotherName.getText().toString());
                crmModel.getMOTSet().get(0).setDob(SystemUtility.getTimestampForDate(mProfileBinding.edMotherDob.getText().toString()));
            }

            ///Sister DOb date
            if (crmModel.getSISSet() != null && crmModel.getSISSet().size() > 0)
                crmModel.getSISSet().get(0).setDob(SystemUtility.getTimestampForDate(mProfileBinding.edSisterDob.getText().toString()));
            else {
                crmModel.getSISSet().clear();
                crmModel.getSISSet().add(new ProfileSisterDetail());
                crmModel.setSisterName(mProfileBinding.edSisterName.getText().toString());
                crmModel.getSISSet().get(0).setDob(SystemUtility.getTimestampForDate(mProfileBinding.edSisterDob.getText().toString()));
            }

            crmModel.setMarrAnni(SystemUtility.getTimestampForDate(mProfileBinding.edMarrigaeDate.getText().toString()));

            ///father dob set
            if (crmModel.getFatSet() != null && crmModel.getFatSet().size() > 0)
                userData.getUsercrm().getResults().get(0).getFatSet().get(0).setDob(SystemUtility.getTimestampForDate(mProfileBinding.edFatherDob.getText().toString()));
            else {
                crmModel.getFatSet().clear();
                crmModel.getFatSet().add(new ProfileFatherModel());
                crmModel.setFatherName(mProfileBinding.edFatherName.getText().toString());
                crmModel.getFatSet().get(0).setDob(SystemUtility.getTimestampForDate(mProfileBinding.edFatherDob.getText().toString()));

            }

            crmModel.setNoChild(String.valueOf(mProfileBinding.spinnerChildNo.getSelectedItem()));
            for (int i = 0; i < userData.getUsercrm().getResults().get(0).getKIDSet().size(); i++) {
                crmModel.getKIDSet().get(i).setDob(SystemUtility.getTimestampForDate(arrChildBinding.get(i).edChildDob.getText().toString()));
            }

            AlertUtility.showAlert(this, getResources().getString(R.string.syr_are_you_sure_profile_update), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Gson gson = new Gson();
                    String data = gson.toJson(userData);
                    //Log.e("qetwtwertwew", data);
                    try {
                        File file = new File(Environment.getExternalStorageDirectory() + "/test.txt");

                        if (!file.exists()) {
                            file.createNewFile();
                        }
                        FileWriter writer = new FileWriter(file);
                        writer.append(data);
                        writer.flush();
                        writer.close();
                    } catch (IOException e) {
                    }
                    mPresenter.updateProfile(userData);
                }
            });
        }
    }

    @Override
    public void onProfileSuccess(UserProfileModel mUserProfileData, Boolean isedit) {
        userData = mUserProfileData;

        if(mUserProfileData.getUserdetail().get(0).getFields().getPan_image()!=null){
            Picasso.with(ProfileActivity.this).load(Constant.Image_Url+mUserProfileData.getUserdetail().get(0).getFields().getPan_image()).into(mProfileBinding.imgPanGet);
            mProfileBinding.imgPanGet.setVisibility(View.VISIBLE);
            mProfileBinding.imgPan.setVisibility(View.GONE);
            mProfileBinding.imgPanGet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    getImageAlert(Constant.Image_Url+mUserProfileData.getUserdetail().get(0).getFields().getPan_image());
                }
            });
        }
        if(mUserProfileData.getUserdetail().get(0).getFields().getUser_image()!=null){
            Picasso.with(ProfileActivity.this).load(Constant.Image_Url+mUserProfileData.getUserdetail().get(0).getFields().getUser_image()).into(mProfileBinding.imgUserGet);
            mProfileBinding.imgUserGet.setVisibility(View.VISIBLE);
            mProfileBinding.imgUserImage.setVisibility(View.GONE);
            mProfileBinding.imgUserGet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    getImageAlert(Constant.Image_Url+mUserProfileData.getUserdetail().get(0).getFields().getUser_image());
                }
            });
        }

        if (mUserProfileData.getUsermaster() != null)
            mProfileBinding.setAgencyName(mUserProfileData.getUsermaster().get(0).getFields().getFirstName());
        else
            mProfileBinding.setAgencyName("");

        if (PreferenceManager.getPrefUserType(ProfileActivity.this).equalsIgnoreCase("AG")) {
            mProfileBinding.toolbarProfile.txtProfileEdit.setVisibility(isedit ? View.VISIBLE : View.GONE);
            isEditable = isedit;
        }

        if (mUserProfileData.getUsercrm().getResults().size()!=0) {

            try {

                mProfileBinding.setItem(mUserProfileData.getUsercrm().getResults().get(0));
                int noOfChild = Integer.parseInt(userData.getUsercrm().getResults().get(0).getNoChild());
                mProfileBinding.spinnerChildNo.setSelection(getIndex(mProfileBinding.spinnerChildNo, noOfChild != 0 ? String.valueOf(noOfChild) : "1"));
                addChildViews(mUserProfileData.getUsercrm().getResults().get(0).getKIDSet());
            }
            catch (NumberFormatException e) {
                e.printStackTrace();
            }
        } else {
            Log.e("sdfsdfdfddsf","else");
            UserCrmModel userCrmModel = new UserCrmModel();
            userData.setUsercrm(userCrmModel);
            mProfileBinding.setItem(userCrmModel.getResults().get(0));
            noChildViews();
        }

    }

    public void getImageAlert(String image) {


        View popupView = null;
        Log.e("wgrtsdgdgsg",image);

        popupView = LayoutInflater.from(ProfileActivity.this).inflate(R.layout.enlarge_image, null);

        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0);

        ImageView closeImg = popupView.findViewById(R.id.closeImg);
        closeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                popupWindow.dismiss();
            }
        });

        PhotoView imageView = popupView.findViewById(R.id.imageView);
        //imageView.setImageURI(parse);
        Picasso.with(ProfileActivity.this).load(image).into(imageView);
    }



    /**
     *  add agents child views on form
     * @param kidSet list of agents child data
     */
    private void addChildViews(List<ProfileChildModel> kidSet) {
        if (kidSet.size() > 0) {
            arrChildBinding.clear();
            arrChildData.clear();
            mProfileBinding.linearChild.removeAllViews();
            for (int i = 0; i < kidSet.size(); i++) {
                mBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.item_child_info, null, false);
                mProfileBinding.linearChild.addView(mBinding.getRoot());
                mProfileBinding.linearChild.setTag(i);
                mBinding.edChildDob.setTag(i);
                arrChildBinding.add(mBinding);
                arrChildData.add(kidSet.get(i));
                mBinding.setItem(kidSet.get(i));
                setChildSelectionData(kidSet.get(i));
                mBinding.spinnerChildEducation.setTag(i);
                mBinding.spinnerChildGender.setTag(i);
                mBinding.edChildDob.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DatePickerDialog datePickerDialog = new DatePickerDialog(ProfileActivity.this, onChildDateListener, childCal
                                .get(Calendar.YEAR), childCal.get(Calendar.MONTH),
                                childCal.get(Calendar.DAY_OF_MONTH));
                        datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
                        datePickerDialog.getDatePicker().setTag(v.getTag());
                        datePickerDialog.show();
                    }
                });
            }
        } else {
            noChildViews();
        }


    }

    /**
     *
     * set child gender's and educations data
     * @param profileChildModel agent child's data
     */

    private void setChildSelectionData(ProfileChildModel profileChildModel) {
        mBinding.spinnerChildGender.setAdapter(new ArrayAdapter<String>(this, R.layout.simple_list_item_1, arrGender));
        mBinding.spinnerChildEducation.setAdapter(new ArrayAdapter<String>(this, R.layout.simple_list_item_1, userData.getEducationList()));
        mBinding.spinnerChildGender.setSelection(getIndex(mBinding.spinnerChildGender, profileChildModel.getGender() == null || profileChildModel.getGender().isEmpty() ? getResources().getString(R.string.str_male) : String.valueOf(profileChildModel.getGender())));
        mBinding.spinnerChildEducation.setSelection(getIndex(mBinding.spinnerChildEducation, profileChildModel.getEducation() == null || profileChildModel.getEducation().isEmpty() ? "Primary" : profileChildModel.getEducation()));
        mBinding.spinnerChildGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                try {
                    int pos = (int) parent.getTag();
                    if(userData.getUsercrm().getResults().get(0).getKIDSet().size()!=0&&userData.getUsercrm().getResults().get(0).getKIDSet()!=null){
                        userData.getUsercrm().getResults().get(0).getKIDSet().get(pos).setGender(String.valueOf(arrChildBinding.get(pos).spinnerChildGender.getSelectedItem()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // will override if require
            }
        });
        mBinding.spinnerChildEducation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                int pos = (int) parent.getTag();

                if(userData.getUsercrm().getResults().get(0).getKIDSet().size()!=0 && userData.getUsercrm().getResults().get(0).getKIDSet()!=null){
                    userData.getUsercrm().getResults().get(0).getKIDSet().get(pos).setEducation(String.valueOf(arrChildBinding.get(pos).spinnerChildEducation.getSelectedItem()));
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                  // will override if require
            }
        });


    }
    /**
     * added child data
     * @param value child count
     */
    private void addChildFronNoOfChildern(int value) {
        for (int i = 0; i < value; i++) {
            mBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.item_child_info, null, false);
            mProfileBinding.linearChild.addView(mBinding.getRoot());
            arrChildBinding.add(mBinding);
            mBinding.setIsEditable(true);
            ProfileChildModel profileChildModel = new ProfileChildModel();
            mBinding.setItem(profileChildModel);
            arrChildData.add(profileChildModel);
            setChildSelectionData(profileChildModel);
            mBinding.edChildDob.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DatePickerDialog datePickerDialog = new DatePickerDialog(ProfileActivity.this, onChildDateListener, childCal
                            .get(Calendar.YEAR), childCal.get(Calendar.MONTH),
                            childCal.get(Calendar.DAY_OF_MONTH));
                    datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
                    datePickerDialog.getDatePicker().setTag(v.getTag());

                    datePickerDialog.show();

                }
            });
            int tag = arrChildBinding.indexOf(mBinding);
            mBinding.spinnerChildEducation.setTag(tag);
            mBinding.spinnerChildGender.setTag(tag);
            mProfileBinding.linearChild.setTag(tag);
            mBinding.edChildDob.setTag(tag);
        }
        userData.getUsercrm().getResults().get(0).setKIDSet(arrChildData);

    }

    /**
     * agents no child data view
     */

    private void noChildViews() {
        ProfileChildModel profileChildModel = new ProfileChildModel();
        arrChildBinding.clear();
        arrChildData.clear();
        mProfileBinding.linearChild.removeAllViews();
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(this), R.layout.item_child_info, null, false);
        mProfileBinding.linearChild.addView(mBinding.getRoot());
        mBinding.edChildDob.setTag(0);
        arrChildBinding.add(mBinding);
        mBinding.setItem(profileChildModel);
        arrChildData.add(profileChildModel);
        setChildSelectionData(profileChildModel);
        mBinding.spinnerChildEducation.setTag(0);
        mBinding.spinnerChildGender.setTag(0);
        mBinding.edChildDob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DatePickerDialog datePickerDialog = new DatePickerDialog(ProfileActivity.this, onChildDateListener, childCal
                        .get(Calendar.YEAR), childCal.get(Calendar.MONTH),
                        childCal.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(Calendar.getInstance().getTimeInMillis());
                datePickerDialog.getDatePicker().setTag(v.getTag());
                datePickerDialog.show();

            }
        });
    }

    /**
     * get the selected item position
     * @param spinner  child's education and gender spinner
     * @param myString childs data
     * @return
     */


    private int getIndex(Spinner spinner, String myString) {
        for (int i = 0; i < spinner.getCount(); i++) {
            if (spinner.getItemAtPosition(i).toString().equalsIgnoreCase(myString)) {
                return i;
            }
        }
        return 0;
    }

    @Override
    public void onItemSelected(AdapterView  <?> parent, View view, int position, long id) {
        if (isEditable) {
            if (arrChildBinding.size() > (int) mProfileBinding.spinnerChildNo.getSelectedItem()) {
                //removing child view
                removeChildsOnSelction(arrChildBinding.size() - ((int) mProfileBinding.spinnerChildNo.getSelectedItem()));
            } else {
                //adding child views
                addChildFronNoOfChildern(((int) mProfileBinding.spinnerChildNo.getSelectedItem()) - arrChildBinding.size());
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        // will override if require
    }

    private void removeChildsOnSelction(int childCount) {
        for (int i = 0; i < childCount; i++) {
            arrChildData.remove(arrChildData.size() - 1);
            arrChildBinding.remove(arrChildBinding.size() - 1);
            mProfileBinding.linearChild.removeViewAt(mProfileBinding.linearChild.getChildCount() - 1);
        }
        userData.getUsercrm().getResults().get(0).setKIDSet(arrChildData);
    }

    @Override
    public void onInfo(String message) {
        super.onInfo(message);
        isEditable = false;
        mProfileBinding.setIsEditable(false);
        for (int i = 0; i < arrChildBinding.size(); i++)
            arrChildBinding.get(i).setIsEditable(false);
        mProfileBinding.toolbarProfile.txtProfileEdit.setVisibility(View.GONE);
        mProfileBinding.toolbarProfile.txtProfileEdit.setText(getResources().getString(R.string.str_edit));
        mPresenter.getUserProfile(agentId.isEmpty() ? PreferenceManager.getAgentId(this) : agentId);
    }




}
