package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;


public class CityResultModel extends BaseModel {
    @SerializedName("results")
    @Expose
    private List<CityListModel> results = null;

    public List<CityListModel> getResults() {
        return results;
    }

    public void setResults(List<CityListModel> results) {
        this.results = results;
    }
}
