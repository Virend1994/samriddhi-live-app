package com.app.samriddhi.ui.activity.main.ui.Grievance;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.samriddhi.R;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.google.android.material.tabs.TabLayout;

public class GrievanceTabActivity extends AppCompatActivity {

    TabLayout tablayout;
    ViewPager viewPager;

    @SuppressLint("ResourceType")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grievance_tab);

        ImageView imgBack=findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        tablayout=findViewById(R.id.tablayout);
        viewPager=findViewById(R.id.viewPager);

        tablayout.addTab(tablayout.newTab().setText(R.string.complaint));
        tablayout.addTab(tablayout.newTab().setText(R.string.query));
        tablayout.addTab(tablayout.newTab().setText(R.string.suggestion));
        tablayout.addTab(tablayout.newTab().setText(R.string.request));

        TextView tabOne = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabOne.setText(R.string.complaint);
        tablayout.getTabAt(0).setCustomView(tabOne);

        TextView tabTwo = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabTwo.setText(R.string.query);
        tablayout.getTabAt(1).setCustomView(tabTwo);

        TextView tabThree= (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabThree.setText(R.string.suggestion);
        tablayout.getTabAt(2).setCustomView(tabThree);

        TextView tabFour= (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
        tabFour.setText(R.string.request);
        tablayout.getTabAt(3).setCustomView(tabFour);


        final GrievanceTabAdapter adapterTablayout = new GrievanceTabAdapter(this.getSupportFragmentManager(), tablayout.getTabCount());
        viewPager.setAdapter(adapterTablayout);

          TextView txtCategories=findViewById(R.id.txtCategories);

        if(PreferenceManager.getAppLang(GrievanceTabActivity.this).equals("hi")) {

            tabOne.setText("शिकायत");
            tabTwo.setText("सवाल");
            tabThree.setText("सुझाव");
            tabFour.setText("निवेदन");
            txtCategories.setText("सहायता और समर्थन");
        }
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tablayout));
        tablayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }
}