package com.app.samriddhi.ui.model;

public class CirReqChequeSetModel {
    public String cheque_mode;
    public String cheque_number;
    public Object cheque_date;
    public String cheque_amt;
    public String cheque_bank;

    public String getCheque_mode() {
        return cheque_mode;
    }

    public void setCheque_mode(String cheque_mode) {
        this.cheque_mode = cheque_mode;
    }

    public String getCheque_number() {
        return cheque_number;
    }

    public void setCheque_number(String cheque_number) {
        this.cheque_number = cheque_number;
    }

    public Object getCheque_date() {
        return cheque_date;
    }

    public void setCheque_date(Object cheque_date) {
        this.cheque_date = cheque_date;
    }

    public String getCheque_amt() {
        return cheque_amt;
    }

    public void setCheque_amt(String cheque_amt) {
        this.cheque_amt = cheque_amt;
    }

    public String getCheque_bank() {
        return cheque_bank;
    }

    public void setCheque_bank(String cheque_bank) {
        this.cheque_bank = cheque_bank;
    }
}
