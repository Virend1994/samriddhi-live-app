package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AgentResultModel extends BaseModel {
    @SerializedName("results")
    @Expose
    private List<AgentListModel> results = null;

    public List<AgentListModel> getResults() {
        return results;
    }

    public void setResults(List<AgentListModel> results) {
        this.results = results;
    }
}
