package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GrievanceOpenItemListModel extends BaseModel {

    @SerializedName("BP_Code")
    @Expose
    private String bPCode;
    @SerializedName("Status_CODE")
    @Expose
    private String statusCODE;
    @SerializedName("Status_Text")
    @Expose
    private String statusText;
    @SerializedName("Incident_category_Code")
    @Expose
    private Integer incidentCategoryCode;
    @SerializedName("Incident_category_Text")
    @Expose
    private String incidentCategoryText;
    @SerializedName("Total Incident")
    @Expose
    private Integer totalIncident;

    public String getBPCode() {
        return bPCode;
    }

    public void setBPCode(String bPCode) {
        this.bPCode = bPCode;
    }

    public String getStatusCODE() {
        return statusCODE;
    }

    public void setStatusCODE(String statusCODE) {
        this.statusCODE = statusCODE;
    }

    public String getStatusText() {
        return statusText;
    }

    public void setStatusText(String statusText) {
        this.statusText = statusText;
    }

    public Integer getIncidentCategoryCode() {
        return incidentCategoryCode;
    }

    public void setIncidentCategoryCode(Integer incidentCategoryCode) {
        this.incidentCategoryCode = incidentCategoryCode;
    }

    public String getIncidentCategoryText() {
        return incidentCategoryText;
    }

    public void setIncidentCategoryText(String incidentCategoryText) {
        this.incidentCategoryText = incidentCategoryText;
    }

    public Integer getTotalIncident() {
        return totalIncident;
    }

    public void setTotalIncident(Integer totalIncident) {
        this.totalIncident = totalIncident;
    }
}
