package com.app.samriddhi.ui.model;

import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GrievanceCategoryFieldModels extends BaseModel {

    @SerializedName("IncidentCategory_Text")
    @Expose
    public String incidentCategoryText;
    @SerializedName("IncidentCategory_Srno")
    @Expose
    private Integer incidentCategorySrno;
    @SerializedName("IncidentCategory_Status")
    @Expose
    private Integer incidentCategoryStatus;

    public Integer getIncidentCategorySrno() {
        return incidentCategorySrno;
    }

    public void setIncidentCategorySrno(Integer incidentCategorySrno) {
        this.incidentCategorySrno = incidentCategorySrno;
    }

    @Bindable
    public String getIncidentCategoryText() {
        return incidentCategoryText;
    }

    public void setIncidentCategoryText(String incidentCategoryText) {
        this.incidentCategoryText = incidentCategoryText;
        notifyPropertyChanged(BR.incidentCategoryText);
    }

    public Integer getIncidentCategoryStatus() {
        return incidentCategoryStatus;
    }

    public void setIncidentCategoryStatus(Integer incidentCategoryStatus) {
        this.incidentCategoryStatus = incidentCategoryStatus;
    }

}
