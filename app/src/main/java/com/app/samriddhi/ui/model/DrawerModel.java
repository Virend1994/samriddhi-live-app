package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;

public class DrawerModel extends BaseModel {

    public String item_name;
    public int item_img;

    public DrawerModel(String itemName, int img) {
        this.item_name = itemName;
        this.item_img = img;
    }

}
