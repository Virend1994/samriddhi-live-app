package com.app.samriddhi.ui.activity.main.ui.OrderSupply;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.transition.Visibility;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.DropdownMenuAdapter;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.Adapter.PendingOrderAdapter;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.Adapter.PoAdapter;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.POModal.DailyPOModal;
import com.app.samriddhi.ui.model.DropDownModel;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.Util;
import com.google.android.material.button.MaterialButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import okhttp3.Credentials;

public class PendingOrdersActivity extends BaseActivity implements DropdownMenuAdapter.OnMeneuClickListnser {

    RecyclerView recyclerAgency;
    LinearLayoutManager linearLayoutManager;
    List<DailyPOModal> poModalList;
    PendingOrderAdapter poAdapter;
    LinearLayout btnlayout;
    private String dropDownSelectType = "";
    TextView tvSaleDistrict, tvCopies, tvstatus;
    public ArrayList<DropDownModel> unitMaster;
    public ArrayList<DropDownModel> editionMaster;
    public ArrayList<DropDownModel> statusMaster;
    int allstatus = 0;
    String strUnitid = "", strUnitCode = "", strEditionName = "", strEditionId = "", strEdtionCode = "", strStatusId = "";
    boolean flagunit = false, flagEdition = false;
    public static TextView txtSalesMain, txtSalesJJ, txtRefMain, txtRefJJ, txtJJDiff, txtFreeDiff;
    TextView txtSaleDate, txtRefDate;
    EditText search;
    List<DailyPOModal> sendOrders;

    public List<String> reasonName;
    public List<String> reasonId;
    MaterialButton btnApprove;
    MaterialButton btnReject;
    public static CheckBox selectAll;
    String strSelectAllStatus = "false";
    LinearLayout linearRefrn;
    LinearLayout linearSale;

    public static TextView txtRefTotal;
    public static TextView txtSaleTotal;
    public static TextView txtFreeMain;
    public static TextView txtFreeRefn;
    public static TextView txtTotalCopiesDiff;
    public static TextView txtMainDiff;
    int OrderStatus = 0;
    int diffstattus = 0;
    public static TextView txtInc, txtDec, txtINCDECTotal;
    List<DailyPOModal> sendPoList;

    int orderCheckedStatus = 0;

    Calendar myCalendar;
    String strDate = "";
    DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pending_orders);
        ImageView imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
        sendPoList = new ArrayList<>();
        txtInc = findViewById(R.id.txtInc);
        txtDec = findViewById(R.id.txtDec);
        txtINCDECTotal = findViewById(R.id.txtINCDECTotal);

        sendOrders = new ArrayList<>();
        selectAll = findViewById(R.id.selectAll);
        btnlayout = findViewById(R.id.btnlayout);

        txtFreeRefn = findViewById(R.id.txtFreeRefn);
        txtFreeMain = findViewById(R.id.txtFreeMain);
        txtMainDiff = findViewById(R.id.txtMainDiff);
        txtTotalCopiesDiff = findViewById(R.id.txtTotalCopiesDiff);
        txtJJDiff = findViewById(R.id.txtJJDiff);
        txtFreeDiff = findViewById(R.id.txtFreeDiff);

        txtSaleTotal = findViewById(R.id.txtSaleTotal);
        txtRefTotal = findViewById(R.id.txtRefTotal);
        btnApprove = findViewById(R.id.btnApprove);
        btnReject = findViewById(R.id.btnReject);
        tvstatus = findViewById(R.id.tvstatus);
        txtSaleDate = findViewById(R.id.txtSaleDate);
        txtRefDate = findViewById(R.id.txtRefDate);
        search = findViewById(R.id.search);
        txtSalesMain = findViewById(R.id.txtSalesMain);
        txtSalesJJ = findViewById(R.id.txtSalesJJ);
        linearSale = findViewById(R.id.linearSale);
        linearRefrn = findViewById(R.id.linearRefrn);


        txtRefMain = findViewById(R.id.txtRefMain);
        txtRefJJ = findViewById(R.id.txtRefJJ);

        recyclerAgency = findViewById(R.id.recyclerAgency);
        recyclerAgency.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerAgency.setLayoutManager(linearLayoutManager);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        strDate = dateFormat.format(cal.getTime());
        Log.e("sgsdgsdgsdg", strDate);

        tvSaleDistrict = findViewById(R.id.tvSaleDistrict);
        tvSaleDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dropDownSelectType = "unit";
                if (flagunit == true) {
                    Util.showDropDown(unitMaster, "Select unit", PendingOrdersActivity.this, PendingOrdersActivity.this::onOptionClick);
                } else {
                    Toast.makeText(PendingOrdersActivity.this, "Please wait", Toast.LENGTH_SHORT).show();
                }
            }
        });


        tvCopies = findViewById(R.id.tvCopies);
        tvCopies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dropDownSelectType = "editions";

                if (flagEdition == true) {
                    Util.showDropDown(editionMaster, "Select Editions", PendingOrdersActivity.this, PendingOrdersActivity.this::onOptionClick);
                } else {
                    Toast.makeText(PendingOrdersActivity.this, "Please wait", Toast.LENGTH_SHORT).show();
                }
            }
        });

        tvstatus = findViewById(R.id.tvstatus);
        tvstatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dropDownSelectType = "status";
                Util.showDropDown(statusMaster, "Select Status", PendingOrdersActivity.this, PendingOrdersActivity.this::onOptionClick);
            }
        });




        getUnit();
        getStatus();

        selectAll.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {

                    strSelectAllStatus = "true";
                    allstatus = 1;
                    SetAdapter();
                } else {

                    strSelectAllStatus = "false";
                    allstatus = 0;
                    SetAdapter();
                }
            }
        });


        btnApprove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                OrderApproveORReject(1);
            }
        });

        btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OrderApproveORReject(2);

            }
        });
        myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener dateSales = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateSale();
            }

        };


        txtSaleDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                datePickerDialog = new DatePickerDialog(PendingOrdersActivity.this, dateSales, myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));

                // disable dates before two days and after two days after today
                Calendar today = Calendar.getInstance();
                Calendar twoDaysAgo = (Calendar) today.clone();
                twoDaysAgo.add(Calendar.DATE, 1);
                Calendar twoDaysLater = (Calendar) today.clone();
                twoDaysLater.add(Calendar.DATE, 5);
                datePickerDialog.getDatePicker().setMinDate(twoDaysAgo.getTimeInMillis());
                datePickerDialog.getDatePicker().setMaxDate(twoDaysLater.getTimeInMillis());
                datePickerDialog.show();
            }
        });

    }


    private void updateSale() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        strDate = sdf.format(myCalendar.getTime());
        txtSaleDate.setText(strDate);
        getTotalQTD();

    }


    public void UpldatePoDiff() {

        int getSaleMain = Integer.parseInt(txtSalesMain.getText().toString());
        int getRefMain = Integer.parseInt(txtRefMain.getText().toString());
        int totalMainDiff = getSaleMain - getRefMain;
        txtMainDiff.setText(totalMainDiff + "");

        if (totalMainDiff < 0) {
            txtMainDiff.setTextColor(getResources().getColor(R.color.red));
        } else {
            txtMainDiff.setTextColor(getResources().getColor(R.color.green_button_color));

        }

        int getSaleJJ = Integer.parseInt(txtSalesJJ.getText().toString());
        int getRefJJ = Integer.parseInt(txtRefJJ.getText().toString());
        int totalJJDiff = getSaleJJ - getRefJJ;
        txtJJDiff.setText(totalJJDiff + "");

        if (totalJJDiff < 0) {
            txtJJDiff.setTextColor(getResources().getColor(R.color.red));
        } else {
            txtJJDiff.setTextColor(getResources().getColor(R.color.green_button_color));

        }

        int totalMainCopies = getSaleMain + getSaleJJ;
        txtSaleTotal.setText(totalMainCopies + "");

        int txtRefTotalCopies = Integer.parseInt(txtRefTotal.getText().toString());
        int totalCopesDiff = totalMainCopies - txtRefTotalCopies;
        txtTotalCopiesDiff.setText(totalCopesDiff + "");

        if (totalCopesDiff < 0) {
            txtTotalCopiesDiff.setTextColor(getResources().getColor(R.color.red));
        } else {
            txtTotalCopiesDiff.setTextColor(getResources().getColor(R.color.green_button_color));

        }

    }


    public void GetINCDECValues() {
        int refQTD = 0;
        int proQTD = 0;
        int total = 0;
        int sumOfInc = 0;
        int sumOfDec = 0;

        int incStatus = 0;
        int decStatus = 0;

        sendPoList = poAdapter.getArrayData();
        if (sendPoList != null) {
            for (int i = 0; i < sendPoList.size(); i++) {
                if (!sendPoList.get(i).getItem_cat().equals("ZFOR")) {

                    // if (sendPoList.get(i).getChangeStatus().equals("1")) {

                    try {

                        refQTD = Integer.parseInt(sendPoList.get(i).getSale_qtd());
                        proQTD = Integer.parseInt(sendPoList.get(i).getProposed_qty());
                        total = proQTD - refQTD;

                        //  total = Integer.parseInt(sendPoList.get(i).getIncreament());

                        if (total > 0) {
                            Log.e("dfsdfsfsd", "Inc");
                            sumOfInc += total;
                            // txtInc.setText(Math.abs(sumOfInc) + "");
                        } else {
                            Log.e("dfsdfsfsd", "Dec");
                            sumOfDec += total;
                            // txtDec.setText(Math.abs(sumOfDec) + "");
                        }
                    } catch (NumberFormatException e) {

                    }
                    // }
                }
            }
            txtInc.setText(Math.abs(sumOfInc) + "");
            txtDec.setText(Math.abs(sumOfDec) + "");
            int getInCText = Integer.parseInt(txtInc.getText().toString());
            int getDescText = Integer.parseInt(txtDec.getText().toString());
            int valTotal = getInCText - getDescText;
            txtINCDECTotal.setText(valTotal + "");

          /*  txtInc.setText("Increase" + " " + sumOfInc + "");
            txtDec.setText("Decrease" + " " + sumOfDec + "");
            int valTotal = sumOfInc + sumOfDec;
            txtINCDECTotal.setText("Total" + " " + valTotal + "");*/
        }

    }


    /*public void GetINCDECValues() {
        int refQTD = 0;
        int proQTD = 0;
        int total = 0;
        int sumOfInc = 0;
        int sumOfDec = 0;

        int incStatus=0;
        int decStatus=0;

        sendPoList = poAdapter.getArrayData();
        if (sendPoList != null) {
            for (int i = 0; i < sendPoList.size(); i++) {
                if (!sendPoList.get(i).getItem_cat().equals("ZFOR")) {

                    if (sendPoList.get(i).getChangeStatus().equals("1")) {

                        try {
                            refQTD = Integer.parseInt(sendPoList.get(i).getSale_qtd());
                            proQTD = Integer.parseInt(sendPoList.get(i).getProposed_qty());
                            total = proQTD - refQTD;

                            if (total >= 0) {
                                Log.e("dfsdfsfsd","Inc");
                                sumOfInc += total;
                            } else {
                                 Log.e("dfsdfsfsd","Dec");
                                sumOfDec += total;
                            }
                        } catch (NumberFormatException e) {

                        }
                    }
                }
            }

            txtInc.setText("Increase" + " " + sumOfInc + "");
            txtDec.setText("Decrease" + " " + sumOfDec + "");
            int valTotal = sumOfInc + sumOfDec;
            txtINCDECTotal.setText("Total" + " " + valTotal + "");
        }


    }*/


    public void OrderApproveORReject(int status) {
        enableLoadingBar(true);


        JSONObject objMain = new JSONObject();
        try {

            JSONArray jsonArray = new JSONArray();


            sendOrders = poAdapter.getArrayData();
            if (sendOrders != null) {
                for (int i = 0; i < sendOrders.size(); i++) {


                    if (strSelectAllStatus.equals("true")) {
                        orderCheckedStatus = 1;
                        if (sendOrders.get(i).getCheckedItem().equals("1")) {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("CIR_Order_Update_id", sendOrders.get(i).getCIROrderId());
                            jsonObject.put("proposed_qty", sendOrders.get(i).getProposed_qty());
                            jsonArray.put(jsonObject);
                        }

                    } else {

                        if (sendOrders.get(i).getCheckedItem().equals("1")) {
                            orderCheckedStatus = 1;
                            JSONObject jsonObject = new JSONObject();
                            int cir = Integer.parseInt(sendOrders.get(i).getCIROrderId());
                            int qtd = Integer.parseInt(sendOrders.get(i).getProposed_qty());
                            jsonObject.put("CIR_Order_Update_id", cir);
                            jsonObject.put("proposed_qty", qtd);
                            jsonArray.put(jsonObject);

                        }
                    }
                }

                objMain.put("selected", jsonArray);
                objMain.put("status", status);
                objMain.put("bp_code", PreferenceManager.getAgentId(PendingOrdersActivity.this));
                Log.e("sdggsdgsgsgd", objMain.toString());

                if (orderCheckedStatus == 1) {

                    AndroidNetworking.post(Constant.BASE_PORTAL_URL + "dash/api/send-order-approval")
                            .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                            .addJSONObjectBody(objMain)
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {

                                        Log.e("sdgsdgsdgsd", "Order App/Rej" + " " + response.toString());

                                        if (response.getString("message").equals("Order Approved Successfully :")) {
                                            enableLoadingBar(false);
                                            Toast.makeText(PendingOrdersActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                                            getTotalQTD();

                                        } else {
                                            enableLoadingBar(false);
                                            Toast.makeText(PendingOrdersActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                                            getTotalQTD();
                                        }


                                    } catch (Exception ex) {
                                        enableLoadingBar(false);
                                        Log.e("sjdfhskfsd", ex.getMessage());
                                    }
                                }

                                @Override
                                public void onError(ANError anError) {
                                    enableLoadingBar(false);
                                    Log.e("sjdfhskfsd", anError.getMessage());
                                }
                            });


                } else {
                    enableLoadingBar(false);
                    Toast.makeText(this, "Select Order", Toast.LENGTH_SHORT).show();
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onOptionClick(DropDownModel dataList) {

        if (dropDownSelectType.equalsIgnoreCase("unit")) {
            Util.hideDropDown();

            tvSaleDistrict.setText(dataList.getName());
            strUnitCode = dataList.getCode();
            getEdition();
            getTotalQTD();

        } else if (dropDownSelectType.equalsIgnoreCase("editions")) {
            Util.hideDropDown();
            Log.e("sdfsdfsdfs", dataList.getName());
            if (dataList.getName().equals("All")) {
                tvCopies.setText(dataList.getName());
                strEditionId = dataList.getId();
                strEdtionCode = "";
                getTotalQTD();
            }

            tvCopies.setText(dataList.getName());
            strEditionId = dataList.getId();
            strEdtionCode = dataList.getCode();
            getTotalQTD();

        } else if (dropDownSelectType.equalsIgnoreCase("status")) {
            Util.hideDropDown();

            tvstatus.setText(dataList.getName());
            strStatusId = dataList.getId();


            if (dataList.getName().equals("All Pending")) {
                OrderStatus = 0;
                selectAll.setVisibility(View.VISIBLE);
            } else if (dataList.getName().equals("Approved")) {

                selectAll.setVisibility(View.GONE);
                OrderStatus = 1;
            } else if (dataList.getName().equals("Rejected")) {
                OrderStatus = 2;
                selectAll.setVisibility(View.GONE);
            }
            else if (dataList.getName().equals("Submitted")){
                OrderStatus = 3;
                selectAll.setVisibility(View.GONE);
            }
            else {
                OrderStatus = 4;
                selectAll.setVisibility(View.VISIBLE);
            }

            getTotalQTD();
        }

    }

    public void getStatus() {

        statusMaster = new ArrayList<>();
        for (int i = 0; i < 5; i++) {

            DropDownModel dropDownModel = new DropDownModel();
            if (i == 0) {

                dropDownModel.setId(i + "");
                dropDownModel.setDescription("Status");
                dropDownModel.setName("Status");
                dropDownModel.setCode("Sta");
                statusMaster.add(dropDownModel);

            } else if (i == 1) {

                dropDownModel.setId(i + "");
                dropDownModel.setDescription("All Pending");
                dropDownModel.setName("All Pending");
                dropDownModel.setCode("Pending");
                statusMaster.add(dropDownModel);

            } else if (i == 2) {

                dropDownModel.setId(i + "");
                dropDownModel.setDescription("Approved");
                dropDownModel.setName("Approved");
                dropDownModel.setCode("App");
                statusMaster.add(dropDownModel);
            }


            else if (i == 3) {

                dropDownModel.setId(i + "");
                dropDownModel.setDescription("Submitted");
                dropDownModel.setName("Submitted");
                dropDownModel.setCode("Sub");
                statusMaster.add(dropDownModel);
            }
            else if (i == 4) {

                dropDownModel.setId(i + "");
                dropDownModel.setDescription("Rejected");
                dropDownModel.setName("Rejected");
                dropDownModel.setCode("Rjt");
                statusMaster.add(dropDownModel);
            }


        }

    }


    public void getTotalQTD() {

        enableLoadingBar(true);


        Log.e("shfvjsafa", strUnitCode);
        Log.e("shfvjsafa", strEdtionCode);
        Log.e("wetwtewetwetwt", OrderStatus + "");

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("edition_filter", strEdtionCode);
            jsonObject.put("sale_po_pick_date", strDate);
            jsonObject.put("agent_id", PreferenceManager.getAgentId(this));
            //jsonObject.put("filter_data", OrderStatus);
            if (OrderStatus == 4) {

                jsonObject.put("filter_data", "");
            } else {

                jsonObject.put("filter_data", OrderStatus);
            }
            jsonObject.put("unit_filter", strUnitCode);
            Log.e("sdfsfsfdsf", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("wetwtewetwetwt", jsonObject.toString());
        AndroidNetworking.post(Constant.BASE_PORTAL_URL + "dash/api/order-approval-data")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        reasonName = new ArrayList<>();
                        reasonName.add("Select Reason");
                        reasonId = new ArrayList<>();
                        reasonId.add("false");
                        int totalDec = 0;
                        int totalInc = 0;


                        String strSaleMain = "", strSaleJJ = "", strRefMain = "", strRefJJ = "";
                        String strSaleFree = "", strRefFree = "", strSaleCopies = "", strRefCopies = "";
                        try {
                            poModalList = new ArrayList<>();
                            txtSaleDate.setText(response.getString("sales_po_date"));
                            txtRefDate.setText(response.getString("refn_po_date"));
                            JSONArray objReason = new JSONArray(response.getString("status_flags"));
                            for (int i = 0; i < objReason.length(); i++) {
                                JSONObject objStr = objReason.getJSONObject(i);
                                reasonName.add(objStr.getString("name"));
                                reasonId.add(objStr.getString("id"));
                            }
                            JSONArray objSummary = new JSONArray(response.getString("summary_data"));
                            Log.e("sfsdfsfsf", objSummary.toString());

                            for (int i = 0; i < objSummary.length(); i++) {
                                JSONObject objStr = objSummary.getJSONObject(i);

                                if (i == 0) {
                                    linearSale.setVisibility(View.GONE);
                                    linearRefrn.setVisibility(View.VISIBLE);


                                    txtSalesMain.setText(objStr.getString("main"));
                                    strSaleMain = objStr.getString("main");

                                    txtSalesJJ.setText(objStr.getString("jj"));
                                    strSaleJJ = objStr.getString("jj");

                                    txtSaleTotal.setText(objStr.getString("copy"));
                                    strSaleCopies = objStr.getString("copy");


                                    txtFreeMain.setText(objStr.getString("zfor"));
                                    strSaleFree = objStr.getString("zfor");

                                    diffstattus = 0;
                                } else if (i == 1) {
                                    linearSale.setVisibility(View.VISIBLE);
                                    linearRefrn.setVisibility(View.VISIBLE);
                                    txtRefMain.setText(objStr.getString("main"));
                                    strRefMain = objStr.getString("main");

                                    txtRefJJ.setText(objStr.getString("jj"));
                                    strRefJJ = objStr.getString("jj");

                                    txtRefTotal.setText(objStr.getString("copy"));
                                    strRefCopies = objStr.getString("copy");

                                    txtFreeRefn.setText(objStr.getString("zfor"));
                                    strRefFree = objStr.getString("zfor");
                                    diffstattus = 1;
                                }
                            }


                            JSONArray objData = new JSONArray(response.getString("portal_data"));
                            Log.e("wetwtewetwetwt", objData.toString());

                            for (int i = 0; i < objData.length(); i++) {
                                JSONObject objStr = objData.getJSONObject(i);

                                DailyPOModal dailyPOModal = new DailyPOModal();

                                if (OrderStatus == 4) {

                                    Log.e("sdfdsfsdssds", "Order Status 4");

                                    dailyPOModal.setAgency_id(i + "");
                                    dailyPOModal.setTotalSaleMain(strSaleMain);
                                    dailyPOModal.setTotalRefMain(strRefMain);

                                    dailyPOModal.setTotalSaleJJ(strSaleJJ);
                                    dailyPOModal.setTotalRefJJ(strRefJJ);

                                    dailyPOModal.setTotalSaleFree(strSaleFree);
                                    dailyPOModal.setTotalRefFree(strRefFree);

                                    dailyPOModal.setTotalSaleCopies(strSaleCopies);
                                    dailyPOModal.setTotalRefCopies(strRefCopies);

                                    dailyPOModal.setBase_min(objStr.getString("base_min"));
                                    dailyPOModal.setBase_max(objStr.getString("base_max"));
                                    dailyPOModal.setBase_copy(objStr.getString("base_copy"));
                                    dailyPOModal.setEdi_name(objStr.getString("edition_name"));
                                    dailyPOModal.setCIROrderId(objStr.getString("CIR_Order_Update_id"));
                                    dailyPOModal.setSold_to_party(objStr.getString("sold_to_party"));
                                    dailyPOModal.setCust_name(objStr.getString("cust_name"));
                                    dailyPOModal.setCity_name(objStr.getString("city_name"));
                                    dailyPOModal.setShip_to_party(objStr.getString("ship_to_party"));
                                    dailyPOModal.setVbeln(objStr.getString("vbeln"));
                                    dailyPOModal.setPosnr(objStr.getString("posnr"));
                                    dailyPOModal.setPstyv(objStr.getString("pstyv"));
                                    dailyPOModal.setPick_date(objStr.getString("ord_date_f"));
                                    dailyPOModal.setStoreProposed(objStr.getString("store_proposed"));
                                    dailyPOModal.setReason_qty_change(objStr.getString("reason_qty_change"));
                                    // dailyPOModal.setPva(objStr.getString("pva"));
                                    dailyPOModal.setProposed_qty(objStr.getString("proposed_qty"));
                                    dailyPOModal.setItem_cat(objStr.getString("pstyv"));
                                    dailyPOModal.setRemark(objStr.getString("remark"));
                                    // dailyPOModal.setRef_qtd(objStr.getString("refn_proposed_qty"));
                                    dailyPOModal.setSale_qtd(objStr.getString("sales_proposed_qty"));
                                    dailyPOModal.setOrderApproval(objStr.getString("approval_status"));
                                    dailyPOModal.setRj_sh_flag(objStr.getString("rj_sh_flag"));
                                    dailyPOModal.setApprovalINCDEC(objStr.getString("per_inc_dec"));
                                    dailyPOModal.setCopies_type(objStr.getString("main_jj"));
                                    dailyPOModal.setStatus(OrderStatus + "");
                                    dailyPOModal.setChangeStatus("0");
                                    //dailyPOModal.setInc_dsc(objStr.getString("per_inc_dec"));

                                    try {
                                        String pq = objStr.getString("proposed_qty");
                                        String sq = objStr.getString("sales_proposed_qty");
                                        int Proposed = Integer.parseInt(pq);
                                        int Sales = Integer.parseInt(sq);
                                        int getIncDec = Proposed - Sales;
                                        dailyPOModal.setInc_dsc(String.valueOf(getIncDec));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (NumberFormatException e) {
                                        e.printStackTrace();
                                    }
                                    dailyPOModal.setPerincreased("0");
                                    dailyPOModal.setIs_approval("1");
                                    dailyPOModal.setIncreament("0");
                                    dailyPOModal.setCheckedItem("0");

                                    if (diffstattus == 1) {

                                        dailyPOModal.setMainSaleTotal(txtSalesMain.getText().toString());
                                        dailyPOModal.setRefTotal(txtRefMain.getText().toString());
                                    }
                                    dailyPOModal.setDiffStatus(diffstattus + "");
                                    poModalList.add(dailyPOModal);
                                    int getIncDec = Integer.parseInt(objStr.getString("per_inc_dec"));
                                    if (getIncDec < 0) {
                                        totalDec += getIncDec;
                                    } else {
                                        totalInc += getIncDec;
                                    }
                                    txtInc.setText(Math.abs(totalInc) + "");
                                    txtDec.setText(Math.abs(totalDec) + "");

                                } else {
                                    Log.e("sdfdsfsdssds", "Else");
                                    dailyPOModal.setAgency_id(i + "");
                                    dailyPOModal.setTotalSaleMain(strSaleMain);
                                    dailyPOModal.setTotalRefMain(strRefMain);

                                    dailyPOModal.setTotalSaleJJ(strSaleJJ);
                                    dailyPOModal.setTotalRefJJ(strRefJJ);

                                    dailyPOModal.setTotalSaleFree(strSaleFree);
                                    dailyPOModal.setTotalRefFree(strRefFree);

                                    dailyPOModal.setTotalSaleCopies(strSaleCopies);
                                    dailyPOModal.setTotalRefCopies(strRefCopies);

                                    dailyPOModal.setBase_min(objStr.getString("base_min"));
                                    dailyPOModal.setBase_max(objStr.getString("base_max"));
                                    dailyPOModal.setBase_copy(objStr.getString("base_copy"));
                                    dailyPOModal.setEdi_name(objStr.getString("edition_name"));
                                    dailyPOModal.setCIROrderId(objStr.getString("CIR_Order_Update_id"));
                                    dailyPOModal.setSold_to_party(objStr.getString("sold_to_party"));
                                    dailyPOModal.setCust_name(objStr.getString("cust_name"));
                                    dailyPOModal.setCity_name(objStr.getString("city_name"));
                                    dailyPOModal.setShip_to_party(objStr.getString("ship_to_party"));
                                    dailyPOModal.setVbeln(objStr.getString("vbeln"));
                                    dailyPOModal.setPosnr(objStr.getString("posnr"));
                                    dailyPOModal.setPstyv(objStr.getString("pstyv"));
                                    dailyPOModal.setPick_date(objStr.getString("ord_date_f"));
                                    dailyPOModal.setStoreProposed(objStr.getString("store_proposed"));
                                    dailyPOModal.setStoreProposed(objStr.getString("store_proposed"));
                                    dailyPOModal.setReason_qty_change(objStr.getString("reason_qty_change"));
                                    // dailyPOModal.setPva(objStr.getString("pva"));
                                    dailyPOModal.setProposed_qty(objStr.getString("proposed_qty"));
                                    dailyPOModal.setItem_cat(objStr.getString("pstyv"));
                                    dailyPOModal.setRemark(objStr.getString("remark"));
                                    // dailyPOModal.setRef_qtd(objStr.getString("refn_proposed_qty"));
                                    dailyPOModal.setSale_qtd(objStr.getString("sales_proposed_qty"));
                                    dailyPOModal.setOrderApproval(objStr.getString("approval_status"));
                                    dailyPOModal.setRj_sh_flag(objStr.getString("rj_sh_flag"));
                                    dailyPOModal.setApprovalINCDEC(objStr.getString("per_inc_dec"));
                                    dailyPOModal.setCopies_type(objStr.getString("main_jj"));
                                    dailyPOModal.setStatus(OrderStatus + "");
                                    dailyPOModal.setChangeStatus("0");
                                    //dailyPOModal.setInc_dsc(objStr.getString("per_inc_dec"));

                                    try {
                                        String pq = objStr.getString("proposed_qty");
                                        String sq = objStr.getString("sales_proposed_qty");
                                        int Proposed = Integer.parseInt(pq);
                                        int Sales = Integer.parseInt(sq);
                                        int getIncDec = Proposed - Sales;
                                        dailyPOModal.setInc_dsc(String.valueOf(getIncDec));
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (NumberFormatException e) {
                                        e.printStackTrace();
                                    }
                                    dailyPOModal.setPerincreased("0");
                                    dailyPOModal.setIs_approval("1");
                                    dailyPOModal.setIncreament("0");

                                    dailyPOModal.setCheckedItem("0");

                                    if (diffstattus == 1) {

                                        dailyPOModal.setMainSaleTotal(txtSalesMain.getText().toString());
                                        dailyPOModal.setRefTotal(txtRefMain.getText().toString());
                                    }
                                    dailyPOModal.setDiffStatus(diffstattus + "");
                                    poModalList.add(dailyPOModal);

                                    int getIncDec = Integer.parseInt(objStr.getString("per_inc_dec"));
                                    if (getIncDec < 0) {
                                        totalDec += getIncDec;
                                    } else {
                                        totalInc += getIncDec;
                                    }

                                    txtInc.setText(Math.abs(totalInc) + "");
                                    txtDec.setText(Math.abs(totalDec) + "");
                                }
                            }

                            Log.e("sdfsdfsfsf", totalInc + "");
                            Log.e("sdfsdfsfsf", totalDec + "");
                            int xInc = Math.abs(totalInc);
                            int xDec = Math.abs(totalDec);
                            int totInc = xInc - xDec;
                            Log.e("sdfsdfsfsf", totInc + "");

                            txtINCDECTotal.setText(totInc + "");

                            if (diffstattus == 1) {

                                int txtSaleTotal = Integer.parseInt(PendingOrdersActivity.txtSaleTotal.getText().toString());
                                int txtRef = Integer.parseInt(PendingOrdersActivity.txtRefTotal.getText().toString());
                                int txtRefMain = Integer.parseInt(PendingOrdersActivity.txtSalesMain.getText().toString());
                                int txtRefTotal = Integer.parseInt(PendingOrdersActivity.txtRefMain.getText().toString());

                                int saleJJ = Integer.parseInt(PendingOrdersActivity.txtSalesJJ.getText().toString());
                                int refJJ = Integer.parseInt(PendingOrdersActivity.txtRefJJ.getText().toString());
                                int jjDiff = saleJJ - refJJ;
                                txtJJDiff.setText(jjDiff + "");

                                if (jjDiff < 0) {
                                    txtJJDiff.setTextColor(getResources().getColor(R.color.red));
                                } else {
                                    txtJJDiff.setTextColor(getResources().getColor(R.color.green_button_color));
                                }

                                int saleFree = Integer.parseInt(PendingOrdersActivity.txtFreeMain.getText().toString());
                                int refFree = Integer.parseInt(PendingOrdersActivity.txtFreeRefn.getText().toString());
                                int freeDiff = saleFree - refFree;
                                txtFreeDiff.setText(freeDiff + "");

                                if (freeDiff < 0) {
                                    txtFreeDiff.setTextColor(getResources().getColor(R.color.red));
                                } else {
                                    txtFreeDiff.setTextColor(getResources().getColor(R.color.green_button_color));
                                }


                                int mainDiff = txtRefMain - txtRefTotal;
                                int CopiesDiff = txtSaleTotal - txtRef;

                                txtMainDiff.setText(mainDiff + "");
                                txtTotalCopiesDiff.setText(CopiesDiff + "");

                                if (mainDiff < 0) {
                                    txtMainDiff.setTextColor(getResources().getColor(R.color.red));
                                } else {
                                    txtMainDiff.setTextColor(getResources().getColor(R.color.green_button_color));
                                }

                                if (CopiesDiff < 0) {
                                    txtTotalCopiesDiff.setTextColor(getResources().getColor(R.color.red));
                                } else {
                                    txtTotalCopiesDiff.setTextColor(getResources().getColor(R.color.green_button_color));
                                }
                            }

                            SetAdapter();
                            if (poModalList.size() == 0) {

                                Toast.makeText(PendingOrdersActivity.this, "Orders not found", Toast.LENGTH_SHORT).show();
                                selectAll.setVisibility(View.GONE);
                                btnlayout.setVisibility(View.GONE);
                            } else {


                                if (OrderStatus == 0) {
                                    txtInc.setText(Math.abs(totalInc) + "");
                                    txtDec.setText(Math.abs(totalDec) + "");
                                    btnlayout.setVisibility(View.VISIBLE);
                                    selectAll.setVisibility(View.VISIBLE);
                                } else {

                                    btnlayout.setVisibility(View.GONE);
                                    selectAll.setVisibility(View.GONE);
                                }
                            }

                        } catch (Exception ex) {
                            enableLoadingBar(false);
                            Log.e("sjdfhskfsd", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        enableLoadingBar(false);
                        Log.e("sjdfhskfsd", anError.getMessage());
                    }
                });
    }

    public void SetAdapter() {
        if (poModalList.size() == 0) {
            btnlayout.setVisibility(View.GONE);
        } else {
            btnlayout.setVisibility(View.VISIBLE);

        }
        poAdapter = new PendingOrderAdapter(poModalList, PendingOrdersActivity.this, reasonName, reasonId, allstatus);
        recyclerAgency.setAdapter(poAdapter);
        enableLoadingBar(false);
        //  GetINCDECValues();

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                String text = search.getText().toString().toLowerCase(Locale.getDefault());
                Log.e("bhs==>>", text);

                poAdapter.getFilter().filter(cs);
                poAdapter.notifyDataSetChanged();

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    public void getUnit() {

        flagunit = false;

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("unit_filter", "");
            jsonObject.put("edition_filter", "");
            jsonObject.put("sale_po_pick_date", strDate);
            jsonObject.put("agent_id", PreferenceManager.getAgentId(this));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(Constant.BASE_PORTAL_URL + "dash/api/generate-order-data")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            unitMaster = new ArrayList<>();
                            Log.e("sfsdfsfsf", response.toString());
                            JSONArray objData = new JSONArray(response.getString("units"));

                            for (int i = 0; i < objData.length(); i++) {
                                JSONObject objStr = objData.getJSONObject(i);
                                DropDownModel dropDownModel = new DropDownModel();
                                dropDownModel.setId(objStr.getString("unit_id"));
                                dropDownModel.setDescription(objStr.getString("unit_name"));
                                dropDownModel.setName(objStr.getString("unit_name"));
                                dropDownModel.setCode(objStr.getString("unit_code"));
                                unitMaster.add(dropDownModel);
                            }
                            flagunit = true;

                        } catch (Exception ex) {
                            Log.e("sjdfhskfsd", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        Log.e("sjdfhskfsd", anError.getMessage());
                    }
                });
    }


    public void getEdition() {
        flagEdition = false;
        Log.e("werwerwerwe", strUnitCode);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("edition_filter", "");
            jsonObject.put("filter_data", 0);
            jsonObject.put("unit_filter", strUnitCode);
            jsonObject.put("sale_po_pick_date", strDate);
            jsonObject.put("agent_id", PreferenceManager.getAgentId(this));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // AndroidNetworking.post(Constant.BASE_PORTAL_URL + "dash/api/generate-order-data")
        AndroidNetworking.post(Constant.BASE_PORTAL_URL + "dash/api/order-approval-data")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            editionMaster = new ArrayList<>();
                            DropDownModel dropDownModel2 = new DropDownModel();
                            dropDownModel2.setId("1001");
                            dropDownModel2.setDescription("All");
                            dropDownModel2.setName("All");
                            dropDownModel2.setCode("all");
                            editionMaster.add(dropDownModel2);

                            Log.e("sfsdfsfsf", response.toString());
                            JSONArray objData = new JSONArray(response.getString("editions"));
                            for (int i = 0; i < objData.length(); i++) {

                                JSONObject objStr = objData.getJSONObject(i);
                                DropDownModel dropDownModel = new DropDownModel();
                                dropDownModel.setId(objStr.getString("edition_id"));
                                dropDownModel.setDescription(objStr.getString("edition_name"));
                                dropDownModel.setName(objStr.getString("edition_name"));
                                dropDownModel.setCode(objStr.getString("edition_code"));
                                editionMaster.add(dropDownModel);

                            }
                            flagEdition = true;

                        } catch (Exception ex) {
                            Log.e("sjdfhskfsd", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        Log.e("sjdfhskfsd", anError.getMessage());
                    }
                });
    }

    public void enableLoadingBar(boolean enable) {
        if (enable) {
            loadProgressBar(null, null, false);
        } else {
            dismissProgressBar();
        }
    }

}