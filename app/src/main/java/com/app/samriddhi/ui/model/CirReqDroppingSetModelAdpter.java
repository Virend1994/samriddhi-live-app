package com.app.samriddhi.ui.model;

import java.util.ArrayList;

public class CirReqDroppingSetModelAdpter {

    String droppingPk,self_copy,contact,noofcopy,routePk,routeName,droppingName,sub_agent_name,editionName,editionPk,location;

    public String getEditionName() {
        return editionName;
    }

    public void setEditionName(String editionName) {
        this.editionName = editionName;
    }

    public String getEditionPk() {
        return editionPk;
    }

    public void setEditionPk(String editionPk) {
        this.editionPk = editionPk;
    }

    ArrayList<EditionCRModel> CirReqDropping_set;

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public ArrayList<EditionCRModel> getCirReqDropping_set() {
        return CirReqDropping_set;
    }

    public void setCirReqDropping_set(ArrayList<EditionCRModel> cirReqDropping_set) {
        CirReqDropping_set = cirReqDropping_set;
    }

    public String getSub_agent_name() {
        return sub_agent_name;
    }

    public void setSub_agent_name(String sub_agent_name) {
        this.sub_agent_name = sub_agent_name;
    }

    public String getDroppingPk() {
        return droppingPk;
    }

    public void setDroppingPk(String droppingPk) {
        this.droppingPk = droppingPk;
    }

    public String getSelf_copy() {
        return self_copy;
    }

    public void setSelf_copy(String self_copy) {
        this.self_copy = self_copy;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getNoofcopy() {
        return noofcopy;
    }

    public void setNoofcopy(String noofcopy) {
        this.noofcopy = noofcopy;
    }

    public String getRoutePk() {
        return routePk;
    }

    public void setRoutePk(String routePk) {
        this.routePk = routePk;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public String getDroppingName() {
        return droppingName;
    }

    public void setDroppingName(String droppingName) {
        this.droppingName = droppingName;
    }
}
