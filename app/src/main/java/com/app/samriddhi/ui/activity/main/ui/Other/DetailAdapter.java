package com.app.samriddhi.ui.activity.main.ui.Other;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.Adapter.PoAdapter;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.DailyPoActivity;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.POModal.DailyPOModal;
import com.google.android.material.card.MaterialCardView;

import java.util.ArrayList;
import java.util.List;

public class DetailAdapter extends RecyclerView.Adapter<DetailAdapter.ViewHolder>{

    Context context;
    List<DetailModal> dataAdapters;


    public DetailAdapter(List<DetailModal> getDataAdapter, Context context) {

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.detail_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, @SuppressLint("RecyclerView") int position) {

        DetailModal dataAdapterOBJ = dataAdapters.get(position);
        Viewholder.setIsRecyclable(false);
        Viewholder.detailName.setText(dataAdapterOBJ.getName());

       /* Viewholder.cardViewMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });*/

    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }



    class ViewHolder extends RecyclerView.ViewHolder {

        TextView detailName;

        public ViewHolder(View itemView) {
            super(itemView);

            detailName = itemView.findViewById(R.id.detailName);

        }
    }

}