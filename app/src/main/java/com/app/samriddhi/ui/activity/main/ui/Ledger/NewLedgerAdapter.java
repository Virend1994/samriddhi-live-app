package com.app.samriddhi.ui.activity.main.ui.Ledger;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;
import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AppsContants;
import com.app.samriddhi.ui.model.NewLedgerModel;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.textview.MaterialTextView;

import java.util.List;

public class NewLedgerAdapter extends RecyclerView.Adapter<NewLedgerAdapter.ViewHolder> {
    Context context;
    List<NewLedgerModel> dataAdapters;

    public NewLedgerAdapter(List<NewLedgerModel> getDataAdapter, Context context) {
        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ledger, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, @SuppressLint("RecyclerView") int position) {

        NewLedgerModel dataAdapterOBJ = dataAdapters.get(position);
        Viewholder.setIsRecyclable(false);

        Viewholder.txtDate.setText(dataAdapterOBJ.getVDate());
        Viewholder.txtNarration.setText(dataAdapterOBJ.getNarration());
        Viewholder.txtDebit.setText(dataAdapterOBJ.getDebit());
        Viewholder.txtCredit.setText(dataAdapterOBJ.getCredit());
        Viewholder.txtBalance.setText(dataAdapterOBJ.getBalance());
        Log.e("dgsdgsdgsgs",dataAdapterOBJ.getCredit());
        Log.e("dgsdgsdgsgs",dataAdapterOBJ.getDebit());

    }

    @Override
    public int getItemCount() {
        return dataAdapters.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        MaterialTextView txtBalance, txtCredit, txtDebit,txtNarration,txtDate;

        public ViewHolder(View itemView) {
            super(itemView);
            txtDate = itemView.findViewById(R.id.txtDate);
            txtNarration = itemView.findViewById(R.id.txtNarration);
            txtDebit = itemView.findViewById(R.id.txtDebit);
            txtCredit = itemView.findViewById(R.id.txtCredit);
            txtBalance = itemView.findViewById(R.id.txtBalance);
        }
    }
}