package com.app.samriddhi.ui.model;

import androidx.annotation.NonNull;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NavSubagentList extends BaseModel {


    @SerializedName("SoldToParty")
    @Expose
    private String soldToParty;
    @SerializedName("CityName")
    @Expose
    private String cityName;
    @SerializedName("ShipToParty")
    @Expose
    private String shipToParty;


    public String getSoldToParty() {
        return soldToParty;
    }


    public void setSoldToParty(String soldToParty) {
        this.soldToParty = soldToParty;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getShipToParty() {
        return shipToParty;
    }

    public void setShipToParty(String shipToParty) {
        this.shipToParty = shipToParty;
    }

    @NonNull
    @Override
    public String toString() {
        return cityName;
    }
}
