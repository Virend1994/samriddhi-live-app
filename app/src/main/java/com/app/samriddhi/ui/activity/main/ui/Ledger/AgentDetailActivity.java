package com.app.samriddhi.ui.activity.main.ui.Ledger;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AppsContants;
import com.app.samriddhi.util.Constant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class AgentDetailActivity extends BaseActivity {

    RecyclerView recyclerview;
    LinearLayoutManager linearLayoutManager;
    List<AgentDetailModal> agentDetailModals;
    AgentDetailAdapter agentDetailAdapter;
    TextView tvDate;

    Calendar myCalendar;
    String strDate = "";
    TextView txtFromDate,txtToDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_detail);

        AppsContants.sharedpreferences =getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strAgentName = AppsContants.sharedpreferences.getString(AppsContants.AgencyName, "");

        TextView txtAgenctName=findViewById(R.id.txtAgenctName);
        txtAgenctName.setText(strAgentName);

        tvDate = findViewById(R.id.tvDate);
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 0);
        tvDate.setText(dateFormat.format(cal.getTime()));

        ImageView imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });



        myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener dateTO = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateToLabel("To");
            }
        };


        myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener dateFrom = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {

                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateToLabel("from");
            }
        };

        txtFromDate=findViewById(R.id.txtFromDate);
        txtFromDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DatePickerDialog(AgentDetailActivity.this, dateFrom, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        txtToDate=findViewById(R.id.txtToDate);
        txtToDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DatePickerDialog(AgentDetailActivity.this, dateTO, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        recyclerview = findViewById(R.id.recyclerview);
        recyclerview.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(AgentDetailActivity.this);
        recyclerview.setLayoutManager(linearLayoutManager);
        getTransactionDetail();
    }


    private void updateToLabel(String status) {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        strDate = sdf.format(myCalendar.getTime());
        if(status.equals("To")){
            txtToDate.setText(strDate);
        }
        else {
            txtFromDate.setText(strDate);
        }
    }



    public void getTransactionDetail() {
        agentDetailModals = new ArrayList<>();
        enableLoadingBar(true);


        String strUserId= PreferenceManager.getAgentId(AgentDetailActivity.this);
        AppsContants.sharedpreferences =getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strAgencyId = AppsContants.sharedpreferences.getString(AppsContants.AgencyId, "");



        System.out.println("APICALL"+ Constant.BASE_PORTAL_URL+"ledger/api/all-in-one/"+strAgencyId);

        AndroidNetworking.get(Constant.BASE_PORTAL_URL+"ledger/api/all-in-one/"+strAgencyId)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if(response.getString("message").equals("Records are available.")){
                                JSONArray jsonArray=response.getJSONArray("data");
                                int pos=0;
                                agentDetailModals = new ArrayList<>();
                                for (int i = 0; i <jsonArray.length(); i++) {
                                    pos++;

                                    AgentDetailModal agentDetailModal = new AgentDetailModal();
                                    JSONObject jsonObject=jsonArray.getJSONObject(i);
                                    agentDetailModal.setSno(pos+"");
                                    agentDetailModal.setTran_date(jsonObject.getString("tran_date"));
                                    agentDetailModal.setTran_type(jsonObject.getString("tran_type"));
                                    agentDetailModal.setTran_amt(jsonObject.getString("tran_amount"));
                                    agentDetailModal.setTran_reason(jsonObject.getString("tran_reason"));
                                    agentDetailModals.add(agentDetailModal);
                                }

                                agentDetailAdapter=new AgentDetailAdapter(agentDetailModals,AgentDetailActivity.this);
                                recyclerview.setAdapter(agentDetailAdapter);
                                enableLoadingBar(false);
                            }
                        }
                        catch (Exception ex){
                            enableLoadingBar(false);
                            Log.e("Sdfsdfs",ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        enableLoadingBar(false);
                        Log.e("Sdfsdfs",anError.getErrorBody());

                    }
                });
    }
}