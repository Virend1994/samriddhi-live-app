package com.app.samriddhi.ui.presenter;

import android.util.Log;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.response.BaseResponse;
import com.app.samriddhi.api.response.JsonArrayResponse;
import com.app.samriddhi.base.BasePresenter;
import com.app.samriddhi.ui.model.NotificationModel;
import com.app.samriddhi.ui.view.INotificationView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationPresenter extends BasePresenter<INotificationView> {


    /**
     *  get all teh notifications of logged in user
     * @param bpCode logged in user id
     */
    public void getNotifications(String bpCode) {

        Log.e("sdfsdfsd",bpCode);
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getNotifications(bpCode).enqueue(new Callback<JsonArrayResponse<NotificationModel>>() {
            @Override
            public void onResponse(Call<JsonArrayResponse<NotificationModel>> call, Response<JsonArrayResponse<NotificationModel>> response) {
                getView().enableLoadingBar(false);

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body() != null)
                            getView().onNotificationsSuccess(response.body().list);
                        else
                            getView().onError(response.body().replyMsg);
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonArrayResponse<NotificationModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

    /**
     * update the notification read status
     * @param bpCode logged in user id
     */
    public void updateNotificationCount(String bpCode) {
        SamriddhiApplication.getmInstance().getApiService().notificationRead(bpCode).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {

                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body() != null)
                            getView().onInfo(response.message());
                        else
                            getView().onInfo(response.body().replyMsg);
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onInfo(null);
            }
        });
    }
}
