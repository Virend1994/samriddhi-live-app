package com.app.samriddhi.ui.activity.auth;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.databinding.ActivitySignupBinding;
import com.app.samriddhi.ui.model.UserModel;
import com.app.samriddhi.ui.presenter.SignUpPresenter;
import com.app.samriddhi.ui.view.ISignUpView;
import com.app.samriddhi.util.AlertUtility;
import com.app.samriddhi.util.StringUtility;
import com.app.samriddhi.util.SystemUtility;

public class SignUp extends BaseActivity implements TextWatcher, ISignUpView {
    ActivitySignupBinding mBinding;
    SignUpPresenter mSignUpPresenter;
    UserModel userModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_signup);
        mBinding.edOtp.addTextChangedListener(this);
        mSignUpPresenter = new SignUpPresenter();
        mSignUpPresenter.setView(this);

    }
/* Handle buttons click*/
    public void onItemsClick(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:
                if (isValidAllFields()) {
                    userModel.setDevice_id(SystemUtility.getDeviceId(this));
                    mSignUpPresenter.registerUser(userModel);
                }
                break;
            case R.id.txt_click_login:
                onBackPressed();
                break;
            case R.id.btn_send_otp:
                if (!StringUtility.validateString(mBinding.edAgentId.getText().toString())) {
                    snackBar(mBinding.edAgentId, R.string.error_agent_id_empty);
                    return;
                } else if (!StringUtility.validateString(mBinding.edMobileNumber.getText().toString())) {
                    snackBar(mBinding.edMobileNumber, R.string.str_mobile_no_empty);
                    return;
                } else if (!StringUtility.validateMobileNumber(mBinding.edMobileNumber)) {
                    snackBar(mBinding.edMobileNumber, R.string.str_mobile_no_validate_error);
                    return;
                } else {
                    mSignUpPresenter.getOtp(mBinding.edAgentId.getText().toString(), mBinding.edMobileNumber.getText().toString());
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        mBinding.edOtp.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mobile, 0, 0, 0);
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        mBinding.edOtp.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mobile, 0, 0, 0);
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s.length() == 4) {
            if (s.toString().equals(userModel.getOtp())) {
                mBinding.edOtp.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mobile, 0, R.drawable.check, 0);
                mBinding.edOtp.setEnabled(false);
                userModel.setOtpVerified(true);
            } else {
                userModel.setOtpVerified(false);

                mBinding.edOtp.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mobile, 0, R.drawable.cross, 0);
            }
        } else {
            mBinding.edOtp.setCompoundDrawablesWithIntrinsicBounds(R.drawable.mobile, 0, 0, 0);
        }

    }

/* After verifying the Otp on mobile number success */

    @Override
    public void onSuccess(UserModel userModel) {
        if (userModel != null) {
            String id = mBinding.edAgentId.getText().toString();
            this.userModel = userModel;
            userModel.setBp_code(id);
            mBinding.setUser(userModel);
        }

    }

/*Response handle after the success from the server on SignUp process*/
    @Override
    public void onRegisterSuccess(String msg) {
        AlertUtility.showAlertWithListener(this, msg, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
                startActivity(new Intent(SignUp.this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
            }
        });


    }

    @Override
    public Context getContext() {
        return this;
    }

/* Check the validations on all fields of sign up form before submitting to server*/
    private boolean isValidAllFields() {
        if (!StringUtility.validateEditText(mBinding.edPassword)) {
            snackBar(mBinding.edPassword, R.string.str_pwd_empty);
            return false;
        } else if (mBinding.edPassword.getText().toString().length() < 6) {
            snackBar(mBinding.edPassword, R.string.str_password_length);
            return false;
        } else if (!StringUtility.validateEditText(mBinding.edConfirmPassword)) {
            snackBar(mBinding.edConfirmPassword, R.string.str_confirm_pwd_empty);
            return false;
        } else if (!(mBinding.edPassword.getText().toString()).equals(mBinding.edConfirmPassword.getText().toString())) {
            snackBar(mBinding.edPassword, R.string.str_pwd_not_match);
            return false;
        } else {
            userModel.setPassword(mBinding.edPassword.getText().toString());
            return true;
        }
    }
}
