package com.app.samriddhi.ui.model;

import java.util.List;

public class AgencyRequestCIRReqSetModel {
    public List<CirReqEditionSetModel> CirReqEdition_set;
    public List<CirReqDroppingSetModel> CirReqDropping_set;
    public List<CirReqChequeSetModel> CirReqCheque_set;
    public int edition_count;
    public Object start_date;
    public int ag_detail_id;
    public int commission_per;
    public String commission_amt;
    public  String mainorjj;
    public String asd_as_per_norms;
    public String asd_collected;
    public int cheque_count;
    public String asd_mode;
    public String asd_days;
    public int dropping_count;
    public String executive_code;
    public String executive_name;
    public String UOH_code;
    public String UOH_name;
    public String SOH_code;
    public String SOH_name;
    public String creation_comments;
    public String copies_rate;


    public String getCopies_rate() {
        return copies_rate;
    }

    public void setCopies_rate(String copies_rate) {
        this.copies_rate = copies_rate;
    }

    public String getMainorjj() {
        return mainorjj;
    }

    public void setMainorjj(String mainorjj) {
        this.mainorjj = mainorjj;
    }

    public String getUOH_code() {
        return UOH_code;
    }

    public void setUOH_code(String UOH_code) {
        this.UOH_code = UOH_code;
    }

    public String getUOH_name() {
        return UOH_name;
    }

    public void setUOH_name(String UOH_name) {
        this.UOH_name = UOH_name;
    }

    public String getSOH_code() {
        return SOH_code;
    }

    public void setSOH_code(String SOH_code) {
        this.SOH_code = SOH_code;
    }

    public String getSOH_name() {
        return SOH_name;
    }

    public void setSOH_name(String SOH_name) {
        this.SOH_name = SOH_name;
    }

    public int getAg_detail_id() {
        return ag_detail_id;
    }

    public void setAg_detail_id(int ag_detail_id) {
        this.ag_detail_id = ag_detail_id;
    }

    public List<CirReqEditionSetModel> getCirReqEdition_set() {
        return CirReqEdition_set;
    }

    public void setCirReqEdition_set(List<CirReqEditionSetModel> cirReqEdition_set) {
        this.CirReqEdition_set = cirReqEdition_set;
    }

    public List<CirReqDroppingSetModel> getCirReqDropping_set() {
        return CirReqDropping_set;
    }


    public void setCirReqDropping_set(List<CirReqDroppingSetModel> cirReqDropping_set) {
        this.CirReqDropping_set = cirReqDropping_set;
    }

    public List<CirReqChequeSetModel> getCirReqCheque_set() {
        return CirReqCheque_set;
    }

    public void setCirReqCheque_set(List<CirReqChequeSetModel> cirReqCheque_set) {
        this.CirReqCheque_set = cirReqCheque_set;
    }

    public int getEdition_count() {
        return edition_count;
    }

    public void setEdition_count(int edition_count) {
        this.edition_count = edition_count;
    }

    public Object getStart_date() {
        return start_date;
    }

    public void setStart_date(Object start_date) {
        this.start_date = start_date;
    }

    public int getCommission_per() {
        return commission_per;
    }

    public void setCommission_per(int commission_per) {
        this.commission_per = commission_per;
    }

    public String getCommission_amt() {
        return commission_amt;
    }

    public void setCommission_amt(String commission_amt) {
        this.commission_amt = commission_amt;
    }

    public String getAsd_as_per_norms() {
        return asd_as_per_norms;
    }

    public void setAsd_as_per_norms(String asd_as_per_norms) {
        this.asd_as_per_norms = asd_as_per_norms;
    }

    public String getAsd_collected() {
        return asd_collected;
    }

    public void setAsd_collected(String asd_collected) {
        this.asd_collected = asd_collected;
    }

    public int getCheque_count() {
        return cheque_count;
    }

    public void setCheque_count(int cheque_count) {
        this.cheque_count = cheque_count;
    }

    public String getAsd_mode() {
        return asd_mode;
    }

    public void setAsd_mode(String asd_mode) {
        this.asd_mode = asd_mode;
    }

    public String getAsd_days() {
        return asd_days;
    }

    public void setAsd_days(String asd_days) {
        this.asd_days = asd_days;
    }

    public int getDropping_count() {
        return dropping_count;
    }

    public void setDropping_count(int dropping_count) {
        this.dropping_count = dropping_count;
    }

    public String getExecutive_code() {
        return executive_code;
    }

    public void setExecutive_code(String executive_code) {
        this.executive_code = executive_code;
    }

    public String getExecutive_name() {
        return executive_name;
    }

    public void setExecutive_name(String executive_name) {
        this.executive_name = executive_name;
    }

    public String getCreation_comments() {
        return creation_comments;
    }

    public void setCreation_comments(String creation_comments) {
        this.creation_comments = creation_comments;
    }
}
