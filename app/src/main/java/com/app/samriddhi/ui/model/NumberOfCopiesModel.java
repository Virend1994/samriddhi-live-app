package com.app.samriddhi.ui.model;


import android.os.Parcel;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class NumberOfCopiesModel extends BaseModel {
    public static final Creator<NumberOfCopiesModel> CREATOR = new Creator<NumberOfCopiesModel>() {
        @Override
        public NumberOfCopiesModel createFromParcel(Parcel source) {
            return new NumberOfCopiesModel(source);
        }

        @Override
        public NumberOfCopiesModel[] newArray(int size) {
            return new NumberOfCopiesModel[size];
        }
    };
    @SerializedName("results")
    @Expose
    private ArrayList<CopiesData> results = null;

    public NumberOfCopiesModel() {
    }


    protected NumberOfCopiesModel(Parcel in) {
        super(in);
        this.results = in.createTypedArrayList(CopiesData.CREATOR);
    }

    public ArrayList<CopiesData> getResults() {
        return results;
    }

    public void setResults(ArrayList<CopiesData> results) {
        this.results = results;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        super.writeToParcel(dest, flags);
        dest.writeTypedList(this.results);
    }
}
