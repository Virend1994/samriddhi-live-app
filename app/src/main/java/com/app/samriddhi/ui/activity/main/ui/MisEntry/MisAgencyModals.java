package com.app.samriddhi.ui.activity.main.ui.MisEntry;

public class MisAgencyModals {

    public String id;
    public String agency_name;
    public String agency_code;
    public String edition;
    public String edition_code;
    public String po;
    public String unsold;
    public String nps;
    public String center;
    public String executive_name;
    public String MainJJ;
    public String editStatus;
    public String City_UPC;
    public String Executive_Code;
    public String posnr;
    public String Cash_Credit;
    public String Ord_Date;
    public String fresh_unsold;
    public String old_unsold;
    public String pstyv;
    public String vbeln;
    public String totalNPS;
    public String totalUnsold;
    public String totalPO;
    public String totalFreeCopies;
    public String freshOldStatus;
    public String cityupcraw;
    public String cashcreditraw;
    public String Ord_Date_Int;

    public String getEdition_code() {
        return edition_code;
    }

    public void setEdition_code(String edition_code) {
        this.edition_code = edition_code;
    }

    public String getCityupcraw() {
        return cityupcraw;
    }

    public void setCityupcraw(String cityupcraw) {
        this.cityupcraw = cityupcraw;
    }

    public String getCashcreditraw() {
        return cashcreditraw;
    }

    public void setCashcreditraw(String cashcreditraw) {
        this.cashcreditraw = cashcreditraw;
    }

    public String getOrd_Date_Int() {
        return Ord_Date_Int;
    }

    public void setOrd_Date_Int(String ord_Date_Int) {
        Ord_Date_Int = ord_Date_Int;
    }

    public String getFreshOldStatus() {
        return freshOldStatus;
    }

    public void setFreshOldStatus(String freshOldStatus) {
        this.freshOldStatus = freshOldStatus;
    }

    public String getTotalFreeCopies() {
        return totalFreeCopies;
    }

    public void setTotalFreeCopies(String totalFreeCopies) {
        this.totalFreeCopies = totalFreeCopies;
    }

    public String getTotalPO() {
        return totalPO;
    }

    public void setTotalPO(String totalPO) {
        this.totalPO = totalPO;
    }

    public String getTotalNPS() {
        return totalNPS;
    }

    public void setTotalNPS(String totalNPS) {
        this.totalNPS = totalNPS;
    }

    public String getTotalUnsold() {
        return totalUnsold;
    }

    public void setTotalUnsold(String totalUnsold) {
        this.totalUnsold = totalUnsold;
    }

    public String getVbeln() {
        return vbeln;
    }

    public void setVbeln(String vbeln) {
        this.vbeln = vbeln;
    }

    public String getCity_UPC() {
        return City_UPC;
    }

    public void setCity_UPC(String city_UPC) {
        City_UPC = city_UPC;
    }

    public String getExecutive_Code() {
        return Executive_Code;
    }

    public void setExecutive_Code(String executive_Code) {
        Executive_Code = executive_Code;
    }

    public String getPosnr() {
        return posnr;
    }

    public void setPosnr(String posnr) {
        this.posnr = posnr;
    }

    public String getCash_Credit() {
        return Cash_Credit;
    }

    public void setCash_Credit(String cash_Credit) {
        Cash_Credit = cash_Credit;
    }

    public String getOrd_Date() {
        return Ord_Date;
    }

    public void setOrd_Date(String ord_Date) {
        Ord_Date = ord_Date;
    }

    public String getFresh_unsold() {
        return fresh_unsold;
    }

    public void setFresh_unsold(String fresh_unsold) {
        this.fresh_unsold = fresh_unsold;
    }

    public String getOld_unsold() {
        return old_unsold;
    }

    public void setOld_unsold(String old_unsold) {
        this.old_unsold = old_unsold;
    }

    public String getPstyv() {
        return pstyv;
    }

    public void setPstyv(String pstyv) {
        this.pstyv = pstyv;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgency_name() {
        return agency_name;
    }

    public void setAgency_name(String agency_name) {
        this.agency_name = agency_name;
    }

    public String getAgency_code() {
        return agency_code;
    }

    public void setAgency_code(String agency_code) {
        this.agency_code = agency_code;
    }

    public String getEdition() {
        return edition;
    }

    public void setEdition(String edition) {
        this.edition = edition;
    }

    public String getPo() {
        return po;
    }

    public void setPo(String po) {
        this.po = po;
    }

    public String getUnsold() {
        return unsold;
    }

    public void setUnsold(String unsold) {
        this.unsold = unsold;
    }

    public String getNps() {
        return nps;
    }

    public void setNps(String nps) {
        this.nps = nps;
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

    public String getExecutive_name() {
        return executive_name;
    }

    public void setExecutive_name(String executive_name) {
        this.executive_name = executive_name;
    }

    public String getMainJJ() {
        return MainJJ;
    }

    public void setMainJJ(String mainJJ) {
        MainJJ = mainJJ;
    }

    public String getEditStatus() {
        return editStatus;
    }

    public void setEditStatus(String editStatus) {
        this.editStatus = editStatus;
    }
}
