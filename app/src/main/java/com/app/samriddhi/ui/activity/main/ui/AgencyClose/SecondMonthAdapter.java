package com.app.samriddhi.ui.activity.main.ui.AgencyClose;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;

import java.util.List;

public class SecondMonthAdapter extends RecyclerView.Adapter<SecondMonthAdapter.MyViewHolder>{

    private final List<SecondMonthModal> requestDetailModals;
    Context context;
    int row_index = -1;

    public SecondMonthAdapter(List<SecondMonthModal> requestDetailModals, Context context) {
        this.requestDetailModals = requestDetailModals;
        this.context = context;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = null;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.copies_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder Viewholder, @SuppressLint("RecyclerView") int position) {
        SecondMonthModal listData = requestDetailModals.get(position);



        if(listData.getDate().contains("-")){
            String[] strSplit =listData.getDate().split("-");
            Viewholder.txtDay.setText(strSplit[2]);
            Viewholder.txtCopies.setText(listData.getCoppies());
        }
        else {
            Viewholder.txtDay.setText(listData.getDate());
            Viewholder.txtCopies.setText(listData.getCoppies());
        }
    }

    @Override
    public int getItemCount() {
        return requestDetailModals.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtDay,txtCopies,txtMonthName;

        MyViewHolder(View view) {
            super(view);

            txtDay = view.findViewById(R.id.txtDay);
            txtCopies = view.findViewById(R.id.txtCopies);
        }
    }


}
