package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AgentChildResultListModel extends BaseModel {
    @SerializedName("Type")
    @Expose
    private String type;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("ParentFlag")
    @Expose
    private String parentFlag;
    @SerializedName("Partner")
    @Expose
    private String partner;
    @SerializedName("Parent")
    @Expose
    private String parent;
    @SerializedName("Mobile")
    @Expose
    private String mobile;
    @SerializedName("ReqsentFlag")
    @Expose
    private String reqsentFlag;
    @SerializedName("Name1")
    @Expose
    private String name1;
    @SerializedName("Name2")
    @Expose
    private String name2;
    @SerializedName("ActiveFlag")
    @Expose
    private String activeFlag;
    @SerializedName("Deviceid")
    @Expose
    private String deviceid;

    public AgentChildResultListModel(String type, String email, String partner, String mobile, String name1) {
        this.type = type;
        this.email = email;
        this.partner = partner;
        this.mobile = mobile;
        this.name1 = name1;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getParentFlag() {
        return parentFlag;
    }

    public void setParentFlag(String parentFlag) {
        this.parentFlag = parentFlag;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getReqsentFlag() {
        return reqsentFlag;
    }

    public void setReqsentFlag(String reqsentFlag) {
        this.reqsentFlag = reqsentFlag;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getActiveFlag() {
        return activeFlag;
    }

    public void setActiveFlag(String activeFlag) {
        this.activeFlag = activeFlag;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }
}
