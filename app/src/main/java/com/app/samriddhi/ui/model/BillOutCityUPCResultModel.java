package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BillOutCityUPCResultModel extends BaseModel {
    @SerializedName("results")
    @Expose
    private List<BillOutCityUPCListModel> results = null;

    public List<BillOutCityUPCListModel> getResults() {
        return results;
    }

    public void setResults(List<BillOutCityUPCListModel> results) {
        this.results = results;
    }


}
