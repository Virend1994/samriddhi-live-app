package com.app.samriddhi.ui.presenter;

import android.util.Log;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.response.JsonObjectResponse;
import com.app.samriddhi.base.BasePresenter;
import com.app.samriddhi.ui.model.AgentResultModel;
import com.app.samriddhi.ui.model.BillOutAgentResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCDistWiseResultModel;
import com.app.samriddhi.ui.model.BillOutCityUPCResultModel;
import com.app.samriddhi.ui.model.BillingOutstandingResultModel;
import com.app.samriddhi.ui.model.CashCreditResultModel;
import com.app.samriddhi.ui.model.CityResultModel;
import com.app.samriddhi.ui.model.LogoutReqModel;
import com.app.samriddhi.ui.model.LogoutResultModel;
import com.app.samriddhi.ui.model.SalesDistrictWiseResultModel;
import com.app.samriddhi.ui.model.SalesOrgCityResultModel;
import com.app.samriddhi.ui.model.StateResultModel;
import com.app.samriddhi.ui.model.UsersResultModel;
import com.app.samriddhi.ui.view.IUserLogoutView;
import com.app.samriddhi.ui.view.IUserTypeCopiesView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserLogoutPresenter extends BasePresenter<IUserLogoutView> {


    public void getLogoutStatus(LogoutReqModel logoutReqModel) {
        getView().enableLoadingBar(true);

        SamriddhiApplication.getmInstance().getApiService().getLogoutStatus(logoutReqModel).enqueue(new Callback<LogoutResultModel>() {
            @Override
            public void onResponse(Call<LogoutResultModel> call, Response<LogoutResultModel> response) {
                getView().enableLoadingBar(false);


                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body() != null)
                            getView().onLogoutSuccess(response.body());

                    }

            }

            @Override
            public void onFailure(Call<LogoutResultModel> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }


}
