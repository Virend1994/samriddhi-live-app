package com.app.samriddhi.ui.activity.main.ui.Ledger;

public class LedgerModal {

    /* For states */
    public String state_id;
    public String state_name;
    public String state_amt;
    public String serial_num;
    public String state_code;

    /*For cities*/
    public String city_id;
    public String city_name;
    public String city_amt;
    public String city_code;
    public String cityupc;
    public String market;
    public String market_code;


    /* Agenct list */

    public String agent_code;
    public String agent_name;
    public String agent_amt;


    public String getMarket() {
        return market;
    }

    public void setMarket(String market) {
        this.market = market;
    }

    public String getMarket_code() {
        return market_code;
    }

    public void setMarket_code(String market_code) {
        this.market_code = market_code;
    }

    public String getCity_code() {
        return city_code;
    }

    public void setCity_code(String city_code) {
        this.city_code = city_code;
    }

    public String getState_code() {
        return state_code;
    }

    public void setState_code(String state_code) {
        this.state_code = state_code;
    }

    public String getSerial_num() {
        return serial_num;
    }

    public void setSerial_num(String serial_num) {
        this.serial_num = serial_num;
    }

    public String getAgent_code() {
        return agent_code;
    }

    public void setAgent_code(String agent_code) {
        this.agent_code = agent_code;
    }

    public String getAgent_name() {
        return agent_name;
    }

    public void setAgent_name(String agent_name) {
        this.agent_name = agent_name;
    }

    public String getAgent_amt() {
        return agent_amt;
    }

    public void setAgent_amt(String agent_amt) {
        this.agent_amt = agent_amt;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getCity_name() {
        return city_name;
    }

    public void setCity_name(String city_name) {
        this.city_name = city_name;
    }

    public String getCity_amt() {
        return city_amt;
    }

    public void setCity_amt(String city_amt) {
        this.city_amt = city_amt;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getState_name() {
        return state_name;
    }

    public void setState_name(String state_name) {
        this.state_name = state_name;
    }

    public String getState_amt() {
        return state_amt;
    }

    public void setState_amt(String state_amt) {
        this.state_amt = state_amt;
    }


    public String getCityupc() {
        return cityupc;
    }

    public void setCityupc(String cityupc) {
        this.cityupc = cityupc;
    }
}
