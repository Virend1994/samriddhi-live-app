package com.app.samriddhi.ui.model;

import java.io.Serializable;

public class NewAgencyModel implements Serializable {

    public String id;
    public String state;
    public String state_code;
    public String unit;
    public String city_upc;
    public String district;
    public String town;
    public String location;
    public String pincode;
    public String population;
    public String noMainCopy;
    public String salesOffice;
    public String saleCluster;
    public String main;
    public String janJagrati;
    public String insertDate;
    public String status;
    public String UOHApproval,SOHApproval,HOApproval,UHFApproval,CBRApproval,BPClosure;
    public String MainCirStatus,JJStatus,SurveyStatus,KybpStatus,OTPStatus;


    public String getMainCirStatus() {
        return MainCirStatus;
    }

    public void setMainCirStatus(String mainCirStatus) {
        MainCirStatus = mainCirStatus;
    }

    public String getJJStatus() {
        return JJStatus;
    }

    public void setJJStatus(String JJStatus) {
        this.JJStatus = JJStatus;
    }

    public String getSurveyStatus() {
        return SurveyStatus;
    }

    public void setSurveyStatus(String surveyStatus) {
        SurveyStatus = surveyStatus;
    }

    public String getKybpStatus() {
        return KybpStatus;
    }

    public void setKybpStatus(String kybpStatus) {
        KybpStatus = kybpStatus;
    }

    public String getOTPStatus() {
        return OTPStatus;
    }

    public void setOTPStatus(String OTPStatus) {
        this.OTPStatus = OTPStatus;
    }

    public String getState_code() {
        return state_code;
    }

    public void setState_code(String state_code) {
        this.state_code = state_code;
    }

    public String reason;
    public String reasonArray;

    public String unitId;
    public String state_id;

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getUOHApproval() {

        return UOHApproval;
    }

    public String getUHFApproval() {
        return UHFApproval;
    }

    public void setUHFApproval(String UHFApproval) {
        this.UHFApproval = UHFApproval;
    }

    public String getCBRApproval() {
        return CBRApproval;
    }

    public void setCBRApproval(String CBRApproval) {
        this.CBRApproval = CBRApproval;
    }

    public String getBPClosure() {
        return BPClosure;
    }

    public void setBPClosure(String BPClosure) {
        this.BPClosure = BPClosure;
    }

    public void setUOHApproval(String UOHApproval) {
        this.UOHApproval = UOHApproval;
    }

    public String getSOHApproval() {
        return SOHApproval;
    }

    public void setSOHApproval(String SOHApproval) {
        this.SOHApproval = SOHApproval;
    }

    public String getHOApproval() {
        return HOApproval;
    }

    public void setHOApproval(String HOApproval) {
        this.HOApproval = HOApproval;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String SalesOfficeNameJJ,SalesOfficeNameMain,SalesDistName;

    public String getSalesOfficeNameJJ() {
        return SalesOfficeNameJJ;
    }

    public void setSalesOfficeNameJJ(String salesOfficeNameJJ) {
        SalesOfficeNameJJ = salesOfficeNameJJ;
    }

    public String getSalesOfficeNameMain() {
        return SalesOfficeNameMain;
    }

    public void setSalesOfficeNameMain(String salesOfficeNameMain) {
        SalesOfficeNameMain = salesOfficeNameMain;
    }

    public String getSalesDistName() {
        return SalesDistName;
    }

    public void setSalesDistName(String salesDistName) {
        SalesDistName = salesDistName;
    }

    public String getReasonArray() {
        return reasonArray;
    }

    public void setReasonArray(String reasonArray) {
        this.reasonArray = reasonArray;
    }

    public String getCity_upc() {
        return city_upc;
    }

    public void setCity_upc(String city_upc) {
        this.city_upc = city_upc;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    public String getNoMainCopy() {
        return noMainCopy;
    }

    public void setNoMainCopy(String noMainCopy) {
        this.noMainCopy = noMainCopy;
    }

    public String getSalesOffice() {
        return salesOffice;
    }

    public void setSalesOffice(String salesOffice) {
        this.salesOffice = salesOffice;
    }

    public String getSaleCluster() {
        return saleCluster;
    }

    public void setSaleCluster(String saleCluster) {
        this.saleCluster = saleCluster;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getJanJagrati() {
        return janJagrati;
    }

    public void setJanJagrati(String janJagrati) {
        this.janJagrati = janJagrati;
    }

    public String getInsertDate() {
        return insertDate;
    }

    public void setInsertDate(String insertDate) {
        this.insertDate = insertDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }


}
