package com.app.samriddhi.ui.model;

import android.content.Context;
import android.view.View;

import com.app.samriddhi.base.model.BaseModel;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.SystemUtility;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AgentListModel extends BaseModel {

    @SerializedName("UnitName")
    @Expose
    private String UnitName;


    @SerializedName("UnitCode")
    @Expose
    private String UnitCode;



    @SerializedName("SalesDist")
    @Expose
    private String SalesDist;



    @SerializedName("SalesCode")
    @Expose
    private String SalesCode;

    @SerializedName("SoldToParty")
    @Expose
    private String SoldToParty;


    @SerializedName("SoldToPartyName")
    @Expose
    private String SoldToPartyName;


    @SerializedName("Market")
    @Expose
    private String Market;



    @SerializedName("CityUPC")
    @Expose
    private String CityUPC;



    @SerializedName("YesterdayPO")
    @Expose
    private String YesterdayPO;



    @SerializedName("YesterdayNPS")
    @Expose
    private String YesterdayNPS;



    @SerializedName("TodayPO")
    @Expose
    private String TodayPO;



    @SerializedName("TodayNPS")
    @Expose
    private String TodayNPS;




    @SerializedName("PODiff")
    @Expose
    private String PODiff;


    @SerializedName("NPSDiff")
    @Expose
    private String NPSDiff;

    public String getUnitName() {
        return UnitName;
    }

    public void setUnitName(String unitName) {
        UnitName = unitName;
    }

    public String getUnitCode() {
        return UnitCode;
    }

    public void setUnitCode(String unitCode) {
        UnitCode = unitCode;
    }

    public String getSalesDist() {
        return SalesDist;
    }

    public void setSalesDist(String salesDist) {
        SalesDist = salesDist;
    }

    public String getSalesCode() {
        return SalesCode;
    }

    public void setSalesCode(String salesCode) {
        SalesCode = salesCode;
    }

    public String getSoldToParty() {
        return SoldToParty;
    }

    public void setSoldToParty(String soldToParty) {
        SoldToParty = soldToParty;
    }

    public String getSoldToPartyName() {
        return SoldToPartyName;
    }

    public void setSoldToPartyName(String soldToPartyName) {
        SoldToPartyName = soldToPartyName;
    }

    public String getMarket() {
        return Market;
    }

    public void setMarket(String market) {
        Market = market;
    }

    public String getCityUPC() {
        return CityUPC;
    }

    public void setCityUPC(String cityUPC) {
        CityUPC = cityUPC;
    }

    public String getYesterdayPO() {
        return YesterdayPO;
    }

    public void setYesterdayPO(String yesterdayPO) {
        YesterdayPO = yesterdayPO;
    }

    public String getYesterdayNPS() {
        return YesterdayNPS;
    }

    public void setYesterdayNPS(String yesterdayNPS) {
        YesterdayNPS = yesterdayNPS;
    }

    public String getTodayPO() {
        return TodayPO;
    }

    public void setTodayPO(String todayPO) {
        TodayPO = todayPO;
    }

    public String getTodayNPS() {
        return TodayNPS;
    }

    public void setTodayNPS(String todayNPS) {
        TodayNPS = todayNPS;
    }

    public String getPODiff() {
        return PODiff;
    }

    public void setPODiff(String PODiff) {
        this.PODiff = PODiff;
    }

    public String getNPSDiff() {
        return NPSDiff;
    }

    public void setNPSDiff(String NPSDiff) {
        this.NPSDiff = NPSDiff;
    }
}
