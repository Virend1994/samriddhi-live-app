package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class YearDataModel extends BaseModel {
    @SerializedName("YTDdata")
    @Expose
    private List<YearListModel> yTDdata = null;
    @SerializedName("todate")
    @Expose
    private String todate;
    @SerializedName("frmdate")
    @Expose
    private String frmdate;
    @SerializedName("reply")
    @Expose
    private String reply;

    public List<YearListModel> getYTDdata() {
        return yTDdata;
    }

    public void setYTDdata(List<YearListModel> yTDdata) {
        this.yTDdata = yTDdata;
    }

    public String getTodate() {
        return todate;
    }

    public void setTodate(String todate) {
        this.todate = todate;
    }

    public String getFrmdate() {
        return frmdate;
    }

    public void setFrmdate(String frmdate) {
        this.frmdate = frmdate;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }
}
