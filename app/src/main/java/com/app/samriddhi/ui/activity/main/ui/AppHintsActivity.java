package com.app.samriddhi.ui.activity.main.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.RecyclerViewArrayAdapter;
import com.app.samriddhi.databinding.ActivityAppHintsBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.model.HintModel;
import com.app.samriddhi.ui.presenter.AppHintsPresenter;
import com.app.samriddhi.ui.view.IAppHintView;

import java.util.List;

public class AppHintsActivity extends BaseActivity implements IAppHintView {

    ActivityAppHintsBinding mBining;
    AppHintsPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBining = DataBindingUtil.setContentView(this, R.layout.activity_app_hints);
        initializeView();
    }

    /**
     * initialize views
     */
    private void initializeView() {
        mPresenter = new AppHintsPresenter();
        mPresenter.setView(this);
        mBining.recycleHints.setLayoutManager(new LinearLayoutManager(this));
        mPresenter.getAppHints(PreferenceManager.getAppLang(this).toUpperCase());
        mBining.toolbarHint.rlNotification.setVisibility(View.INVISIBLE);
        mBining.toolbarHint.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mBining.toolbarHint.txtTittle.setText(getResources().getString(R.string.str_hint_tittle));
    }

    @Override
    public void onSuccess(List<HintModel> mData) {
        if (mData != null && mData.size() > 0) {
            mBining.recycleHints.setAdapter(new RecyclerViewArrayAdapter(mData));
        }

    }

    @Override
    public Context getContext() {
        return this;
    }
}
