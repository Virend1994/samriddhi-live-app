package com.app.samriddhi.ui.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CIR_Order_Update implements Serializable {

    @SerializedName("vbeln")
    @Expose
    private String vbeln;
    @SerializedName("CIR_Order_Update_id")
    @Expose
    private String CIR_Order_Update_id;
    @SerializedName("posnr")
    @Expose
    private String posnr;
    @SerializedName("gjahr")
    @Expose
    private String gjahr;
    @SerializedName("monat")
    @Expose
    private String monat;
    @SerializedName("vkorg")
    @Expose
    private String vkorg;
    @SerializedName("vtweg")
    @Expose
    private String vtweg;
    @SerializedName("spart")
    @Expose
    private String spart;
    @SerializedName("pstyv")
    @Expose
    private String pstyv;
    @SerializedName("sold_to_party")
    @Expose
    private String soldToParty;
    @SerializedName("ship_to_party")
    @Expose
    private String shipToParty;
    @SerializedName("ord_date")
    @Expose
    private String ordDate;
    @SerializedName("vgbel")
    @Expose
    private String vgbel;
    @SerializedName("bezei")
    @Expose
    private String bezei;
    @SerializedName("drerz")
    @Expose
    private String drerz;
    @SerializedName("pva")
    @Expose
    private String pva;
    @SerializedName("matnr")
    @Expose
    private String matnr;
    @SerializedName("pub_name")
    @Expose
    private String pubName;
    @SerializedName("soff_name")
    @Expose
    private String soffName;
    @SerializedName("cg_name")
    @Expose
    private String cgName;
    @SerializedName("sdist_name")
    @Expose
    private String sdistName;
    @SerializedName("edition_name")
    @Expose
    private String editionName;
    @SerializedName("cust_name")
    @Expose
    private String custName;
    @SerializedName("city_name")
    @Expose
    private String cityName;
    @SerializedName("vkgrp")
    @Expose
    private String vkgrp;
    @SerializedName("sgrp_name")
    @Expose
    private String sgrpName;
    @SerializedName("vkbur")
    @Expose
    private String vkbur;
    @SerializedName("kdgrp")
    @Expose
    private String kdgrp;
    @SerializedName("bzirk")
    @Expose
    private String bzirk;
    @SerializedName("route")
    @Expose
    private String route;
    @SerializedName("ord_date_f")
    @Expose
    private String ordDateF;
    @SerializedName("proposed_qty")
    @Expose
    private Integer proposedQty;
    @SerializedName("sale_qty")
    @Expose
    private Integer saleQty;
    @SerializedName("sampling_qty")
    @Expose
    private Integer samplingQty;
    @SerializedName("reason_qty_change")
    @Expose
    private String reasonQtyChange;
    @SerializedName("per_inc_dec")
    @Expose
    private Integer perIncDec;
    @SerializedName("copies_inc_dec")
    @Expose
    private Integer copiesIncDec;
    @SerializedName("remark")
    @Expose
    private String remark;
    @SerializedName("is_approval")
    @Expose
    private Integer isApproval;
    @SerializedName("base_copy")
    @Expose
    private Integer baseCopy;
    @SerializedName("lower_limit")
    @Expose
    private Integer lowerLimit;
    @SerializedName("uper_limit")
    @Expose
    private Integer uperLimit;
    @SerializedName("is_hold")
    @Expose
    private Integer isHold;
    @SerializedName("hold_date_from")
    @Expose
    private Object holdDateFrom;

    @SerializedName("hold_date_to")
    @Expose
    private Object holdDateTo;

    @SerializedName("approval_status")
    @Expose
    private int approval_status;


    @SerializedName("approved_by")
    @Expose
    private Object approved_by;

    public String getCIR_Order_Update_id() {
        return CIR_Order_Update_id;
    }

    public void setCIR_Order_Update_id(String CIR_Order_Update_id) {
        this.CIR_Order_Update_id = CIR_Order_Update_id;
    }

    public int getApproval_status() {
        return approval_status;
    }

    public void setApproval_status(int approval_status) {
        this.approval_status = approval_status;
    }

    public Object getApproved_by() {
        return approved_by;
    }

    public void setApproved_by(Object approved_by) {
        this.approved_by = approved_by;
    }


    public String getVbeln() {
        return vbeln;
    }

    public void setVbeln(String vbeln) {
        this.vbeln = vbeln;
    }

    public String getPosnr() {
        return posnr;
    }

    public void setPosnr(String posnr) {
        this.posnr = posnr;
    }

    public String getGjahr() {
        return gjahr;
    }

    public void setGjahr(String gjahr) {
        this.gjahr = gjahr;
    }

    public String getMonat() {
        return monat;
    }

    public void setMonat(String monat) {
        this.monat = monat;
    }

    public String getVkorg() {
        return vkorg;
    }

    public void setVkorg(String vkorg) {
        this.vkorg = vkorg;
    }

    public String getVtweg() {
        return vtweg;
    }

    public void setVtweg(String vtweg) {
        this.vtweg = vtweg;
    }

    public String getSpart() {
        return spart;
    }

    public void setSpart(String spart) {
        this.spart = spart;
    }

    public String getPstyv() {
        return pstyv;
    }

    public void setPstyv(String pstyv) {
        this.pstyv = pstyv;
    }

    public String getSoldToParty() {
        return soldToParty;
    }

    public void setSoldToParty(String soldToParty) {
        this.soldToParty = soldToParty;
    }

    public String getShipToParty() {
        return shipToParty;
    }

    public void setShipToParty(String shipToParty) {
        this.shipToParty = shipToParty;
    }

    public String getOrdDate() {
        return ordDate;
    }

    public void setOrdDate(String ordDate) {
        this.ordDate = ordDate;
    }

    public String getVgbel() {
        return vgbel;
    }

    public void setVgbel(String vgbel) {
        this.vgbel = vgbel;
    }

    public String getBezei() {
        return bezei;
    }

    public void setBezei(String bezei) {
        this.bezei = bezei;
    }

    public String getDrerz() {
        return drerz;
    }

    public void setDrerz(String drerz) {
        this.drerz = drerz;
    }

    public String getPva() {
        return pva;
    }

    public void setPva(String pva) {
        this.pva = pva;
    }

    public String getMatnr() {
        return matnr;
    }

    public void setMatnr(String matnr) {
        this.matnr = matnr;
    }

    public String getPubName() {
        return pubName;
    }

    public void setPubName(String pubName) {
        this.pubName = pubName;
    }

    public String getSoffName() {
        return soffName;
    }

    public void setSoffName(String soffName) {
        this.soffName = soffName;
    }

    public String getCgName() {
        return cgName;
    }

    public void setCgName(String cgName) {
        this.cgName = cgName;
    }

    public String getSdistName() {
        return sdistName;
    }

    public void setSdistName(String sdistName) {
        this.sdistName = sdistName;
    }

    public String getEditionName() {
        return editionName;
    }

    public void setEditionName(String editionName) {
        this.editionName = editionName;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getVkgrp() {
        return vkgrp;
    }

    public void setVkgrp(String vkgrp) {
        this.vkgrp = vkgrp;
    }

    public String getSgrpName() {
        return sgrpName;
    }

    public void setSgrpName(String sgrpName) {
        this.sgrpName = sgrpName;
    }

    public String getVkbur() {
        return vkbur;
    }

    public void setVkbur(String vkbur) {
        this.vkbur = vkbur;
    }

    public String getKdgrp() {
        return kdgrp;
    }

    public void setKdgrp(String kdgrp) {
        this.kdgrp = kdgrp;
    }

    public String getBzirk() {
        return bzirk;
    }

    public void setBzirk(String bzirk) {
        this.bzirk = bzirk;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getOrdDateF() {
        return ordDateF;
    }

    public void setOrdDateF(String ordDateF) {
        this.ordDateF = ordDateF;
    }

    public Integer getProposedQty() {
        return proposedQty;
    }

    public void setProposedQty(Integer proposedQty) {
        this.proposedQty = proposedQty;
    }

    public Integer getSaleQty() {
        return saleQty;
    }

    public void setSaleQty(Integer saleQty) {
        this.saleQty = saleQty;
    }

    public Integer getSamplingQty() {
        return samplingQty;
    }

    public void setSamplingQty(Integer samplingQty) {
        this.samplingQty = samplingQty;
    }

    public String getReasonQtyChange() {
        return reasonQtyChange;
    }

    public void setReasonQtyChange(String reasonQtyChange) {
        this.reasonQtyChange = reasonQtyChange;
    }

    public Integer getPerIncDec() {
        return perIncDec;
    }

    public void setPerIncDec(Integer perIncDec) {
        this.perIncDec = perIncDec;
    }

    public Integer getCopiesIncDec() {
        return copiesIncDec;
    }

    public void setCopiesIncDec(Integer copiesIncDec) {
        this.copiesIncDec = copiesIncDec;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getIsApproval() {
        return isApproval;
    }

    public void setIsApproval(Integer isApproval) {
        this.isApproval = isApproval;
    }

    public Integer getBaseCopy() {
        return baseCopy;
    }

    public void setBaseCopy(Integer baseCopy) {
        this.baseCopy = baseCopy;
    }

    public Integer getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(Integer lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public Integer getUperLimit() {
        return uperLimit;
    }

    public void setUperLimit(Integer uperLimit) {
        this.uperLimit = uperLimit;
    }

    public Integer getIsHold() {
        return isHold;
    }

    public void setIsHold(Integer isHold) {
        this.isHold = isHold;
    }

    public Object getHoldDateFrom() {
        return holdDateFrom;
    }

    public void setHoldDateFrom(Object holdDateFrom) {
        this.holdDateFrom = holdDateFrom;
    }

    public Object getHoldDateTo() {
        return holdDateTo;
    }

    public void setHoldDateTo(Object holdDateTo) {
        this.holdDateTo = holdDateTo;
    }
}
