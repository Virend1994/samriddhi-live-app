package com.app.samriddhi.ui.activity.main.ui.Ledger;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;
import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AppsContants;
import com.app.samriddhi.ui.model.AgentListModel;
import com.google.android.material.card.MaterialCardView;

import java.util.List;

public class AgentDetailAdapter extends RecyclerView.Adapter<AgentDetailAdapter.ViewHolder> {
    Context context;
    List<AgentDetailModal> dataAdapters;

    public AgentDetailAdapter(List<AgentDetailModal> getDataAdapter, Context context) {
        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.agent_detail_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, @SuppressLint("RecyclerView") int position) {

        AgentDetailModal dataAdapterOBJ = dataAdapters.get(position);
        Viewholder.setIsRecyclable(false);
        Viewholder.tvTranDate.setText(dataAdapterOBJ.getTran_date());
        Viewholder.tvTranType.setText(dataAdapterOBJ.getTran_type());
        Viewholder.tvTranAmt.setText(dataAdapterOBJ.getTran_amt());
        Viewholder.tvDescription.setText(dataAdapterOBJ.getTran_reason());

        /*Viewholder.cardViewMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof CityActivity) {

                    AppsContants.sharedpreferences=context.getSharedPreferences(AppsContants.MyPREFERENCES,Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor=AppsContants.sharedpreferences.edit();
                    editor.putString(AppsContants.unitCode,dataAdapterOBJ.getCity_code());
                    editor.commit();
                    ((CityActivity) context).startActivityAnimation(context, ShowAgentListActivity.class, false);
                }
            }
        });*/
    }

    @Override
    public int getItemCount() {
        return dataAdapters.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTranDate, tvTranType, tvTranAmt,tvDescription;
        MaterialCardView cardViewMain;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTranDate = itemView.findViewById(R.id.tvTranDate);
            tvTranType = itemView.findViewById(R.id.tvTranType);
            tvTranAmt = itemView.findViewById(R.id.tvTranAmt);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            //cardViewMain = itemView.findViewById(R.id.cardViewMain);
        }
    }
}