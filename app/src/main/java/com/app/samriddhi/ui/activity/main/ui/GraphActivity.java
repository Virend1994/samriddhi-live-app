package com.app.samriddhi.ui.activity.main.ui;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.databinding.ActivityGraphBinding;
import com.app.samriddhi.ui.fragment.BillingGraphFragment;
import com.app.samriddhi.ui.fragment.NoOfCopyGraphFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class GraphActivity extends BaseActivity  {
    private ActivityGraphBinding binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         binding = DataBindingUtil.setContentView(this, R.layout.activity_graph);
        binding.toolbarGraph.txtTittle.setText(R.string.str_graph);
         binding.toolbarGraph.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
         binding.toolbarGraph.rlNotification.setVisibility(View.GONE);


        binding.bottomNavigation.setOnNavigationItemSelectedListener (new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_no_of_copy:
                        loadDefaultFrag();
                        break;
                    case R.id.navigation_billing:
                        Fragment fragment1 = new BillingGraphFragment();
                        FragmentTransaction transaction1 = getFragmentManager().beginTransaction();
                        transaction1.replace(R.id.main_container, fragment1); // fragment container id in first parameter is the  container(Main layout id) of Activity
                        transaction1.addToBackStack(null);  // this will manage backstack
                        transaction1.commit();
                        break;



                }
                return true;
            }
        });



    }

    private void loadDefaultFrag() {
        Fragment fragment = new NoOfCopyGraphFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.main_container, fragment); // fragment container id in first parameter is the  container(Main layout id) of Activity
        transaction.addToBackStack(null);  // this will manage backstack
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();



    }

    @Override
    protected void onResume() {
        super.onResume();
        loadDefaultFrag();

    }}
