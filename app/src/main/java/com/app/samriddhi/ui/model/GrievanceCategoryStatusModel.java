package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GrievanceCategoryStatusModel extends BaseModel {
    @SerializedName("IncidentStatus_Srno")
    @Expose
    private Integer incidentStatusSrno;
    @SerializedName("IncidentStatus_Text")
    @Expose
    private String incidentStatusText;
    @SerializedName("IncidentStatus_Status")
    @Expose
    private Integer incidentStatusStatus;

    public Integer getIncidentStatusSrno() {
        return incidentStatusSrno;
    }

    public void setIncidentStatusSrno(Integer incidentStatusSrno) {
        this.incidentStatusSrno = incidentStatusSrno;
    }

    public String getIncidentStatusText() {
        return incidentStatusText;
    }

    public void setIncidentStatusText(String incidentStatusText) {
        this.incidentStatusText = incidentStatusText;
    }

    public Integer getIncidentStatusStatus() {
        return incidentStatusStatus;
    }

    public void setIncidentStatusStatus(Integer incidentStatusStatus) {
        this.incidentStatusStatus = incidentStatusStatus;
    }

}
