package com.app.samriddhi.ui.activity.main.ui.OrderSupply.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.DailyPoActivity;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.POModal.DailyPOModal;
import com.app.samriddhi.ui.model.DropDownModel;
import com.google.android.material.card.MaterialCardView;

import java.util.ArrayList;
import java.util.List;

public class PoAdapter extends RecyclerView.Adapter<PoAdapter.ViewHolder> implements Filterable {

    Context context;
    List<DailyPOModal> dataAdapters;
    private final List<DailyPOModal> searchArray;
    private final List<String> rName;
    private final List<String> rId;
    int total = 0;
    int row_index = -1;
    int baseMin = 0;
    int baseMax = 0;
    int baseCopy = 0;

    public PoAdapter(List<DailyPOModal> getDataAdapter, Context context, List<String> reasonName, List<String> reasonId) {

        super();
        this.dataAdapters = getDataAdapter;
        this.rName = reasonName;
        this.rId = reasonId;
        this.searchArray = new ArrayList<>();
        this.searchArray.addAll(dataAdapters);
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.po_items_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, @SuppressLint("RecyclerView") int position) {

        DailyPOModal dataAdapterOBJ = dataAdapters.get(position);
        Viewholder.setIsRecyclable(false);

        Viewholder.txtMainEditionName.setText(dataAdapterOBJ.getCust_name() + " " + "(" + dataAdapterOBJ.getSold_to_party() + ")");
        Viewholder.txtCityname.setText(dataAdapterOBJ.getCity_name());
        Viewholder.tvEdtion.setText(dataAdapterOBJ.getEdi_name());
        Viewholder.tvCopyType.setText(dataAdapterOBJ.getCopies_type());
        Viewholder.tvSoldToParty.setText(dataAdapterOBJ.getSold_to_party());
        Viewholder.tvShipToParty.setText(dataAdapterOBJ.getShip_to_party());
        Viewholder.tvItemCat.setText(dataAdapterOBJ.getItem_cat());
        Viewholder.editSaleQTD.setText(dataAdapterOBJ.getProposed_qty());
        Viewholder.tvINCDEC.setText(dataAdapterOBJ.getInc_dsc());
        Viewholder.tvRefQtd.setText(dataAdapterOBJ.getRef_qtd());
        Viewholder.editRemark.setText(dataAdapterOBJ.getRemark());
        Viewholder.txtShipToParty.setText("Ship to party" + " " + dataAdapterOBJ.getShip_to_party());


        Viewholder.cardViewMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Viewholder.btnSaveSaleQty.setVisibility(View.GONE);
                DailyPOModal dataAdapterOBJ = dataAdapters.get(position);
                Log.e("Afasaasfas", dataAdapterOBJ.getProposed_qty());
                Viewholder.editSaleQTD.setText(dataAdapterOBJ.getProposed_qty());

                row_index = position;
                notifyDataSetChanged();
            }
        });

        if (row_index == position) {


            Viewholder.linDetail.setVisibility(View.VISIBLE);

        } else {
            Viewholder.linDetail.setVisibility(View.GONE);
        }


        if (dataAdapterOBJ.getItem_cat().equals("ZFOR")) {

            Viewholder.editSaleQTD.setEnabled(false);
            Viewholder.btnSaveSaleQty.setVisibility(View.GONE);
            Viewholder.editRemark.setEnabled(false);

            Viewholder.editSaleQTD.setText(dataAdapterOBJ.getSale_qtd());

            Viewholder.editRemark.setBackground(context.getResources().getDrawable(R.drawable.edit_not_editable));
            Viewholder.editSaleQTD.setBackground(context.getResources().getDrawable(R.drawable.edit_not_editable));

            Viewholder.spinReason.setBackground(context.getResources().getDrawable(R.drawable.edit_not_editable));
            Viewholder.tvINCDEC.setText("0");
            dataAdapters.get(position).setIs_approval("1");
            dataAdapters.get(position).setRemark("");


        } else {

            Viewholder.editSaleQTD.setEnabled(true);
            Viewholder.editRemark.setEnabled(true);
          //  Viewholder.btnSaveSaleQty.setVisibility(View.VISIBLE);



            Viewholder.editRemark.setBackground(context.getResources().getDrawable(R.drawable.border));
            Viewholder.editSaleQTD.setBackground(context.getResources().getDrawable(R.drawable.border));
            Viewholder.spinReason.setBackground(context.getResources().getDrawable(R.drawable.border));

            if (dataAdapterOBJ.getBase_min() != null && !dataAdapterOBJ.getBase_min().isEmpty() && !dataAdapterOBJ.getBase_min().equals("null")) {
                baseMin = Integer.parseInt(dataAdapterOBJ.getBase_min());
                baseMax = Integer.parseInt(dataAdapterOBJ.getBase_max());
                baseCopy = Integer.parseInt(dataAdapterOBJ.getBase_copy());
            }

            if (!dataAdapterOBJ.getInc_dsc().equals("0")) {

                if (dataAdapterOBJ.getQuantityPerStatus().equals("1")) {



                    Viewholder.editSaleQTD.setEnabled(true);
                    int actualCopies = Integer.parseInt(dataAdapterOBJ.getSale_qtd());
                    int actualIncreasedCopies = Integer.parseInt(dataAdapterOBJ.getInc_dsc());
                    total = actualCopies + actualIncreasedCopies;
                    Viewholder.editSaleQTD.setText(total + "");
                    dataAdapters.get(position).setProposed_qty(total + "");

                    if (dataAdapterOBJ.getRef_qtd() != null && !dataAdapterOBJ.getRef_qtd().isEmpty() && !dataAdapterOBJ.getRef_qtd().equals("null")) {

                        int getTotal = Integer.parseInt(dataAdapterOBJ.getInc_dsc());
                        int refQTD = 0;
                        int totalINC = 0;

                        refQTD = Integer.parseInt(dataAdapterOBJ.getRef_qtd());
                        totalINC = getTotal - refQTD;
                        Log.e("sdgsdgsdgsg", totalINC + "");
                        Viewholder.tvINCDEC.setText(totalINC + "");
                        dataAdapters.get(position).setInc_dsc(totalINC + "");
                    }

                    dataAdapters.get(position).setProposed_qty(total + "");
                    if (total >= baseCopy) { /* total 105 : baseCopy 100*/
                        if (total > baseMax) { /* total 105 : baseMax 103*/

                            Viewholder.linearWarning.setVisibility(View.VISIBLE);
                            Viewholder.txtBaseCopy.setText("Total base copies are:" + " " + baseCopy + "");
                            Viewholder.txtBaseMax.setText("Upper limit:" + " " + baseMax + "");
                            Viewholder.txtBaseMin.setText("Lower limit:" + " " + baseMin + "");

                            dataAdapters.get(position).setIs_approval("0");
                            dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                            dataAdapters.get(position).setProposed_qty(total + "");
                            //  Viewholder.relMain.setBackgroundColor(Color.parseColor("#F3E0DA"));

                        } else {

                            Viewholder.linearWarning.setVisibility(View.GONE);
                            dataAdapters.get(position).setIs_approval("1");
                            dataAdapters.get(position).setRemark("");
                            dataAdapters.get(position).setProposed_qty(total + "");
                            //Viewholder.relMain.setBackgroundColor(Color.parseColor("#fafafa"));

                        }

                    } else {/* total 98 : baseCopy 100*/

                        if (total < baseMin) { /* total 94 : baseMin 95*/
                            Viewholder.linearWarning.setVisibility(View.VISIBLE);
                            Viewholder.txtBaseCopy.setText("Total base copies are:" + " " + baseCopy + "");
                            Viewholder.txtBaseMax.setText("Upper limit:" + " " + baseMax + "");
                            Viewholder.txtBaseMin.setText("Lower limit:" + " " + baseMin + "");
                            dataAdapters.get(position).setIs_approval("0");
                            dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                            dataAdapters.get(position).setProposed_qty(total + "");
                            // Viewholder.relMain.setBackgroundColor(Color.parseColor("#F3E0DA"));


                        } else {

                            Viewholder.linearWarning.setVisibility(View.GONE);
                            dataAdapters.get(position).setIs_approval("1");
                            dataAdapters.get(position).setRemark("");
                            dataAdapters.get(position).setProposed_qty(total + "");
                            // Viewholder.relMain.setBackgroundColor(Color.parseColor("#fafafa"));

                        }
                    }

                    if (dataAdapterOBJ.getScalabal_copies() != null && !dataAdapterOBJ.getScalabal_copies().isEmpty() && !dataAdapterOBJ.getScalabal_copies().equals("null")) {


                        if (!dataAdapterOBJ.getScalabal_copies().equals("0")) {
                            // Toast.makeText(context, "Ok", Toast.LENGTH_SHORT).show();


                            int totalScalable = 0;
                            int totalProposed = 0;
                            for (int i = 0; i < dataAdapters.size(); i++) {

                                if (dataAdapters.get(i).getSold_to_party().equals(dataAdapterOBJ.getSold_to_party())) {

                                    try {
                                        totalScalable = Integer.parseInt(dataAdapters.get(i).getScalabal_copies());

                                        int getProposedQTD = Integer.parseInt(dataAdapters.get(i).getProposed_qty());
                                        totalProposed += getProposedQTD;

                                        if (totalScalable < getProposedQTD) {

                                            Viewholder.txtCantAbove.setVisibility(View.VISIBLE);
                                            Viewholder.txtCantAbove.setText("Sold to party" + " " + dataAdapterOBJ.getSold_to_party() + " " + "can't be above than" + " " + totalScalable + "");
                                            dataAdapters.get(position).setError("true");
                                            Toast.makeText(context, "Sold to party" + " " + dataAdapterOBJ.getSold_to_party() + " " + "can't be above than" + " " + totalScalable + "", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Viewholder.txtCantAbove.setVisibility(View.GONE);
                                            dataAdapters.get(position).setError("false");
                                        }
                                    } catch (NumberFormatException e) {
                                        e.printStackTrace();
                                    }
                                }
                            }

                        } else {
                            Viewholder.txtCantAbove.setText("");
                            dataAdapters.get(position).setError("false");
                            //  Toast.makeText(context, "Else", Toast.LENGTH_SHORT).show();
                        }


                    } else {

                        // Toast.makeText(context, "Scalable copies not found", Toast.LENGTH_SHORT).show();
                    }

                    Viewholder.spinReason.setBackground(context.getResources().getDrawable(R.drawable.border));
                    Viewholder.tvINCDEC.setText(dataAdapterOBJ.getInc_dsc());

                    if (!dataAdapterOBJ.getInc_dsc().equals("0")) {

                        Viewholder.editSaleQTD.setBackground(context.getResources().getDrawable(R.drawable.border_green));

                    } else {

                        Viewholder.editSaleQTD.setBackground(context.getResources().getDrawable(R.drawable.border));
                    }

                }

            }

        }


        Viewholder.editSaleQTD.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Viewholder.btnSaveSaleQty.setVisibility(View.VISIBLE);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });

        Viewholder.btnSaveSaleQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Viewholder.btnSaveSaleQty.setVisibility(View.GONE);

                    //do here your stuff f

                    if (dataAdapterOBJ.getBase_min() != null && !dataAdapterOBJ.getBase_min().isEmpty() && !dataAdapterOBJ.getBase_min().equals("null")) {
                        baseMin = Integer.parseInt(dataAdapters.get(position).getBase_min());
                        baseMax = Integer.parseInt(dataAdapters.get(position).getBase_max());
                        baseCopy = Integer.parseInt(dataAdapters.get(position).getBase_copy());
                    }

                    if (v != null) {
                        InputMethodManager imm =
                                (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }


                    String strData = Viewholder.editSaleQTD.getText().toString();

                    if (strData.length() > 0) {

                        total = Integer.parseInt(strData);

                        int refQTD = 0;
                        int totalINC = 0;

                        if (dataAdapterOBJ.getRef_qtd() != null && !dataAdapterOBJ.getRef_qtd().isEmpty() && !dataAdapterOBJ.getRef_qtd().equals("null")) {

                            refQTD = Integer.parseInt(dataAdapterOBJ.getRef_qtd());
                            totalINC = total - refQTD;
                            Log.e("sdgsdgsdgsg", totalINC + "");
                            Viewholder.tvINCDEC.setText(totalINC + "");
                            dataAdapters.get(position).setInc_dsc(totalINC + "");
                            dataAdapters.get(position).setProposed_qty(total + "");

                           /* String strVal = "";
                            int tot = 0;
                            for (int i = 0; i < dataAdapters.size(); i++) {
                                strVal = dataAdapters.get(i).getProposed_qty();
                                int values = Integer.parseInt(strVal);
                                tot += values;
                            }


                            int txtRefMain = Integer.parseInt(DailyPoActivity.txtRefMain.getText().toString());
                            int txtRefTotal = Integer.parseInt(DailyPoActivity.txtRefTotal.getText().toString());



                            DailyPoActivity.txtSalesMain.setText(tot + "");
                            DailyPoActivity.txtSaleTotal.setText(tot + "");



                            int mainDiff = tot - txtRefMain;
                            int CopiesDiff = tot - txtRefTotal;
                            DailyPoActivity.txtMainDiff.setText(mainDiff + "");
                            DailyPoActivity.txtTotalCopiesDiff.setText(CopiesDiff + "");

                            if (mainDiff < 0) {
                                DailyPoActivity.txtMainDiff.setTextColor(context.getResources().getColor(R.color.red));
                                DailyPoActivity.txtTotalCopiesDiff.setTextColor(context.getResources().getColor(R.color.red));
                            } else {

                                DailyPoActivity.txtMainDiff.setTextColor(context.getResources().getColor(R.color.green_button_color));
                                DailyPoActivity.txtTotalCopiesDiff.setTextColor(context.getResources().getColor(R.color.green_button_color));
                            }*/
                        }

                        if (total >= baseCopy) {

                            if (total > baseMax) {

                                Viewholder.linearWarning.setVisibility(View.VISIBLE);
                                Viewholder.txtBaseCopy.setText("Total base copies are:" + " " + baseCopy + "");
                                Viewholder.txtBaseMax.setText("Upper limit:" + " " + baseMax + "");
                                Viewholder.txtBaseMin.setText("Lower limit:" + " " + baseMin + "");
                                dataAdapters.get(position).setIs_approval("0");
                                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                                dataAdapters.get(position).setProposed_qty(total + "");


                            } else {

                              /* if(total>refQTD){
                                   Log.e("sgdgdsgsdg","else");

                               }
                               else {

                                   Log.e("sgdgdsgsdg","else");
                               }*/

                                dataAdapters.get(position).setError("false");
                                Viewholder.linearWarning.setVisibility(View.GONE);
                                dataAdapters.get(position).setIs_approval("1");   /*copies Changed*/
                                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                                dataAdapters.get(position).setProposed_qty(total + "");

                                int refQ = Integer.parseInt(dataAdapterOBJ.getRef_qtd());
                                if (total != refQ) {

                                    dataAdapters.get(position).setIs_approval("0");/*copies not Changed*/
                                    dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                                    dataAdapters.get(position).setProposed_qty(total + "");

                                }

                                Log.e("fdgdgddfgdf","1"+" "+dataAdapterOBJ.getScalabal_copies());

                                if (dataAdapterOBJ.getScalabal_copies() != null && !dataAdapterOBJ.getScalabal_copies().isEmpty() && !dataAdapterOBJ.getScalabal_copies().equals("null")) {

                                    if (!dataAdapterOBJ.getScalabal_copies().equals("0")) {
                                        // Toast.makeText(context, "Ok", Toast.LENGTH_SHORT).show();

                                        int totalScalable = Integer.parseInt(dataAdapterOBJ.getScalabal_copies());
                                        int totalProposed =total;
                                        if (totalProposed > totalScalable) {

                                            Viewholder.linearWarning.setVisibility(View.VISIBLE);
                                            Viewholder.txtBaseCopy.setText("Total base copies are:" + " " + baseCopy + "");
                                            Viewholder.txtBaseMax.setText("Upper limit:" + " " + baseMax + "");
                                            Viewholder.txtBaseMin.setText("Lower limit:" + " " + baseMin + "");
                                            dataAdapters.get(position).setIs_approval("0");
                                            dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                                            dataAdapters.get(position).setProposed_qty(total + "");

                                            Viewholder.txtCantAbove.setVisibility(View.VISIBLE);
                                            Viewholder.txtCantAbove.setText("Sold to party" + " " + dataAdapterOBJ.getSold_to_party() + " " + "can't be above than" + " " + totalScalable + "");
                                            dataAdapters.get(position).setError("true");
                                            // Toast.makeText(context, "Sold to party" + " " + dataAdapterOBJ.getSold_to_party() + " " + "can't be above than" + " " + totalScalable + "", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Log.e("fdgdgddfgdf","IF"+" "+" Less");
                                            dataAdapters.get(position).setError("false");
                                            Viewholder.txtCantAbove.setVisibility(View.GONE);
                                            Viewholder.linearWarning.setVisibility(View.GONE);
                                            dataAdapters.get(position).setIs_approval("1");
                                            dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                                            dataAdapters.get(position).setProposed_qty(total + "");


                                            /*This is dupliacte data need to replace above 386 to 393*/

                                            int refQq = Integer.parseInt(dataAdapterOBJ.getRef_qtd());
                                            if (total != refQq) {
                                                dataAdapters.get(position).setIs_approval("0");
                                                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                                                dataAdapters.get(position).setProposed_qty(total + "");

                                            }
                                        }
                                    }
                                }
                            }

                        }
                        else {

                            if (total < baseMin) {
                                Viewholder.linearWarning.setVisibility(View.VISIBLE);
                                Viewholder.txtBaseCopy.setText("Total base copies are:" + " " + baseCopy + "");
                                Viewholder.txtBaseMax.setText("Upper limit:" + " " + baseMax + "");
                                Viewholder.txtBaseMin.setText("Lower limit:" + " " + baseMin + "");
                                dataAdapters.get(position).setIs_approval("0");
                                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                                dataAdapters.get(position).setProposed_qty(total + "");
                            }

                            else {
                                Viewholder.linearWarning.setVisibility(View.GONE);
                                dataAdapters.get(position).setIs_approval("1");
                                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                                dataAdapters.get(position).setProposed_qty(total + "");

                                int refQ = Integer.parseInt(dataAdapterOBJ.getRef_qtd());

                                if (total != refQ) {

                                    dataAdapters.get(position).setIs_approval("0");
                                    dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                                    dataAdapters.get(position).setProposed_qty(total + "");

                                }


                                Log.e("fdgdgddfgdf","2"+" "+dataAdapterOBJ.getScalabal_copies());

                            }
                        }

                    }
                    else {
                        total = 0;
                        if (total >= baseCopy) {
                            if (total > baseMax) {

                                Viewholder.linearWarning.setVisibility(View.VISIBLE);
                                Viewholder.txtBaseCopy.setText("Total base copies are:" + " " + baseCopy + "");
                                Viewholder.txtBaseMax.setText("Upper limit:" + " " + baseMax + "");
                                Viewholder.txtBaseMin.setText("Lower limit:" + " " + baseMin + "");
                                dataAdapters.get(position).setIs_approval("0");
                                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                                dataAdapters.get(position).setProposed_qty(total + "");

                            }
                            else {

                                Viewholder.linearWarning.setVisibility(View.GONE);
                                dataAdapters.get(position).setIs_approval("1");
                                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                                dataAdapters.get(position).setProposed_qty(total + "");

                                int refQ = Integer.parseInt(dataAdapterOBJ.getRef_qtd());
                                if (total != refQ) {

                                    dataAdapters.get(position).setIs_approval("0");
                                    dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                                    dataAdapters.get(position).setProposed_qty(total + "");

                                }

                            }

                        } else {

                            if (total < baseMin) {
                                Viewholder.linearWarning.setVisibility(View.VISIBLE);
                                Viewholder.txtBaseCopy.setText("Total base copies are:" + " " + baseCopy + "");
                                Viewholder.txtBaseMax.setText("Upper limit:" + " " + baseMax + "");
                                Viewholder.txtBaseMin.setText("Lower limit:" + " " + baseMin + "");
                                dataAdapters.get(position).setIs_approval("0");
                                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                                dataAdapters.get(position).setProposed_qty(total + "");


                            } else {
                                Viewholder.linearWarning.setVisibility(View.GONE);
                                dataAdapters.get(position).setIs_approval("1");
                                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                                dataAdapters.get(position).setProposed_qty(total + "");

                                int refQ = Integer.parseInt(dataAdapterOBJ.getRef_qtd());
                                if (total != refQ) {

                                    dataAdapters.get(position).setIs_approval("0");
                                    dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                                    dataAdapters.get(position).setProposed_qty(total + "");

                                }

                            }
                        }
                    }

                    dataAdapters.get(position).setProposed_qty(total + "");

                    if (dataAdapterOBJ.getScalabal_copies() != null && !dataAdapterOBJ.getScalabal_copies().isEmpty() && !dataAdapterOBJ.getScalabal_copies().equals("null")) {

                        if (!dataAdapterOBJ.getScalabal_copies().equals("0")) {
                            // Toast.makeText(context, "Ok", Toast.LENGTH_SHORT).show();


                            int totalScalable = 0;
                            int totalProposed = 0;

                            for (int i = 0; i < dataAdapters.size(); i++) {
                                if (dataAdapters.get(i).getSold_to_party().equals(dataAdapterOBJ.getSold_to_party())) {
                                    totalScalable = Integer.parseInt(dataAdapters.get(i).getScalabal_copies());
                                    int getProposedQTD = Integer.parseInt(dataAdapters.get(i).getProposed_qty());
                                    totalProposed += getProposedQTD;

                                    if (totalProposed >totalScalable) {
                                        Viewholder.txtCantAbove.setVisibility(View.VISIBLE);
                                        Viewholder.txtCantAbove.setText("Sold to party" + " " + dataAdapterOBJ.getSold_to_party() + " " + "can't be above than" + " " + totalScalable + "");
                                        dataAdapters.get(position).setError("true");
                                        Log.e("afsfaasfaf","Error");
                                        // Toast.makeText(context, "Sold to party" + " " + dataAdapterOBJ.getSold_to_party() + " " + "can't be above than" + " " + totalScalable + "", Toast.LENGTH_SHORT).show();
                                    } else {

                                        Viewholder.txtCantAbove.setVisibility(View.GONE);
                                        dataAdapters.get(position).setError("false");

                                    }
                                }
                            }

                        } else {
                            Viewholder.txtCantAbove.setText("");
                            dataAdapters.get(position).setError("false");
                            //  Toast.makeText(context, "Else", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        //Toast.makeText(context, "Scalable copies not found", Toast.LENGTH_SHORT).show();
                    }



                    if(total==0){

                        Viewholder.spinReason.setVisibility(View.GONE);
                        Viewholder.txtReason.setVisibility(View.VISIBLE);
                        Viewholder.txtReason.setText("HOLD");
                        dataAdapters.get(position).setReason_qty_change("22");
                    }
                    else {

                        Viewholder.spinReason.setVisibility(View.VISIBLE);
                        Viewholder.txtReason.setVisibility(View.GONE);
                    }

                    if (context instanceof DailyPoActivity) {
                        ((DailyPoActivity)context).GetINCDECValues();
                    }
                    if (context instanceof DailyPoActivity) {
                        ((DailyPoActivity)context).UpdateMainJJCopiesDifference();
                    }


            }
        });


        Viewholder.editRemark.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
            }
        });


        /*Viewholder.editRemark.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //do here your stuff f
                    Log.e("sdjfhkjsdhfkjshd", position + "");

                    if (v != null) {
                        InputMethodManager imm =
                                (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                    }
                    dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                    return true;
                }
                return false;
            }
        });*/


        if(!dataAdapterOBJ.getItem_cat().equals("ZFOR")) {
            Viewholder.spinReason.setEnabled(true);
            ArrayAdapter aa = new ArrayAdapter(context, android.R.layout.simple_spinner_item, rName);
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            Viewholder.spinReason.setAdapter(aa);
            Viewholder.spinReason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int positions, long id) {

                    try {
                        Log.e("sdgsdg", rId.get(positions));
                        dataAdapters.get(position).setReason_qty_change(rId.get(positions));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
        else {

            Viewholder.spinReason.setEnabled(false);
            ArrayAdapter aa = new ArrayAdapter(context, android.R.layout.simple_spinner_item, rName);
            aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            Viewholder.spinReason.setAdapter(aa);
            Viewholder.spinReason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int positions, long id) {

                    try {
                        Log.e("sdgsdg", rId.get(positions));
                        dataAdapters.get(position).setReason_qty_change(rId.get(positions));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }

        if (dataAdapterOBJ.getItem_cat().equals("JTAP")) {

            Log.e("Afasaasfas", "1");
            if (dataAdapterOBJ.getIs_approval().equals("0")) {

                Viewholder.relMain.setBackgroundColor(Color.parseColor("#F3E0DA"));
                Viewholder.spinReason.setEnabled(true);
                ArrayAdapter aa = new ArrayAdapter(context, android.R.layout.simple_spinner_item, rName);
                aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                Viewholder.spinReason.setAdapter(aa);
                       if (!dataAdapterOBJ.getReason_qty_change().equals("false")) {
                            int pos = Integer.parseInt(dataAdapters.get(position).getReason_qty_change());
                            Viewholder.spinReason.setSelection(pos);
                        }
                Viewholder.spinReason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int positions, long id) {

                        try {
                            Log.e("sdgsdg", rId.get(positions));
                            dataAdapters.get(position).setReason_qty_change(rId.get(positions));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public ArrayList<DailyPOModal> getArrayData() {
        return (ArrayList<DailyPOModal>) dataAdapters;
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout linDetail, linearWarning;
        ImageView imgUp;
        ImageView imgDrop;
        EditText editSaleQTD;
        TextView tvEdtion, txtShipToParty, tvCopyType, tvSoldToParty, tvShipToParty, tvItemCat, tvRefQtd, tvINCDEC, txtMainEditionName;
        TextView txtBaseCopy,txtReason, txtBaseMax, txtBaseMin, txtCantAbove,txtCityname;
        EditText editRemark;
        Spinner spinReason;
        LinearLayout relMain;
        MaterialCardView cardViewMain;
        Button btnSaveSaleQty;


        public ViewHolder(View itemView) {
            super(itemView);
            imgUp = itemView.findViewById(R.id.imgUp);
            imgDrop = itemView.findViewById(R.id.imgDrop);
            linDetail = itemView.findViewById(R.id.linDetail);
            relMain = itemView.findViewById(R.id.relMain);

            tvEdtion = itemView.findViewById(R.id.tvEdtion);
            tvCopyType = itemView.findViewById(R.id.tvCopyType);
            tvSoldToParty = itemView.findViewById(R.id.tvSoldToParty);
            tvShipToParty = itemView.findViewById(R.id.tvShipToParty);
            tvItemCat = itemView.findViewById(R.id.tvItemCat);
            tvRefQtd = itemView.findViewById(R.id.tvRefQtd);
            editSaleQTD = itemView.findViewById(R.id.editSaleQTD);
            btnSaveSaleQty = itemView.findViewById(R.id.btn_save_sale_qty);
            tvINCDEC = itemView.findViewById(R.id.tvINCDEC);
            editRemark = itemView.findViewById(R.id.editRemark);
            spinReason = itemView.findViewById(R.id.spinReason);
            txtBaseCopy = itemView.findViewById(R.id.txtBaseCopy);
            txtBaseMin = itemView.findViewById(R.id.txtBaseMin);
            txtCantAbove = itemView.findViewById(R.id.txtCantAbove);
            txtBaseMax = itemView.findViewById(R.id.txtBaseMax);
            linearWarning = itemView.findViewById(R.id.linearWarning);
            txtMainEditionName = itemView.findViewById(R.id.txtMainEditionName);
            cardViewMain = itemView.findViewById(R.id.cardViewMain);
            txtShipToParty = itemView.findViewById(R.id.txtShipToParty);
            txtReason = itemView.findViewById(R.id.txtReason);
            txtCityname = itemView.findViewById(R.id.txtCityname);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String filterString = charSequence.toString().toLowerCase();
                //arraylist1.clear();
                FilterResults results = new FilterResults();
                int count = searchArray.size();
                final List<DailyPOModal> list = new ArrayList<>();

                DailyPOModal filterableString;

                for (int i = 0; i < count; i++) {
                    filterableString = searchArray.get(i);
                    if (searchArray.get(i).getEdi_name().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                    } else if (searchArray.get(i).getSold_to_party().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                    } else if (searchArray.get(i).getShip_to_party().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                    } else if (searchArray.get(i).getCust_name().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                    } else {
                    }
                }

                results.values = list;
                results.count = list.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                dataAdapters = (List<DailyPOModal>) filterResults.values;
                notifyDataSetChanged();

            }
        };

    }
}