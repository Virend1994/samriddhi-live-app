package com.app.samriddhi.ui.activity.main.ui;

import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;

import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.events.LangugeEvent;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.auth.LoginActivity;
import com.app.samriddhi.ui.fragment.LanguageDialogFragment;
import com.app.samriddhi.ui.model.LanguageModel;
import com.app.samriddhi.ui.presenter.LanguagePresenter;
import com.app.samriddhi.ui.view.IView;
import com.app.samriddhi.util.SystemUtility;
import com.google.firebase.iid.FirebaseInstanceId;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class LanguageActivity extends BaseActivity implements IView {
    LanguagePresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lang_selection);
        showDialog();
        askStoragePermission(202);

    }
    /**
     * open the language selection dialog
     */

    private void showDialog() {
        LanguageDialogFragment fragment = new LanguageDialogFragment();
        fragment.setCancelable(false);
        fragment.show(getFragmentManager(), "dialog");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onLangaugeChangeEvent(LangugeEvent event) {
        mPresenter=new LanguagePresenter();
        mPresenter.setView(this);
        mPresenter.submitLanguageChange(new LanguageModel(PreferenceManager.getAgentId(this), SystemUtility.getDeviceId(this),
                PreferenceManager.getAppLang(this), FirebaseInstanceId.getInstance().getToken()));
    }

    @Override
    public void onInfo(String message) {
        finish();
        if (!PreferenceManager.getIsUserLoggedIn(this))
            startActivityAnimation(this, LoginActivity.class, false);
        else
            startActivityAnimation(this, HomeActivity.class, true);
    }

    @Override
    public Context getContext() {
        return this;
    }
}
