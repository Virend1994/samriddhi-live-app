package com.app.samriddhi.ui.fragment;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.app.samriddhi.R;
import com.app.samriddhi.databinding.FragLanguageDialogBinding;
import com.app.samriddhi.events.LangugeEvent;
import com.app.samriddhi.prefernces.PreferenceManager;

import org.greenrobot.eventbus.EventBus;

public class LanguageDialogFragment extends DialogFragment implements RadioGroup.OnCheckedChangeListener {
    FragLanguageDialogBinding fragLanguageDialogBinding;
    private boolean isChecking = true;
    private int mCheckedId = R.id.radio_hindi;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragLanguageDialogBinding = DataBindingUtil.inflate(inflater, R.layout.frag_language_dialog, container, false);
        fragLanguageDialogBinding.setFragment(this);
        fragLanguageDialogBinding.radioGroupFirst.setOnCheckedChangeListener(this);
        fragLanguageDialogBinding.radioGpSecond.setOnCheckedChangeListener(this);
        PreferenceManager.setLangDialogShown(getActivity(), true);
        initalizeViews();
        return fragLanguageDialogBinding.getRoot();
    }

    private void initalizeViews() {
        if (PreferenceManager.getAppLang(getActivity()).equalsIgnoreCase("hi")) {
            mCheckedId = R.id.radio_hindi;
            fragLanguageDialogBinding.radioHindi.setChecked(true);
        } else if (PreferenceManager.getAppLang(getActivity()).equalsIgnoreCase("en")) {
            mCheckedId = R.id.radio_english;
            fragLanguageDialogBinding.radioEnglish.setChecked(true);
        } else if (PreferenceManager.getAppLang(getActivity()).equalsIgnoreCase("gu")) {
            mCheckedId = R.id.radio_gujarati;
            fragLanguageDialogBinding.radioGujarati.setChecked(true);
        } else if (PreferenceManager.getAppLang(getActivity()).equalsIgnoreCase("mr")) {
            mCheckedId = R.id.radio_marathi;
            fragLanguageDialogBinding.radioMarathi.setChecked(true);
        } else {
            mCheckedId = R.id.radio_hindi;
            fragLanguageDialogBinding.radioHindi.setChecked(true);
        }
    }

    /**
     * handle the button click
     *
     * @param view
     */
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_cancel:
                getSelectedAppLanguage();
                break;
            case R.id.btn_ok:
                getSelectedAppLanguage();
                break;
            default:
                break;
        }

    }

    /**
     * check the selected language and update
     */


    private void getSelectedAppLanguage() {
        switch (mCheckedId) {
            case R.id.radio_english:
                savedSelectedLanguage("en");
                break;
            case R.id.radio_hindi:
                savedSelectedLanguage("hi");
                break;
            case R.id.radio_gujarati:
                savedSelectedLanguage("gu");
                break;
            case R.id.radio_marathi:
                savedSelectedLanguage("mr");
                break;
            default:
                break;
        }
    }


    private void savedSelectedLanguage(String langCode) {
        PreferenceManager.setAppLang(getActivity(), langCode);
        EventBus.getDefault().post(new LangugeEvent("languageChange"));
        //dismiss();
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (group.getId()) {
            case R.id.radio_group_first:
                if (checkedId != -1 && isChecking) {
                    isChecking = false;
                    fragLanguageDialogBinding.radioGpSecond.clearCheck();
                    mCheckedId = checkedId;
                }
                isChecking = true;
                break;

            case R.id.radio_gp_second:
                if (checkedId != -1 && isChecking) {
                    isChecking = false;
                    fragLanguageDialogBinding.radioGroupFirst.clearCheck();
                    mCheckedId = checkedId;
                }
                isChecking = true;
                break;
        }
    }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

}
