package com.app.samriddhi.ui.activity.main.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.RecyclerViewArrayAdapter;
import com.app.samriddhi.databinding.ActivityNotificationBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.auth.LoginActivity;
import com.app.samriddhi.ui.activity.main.ui.Grievance.HelpSupportStatus;
import com.app.samriddhi.ui.model.NotificationModel;
import com.app.samriddhi.ui.presenter.NotificationPresenter;
import com.app.samriddhi.ui.view.INotificationView;
import com.app.samriddhi.util.Constant;

import java.util.ArrayList;
import java.util.List;

public class NotificationActivity extends BaseActivity implements INotificationView, RecyclerViewArrayAdapter.OnItemClickListener<NotificationModel> {
    ActivityNotificationBinding mBinding;
    ArrayList<NotificationModel> arrNotifications = new ArrayList<>();
    NotificationPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_notification);
        initView();
    }

    /**
     * initialize and bind views
     */





    private void initView() {

        mBinding.toolbarNotification.rlNotification.setVisibility(View.GONE);
        mBinding.toolbarNotification.txtTittle.setText(getResources().getString(R.string.str_notification));
        mBinding.toolbarNotification.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mBinding.recycleNotifications.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    public void onNotificationsSuccess(List<NotificationModel> body) {
        mPresenter.updateNotificationCount(PreferenceManager.getAgentId(this));
        if (body != null && body.size() > 0) {

            mBinding.txtNoData.setVisibility(View.GONE);
            mBinding.recycleNotifications.setVisibility(View.VISIBLE);
            mBinding.recycleNotifications.setAdapter(new RecyclerViewArrayAdapter(body, this));
        } else {
            mBinding.txtNoData.setVisibility(View.VISIBLE);
            mBinding.recycleNotifications.setVisibility(View.GONE);
        }
    }

    @Override
    public void onError(String reason) {
        //super.onError(reason);
        mPresenter.updateNotificationCount(PreferenceManager.getAgentId(this));
    }

    @Override
    public void onInfo(String message) {
        // will override if require
    }

    @Override
    public Context getContext() {
        return this;
    }


    @Override
    protected void onResume() {
        super.onResume();
        //mPresenter = new NotificationPresenter();
        mPresenter = new NotificationPresenter();
        mPresenter.setView(this);
        mPresenter.getNotifications(PreferenceManager.getAgentId(this));
    }

    /**
     * handle the notifications click for screens
     * @param screen_name  screens name
     */
    private void handleNotifications(String screen_name) {
        Intent launchIntent = null;
        if (PreferenceManager.getIsUserLoggedIn(this)) {
            if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("AG")) {
                if (screen_name.equalsIgnoreCase("billing")) {
                    launchIntent = new Intent(this, BillingActivity.class);
                } else if (screen_name.equalsIgnoreCase("Profile")) {
                    launchIntent = new Intent(this, ProfileActivity.class);
                    launchIntent.putExtra("profileShow", false);
                } else if (screen_name.equalsIgnoreCase("Copies")) {
                    launchIntent = new Intent(this, NoOfCopiesActivity.class);
                } else if (screen_name.equalsIgnoreCase("Ledger")) {
                    launchIntent = new Intent(this, LedgerActivity.class);
                } else {
                    launchIntent = new Intent(this, HomeActivity.class);
                }
            } else {
                if (screen_name.equalsIgnoreCase("billing")) {
                    if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("EX") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("CE") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("UE")) {
                        launchIntent = new Intent(this, AgentListActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardBill);
                    } else if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("SH")) {
                        launchIntent = new Intent(this, UserCityActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardBill);
                    } else if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("CO")) {
                        launchIntent = new Intent(this, UserStateActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardBill);
                    }
                } else if (screen_name.equalsIgnoreCase("Profile")) {
                    if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("EX") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("CE") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("UE")) {
                        launchIntent = new Intent(this, AgentListActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DrawerProfile);
                    } else if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("SH")) {
                        launchIntent = new Intent(this, UserCityActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DrawerProfile);
                    } else if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("CO")) {
                        launchIntent = new Intent(this, UserStateActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DrawerProfile);
                    }
                } else if (screen_name.equalsIgnoreCase("Ledger")) {
                    if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("EX") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("CE") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("UE")) {
                        launchIntent = new Intent(this, AgentListActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardOutstanding);
                    } else if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("SH")) {
                        launchIntent = new Intent(this, UserCityActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardOutstanding);
                    } else if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("CO")) {
                        launchIntent = new Intent(this, UserStateActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardOutstanding);
                    }
                } else if (screen_name.equalsIgnoreCase("Copies")) {
                    if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("EX") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("CE") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("UE")) {
                        launchIntent = new Intent(this, AgentListActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardNoOfCopies);
                    } else if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("UI") || PreferenceManager.getPrefUserType(this).equalsIgnoreCase("SH")) {
                        launchIntent = new Intent(this, SalesOrgCityActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardNoOfCopies);
                    } else if (PreferenceManager.getPrefUserType(this).equalsIgnoreCase("CO")) {
                        launchIntent = new Intent(this, UserStateActivity.class).putExtra(Constant.SCREEN_NAME, Constant.DashBoardNoOfCopies);
                    }
                } else {
                    launchIntent = new Intent(this, HomeActivity.class);
                }
            }
        } else {
            launchIntent = new Intent(this, LoginActivity.class);
        }

        startActivity(launchIntent);
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }
    @Override
    public void onItemClick(View view, NotificationModel object) {


         Log.e("Sdsdfsd","Type"+" "+object.getType());

         if (object.getType().equals("20")) {
             mPresenter.notificationRead(String.valueOf(object.getNitificationId()));
            handleNotifications(object.getScreen_Name());
            Intent intent =new Intent(NotificationActivity.this,HelpSupportStatus.class);
            startActivity(intent);

        }

       else if(object.getType().equals("3")||object.getType().equals("2")){


           mPresenter.notificationRead(String.valueOf(object.getNitificationId()));
            handleNotifications(object.getScreen_Name());
            Intent intent =new Intent(NotificationActivity.this,WebviewUrl.class);
            intent.putExtra("url",object.getUrl());
            intent.putExtra("type",object.getType());
            startActivity(intent);
            //Toast.makeText(NotificationActivity.this, "", Toast.LENGTH_SHORT).show();
        }
        else
        {
            mPresenter.notificationRead(String.valueOf(object.getNitificationId()));
            handleNotifications(object.getScreen_Name());
        }
    }
}
