package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BillingModel extends BaseModel {
    @SerializedName("results")
    @Expose
    private List<BillingListModel> results = null;

    public List<BillingListModel> getResults() {
        return results;
    }

    public void setResults(List<BillingListModel> results) {
        this.results = results;
    }
}
