package com.app.samriddhi.ui.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;

import java.io.Serializable;

public class OrderSupplyModel implements Serializable {
    @SerializedName("zvt_portal_cir_id")
    @Expose
    private Integer zvtPortalCirId;
    @SerializedName("mandt")
    @Expose
    private String mandt;
    @SerializedName("vbeln")
    @Expose
    private String vbeln;
    @SerializedName("posnr")
    @Expose
    private String posnr;
    @SerializedName("gjahr")
    @Expose
    private String gjahr;
    @SerializedName("monat")
    @Expose
    private String monat;
    @SerializedName("vkorg")
    @Expose
    private String vkorg;
    @SerializedName("vtweg")
    @Expose
    private String vtweg;
    @SerializedName("spart")
    @Expose
    private String spart;
    @SerializedName("pstyv")
    @Expose
    private String pstyv;
    @SerializedName("sold_to_party")
    @Expose
    private String soldToParty;
    @SerializedName("ship_to_party")
    @Expose
    private String shipToParty;
    @SerializedName("ord_date")
    @Expose
    private String ordDate;
    @SerializedName("vgbel")
    @Expose
    private String vgbel;
    @SerializedName("bezei")
    @Expose
    private String bezei;
    @SerializedName("drerz")
    @Expose
    private String drerz;
    @SerializedName("pva")
    @Expose
    private String pva;
    @SerializedName("matnr")
    @Expose
    private String matnr;
    @SerializedName("pub_name")
    @Expose
    private String pubName;
    @SerializedName("soff_name")
    @Expose
    private String soffName;
    @SerializedName("cg_name")
    @Expose
    private String cgName;
    @SerializedName("sdist_name")
    @Expose
    private String sdistName;
    @SerializedName("edition_name")
    @Expose
    private String editionName;
    @SerializedName("cust_name")
    @Expose
    private String custName;
    @SerializedName("city_name")
    @Expose
    private String cityName;
    @SerializedName("vkgrp")
    @Expose
    private String vkgrp;
    @SerializedName("sgrp_name")
    @Expose
    private String sgrpName;
    @SerializedName("vkbur")
    @Expose
    private String vkbur;
    @SerializedName("kdgrp")
    @Expose
    private String kdgrp;
    @SerializedName("bzirk")
    @Expose
    private String bzirk;
    @SerializedName("rate")
    @Expose
    private String rate;
    @SerializedName("disc_perc")
    @Expose
    private String discPerc;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("gross_copy")
    @Expose
    private Integer grossCopy;
    @SerializedName("free_copy")
    @Expose
    private Integer freeCopy;
    @SerializedName("paid_copy")
    @Expose
    private Integer paidCopy;
    @SerializedName("zlao")
    @Expose
    private Integer zlao;
    @SerializedName("zcoo")
    @Expose
    private Integer zcoo;
    @SerializedName("gross_value")
    @Expose
    private String grossValue;
    @SerializedName("net_value")
    @Expose
    private String netValue;
    @SerializedName("route")
    @Expose
    private String route;
    @SerializedName("ord_date_f")
    @Expose
    private String ordDateF;
    @SerializedName("statename")
    @Expose
    private String statename;
    @SerializedName("editionname")
    @Expose
    private String editionname;
    @SerializedName("mainjj")
    @Expose
    private String mainjj;
    @SerializedName("proposed_qty")
    @Expose
    private Integer proposedQty;
    @SerializedName("CIR_Order_Update")
    @Expose
    private JSONArray cIROrderUpdate = null;
    @SerializedName("base_copy")
    @Expose
    private JSONArray baseCopy = null;


    @SerializedName("current_copy")
    @Expose
    private String current_copy = "";


    @SerializedName("from_date")
    @Expose
    private String from_date = "";


    @SerializedName("to_date")
    @Expose
    private String to_date = "";

    @SerializedName("remark")
    @Expose
    private String remark = "";

    @SerializedName("reason")
    @Expose
    private String reason = "";


    public JSONArray getcIROrderUpdate() {
        return cIROrderUpdate;
    }

    public void setcIROrderUpdate(JSONArray cIROrderUpdate) {
        this.cIROrderUpdate = cIROrderUpdate;
    }

    public String getCurrent_copy() {
        return current_copy;
    }

    public void setCurrent_copy(String current_copy) {
        this.current_copy = current_copy;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getZvtPortalCirId() {
        return zvtPortalCirId;
    }

    public void setZvtPortalCirId(Integer zvtPortalCirId) {
        this.zvtPortalCirId = zvtPortalCirId;
    }

    public String getMandt() {
        return mandt;
    }

    public void setMandt(String mandt) {
        this.mandt = mandt;
    }

    public String getVbeln() {
        return vbeln;
    }

    public void setVbeln(String vbeln) {
        this.vbeln = vbeln;
    }

    public String getPosnr() {
        return posnr;
    }

    public void setPosnr(String posnr) {
        this.posnr = posnr;
    }

    public String getGjahr() {
        return gjahr;
    }

    public void setGjahr(String gjahr) {
        this.gjahr = gjahr;
    }

    public String getMonat() {
        return monat;
    }

    public void setMonat(String monat) {
        this.monat = monat;
    }

    public String getVkorg() {
        return vkorg;
    }

    public void setVkorg(String vkorg) {
        this.vkorg = vkorg;
    }

    public String getVtweg() {
        return vtweg;
    }

    public void setVtweg(String vtweg) {
        this.vtweg = vtweg;
    }

    public String getSpart() {
        return spart;
    }

    public void setSpart(String spart) {
        this.spart = spart;
    }

    public String getPstyv() {
        return pstyv;
    }

    public void setPstyv(String pstyv) {
        this.pstyv = pstyv;
    }

    public String getSoldToParty() {
        return soldToParty;
    }

    public void setSoldToParty(String soldToParty) {
        this.soldToParty = soldToParty;
    }

    public String getShipToParty() {
        return shipToParty;
    }

    public void setShipToParty(String shipToParty) {
        this.shipToParty = shipToParty;
    }

    public String getOrdDate() {
        return ordDate;
    }

    public void setOrdDate(String ordDate) {
        this.ordDate = ordDate;
    }

    public String getVgbel() {
        return vgbel;
    }

    public void setVgbel(String vgbel) {
        this.vgbel = vgbel;
    }

    public String getBezei() {
        return bezei;
    }

    public void setBezei(String bezei) {
        this.bezei = bezei;
    }

    public String getDrerz() {
        return drerz;
    }

    public void setDrerz(String drerz) {
        this.drerz = drerz;
    }

    public String getPva() {
        return pva;
    }

    public void setPva(String pva) {
        this.pva = pva;
    }

    public String getMatnr() {
        return matnr;
    }

    public void setMatnr(String matnr) {
        this.matnr = matnr;
    }

    public String getPubName() {
        return pubName;
    }

    public void setPubName(String pubName) {
        this.pubName = pubName;
    }

    public String getSoffName() {
        return soffName;
    }

    public void setSoffName(String soffName) {
        this.soffName = soffName;
    }

    public String getCgName() {
        return cgName;
    }

    public void setCgName(String cgName) {
        this.cgName = cgName;
    }

    public String getSdistName() {
        return sdistName;
    }

    public void setSdistName(String sdistName) {
        this.sdistName = sdistName;
    }

    public String getEditionName() {
        return editionName;
    }

    public void setEditionName(String editionName) {
        this.editionName = editionName;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getVkgrp() {
        return vkgrp;
    }

    public void setVkgrp(String vkgrp) {
        this.vkgrp = vkgrp;
    }

    public String getSgrpName() {
        return sgrpName;
    }

    public void setSgrpName(String sgrpName) {
        this.sgrpName = sgrpName;
    }

    public String getVkbur() {
        return vkbur;
    }

    public void setVkbur(String vkbur) {
        this.vkbur = vkbur;
    }

    public String getKdgrp() {
        return kdgrp;
    }

    public void setKdgrp(String kdgrp) {
        this.kdgrp = kdgrp;
    }

    public String getBzirk() {
        return bzirk;
    }

    public void setBzirk(String bzirk) {
        this.bzirk = bzirk;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getDiscPerc() {
        return discPerc;
    }

    public void setDiscPerc(String discPerc) {
        this.discPerc = discPerc;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public Integer getGrossCopy() {
        return grossCopy;
    }

    public void setGrossCopy(Integer grossCopy) {
        this.grossCopy = grossCopy;
    }

    public Integer getFreeCopy() {
        return freeCopy;
    }

    public void setFreeCopy(Integer freeCopy) {
        this.freeCopy = freeCopy;
    }

    public Integer getPaidCopy() {
        return paidCopy;
    }

    public void setPaidCopy(Integer paidCopy) {
        this.paidCopy = paidCopy;
    }

    public Integer getZlao() {
        return zlao;
    }

    public void setZlao(Integer zlao) {
        this.zlao = zlao;
    }

    public Integer getZcoo() {
        return zcoo;
    }

    public void setZcoo(Integer zcoo) {
        this.zcoo = zcoo;
    }

    public String getGrossValue() {
        return grossValue;
    }

    public void setGrossValue(String grossValue) {
        this.grossValue = grossValue;
    }

    public String getNetValue() {
        return netValue;
    }

    public void setNetValue(String netValue) {
        this.netValue = netValue;
    }

    public String getRoute() {
        return route;
    }

    public void setRoute(String route) {
        this.route = route;
    }

    public String getOrdDateF() {
        return ordDateF;
    }

    public void setOrdDateF(String ordDateF) {
        this.ordDateF = ordDateF;
    }

    public String getStatename() {
        return statename;
    }

    public void setStatename(String statename) {
        this.statename = statename;
    }

    public String getEditionname() {
        return editionname;
    }

    public void setEditionname(String editionname) {
        this.editionname = editionname;
    }

    public String getMainjj() {
        return mainjj;
    }

    public void setMainjj(String mainjj) {
        this.mainjj = mainjj;
    }

    public Integer getProposedQty() {
        return proposedQty;
    }

    public void setProposedQty(Integer proposedQty) {
        this.proposedQty = proposedQty;
    }

    public JSONArray getCIROrderUpdate() {
        return cIROrderUpdate;
    }

    public void setCIROrderUpdate(JSONArray cIROrderUpdate) {
        this.cIROrderUpdate = cIROrderUpdate;
    }

    public JSONArray getBaseCopy() {
        return baseCopy;
    }

    public void setBaseCopy(JSONArray baseCopy) {
        this.baseCopy = baseCopy;
    }




}





 


