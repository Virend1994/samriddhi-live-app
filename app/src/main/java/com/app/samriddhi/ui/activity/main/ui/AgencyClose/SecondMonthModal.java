package com.app.samriddhi.ui.activity.main.ui.AgencyClose;

public class SecondMonthModal {

    public String id;
    public String date;
    public String coppies;
    public String monthName;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCoppies() {
        return coppies;
    }

    public void setCoppies(String coppies) {
        this.coppies = coppies;
    }

    public String getMonthName() {
        return monthName;
    }

    public void setMonthName(String monthName) {
        this.monthName = monthName;
    }
}
