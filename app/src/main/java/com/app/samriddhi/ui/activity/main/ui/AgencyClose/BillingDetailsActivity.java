package com.app.samriddhi.ui.activity.main.ui.AgencyClose;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.util.Constant;
import com.itextpdf.text.Image;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

import okhttp3.Credentials;

public class BillingDetailsActivity extends BaseActivity {

    TextView txtFirstMonthName, txtFirstSecondName, txtThirdMonthName,txtFourthMonthName;
    ImageView imgBack;
    RecyclerView recyclerview, recyclerviewSecond, recyclerviewThird,recyclerviewFourth;
    List<LastThreeMonthBillingModal> detailList;
    List<LastThreeMonthCopiesModal> copiesdetailList;
    List<SecondMonthModal> secondMonthcopiesList;
    List<ThirdMonthModal> thirdMonthcopiesList;
    List<FourthMonthModal> fourthMonthModalList;
    LastThreeMonthsBillingAdapter lastThreeMonthsBillingAdapter;
    String strDetailStatus = "";
    String AgencyId = "";
    String[] monthNames = {"January", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    String strCurrentMOnth="",strLastMonth="",strSecondLastMonth="",strThirdLastMonth="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_billing_details);
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strDetailStatus = AppsContants.sharedpreferences.getString(AppsContants.DetailStatus, "");
        AgencyId = AppsContants.sharedpreferences.getString(AppsContants.AgencyId, "");

        txtFirstMonthName = findViewById(R.id.txtFirstMonthName);
        txtFirstSecondName = findViewById(R.id.txtFirstSecondName);
        txtThirdMonthName = findViewById(R.id.txtThirdMonthName);
        txtFourthMonthName = findViewById(R.id.txtFourthMonthName);


        recyclerview = findViewById(R.id.recyclerview);
        recyclerviewSecond = findViewById(R.id.recyclerviewSecond);
        recyclerviewThird = findViewById(R.id.recyclerviewThird);
        recyclerviewFourth = findViewById(R.id.recyclerviewFourth);

        imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });


        DateFormat dateFormat = new SimpleDateFormat("MM");
        Date date2 = new Date();
        strCurrentMOnth=dateFormat.format(date2);


        Calendar c = new GregorianCalendar();
        c.setTime(new Date());
        SimpleDateFormat sdf = new SimpleDateFormat("MM");
        c.add(Calendar.MONTH, -1);
        strLastMonth= sdf.format(c.getTime());
        Log.e("qweqqeqwqe",strLastMonth);

        Calendar c2 = new GregorianCalendar();
        c2.setTime(new Date());
        SimpleDateFormat sdf2 = new SimpleDateFormat("MM");
        c2.add(Calendar.MONTH, -2);
        strSecondLastMonth= sdf2.format(c2.getTime());
        Log.e("qweqqeqwqe",strSecondLastMonth);



        Calendar c3= new GregorianCalendar();
        c3.setTime(new Date());
        SimpleDateFormat sdf3 = new SimpleDateFormat("MM");
        c3.add(Calendar.MONTH, -3);
        strThirdLastMonth= sdf3.format(c3.getTime());
        Log.e("qweqqeqwqe",strThirdLastMonth);


        getCopies();
    }


    public void getCopies() {
        enableLoadingBar(true);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("agency_code", AgencyId);
            // jsonObject.put("agency_code", "10011137");
        } catch (JSONException e) {
            e.printStackTrace();
        }


        AndroidNetworking.post(Constant.BASE_PORTAL_URL + "dash/api/agency-no-of-copies")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .addJSONObjectBody(jsonObject)
                .setTag("Copies")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        String strMonthName = "";

                        try {
                            Log.e("rtrtrtr", response.toString());
                            JSONObject jsonObject = new JSONObject(response.getString("copies"));
                            JSONArray jsonArray = new JSONArray(jsonObject.getString("results"));
                            copiesdetailList = new ArrayList<>();
                            secondMonthcopiesList = new ArrayList<>();
                            thirdMonthcopiesList = new ArrayList<>();
                            fourthMonthModalList = new ArrayList<>();

                            Log.e("dsgsdgsdgsgs", jsonArray.length() + "");

                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                String[] strSplit = jsonObject1.getString("OrdDate").split("-");
                                String strGetMonth = strSplit[1];
                                if (strGetMonth.equals(strCurrentMOnth)) {
                                    try {
                                        Log.e("Asfasfaf", jsonObject1.getString("OrdDate"));
                                        String[] strMonthSplit = jsonObject1.getString("OrdDate").split("-");

                                        String strMOnthName = monthNames[Integer.parseInt(strMonthSplit[1])];
                                        String strYear = (strMonthSplit[0]);
                                        txtThirdMonthName.setText(strMOnthName + " " + strYear);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (NumberFormatException e) {
                                        e.printStackTrace();
                                    }
                                    ThirdMonthModal thirdMonthModal = new ThirdMonthModal();
                                    thirdMonthModal.setDate(jsonObject1.getString("OrdDate"));
                                    thirdMonthModal.setCoppies(jsonObject1.getString("PaidCopy"));
                                    thirdMonthcopiesList.add(thirdMonthModal);

                                } else if (strGetMonth.equals(strLastMonth)) {

                                    try {
                                        Log.e("Asfasfaf", jsonObject1.getString("OrdDate"));
                                        String[] strMonthSplit = jsonObject1.getString("OrdDate").split("-");
                                        String strMOnthName = monthNames[Integer.parseInt(strMonthSplit[1])];
                                        String strYear = (strMonthSplit[0]);
                                        txtFirstSecondName.setText(strMOnthName + " " + strYear);

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (NumberFormatException e) {
                                        e.printStackTrace();
                                    }
                                    SecondMonthModal secondMonthModal = new SecondMonthModal();
                                    secondMonthModal.setDate(jsonObject1.getString("OrdDate"));
                                    secondMonthModal.setCoppies(jsonObject1.getString("PaidCopy"));
                                    secondMonthcopiesList.add(secondMonthModal);

                                } else if (strGetMonth.equals(strSecondLastMonth)) {
                                    try {
                                        String[] strMonthSplit = jsonObject1.getString("OrdDate").split("-");

                                        String strMOnthName = monthNames[Integer.parseInt(strMonthSplit[1])];
                                        String strYear = (strMonthSplit[0]);
                                        txtFirstMonthName.setText(strMOnthName + " " + strYear);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (NumberFormatException e) {
                                        e.printStackTrace();
                                    }

                                    LastThreeMonthCopiesModal lastThreeMonthCopiesModal = new LastThreeMonthCopiesModal();
                                    lastThreeMonthCopiesModal.setDate(jsonObject1.getString("OrdDate"));
                                    lastThreeMonthCopiesModal.setCoppies(jsonObject1.getString("PaidCopy"));
                                    copiesdetailList.add(lastThreeMonthCopiesModal);
                                }


                                else if (strGetMonth.equals(strThirdLastMonth)) {
                                    try {
                                        String[] strMonthSplit = jsonObject1.getString("OrdDate").split("-");

                                        String strMOnthName = monthNames[Integer.parseInt(strMonthSplit[1])];
                                        String strYear = (strMonthSplit[0]);
                                        txtFourthMonthName.setText(strMOnthName + " " + strYear);
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    } catch (NumberFormatException e) {
                                        e.printStackTrace();
                                    }

                                    FourthMonthModal fourthMonthModal = new FourthMonthModal();
                                    fourthMonthModal.setDate(jsonObject1.getString("OrdDate"));
                                    fourthMonthModal.setCoppies(jsonObject1.getString("PaidCopy"));
                                    fourthMonthModalList.add(fourthMonthModal);
                                }
                            }

                            //  Log.e("dfgdfgdfgdf", copiesdetailList.size() + "");

                            recyclerview.setHasFixedSize(true);
                            GridLayoutManager linearLayoutManager = new GridLayoutManager(BillingDetailsActivity.this, 7);
                            recyclerview.setLayoutManager(linearLayoutManager);
                            CopiesAdapter copiesAdapter = new CopiesAdapter(copiesdetailList, BillingDetailsActivity.this);
                            recyclerview.setAdapter(copiesAdapter);

                            if(copiesdetailList.size()==0){
                                recyclerview.setVisibility(View.GONE);
                                txtFirstMonthName.setVisibility(View.GONE);

                            }


                            recyclerviewSecond.setHasFixedSize(true);
                            recyclerviewSecond.setLayoutManager(new GridLayoutManager(BillingDetailsActivity.this, 7));
                            SecondMonthAdapter secondMonthAdapter = new SecondMonthAdapter(secondMonthcopiesList, BillingDetailsActivity.this);
                            recyclerviewSecond.setAdapter(secondMonthAdapter);

                            if(secondMonthcopiesList.size()==0){
                                recyclerviewSecond.setVisibility(View.GONE);
                                txtFirstSecondName.setVisibility(View.GONE);

                            }

                            recyclerviewThird.setHasFixedSize(true);
                            recyclerviewThird.setLayoutManager(new GridLayoutManager(BillingDetailsActivity.this, 7));
                            ThirMonthAdapter thirMonthAdapter = new ThirMonthAdapter(thirdMonthcopiesList, BillingDetailsActivity.this);
                            recyclerviewThird.setAdapter(thirMonthAdapter);
                            if(thirdMonthcopiesList.size()==0){
                                recyclerviewThird.setVisibility(View.GONE);
                                txtThirdMonthName.setVisibility(View.GONE);

                            }

                            recyclerviewFourth.setHasFixedSize(true);
                            recyclerviewFourth.setLayoutManager(new GridLayoutManager(BillingDetailsActivity.this, 7));
                            FourthMonthAdapter fourthMonthAdapter = new FourthMonthAdapter(fourthMonthModalList, BillingDetailsActivity.this);
                            recyclerviewFourth.setAdapter(fourthMonthAdapter);

                            if(fourthMonthModalList.size()==0){

                                recyclerviewFourth.setVisibility(View.GONE);
                                txtFourthMonthName.setVisibility(View.GONE);
                            }


                            dismissProgressBar();
                        } catch (Exception ex) {
                            dismissProgressBar();
                            Log.e("asfasfafa", ex.getMessage());
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        dismissProgressBar();
                        Log.e("asfasfafa", anError.getMessage());
                    }
                });
    }
}