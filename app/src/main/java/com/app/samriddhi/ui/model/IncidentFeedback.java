package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IncidentFeedback extends BaseModel {

    @SerializedName("Incident_Number")
    @Expose
    private Integer incidentNumber;
    @SerializedName("IncidentFeedback_Number")
    @Expose
    private Integer incidentFeedbackNumber;
    @SerializedName("IncidentFeedback_Rating")
    @Expose
    private Integer incidentFeedbackRating;
    @SerializedName("IncidentFeedback_Text")
    @Expose
    private String incidentFeedbackText;
    @SerializedName("IncidentFeedback_By")
    @Expose
    private String incidentFeedbackBy;
    @SerializedName("IncidentFeedback_On")
    @Expose
    private String incidentFeedbackOn;

    public Integer getIncidentNumber() {
        return incidentNumber;
    }

    public void setIncidentNumber(Integer incidentNumber) {
        this.incidentNumber = incidentNumber;
    }

    public Integer getIncidentFeedbackNumber() {
        return incidentFeedbackNumber;
    }

    public void setIncidentFeedbackNumber(Integer incidentFeedbackNumber) {
        this.incidentFeedbackNumber = incidentFeedbackNumber;
    }

    public Integer getIncidentFeedbackRating() {
        return incidentFeedbackRating;
    }

    public void setIncidentFeedbackRating(Integer incidentFeedbackRating) {
        this.incidentFeedbackRating = incidentFeedbackRating;
    }

    public String getIncidentFeedbackText() {
        return incidentFeedbackText;
    }

    public void setIncidentFeedbackText(String incidentFeedbackText) {
        this.incidentFeedbackText = incidentFeedbackText;
    }

    public String getIncidentFeedbackBy() {
        return incidentFeedbackBy;
    }

    public void setIncidentFeedbackBy(String incidentFeedbackBy) {
        this.incidentFeedbackBy = incidentFeedbackBy;
    }

    public String getIncidentFeedbackOn() {
        return incidentFeedbackOn;
    }

    public void setIncidentFeedbackOn(String incidentFeedbackOn) {
        this.incidentFeedbackOn = incidentFeedbackOn;
    }
}
