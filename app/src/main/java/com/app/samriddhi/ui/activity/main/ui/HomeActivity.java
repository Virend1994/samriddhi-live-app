package com.app.samriddhi.ui.activity.main.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.databinding.DataBindingUtil;

import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.databinding.ActivityHomeBinding;
import com.app.samriddhi.events.CoachMarksEvent;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.fragment.DashboardFragment;
import com.app.samriddhi.ui.fragment.NavFragment;
import com.google.android.material.textview.MaterialTextView;

import org.greenrobot.eventbus.EventBus;


public class HomeActivity extends BaseActivity implements View.OnClickListener {
    private static final  String HASSHOT="hasShot";
    ActivityHomeBinding mBinding;
    String hintCount;
    Toolbar toolbar;
   public static TextView txt_notification_count;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        txt_notification_count=findViewById(R.id.txt_notification_count);
        initToolbar();


    }

    /**
     * initialize the home screen toolbar
     */

    private void initToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu_black_24dp);
        replaceFragment(R.id.nav_view, new NavFragment());
        replaceFragment(R.id.frame_container, new DashboardFragment());
        setTitle(getResources().getString(R.string.str_dashboard_tittle));
        setLogoOnLanguageChange();
        mBinding.layoutToolbar.txtHintCounts.setOnClickListener(this);
        mBinding.layoutToolbar.rlNotification.setOnClickListener(this);
    }

    /**
     * set the app language after logout from the app
     */
    private void setLogoOnLanguageChange() {
        Log.e("language=== ", "" + PreferenceManager.getAppLang(this));
        if (PreferenceManager.getAppLang(this).equalsIgnoreCase("GU"))
            mBinding.layoutToolbar.imgLogo.setImageResource(R.drawable.logo_gujrati);
        else if (PreferenceManager.getAppLang(this).equalsIgnoreCase("MR"))
            mBinding.layoutToolbar.imgLogo.setImageResource(R.drawable.logo_marathi);
        else if (PreferenceManager.getAppLang(this).equalsIgnoreCase("Hi"))
            mBinding.layoutToolbar.imgLogo.setImageResource(R.drawable.logo_hindi);
        else
            mBinding.layoutToolbar.imgLogo.setImageResource(R.drawable.logo_english);


    }

    /**
     * provides the guidance textview id
     * @return the text view
     */
    public MaterialTextView getHintId() {
        return mBinding.layoutToolbar.txtHintCounts;
    }


    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (mBinding.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
                mBinding.drawerLayout.closeDrawer(Gravity.LEFT);
            } else {
                mBinding.drawerLayout.openDrawer(Gravity.LEFT);
            }
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Close the drawer menu
     */
    public void closeDrawer() {
        if (mBinding.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mBinding.drawerLayout.closeDrawer(Gravity.LEFT);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_hint_counts:
                showCoachMarks();

//                if (!hintCount.isEmpty()) {
//                    startActivityAnimation(this, AppHintsActivity.class, false);
//                }
                break;
            case R.id.rl_notification:
                startActivityAnimation(this, NotificationActivity.class, false);
                break;
            default:
                break;
        }

    }

    /**
     * Sow the coach marks on all screens after clicking on Guide button
     */

    private void showCoachMarks() {
        SharedPreferences internal = getSharedPreferences("showcase_internal", Context.MODE_PRIVATE);
        internal.edit().putBoolean(HASSHOT + PreferenceManager.PREF_DASHBOARD_SHOT, false).apply();
        internal.edit().putBoolean(HASSHOT + PreferenceManager.PREF_BILLING_SHOT, false).apply();
        internal.edit().putBoolean(HASSHOT + PreferenceManager.PREF_LEDGER_SHOT, false).apply();
        internal.edit().putBoolean(HASSHOT + PreferenceManager.PREF_NO_OF_COPIES_SHOT, false).apply();
        internal.edit().putBoolean(HASSHOT + PreferenceManager.PREF_NO_OF_COPIES_FILTER_SHOT, false).apply();
        internal.edit().putBoolean(HASSHOT + PreferenceManager.PREF_INVOICE_SHOT, false).apply();
        EventBus.getDefault().post(new CoachMarksEvent("coachmarks"));

    }

    /**
     * show the app toolbar notifications count and hint count on Guides
     * @param hintCount app hints count
     */
    public void setHintCount(String hintCount) {
        this.hintCount = hintCount;
        // mBinding.layoutToolbar.setHintCount(hintCount.isEmpty() ? 0 : Integer.parseInt(hintCount));
        mBinding.layoutToolbar.setNotificationCount(PreferenceManager.getNotificationCount(this));

    }

}
