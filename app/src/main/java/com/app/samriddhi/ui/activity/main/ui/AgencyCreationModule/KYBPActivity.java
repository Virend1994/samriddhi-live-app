package com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.app.samriddhi.R;
import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.Child_EducationAdapter;
import com.app.samriddhi.base.adapter.NewsPaperAdapterList;
import com.app.samriddhi.base.adapter.RecyclerViewArrayAdapter;
import com.app.samriddhi.databinding.ActivityKybpBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AgencyCloseActivity;
import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AppsContants;
import com.app.samriddhi.ui.activity.main.ui.MisEntry.MisEntryActivity;
import com.app.samriddhi.ui.model.AgencyChildSet;
import com.app.samriddhi.ui.model.AgencyOtherNewsPaperSet;
import com.app.samriddhi.ui.model.AgencyRequestKYBPSetModel;
import com.app.samriddhi.ui.model.BankListModel;
import com.app.samriddhi.ui.model.DropDownModel;
import com.app.samriddhi.ui.model.NewsPaperModel;
import com.app.samriddhi.util.AlertUtility;
import com.app.samriddhi.util.Globals;
import com.app.samriddhi.util.Util;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.github.chrisbanes.photoview.PhotoView;
import com.github.dhaval2404.imagepicker.ImagePicker;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;
import com.iceteck.silicompressorr.SiliCompressor;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KYBPActivity extends BaseActivity implements NewsPaperAdapterList.OnMeneuClickListnser, RecyclerViewArrayAdapter.OnItemClickListener<BankListModel>, TextWatcher {
    ActivityKybpBinding mBinding;
    private int[] layouts;
    private String dropSelectType = "";
    private int currentPage = 0;
    Handler handler = new Handler();
    RadioButton relation_male, an_yes, an_no, hindu_rb, shikh_rb, other_rb, muslim_rb, relation_female, relation_other, Rdrelation_yes, Rdrelation_no;

    String strMrType = "Mr";
    KYBPActivity checkContext;
    AlertDialog alertDialog;

    boolean childIdCheck = false;
    Bitmap bitmap = null;
    Bitmap bitmap2 = null;
    Bitmap bitmap3 = null;

    StringBuilder stringBuilder;
    URL url;
    Runnable refresh;
    int agentAge = 0;
    private static final int CAMERA_REQUEST = 0;
    String panCardBitmap = "", Nominee_relation = "", addharCardBitMap = "", bankPassBookBitmap = "";
    ImageView pan_gal, pan_cam, aadhar_gal, aadhar_cam, bank_gal, bank_cam;
    private String imgaeType = "", marital_status_valpk = "", kybp_gendertype = "", agency_newspaper = "", kybp_religiontype = "", relation_yesno = "", relation_gender = "", residance_city_val = "", residance_state_val = "", bisiness_city_val = "", bisiness_state_val = "", marital_status_val = "",
            noofchild_val = "", child_gender_val = "", nominee_name = "", noofnewspaper_val = "", newspapaper_name_val = "", city_val = "", relationship_val = "",
            add_proof_type_val = "", noofcharidproof = "", bankPassbook_val = "", relative_state = "", relative_city = "", designation = "", department = "";
    MaterialButton save_kybp_data;
    Context context;

    String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
    Globals g = Globals.getInstance(context);
    List<String> list = new ArrayList<String>();

    String kybp_id = "";
    Spinner marital_status, bankPassbook;

    TextView noofchild;


    TextView tvNomineeName, agent_dob, spousedob, anneversiry, ag_father_dob, ag_mother_dob, ag_brother_dob, ag_sister_dob,
            residance_state, residance_city, bisiness_city, bisiness_state, relationship, state_rel, unit_rel,
            aadhar_spinner, desig, depart, state_business, city_business;

    EditText firstname, nomi_name, middlename, lastname, kybp_agency_name, residance_flat, residance_buiding, residance_street, residance_pin,
            residance_mobile, residance_whatsapp, residance_email, bisiness_flat, bisiness_buiding, bisiness_street, bisiness_pin, bisiness_mobile,
            bisiness_whatsapp, bisiness_email, spousename, ag_father_name, other_religion,
            ag_mother_name, ag_brother_name, ag_sister_name, ac_holder, account_no, bank_branch,
            bank_address, ifsc, gstn, fullname, employee, ac_number, pancard, aadharProof, editLocation;
    TextView noofnewspaper;
    ImageView panImg;

    CheckBox checkbox;
    int idProof = 0;
    LinearLayout marital_lay;
    TextView newPaperList;
    RecyclerView child_recycler;
    String[] multi_selected_value;
    public ArrayList<String> childList = new ArrayList<String>();
    public ArrayList<DropDownModel> stateMasterArray;
    public ArrayList<DropDownModel> cityMasterArray;


    Child_EducationAdapter ChildEducatinAdapter;
    public ArrayList<DropDownModel> relativeUnitMasterArray;
    RadioButton male_kybp_rb, female_kybp_rb, other_kybp_rb;
    RadioButton radio_ms, radio_mr, radio_mrs, radio_msboth;
    public ArrayList<DropDownModel> relationMasterArray;
    LinearLayout relative;
    public ArrayList<DropDownModel> AddressProofMasterArray;
    public ArrayList<AgencyChildSet> agencyChildSetModelList;
    public ArrayList<AgencyOtherNewsPaperSet> agencyOtherNewsPaperSetModelList;

    private String newListName = "", strGetGenderType = "Mr";
    private static int formStatus = 0, childselect = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_kybp);
        stringBuilder = new StringBuilder(newListName);

        mBinding.toolbar.setTitle("KYBP Form ");
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        context = this;
        checkContext = this;

        stateMasterArray = new ArrayList<>();
        cityMasterArray = new ArrayList<>();
        relativeUnitMasterArray = new ArrayList<>();
        relationMasterArray = new ArrayList<>();
        AddressProofMasterArray = new ArrayList<>();
        //    nomineeArray=new ArrayList<>();
        agencyChildSetModelList = new ArrayList<>();
        agencyOtherNewsPaperSetModelList = new ArrayList<>();

        initeView();
        getMaritialStatus();

        if (g.KYBPStatus == 1) {
            formStatus = 1;
            getKYBPData();
        } else {
        }

    }

    @Override
    public boolean onSupportNavigateUp() {


        showBackAlert();

        return true;
    }

    public void showBackAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(KYBPActivity.this);

        View view1 = LayoutInflater.from(KYBPActivity.this).inflate(R.layout.back_alert_layout, null);

        MaterialButton btnNo = view1.findViewById(R.id.btnNo);
        MaterialButton btnYes = view1.findViewById(R.id.btnYes);

        btnYes.setOnClickListener(v -> {
            alertDialog.dismiss();
            alertDialog.cancel();
            finish();
        });
        btnNo.setOnClickListener(V -> {
            alertDialog.dismiss();
            alertDialog.cancel();
        });
        builder.setView(view1);
        alertDialog = builder.create();
        alertDialog.show();
    }

    public void initeView() {
        addBottomDots(currentPage);
        layouts = new int[]{
                R.layout.business_appointment_one,
                R.layout.business_appointment_two,
                R.layout.business_appoinment_three
        };
        MyViewPagerAdapter myViewPagerAdapter = new MyViewPagerAdapter();
        mBinding.viewPager.setAdapter(myViewPagerAdapter);
        mBinding.viewPager.setOffscreenPageLimit(layouts.length);

        mBinding.viewPager.addOnPageChangeListener(pageChangeListener);
        getRelationShip();
        getAddressProof();


    }

    @Override
    public void onBackPressed() {


        showBackAlert();


    }

    private void addBottomDots(int currentPage) {
        this.currentPage = currentPage;
    }

    private ViewPager.OnPageChangeListener pageChangeListener =
            new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageSelected(int position) {
                    addBottomDots(position);
                }

                @Override
                public void onPageScrolled(int arg0, float arg1, int arg2) {
                }

                @Override
                public void onPageScrollStateChanged(int arg0) {
                }
            };

    @Override
    public void onCheckValue(ArrayList<NewsPaperModel> liveTest) {

        int count = 0;
        StringBuilder newsName = new StringBuilder();
        agencyOtherNewsPaperSetModelList.clear();

        for (int i = 0; i < liveTest.size(); i++) {


            if (liveTest.get(i).isStatus()) {

                AgencyOtherNewsPaperSet obj = new AgencyOtherNewsPaperSet();
                newsName.append(liveTest.get(i).getPaperName() + " ");

                obj.setNewspaper_id(liveTest.get(i).getPk());

                count++;
                agencyOtherNewsPaperSetModelList.add(obj);
            } else {

            }

        }
        noofnewspaper_val = String.valueOf(count);
        noofnewspaper.setText(String.valueOf(count));
        newPaperList.setText(newsName.toString());


        //  Toast.makeText(context,"Name "+newsName,Toast.LENGTH_SHORT).show();

    }


    private class MyViewPagerAdapter extends PagerAdapter {
        MyViewPagerAdapter() {
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            View v = LayoutInflater.from(KYBPActivity.this)
                    .inflate(layouts[position], container, false);

            switch (position) {
                case 0:
                    RadioGroup rg = (RadioGroup) v.findViewById(R.id.kybp_gender_rg);
                    kybp_gendertype = "M";
                    male_kybp_rb = v.findViewById(R.id.male_kybp_rb);
                    female_kybp_rb = v.findViewById(R.id.female_kybp_rb);
                    other_kybp_rb = v.findViewById(R.id.other_kybp_rb);
                    rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            switch (checkedId) {
                                case R.id.male_kybp_rb:
                                    kybp_gendertype = "M";
                                    radio_mr.setChecked(true);
                                    radio_ms.setClickable(false);
                                    radio_mrs.setClickable(false);
                                    radio_msboth.setClickable(false);
                                    break;
                                case R.id.female_kybp_rb:
                                    radio_mr.setClickable(false);
                                    radio_ms.setChecked(true);
                                    radio_ms.setClickable(true);
                                    radio_mrs.setClickable(true);
                                    radio_msboth.setClickable(true);
                                    kybp_gendertype = "F";
                                    break;
                                case R.id.other_kybp_rb:
                                    //kybp_gendertype = "O";
                                    kybp_gendertype = "O";
                                    radio_mr.setClickable(false);
                                    radio_ms.setClickable(false);
                                    radio_mrs.setClickable(false);
                                    radio_msboth.setClickable(true);
                                    radio_msboth.setChecked(true);
                                    break;
                            }
                        }
                    });

                    RadioGroup kybp_Mr_group = (RadioGroup) v.findViewById(R.id.kybp_Mr_group);
                    radio_ms = v.findViewById(R.id.radio_ms);
                    radio_mr = v.findViewById(R.id.radio_mr);
                    radio_mrs = v.findViewById(R.id.radio_mrs);
                    radio_msboth = v.findViewById(R.id.radio_msboth);

                    kybp_Mr_group.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            switch (checkedId) {
                                case R.id.radio_mr:
                                    strMrType = "Mr";
                                    break;
                                case R.id.radio_ms:
                                    strMrType = "Ms";
                                    break;
                                case R.id.radio_mrs:
                                    strMrType = "Mrs";
                                    break;

                                case R.id.radio_msboth:
                                    strMrType = "M/s";
                                    break;
                            }
                        }
                    });


                    editLocation = v.findViewById(R.id.editLocation);
                    firstname = v.findViewById(R.id.first_name);
                    middlename = v.findViewById(R.id.middle_name);
                    lastname = v.findViewById(R.id.last_name);
                    kybp_agency_name = v.findViewById(R.id.kybp_agency_name);
                    residance_flat = v.findViewById(R.id.flat_resi);
                    residance_buiding = v.findViewById(R.id.building_resi);
                    residance_street = v.findViewById(R.id.street_resi);
                    residance_city = v.findViewById(R.id.city_resi);
                    residance_state = v.findViewById(R.id.state_resi);
                    residance_pin = v.findViewById(R.id.pin_resi);
                    residance_mobile = v.findViewById(R.id.mobile_resi);
                    residance_whatsapp = v.findViewById(R.id.whatsapp_resi);
                    residance_email = v.findViewById(R.id.email_resi);
                    bisiness_flat = v.findViewById(R.id.flat_business);
                    bisiness_buiding = v.findViewById(R.id.building_business);
                    bisiness_street = v.findViewById(R.id.street_business);
                    bisiness_city = v.findViewById(R.id.city_business);
                    bisiness_state = v.findViewById(R.id.state_business);
                    bisiness_pin = v.findViewById(R.id.pin_business);
                    bisiness_mobile = v.findViewById(R.id.mobile_business);
                    bisiness_whatsapp = v.findViewById(R.id.whatsapp_business);
                    bisiness_email = v.findViewById(R.id.email_business);
                    state_business = v.findViewById(R.id.state_business);
                    city_business = v.findViewById(R.id.city_business);
                    checkbox = v.findViewById(R.id.checkBox2);

                    kybp_agency_name.setText(g.agencyNameStr);
                    firstname.setText(g.agentNameStr.trim());
                    residance_mobile.setText(g.agentMobileStr);


                    kybp_agency_name.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            g.mainAgencyName = kybp_agency_name.getText().toString();
                        }
                    });
                    getMasterState();

                    residance_state.setOnClickListener(vi -> {
                        dropSelectType = "resi_state";
                        residance_city.setText("");
                        Util.showDropDown(stateMasterArray, "Select State", KYBPActivity.this, this::onOptionClick);
                    });

                    residance_city.setOnClickListener(vi -> {
                        dropSelectType = "resi_city";
                        Util.showDropDown(cityMasterArray, "Select City", KYBPActivity.this, this::onOptionClick);
                    });

                    bisiness_state.setOnClickListener(vi -> {
                        dropSelectType = "b_state";
                        bisiness_city.setText("");
                        Util.showDropDown(stateMasterArray, "Select State", KYBPActivity.this, this::onOptionClick);
                    });

                    bisiness_city.setOnClickListener(vi -> {
                        dropSelectType = "b_city";
                        Util.showDropDown(cityMasterArray, "Select City", KYBPActivity.this, this::onOptionClick);
                    });


                    checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                            if (b) {

                                String resi_flat = residance_flat.getText().toString();
                                String resi_building = residance_buiding.getText().toString();
                                String resi_street = residance_street.getText().toString();
                                String resi_pin = residance_pin.getText().toString();
                                String resi_mob = residance_mobile.getText().toString();
                                String resi_whatsapp = residance_whatsapp.getText().toString();
                                String resi_email = residance_email.getText().toString();

                                bisiness_flat.setText(resi_flat);
                                bisiness_buiding.setText(resi_building);
                                bisiness_street.setText(resi_street);
                                bisiness_pin.setText(resi_pin);

                                bisiness_mobile.setText(resi_mob);
                                bisiness_whatsapp.setText(resi_whatsapp);
                                bisiness_email.setText(resi_email);

                                state_business.setText(residance_state.getText().toString());
                                city_business.setText(residance_city.getText().toString());

                                bisiness_city_val = residance_city_val;
                                bisiness_state_val = residance_state_val;

                            } else {

                                bisiness_flat.setText("");
                                bisiness_buiding.setText("");
                                bisiness_street.setText("");
                                bisiness_pin.setText("");
                                bisiness_mobile.setText("");
                                bisiness_whatsapp.setText("");
                                bisiness_email.setText("");
                                state_business.setText("");
                                city_business.setText("");

                                bisiness_flat.setHint(R.string.str_flat_no);
                                bisiness_buiding.setHint(R.string.str_build_society_no);
                                bisiness_street.setHint(R.string.str_street_name);
                                bisiness_pin.setHint(R.string.str_pin_code);
                                bisiness_mobile.setHint(R.string.str_mobile_no);
                                bisiness_whatsapp.setHint(R.string.str_whatsapp_no);
                                bisiness_email.setHint(R.string.email);
                            }
                        }
                    });

                    String email = bisiness_email.getText().toString().trim();
                    bisiness_email.addTextChangedListener(new TextWatcher() {
                        public void afterTextChanged(Editable s) {


                        }

                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                            // other stuffs
                        }

                        public void onTextChanged(CharSequence s, int start, int before, int count) {
                            // other stuffs
                        }
                    });


                    break;

                case 1:

                    child_recycler = v.findViewById(R.id.child_rec);


                    child_recycler.setHasFixedSize(true);
                    child_recycler.setNestedScrollingEnabled(true);
                    child_recycler.setLayoutManager(new LinearLayoutManager(KYBPActivity.this));
                    ChildEducatinAdapter = new Child_EducationAdapter(KYBPActivity.this, g.child_rec_list);
                    child_recycler.setAdapter(ChildEducatinAdapter);
                    ChildEducatinAdapter.notifyDataSetChanged();


                    RadioGroup rg1 = (RadioGroup) v.findViewById(R.id.kybp_religion_rg);
                    LinearLayout l1 = v.findViewById(R.id.other_religion);
                    kybp_religiontype = "Hindu";
                    hindu_rb = v.findViewById(R.id.hindu_rb);
                    shikh_rb = v.findViewById(R.id.shikh_rb);
                    muslim_rb = v.findViewById(R.id.muslim_rb);
                    other_rb = v.findViewById(R.id.other_rb);

                    rg1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            switch (checkedId) {
                                case R.id.hindu_rb:
                                    l1.setVisibility(View.GONE);
                                    kybp_religiontype = "Hindu";
                                    break;
                                case R.id.muslim_rb:
                                    l1.setVisibility(View.GONE);
                                    kybp_religiontype = "Muslim";
                                    break;
                                case R.id.shikh_rb:
                                    l1.setVisibility(View.GONE);
                                    kybp_religiontype = "Sikh";
                                    break;
                                case R.id.other_rb:
                                    kybp_religiontype = "Others";
                                    l1.setVisibility(View.VISIBLE);
                                    break;
                            }
                        }
                    });
                    tvNomineeName = v.findViewById(R.id.tvNominee_name);
                    tvNomineeName.setOnClickListener(vi -> {
                        dropSelectType = "nomi_name";
                        Util.showDropDown(relationMasterArray, "Select Nominee Relation", KYBPActivity.this, this::onOptionClick);

                    });
                    an_yes = v.findViewById(R.id.an_yes);
                    an_no = v.findViewById(R.id.an_no);
                    RadioGroup rg2 = (RadioGroup) v.findViewById(R.id.agency_newspaper_rg);
                    LinearLayout ag_nw = v.findViewById(R.id.agency_newspaper_yesno_layout);
                    agency_newspaper = "Yes";
                    ag_nw.setVisibility(View.VISIBLE);
                    rg2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            switch (checkedId) {
                                case R.id.an_yes:
                                    agency_newspaper = "Yes";
                                    ag_nw.setVisibility(View.VISIBLE);
                                    break;
                                case R.id.an_no:
                                    agency_newspaper = "No";
                                    ag_nw.setVisibility(View.GONE);
                                    break;

                            }
                        }
                    });


                    agent_dob = v.findViewById(R.id.agent_dob_kybp);
                    other_religion = v.findViewById(R.id.religion_edit);
                    marital_status = v.findViewById(R.id.marital_kybp);
                    spousename = v.findViewById(R.id.spouse_name_kybp);
                    spousedob = v.findViewById(R.id.spouse_dob_kybp);
                    anneversiry = v.findViewById(R.id.anneversiry_date_kybp);
                    noofchild = v.findViewById(R.id.noofchild_kybp);
                    ag_father_name = v.findViewById(R.id.agent_fname_kybp);
                    ag_father_dob = v.findViewById(R.id.father_dob);
                    ag_mother_name = v.findViewById(R.id.mother_name);
                    ag_mother_dob = v.findViewById(R.id.mother_dob);
                    ag_brother_name = v.findViewById(R.id.brother_name);
                    ag_brother_dob = v.findViewById(R.id.brother_dob);
                    ag_sister_name = v.findViewById(R.id.sister_name);
                    ag_sister_dob = v.findViewById(R.id.sister_dob);
                    nomi_name = v.findViewById(R.id.nominee_name);
                    noofnewspaper = v.findViewById(R.id.noofnewspaper);
                    marital_lay = v.findViewById(R.id.marital_layout);

                    newPaperList = v.findViewById(R.id.newPaperList);

                    newPaperList.setOnClickListener(v3 -> {
                        Util.showCheckBoxDropDown(g.newsPaperNameMasterArray, "Select News Paper", KYBPActivity.this, checkContext);
                    });

                    /*List<String> categories = new ArrayList<String>();
                    Log.e("size marrital", String.valueOf(g.getMaritialArray.size()));
                    for (int l = 0; l < g.getMaritialArray.size(); l++) {
                        categories.add(g.getMaritialArray.get(l).getDescription());
                    }
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, categories);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    marital_status.setAdapter(dataAdapter);*/

                    marital_status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                            // your code here

                            marital_status_val = marital_status.getSelectedItem().toString();
                            marital_status_valpk = g.getMaritialArray.get(position).getId();
                            g.getMaritialArray.get(position).getId();
                            Log.e("position", marital_status_valpk);
                            if (marital_status_val.equalsIgnoreCase("Married") || marital_status_val.equalsIgnoreCase("Divorced")) {
                                marital_lay.setVisibility(View.VISIBLE);
                            } else {
                                marital_lay.setVisibility(View.GONE);
                            }

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parentView) {
                            // your code here
                        }

                    });


                    List<String> child_number = new ArrayList<String>();
                    child_number.add("0");
                    child_number.add("1");
                    child_number.add("2");
                    child_number.add("3");
                    child_number.add("4");
                    child_number.add("5");


                    noofchild.setOnClickListener(v1 -> {


                        if (agent_dob.getText().length() == 0) {
                            alertMessage("Please first select agent dob");
                            return;

                        } else {
                            final CharSequence charSequence[] = child_number.toArray(new CharSequence[child_number.size()]);
                            AlertDialog.Builder builder = new AlertDialog.Builder(KYBPActivity.this);

                            View view1 = LayoutInflater.from(KYBPActivity.this).inflate(R.layout.spinnerlayout, null);


                            builder.setView(view1);
                            builder.setItems(charSequence, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    noofchild.setText(charSequence[i]);
                                    noofchild_val = noofchild.getText().toString();

                                    if (noofchild_val.equalsIgnoreCase("0")) {
                                        child_recycler.setVisibility(View.GONE);

                                    } else {
                                        g.child_rec_list.clear();
                                        childselect = 1;

                                        for (int k = 0; k < Integer.parseInt(noofchild_val); k++) {
                                            AgencyChildSet ob = new AgencyChildSet();
                                            ob.setChild_name("");
                                            ob.setChild_gender("");
                                            ob.setChild_DOB("");
                                            ob.setChild_educationName("");
                                            ob.setChild_education("");
                                            ob.setAg_child_id("");
                                            g.child_rec_list.add(ob);
                                            ChildEducatinAdapter.notifyDataSetChanged();
                                        }
                                        child_recycler.setVisibility(View.VISIBLE);
                                    }


                                }
                            });
                            alertDialog = builder.create();
                            alertDialog.show();

                        }


                    });


                    agent_dob.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            int mYear, mMonth, mDay;
                            final Calendar c = Calendar.getInstance();
                            mYear = c.get(Calendar.YEAR);
                            mMonth = c.get(Calendar.MONTH);
                            mDay = c.get(Calendar.DAY_OF_MONTH);


                            DatePickerDialog datePickerDialog = new DatePickerDialog(KYBPActivity.this,
                                    new DatePickerDialog.OnDateSetListener() {

                                        @Override
                                        public void onDateSet(DatePicker view, int year,
                                                              int monthOfYear, int dayOfMonth) {

                                            //  ppr_rcv_date_value = String.format("%02d/%02d/%02d", dayOfMonth,monthOfYear + 1,year);
                                            int ageMontYear = monthOfYear + 1;
                                            g.agentAge = getAge(year, ageMontYear, dayOfMonth);
                                            agentAge = getAge(year, ageMontYear, dayOfMonth);
                                            if (getAge(year, ageMontYear, dayOfMonth) > 18) {
                                                agent_dob.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);

                                            } else {
                                                alertMessage("Vendor Agent Should Be grater then 18 Yrs");
                                                agent_dob.setText("");

                                            }

                                        }
                                    }, mYear, mMonth, mDay);
                            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                            datePickerDialog.show();

                        }
                    });


                    spousedob.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            int mYear, mMonth, mDay;
                            final Calendar c = Calendar.getInstance();
                            mYear = c.get(Calendar.YEAR);
                            mMonth = c.get(Calendar.MONTH);
                            mDay = c.get(Calendar.DAY_OF_MONTH);


                            DatePickerDialog datePickerDialog = new DatePickerDialog(KYBPActivity.this,
                                    new DatePickerDialog.OnDateSetListener() {

                                        @Override
                                        public void onDateSet(DatePicker view, int year,
                                                              int monthOfYear, int dayOfMonth) {

                                            spousedob.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                                            //  ppr_rcv_date_value = String.format("%02d/%02d/%02d", dayOfMonth,monthOfYear + 1,year);


                                        }
                                    }, mYear, mMonth, mDay);
                            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

                            datePickerDialog.show();

                        }
                    });

                    anneversiry.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            int mYear, mMonth, mDay;
                            final Calendar c = Calendar.getInstance();
                            mYear = c.get(Calendar.YEAR);
                            mMonth = c.get(Calendar.MONTH);
                            mDay = c.get(Calendar.DAY_OF_MONTH);


                            DatePickerDialog datePickerDialog = new DatePickerDialog(KYBPActivity.this,
                                    new DatePickerDialog.OnDateSetListener() {

                                        @Override
                                        public void onDateSet(DatePicker view, int year,
                                                              int monthOfYear, int dayOfMonth) {

                                            anneversiry.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                                            //  ppr_rcv_date_value = String.format("%02d/%02d/%02d", dayOfMonth,monthOfYear + 1,year);


                                        }
                                    }, mYear, mMonth, mDay);
                            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());

                            datePickerDialog.show();

                        }
                    });


                    ag_father_dob.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            int mYear, mMonth, mDay;
                            final Calendar c = Calendar.getInstance();
                            mYear = c.get(Calendar.YEAR);
                            mMonth = c.get(Calendar.MONTH);
                            mDay = c.get(Calendar.DAY_OF_MONTH);


                            DatePickerDialog datePickerDialog = new DatePickerDialog(KYBPActivity.this,
                                    new DatePickerDialog.OnDateSetListener() {

                                        @Override
                                        public void onDateSet(DatePicker view, int year,
                                                              int monthOfYear, int dayOfMonth) {

                                            // ag_father_dob.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                                            //  ppr_rcv_date_value = String.format("%02d/%02d/%02d", dayOfMonth,monthOfYear + 1,year);

                                            int ageMontYear = monthOfYear + 1;

                                            if (getAge(year, ageMontYear, dayOfMonth) > 20) {
                                                ag_father_dob.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);

                                            } else {
                                                alertMessage("Father Age Should Be grater then Agent Age");
                                                ag_father_dob.setText("");

                                            }
                                        }
                                    }, mYear, mMonth, mDay);
                            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                            datePickerDialog.show();

                        }
                    });
                    ag_mother_dob.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            int mYear, mMonth, mDay;
                            final Calendar c = Calendar.getInstance();
                            mYear = c.get(Calendar.YEAR);
                            mMonth = c.get(Calendar.MONTH);
                            mDay = c.get(Calendar.DAY_OF_MONTH);


                            DatePickerDialog datePickerDialog = new DatePickerDialog(KYBPActivity.this,
                                    new DatePickerDialog.OnDateSetListener() {

                                        @Override
                                        public void onDateSet(DatePicker view, int year,
                                                              int monthOfYear, int dayOfMonth) {

                                            //ag_mother_dob.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                                            //  ppr_rcv_date_value = String.format("%02d/%02d/%02d", dayOfMonth,monthOfYear + 1,year);

                                            int ageMontYear = monthOfYear + 1;

                                            if (getAge(year, ageMontYear, dayOfMonth) > 20) {
                                                ag_mother_dob.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);

                                            } else {
                                                alertMessage("Mather Age Should Be grater then Agent Age");
                                                ag_mother_dob.setText("");

                                            }
                                        }
                                    }, mYear, mMonth, mDay);
                            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                            datePickerDialog.show();

                        }
                    });
                    ag_brother_dob.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            int mYear, mMonth, mDay;
                            final Calendar c = Calendar.getInstance();
                            mYear = c.get(Calendar.YEAR);
                            mMonth = c.get(Calendar.MONTH);
                            mDay = c.get(Calendar.DAY_OF_MONTH);


                            DatePickerDialog datePickerDialog = new DatePickerDialog(KYBPActivity.this,
                                    new DatePickerDialog.OnDateSetListener() {

                                        @Override
                                        public void onDateSet(DatePicker view, int year,
                                                              int monthOfYear, int dayOfMonth) {

                                            ag_brother_dob.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                                            //  ppr_rcv_date_value = String.format("%02d/%02d/%02d", dayOfMonth,monthOfYear + 1,year);


                                        }
                                    }, mYear, mMonth, mDay);
                            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                            datePickerDialog.show();

                        }
                    });

                    ag_sister_dob.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            int mYear, mMonth, mDay;
                            final Calendar c = Calendar.getInstance();
                            mYear = c.get(Calendar.YEAR);
                            mMonth = c.get(Calendar.MONTH);
                            mDay = c.get(Calendar.DAY_OF_MONTH);

                            DatePickerDialog datePickerDialog = new DatePickerDialog(KYBPActivity.this,
                                    new DatePickerDialog.OnDateSetListener() {


                                        @Override
                                        public void onDateSet(DatePicker view, int year,
                                                              int monthOfYear, int dayOfMonth) {
                                            ag_sister_dob.setText(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                                            //  ppr_rcv_date_value = String.format("%02d/%02d/%02d", dayOfMonth,monthOfYear + 1,year);
                                        }
                                    }, mYear, mMonth, mDay);
                            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                            datePickerDialog.show();

                        }
                    });


                    break;


                case 2:

                    RadioGroup relation_with_db_rg = (RadioGroup) v.findViewById(R.id.relation_with_db_rg);
                    relative = v.findViewById(R.id.relative_db);
                    Rdrelation_yes = v.findViewById(R.id.Rdrelation_yes);
                    Rdrelation_no = v.findViewById(R.id.Rdrelation_no);
                    relation_yesno = "1";
                    relative.setVisibility(View.VISIBLE);
                    relation_with_db_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        public void onCheckedChanged(RadioGroup group, int checkedId) {


                            switch (checkedId) {
                                case R.id.Rdrelation_yes:
                                    relative.setVisibility(View.VISIBLE);
                                    relation_yesno = "1";
                                    break;
                                case R.id.Rdrelation_no:
                                    relation_yesno = "0";
                                    relative.setVisibility(View.GONE);
                                    break;

                            }
                        }
                    });

                    relation_male = v.findViewById(R.id.relation_male);
                    relation_female = v.findViewById(R.id.relation_female);
                    relation_other = v.findViewById(R.id.relation_other);
                    RadioGroup relation_gender_rg = (RadioGroup) v.findViewById(R.id.relation_gender_rg);
                    relation_gender = "M";
                    relation_gender_rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            switch (checkedId) {
                                case R.id.relation_male:
                                    relation_gender = "M";
                                    break;
                                case R.id.relation_female:
                                    relation_gender = "F";
                                    break;

                                case R.id.relation_other:
                                    relation_gender = "O";
                                    break;
                            }
                        }
                    });
                    ac_holder = v.findViewById(R.id.ac_holder_name);
                    account_no = v.findViewById(R.id.ac_no);
                    bank_branch = v.findViewById(R.id.bank_branch);
                    bank_address = v.findViewById(R.id.bank_address);
                    ifsc = v.findViewById(R.id.ifsc_code);
                    gstn = v.findViewById(R.id.gstn_no);
                    fullname = v.findViewById(R.id.full_name);
                    employee = v.findViewById(R.id.emp_id);
                    desig = v.findViewById(R.id.designation);
                    depart = v.findViewById(R.id.department);
                    unit_rel = v.findViewById(R.id.unit_relative);
                    state_rel = v.findViewById(R.id.state_relative);
                    relationship = v.findViewById(R.id.relation);
                    pancard = v.findViewById(R.id.pan_card);
                    aadhar_spinner = v.findViewById(R.id.adhar_spin);
                    aadharProof = v.findViewById(R.id.aadhar_proof);
                    bankPassbook = v.findViewById(R.id.bank_passbook);
                    save_kybp_data = v.findViewById(R.id.save_kybp);
                    pan_gal = v.findViewById(R.id.pan_gallery);
                    pan_cam = v.findViewById(R.id.pan_camera);
                    aadhar_gal = v.findViewById(R.id.aadhar_gallery);
                    aadhar_cam = v.findViewById(R.id.aadhar_camera);
                    bank_gal = v.findViewById(R.id.bank_gallery);
                    bank_cam = v.findViewById(R.id.bank_camera);


                    state_rel.setOnClickListener(vi -> {
                        dropSelectType = "relative_state";
                        Util.showDropDown(stateMasterArray, "Select State", KYBPActivity.this, this::onOptionClick);
                    });
                    unit_rel.setOnClickListener(vi -> {
                        dropSelectType = "relative_city";
                        Util.showDropDown(relativeUnitMasterArray, "Select Unit", KYBPActivity.this, this::onOptionClick);
                    });
                    desig.setOnClickListener(vi -> {
                        dropSelectType = "relative_desig";
                        Util.showDropDown(g.designationMasterArray, "Select Designation", KYBPActivity.this, this::onOptionClick);
                    });

                    depart.setOnClickListener(vi -> {
                        dropSelectType = "relative_depart";
                        Util.showDropDown(g.departmentMasterArray, "Select Department", KYBPActivity.this, this::onOptionClick);
                    });
                    relationship.setOnClickListener(vi -> {
                        dropSelectType = "relation";
                        Util.showDropDown(relationMasterArray, "Select Relationship", KYBPActivity.this, this::onOptionClick);
                    });

                    aadhar_spinner.setOnClickListener(vi -> {
                        dropSelectType = "add_proof";
                        Util.showDropDown(AddressProofMasterArray, "Select Address Proof", KYBPActivity.this, this::onOptionClick);
                    });

                    List<String> bank_passbookList = new ArrayList<String>();
                    bank_passbookList.add("Bank Passbook");
                    bank_passbookList.add("Cancel Cheque");

                    ArrayAdapter<String> bank_passbook = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, bank_passbookList);
                    bank_passbook.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    bankPassbook.setAdapter(bank_passbook);

                    bankPassbook.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                            // your code here

                            bankPassbook_val = bankPassbook.getSelectedItem().toString();

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parentView) {
                            // your code here
                        }

                    });

                    save_kybp_data.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Map<String, String> params = new HashMap<>();
                            Log.e("yesdb", relation_yesno);
                            //Log.e("addharCardBitMap",addharCardBitMap);
                            //Log.e("bankPassBookBitmap",bankPassBookBitmap);


                        }
                    });


                    pan_gal.setOnClickListener(vv -> {
                        imgaeType = "1";
                        ImagePicker.Companion.with(checkContext)//Crop image(Optional), Check Customization for more option
                                .compress(1024)
                                .galleryOnly()
                                .maxResultSize(1080, 1080)//Crop square image, its same as crop(1f, 1f)//Final image size will be less than 1 MB(Optional)
                                .start(0);
                    });


                    aadhar_gal.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            imgaeType = "2";
                            ImagePicker.Companion.with(checkContext)//Crop image(Optional), Check Customization for more option
                                    .compress(1024)
                                    .galleryOnly()
                                    .maxResultSize(1080, 1080)//Crop square image, its same as crop(1f, 1f)//Final image size will be less than 1 MB(Optional)
                                    .start(0);
                        }
                    });


                    bank_gal.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            imgaeType = "3";
                            ImagePicker.Companion.with(checkContext)//Crop image(Optional), Check Customization for more option
                                    .compress(1024)
                                    .galleryOnly()
                                    .maxResultSize(1080, 1080)//Crop square image, its same as crop(1f, 1f)//Final image size will be less than 1 MB(Optional)
                                    .start(0);
                        }
                    });


                    break;
            }

            container.addView(v);
            return v;
        }

        private void onOptionClick(DropDownModel dataList) {
            if (dropSelectType.equalsIgnoreCase("resi_state")) {
                Util.hideDropDown();
                getCity(dataList.getId());
                notifyDataSetChanged();
                residance_state.setText(dataList.getDescription());
                residance_state_val = dataList.getId();

            }
            if (dropSelectType.equalsIgnoreCase("resi_city")) {
                Util.hideDropDown();
                residance_city.setText(dataList.getDescription());
                residance_city_val = dataList.getId();
            }
            if (dropSelectType.equalsIgnoreCase("relative_desig")) {
                Util.hideDropDown();
                desig.setText(dataList.getDescription());
                designation = dataList.getId();
            }
            if (dropSelectType.equalsIgnoreCase("relative_depart")) {
                Util.hideDropDown();
                depart.setText(dataList.getDescription());
                department = dataList.getId();
            }
            if (dropSelectType.equalsIgnoreCase("b_state")) {
                Util.hideDropDown();
                getCity(dataList.getId());
                notifyDataSetChanged();
                bisiness_state.setText(dataList.getDescription());
                bisiness_state_val = dataList.getId();
            }
            if (dropSelectType.equalsIgnoreCase("b_city")) {
                Util.hideDropDown();
                bisiness_city.setText(dataList.getDescription());
                bisiness_city_val = dataList.getId();
            }
            if (dropSelectType.equalsIgnoreCase("relative_city")) {
                Util.hideDropDown();
                unit_rel.setText(dataList.getDescription());
                relative_city = dataList.getId();
            }
            if (dropSelectType.equalsIgnoreCase("relative_state")) {
                Util.hideDropDown();
                getRelativeUnit(dataList.getId());
                notifyDataSetChanged();
                state_rel.setText(dataList.getDescription());
                relative_state = dataList.getId();
            }
            if (dropSelectType.equalsIgnoreCase("relation")) {
                Util.hideDropDown();
                relationship.setText(dataList.getDescription());
                relationship_val = dataList.getId();
            }

            if (dropSelectType.equalsIgnoreCase("nomi_name")) {
                Util.hideDropDown();
                tvNomineeName.setText(dataList.getDescription());
                Nominee_relation = dataList.getDescription();

            }
            if (dropSelectType.equalsIgnoreCase("add_proof")) {
                Util.hideDropDown();
                add_proof_type_val = dataList.getId();
                noofcharidproof = dataList.getName();
                aadharProof.setText("");
                //Toast.makeText(KYBPActivityBhs.this,noofcharidproof,Toast.LENGTH_SHORT).show();
                aadhar_spinner.setText(dataList.getDescription());
                aadharProof.setHint(dataList.getDescription() + " Number ");
                aadharProof.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Integer.parseInt(noofcharidproof))});


            }
        }

        @Override
        public int getCount() {
            return layouts.length;
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
            return view == obj;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, @NonNull Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    private int getAge(int year, int month, int day) {
        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.set(year, month, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }

        Integer ageInt = new Integer(age);
        int ageS = ageInt;

        return ageS;
    }

    public void addKyBpForm(AgencyRequestKYBPSetModel abc) {

        enableLoadingBar(true);
        Call<String> call;

        if (formStatus > 0) {
            call = SamriddhiApplication.getmInstance().getApiService().updateKYPRequest(kybp_id, abc);
        } else {
            call = SamriddhiApplication.getmInstance().getApiService().addKyBPForm(abc);
        }

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;


                    Log.e("dataShow", response.toString());


                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        // Toast.makeText(KYBPActivity.this, jsonObject.toString(), Toast.LENGTH_SHORT).show();

                        // String message = jsonObject.getString("message");

                        Toast.makeText(KYBPActivity.this, "Your Request Order SuccessFully generated", Toast.LENGTH_SHORT).show();
                        finish();


                        //  enableLoadingBar(false);
                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                    enableLoadingBar(false);

                } else {
                    enableLoadingBar(false);
                    try {
                        Toast.makeText(KYBPActivity.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        enableLoadingBar(false);
                        Toast.makeText(KYBPActivity.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(KYBPActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });

    }


    private void getRelativeUnit(String id) {
        enableLoadingBar(true);

        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getUnit(PreferenceManager.getAgentId(context), id);
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                enableLoadingBar(false);
                if (response.isSuccessful()) {

                    assert response.body() != null;

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");

                        if (status.equalsIgnoreCase("Success")) {
                            relativeUnitMasterArray.clear();
                            for (int i = 0; i < objData.length(); i++) {
                                JSONObject objStr = objData.getJSONObject(i);
                                DropDownModel dropDownModel = new DropDownModel();
                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("unit_name"));
                                relativeUnitMasterArray.add(dropDownModel);

                            }
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(KYBPActivity.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(KYBPActivity.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(KYBPActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });

    }

    public void getKYBPData() {
        enableLoadingBar(true);
        Log.e("requestId", g.agentId);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getKyBPForm(g.agentId);
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                enableLoadingBar(false);
                if (response.isSuccessful()) {

                    assert response.body() != null;

                    try {
                        JSONArray jsonArray = new JSONArray(response.body());


                        if (jsonArray.length() > 0) {


                            Log.e("jsonArray", jsonArray.toString());
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject obj = jsonArray.getJSONObject(i);
                                kybp_id = obj.getString("kybp_id");

                                firstname.setText(obj.getString("first_name"));
                                lastname.setText(obj.getString("last_name"));
                                middlename.setText(obj.getString("middle_name"));
                                kybp_agency_name.setText(obj.getString("agency_name"));
                                editLocation.setText(obj.getString("resi_location"));
                                residance_flat.setText(obj.getString("resi_houseno"));
                                residance_buiding.setText(obj.getString("resi_building"));
                                residance_street.setText(obj.getString("resi_street"));

                                kybp_gendertype = obj.getString("ag_gender");
                                // strMrType = obj.getString("ag_title");
                                strGetGenderType = obj.getString("ag_title");

                                residance_city.setText(obj.getString("resi_cityName"));
                                residance_city_val = obj.getString("resi_city");


                                residance_state.setText(obj.getString("resi_stateName"));
                                residance_state_val = obj.getString("resi_state");


                                residance_pin.setText(obj.getString("resi_pincode"));
                                residance_mobile.setText(obj.getString("resi_mono"));
                                residance_whatsapp.setText(obj.getString("resi_watsup"));
                                residance_email.setText(obj.getString("resi_email"));
                                bisiness_flat.setText(obj.getString("resi_houseno"));
                                bisiness_buiding.setText(obj.getString("busi_building"));

                                bisiness_city.setText(obj.getString("busi_cityName"));
                                bisiness_city_val = obj.getString("busi_city");

                                bisiness_state.setText(obj.getString("busi_stateName"));

                                bisiness_state_val = obj.getString("busi_state");

                                bisiness_pin.setText(obj.getString("busi_pincode"));

                                bisiness_street.setText(obj.getString("busi_street"));

                                bisiness_mobile.setText(obj.getString("busi_mono"));
                                bisiness_whatsapp.setText(obj.getString("busi_watsup"));
                                bisiness_email.setText(obj.getString("busi_email"));
                                agent_dob.setText(obj.getString("ag_dob").replace("T00:00:00", ""));
                                marital_status_valpk = obj.getString("ag_maritial_id");


                                if (obj.getString("ag_maritial_id").equalsIgnoreCase("1")) {
                                    marital_status.setSelection(0);

                                } else if (obj.getString("ag_maritial_id").equalsIgnoreCase("2")) {

                                    Log.e("sdfsdfsfsf", "DONE");
                                    marital_status.setSelection(1);
                                } else if (obj.getString("ag_maritial_id").equalsIgnoreCase("3")) {
                                    marital_status.setSelection(2);
                                }

                                spousename.setText(obj.getString("spouse_name"));


                                if (!obj.getString("spouse_dob").equalsIgnoreCase("null")) {
                                    spousedob.setText(obj.getString("spouse_dob").replace("T00:00:00", ""));
                                }
                                if (obj.getString("spouse_dob").equalsIgnoreCase("9999-12-31T00:00:00") || obj.getString("spouse_dob").equalsIgnoreCase("null")) {
                                    spousedob.setText("");
                                }

                                ag_father_name.setText(obj.getString("father_name"));

                                if (!obj.getString("father_dob").equalsIgnoreCase("null")) {
                                    ag_father_dob.setText(obj.getString("father_dob").replace("T00:00:00", ""));
                                }

                                if (obj.getString("father_dob").equalsIgnoreCase("9999-12-31T00:00:00") || obj.getString("father_dob").equalsIgnoreCase("null")) {
                                    ag_father_dob.setText("");
                                }


                                ag_mother_name.setText(obj.getString("mother_name"));

                                if (!obj.getString("mother_dob").equalsIgnoreCase("null")) {
                                    ag_mother_dob.setText(obj.getString("mother_dob").replace("T00:00:00", ""));
                                }

                                if (obj.getString("mother_dob").equalsIgnoreCase("9999-12-31T00:00:00") || obj.getString("mother_dob").equalsIgnoreCase("null")) {
                                    ag_mother_dob.setText("");
                                }


                                if (!obj.getString("ag_marrige_ani").equalsIgnoreCase("null")) {
                                    anneversiry.setText(obj.getString("ag_marrige_ani").replace("T00:00:00", ""));
                                }

                                if (obj.getString("ag_marrige_ani").equalsIgnoreCase("9999-12-31T00:00:00") || obj.getString("ag_marrige_ani").equalsIgnoreCase("null")) {
                                    anneversiry.setText("");
                                }

                                noofchild.setText(String.valueOf(obj.getInt("no_of_children")));


                                if (!obj.getString("brother_dob").equalsIgnoreCase("null")) {
                                    ag_brother_dob.setText(obj.getString("brother_dob").replace("T00:00:00", ""));
                                }
                                if (obj.getString("brother_dob").equalsIgnoreCase("9999-12-31T00:00:00") || obj.getString("brother_dob").equalsIgnoreCase("null")) {
                                    ag_brother_dob.setText("");
                                }
                                ag_brother_name.setText(obj.getString("brother_name"));

                                if (!obj.getString("sister_dob").equalsIgnoreCase("null")) {
                                    ag_sister_dob.setText(obj.getString("sister_dob").replace("T00:00:00", ""));
                                }
                                if (obj.getString("sister_dob").equalsIgnoreCase("9999-12-31T00:00:00") || obj.getString("sister_dob").equalsIgnoreCase("null")) {
                                    ag_sister_dob.setText("");
                                }

                                ag_sister_name.setText(obj.getString("sister_name"));
                                nomi_name.setText(obj.getString("nominee_name"));
                                tvNomineeName.setText(obj.getString("nominee_relation"));

                                Nominee_relation = obj.getString("nominee_relation");

//                                if(relationMasterArray.size()>0){
//                                    for(int p=0;p<relationMasterArray.size();p++){
//                                        if(relationMasterArray.get(p).getDescription().contains(obj.getString("nominee_relation")))
//
//                                    }
//                                }


                                ac_holder.setText(obj.getString("bank_holder_name"));
                                ac_holder.setText(obj.getString("bank_holder_name"));
                                account_no.setText(obj.getString("bank_acc_no"));
                                bank_branch.setText(obj.getString("bank_name_branch"));
                                bank_address.setText(obj.getString("bank_address"));
                                ifsc.setText(obj.getString("bank_IFSC"));

                                gstn.setText(obj.getString("GSTIN"));

                                if (obj.getString("ag_religion").equalsIgnoreCase("Hindu")) {
                                    hindu_rb.setChecked(true);
                                } else if (obj.getString("ag_religion").equalsIgnoreCase("Muslim")) {
                                    muslim_rb.setChecked(true);
                                } else if (obj.getString("ag_religion").equalsIgnoreCase("Sikh")) {
                                    shikh_rb.setChecked(true);
                                } else if (obj.getString("ag_religion").equalsIgnoreCase("Others")) {
                                    other_rb.setChecked(true);
                                }

                                for (int k = 0; k < AddressProofMasterArray.size(); k++) {

                                    Log.e("AddressProofMasterArray" + k, AddressProofMasterArray.get(k).getDescription());
                                    Log.e("add_proof_type_Name", obj.getString("add_proof_type_Name"));

                                    if (AddressProofMasterArray.get(k).getDescription().equalsIgnoreCase(obj.getString("add_proof_type_Name"))) {
                                        Log.e("namev", AddressProofMasterArray.get(k).getName());
                                        Log.e("getDescriptionc", AddressProofMasterArray.get(k).getDescription());
                                        noofcharidproof = AddressProofMasterArray.get(k).getName();
                                        aadharProof.setFilters(new InputFilter[]{new InputFilter.LengthFilter(Integer.parseInt(noofcharidproof))});

                                    }
                                }

                                aadharProof.setText(obj.getString("add_proof_no"));
                                aadhar_spinner.setText(obj.getString("add_proof_type_Name"));
                                if (obj.getString("add_proof_type_Name").length() > 0) {
                                    noofcharidproof = "0";
                                }
                                relation_yesno = obj.getString("ag_db_relative");
                                add_proof_type_val = obj.getString("add_proof_type");

                                if (obj.getString("ag_gender").equalsIgnoreCase("M")) {
                                    male_kybp_rb.setChecked(true);
                                } else if (obj.getString("ag_gender").equalsIgnoreCase("F")) {
                                    female_kybp_rb.setChecked(true);
                                } else if (obj.getString("ag_gender").equalsIgnoreCase("O")) {
                                    other_kybp_rb.setChecked(true);
                                }

                                if (relation_yesno.equalsIgnoreCase("1")) {

                                    Rdrelation_yes.setChecked(true);
                                    relative.setVisibility(View.VISIBLE);
                                    state_rel.setText(obj.getString("db_State_Name"));
                                    unit_rel.setText(obj.getString("db_cityName"));
                                    relative_city = obj.getString("db_city");
                                    relative_state = obj.getString("db_State_ID");
                                    fullname.setText(obj.getString("db_fullname"));
                                    employee.setText(obj.getString("db_empid"));
                                    relationship.setText(obj.getString("db_relation_Name"));
                                    relationship_val = obj.getString("db_relation");
                                    if (obj.getString("db_dept_Name").equalsIgnoreCase("") || obj.getString("db_dept_Name").equalsIgnoreCase("null")) {
                                        depart.setText("");
                                    } else {
                                        depart.setText(obj.getString("db_dept_Name"));
                                    }
                                    desig.setText(obj.getString("db_desig_Name").equalsIgnoreCase("null") ? "" : obj.getString("db_desig_Name"));

                                    designation = obj.getString("db_desig");
                                    department = obj.getString("db_dept");
                                    if (obj.getString("db_gender").equalsIgnoreCase("M")) {
                                        relation_male.setChecked(true);
                                        kybp_gendertype = "M";
                                    } else if (obj.getString("db_gender").equalsIgnoreCase("F")) {
                                        relation_female.setChecked(true);
                                        kybp_gendertype = "F";
                                    } else if (obj.getString("db_gender").equalsIgnoreCase("O")) {
                                        relation_other.setChecked(true);
                                        kybp_gendertype = "O";
                                    }


                                } else {

                                    Rdrelation_no.setChecked(true);
                                    relative.setVisibility(View.GONE);
                                }

                                pancard.setText(obj.getString("pan_no"));
                                RequestOptions options = new RequestOptions().placeholder(R.drawable.loader).error(R.drawable.logo);
                                String photo = "";


                                if (!obj.getString("pan_copy").equalsIgnoreCase("null")) {
                                    pan_cam.setVisibility(View.VISIBLE);
                                    Glide.with(context).asBitmap().load(obj.getString("pan_copy")).into(new CustomTarget<Bitmap>() {
                                        @Override
                                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                            panCardBitmap = Util.encodeImageToBase64(resource);
                                            pan_gal.setImageBitmap(resource);
                                            bitmap = resource;

                                        }

                                        @Override
                                        public void onLoadCleared(@Nullable Drawable placeholder) {
                                        }
                                    });


                                    pan_cam.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            ShowPopup(bitmap);
                                        }
                                    });


                                }
                                if (!obj.getString("add_proof_copy").equalsIgnoreCase("null")) {

                                    aadhar_cam.setVisibility(View.VISIBLE);
                                    Glide.with(context).asBitmap().load(obj.getString("add_proof_copy")).into(new CustomTarget<Bitmap>() {
                                        @Override
                                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                            addharCardBitMap = Util.encodeImageToBase64(resource);
                                            aadhar_gal.setImageBitmap(resource);
                                            bitmap2 = resource;
                                        }

                                        @Override
                                        public void onLoadCleared(@Nullable Drawable placeholder) {
                                        }
                                    });


                                    aadhar_cam.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

                                            ShowPopup(bitmap2);
                                        }
                                    });
                                    //Glide.with(context).load(obj.getString("add_proof_copy")).into(aadhar_gal);

                                }
                                if (!obj.getString("bank_copy").equalsIgnoreCase("null")) {

                                    bank_cam.setVisibility(View.VISIBLE);
                                    Log.e("sfsfsfsf", obj.getString("bank_copy"));
                                    // Glide.with(context).load(obj.getString("bank_copy")).into(bank_gal);
                                    Glide.with(context).asBitmap().load(obj.getString("bank_copy")).into(new CustomTarget<Bitmap>() {
                                        @Override
                                        public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                            bankPassBookBitmap = Util.encodeImageToBase64(resource);
                                            bank_gal.setImageBitmap(resource);
                                            bitmap3 = resource;
                                        }

                                        @Override
                                        public void onLoadCleared(@Nullable Drawable placeholder) {
                                        }
                                    });


                                    bank_cam.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {

                                            ShowPopup(bitmap3);
                                        }
                                    });


                                }

//                                Glide.with(context).load(obj.getString("add_proof_copy")).into(aadhar_gal);
//
//                                Glide.with(context).load(obj.getString("bank_copy")).into(bank_gal);

                                // Glide.with(context).load(obj.getString("bank_copy")).error(R.drawable.loader).diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true).apply(options).into(bank_gal);

                                JSONArray childArray = new JSONArray(obj.getString("Agency_Child_set"));

                                if (childArray.length() > 0) {
                                    g.child_rec_list.clear();
                                    child_recycler.setVisibility(View.VISIBLE);
                                    noofchild.setClickable(false);
                                    noofchild.setEnabled(false);
                                }

                                Log.e("childArray", childArray.toString());
                                noofchild_val = String.valueOf(childArray.length());
                                for (int k = 0; k < childArray.length(); k++) {

                                    JSONObject AgencyObj = childArray.getJSONObject(k);
                                    AgencyChildSet ob = new AgencyChildSet();
                                    ob.setChild_name(AgencyObj.getString("child_name"));
                                    ob.setChild_gender(AgencyObj.getString("child_gender"));
                                    ob.setChild_DOB(AgencyObj.getString("child_DOB").replace("T00:00:00", ""));
                                    ob.setChild_educationName(AgencyObj.getString("child_educationName"));
                                    ob.setChild_education(AgencyObj.getString("child_education"));
                                    ob.setAg_child_id(AgencyObj.getString("ag_child_id"));
                                    g.child_rec_list.add(ob);

                                    ChildEducatinAdapter.notifyDataSetChanged();


                                }


                                JSONArray AgencyOtherNewsPaperset = new JSONArray(obj.getString("Agency_OtherNewsPaper_set"));
                                noofnewspaper.setText(String.valueOf(AgencyOtherNewsPaperset.length()));


                                if (AgencyOtherNewsPaperset.length() == 0) {
                                    agency_newspaper = "No";
                                    an_no.setChecked(true);
                                } else {
                                    agency_newspaper = "Yes";
                                    an_yes.setChecked(true);
                                    agencyOtherNewsPaperSetModelList.clear();
                                    for (int k = 0; k < AgencyOtherNewsPaperset.length(); k++) {

                                        JSONObject AgencyObj = AgencyOtherNewsPaperset.getJSONObject(i);
                                        AgencyOtherNewsPaperSet objChild = new AgencyOtherNewsPaperSet();


                                        AgencyObj.getString("newspaper_idName");
                                        stringBuilder.append(AgencyObj.getString("newspaper_idName"));
                                        objChild.setNewspaper_id(AgencyObj.getString("newspaper_id"));


                                        agencyOtherNewsPaperSetModelList.add(objChild);
                                    }

                                    newPaperList.setText(stringBuilder.toString());


                                }


                            }
                        }


                        Log.e("tjioitjwotjowejtwe", strGetGenderType);
                        // strMrType="Mrs";
                        if (strGetGenderType.equalsIgnoreCase("Mr")) {
                            radio_mr.setChecked(true);
                            Log.e("dfjhaskfhaks 0", strGetGenderType);

                            strGetGenderType = "Mr";
                        } else if (strGetGenderType.equalsIgnoreCase("Ms")) {
                            radio_ms.setChecked(true);
                            Log.e("dfjhaskfhaks 1", strGetGenderType);

                            strGetGenderType = "Ms";
                        } else if (strGetGenderType.equalsIgnoreCase("Mrs")) {
                            radio_mrs.setChecked(true);
                            Log.e("dfjhaskfhaks 2", strGetGenderType);

                            strGetGenderType = "Mrs";
                        } else {
                            radio_msboth.setChecked(true);
                            strGetGenderType = "M/s";
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(KYBPActivity.this, "Data not found", Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        Toast.makeText(KYBPActivity.this, "Data not found", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(KYBPActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });


        strMrType = strGetGenderType;   /* jugaad */
    }


    public void ShowPopup(Bitmap bitmap) {

        View popupView = null;

        popupView = LayoutInflater.from(KYBPActivity.this).inflate(R.layout.enlarge_image, null);
        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0);

        ImageView download_Image = popupView.findViewById(R.id.download_Image);
        download_Image.setVisibility(View.VISIBLE);

        download_Image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                File pictureFile = getOutputMediaFile();
                if (bitmap == null) {
                    Log.e("Dsgsggsdgs", "Error creating media file, check storage permissions: ");// e.getMessage());
                    return;
                }
                try {
                    FileOutputStream fos = new FileOutputStream(pictureFile);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 90, fos);
                    fos.close();
                    Toast.makeText(checkContext, "Image saved in MyAppFilder under Storage", Toast.LENGTH_SHORT).show();

                    Log.e("Dsgsggsdgs", "File not found: " + pictureFile.toString());
                } catch (FileNotFoundException e) {
                    Log.e("Dsgsggsdgs", "File not found: " + e.getMessage());
                } catch (IOException e) {
                    Log.e("Dsgsggsdgs", "Error accessing file: " + e.getMessage());
                }

            }
        });

        ImageView closeImg = popupView.findViewById(R.id.closeImg);
        closeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();
            }
        });

        PhotoView imageView = popupView.findViewById(R.id.imageView);
        imageView.setImageBitmap(bitmap);

    }


    private File getOutputMediaFile() {
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.
        File mediaStorageDir = new File(Environment.getExternalStorageDirectory().getPath()
                + "/MyAppFolder");

        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        // Create a media file name
        String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmm").format(new Date());
        File mediaFile;
        String mImageName = "Kybp_img" + timeStamp + ".jpg";
        mediaFile = new File(mediaStorageDir.getPath() + File.separator + mImageName);
        return mediaFile;
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void getCity(String id) {

        Log.e("sfgdfsdfs", id);
        Log.e("sfgdfsdfs", PreferenceManager.getAgentId(context));

        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getCity(PreferenceManager.getAgentId(context), id);
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                enableLoadingBar(false);
                if (response.isSuccessful()) {

                    assert response.body() != null;

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");

                        if (status.equalsIgnoreCase("Success")) {
                            cityMasterArray.clear();
                            for (int i = 0; i < objData.length(); i++) {

                                JSONObject objStr = objData.getJSONObject(i);
                                DropDownModel dropDownModel = new DropDownModel();
                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("city_name"));
                                cityMasterArray.add(dropDownModel);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(KYBPActivity.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(KYBPActivity.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(KYBPActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });

    }

    public void getMasterState() {
        // Calling JSON
        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getStateMaster(PreferenceManager.getAgentId(context));
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                enableLoadingBar(false);
                if (response.isSuccessful()) {


                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {
                            stateMasterArray.clear();
                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("state_name"));
                                stateMasterArray.add(dropDownModel);
                            }
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(KYBPActivity.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(KYBPActivity.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(KYBPActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });

    }


    public void getAddressProof() {

        enableLoadingBar(true);
        // Calling JSON
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getAdressProof(PreferenceManager.getAgentId(context));
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    enableLoadingBar(false);

                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {
                            AddressProofMasterArray.clear();
                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();
                                dropDownModel.setId(objStr.getString("pk"));

                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("addressproof_name"));
                                dropDownModel.setName(objStr.getJSONObject("fields").getString("noofchar"));

                                AddressProofMasterArray.add(dropDownModel);
                            }
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(KYBPActivity.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(KYBPActivity.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(KYBPActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });

    }


    public void getRelationShip() {
        // Calling JSON
        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getRelationShip(PreferenceManager.getAgentId(context));
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                enableLoadingBar(false);
                if (response.isSuccessful()) {


                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            relationMasterArray.clear();
                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("relation"));
                                dropDownModel.setId(objStr.getString("pk"));
                                relationMasterArray.add(dropDownModel);
                            }
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(KYBPActivity.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(KYBPActivity.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(KYBPActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });

    }


    private void alertMessage(String message) {

        AlertUtility.showFormAlert(KYBPActivity.this, message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
    }

    private void openGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, 0);
    }


    private void openCamera() {
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    @Override
    public void onItemClick(View view, BankListModel object) {

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && data != null) {
//Image Uri will not be null for RESULT_OK

            Uri compressUri = getPickImageResultUri(this, data);
//You can get File object from intent
//Uri fileUri = data;
            File file = ImagePicker.Companion.getFile(data);

//You can also get File Path from intent
            String filePath = ImagePicker.Companion.getFilePath(data);

            if (imgaeType.equalsIgnoreCase("1")) {

                pan_gal.setImageURI(compressUri);
                try {
                    Bitmap selectedImage = SiliCompressor.with(checkContext).getCompressBitmap(filePath);
                    panCardBitmap = Util.encodeImageToBase64(selectedImage);
                } catch (Exception e) {

                }
            }
            if (imgaeType.equalsIgnoreCase("2")) {

                aadhar_gal.setImageURI(compressUri);

                try {
                    Bitmap selectedImage = SiliCompressor.with(checkContext).getCompressBitmap(filePath);
                    addharCardBitMap = Util.encodeImageToBase64(selectedImage);
                } catch (Exception e) {

                }
            }
            if (imgaeType.equalsIgnoreCase("3")) {
                bank_gal.setImageURI(compressUri);
                try {
                    Bitmap selectedImage = SiliCompressor.with(checkContext).getCompressBitmap(filePath);
                    bankPassBookBitmap = Util.encodeImageToBase64(selectedImage);
                } catch (Exception e) {

                }

            }

//You can get File object from intent

        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, ImagePicker.Companion.getError(data), Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show();
        }
    }


    public static Uri getPickImageResultUri(@NonNull Context context, @Nullable Intent data) {
        boolean isCamera = true;
        if (data != null && data.getData() != null) {
            String action = data.getAction();
            isCamera = action != null && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);


        }
        return isCamera || data.getData() == null ? getCaptureImageOutputUri(context) : data.getData();
    }

    public static Uri getCaptureImageOutputUri(@NonNull Context context) {
        Uri outputFileUri = null;
        File getImage = context.getExternalCacheDir();
        if (getImage != null) {

            Log.e("demobhsurl", getImage.getPath());
            outputFileUri = Uri.fromFile(new File(getImage.getPath(), "pickImageResult.jpeg"));

        }
        return outputFileUri;
    }


    public void onclick(View view) {
        switch (view.getId()) {
            case R.id.btn_next:
                mBinding.viewPager.setCurrentItem(currentPage - 1);
                Log.e("cuurentPage", String.valueOf(currentPage));

                if (currentPage == 0) {
                    showBackAlert();
                }
                if (currentPage + 1 == 1) {
                    mBinding.btnPrevious.setText("NEXT");
                }
                if (currentPage + 1 == 2) {
                    mBinding.btnPrevious.setText("NEXT");
                }

                if (currentPage + 1 == 3) {
                    mBinding.btnPrevious.setText("SAVE");
                    currentPage = 0;
                }

                break;
            case R.id.btn_previous:
                // mBinding.viewPager.setCurrentItem(currentPage + 1);
                Log.e("cuurentPagepre", String.valueOf(currentPage + 1));

                if (currentPage + 1 == 1) {
                    mBinding.btnPrevious.setText("NEXT");
                }
                if (currentPage + 1 == 2) {
                    mBinding.btnPrevious.setText("NEXT");
                }
                if (currentPage + 1 == 3) {

                    mBinding.btnPrevious.setText("SAVE");
                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                    String strUOHStatus = AppsContants.sharedpreferences.getString(AppsContants.UOHApproveStatus, "");
                    String SOHApproveStatus = AppsContants.sharedpreferences.getString(AppsContants.SOHApproveStatus, "");
                    String HoApproveStatus = AppsContants.sharedpreferences.getString(AppsContants.HoApproveStatus, "");


                    /*   if (PreferenceManager.getPrefUserType(this).equals("UH")) { *//* Do not show to Agent *//*
                        if (strUOHStatus.equals("1")) {
                            mBinding.btnPrevious.setVisibility(View.GONE);
                        } else {
                            mBinding.btnPrevious.setVisibility(View.VISIBLE);
                            mBinding.btnPrevious.setText("SAVE");
                        }
                    } else if (PreferenceManager.getPrefUserType(this).equals("SH")) { *//* Do not show to SOH *//*
                        if (SOHApproveStatus.equals("1")) {
                            mBinding.btnPrevious.setVisibility(View.GONE);
                        } else {
                            mBinding.btnPrevious.setVisibility(View.VISIBLE);
                            mBinding.btnPrevious.setText("SAVE");
                        }

                    } else if (PreferenceManager.getPrefUserType(this).equals("HO")) { *//* Do not show to HO *//*
                        if (HoApproveStatus.equals("1")) {
                            mBinding.btnPrevious.setVisibility(View.GONE);
                        } else {
                            mBinding.btnPrevious.setVisibility(View.VISIBLE);
                            mBinding.btnPrevious.setText("SAVE");
                        }
                    }*/

                }

                if (mBinding.btnPrevious.getText().equals("SAVE")) {

                    if (ac_holder.getText().toString().length() == 0) {
                        alertMessage("Please fill account holder name");
                        return;
                    }
                    if (account_no.getText().toString().length() == 0) {
                        alertMessage("Please fill bank account no.");
                        return;
                    }
                    if (bank_branch.getText().toString().length() == 0) {
                        alertMessage("Please fill bank branch name");
                        return;
                    }

                    if (bank_address.getText().toString().length() == 0) {
                        alertMessage("Please fill bank address name");
                        return;
                    }


                    if (ifsc.getText().toString().length() == 0 || ifsc.getText().toString().length() < 10) {

                        alertMessage("Please Enter IFSC CODE");
                        return;

                    }
                    if (relation_yesno.length() == 0) {
                        alertMessage("Do you have any relationship with DB?");
                        return;
                    }

                    if (relation_yesno.equalsIgnoreCase("1")) {

                        if (fullname.getText().length() == 0) {
                            alertMessage("Please enter the full name");
                            return;
                        }
                        if (employee.getText().length() == 0) {
                            alertMessage("Please enter the employee id");
                            return;
                        }

                        if (desig.getText().length() == 0) {
                            alertMessage("Please select employee designation");
                            return;
                        }
                        if (depart.getText().length() == 0) {
                            alertMessage("Please select employee department");
                            return;
                        }
                        if (unit_rel.getText().length() == 0) {
                            alertMessage("Please select employee unit");
                            return;
                        }

                        if (state_rel.getText().length() == 0) {
                            alertMessage("Please select employee state");
                            return;
                        }


                        if (relationship.getText().length() == 0) {
                            alertMessage("Please select employee relationship");
                            return;
                        }

                    }
                    if (pancard.getText().toString().length() == 0 || pancard.getText().toString().length() < 10) {

                        alertMessage("Please enter Pan card no.");
                        return;
                    }


                    if (panCardBitmap.length() == 0) {

                        alertMessage("Please upload agent’s PAN card image");
                        return;
                    }
                    if (aadhar_spinner.getText().toString().length() == 0) {
                        alertMessage("Please select address proof");
                        return;
                    }
                    Log.e("noofcharidproof---1",noofcharidproof);
                    Log.e("noofcharidproof---2",aadhar_spinner.getText().toString());
                    if(aadhar_spinner.getText().toString().equalsIgnoreCase("Aadhar Card") && aadharProof.getText().toString().length()!=Integer.parseInt(noofcharidproof))
                    {
                        alertMessage("Please enter valid Aadhar card no");
                        return;
                    }

                    if(aadhar_spinner.getText().toString().equalsIgnoreCase("Diving License") && aadharProof.getText().toString().length()!=Integer.parseInt(noofcharidproof))
                    {
                        alertMessage("Please enter valid Diving License no");
                        return;
                    }

                    if(aadhar_spinner.getText().toString().equalsIgnoreCase("Ration Card") && aadharProof.getText().toString().length()!=Integer.parseInt(noofcharidproof))
                    {
                        alertMessage("Please enter valid Ration Card no");
                        return;
                    }

                    if(aadhar_spinner.getText().toString().equalsIgnoreCase("Voter Id") && aadharProof.getText().toString().length()!=Integer.parseInt(noofcharidproof))
                    {
                        alertMessage("Please enter valid Voter Id no");
                        return;
                    }

                    if(aadhar_spinner.getText().toString().equalsIgnoreCase("Passport") && aadharProof.getText().toString().length()!=Integer.parseInt(noofcharidproof))
                    {
                        alertMessage("Please enter valid Passport no");
                        return;
                    }

                    if (aadhar_spinner.getText().toString().length() == 0) {
                        alertMessage("Please select address proof");
                        return;
                    }
                    Log.e("noofcharidproof---1",noofcharidproof);
                    Log.e("noofcharidproof---2",aadhar_spinner.getText().toString());
                    if(aadhar_spinner.getText().toString().equalsIgnoreCase("Aadhar Card") && aadharProof.getText().toString().length()!=Integer.parseInt(noofcharidproof))
                    {
                        alertMessage("Please enter valid Aadhar card no");
                        return;
                    }

                    if(aadhar_spinner.getText().toString().equalsIgnoreCase("Diving License") && aadharProof.getText().toString().length()!=Integer.parseInt(noofcharidproof))
                    {
                        alertMessage("Please enter valid Diving License no");
                        return;
                    }

                    if(aadhar_spinner.getText().toString().equalsIgnoreCase("Ration Card") && aadharProof.getText().toString().length()!=Integer.parseInt(noofcharidproof))
                    {
                        alertMessage("Please enter valid Ration Card no");
                        return;
                    }

                    if(aadhar_spinner.getText().toString().equalsIgnoreCase("Voter Id") && aadharProof.getText().toString().length()!=Integer.parseInt(noofcharidproof))
                    {
                        alertMessage("Please enter valid Voter Id no");
                        return;
                    }

                    if(aadhar_spinner.getText().toString().equalsIgnoreCase("Passport") && aadharProof.getText().toString().length()!=Integer.parseInt(noofcharidproof))
                    {
                        alertMessage("Please enter valid Passport no");
                        return;
                    }

                    if (aadharProof.getText().toString().length() == 0) {

                        alertMessage("Please enter Address proof no.");
                        return;
                    }

                    if (addharCardBitMap.length() == 0) {
                        alertMessage("Please upload agent’s Address Card Image");
                        return;
                    }
                    if (bankPassBookBitmap.length() == 0) {
                        alertMessage("Please upload agent’s " + bankPassbook_val + "  Image");
                        return;
                    }




                    AlertUtility.showAlert(this, "Are Sure want to save your KYBP form", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                            try {

                                agencyChildSetModelList.clear();

                                String first_name = firstname.getText().toString().trim();
                                String strLocation = editLocation.getText().toString();
                                String middle_name = middlename.getText().toString().trim();
                                String last_name = lastname.getText().toString().trim();
                                String agency_name = kybp_agency_name.getText().toString();
                                String resi_flat = residance_flat.getText().toString();
                                String resi_building = residance_buiding.getText().toString();
                                String resi_street = residance_street.getText().toString();
                                String resi_pin = residance_pin.getText().toString();
                                String resi_mob = residance_mobile.getText().toString();


                                String resi_whatsapp = "";
                                if (residance_whatsapp.getText().toString().length() == 0) {
                                    resi_whatsapp = "";
                                } else {
                                    resi_whatsapp = residance_whatsapp.getText().toString();
                                }
                                String resi_email = residance_email.getText().toString();
                                String bis_flat = bisiness_flat.getText().toString();

                                if (bis_flat.length() > 10) {

                                    alertMessage("Address should not be greater than 10 characters");
                                    return;
                                }
                                if (resi_flat.length() > 10) {

                                    alertMessage("Address should not be greater than 10 characters");
                                    return;
                                }
                                String bis_building = bisiness_buiding.getText().toString();
                                String bis_street = bisiness_street.getText().toString();
                                String bis_pin = bisiness_pin.getText().toString();
                                String bis_mobile = bisiness_mobile.getText().toString();

                                String bis_whatsapp = "";
                                if (bisiness_whatsapp.getText().toString().length() == 0) {
                                    bis_whatsapp = "";
                                } else {
                                    bis_whatsapp = bisiness_whatsapp.getText().toString();
                                }


                                String bis_email = bisiness_email.getText().toString();
                                String other_reli = other_religion.getText().toString();
                                String agentdob = agent_dob.getText().toString() + "T00:00:00";
                                String spouse_name = spousename.getText().toString();
                                String spouse_dob = spousedob.getText().toString() + "T00:00:00";
                                String anneversiry_date = anneversiry.getText().toString() + "T00:00:00";
                                String child_name = "";
                                String childob = "";
                                String child_edu = "";
                                String agency_father_name = ag_father_name.getText().toString();
                                String agency_father_dob = "";

                                if (ag_father_dob.getText().toString().length() == 0) {
                                    // agency_father_dob = "9999-12-31T00:00:00";
                                    agency_father_dob = null;
                                } else {
                                    agency_father_dob = ag_father_dob.getText().toString() + "T00:00:00";
                                }

//                    if(Nominee_relation.length()==0){
//                        alertMessage("Please Select Nominee");
//                        return;
//                    }


                                String mother_dob = "";
                                if (ag_mother_dob.getText().toString().length() == 0) {
                                    // mother_dob = "9999-12-31T00:00:00";
                                    mother_dob = null;
                                } else {
                                    mother_dob = ag_mother_dob.getText().toString() + "T00:00:00";
                                }

                                String bro_dob = "";
                                if (ag_brother_dob.getText().toString().length() == 0) {
                                    // bro_dob = "9999-12-31T00:00:00";
                                    bro_dob = null;
                                } else {
                                    bro_dob = ag_brother_dob.getText().toString() + "T00:00:00";
                                }
                                String sis_dob = "";
                                if (ag_sister_dob.getText().toString().length() == 0) {
                                    //  sis_dob = "9999-12-31T00:00:00";
                                    sis_dob = null;
                                } else {
                                    sis_dob = ag_sister_dob.getText().toString() + "T00:00:00";
                                }

                                String mother_name = ag_mother_name.getText().toString();

                                String bro_name = ag_brother_name.getText().toString();

                                String sis_name = ag_sister_name.getText().toString();
                                String nominee_name = nomi_name.getText().toString();
                                String holder_name = ac_holder.getText().toString();
                                String acc_no = account_no.getText().toString();
                                String bankBranch = bank_branch.getText().toString();
                                String bankAddress = bank_address.getText().toString();
                                String ifsc_code = ifsc.getText().toString();
                                String gstn_no = gstn.getText().toString();
                                String full_name = fullname.getText().toString();
                                String employeeID = employee.getText().toString();
                                String ac_no = "";
                                String pan_card = pancard.getText().toString();
                                String aadhar_proof = aadharProof.getText().toString();


                                for (int i = 0; i < g.child_rec_list.size(); i++) {
                                    AgencyChildSet obj1 = new AgencyChildSet();
                                    String child_name_val = g.child_rec_list.get(i).getChild_name();


                                  /*  if (g.child_rec_list.get(i).getChild_name().length() == 0) {
                                        alertMessage("Please Enter child Name");
                                        return;
                                    }
                                    if (g.child_rec_list.get(i).getChild_DOB().length() == 0 || g.child_rec_list.get(i).getChild_DOB().toString().equalsIgnoreCase("null")) {

                                        alertMessage("Please Enter child dob");
                                        return;
                                    }*/

                                    String child_dob_val = "";
                                    if (g.child_rec_list.get(i).getChild_DOB().toString().length() == 0) {
                                        // child_dob_val = "9999-12-31T00:00:00";
                                        child_dob_val = null;
                                    } else {
                                        child_dob_val = g.child_rec_list.get(i).getChild_DOB().toString() + "T00:00:00";
                                    }

                                    String child_edu_val = g.child_rec_list.get(i).getChild_education();
                                    if (childselect == 1 || noofchild_val.equalsIgnoreCase("0")) {
                                        // obj1.setAg_child_id(null);
                                    } else {
                                        String child_edu_val_id = g.child_rec_list.get(i).getAg_child_id().toString();
                                        obj1.setAg_child_id(child_edu_val_id);

                                    }
                                    String child_gender_val = g.child_rec_list.get(i).getChild_gender();

                                    obj1.setChild_name(child_name_val);
                                    obj1.setChild_DOB(child_dob_val);
                                    obj1.setChild_education(child_edu_val);
                                    obj1.setChild_gender(child_gender_val);
                                    agencyChildSetModelList.add(obj1);

                                }


                                AgencyRequestKYBPSetModel obj = new AgencyRequestKYBPSetModel();
                                Log.e("hsvfjsdbjsdfb",kybp_gendertype);
                                Log.e("hsvfjsdbjsdfb",strMrType);
                                obj.setAg_gender(kybp_gendertype);
                                obj.setAg_title(strMrType);
                                obj.setFirst_name(first_name);
                                obj.setResi_location(strLocation);
                                obj.setAg_detail_id(Integer.parseInt(g.agentId));
                                obj.setMiddle_name(middle_name);
                                obj.setLast_name(last_name);
                                obj.setAgency_name(agency_name);
                                obj.setResi_houseno(resi_flat);
                                obj.setResi_building(resi_building);
                                obj.setResi_street(resi_street);

                                if (residance_state_val.length() == 0) {
                                    obj.setResi_state(0);
                                } else {
                                    obj.setResi_state(Integer.parseInt(residance_state_val));
                                }
                                if (residance_city_val.length() == 0) {
                                    obj.setResi_city(0);
                                } else {
                                    obj.setResi_city(Integer.parseInt(residance_city_val));
                                }

                                obj.setResi_pincode(resi_pin);
                                obj.setResi_mono(resi_mob);
                                obj.setResi_watsup(resi_whatsapp);
                                obj.setResi_email(resi_email);
                                obj.setBusi_houseno(bis_flat);
                                obj.setBusi_building(bis_building);
                                obj.setBusi_street(bis_street);

                                if (bisiness_state_val.length() == 0) {
                                    obj.setBusi_state(0);
                                } else {
                                    obj.setBusi_state(Integer.parseInt(bisiness_state_val));
                                }

                                if (bisiness_city_val.length() == 0) {
                                    obj.setBusi_city(0);
                                } else {
                                    obj.setBusi_city(Integer.parseInt(bisiness_city_val));
                                }


                                obj.setBusi_pincode(bis_pin);
                                obj.setBusi_mono(bis_mobile);
                                obj.setAgency_OtherNewsPaper_set(agencyOtherNewsPaperSetModelList);
                                obj.setBusi_watsup(bis_whatsapp);
                                obj.setBusi_email(bis_email);
                                obj.setAg_religion(kybp_religiontype);
                                obj.setAg_dob(agentdob);


                                if (marital_status_valpk.equalsIgnoreCase("1")) {
                                    agencyChildSetModelList.clear();
                                    obj.setAg_maritial_id(marital_status_valpk);
                                    obj.setSpouse_name("");
                                    obj.setSpouse_dob(null);
                                    obj.setAg_marrige_ani(null);


                                } else {
                                    obj.setAg_maritial_id(marital_status_valpk);
                                    obj.setSpouse_name(spouse_name);
                                    obj.setSpouse_dob(spouse_dob);
                                    obj.setAg_marrige_ani(anneversiry_date);
                                }


                                if (noofchild_val.length() == 0) {
                                    obj.setNo_of_children(0);
                                } else {
                                    obj.setNo_of_children(Integer.parseInt(noofchild_val));
                                }

                                obj.setFather_name(agency_father_name);
                                obj.setFather_dob(agency_father_dob);
                                obj.setMother_name(mother_name);
                                obj.setMother_dob(mother_dob);
                                obj.setBrother_name(bro_name);
                                obj.setBrother_dob(bro_dob);
                                obj.setSister_name(sis_name);
                                obj.setSister_dob(sis_dob);
                                obj.setNominee_name(nominee_name);
                                obj.setNominee_relation(Nominee_relation);
                                obj.setBank_holder_name(holder_name);
                                obj.setBank_acc_no(acc_no);
                                obj.setBank_name_branch(bankBranch);
                                obj.setBank_address(bankAddress);
                                obj.setBank_IFSC(ifsc_code);
                                obj.setgSTIN(gstn_no);




                                if (relation_yesno.equalsIgnoreCase("1")) {
                                    // Toast.makeText(context,"True",Toast.LENGTH_SHORT).show();
                                    obj.setAg_db_relative(relation_yesno);
                                    obj.setDb_gender(relation_gender);
                                    obj.setDb_fullname(full_name);
                                    obj.setDb_empid(employeeID);
                                    obj.setDb_desig(designation);
                                    obj.setDb_dept(department);
                                    obj.setDb_city(bisiness_city_val);
                                    obj.setDb_relation(relationship_val);
                                } else {
                                    //  Toast.makeText(context,"false",Toast.LENGTH_SHORT).show();
                                    obj.setAg_db_relative(relation_yesno);
                                    obj.setDb_gender("M");
                                    obj.setDb_fullname("");
                                    obj.setDb_empid("1");
                                    obj.setDb_desig("1");
                                    obj.setDb_dept("1");
                                    obj.setDb_city("1");
                                    obj.setDb_relation("1");
                                }

                                obj.setPan_no(pan_card);

                                if (ac_holder.getText().toString().length() == 0) {
                                    alertMessage("Please Enter account holder name");
                                    return;
                                }
                                if (account_no.getText().toString().length() == 0) {
                                    alertMessage("Please Enter account no");
                                    return;

                                }

                                if (ifsc.getText().toString().length() == 0 || ifsc.getText().toString().length() < 10) {

                                    alertMessage("Please Enter IFSC CODE");
                                    return;

                                }

                                if (bank_branch.getText().toString().length() == 0) {
                                    alertMessage("Please Enter account branch");
                                    return;
                                }
                                if (bank_address.getText().toString().length() == 0) {
                                    alertMessage("Please Enter bank address");
                                    return;
                                }
                                if (ifsc.getText().toString().length() == 0) {

                                    alertMessage("Please Enter IFSC");
                                    return;
                                }

                                if (relation_yesno.length() == 0) {
                                    alertMessage("Do you have any relationship with DB?");
                                    return;
                                }

                                if (relation_yesno.equalsIgnoreCase("1")) {

                                    if (fullname.getText().length() == 0) {
                                        alertMessage("Please enter the full name");
                                        return;
                                    }
                                    if (employee.getText().length() == 0) {
                                        alertMessage("Please enter the employee id");
                                        return;
                                    }

                                    if (desig.getText().length() == 0) {
                                        alertMessage("Please select employee designation");
                                        return;
                                    }
                                    if (depart.getText().length() == 0) {
                                        alertMessage("Please select employee department");
                                        return;
                                    }
                                    if (unit_rel.getText().length() == 0) {
                                        alertMessage("Please select employee unit");
                                        return;
                                    }

                                    if (state_rel.getText().length() == 0) {
                                        alertMessage("Please select employee state");
                                        return;
                                    }


                                    if (relationship.getText().length() == 0) {
                                        alertMessage("Please select employee relationship");
                                        return;
                                    }

                                }

                              /*  if (ag_brother_dob.getText().toString().length() > 0 && ag_brother_name.getText().toString().length() == 0) {
                                    alertMessage("Please fill brother name and brother dob");
                                    return;
                                }
                                if (ag_brother_name.getText().toString().length() > 0 && ag_brother_dob.getText().toString().length() == 0) {
                                    alertMessage("Please fill brother name and brother dob");
                                    return;
                                }

                                if (ag_sister_dob.getText().toString().length() > 0 && ag_sister_name.getText().toString().length() == 0) {
                                    alertMessage("Please fill sister name and brother dob");
                                    return;
                                }
                                if (ag_sister_name.getText().toString().length() > 0 && ag_sister_dob.getText().toString().length() == 0) {
                                    alertMessage("Please fill sister name and brother dob");
                                    return;
                                }
*/
                                if (ag_father_dob.getText().toString().length() > 0 && ag_father_name.getText().toString().length() == 0) {
                                    alertMessage("Please fill father name and brother dob");
                                    return;
                                }
                                if (ag_father_name.getText().toString().length() > 0 && ag_father_dob.getText().toString().length() == 0) {
                                    alertMessage("Please fill father name and brother dob");
                                    return;
                                }

                              /*  if (ag_mother_dob.getText().toString().length() > 0 && ag_mother_name.getText().toString().length() == 0) {
                                    alertMessage("Please fill father name and brother dob");
                                    return;
                                }
                                if (ag_mother_name.getText().toString().length() > 0 && ag_mother_dob.getText().toString().length() == 0) {
                                    alertMessage("Please fill father name and brother dob");
                                    return;
                                }*/

                                if (pancard.getText().toString().length() == 0 || pancard.getText().toString().length() < 10) {

                                    alertMessage("Please enter Pan card no.");
                                    return;
                                }


                                if (panCardBitmap.length() == 0) {

                                    alertMessage("Please upload agent’s PAN card image");
                                    return;
                                }


                                if (aadharProof.getText().toString().length() == 0) {

                                    alertMessage("Please enter Address proof no.");
                                    return;
                                }


                                if (addharCardBitMap.length() == 0) {
                                    alertMessage("Please upload agent’s Address Card Image");
                                    return;
                                }
                                if (bankPassBookBitmap.length() == 0) {
                                    alertMessage("Please upload agent’s " + bankPassbook_val + "  Image");
                                    return;
                                }
                                if (panCardBitmap.equals("") || panCardBitmap.equalsIgnoreCase("")) {
                                    obj.setPan_copy(null);
                                } else {
                                    obj.setPan_copy("data:image/jpeg;base64," + panCardBitmap);
                                }

                                if (addharCardBitMap.equals("") || addharCardBitMap.equalsIgnoreCase("")) {
                                    obj.setAdd_proof_copy(null);
                                } else {
                                    obj.setAdd_proof_copy("data:image/jpeg;base64," + addharCardBitMap);
                                }

                                obj.setAdd_proof_no(aadhar_proof);
                                //     obj.setAdd_proof_copy("data:image/jpeg;base64,"+addharCardBitMap);
                                obj.setAdd_proof_type(add_proof_type_val);
                                obj.setBank_copy_type(bankPassbook_val);

                                if (bankPassBookBitmap.equals("") || bankPassBookBitmap.equalsIgnoreCase("")) {
                                    obj.setBank_copy(null);
                                } else {
                                    obj.setBank_copy("data:image/jpeg;base64," + bankPassBookBitmap);
                                }
                                //   obj.setBank_copy("data:image/jpeg;base64,"+bankPassBookBitmap);
                                obj.setAgency_Child_set(agencyChildSetModelList);

                                g.agencyRequestKYBPSetList.add(obj);
//


                                //  postData(rootModelList);
                                // Toast.makeText(context, "dfdfgdgd", Toast.LENGTH_SHORT).show();
                                //  String data=gson.toJson(obj).toString();
//                            Log.e("agencyList",data);
//                            Toast.makeText(KYBPActivity.this, "Data Save Successfully", Toast.LENGTH_SHORT).show();
//


                                if (pancard.getText().toString().length() == 0 || pancard.getText().toString().length() < 10) {

                                    alertMessage("Please Enter valid pan card");
                                    return;
                                }
                                if (aadhar_spinner.getText().toString().length() == 0) {

                                    alertMessage("Please Select Id Proof Type");
                                    return;
                                }
                                if (aadharProof.getText().toString().length() == 0 || aadharProof.getText().toString().length() < 12) {
                                    alertMessage("Please Enter valid aadhar no");
                                    return;
                                }
                                if (bankPassbook_val.length() == 0) {

                                    alertMessage("Select Bank PassBook ");
                                    return;
                                }
                                Gson gson = new Gson();
                                String data1 = gson.toJson(obj).toString();
                                Log.e("kyBp", data1);
                                addKyBpForm(obj);

                            }


                            catch (Exception e) {
                                alertMessage("Please tell to developer " + e.getMessage() + "line no " + e.getStackTrace()[0].getLineNumber());

                            }
                        }
                    });


                }
                if (currentPage + 1 == 1) {


                    boolean resiEmailCheck = false, busEmailCheck = false;

                    if (residance_email.getText().toString().length() > 0) {


                        String email = residance_email.getText().toString();
                        if (email.matches(emailPattern) && residance_email.getText().toString().length() > 0) {
                            // or
                            resiEmailCheck = false;

                        } else {
                            //or
                            resiEmailCheck = true;

                        }


                    }

                    if (bisiness_email.getText().toString().length() > 0) {


                        String email = bisiness_email.getText().toString();
                        if (email.matches(emailPattern) && bisiness_email.getText().toString().length() > 0) {
                            // or
                            busEmailCheck = false;

                        } else {
                            //or
                            busEmailCheck = true;

                        }


                    }
                    if (resiEmailCheck) {
                        alertMessage("Please enter right residence  email");
                        return;
                    }
                    if (busEmailCheck) {
                        alertMessage("Please enter right business  email");
                        return;
                    }

                    if (kybp_gendertype.length() == 0) {
                        alertMessage("Please select the gender");
                        return;
                    }
                    if (firstname.getText().toString().length() == 0) {
                        alertMessage("Please enter first name");
                        return;
                    }
                    if (lastname.getText().toString().length() == 0) {
                        alertMessage("Please fill the last name");
                        return;
                    }
                    if (editLocation.getText().toString().length() == 0) {
                        alertMessage("Please fill the location");
                        return;
                    }
                    if (kybp_agency_name.getText().toString().length() == 0) {
                        alertMessage("Please fill the agency name");
                        return;
                    }
                    if (residance_flat.getText().toString().length() == 0) {
                        alertMessage("Please fill agent’s residential address");
                        return;
                    }
                    if (residance_buiding.getText().toString().length() == 0) {
                        alertMessage("Please fill the residence building no");
                        return;

                    }
                    if (residance_street.getText().toString().length() == 0) {
                        alertMessage("Please fill the residence street");
                        return;
                    }
                    if (residance_state.getText().toString().length() == 0) {
                        alertMessage("Please fill the residence state");
                        return;
                    }
                    if (residance_city.getText().toString().length() == 0) {
                        alertMessage("Please fill agent’s residential city/village");
                        return;
                    }
                    if (residance_pin.getText().toString().length() == 0 || residance_pin.getText().toString().length() < 6) {
                        alertMessage("Please fill agents’ residential address pin code");
                        return;
                    }
                    if (residance_mobile.getText().toString().length() == 0 || residance_mobile.getText().toString().length() < 10) {
                        alertMessage("Please enter valid residence mobile");
                        return;
                    }
//                    if(residance_whatsapp.getText().toString().length()==0 || residance_whatsapp.getText().toString().length()<10){
//                        alertMessage("Please enter valid residence whatsapp no");
//                        return;
//                    }

                    if (bisiness_flat.getText().toString().length() == 0) {
                        alertMessage("Please fill agents’ business address");
                        return;
                    }

                    if (bisiness_buiding.getText().toString().length() == 0) {
                        alertMessage("Please fill the business building");
                        return;
                    }

                    if (bisiness_street.getText().toString().length() == 0) {
                        alertMessage("Please fill the business street");
                        return;
                    }
                    if (bisiness_state.getText().toString().length() == 0) {
                        alertMessage("Please fill the business state");
                        return;
                    }
                    if (bisiness_city.getText().toString().length() == 0) {
                        alertMessage("Please fill the business city");
                        return;
                    }
                    if (bisiness_pin.getText().toString().length() == 0 || bisiness_pin.getText().toString().length() < 6) {
                        alertMessage("Please enter valid business pin");
                        return;
                    }
                    if (bisiness_mobile.getText().toString().length() == 0 || bisiness_mobile.getText().toString().length() < 10) {
                        alertMessage("Please enter valid business mobile");
                        return;
                    }
//                    if(bisiness_whatsapp.getText().toString().length()==0 || bisiness_whatsapp.getText().toString().length()<10){
//                        alertMessage("Please enter valid business whatsapp");
//                        return;
//                    }


                    mBinding.viewPager.setCurrentItem(currentPage + 1);

                } else if (currentPage + 1 == 2) {
                    if (kybp_religiontype.length() == 0) {
                        alertMessage("Please select Religion ");
                        return;
                    }
                    if (agent_dob.length() == 0) {
                        alertMessage("Please select agent DOB ");
                        return;
                    }
                    if (marital_status_val.equals("")) {
                        alertMessage("Please select agent’s marital status");
                        return;
                    }

                    if (marital_status_val.equalsIgnoreCase("Married") || marital_status_val.equalsIgnoreCase("Divorced")) {
                        if (spousename.getText().toString().length() == 0) {
                            alertMessage("Please fill the spouse name");
                            return;
                        }
                        if (spousedob.getText().toString().length() == 0) {
                            alertMessage("Please fill the spouse dob");
                            return;
                        }
                        if (anneversiry.getText().toString().length() == 0) {
                            alertMessage("Please fill the anniversary date");
                            return;

                        }
                        if (anneversiry.getText().toString().length() == 0) {
                            alertMessage("Please select the anniversary date");
                            return;

                        }

                        if (noofchild_val.length() == 0) {
                            alertMessage("Please enter the number of child ");
                            return;
                        }

                    }
                    //TODO : check for if marital status is single and relationship is equal to Spouse , Son , Doughter then ask to change nominee relation

                    /*if(marital_status_val.equals("Single"))
                    {
                       if(tvNomineeName.getText().equals("Spouse") ||  tvNomineeName.getText().equals("Son")  || tvNomineeName.getText().equals("Doughter"))
                       {
                           alertMessage("Please change nominee relationship ");
                           return;
                       }
                    }*/

                    if (ag_father_name.getText().length() == 0) {
                        alertMessage("Please enter father name");
                        return;
                    }
                    if (ag_father_dob.getText().length() == 0) {
                        alertMessage("Please enter father DOB");
                        return;
                    }

                    if (tvNomineeName.getText().length() == 0) {
                        alertMessage("Please enter nominee relation");
                        return;
                    }
                    if (marital_status_valpk.equals("1")) {
                        if (tvNomineeName.getText().toString().equals("Spouse") || tvNomineeName.getText().toString().equals("Son") || tvNomineeName.getText().toString().equals("Daughter"))
                            alertMessage("Please select nominee relation apart from Spouse,Son and Daughter");
                    }
                    if (nomi_name.getText().length() == 0) {
                        alertMessage("Please enter nominee name");
                        return;
                    }
                    if (agency_newspaper.length() == 0) {
                        alertMessage("Please select “Yes” if you have any other newspaper agency");
                        return;

                    }

                    if (agency_newspaper.equalsIgnoreCase("Yes")) {
                        if (noofnewspaper.getText().length() == 0) {
                            alertMessage("Please enter newspaper count");
                            return;
                        }
                        if (agencyOtherNewsPaperSetModelList.size() <= 0) {
                            alertMessage("Please Select News Paper");
                            return;
                        }
                    }

                    mBinding.viewPager.setCurrentItem(currentPage + 1);
                } else {

                    mBinding.viewPager.setCurrentItem(currentPage + 1);
                }


                break;

            default:
                break;
        }
    }


    public void getRelation() {

    }


    public void getClearData() {

    }


    public void getMaritialStatus() {
        g.getMaritialArray.clear();
        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getMaritial(PreferenceManager.getAgentId(context));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;
                    Log.e("getMaritial", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            for (int i = 0; i < objData.length(); i++) {

                                JSONObject objStr = objData.getJSONObject(i);
                                DropDownModel dropDownModel = new DropDownModel();
                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("maritial_status"));
                                g.getMaritialArray.add(dropDownModel);
                            }


                            List<String> categories = new ArrayList<String>();
                            Log.e("size marrital", String.valueOf(g.getMaritialArray.size()));
                            for (int l = 0; l < g.getMaritialArray.size(); l++) {
                                categories.add(g.getMaritialArray.get(l).getDescription());
                            }
                            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, categories);
                            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            marital_status.setAdapter(dataAdapter);
                        }
                        enableLoadingBar(false);
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Log.e("dfgsds", "1387");
                        //  Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        Log.e("dfgsds", "1391");
                        //Log.e("dfgsds","1693");
                        //  Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(KYBPActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }


}