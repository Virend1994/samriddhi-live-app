package com.app.samriddhi.ui.activity.main.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;

import com.app.samriddhi.R;
import com.bumptech.glide.Glide;

public class WebviewUrl extends AppCompatActivity {

    WebView browser;
    ImageView imgView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview_url);

        Intent intent = getIntent();
        String strType=intent.getStringExtra("type");
        String strURL=intent.getStringExtra("url");
        browser = findViewById(R.id.webView);
        imgView =findViewById(R.id.imgView);
        Log.e("sdgsdgsd",strType);
        Log.e("sdgsdgsd",strURL);

        Intent intents = new Intent(Intent.ACTION_VIEW, Uri.parse(strURL));
        intents.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intents.setPackage("com.android.chrome");

        try {
            startActivity(intents);
        } catch (ActivityNotFoundException ex) {
            // Chrome browser presumably not installed so allow user to choose instead
            intents.setPackage(null);
            startActivity(intents);
        }


      /*  if(strType.equals("2")){
            browser.setVisibility(View.GONE);
            imgView.setVisibility(View.VISIBLE);

            try {
                Glide.with(this)
                        .load(strURL)
                        .thumbnail(Glide.with(this).load(R.drawable.img_loader))
                        .into(imgView);
            } catch (Exception e) {
                e.printStackTrace();
            }


        }
        else{

            imgView.setVisibility(View.GONE);
            browser.setVisibility(View.VISIBLE);
            browser.loadUrl(strURL);
        }*/
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}