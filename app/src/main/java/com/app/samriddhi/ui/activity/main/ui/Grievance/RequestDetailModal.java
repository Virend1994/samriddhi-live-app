package com.app.samriddhi.ui.activity.main.ui.Grievance;

public class RequestDetailModal {
    public String Id;
    public String number;
    public String date;
    public String status;
    public String type;
    public String sub_cat_name;
    public String description;
    public String image;
    public String path;
    public String ho_remark;
    public String ho_close_date;


    public String getHo_remark() {
        return ho_remark;
    }

    public void setHo_remark(String ho_remark) {
        this.ho_remark = ho_remark;
    }

    public String getHo_close_date() {
        return ho_close_date;
    }

    public void setHo_close_date(String ho_close_date) {
        this.ho_close_date = ho_close_date;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSub_cat_name() {
        return sub_cat_name;
    }

    public void setSub_cat_name(String sub_cat_name) {
        this.sub_cat_name = sub_cat_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
