package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BillOutAgentListModel extends BaseModel {

    @SerializedName("SoldToParty")
    @Expose
    private String SoldToParty;


    @SerializedName("SoldToPartyName")
    @Expose
    private String SoldToPartyName;



    @SerializedName("Billing")
    @Expose
    private String Billing;



    @SerializedName("Outstanding")
    @Expose
    private String Outstanding;

    public String getSoldToParty() {
        return SoldToParty;
    }

    public void setSoldToParty(String soldToParty) {
        SoldToParty = soldToParty;
    }

    public String getSoldToPartyName() {
        return SoldToPartyName;
    }

    public void setSoldToPartyName(String soldToPartyName) {
        SoldToPartyName = soldToPartyName;
    }

    public String getBilling() {
        return Billing;
    }

    public void setBilling(String billing) {
        Billing = billing;
    }

    public String getOutstanding() {
        return Outstanding;
    }

    public void setOutstanding(String outstanding) {
        Outstanding = outstanding;
    }
}
