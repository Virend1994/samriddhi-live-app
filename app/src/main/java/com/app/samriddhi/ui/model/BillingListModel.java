package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.app.samriddhi.util.SystemUtility;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BillingListModel extends BaseModel {

    @SerializedName("BillNo")
    @Expose
    private String billNo;
    @SerializedName("CloseBal")
    @Expose
    private String closeBal;
    @SerializedName("Fkdat")
    @Expose
    private String fkdat;
    @SerializedName("SoldToPty")
    @Expose
    private String soldToPty;
    @SerializedName("Monat")
    @Expose
    private String monat;
    @SerializedName("Gjahr")
    @Expose
    private String gjahr;
    @SerializedName("SoldCopies")
    @Expose
    private String soldCopies;
    @SerializedName("NetBilling")
    @Expose
    private String netBilling;
    @SerializedName("OpenBal")
    @Expose
    private String openBal;
    @SerializedName("PayAmt")
    @Expose
    private String payAmt;
    @SerializedName("Payment")
    @Expose
    private String payment;
    @SerializedName("CollectionAmount")
    @Expose
    private String CollectionAmount;
    @SerializedName("CreditAmount")
    @Expose
    private String CreditAmount;

    @SerializedName("EstdCloseBal")
    @Expose
    private String estdCloseBal;
    @SerializedName("UnbillAmt")
    @Expose
    private String unbillAmt;
    @SerializedName("CopyMtd")
    @Expose
    private String CopyMtd;

    /**
     * provides the number of copies
     * @return
     */

    public String getCopyMtd() {
        // return CopyMtd;
        if (!CopyMtd.isEmpty()) {
            String value = SystemUtility.formatDecimalValues(Float.parseFloat((CopyMtd)));
            return value;
        } else return CopyMtd;
    }

    public void setCopyMtd(String copyMtd) {
        CopyMtd = copyMtd;
    }

    /**
     * provides the estimated closing balance
     * @return
     */
    public String getEstdCloseBal() {
        //return estdCloseBal;
        if (!estdCloseBal.isEmpty()) {
            String value = SystemUtility.formatDecimalValues(Float.parseFloat((estdCloseBal)));
            return value;
        } else return estdCloseBal;
    }

    public void setEstdCloseBal(String estdCloseBal) {
        this.estdCloseBal = estdCloseBal;
    }

    /**
     * return the unbilled amount of a bill
     * @return
     */
    public String getUnbillAmt() {
        //return unbillAmt;
        if (!unbillAmt.isEmpty()) {
            String value = SystemUtility.formatDecimalValues(Float.parseFloat((unbillAmt)));
            return value;
        } else return unbillAmt;
    }

    public void setUnbillAmt(String unbillAmt) {
        this.unbillAmt = unbillAmt;
    }


    /**
     * returns the collections Amount of bill
     * @return
     */
    public String getCollectionAmount() {
        if (!CollectionAmount.isEmpty()) {
            String value = SystemUtility.formatDecimalValues(Float.parseFloat((CollectionAmount)));
            return value;
        } else return CollectionAmount;
    }

    public void setCollectionAmount(String collectionAmount) {
        CollectionAmount = collectionAmount;
    }

    /**
     * returns the formatted credit amount of bill
     * @return
     */
    public String getCreditAmount() {

        if (!CreditAmount.isEmpty()) {
            String value = SystemUtility.formatDecimalValues(Float.parseFloat((CreditAmount)));
            return value;
        } else return CreditAmount;

    }

    public void setCreditAmount(String creditAmount) {
        CreditAmount = creditAmount;
    }

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    /**
     * returns the formatted closing balance
     * @return
     */
    public String getCloseBal() {
        if (!closeBal.isEmpty()) {
            String value = SystemUtility.formatDecimalValues(Float.parseFloat((closeBal)));
            return (value);
        } else return closeBal;
    }

    public void setCloseBal(String closeBal) {
        this.closeBal = closeBal;
    }

    public String getFkdat() {
        return fkdat;
    }

    public void setFkdat(String fkdat) {
        this.fkdat = fkdat;
    }

    public String getSoldToPty() {
        return soldToPty;
    }

    public void setSoldToPty(String soldToPty) {
        this.soldToPty = soldToPty;
    }

    public String getMonat() {
        return monat;
    }

    public void setMonat(String monat) {
        this.monat = monat;
    }

    public String getGjahr() {
        return gjahr;
    }

    public void setGjahr(String gjahr) {
        this.gjahr = gjahr;
    }

    public String getSoldCopies() {
        return soldCopies;
    }

    public void setSoldCopies(String soldCopies) {
        this.soldCopies = soldCopies;
    }

    /**
     * returns the formatted NetBilling Amount
     * @return
     */
    public String getNetBilling() {
        if (!netBilling.isEmpty()) {
            String value = SystemUtility.formatDecimalValues(Float.parseFloat((netBilling)));
            return value;
        }
        return netBilling;
    }

    public void setNetBilling(String netBilling) {

        this.netBilling = netBilling;
    }

    /**
     * returns the formatted opening balance
     * @return openingBalance
     */
    public String getOpenBal() {

        if (!openBal.isEmpty()) {
            String value = SystemUtility.formatDecimalValues(Float.parseFloat((openBal)));
            return value;
        }
        return openBal;
    }

    public void setOpenBal(String openBal) {
        this.openBal = openBal;
    }

    /**
     * return the formatted payment amount
     * @return amount
     */
    public String getPayAmt() {
        if (!payAmt.isEmpty()) {
            String value = SystemUtility.formatDecimalValues(Float.parseFloat((payAmt)));
            return value;
        }
        return payAmt;
    }

    public void setPayAmt(String payAmt) {
        this.payAmt = payAmt;
    }

    public String getPayment() {
        if (!payment.isEmpty()) {
            String value = SystemUtility.formatDecimalValues(Float.parseFloat((payment)));
            return value;
        }
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }


    /**
     * returns the formatted months for billing
     * @return
     */
    public String getFormattedMonth() {

        if (monat.equalsIgnoreCase("01")) {
            return "Jan - " + gjahr;
        } else if (monat.equalsIgnoreCase("02")) {
            return "Feb - " + gjahr;
        } else if (monat.equalsIgnoreCase("03")) {
            return "Mar - " + gjahr;
        } else if (monat.equalsIgnoreCase("04")) {
            return "Apr - " + gjahr;
        } else if (monat.equalsIgnoreCase("05")) {
            return "May - " + gjahr;
        } else if (monat.equalsIgnoreCase("06")) {
            return "Jun - " + gjahr;
        } else if (monat.equalsIgnoreCase("07")) {
            return "Jul - " + gjahr;
        } else if (monat.equalsIgnoreCase("08")) {
            return "Aug - " + gjahr;
        } else if (monat.equalsIgnoreCase("09")) {
            return "Sep - " + gjahr;
        } else if (monat.equalsIgnoreCase("10")) {
            return "Oct - " + gjahr;
        } else if (monat.equalsIgnoreCase("11")) {
            return "Nov - " + gjahr;
        } else if (monat.equalsIgnoreCase("12")) {
            return "Dec - " + gjahr;
        } else return "";
    }


}
