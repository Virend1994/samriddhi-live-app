package com.app.samriddhi.ui.activity.main.ui.Grievance;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.R;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.Grievance.multipleimage.ImageAdapter;
import com.app.samriddhi.util.Constant;
import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.material.card.MaterialCardView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ImagaAdapter  extends RecyclerView.Adapter<ImagaAdapter.MyViewHolder>{

    private final List<ImageModal> requestDetailModals;
    Context context;
    int row_index = -1;
    public ImagaAdapter(List<ImageModal> requestDetailModals, Context context) {
        this.requestDetailModals = requestDetailModals;
        this.context = context;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = null;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder Viewholder, @SuppressLint("RecyclerView") int position) {
        ImageModal listData = requestDetailModals.get(position);
        Viewholder.setIsRecyclable(false);




        if(listData.getImage()!="null"){

            Viewholder.relImage.setVisibility(View.VISIBLE);
            try {
                Log.e("dsgsdgsdg",listData.getImage());
                Glide.with(context).load(listData.getImageurl()+listData.getImage()).into(Viewholder.capImage);
                // Glide.with(context).load("https://image.shutterstock.com/image-photo/mountains-under-mist-morning-amazing-260nw-1725825019.jpg").into(Viewholder.capImage);
            } catch (Exception e) {
                Log.e("sfsdfsfsfsdf",e.getMessage());
            }

        }

        else {
            Log.e("asfasfasfa","else");
            Viewholder.relImage.setVisibility(View.GONE);

        }

        Viewholder.capImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showAlert(listData.getImageurl()+listData.getImage());
            }
        });
    }
    public void showAlert(String status) {


        View popupView = null;
        Log.e("sfsafasasfa",status);

        popupView = LayoutInflater.from(context).inflate(R.layout.enlarge_image, null);


        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0);

        ImageView closeImg = popupView.findViewById(R.id.closeImg);
        closeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                popupWindow.dismiss();
            }
        });

        PhotoView imageView = popupView.findViewById(R.id.imageView);
        Glide.with(context).load(status).into(imageView);



    }


    @Override
    public int getItemCount() {
        return requestDetailModals.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView capImage;
        RelativeLayout relImage;
        MyViewHolder(View view) {
            super(view);

            capImage = view.findViewById(R.id.capImage);
            relImage = view.findViewById(R.id.relImage);
        }
    }


}

