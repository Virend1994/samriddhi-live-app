package com.app.samriddhi.ui.activity.main.ui.Grievance;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.app.samriddhi.R;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.google.android.material.button.MaterialButton;

public class SubmitRequestPopup extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_submit_request_popup2);



        TextView txtComplaintIid=findViewById(R.id.txtComplaintIid);
        txtComplaintIid.setText("#"+PreferenceManager.getComplaint_number(this));

        MaterialButton save_cr=findViewById(R.id.save_cr);
        save_cr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(SubmitRequestPopup.this,GrievanceTabActivity.class));
                finish();
            }
        });

        TextView txtThankyou=findViewById(R.id.txtThankyou);


        if(PreferenceManager.getAppLang(this).equals("hi")){

            save_cr.setText("ठीक है");
            txtThankyou.setText("धन्यवाद");
        }
        else {

            save_cr.setText("Okay");
            txtThankyou.setText("Thank You");
        }
        TextView tvTitle=findViewById(R.id.tvTitle);
        TextView tvDetail=findViewById(R.id.tvDetail);
        TextView tvNumber=findViewById(R.id.tvNumber);

        Log.e("sdfdsfsdfs",PreferenceManager.getAppLang(SubmitRequestPopup.this));

        if(PreferenceManager.getComplaint_Type(this).equals("c")){

            if(PreferenceManager.getAppLang(this).equals("hi")){

                tvTitle.setText("आपकी शिकायत के लिए");
                tvDetail.setText("24 घंटे के भीतर आपकी शिकायत संबंधित व्यक्ति को भेज दी जाएगी।");
                tvNumber.setText("आपका शिकायत नंबर है");
            }
            else {
                tvTitle.setText(R.string.for_complaint);
                tvDetail.setText(R.string.complaint_within);
                tvNumber.setText(R.string.compaint_number);
            }



        }
        else if(PreferenceManager.getComplaint_Type(this).equals("s")){


            if(PreferenceManager.getAppLang(this).equals("hi")){

                tvTitle.setText("आपके सुझाव के लिए");
                tvDetail.setText("24 घंटे के भीतर आपका सुझाव संबंधित व्यक्ति को भेज दी जाएगी");
                tvNumber.setText("आपका सुझाव नंबर है");
            }
            else {
                tvTitle.setText(R.string.for_suggestion);
                tvDetail.setText(R.string.suggestion_within);
                tvNumber.setText(R.string.suggestion_number);
            }


        }

        else if(PreferenceManager.getComplaint_Type(this).equals("q")){


            if(PreferenceManager.getAppLang(this).equals("hi")){
                tvTitle.setText("आपके सवाल के लिए");
                tvDetail.setText("24 घंटे के भीतर आपका सवाल संबंधित व्यक्ति को भेज दी जाएगी");
                tvNumber.setText("आपका सवाल नंबर है");
            }
            else {
                tvTitle.setText(R.string.for_query);
                tvDetail.setText(R.string.query_within);
                tvNumber.setText(R.string.query_number);
            }

        }
        else {

            if(PreferenceManager.getAppLang(this).equals("hi")){
                tvTitle.setText("आपके निवेदन के लिए");
                tvDetail.setText("24 घंटे के भीतर आपका निवेदन संबंधित व्यक्ति को भेज दी जाएगी");
                tvNumber.setText("आपका निवेदन नंबर है");
            }
            else {
                tvTitle.setText(R.string.for_request);
                tvDetail.setText(R.string.request_within);
                tvNumber.setText(R.string.request_number);
            }


        }

    }
}