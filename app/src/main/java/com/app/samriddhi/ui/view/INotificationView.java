package com.app.samriddhi.ui.view;

import com.app.samriddhi.ui.model.NotificationModel;

import java.util.List;

public interface INotificationView extends IView {
    void onNotificationsSuccess(List<NotificationModel> body);
}
