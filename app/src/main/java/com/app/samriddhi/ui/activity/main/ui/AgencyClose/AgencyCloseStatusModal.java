package com.app.samriddhi.ui.activity.main.ui.AgencyClose;

public class AgencyCloseStatusModal {

    public String sno;
    public String id;
    public String unit;
    public String place;
    public String uoh_Staus;
    public String uoh_updated_time;
    public String soh_status;
    public String soh_updated_time;
    public String ho_status;
    public String ho_update_time;
    public String ufh_status;
    public String ufh_remark;
    public String ufh_updated_time;
    public String cbr_status;
    public String cbr_reamrk;
    public String cbr_updated_time;
    public String closure_type;
    public String agency_code;
    public String agency_name;
    public String agent_name;
    public String copies;
    public String start_date;
    public String end_date;
    public String asd;
    public String outstanding;
    public String total_time;
    public String document;
    public String uoh_remark;
    public String soh_remark;
    public String soh_name;
    public String uoh_name;
    public String ho_remark;
    public String remark_time;
    public String agency_remark;
    public String create_date;
    public String created_by;
    public String status;
    public String UOHName;
    public String SohName;
    public String HOName;
    public String UFHName;
    public String CBRName;
    public String reason_id;
    public String nominee_name;
    public String nominee_relationship;
    public String est_bill_amt;
    public String total_ots;
    public String unbilled_amount;
    public String remark_new;
    public String balance_amount;
    public String agency_location;
    public String state_name;
    public String request_date;

    public String getState_name() {
        return state_name;
    }

    public void setState_name(String state_name) {
        this.state_name = state_name;
    }

    public String getEst_bill_amt() {
        return est_bill_amt;
    }

    public void setEst_bill_amt(String est_bill_amt) {
        this.est_bill_amt = est_bill_amt;
    }

    public String getTotal_ots() {
        return total_ots;
    }

    public void setTotal_ots(String total_ots) {
        this.total_ots = total_ots;
    }

    public String getUnbilled_amount() {
        return unbilled_amount;
    }

    public void setUnbilled_amount(String unbilled_amount) {
        this.unbilled_amount = unbilled_amount;
    }

    public String getRemark_new() {
        return remark_new;
    }

    public void setRemark_new(String remark_new) {
        this.remark_new = remark_new;
    }

    public String getBalance_amount() {
        return balance_amount;
    }

    public void setBalance_amount(String balance_amount) {
        this.balance_amount = balance_amount;
    }

    public String getAgency_location() {
        return agency_location;
    }

    public void setAgency_location(String agency_location) {
        this.agency_location = agency_location;
    }

    public String getReason_id() {
        return reason_id;
    }

    public void setReason_id(String reason_id) {
        this.reason_id = reason_id;
    }

    public String getNominee_name() {
        return nominee_name;
    }

    public void setNominee_name(String nominee_name) {
        this.nominee_name = nominee_name;
    }

    public String getNominee_relationship() {
        return nominee_relationship;
    }

    public void setNominee_relationship(String nominee_relationship) {
        this.nominee_relationship = nominee_relationship;
    }

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getUOHName() {
        return UOHName;
    }

    public void setUOHName(String UOHName) {
        this.UOHName = UOHName;
    }

    public String getSohName() {
        return SohName;
    }

    public void setSohName(String sohName) {
        SohName = sohName;
    }

    public String getHOName() {
        return HOName;
    }

    public void setHOName(String HOName) {
        this.HOName = HOName;
    }

    public String getUFHName() {
        return UFHName;
    }

    public void setUFHName(String UFHName) {
        this.UFHName = UFHName;
    }

    public String getCBRName() {
        return CBRName;
    }

    public void setCBRName(String CBRName) {
        this.CBRName = CBRName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getAgency_remark() {
        return agency_remark;
    }

    public void setAgency_remark(String agency_remark) {
        this.agency_remark = agency_remark;
    }

    public String getAgent_name() {
        return agent_name;
    }

    public void setAgent_name(String agent_name) {
        this.agent_name = agent_name;
    }

    public String getUfh_remark() {
        return ufh_remark;
    }

    public void setUfh_remark(String ufh_remark) {
        this.ufh_remark = ufh_remark;
    }

    public String getCbr_reamrk() {
        return cbr_reamrk;
    }

    public void setCbr_reamrk(String cbr_reamrk) {
        this.cbr_reamrk = cbr_reamrk;
    }

    public String getCbr_updated_time() {
        return cbr_updated_time;
    }

    public void setCbr_updated_time(String cbr_updated_time) {
        this.cbr_updated_time = cbr_updated_time;
    }

    public String getHo_remark() {
        return ho_remark;
    }

    public void setHo_remark(String ho_remark) {
        this.ho_remark = ho_remark;
    }

    public String getUoh_updated_time() {
        return uoh_updated_time;
    }

    public void setUoh_updated_time(String uoh_updated_time) {
        this.uoh_updated_time = uoh_updated_time;
    }

    public String getSoh_updated_time() {
        return soh_updated_time;
    }

    public void setSoh_updated_time(String soh_updated_time) {
        this.soh_updated_time = soh_updated_time;
    }

    public String getHo_update_time() {
        return ho_update_time;
    }

    public void setHo_update_time(String ho_update_time) {
        this.ho_update_time = ho_update_time;
    }

    public String getUfh_updated_time() {
        return ufh_updated_time;
    }

    public void setUfh_updated_time(String ufh_updated_time) {
        this.ufh_updated_time = ufh_updated_time;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public String getUoh_Staus() {
        return uoh_Staus;
    }

    public void setUoh_Staus(String uoh_Staus) {
        this.uoh_Staus = uoh_Staus;
    }

    public String getSoh_status() {
        return soh_status;
    }

    public void setSoh_status(String soh_status) {
        this.soh_status = soh_status;
    }

    public String getHo_status() {
        return ho_status;
    }

    public void setHo_status(String ho_status) {
        this.ho_status = ho_status;
    }

    public String getUfh_status() {
        return ufh_status;
    }

    public void setUfh_status(String ufh_status) {
        this.ufh_status = ufh_status;
    }

    public String getCbr_status() {
        return cbr_status;
    }

    public void setCbr_status(String cbr_status) {
        this.cbr_status = cbr_status;
    }

    public String getClosure_type() {
        return closure_type;
    }

    public void setClosure_type(String closure_type) {
        this.closure_type = closure_type;
    }

    public String getAgency_code() {
        return agency_code;
    }

    public void setAgency_code(String agency_code) {
        this.agency_code = agency_code;
    }

    public String getAgency_name() {
        return agency_name;
    }

    public void setAgency_name(String agency_name) {
        this.agency_name = agency_name;
    }

    public String getCopies() {
        return copies;
    }

    public void setCopies(String copies) {
        this.copies = copies;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getAsd() {
        return asd;
    }

    public void setAsd(String asd) {
        this.asd = asd;
    }

    public String getOutstanding() {
        return outstanding;
    }

    public void setOutstanding(String outstanding) {
        this.outstanding = outstanding;
    }

    public String getTotal_time() {
        return total_time;
    }

    public void setTotal_time(String total_time) {
        this.total_time = total_time;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getUoh_remark() {
        return uoh_remark;
    }

    public void setUoh_remark(String uoh_remark) {
        this.uoh_remark = uoh_remark;
    }

    public String getSoh_remark() {
        return soh_remark;
    }

    public void setSoh_remark(String soh_remark) {
        this.soh_remark = soh_remark;
    }

    public String getSoh_name() {
        return soh_name;
    }

    public void setSoh_name(String soh_name) {
        this.soh_name = soh_name;
    }

    public String getUoh_name() {
        return uoh_name;
    }

    public void setUoh_name(String uoh_name) {
        this.uoh_name = uoh_name;
    }

    public String getRemark_time() {
        return remark_time;
    }

    public void setRemark_time(String remark_time) {
        this.remark_time = remark_time;
    }

    public String getRequest_date() {
        return request_date;
    }

    public void setRequest_date(String request_date) {
        this.request_date = request_date;
    }
}
