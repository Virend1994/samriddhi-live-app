package com.app.samriddhi.ui.model;

import android.content.Context;
import android.view.View;

import com.app.samriddhi.base.model.BaseModel;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.SystemUtility;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CityListModel extends BaseModel {
    @SerializedName("BIllingMonth")
    @Expose
    private String BIllingMonth;
    @SerializedName("UnitName")
    @Expose
    private String UnitName;
    @SerializedName("UnitCode")
    @Expose
    private String UnitCode;
    @SerializedName("Billing")
    @Expose
    private String Billing;
    @SerializedName("Outstanding")
    @Expose
    private String Outstanding;

    public String getBIllingMonth() {
        return BIllingMonth;
    }

    public void setBIllingMonth(String BIllingMonth) {
        this.BIllingMonth = BIllingMonth;
    }

    public String getUnitName() {
        return UnitName;
    }

    public void setUnitName(String unitName) {
        UnitName = unitName;
    }

    public String getUnitCode() {
        return UnitCode;
    }

    public void setUnitCode(String unitCode) {
        UnitCode = unitCode;
    }

    public String getBilling() {
        return Billing;
    }

    public void setBilling(String billing) {
        Billing = billing;
    }

    public String getOutstanding() {
        return Outstanding;
    }

    public void setOutstanding(String outstanding) {
        Outstanding = outstanding;
    }
}
