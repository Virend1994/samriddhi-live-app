package com.app.samriddhi.ui.model;

import android.content.Context;
import android.view.View;

import com.app.samriddhi.base.model.BaseModel;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.SystemUtility;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StateListModel extends BaseModel {

    @SerializedName("StateName")
    @Expose
    private String StateName;


    @SerializedName("StateCode")
    @Expose
    private String StateCode;



    @SerializedName("YesterdayPO")
    @Expose
    private String YesterdayPO;



    @SerializedName("YesterdayNPS")
    @Expose
    private String YesterdayNPS;



    @SerializedName("TodayPO")
    @Expose
    private String TodayPO;



    @SerializedName("TodayNPS")
    @Expose
    private String TodayNPS;




    @SerializedName("PODiff")
    @Expose
    private String PODiff;


    @SerializedName("NPSDiff")
    @Expose
    private String NPSDiff;


    public String getStateName() {
        return StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }

    public String getStateCode() {
        return StateCode;
    }

    public void setStateCode(String stateCode) {
        StateCode = stateCode;
    }

    public String getYesterdayPO() {
        return YesterdayPO;
    }

    public void setYesterdayPO(String yesterdayPO) {
        YesterdayPO = yesterdayPO;
    }

    public String getYesterdayNPS() {
        return YesterdayNPS;
    }

    public void setYesterdayNPS(String yesterdayNPS) {
        YesterdayNPS = yesterdayNPS;
    }

    public String getTodayPO() {
        return TodayPO;
    }

    public void setTodayPO(String todayPO) {
        TodayPO = todayPO;
    }

    public String getTodayNPS() {
        return TodayNPS;
    }

    public void setTodayNPS(String todayNPS) {
        TodayNPS = todayNPS;
    }

    public String getPODiff() {
        return PODiff;
    }

    public void setPODiff(String PODiff) {
        this.PODiff = PODiff;
    }

    public String getNPSDiff() {
        return NPSDiff;
    }

    public void setNPSDiff(String NPSDiff) {
        this.NPSDiff = NPSDiff;
    }

    public int getItemVisibility(String screenName) {
        if (screenName.equalsIgnoreCase(Constant.DashBoardGrievance)) {
            return View.VISIBLE;
        } else return View.GONE;

    }


}
