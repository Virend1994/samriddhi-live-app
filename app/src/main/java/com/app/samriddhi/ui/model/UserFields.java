package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserFields extends BaseModel {
    @SerializedName("usermaster")
    @Expose
    private Integer usermaster;
    @SerializedName("access_type")
    @Expose
    private String accessType;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("from_date")
    @Expose
    private String fromDate;
    @SerializedName("to_date")
    @Expose
    private String toDate;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("create_date")
    @Expose
    private String createDate;
    @SerializedName("update_date")
    @Expose
    private String updateDate;
    @SerializedName("create_user")
    @Expose
    private String createUser;
    @SerializedName("update_user")
    @Expose
    private String updateUser;
    @SerializedName("last_login_on")
    @Expose
    private Object lastLoginOn;
    @SerializedName("mobile_no")
    @Expose
    private String mobileNo;

    @SerializedName("pan_image")
    @Expose
    private String pan_image;

    @SerializedName("user_image")
    @Expose
    private String user_image;

    @SerializedName("agent_city_upc")
    @Expose
    private String agent_city_upc;

    public String getAgent_city_upc() {
        return agent_city_upc;
    }

    public void setAgent_city_upc(String agent_city_upc) {
        this.agent_city_upc = agent_city_upc;
    }

    public String getPan_image() {
        return pan_image;
    }

    public void setPan_image(String pan_image) {
        this.pan_image = pan_image;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public Integer getUsermaster() {
        return usermaster;
    }

    public void setUsermaster(Integer usermaster) {
        this.usermaster = usermaster;
    }

    public String getAccessType() {
        return accessType;
    }

    public void setAccessType(String accessType) {
        this.accessType = accessType;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Object getLastLoginOn() {
        return lastLoginOn;
    }

    public void setLastLoginOn(Object lastLoginOn) {
        this.lastLoginOn = lastLoginOn;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }
}
