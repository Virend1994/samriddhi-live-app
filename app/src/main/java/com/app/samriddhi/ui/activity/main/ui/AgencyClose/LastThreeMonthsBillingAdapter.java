package com.app.samriddhi.ui.activity.main.ui.AgencyClose;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.Grievance.RequestDetailAdapter;
import com.app.samriddhi.ui.activity.main.ui.Grievance.RequestDetailModal;
import com.bumptech.glide.Glide;
import com.github.chrisbanes.photoview.PhotoView;
import com.google.android.material.card.MaterialCardView;

import java.util.List;

public class LastThreeMonthsBillingAdapter  extends RecyclerView.Adapter<LastThreeMonthsBillingAdapter.MyViewHolder>{

    private final List<LastThreeMonthBillingModal> requestDetailModals;
    Context context;
    int row_index = -1;

    public LastThreeMonthsBillingAdapter(List<LastThreeMonthBillingModal> requestDetailModals, Context context) {
        this.requestDetailModals = requestDetailModals;
        this.context = context;

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = null;
        itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.last_three_months_billing_adapter, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder Viewholder, @SuppressLint("RecyclerView") int position) {
        LastThreeMonthBillingModal listData = requestDetailModals.get(position);

        Viewholder.txtShowOpnBalance.setText(listData.getOpening_balance());
         Viewholder.txtShowMonth.setText(listData.getMonth());
         Viewholder.tvBillingCopies.setText(listData.getCopies());
         Viewholder.tvBillingamount.setText(listData.getBilling_amount());
         Viewholder.tvUnbillingAmount.setText(listData.getUnbilling_amount());
         Viewholder.tvNetAmount.setText(listData.getNet_amount());
         Viewholder.tvClosingBalance.setText(listData.getClosing_balance());
         Viewholder.tvPaymentCollection.setText(listData.getPayment());
       //Viewholder.txtSoldCopies.setText(listData.getSold());

    }

    @Override
    public int getItemCount() {
        return requestDetailModals.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtShowOpnBalance,txtShowMonth,tvBillingCopies,tvBillingamount;
        TextView tvUnbillingAmount,tvNetAmount,tvClosingBalance,tvPaymentCollection;


        MyViewHolder(View view) {
            super(view);


            txtShowOpnBalance = view.findViewById(R.id.txtShowOpnBalance);
            txtShowMonth = view.findViewById(R.id.txtShowMonth);
            tvBillingCopies = view.findViewById(R.id.tvBillingCopies);
            tvBillingamount = view.findViewById(R.id.tvBillingamount);
            tvUnbillingAmount = view.findViewById(R.id.tvUnbillingAmount);
            tvNetAmount = view.findViewById(R.id.tvNetAmount);
            tvClosingBalance = view.findViewById(R.id.tvClosingBalance);
            tvPaymentCollection = view.findViewById(R.id.tvPaymentCollection);

        }
    }


}

