package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BillOutCityUPCDistWiseResultModel extends BaseModel {

    @SerializedName("results")
    @Expose
    private List<BillOutCityUPCDistWiseListModel> salesOrgCityList = null;

    public List<BillOutCityUPCDistWiseListModel> getSalesOrgCityList() {
        return salesOrgCityList;
    }

    public void setSalesOrgCityList(List<BillOutCityUPCDistWiseListModel> salesOrgCityList) {
        this.salesOrgCityList = salesOrgCityList;
    }
}
