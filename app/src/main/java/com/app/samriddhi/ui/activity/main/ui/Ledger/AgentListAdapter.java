package com.app.samriddhi.ui.activity.main.ui.Ledger;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;
import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AppsContants;
import com.app.samriddhi.ui.activity.main.ui.LedgerActivity;
import com.google.android.material.card.MaterialCardView;

import java.util.List;

public class AgentListAdapter extends RecyclerView.Adapter<AgentListAdapter.ViewHolder> {
    Context context;
    List<LedgerModal> dataAdapters;


    public AgentListAdapter(List<LedgerModal> getDataAdapter, Context context) {
        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.agent_list_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, @SuppressLint("RecyclerView") int position) {

        LedgerModal dataAdapterOBJ = dataAdapters.get(position);
        Viewholder.setIsRecyclable(false);
        Viewholder.txtAgencyCode.setText(dataAdapterOBJ.getAgent_code());
        Viewholder.txtName.setText(dataAdapterOBJ.getAgent_name());
        Viewholder.txtSerial.setText(dataAdapterOBJ.getSerial_num());
        Viewholder.txtAmount.setText(dataAdapterOBJ.getAgent_amt());

        Viewholder.cardViewMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof ShowAgentListActivity) {

                    AppsContants.sharedpreferences=context.getSharedPreferences(AppsContants.MyPREFERENCES,Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor=AppsContants.sharedpreferences.edit();
                    editor.putString(AppsContants.AgencyName,dataAdapterOBJ.getAgent_name());
                    editor.putString(AppsContants.AgencyId,dataAdapterOBJ.getAgent_code());
                    editor.commit();
                    ((ShowAgentListActivity)context).startActivityAnimation(context, LedgerActivity.class, false);

                    // ((ShowAgentListActivity)context).startActivityAnimation(context, AgentDetailActivity.class, false);
                   // ((StatesActivity) context).startActivityAnimation(context, ShowAgentListActivity.class, false);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataAdapters.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtSerial, txtAgencyCode, txtName,txtAmount;
        MaterialCardView cardViewMain;

        public ViewHolder(View itemView) {
            super(itemView);
            txtSerial = itemView.findViewById(R.id.txtSerial);
            txtAgencyCode = itemView.findViewById(R.id.txtAgencyCode);
            txtName = itemView.findViewById(R.id.txtName);
            txtAmount = itemView.findViewById(R.id.txtAmount);
            cardViewMain = itemView.findViewById(R.id.cardViewMain);
        }
    }
}