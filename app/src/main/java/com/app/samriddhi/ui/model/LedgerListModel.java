package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LedgerListModel extends BaseModel {
    /*@SerializedName("Budat")
    @Expose
    private String budat;
    @SerializedName("Kunnr")
    @Expose
    private String kunnr;
    @SerializedName("Blart")
    @Expose
    private String blart;*/

    @SerializedName("OpenBalance")
    @Expose
   // private String OpenBalance;
    private String opAmt;

  /*  @SerializedName("ClAmt")
    @Expose
    private String clAmt;
     */

   // @SerializedName("TRNSet")
    @SerializedName("results")
    @Expose
    private List<LedgerTransModel> tRNSet = null;

   /* public String getBudat() {
        return budat;
    }

    public void setBudat(String budat) {
        this.budat = budat;
    }

    public String getKunnr() {
        return kunnr;
    }

    public void setKunnr(String kunnr) {
        this.kunnr = kunnr;
    }

    public String getBlart() {
        return blart;
    }

    public void setBlart(String blart) {
        this.blart = blart;
    }

    public String getOpAmt() {
        return opAmt;
    }

    public void setOpAmt(String opAmt) {
        this.opAmt = opAmt;
    }

    public String getClAmt() {
        return clAmt;
    }

    public void setClAmt(String clAmt) {
        this.clAmt = clAmt;
    }*/

    public List<LedgerTransModel> getTRNSet() {
        return tRNSet;
    }

    public void setTRNSet(List<LedgerTransModel> tRNSet) {
        this.tRNSet = tRNSet;
    }


    public String getOpAmt() {
        return opAmt;
    }

    public void setOpAmt(String opAmt) {
        this.opAmt = opAmt;
    }

    public List<LedgerTransModel> gettRNSet() {
        return tRNSet;
    }

    public void settRNSet(List<LedgerTransModel> tRNSet) {
        this.tRNSet = tRNSet;
    }
}
