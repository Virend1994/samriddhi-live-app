package com.app.samriddhi.ui.presenter;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.response.JsonObjectResponse;
import com.app.samriddhi.base.BasePresenter;
import com.app.samriddhi.ui.model.ServerPortModel;
import com.app.samriddhi.ui.view.ISplashView;
import com.app.samriddhi.util.Constant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashPresenter extends BasePresenter<ISplashView> {

    /**
     * provides the credentials for SAP portal.
     */
    public void getSapPortal() {
        SamriddhiApplication.getmInstance().getApiService().getSapPort(Constant.SERVER_TYPE).enqueue(new Callback<JsonObjectResponse<ServerPortModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<ServerPortModel>> call, Response<JsonObjectResponse<ServerPortModel>> response) {
                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        getView().onSuccess(response.body().body);
                    }
                    else {
                        getView().onError("");
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<ServerPortModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError("");
            }
        });
    }
}
