package com.app.samriddhi.ui.model;

import org.json.JSONArray;

public class ProposedAgencyModel {
    String otp_validation,jj_copies,main_copies,commission_per,commission_amt,asd_as_per_norms,total_copies,user_id,
            asd_collected,createdUser,agency_name,mobile,id,requestId,agenct_name,ag_req_id,ag_detail_id,
            UOHApproval,UOHRemark,UOHUpdateBy,UOHUpdateOn,SOHRemark,SOHUpdateBy,SOHUpdateOn,SOHApproval,HOApproval,HORemark,HOUpdateBy,HOUpdateOn;
    int AgencyRequestSurvey_count,AgencyRequestKYBP_count,AgencyRequestCIRReq_count,AgencyRequestCIRReqJJ_count,agreement_form_yn;
    JSONArray AgencyRequestCIRReq_set;

    public JSONArray getAgencyRequestCIRReq_set() {
        return AgencyRequestCIRReq_set;
    }

    public void setAgencyRequestCIRReq_set(JSONArray agencyRequestCIRReq_set) {
        AgencyRequestCIRReq_set = agencyRequestCIRReq_set;
    }


    public int getAgreement_form_yn() {
        return agreement_form_yn;
    }

    public void setAgreement_form_yn(int agreement_form_yn) {
        this.agreement_form_yn = agreement_form_yn;
    }


    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTotal_copies() {
        return total_copies;
    }

    public void setTotal_copies(String total_copies) {
        this.total_copies = total_copies;
    }

    public String getCreatedUser() {
        return createdUser;
    }

    public void setCreatedUser(String createdUser) {
        this.createdUser = createdUser;
    }

    public String getOtp_validation() {
        return otp_validation;
    }

    public void setOtp_validation(String otp_validation) {
        this.otp_validation = otp_validation;
    }

    public String getUOHUpdateOn() {
        return UOHUpdateOn;
    }

    public void setUOHUpdateOn(String UOHUpdateOn) {
        this.UOHUpdateOn = UOHUpdateOn;
    }

    public String getSOHRemark() {
        return SOHRemark;
    }

    public void setSOHRemark(String SOHRemark) {
        this.SOHRemark = SOHRemark;
    }

    public String getSOHUpdateBy() {
        return SOHUpdateBy;
    }

    public void setSOHUpdateBy(String SOHUpdateBy) {
        this.SOHUpdateBy = SOHUpdateBy;
    }

    public String getSOHUpdateOn() {
        return SOHUpdateOn;
    }

    public void setSOHUpdateOn(String SOHUpdateOn) {
        this.SOHUpdateOn = SOHUpdateOn;
    }

    public String getSOHApproval() {
        return SOHApproval;
    }

    public void setSOHApproval(String SOHApproval) {
        this.SOHApproval = SOHApproval;
    }

    public String getHOApproval() {
        return HOApproval;
    }

    public void setHOApproval(String HOApproval) {
        this.HOApproval = HOApproval;
    }

    public String getHORemark() {
        return HORemark;
    }

    public void setHORemark(String HORemark) {
        this.HORemark = HORemark;
    }

    public String getHOUpdateBy() {
        return HOUpdateBy;
    }

    public void setHOUpdateBy(String HOUpdateBy) {
        this.HOUpdateBy = HOUpdateBy;
    }

    public String getHOUpdateOn() {
        return HOUpdateOn;
    }

    public void setHOUpdateOn(String HOUpdateOn) {
        this.HOUpdateOn = HOUpdateOn;
    }

    public String getCommission_per() {
        return commission_per;
    }

    public String getJj_copies() {
        return jj_copies;
    }

    public void setJj_copies(String jj_copies) {
        this.jj_copies = jj_copies;
    }

    public String getMain_copies() {
        return main_copies;
    }

    public void setMain_copies(String main_copies) {
        this.main_copies = main_copies;
    }

    public void setCommission_per(String commission_per) {
        this.commission_per = commission_per;
    }

    public String getCommission_amt() {
        return commission_amt;
    }

    public void setCommission_amt(String commission_amt) {
        this.commission_amt = commission_amt;
    }

    public String getAsd_as_per_norms() {
        return asd_as_per_norms;
    }

    public void setAsd_as_per_norms(String asd_as_per_norms) {
        this.asd_as_per_norms = asd_as_per_norms;
    }

    public String getAsd_collected() {
        return asd_collected;
    }

    public void setAsd_collected(String asd_collected) {
        this.asd_collected = asd_collected;
    }

    public int getAgencyRequestCIRReqJJ_count() {
        return AgencyRequestCIRReqJJ_count;
    }

    public void setAgencyRequestCIRReqJJ_count(int agencyRequestCIRReqJJ_count) {
        AgencyRequestCIRReqJJ_count = agencyRequestCIRReqJJ_count;
    }

    public String getAgency_name() {
        return agency_name;
    }

    public void setAgency_name(String agency_name) {
        this.agency_name = agency_name;
    }

    public String getAg_req_id() {
        return ag_req_id;
    }

    public void setAg_req_id(String ag_req_id) {
        this.ag_req_id = ag_req_id;
    }

    public String getAg_detail_id() {
        return ag_detail_id;
    }

    public void setAg_detail_id(String ag_detail_id) {
        this.ag_detail_id = ag_detail_id;
    }

    public String getUOHApproval() {
        return UOHApproval;
    }

    public void setUOHApproval(String UOHApproval) {
        this.UOHApproval = UOHApproval;
    }

    public String getUOHRemark() {
        return UOHRemark;
    }

    public void setUOHRemark(String UOHRemark) {
        this.UOHRemark = UOHRemark;
    }

    public String getUOHUpdateBy() {
        return UOHUpdateBy;
    }

    public void setUOHUpdateBy(String UOHUpdateBy) {
        this.UOHUpdateBy = UOHUpdateBy;
    }

    boolean statusCheck;

    public boolean isStatusCheck() {
        return statusCheck;
    }

    public void setStatusCheck(boolean statusCheck) {
        this.statusCheck = statusCheck;
    }



    public int getAgencyRequestSurvey_count() {
        return AgencyRequestSurvey_count;
    }

    public void setAgencyRequestSurvey_count(int agencyRequestSurvey_count) {
        AgencyRequestSurvey_count = agencyRequestSurvey_count;
    }

    public int getAgencyRequestKYBP_count() {
        return AgencyRequestKYBP_count;
    }

    public void setAgencyRequestKYBP_count(int agencyRequestKYBP_count) {
        AgencyRequestKYBP_count = agencyRequestKYBP_count;
    }

    public int getAgencyRequestCIRReq_count() {
        return AgencyRequestCIRReq_count;
    }

    public void setAgencyRequestCIRReq_count(int agencyRequestCIRReq_count) {
        AgencyRequestCIRReq_count = agencyRequestCIRReq_count;
    }

    public String getAgenct_name() {
        return agenct_name;
    }

    public void setAgenct_name(String agenct_name) {
        this.agenct_name = agenct_name;
    }



    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
