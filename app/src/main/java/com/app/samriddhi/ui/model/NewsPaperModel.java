package com.app.samriddhi.ui.model;

public class NewsPaperModel {

    String pk,paperName,ag_othernewspaper_id;
    boolean isStatus;

    public boolean isStatus() {
        return isStatus;
    }

    public void setStatus(boolean status) {
        isStatus = status;
    }

    public String getAg_othernewspaper_id() {
        return ag_othernewspaper_id;
    }

    public void setAg_othernewspaper_id(String ag_othernewspaper_id) {
        this.ag_othernewspaper_id = ag_othernewspaper_id;
    }

    public String getPk() {
        return pk;
    }

    public void setPk(String pk) {
        this.pk = pk;
    }

    public String getPaperName() {
        return paperName;
    }

    public void setPaperName(String paperName) {
        this.paperName = paperName;
    }
}
