package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class YearListModel extends BaseModel {
    @SerializedName("Partner")
    @Expose
    private String partner;
    @SerializedName("AgType")
    @Expose
    private String agType;
    @SerializedName("OrdDate")
    @Expose
    private String ordDate;
    @SerializedName("Pva")
    @Expose
    private String pva;
    @SerializedName("Rate")
    @Expose
    private String rate;
    @SerializedName("GrossCopy")
    @Expose
    private String grossCopy;
    @SerializedName("FreeCopy")
    @Expose
    private String freeCopy;
    @SerializedName("PaidCopy")
    @Expose
    private String paidCopy;
    @SerializedName("Zlao")
    @Expose
    private String zlao;
    @SerializedName("Zcoo")
    @Expose
    private String zcoo;

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public String getAgType() {
        return agType;
    }

    public void setAgType(String agType) {
        this.agType = agType;
    }

    public String getOrdDate() {
        return ordDate;
    }

    public void setOrdDate(String ordDate) {
        this.ordDate = ordDate;
    }

    public String getPva() {
        return pva;
    }

    public void setPva(String pva) {
        this.pva = pva;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getGrossCopy() {
        return grossCopy;
    }

    public void setGrossCopy(String grossCopy) {
        this.grossCopy = grossCopy;
    }

    public String getFreeCopy() {
        return freeCopy;
    }

    public void setFreeCopy(String freeCopy) {
        this.freeCopy = freeCopy;
    }

    public String getPaidCopy() {
        return paidCopy;
    }

    public void setPaidCopy(String paidCopy) {
        this.paidCopy = paidCopy;
    }

    public String getZlao() {
        return zlao;
    }

    public void setZlao(String zlao) {
        this.zlao = zlao;
    }

    public String getZcoo() {
        return zcoo;
    }

    public void setZcoo(String zcoo) {
        this.zcoo = zcoo;
    }
}
