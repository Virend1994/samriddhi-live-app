package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.app.samriddhi.util.SystemUtility;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DashBoardModel extends BaseModel {
    @SerializedName("AsdFlag")
    @Expose
    public String asdFlag;
    @SerializedName("Partner")
    @Expose
    private String partner = "";
    @SerializedName("GrossCopy")
    @Expose
    private String grossCopy = "";
    @SerializedName("FreeCopy")
    @Expose
    private String freeCopy = "";
    @SerializedName("PaidCopy")
    @Expose
    private String paidCopy = "";
    @SerializedName("Zlao")
    @Expose
    private String zlao = "";
    @SerializedName("Zcoo")
    @Expose
    private String zcoo = "";
    @SerializedName("OutStd")
    @Expose
    private String outStd = "";
    @SerializedName("Asd")
    @Expose
    private String asd = "";

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public String getGrossCopy() {
        return grossCopy;
    }

    public void setGrossCopy(String grossCopy) {
        this.grossCopy = grossCopy;
    }

    public String getFreeCopy() {
        return freeCopy;
    }

    public void setFreeCopy(String freeCopy) {
        this.freeCopy = freeCopy;
    }

    public String getPaidCopy() {
        return paidCopy;
    }

    public void setPaidCopy(String paidCopy) {
        this.paidCopy = paidCopy;
    }

    public String getZlao() {
        return zlao;
    }

    public void setZlao(String zlao) {
        this.zlao = zlao;
    }

    public String getZcoo() {
        return zcoo;
    }

    public void setZcoo(String zcoo) {
        this.zcoo = zcoo;
    }

    public String getOutStd() {
        return outStd;
    }

    public void setOutStd(String outStd) {
        this.outStd = outStd;
    }

    public String getAsd() {
        return asd;
    }

    public void setAsd(String asd) {
        this.asd = asd;
    }

    public String getCalcualtedAsd() {
        if (!getAsd().isEmpty()) {
//            float value = Float.parseFloat(SystemUtility.formatDecimalValues(Float.parseFloat((getAsd()))));
            return getAsd();
        }
        return asd;
    }

    /**
     * returns the  formatted outstanding
     * @return
     */
    public String getClaculatedOutStanding() {
        if (outStd != null) {
            if (!outStd.isEmpty()) {
//                int value = SystemUtility.getFormatRoundOfValue(Float.parseFloat((getOutStd())));
//                return String.valueOf((value));
                return String.valueOf((getOutStd()));

            } else
                return outStd;
        } else return "";
    }

    public int paidCopyLength() {
        if (paidCopy != null && !paidCopy.isEmpty()) {
            return paidCopy.length();
        } else
            return 0;
    }


}
