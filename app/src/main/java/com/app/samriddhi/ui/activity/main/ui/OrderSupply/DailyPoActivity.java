package com.app.samriddhi.ui.activity.main.ui.OrderSupply;

import androidx.annotation.IdRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.DropdownMenuAdapter;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule.newRquestActivity;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.Adapter.PoAdapter;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.POModal.DailyPOModal;
import com.app.samriddhi.ui.model.DropDownModel;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.Util;
import com.github.ybq.android.spinkit.SpinKitView;
import com.google.android.material.button.MaterialButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Ref;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.Credentials;

public class DailyPoActivity extends BaseActivity implements DropdownMenuAdapter.OnMeneuClickListnser {


    RecyclerView recyclerAgency;
    LinearLayoutManager linearLayoutManager;
    List<DailyPOModal> poModalList;
    List<DailyPOModal> sendPoList;
    List<DailyPOModal> updateDifList;
    PoAdapter poAdapter;
    private String dropDownSelectType = "";
    TextView tvSaleDistrict, tvCopies;
    public ArrayList<DropDownModel> unitMaster;
    public ArrayList<DropDownModel> editionMaster;

    public List<String> reasonName;
    public List<String> reasonId;
    public static List<Integer> incDecList;
    int getTotalMinDiff = 0;
    int getTotalIncDiff = 0;
    String strUnitid = "", strUnitCode = "", strEditionName = "", strEditionId = "", strEdtionCode = "";
    boolean flagunit = false, flagEdition = false;
    public static TextView txtSalesMain, txtSalesJJ, txtRefMain, txtRefJJ;
    public static TextView txtJJDiff;
    public static TextView txtMainDiff;
    public static TextView txtTotalCopiesDiff;
    public static TextView txtSaleTotal, txtRefTotal, txtSaleDate, txtRefDate;
    EditText search, noofcopy_ac;
    RadioGroup radioGroup;
    RadioButton radioPercent, radioQTD;
    String strStatus = "";
    ProgressBar loader;
    MaterialButton save_cr;
    JSONArray jsonArray;
    JSONObject objsend;

    List<String> listZFOR;
    List<String> listJTAP;
    int remarkShowstatus = 0;
    int errorStatus = 0;
    int reasonStatus = 0;
    public static int MainTotal = 0;

    TextView txtSaleFree;
    TextView txtRefFree;
    TextView txtDiffFree;
    public static int recyclereviewPos = 0;

    int getReasonCheck = 0;
    Calendar myCalendar;
    String strDate = "";
    DatePickerDialog datePickerDialog;

    public static TextView txtInc, txtDec, txtINCDECTotal;

    int remark = 0;
    boolean setError = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_daily_p_o);
        ImageView imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        txtInc = findViewById(R.id.txtInc);
        txtDec = findViewById(R.id.txtDec);
        txtINCDECTotal = findViewById(R.id.txtINCDECTotal);

        txtRefFree = findViewById(R.id.txtRefFree);
        txtSaleFree = findViewById(R.id.txtSaleFree);
        txtDiffFree = findViewById(R.id.txtDiffFree);
        incDecList = new ArrayList<>();

        save_cr = findViewById(R.id.save_cr);
        loader = findViewById(R.id.loader);
        noofcopy_ac = findViewById(R.id.noofcopy_ac);
        txtSaleDate = findViewById(R.id.txtSaleDate);
        txtRefDate = findViewById(R.id.txtRefDate);
        search = findViewById(R.id.search);
        txtSalesMain = findViewById(R.id.txtSalesMain);
        txtSalesJJ = findViewById(R.id.txtSalesJJ);

        txtMainDiff = findViewById(R.id.txtMainDiff);
        txtJJDiff = findViewById(R.id.txtJJDiff);
        txtTotalCopiesDiff = findViewById(R.id.txtTotalCopiesDiff);
        txtSaleTotal = findViewById(R.id.txtSaleTotal);
        txtRefTotal = findViewById(R.id.txtRefTotal);

        txtRefMain = findViewById(R.id.txtRefMain);
        txtRefJJ = findViewById(R.id.txtRefJJ);

        recyclerAgency = findViewById(R.id.recyclerAgency);
        recyclerAgency.setHasFixedSize(true);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerAgency.setLayoutManager(linearLayoutManager);
        reasonName = new ArrayList<>();
        reasonId = new ArrayList<>();
        sendPoList = new ArrayList<>();
        updateDifList = new ArrayList<>();


        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, 1);
        strDate = dateFormat.format(cal.getTime());

        Log.e("sgsdgsdgsdg", strDate);
        Log.e("sgsdgsdgsdg", PreferenceManager.getAgentId(this));


        myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener dateSales = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateSale();
            }

        };


        txtSaleDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                datePickerDialog = new DatePickerDialog(DailyPoActivity.this, dateSales, myCalendar.get(Calendar.YEAR),
                        myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));

                // disable dates before two days and after two days after today
                Calendar today = Calendar.getInstance();
                Calendar twoDaysAgo = (Calendar) today.clone();
                twoDaysAgo.add(Calendar.DATE, 1);
                Calendar twoDaysLater = (Calendar) today.clone();
                twoDaysLater.add(Calendar.DATE, 5);
                datePickerDialog.getDatePicker().setMinDate(twoDaysAgo.getTimeInMillis());
                datePickerDialog.getDatePicker().setMaxDate(twoDaysLater.getTimeInMillis());
                datePickerDialog.show();
            }
        });


        tvSaleDistrict = findViewById(R.id.tvSaleDistrict);
        tvSaleDistrict.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dropDownSelectType = "unit";

                if (flagunit == true) {

                    Util.showDropDown(unitMaster, "Select unit", DailyPoActivity.this, DailyPoActivity.this::onOptionClick);
                } else {
                    alertMessage("Please wait units are loading..");
                    return;
                    // Toast.makeText(DailyPoActivity.this, "Loading units..", Toast.LENGTH_SHORT).show();
                }
            }
        });

        tvCopies = findViewById(R.id.tvCopies);
        tvCopies.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dropDownSelectType = "editions";
                if (flagEdition == true) {
                    Util.showDropDown(editionMaster, "Select Editions", DailyPoActivity.this, DailyPoActivity.this::onOptionClick);
                } else {
                    alertMessage("Please wait editions are loading..");
                    return;
                    //Toast.makeText(DailyPoActivity.this, "Loading editions..", Toast.LENGTH_SHORT).show();
                }
            }
        });

        radioGroup = findViewById(R.id.radioGroup);
        radioPercent = findViewById(R.id.radioPercent);
        radioQTD = findViewById(R.id.radioQTD);

        radioQTD.setChecked(true);
        strStatus = "Quantity";
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {

                i = radioGroup.getCheckedRadioButtonId();
                if (i == R.id.radioPercent) {
                    strStatus = "Percent";
                    Log.e("dfgkdflkgfd", "Percent");
                } else if (i == R.id.radioQTD) {
                    Log.e("dfgkdflkgfd", "Quantity");
                    strStatus = "Quantity";
                }
            }
        });

        noofcopy_ac.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //do here your stuff f
                    String strData = noofcopy_ac.getText().toString();
                    Log.e("dfhdhd", strData);
                    if (strData.length() > 0) {
                        RefreshData(strData);
                    } else {
                        strData = "0";
                        RefreshData(strData);
                    }
                    return true;
                }
                return false;
            }
        });

        getUnit();
        // ShowAgencies();

        /*Save order work starts here*/

        save_cr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                sumbitOrder(search.getText().toString());

            }
        });

        /*Save order work ends here*/
    }

    private void updateSale() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        strDate = sdf.format(myCalendar.getTime());
        txtSaleDate.setText(strDate);
        getTotalQTD();

    }


    private void alertMessage(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })

                .show();
    }


    public void GetINCDECValues() {
        Log.e("sfsfsfss", "Method called");

        int refQTD = 0;
        int proQTD = 0;
        int total = 0;

        int sumOfInc = 0;
        int sumOfDec = 0;

        sendPoList = poAdapter.getArrayData();
        if (sendPoList != null) {
            for (int i = 0; i < sendPoList.size(); i++) {
                if (!sendPoList.get(i).getItem_cat().equals("ZFOR")) {
                    Log.e("ssefsf", sendPoList.get(i).getRef_qtd());
                    Log.e("ssefsf", sendPoList.get(i).getProposed_qty());

                    if (sendPoList.get(i).getIs_approval().equals("0")) {

                        refQTD = Integer.parseInt(sendPoList.get(i).getRef_qtd());
                        proQTD = Integer.parseInt(sendPoList.get(i).getProposed_qty());
                        total = proQTD - refQTD;

                        if (total >= 0) {

                            sumOfInc += total;
                        } else {

                            sumOfDec += total;
                        }
                    } else {
                        Log.e("sdgsdgsg", "One");
                    }
                } else {

                    //Log.e("ssefsf","ZFOR");
                }


            }

            Log.e("werwerwr", total + "");

            txtInc.setText("Increase" + " " + sumOfInc + "");
            txtDec.setText("Decrease" + " " + sumOfDec + "");
            int valTotal = sumOfInc + sumOfDec;
            txtINCDECTotal.setText("Total" + " " + valTotal + "");

        }
    }


    public void UpdateMainJJCopiesDifference() {
        int saleMain = 0;

        int refMain = 0;

        int saleJJ = 0;

        int refJJ = 0;

        int saleMFree = 0;

        int refMFree = 0;

        int saleJFree = 0;

        int refJFree = 0;

        updateDifList = new ArrayList<>();
        updateDifList = poAdapter.getArrayData();

        if (updateDifList != null) {
            for (int i = 0; i < updateDifList.size(); i++) {

                if (updateDifList.get(i).getCopies_type().equals("MAIN")) {
                    if (!updateDifList.get(i).getItem_cat().equals("ZFOR")) {

                        int strSalesMain = Integer.parseInt(updateDifList.get(i).getProposed_qty());
                        saleMain += strSalesMain;
                        txtSalesMain.setText(saleMain + "");

                        int strReMain = Integer.parseInt(updateDifList.get(i).getRef_qtd());
                        refMain += strReMain;
                        txtRefMain.setText(refMain + "");

                    }

                    if (updateDifList.get(i).getItem_cat().equals("ZFOR")) {
                        int strSMain = Integer.parseInt(updateDifList.get(i).getProposed_qty());
                        int strRMain = Integer.parseInt(updateDifList.get(i).getRef_qtd());
                        saleMFree += strSMain;
                        refMFree += strRMain;
                        txtSaleFree.setText(saleMFree + "");
                        txtRefFree.setText(refMFree + "");
                    }

                } else {

                    if (!updateDifList.get(i).getItem_cat().equals("ZFOR")) {

                        int strJJMain = Integer.parseInt(updateDifList.get(i).getProposed_qty());
                        saleJJ += strJJMain;
                        txtSalesJJ.setText(saleJJ + "");
                        int strRefJJ = Integer.parseInt(updateDifList.get(i).getRef_qtd());
                        refJJ += strRefJJ;
                        txtRefJJ.setText(refJJ + "");
                    }

                    if (updateDifList.get(i).getItem_cat().equals("ZFOR")) {
                        int JJMain = Integer.parseInt(updateDifList.get(i).getProposed_qty());
                        int RefJJ = Integer.parseInt(updateDifList.get(i).getRef_qtd());
                        saleMFree += JJMain;
                        refMFree += RefJJ;
                        txtSaleFree.setText(saleMFree + "");
                        txtRefFree.setText(refMFree + "");
                    }

                }
            }
        }

        int FreeDiff = saleMFree - refMFree;
        txtDiffFree.setText(FreeDiff + "");
        if (FreeDiff < 0) {
            txtDiffFree.setTextColor(getResources().getColor(R.color.red));
        } else {
            txtDiffFree.setTextColor(getResources().getColor(R.color.green_button_color));

        }

        int mainDiff = saleMain - refMain;
        txtMainDiff.setText(mainDiff + "");
        if (mainDiff < 0) {
            txtMainDiff.setTextColor(getResources().getColor(R.color.red));
        } else {
            txtMainDiff.setTextColor(getResources().getColor(R.color.green_button_color));

        }

        int jjDiff = saleJJ - refJJ;
        txtJJDiff.setText(jjDiff + "");
        if (jjDiff < 0) {
            txtJJDiff.setTextColor(getResources().getColor(R.color.red));
        } else {
            txtJJDiff.setTextColor(getResources().getColor(R.color.green_button_color));

        }


        Log.e("tyititityit", saleJJ + "");
        Log.e("tyititityit", saleJFree + "");
        Log.e("tyititityit", refJJ + "");
        Log.e("tyititityit", refJFree + "");


        int saleTotalCopies = saleMain + saleJJ + saleMFree;
        txtSaleTotal.setText(saleTotalCopies + "");

        int refTotalCopies = refMain + refJJ + refMFree;

        txtRefTotal.setText(refTotalCopies + "");


        int copiesDiff = saleTotalCopies - refTotalCopies;
        txtTotalCopiesDiff.setText(copiesDiff + "");


        if (copiesDiff < 0) {
            getTotalMinDiff = copiesDiff;
            txtTotalCopiesDiff.setTextColor(getResources().getColor(R.color.red));
        } else {
            getTotalIncDiff = copiesDiff;
            txtTotalCopiesDiff.setTextColor(getResources().getColor(R.color.green_button_color));
        }


    }


    public void sumbitOrder(String strSearch) {
        listZFOR = new ArrayList<>();
        listJTAP = new ArrayList<>();

        ArrayList<Integer> listRemark = new ArrayList<>();
        ArrayList<Integer> listReason = new ArrayList<>();
        ArrayList<Integer> listError = new ArrayList<>();

        sendPoList = poAdapter.getArrayData();
        if (sendPoList != null) {
            jsonArray = new JSONArray();
            objsend = new JSONObject();

            Log.e("asfafasfaf", sendPoList.size() + "");

            for (int i = 0; i < sendPoList.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                try {


                    if (sendPoList.get(i).getIs_approval().equals("1")) {


                        Log.e("wetwetwet", sendPoList.get(i).getProposed_qty());

                        try {
                            if (sendPoList.get(i).getError().equals("true")) {

                                // errorStatus = 1; /*Error*/
                                alertMessage("Sold to party cannot be above");
                                setError = true;
                                break;
                            } else {

                                //errorStatus = 0;
                                setError = false; /*No Error*/
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        listZFOR.add(sendPoList.get(i).getIs_approval());
                        jsonObject.put("remark", sendPoList.get(i).getRemark());
                        jsonObject.put("pick_date", sendPoList.get(i).getPick_date());
                        jsonObject.put("reason_qty_change", sendPoList.get(i).getReason_qty_change());
                        jsonObject.put("pva", sendPoList.get(i).getPva());
                        jsonObject.put("cust_name", sendPoList.get(i).getCust_name());
                        jsonObject.put("city_name", sendPoList.get(i).getCity_name());
                        jsonObject.put("edition_name", sendPoList.get(i).getEdi_name());
                        jsonObject.put("sold_to_party", sendPoList.get(i).getSold_to_party());
                        jsonObject.put("ship_to_party", sendPoList.get(i).getShip_to_party());
                        jsonObject.put("vbeln", sendPoList.get(i).getVbeln());
                        jsonObject.put("posnr", sendPoList.get(i).getPosnr());
                        jsonObject.put("pstyv", sendPoList.get(i).getPstyv());
                        jsonObject.put("proposed_qty", sendPoList.get(i).getProposed_qty());
                        jsonObject.put("sales_proposed_qty", sendPoList.get(i).getSale_qtd());
                        jsonObject.put("refn_proposed_qty", sendPoList.get(i).getRef_qtd());
                        jsonObject.put("main_jj", sendPoList.get(i).getCopies_type());
                        jsonObject.put("per_inc_dec", sendPoList.get(i).getInc_dsc());
                        jsonObject.put("is_approval", sendPoList.get(i).getIs_approval());
                        jsonObject.put("sale_po_date", strDate);
                        jsonObject.put("reff_po_date", txtRefDate.getText().toString());
                        jsonObject.put("create_user_id", PreferenceManager.getAgentId(DailyPoActivity.this));
                        jsonArray.put(jsonObject);
                    } else {

                        listJTAP.add(sendPoList.get(i).getIs_approval());


                        Log.e("dfggdgfgdf", "Remark" + " " + sendPoList.get(i).getRemark());
                        Log.e("sefdfsfsdf", "Size" + " " + listJTAP.size() + "");

                        try {
                            if (!sendPoList.get(i).getRemark().equals("")) {
                                listRemark.add(1);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        try {
                            if (sendPoList.get(i).getError().equals("true")) {

                                //errorStatus = 1; /*Error*/
                                alertMessage("Sold to party cannot be above");
                                setError = true;
                                break;
                            } else {

                                // errorStatus = 0;
                                setError = false; /*No Error*/
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        try {

                            if (!sendPoList.get(i).getReason_qty_change().equals("false")) {
                                listReason.add(2);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        Log.e("sgsdgsgsdg", sendPoList.get(i).getProposed_qty());
                        Log.e("sgsdgsgsdg", sendPoList.get(i).getRemark());

                        jsonObject.put("remark", sendPoList.get(i).getRemark());
                        jsonObject.put("pick_date", sendPoList.get(i).getPick_date());
                        jsonObject.put("reason_qty_change", sendPoList.get(i).getReason_qty_change());
                        jsonObject.put("pva", sendPoList.get(i).getPva());
                        jsonObject.put("cust_name", sendPoList.get(i).getCust_name());
                        jsonObject.put("city_name", sendPoList.get(i).getCity_name());
                        jsonObject.put("edition_name", sendPoList.get(i).getEdi_name());
                        jsonObject.put("sold_to_party", sendPoList.get(i).getSold_to_party());
                        jsonObject.put("ship_to_party", sendPoList.get(i).getShip_to_party());
                        jsonObject.put("vbeln", sendPoList.get(i).getVbeln());
                        jsonObject.put("posnr", sendPoList.get(i).getPosnr());
                        jsonObject.put("pstyv", sendPoList.get(i).getPstyv());
                        jsonObject.put("proposed_qty", sendPoList.get(i).getProposed_qty());
                        // jsonObject.put("proposed_qty","0");
                        jsonObject.put("sales_proposed_qty", sendPoList.get(i).getSale_qtd());
                        jsonObject.put("refn_proposed_qty", sendPoList.get(i).getRef_qtd());
                        jsonObject.put("main_jj", sendPoList.get(i).getCopies_type());
                        jsonObject.put("per_inc_dec", sendPoList.get(i).getInc_dsc());
                        jsonObject.put("is_approval", sendPoList.get(i).getIs_approval());
                        jsonObject.put("sale_po_date", strDate);
                        jsonObject.put("reff_po_date", txtRefDate.getText().toString());
                        jsonObject.put("create_user_id", PreferenceManager.getAgentId(DailyPoActivity.this));
                        jsonArray.put(jsonObject);
                    }

                } catch (JSONException e) {
                    Log.e("sdfsdfsfsd", e.getMessage());
                }
            }
            try {
                if (setError == false) { /*No error of sold to party */

                    objsend.putOpt("selected", jsonArray);
                    Log.e("afsafafasa", objsend.toString());

                    Dialog dialog = new Dialog(DailyPoActivity.this);
                    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

                    dialog.setCancelable(false);
                    dialog.setContentView(R.layout.order_submit_popup);

                    TextView txtTotal = dialog.findViewById(R.id.txtTotal);
                    TextView txtAuto = dialog.findViewById(R.id.txtAuto);
                    TextView txtNeedToApprove = dialog.findViewById(R.id.txtNeedToApprove);
                    txtTotal.setText(sendPoList.size() + "");
                    txtAuto.setText(listZFOR.size() + "");
                    txtNeedToApprove.setText(listJTAP.size() + "");

                    if (strSearch.length() != 0) {
                        txtTotal.setText(poModalList.size() + "");
                        int needToApprove = poModalList.size() - listJTAP.size();
                        txtAuto.setText(needToApprove + "");
                        txtNeedToApprove.setText(listJTAP.size() + "");

                    } else {
                        txtTotal.setText(sendPoList.size() + "");
                        txtAuto.setText(listZFOR.size() + "");
                        txtNeedToApprove.setText(listJTAP.size() + "");
                    }
                    if (listJTAP.size() == 0) {

                        reasonStatus = 0;/*reason selected*/
                    }
                    Button btn_submit = dialog.findViewById(R.id.btn_submit);
                    btn_submit.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            if (listRemark.size() != listJTAP.size()) {
                                dialog.dismiss();
                                Toast.makeText(DailyPoActivity.this, "Remark cannot be empty", Toast.LENGTH_SHORT).show();
                            }
                       /* else if (errorStatus == 1) {

                            dialog.dismiss();
                            Toast.makeText(DailyPoActivity.this, "Sold to party cannot be above", Toast.LENGTH_SHORT).show();
                        } */
                            else if (listReason.size() != listJTAP.size()) {

                                dialog.dismiss();
                                Toast.makeText(DailyPoActivity.this, "Select Reason", Toast.LENGTH_SHORT).show();
                            } else {
                                 //dialog.dismiss();
                                 //Toast.makeText(DailyPoActivity.this, "Submit", Toast.LENGTH_SHORT).show();

                                SendOrderDetails(objsend);
                            }
                        }
                    });
                    dialog.show();
                    dialog.setCanceledOnTouchOutside(true);


                }
            } catch (JSONException e) {
                Log.e("sdfsdfsfsd", e.getMessage());
            }

        }
    }


    public void SendOrderDetails(JSONObject objsend) {



        enableLoadingBar(true);
        AndroidNetworking.post(Constant.BASE_PORTAL_URL + "dash/api/send-generate-order")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .addJSONObjectBody(objsend)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            Log.e("sfsdfsfsf", response.toString());
                            if (response.getString("message").equals("Order Generate Successfully :")) {
                                enableLoadingBar(false);
                                Toast.makeText(DailyPoActivity.this, "Order generated Successfully", Toast.LENGTH_SHORT).show();
                                startActivity(new Intent(DailyPoActivity.this, DailyPoActivity.class));
                                finish();
                            } else {

                                enableLoadingBar(false);
                                Toast.makeText(DailyPoActivity.this, "Something went wrong", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {
                            enableLoadingBar(false);
                            Log.e("sjdfhskfsd", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        enableLoadingBar(false);
                        Log.e("sjdfhskfsd", anError.getMessage());
                    }
                });


    }


    public void RefreshData(String strData) {

        loader.setVisibility(View.VISIBLE);

        txtSalesMain.setText("0");
        txtSalesJJ.setText("0");
        txtRefMain.setText("0");
        txtRefJJ.setText("0");
        txtSaleFree.setText("0");
        txtRefFree.setText("0");
        txtSaleTotal.setText("0");
        txtRefTotal.setText("0");
        txtMainDiff.setText("0");
        txtJJDiff.setText("0");
        txtTotalCopiesDiff.setText("0");

        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("unit_filter", strUnitCode);
            jsonObject.put("edition_filter", strEdtionCode);
            jsonObject.put("sale_po_pick_date", strDate);
            jsonObject.put("agent_id", PreferenceManager.getAgentId(this));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(Constant.BASE_PORTAL_URL + "dash/api/generate-order-data")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        reasonName = new ArrayList<>();
                        reasonName.add("Select Reason");
                        reasonId = new ArrayList<>();
                        reasonId.add("false");
                        int totalMain = 0;
                        int saleMainJTAP = 0;
                        int saleMainZFOR = 0;
                        int refMain = 0;

                        int totalJJ = 0;
                        int saleJJ = 0;
                        int refJJ = 0;
                        int getPer = 0;

                        int saleMFree = 0;
                        int refMFree = 0;
                        int saleJFree = 0;
                        int refJFree = 0;

                        try {
                            Log.e("sfsdfsfsf", response.toString());
                            poModalList = new ArrayList<>();
                            txtSaleDate.setText(response.getString("sales_po_date"));
                            txtRefDate.setText(response.getString("refn_po_date"));

                            JSONArray objReason = new JSONArray(response.getString("status_flags"));
                            for (int i = 0; i < objReason.length(); i++) {
                                JSONObject objStr = objReason.getJSONObject(i);
                                reasonName.add(objStr.getString("name"));
                                reasonId.add(objStr.getString("id"));
                            }

                            JSONArray objData = new JSONArray(response.getString("portal_data"));
                            for (int i = 0; i < objData.length(); i++) {
                                JSONObject objStr = objData.getJSONObject(i);

                                DailyPOModal dailyPOModal = new DailyPOModal();
                                dailyPOModal.setAgency_id(i + "");
                                dailyPOModal.setEdi_name(objStr.getString("edition_name"));
                                dailyPOModal.setCopies_type(objStr.getString("main_jj"));

                                dailyPOModal.setRemark(objStr.getString("remark"));
                                dailyPOModal.setPick_date(objStr.getString("pick_date"));
                                dailyPOModal.setReason_qty_change(objStr.getString("reason_qty_change"));
                                dailyPOModal.setPva(objStr.getString("pva"));
                                dailyPOModal.setCust_name(objStr.getString("cust_name"));
                                dailyPOModal.setCity_name(objStr.getString("city_name"));
                                dailyPOModal.setVbeln(objStr.getString("vbeln"));
                                dailyPOModal.setPosnr(objStr.getString("posnr"));
                                dailyPOModal.setPstyv(objStr.getString("pstyv"));
                                dailyPOModal.setProposed_qty(objStr.getString("proposed_qty"));
                                dailyPOModal.setScalabal_copies(objStr.getString("scalabal_copies"));
                                dailyPOModal.setPerincreased("0");
                                dailyPOModal.setIs_approval("1");
                                dailyPOModal.setSold_to_party(objStr.getString("sold_to_party"));
                                dailyPOModal.setShip_to_party(objStr.getString("ship_to_party"));
                                dailyPOModal.setItem_cat(objStr.getString("pstyv"));
                                dailyPOModal.setRef_qtd(objStr.getString("refn_proposed_qty"));
                                dailyPOModal.setSale_qtd(objStr.getString("sales_proposed_qty"));
                                dailyPOModal.setPick_date(objStr.getString("pick_date"));
                                dailyPOModal.setBase_copy(objStr.getString("base_copy"));
                                dailyPOModal.setBase_min(objStr.getString("base_min"));
                                dailyPOModal.setBase_max(objStr.getString("base_max"));
                                dailyPOModal.setInc_dsc(strData);
                                dailyPOModal.setQuantityPerStatus("1");
                                poModalList.add(dailyPOModal);   /*Yha se */

                                /*Total qunatity work starts from  here*/

                                if (objStr.getString("main_jj").equals("MAIN")) { /*Main*/

                                    if (strStatus.equals("Quantity")) {

                                        if (objStr.getString("pstyv").equals("ZFOR")) {

                                            int strSalesMain = objStr.getInt("proposed_qty");
                                            saleMainZFOR += strSalesMain;

                                        } else {
                                            int strSalesMain = objStr.getInt("proposed_qty");
                                            int totalINCQTD = Integer.parseInt(strData);
                                            saleMainJTAP += strSalesMain + totalINCQTD;
                                        }
                                        totalMain = saleMainJTAP + saleMainZFOR;
                                        txtSalesMain.setText(totalMain + "");


                                        int strReMain = objStr.getInt("refn_proposed_qty");
                                        refMain += strReMain;
                                        txtRefMain.setText(refMain + "");
                                        if (objStr.getString("pstyv").equals("ZFOR")) {
                                            int totalINCQTD = Integer.parseInt(strData);
                                            saleMFree += strReMain + totalINCQTD;
                                            refMFree += strReMain + totalINCQTD;
                                            txtSaleFree.setText(saleMFree + "");
                                            txtRefFree.setText(refMFree + "");
                                        }

                                    } else {


                                        if (objStr.getString("pstyv").equals("ZFOR")) {

                                            int strSalesMain = objStr.getInt("proposed_qty");
                                            saleMainZFOR += strSalesMain;
                                            dailyPOModal.setInc_dsc("0");

                                        } else {
                                            // int strSalesMain = objStr.getInt("proposed_qty");
                                            int strSalesMain = objStr.getInt("proposed_qty");
                                            int totalINCQTD = Integer.parseInt(strData);
                                            // saleMainJTAP += strSalesMain + totalINCQTD;
                                            int per = strSalesMain * totalINCQTD / 100;
                                            getPer = Math.round(per);
                                            saleMainJTAP += strSalesMain + getPer;
                                        }
                                        totalMain = saleMainJTAP + saleMainZFOR;
                                        txtSalesMain.setText(totalMain + "");

                                        int strReMain = objStr.getInt("refn_proposed_qty");
                                        refMain += strReMain;
                                        txtRefMain.setText(refMain + "");

                                        dailyPOModal.setInc_dsc(getPer + "");
                                    }


                                } else {  /*JJ*/

                                    int strJJMain = objStr.getInt("proposed_qty");
                                    saleJJ += strJJMain;
                                    txtSalesJJ.setText(saleJJ + "");

                                    int strRefJJ = objStr.getInt("refn_proposed_qty");
                                    refJJ += strRefJJ;
                                    txtRefJJ.setText(refJJ + "");

                                    if (objStr.getString("pstyv").equals("ZFOR")) {
                                        int totalINCQTD = Integer.parseInt(strData);
                                        saleJFree += strJJMain + totalINCQTD;
                                        refJFree += strRefJJ + totalINCQTD;
                                        txtSaleFree.setText(saleJFree + "");
                                        txtRefFree.setText(refJFree + "");
                                    }

                                }


                            }


                            // int FreeDiff=saleMFree-refMFree;
                            // txtDiffFree.setText(FreeDiff+"");

                            int mainDiff = totalMain - refMain;
                            txtMainDiff.setText(mainDiff + "");
                            if (mainDiff < 0) {
                                txtMainDiff.setTextColor(getResources().getColor(R.color.red));
                            } else {
                                txtMainDiff.setTextColor(getResources().getColor(R.color.green_button_color));

                            }

                            int jjDiff = saleJJ - refJJ;
                            txtJJDiff.setText(jjDiff + "");
                            if (jjDiff < 0) {
                                txtJJDiff.setTextColor(getResources().getColor(R.color.red));
                            } else {
                                txtJJDiff.setTextColor(getResources().getColor(R.color.green_button_color));

                            }

                            int saleTotalCopies = totalMain + saleJJ;
                            txtSaleTotal.setText(saleTotalCopies + "");

                            int refTotalCopies = refMain + refJJ;
                            txtRefTotal.setText(refTotalCopies + "");


                            int copiesDiff = saleTotalCopies - refTotalCopies;
                            txtTotalCopiesDiff.setText(copiesDiff + "");

                            if (copiesDiff < 0) {

                                getTotalMinDiff = copiesDiff;
                                txtTotalCopiesDiff.setTextColor(getResources().getColor(R.color.red));
                            } else {
                                getTotalIncDiff = copiesDiff;
                                txtTotalCopiesDiff.setTextColor(getResources().getColor(R.color.green_button_color));
                            }


                            MainTotal = totalMain;

                            SetAdapter();
                            loader.setVisibility(View.GONE);
                            Log.e("asfafasfaf", poModalList.size() + "");

                        } catch (Exception ex) {
                            loader.setVisibility(View.GONE);
                            Log.e("sjdfhskfsd", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        loader.setVisibility(View.GONE);
                        Log.e("sjdfhskfsd", anError.getMessage());
                    }
                });


    }


    @Override
    public void onOptionClick(DropDownModel dataList) {

        if (dropDownSelectType.equalsIgnoreCase("unit")) {
            Util.hideDropDown();

            tvSaleDistrict.setText(dataList.getName());
            strUnitCode = dataList.getCode();
            getEdition();
            // getTotalQTD();

        } else if (dropDownSelectType.equalsIgnoreCase("editions")) {
            Util.hideDropDown();

            tvCopies.setText(dataList.getName());
            strEditionId = dataList.getId();
            strEdtionCode = dataList.getCode();
            incDecList = new ArrayList<>();
            getTotalQTD();
        }
    }
    /* public void ShowAgencies() {
        poModalList = new ArrayList<>();
        for (int i = 0; i < 6; i++) {

            DailyPOModal dailyPOModal = new DailyPOModal();
            dailyPOModal.setAgency_id("1");
            poModalList.add(dailyPOModal);
        }

        SetAdapter();
    }*/


    public void getTotalQTD() {

        loader.setVisibility(View.VISIBLE);

        txtSalesMain.setText("0");
        txtSalesJJ.setText("0");
        txtRefMain.setText("0");
        txtRefJJ.setText("0");
        txtSaleFree.setText("0");
        txtRefFree.setText("0");
        txtSaleTotal.setText("0");
        txtRefTotal.setText("0");
        txtMainDiff.setText("0");
        txtJJDiff.setText("0");
        txtTotalCopiesDiff.setText("0");

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("unit_filter", strUnitCode);
            jsonObject.put("edition_filter", strEdtionCode);
            jsonObject.put("sale_po_pick_date", strDate);
            jsonObject.put("agent_id", PreferenceManager.getAgentId(this));
            Log.e("sfsafasda", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(Constant.BASE_PORTAL_URL + "dash/api/generate-order-data")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        reasonName = new ArrayList<>();
                        reasonName.add("Select Reason");
                        reasonId = new ArrayList<>();
                        reasonId.add("false");

                        int saleMain = 0;

                        int refMain = 0;

                        int saleJJ = 0;

                        int refJJ = 0;

                        int saleMFree = 0;

                        int refMFree = 0;

                        int saleJFree = 0;

                        int refJFree = 0;

                        try {

                            poModalList = new ArrayList<>();
                            txtSaleDate.setText(response.getString("sales_po_date"));
                            txtRefDate.setText(response.getString("refn_po_date"));

                            JSONArray objReason = new JSONArray(response.getString("status_flags"));
                            for (int j = 0; j < objReason.length(); j++) {
                                JSONObject objStr = objReason.getJSONObject(j);
                                reasonName.add(objStr.getString("name"));
                                reasonId.add(objStr.getString("id"));
                            }

                            JSONArray objData = new JSONArray(response.getString("portal_data"));

                            for (int i = 0; i < objData.length(); i++) {
                                JSONObject objStr = objData.getJSONObject(i);

                                DailyPOModal dailyPOModal = new DailyPOModal();
                                dailyPOModal.setAgency_id(i + "");
                                dailyPOModal.setEdi_name(objStr.getString("edition_name"));
                                dailyPOModal.setRemark(objStr.getString("remark"));
                                dailyPOModal.setPick_date(objStr.getString("pick_date"));
                                dailyPOModal.setReason_qty_change(objStr.getString("reason_qty_change"));
                                dailyPOModal.setPva(objStr.getString("pva"));
                                dailyPOModal.setCust_name(objStr.getString("cust_name"));
                                dailyPOModal.setCity_name(objStr.getString("city_name"));
                                dailyPOModal.setVbeln(objStr.getString("vbeln"));
                                dailyPOModal.setPosnr(objStr.getString("posnr"));
                                dailyPOModal.setPstyv(objStr.getString("pstyv"));
                                dailyPOModal.setProposed_qty(objStr.getString("proposed_qty"));
                                //  dailyPOModal.setScalabal_copies("181");
                                dailyPOModal.setScalabal_copies(objStr.getString("scalabal_copies"));
                                dailyPOModal.setPerincreased("0");
                                dailyPOModal.setIs_approval("1");

                                dailyPOModal.setCopies_type(objStr.getString("main_jj"));
                                dailyPOModal.setSold_to_party(objStr.getString("sold_to_party"));
                                dailyPOModal.setShip_to_party(objStr.getString("ship_to_party"));
                                dailyPOModal.setItem_cat(objStr.getString("pstyv"));
                                dailyPOModal.setRef_qtd(objStr.getString("refn_proposed_qty"));
                                dailyPOModal.setSale_qtd(objStr.getString("sales_proposed_qty"));
                                dailyPOModal.setBase_copy(objStr.getString("base_copy"));
                                dailyPOModal.setBase_min(objStr.getString("base_min"));
                                dailyPOModal.setBase_max(objStr.getString("base_max"));
                                dailyPOModal.setInc_dsc("0");
                                dailyPOModal.setQuantityPerStatus("0");
                                poModalList.add(dailyPOModal);

                                /*Total qunatity work starts from  here*/

                                if (objStr.getString("pstyv").equals("ZFOR")) {
                                    int strSalesMain = objStr.getInt("proposed_qty");
                                    int strReMain = objStr.getInt("refn_proposed_qty");
                                    saleMFree += strSalesMain;
                                    refMFree += strReMain;
                                    txtSaleFree.setText(saleMFree + "");
                                    txtRefFree.setText(refMFree + "");
                                }
                                if (objStr.getString("main_jj").equals("MAIN")) {

                                    /* ZFOR copies are free copies */
                                    if (!objStr.getString("pstyv").equals("ZFOR")) {  /*not allowing to add free copies in main copies*/

                                        int strSalesMain = objStr.getInt("proposed_qty");
                                        saleMain += strSalesMain;
                                        txtSalesMain.setText(saleMain + "");
                                        int strReMain = objStr.getInt("refn_proposed_qty");
                                        refMain += strReMain;
                                        txtRefMain.setText(refMain + "");
                                    }


                                } else {
                                    /* ZFOR copies are free copies */

                                    if (!objStr.getString("pstyv").equals("ZFOR")) {  /*not allowing to add free copies in main copies*/
                                        int strJJMain = objStr.getInt("proposed_qty");
                                        saleJJ += strJJMain;
                                        txtSalesJJ.setText(saleJJ + "");

                                        int strRefJJ = objStr.getInt("refn_proposed_qty");
                                        refJJ += strRefJJ;
                                        txtRefJJ.setText(refJJ + "");

                                    }
                                }
                            }

                            int FreeDiff = saleMFree - refMFree;
                            txtDiffFree.setText(FreeDiff + "");
                            if (FreeDiff < 0) {
                                txtDiffFree.setTextColor(getResources().getColor(R.color.red));
                            } else {
                                txtDiffFree.setTextColor(getResources().getColor(R.color.green_button_color));

                            }
                            int mainDiff = saleMain - refMain;
                            txtMainDiff.setText(mainDiff + "");
                            if (mainDiff < 0) {
                                txtMainDiff.setTextColor(getResources().getColor(R.color.red));
                            } else {
                                txtMainDiff.setTextColor(getResources().getColor(R.color.green_button_color));

                            }

                            int jjDiff = saleJJ - refJJ;
                            txtJJDiff.setText(jjDiff + "");
                            if (jjDiff < 0) {
                                txtJJDiff.setTextColor(getResources().getColor(R.color.red));
                            } else {
                                txtJJDiff.setTextColor(getResources().getColor(R.color.green_button_color));

                            }

                            int saleTotalCopies = saleMain + saleJJ + saleMFree;
                            txtSaleTotal.setText(saleTotalCopies + "");

                            int refTotalCopies = refMain + refJJ + refMFree;
                            txtRefTotal.setText(refTotalCopies + "");

                            int copiesDiff = saleTotalCopies - refTotalCopies;
                            txtTotalCopiesDiff.setText(copiesDiff + "");


                            if (copiesDiff < 0) {
                                getTotalMinDiff = copiesDiff;
                                txtTotalCopiesDiff.setTextColor(getResources().getColor(R.color.red));
                            } else {
                                getTotalIncDiff = copiesDiff;
                                txtTotalCopiesDiff.setTextColor(getResources().getColor(R.color.green_button_color));
                            }

                            MainTotal = saleMain;
                            SetAdapter();
                            loader.setVisibility(View.GONE);

                        } catch (Exception ex) {
                            loader.setVisibility(View.GONE);
                            Log.e("sjdfhskfsd", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        loader.setVisibility(View.GONE);
                        Log.e("sjdfhskfsd", anError.getMessage());
                    }
                });
    }


    public void SetAdapter() {
        if (poModalList.size() == 0) {
            save_cr.setVisibility(View.GONE);
        } else {
            save_cr.setVisibility(View.VISIBLE);
        }
        poAdapter = new PoAdapter(poModalList, DailyPoActivity.this, reasonName, reasonId);
        recyclerAgency.setAdapter(poAdapter);
        recyclerAgency.setHasFixedSize(true);
        recyclerAgency.scrollToPosition(recyclereviewPos);
        GetINCDECValues();
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                String text = search.getText().toString().toLowerCase(Locale.getDefault());
                Log.e("bhs==>>", text);

                poAdapter.getFilter().filter(cs);
                poAdapter.notifyDataSetChanged();

                if (text.length() < 1) {

                    SetAdapter();
                }

            }


            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
    }

    public void getUnit() {

        flagunit = false;
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("unit_filter", "");
            jsonObject.put("edition_filter", "");
            jsonObject.put("sale_po_pick_date", strDate);
            jsonObject.put("agent_id", PreferenceManager.getAgentId(this));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(Constant.BASE_PORTAL_URL + "dash/api/generate-order-data")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {


                            unitMaster = new ArrayList<>();
                            Log.e("sfsdfsfsf", response.toString());
                            JSONArray objData = new JSONArray(response.getString("units"));
                            for (int i = 0; i < objData.length(); i++) {

                                JSONObject objStr = objData.getJSONObject(i);
                                DropDownModel dropDownModel = new DropDownModel();
                                dropDownModel.setId(objStr.getString("unit_id"));
                                dropDownModel.setDescription(objStr.getString("unit_name"));
                                dropDownModel.setName(objStr.getString("unit_name"));
                                dropDownModel.setCode(objStr.getString("unit_code"));
                                unitMaster.add(dropDownModel);
                            }
                            flagunit = true;

                        } catch (Exception ex) {
                            Log.e("sjdfhskfsd", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        Log.e("sjdfhskfsd", anError.getMessage());



                    }
                });
    }

    public void getEdition() {
        flagEdition = false;
        Log.e("sfasfasaasfafa", strUnitCode);

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("unit_filter", strUnitCode);
            jsonObject.put("edition_filter", "");
            jsonObject.put("sale_po_pick_date", strDate);
            jsonObject.put("agent_id", PreferenceManager.getAgentId(this));

            Log.e("SDfsdfsdfsdf", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post(Constant.BASE_PORTAL_URL + "dash/api/generate-order-data")
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            editionMaster = new ArrayList<>();
                            DropDownModel dropDownModels = new DropDownModel();
                            dropDownModels.setId("");
                            dropDownModels.setDescription("All");
                            dropDownModels.setName("All");
                            dropDownModels.setCode("");
                            editionMaster.add(dropDownModels);
                            Log.e("sfasfasaasfafa", response.toString());
                            JSONArray objData = new JSONArray(response.getString("editions"));
                            for (int i = 0; i < objData.length(); i++) {
                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();
                                dropDownModel.setId(objStr.getString("edition_id"));
                                dropDownModel.setDescription(objStr.getString("edition_name"));
                                dropDownModel.setName(objStr.getString("edition_name"));
                                dropDownModel.setCode(objStr.getString("edition_code"));
                                editionMaster.add(dropDownModel);
                            }
                            flagEdition = true;

                        } catch (Exception ex) {
                            Log.e("sfasfasaasfafa", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        Log.e("sfasfasaasfafa", anError.getMessage());
                    }
                });
    }
}
