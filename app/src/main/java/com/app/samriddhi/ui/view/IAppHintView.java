package com.app.samriddhi.ui.view;

import com.app.samriddhi.ui.model.HintModel;

import java.util.List;

public interface IAppHintView extends IView {
    void onSuccess(List<HintModel> msg);
}
