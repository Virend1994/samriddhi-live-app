package com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule;

import android.content.SharedPreferences;
import android.os.Bundle;

import com.app.samriddhi.R;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.DropdownMenuAdapter;
import com.app.samriddhi.base.adapter.Intensification_Adapter;
import com.app.samriddhi.base.adapter.Perposed_Agency_Adapter;
import com.app.samriddhi.base.adapter.RecyclerViewArrayAdapter;

import com.app.samriddhi.databinding.ActivityNewAgencyRquestBinding;

import com.app.samriddhi.prefernces.PreferenceManager;

import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AppsContants;
import com.app.samriddhi.ui.model.AgencyRequestAGDetailSetModel;
import com.app.samriddhi.ui.model.AgencyRequestReasonSetModel;
import com.app.samriddhi.ui.model.BankListModel;
import com.app.samriddhi.ui.model.DropDownModel;
import com.app.samriddhi.ui.model.NewAgencyModel;
import com.app.samriddhi.ui.model.NewsPaperModel;
import com.app.samriddhi.ui.model.ProposedAgencyModel;
import com.app.samriddhi.ui.model.RootModel;

import com.app.samriddhi.ui.presenter.NewAgencyPresenter;
import com.app.samriddhi.ui.view.INewAgencyListView;
import com.app.samriddhi.util.AlertUtility;
import com.app.samriddhi.util.Globals;
import com.app.samriddhi.util.Util;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class newRquestActivity extends BaseActivity implements INewAgencyListView,
        DropdownMenuAdapter.OnMeneuClickListnser, Perposed_Agency_Adapter.OnclickListener,
        RecyclerViewArrayAdapter.OnItemClickListener<BankListModel>, TextWatcher {

    ActivityNewAgencyRquestBinding demoBinding;
    NewAgencyPresenter mPresenter;
    public ArrayList<DropDownModel> stateMasterArray;
    public ArrayList<DropDownModel> unitMaster;
    public ArrayList<DropDownModel> salesOfficeListJJ;
    public ArrayList<DropDownModel> salesOfficeListMain;
    public ArrayList<DropDownModel> salesClusterList;
    public ArrayList<DropDownModel> cityMaster;
    private static AlertDialog alertDialog;
    public ArrayList<DropDownModel> locationMaster;
    public ArrayList<DropDownModel> tvTownMaster;
    public ArrayList<String> s_no1 = new ArrayList<String>();
    private String dropSelectType = "";
    public ArrayList<String> s_no2 = new ArrayList<String>();
    public ArrayList<AgencyRequestReasonSetModel> agencyRequestReasonSetModelArrayList;
    public ArrayList<AgencyRequestAGDetailSetModel> agencyRequestAGDetailSetModelList;
    public ArrayList<RootModel> rootModelList;
    Context context;
    RootModel rootModel;
    String pincode, population, noofcopy, agencyCodeType = "", main, janjag, ag_code, replaced_copies,replaced_agency_name, subagent_reason, sub_agent_agcode, sub_agentMain_agcode, subagent_copy, subagentMain_copy, rep_reason;

    public ArrayList<NewAgencyModel> arrayList;

    boolean jjStatus = false;

    String jjValue;
    String MainAgentCode = "";

    String salesgrp = "";
    Globals g = Globals.getInstance(context);
    String unitCode = "";


    String salesoff_main_id = "", salesoff_jj_id = "", tvSaleDistrict_val = "";

    String town_val = "", dist_val = "", unit_val = "", state_val = "", pincode_val = "", tvOffice_val = "", AGNAME = "", tvClustter_val = "", location_val = "", city_upc = "", agency_name = "", reason_new_agency = "", intensification_ag_code = "", intensification_copy = "", intensification_name = "", intensification_reason = "";

    String totAds = "", totOutstanding = "",agencyLocation = "",unbilledAmount ="",currentOutstanding="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  setContentView(R.layout.activity_demo);
        rootModel = new RootModel();
        demoBinding = DataBindingUtil.setContentView(this, R.layout.activity_new_agency_rquest);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE | WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        g.uniteCode = "";
        stateMasterArray = new ArrayList<>();
        unitMaster = new ArrayList<>();
        cityMaster = new ArrayList<>();
        locationMaster = new ArrayList<>();
        salesOfficeListMain = new ArrayList<>();
        salesOfficeListJJ = new ArrayList<>();
        salesClusterList = new ArrayList<>();
        agencyRequestReasonSetModelArrayList = new ArrayList<>();
        agencyRequestAGDetailSetModelList = new ArrayList<>();
        rootModelList = new ArrayList<>();
        tvTownMaster = new ArrayList<>();
        context = this;
        demoBinding.toolbar.setTitle("New Agency Request");
        setSupportActionBar(demoBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initViews();
    }
    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
    private void initViews() {

        getMasterState();
        getMaritialStatus();
        getDesignation();
        getDepartment();
        // getNewsPaper(dataList.getName());

        mPresenter = new NewAgencyPresenter();

        mPresenter.setView(newRquestActivity.this);

        s_no1.clear();
        s_no1.add("1");


        demoBinding.agencyDetailsRec.setHasFixedSize(true);
        demoBinding.agencyDetailsRec.addItemDecoration(new DividerItemDecoration(getApplicationContext(),
                DividerItemDecoration.VERTICAL));
        demoBinding.agencyDetailsRec.setNestedScrollingEnabled(true);
        demoBinding.agencyDetailsRec.setLayoutManager(new LinearLayoutManager(getApplicationContext()));


        s_no2.clear();
        s_no2.add("1");

        g.intensification_rec_list.clear();

        for (int i = 0; i < s_no2.size(); i++) {
            HashMap map = new HashMap();
            map.put("inten_agcode", "");
            map.put("inten_copy", "0");
            map.put("inten_reason", "");
            map.put("inten_agency_name", "");

            g.intensification_rec_list.add(map);
        }

        demoBinding.intensificationRec.setHasFixedSize(true);
        demoBinding.intensificationRec.addItemDecoration(new DividerItemDecoration(context,
                DividerItemDecoration.VERTICAL));
        demoBinding.intensificationRec.setNestedScrollingEnabled(true);
        demoBinding.intensificationRec.setLayoutManager(new LinearLayoutManager(context));
        Intensification_Adapter adapter1 = new Intensification_Adapter(context, s_no2);
        demoBinding.intensificationRec.setAdapter(adapter1);
        adapter1.notifyDataSetChanged();


        demoBinding.addMoreAgency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s_no2.add("2");
                HashMap map = new HashMap();
                map.put("inten_agcode", "");
                map.put("inten_copy", "0");
                map.put("inten_reason", "");
                map.put("inten_agency_name", "");

                g.intensification_rec_list.add(map);

                adapter1.notifyDataSetChanged();

            }
        });
        demoBinding.tvState.setOnClickListener(v -> {
            clearData();
            dropSelectType = "state";
            Util.showDropDown(stateMasterArray, "Select state", newRquestActivity.this, this::onOptionClick);
        });
        demoBinding.tvUnit.setOnClickListener(v -> {

            jjStatus = false;
            dropSelectType = "unit";
            Util.showDropDown(unitMaster, "Select unit", newRquestActivity.this, this::onOptionClick);

        });
        demoBinding.tvDistrict.setOnClickListener(v -> {
            dropSelectType = "District";
            Util.showDropDown(cityMaster, "Select District", newRquestActivity.this, this::onOptionClick);

        });
        demoBinding.tvTown.setOnClickListener(v -> {
            dropSelectType = "town";
            Util.showDropDown(tvTownMaster, "Select town", newRquestActivity.this, this::onOptionClick);
        });
        demoBinding.tvLocation.setOnClickListener(v -> {
            dropSelectType = "location";
            Util.showDropDown(locationMaster, "Select location", newRquestActivity.this, this::onOptionClick);
        });


        demoBinding.mainSaleOffice.setOnClickListener(v -> {
            dropSelectType = "mainSaleOffice";
            Util.showDropDown(salesOfficeListMain, "Select Office", newRquestActivity.this, this::onOptionClick);

        });
        demoBinding.janjagritiSaleOffice.setOnClickListener(v -> {
            dropSelectType = "janjagriti_sale_office";
            Util.showDropDown(salesOfficeListJJ, "Select Office", newRquestActivity.this, this::onOptionClick);

        });

        demoBinding.tvCluster.setOnClickListener(v -> {
            dropSelectType = "tvCluster";
            Util.showDropDown(salesClusterList, "Select Office", newRquestActivity.this, this::onOptionClick);

        });

        demoBinding.tvSaleDistrict.setOnClickListener(v -> {
            dropSelectType = "tvSaleDistrict";
            //Util.showDropDown(salesClusterList, "Select Office", newRquestActivity.this, this::onOptionClick);
            Util.showDropDown(g.SalesDistArray, "Select Office", newRquestActivity.this, this::onOptionClick);

        });

        demoBinding.intensificationEdittext.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // g.intensification_rec_list.clear();
                if (!demoBinding.intensificationEdittext.getText().toString().equals("")) {
                    if (demoBinding.intensificationEdittext.getText().toString().equals("0") || Integer.parseInt(demoBinding.intensificationEdittext.getText().toString()) > 5) {

                        Toast.makeText(newRquestActivity.this, "Please Enter greater than Zero or less than 5", Toast.LENGTH_SHORT).show();
                    } else {
                        if (!demoBinding.intensificationEdittext.getText().toString().equals("")) {
                            s_no2.clear();
                            g.intensification_rec_list.clear();
                            for (int i = 1; i <= Integer.parseInt(demoBinding.intensificationEdittext.getText().toString()); i++) {
                                s_no2.add("1");

                                HashMap map = new HashMap();
                                map.put("inten_agcode", "");
                                map.put("inten_copy", "0");
                                map.put("inten_reason", "");

                                map.put("inten_agency_name", "");

                                g.intensification_rec_list.add(map);
                            }
                            adapter1.notifyDataSetChanged();
                        }
                    }
                }
            }
        });

        reason_new_agency = "Replacement";
        demoBinding.replacedAgency.setVisibility(View.VISIBLE);
        demoBinding.intensificationLayout.setVisibility(View.GONE);
        demoBinding.agentPermotionLayout.setVisibility(View.GONE);

        demoBinding.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.replacement:
                        //Toast.makeText(context,"Replacement",Toast.LENGTH_SHORT).show();
                        reason_new_agency = "Replacement";
                        demoBinding.replacedAgency.setVisibility(View.VISIBLE);
                        demoBinding.intensificationLayout.setVisibility(View.GONE);
                        demoBinding.agentPermotionLayout.setVisibility(View.GONE);
                        break;
                    case R.id.expansion:

                        // Toast.makeText(context,"Expansion",Toast.LENGTH_SHORT).show();
                        reason_new_agency = "Expansion";
                        demoBinding.replacedAgency.setVisibility(View.GONE);
                        demoBinding.intensificationLayout.setVisibility(View.GONE);
                        demoBinding.agentPermotionLayout.setVisibility(View.GONE);
                        break;
                    case R.id.intensification:

                        //Toast.makeText(context,"Intensification",Toast.LENGTH_SHORT).show();
                        reason_new_agency = "Intensification";
                        demoBinding.replacedAgency.setVisibility(View.GONE);
                        demoBinding.intensificationLayout.setVisibility(View.VISIBLE);
                        demoBinding.agentPermotionLayout.setVisibility(View.GONE);
                        break;

                    case R.id.sub_agent_permotion:

                        //  Toast.makeText(context,reason_new_agency,Toast.LENGTH_SHORT).show();
                        reason_new_agency = "Sub Agent Promotion";
                        demoBinding.replacedAgency.setVisibility(View.GONE);
                        demoBinding.intensificationLayout.setVisibility(View.GONE);
                        demoBinding.agentPermotionLayout.setVisibility(View.VISIBLE);
                        break;

                }
            }
        });

        demoBinding.propesedAgencyRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.agency1_rb:
                        demoBinding.agencyDetailLayout.setVisibility(View.VISIBLE);
                        s_no1.clear();
                        for (int i = 1; i <= 1; i++) {
                            s_no1.add("1");
                        }
                        //adapter.notifyDataSetChanged();
                        break;
                    case R.id.agency2_rb:
                        demoBinding.agencyDetailLayout.setVisibility(View.VISIBLE);
                        s_no1.clear();
                        for (int i = 1; i <= 2; i++) {
                            s_no1.add("1");
                        }
                        //        adapter.notifyDataSetChanged();
                        break;

                }
            }
        });

        city_upc = "City";
        g.mainSaleGroup = "CRL";
        demoBinding.cityUpcRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.city_rb:
                        city_upc = "City";
                        g.mainSaleGroup = "CRL";
                        break;
                    case R.id.upc_rb:
                        city_upc = "UPC";
                        g.mainSaleGroup = "CRU";
                        break;

                }
            }
        });


        demoBinding.addAgencyMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                s_no1.add("2");
                // adapter.notifyDataSetChanged();
            }
        });

        demoBinding.replacedAgencyCode.setOnClickListener(v -> {
            agencyCodeType = "replace";
            AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
            builder2.setCancelable(false);
            View view2 = LayoutInflater.from(context).inflate(R.layout.enter_agency_code, null);
            Button btnSubmit = view2.findViewById(R.id.getAgencyBtn);
            ImageView closeImg = view2.findViewById(R.id.closeImg);
            EditText edtAgency = view2.findViewById(R.id.replaced_agency_code);
            btnSubmit.setOnClickListener(v1 -> {
                if (unitCode.equalsIgnoreCase("")) {
                    alertMessage("Please select state and unit");
                    return;
                }
                getAgencyDetails(edtAgency.getText().toString());

            });
            builder2.setView(view2);
            closeImg.setOnClickListener(v1 -> {
                alertDialog.cancel();
                alertDialog.dismiss();
            });

            alertDialog = builder2.create();
            alertDialog.show();
        });


        demoBinding.subAgentCode.setOnClickListener(v -> {
            agencyCodeType = "subPromoSubAgent";
            AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
            builder2.setCancelable(false);
            View view2 = LayoutInflater.from(context).inflate(R.layout.enter_agency_code, null);
            Button btnSubmit = view2.findViewById(R.id.getAgencyBtn);
            ImageView closeImg = view2.findViewById(R.id.closeImg);
            EditText edtAgency = view2.findViewById(R.id.replaced_agency_code);
            btnSubmit.setOnClickListener(v1 -> {


                if (edtAgency.getText().toString().equalsIgnoreCase(demoBinding.subagentMainAgencyCode.getText().toString())) {
                    alertMessage("Please Enter  Different Agency Code");
                    return;
                }
                if (unitCode.equalsIgnoreCase("")) {
                    alertMessage("Please select state and unit");
                    return;
                }
                getAgencyDetails(edtAgency.getText().toString());

            });
            builder2.setView(view2);
            closeImg.setOnClickListener(v1 -> {
                alertDialog.cancel();
                alertDialog.dismiss();
            });

            alertDialog = builder2.create();
            alertDialog.show();
        });
        demoBinding.subagentMainAgencyCode.setOnClickListener(v -> {
            agencyCodeType = "subPromoMainAgent";
            AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
            builder2.setCancelable(false);
            View view2 = LayoutInflater.from(context).inflate(R.layout.enter_agency_code, null);
            Button btnSubmit = view2.findViewById(R.id.getAgencyBtn);
            ImageView closeImg = view2.findViewById(R.id.closeImg);
            EditText edtAgency = view2.findViewById(R.id.replaced_agency_code);
            btnSubmit.setOnClickListener(v1 -> {

                if (edtAgency.getText().toString().equalsIgnoreCase(demoBinding.subAgentCode.getText().toString())) {
                    alertMessage("Please Enter  Different Agency Code");
                    return;
                }

                if (unitCode.equalsIgnoreCase("")) {
                    alertMessage("Please select state and unit");
                    return;
                }
                getAgencyDetails(edtAgency.getText().toString());

            });
            builder2.setView(view2);
            closeImg.setOnClickListener(v1 -> {
                alertDialog.cancel();
                alertDialog.dismiss();
            });

            alertDialog = builder2.create();
            alertDialog.show();
        });


        demoBinding.noofcopyAc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {

                    g.mainNoOfCopy = Integer.parseInt(demoBinding.noofcopyAc.getText().toString());
                }


            }
        });


//        demoBinding.replacedCopies.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//
//
//                if(demoBinding.replacedCopies.getText().toString().length()>0){
//                    int replacedCopiesNo= Integer.parseInt(demoBinding.replacedCopies.getText().toString());
//
//                    Log.e("value", String.valueOf(replacedCopiesNo));
//                    if(replacedCopiesNo>g.mainNoOfCopy){
//
//                        alertMessage("Please Enter Valid Copy ");
//                        demoBinding.replacedCopies.getText().clear();
//                    }
//                }
//
//            }
//        });
        demoBinding.pinMain.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() > 0) {

                    pincode_val = demoBinding.pinMain.getText().toString();

                }
            }
        });

        demoBinding.population.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    population = demoBinding.population.getText().toString();
                }
            }
        });

        demoBinding.noofcopyAc.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.length() > 0) {
                    noofcopy = demoBinding.noofcopyAc.getText().toString();
                }

            }
        });

        demoBinding.main.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    main = demoBinding.main.getText().toString();
                }
            }
        });

        demoBinding.janJagriti.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 0) {
                    janjag = demoBinding.janJagriti.getText().toString();
                }
            }
        });


        demoBinding.saveNewAgency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {

                    pincode = demoBinding.pinMain.getText().toString();
                    pincode_val = pincode;
                    population = demoBinding.population.getText().toString();
                    noofcopy = demoBinding.noofcopyAc.getText().toString();
                    main = demoBinding.main.getText().toString();
                    janjag = demoBinding.janJagriti.getText().toString();
                    ag_code = demoBinding.replacedAgencyCode.getText().toString();
                    replaced_copies = demoBinding.replacedCopies.getText().toString();
                    replaced_agency_name = demoBinding.tvReplacedAgencyName.getText().toString();
                    rep_reason = demoBinding.replacedReason.getText().toString();
                    sub_agentMain_agcode = demoBinding.subagentMainAgencyCode.getText().toString();
                    subagentMain_copy = demoBinding.subAgentMainCopies.getText().toString();
                    sub_agent_agcode = demoBinding.subAgentCode.getText().toString();
                    subagent_copy = demoBinding.subAgentCopies.getText().toString();
                    subagent_reason = demoBinding.subagentReason.getText().toString();
                    currentOutstanding = demoBinding.tvReplacedCurrentOutstanding.getText().toString();
                    totAds = demoBinding.tvReplacedAsd.getText().toString();
                    totOutstanding = demoBinding.tvReplacedTotalOutstanding.getText().toString();
                    agencyLocation = demoBinding.tvReplacedAgencyLocation.getText().toString();
                    unbilledAmount = demoBinding.tvReplacedUnbilledAmount.getText().toString();


                    if (demoBinding.tvState.getText().toString().length() == 0 || state_val.length() == 0) {
                        alertMessage("Please Select State");
                        return;
                    }
                    if (demoBinding.tvUnit.getText().toString().length() == 0 || unit_val.length() == 0) {
                        alertMessage("Please Select Unit");
                        return;
                    }

                    if (demoBinding.tvDistrict.getText().toString().length() == 0 || dist_val.length() == 0) {
                        alertMessage("Please Select District");
                        return;
                    }
                    if (demoBinding.tvTown.getText().toString().length() == 0 || town_val.length() == 0) {
                        alertMessage("Please Select City/Village");
                        return;
                    }


                   /* if(demoBinding.tvLocation.getText().toString().length()==0 || location_val.length()==0){
                        alertMessage("Please Select Location");
                        return;
                    }*/
                    if (demoBinding.pinMain.getText().toString().length() == 0 || demoBinding.pinMain.getText().toString().length() < 6) {
                        alertMessage("Please enter pin code");
                        return;
                    }

                    if (demoBinding.tvSaleDistrict.getText().toString().length() == 0 || tvSaleDistrict_val.length() == 0) {
                        alertMessage("Please Select Sales District");
                        return;
                    }
                    if (demoBinding.tvCluster.getText().toString().length() == 0 || tvClustter_val.length() == 0) {
                        alertMessage("Please Select Cluster");
                        return;
                    }
                    if (demoBinding.population.getText().toString().length() == 0) {
                        alertMessage("Please Enter Population");
                        return;
                    }
                    if (demoBinding.noofcopyAc.getText().toString().length() == 0) {
                        alertMessage("Please Enter total no of copies");
                        return;
                    }
                    if (demoBinding.main.getText().toString().length() == 0 && demoBinding.janJagriti.getText().toString().length() == 0) {
                        alertMessage("Please Enter Main or Jagriti copies");
                        return;
                    }


                    int mainN = demoBinding.main.getText().toString().length() == 0 ? 0 : Integer.parseInt(demoBinding.main.getText().toString());
                    int jjN = demoBinding.janJagriti.getText().toString().length() == 0 ? 0 : Integer.parseInt(demoBinding.janJagriti.getText().toString());

                    int mainjjToCopy = mainN + jjN;
                    int noOfCopy = Integer.parseInt(demoBinding.noofcopyAc.getText().toString());
                    int mainToCopy = Integer.parseInt(demoBinding.main.getText().toString());


                    if (jjStatus) {
                        if (mainjjToCopy > noOfCopy || mainjjToCopy < noOfCopy) {
                            // alertMessage("jj and main copy should not be grater then and less then  total no of copy : "+noOfCopy+" \n main copy :"+demoBinding.main.getText().toString()+"\n jj copy :"+demoBinding.janJagriti.getText().toString());
                            alertMessage("Main / JJ copies mismatch");
                            return;
                        }
                    } else {
                        if (mainToCopy > noOfCopy || mainToCopy < noOfCopy) {
                            // alertMessage("main copy should not be grater then and less then total no of copy : "+noOfCopy);
                            alertMessage("Main / JJ copies mismatch");
                            return;
                        }
                    }

                    if (demoBinding.main.getText().toString().length() > 0 && demoBinding.mainSaleOffice.getText().toString().length() == 0) {
                        alertMessage("Please Select main sales office");
                        return;
                    }


                    if (demoBinding.janJagriti.getText().toString().length() > 0 && demoBinding.janjagritiSaleOffice.getText().toString().length() == 0) {
                        alertMessage("Please Select jan jagriti sale office");
                        return;
                    }

                    if (city_upc.length() == 0) {
                        alertMessage("Please Chose City or upc");
                        return;
                    }

                    if (reason_new_agency.length() == 0) {
                        alertMessage("Please Select Appointment Reason");
                        return;
                    }

                    AgencyRequestReasonSetModel model1 = new AgencyRequestReasonSetModel();
                    agencyRequestReasonSetModelArrayList.clear();

                    if (reason_new_agency.equalsIgnoreCase("Replacement")) {

                        if (demoBinding.replacedReason.getText().toString().length() == 0) {
                            alertMessage("Please Enter  the Reason");
                            return;
                        }
                        if (ag_code.length() == 0) {
                            alertMessage("Please Enter the agency code");
                            return;
                        }
                        model1.setReason(reason_new_agency);
                        model1.setAg_code(ag_code);
                        model1.setAg_name(AGNAME);
                        model1.setAg_copies(Integer.parseInt(replaced_copies));
                        model1.setAg_reason(rep_reason);
                        model1.setAg_name(replaced_agency_name);
                        model1.setOutstanding(currentOutstanding);
                        model1.setAsd(totAds);
                        model1.setAgency_location(agencyLocation);
                        model1.setTotal_outstanding(totOutstanding);
                        model1.setUnbilled_amount(unbilledAmount);
                        agencyRequestReasonSetModelArrayList.add(model1);
                    }
                    if (reason_new_agency.equalsIgnoreCase("Expansion")) {
                        model1.setReason(reason_new_agency);
                        model1.setAg_code("");
                        model1.setAg_copies(0);
                        model1.setAg_name(agency_name);
                        model1.setAg_reason("");
                        agencyRequestReasonSetModelArrayList.add(model1);
                    }
                    if (reason_new_agency.equalsIgnoreCase("Intensification")) {
                        for (int i = 0; i < g.intensification_rec_list.size(); i++) {
                            Log.e("sdsdsf", "Code" + " " + g.intensification_rec_list.get(i).get("inten_agcode").toString());
                            Log.e("sdsdsf", "reason" + " " + g.intensification_rec_list.get(i).get("inten_reason").toString());
                            intensification_ag_code = g.intensification_rec_list.get(i).get("inten_agcode").toString();
                            intensification_copy = g.intensification_rec_list.get(i).get("inten_copy").toString();
                            intensification_reason = g.intensification_rec_list.get(i).get("inten_reason").toString();
                            if (intensification_reason.length() == 0) {
                                alertMessage("Please Enter  the Reason");
                                return;
                            }
                            if (intensification_ag_code.length() == 0) {
                                alertMessage("Please Enter  the agency code");
                                return;
                            }
                            model1.setReason(reason_new_agency);
                            model1.setAg_code(intensification_ag_code);
                            model1.setAg_name(g.intensification_rec_list.get(i).get("inten_agency_name").toString());
                            model1.setAg_copies(Integer.parseInt(intensification_copy));
                            model1.setAg_reason(intensification_reason);

                            agencyRequestReasonSetModelArrayList.add(model1);
                        }

                    }
                    if (reason_new_agency.equalsIgnoreCase("Sub Agent Promotion")) {
                        if (demoBinding.subagentMainAgencyCode.getText().toString().length() == 0) {
                            alertMessage("Please Enter  Main Agency Code");
                            return;
                        }
                        if (demoBinding.subAgentMainCopies.getText().toString().length() == 0) {
                            alertMessage("Please Enter  Main Agency Copies");
                            return;
                        }

                        if (demoBinding.subAgentCode.getText().toString().length() == 0) {
                            alertMessage("Please Enter  Sub Agency Code");
                            return;
                        }
                        if (demoBinding.subAgentCopies.getText().toString().length() == 0) {
                            alertMessage("Please Enter  Sub Agency Copies");
                            return;
                        }


                        if (demoBinding.subagentReason.getText().toString().length() == 0) {
                            alertMessage("Please Enter  Sub Agent Reason");
                            return;
                        }


                        model1.setReason(demoBinding.subagentReason.getText().toString());

                        model1.setSubag_code(demoBinding.subAgentCode.getText().toString());
                        model1.setSubag_copies(Integer.parseInt(demoBinding.subAgentCopies.getText().toString()));

                        model1.setAg_code(demoBinding.subagentMainAgencyCode.getText().toString());
                        model1.setAg_copies(Integer.parseInt(demoBinding.subAgentMainCopies.getText().toString()));
                        model1.setAg_reason(subagent_reason);

                        agencyRequestReasonSetModelArrayList.add(model1);
                    }


                    Gson gson = new Gson();

                    // postData(rootModelList);
                    String data = gson.toJson(agencyRequestReasonSetModelArrayList);

                    Log.e("Dsgsdgsdgsdgsd", data);

                    AlertUtility.showAlert(newRquestActivity.this, "Are you sure want to save the data..", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            addAgencyData(agencyRequestReasonSetModelArrayList);
                            longLog(data);

                        }
                    });
                    //// Log.d("dataShow",);
                } catch (Exception e) {
                    Toast.makeText(context, "Error" + e.getMessage(), Toast.LENGTH_LONG).show();
                }

            }

//            }
        });

    }


    public void RemoveIntensification(int position) {    /*Calling from intensification adapter*/
        g.intensification_rec_list.remove(position);

    }

    public void clearData() {

        demoBinding.tvUnit.setText("");
        demoBinding.tvState.setText("");
        demoBinding.tvDistrict.setText("");
        demoBinding.tvTown.setText("");
        demoBinding.tvLocation.setText("");
        demoBinding.tvSaleDistrict.setText("");
        demoBinding.mainSaleOffice.setText("");
        demoBinding.janjagritiSaleOffice.setText("");
        demoBinding.tvCluster.setText("");

    }

    public static void longLog(String str) {
        if (str.length() > 4000) {
            Log.d("", str.substring(0, 4000));
            longLog(str.substring(4000));
        } else
            Log.d("", str);
    }

    public void addAgencyData(ArrayList<AgencyRequestReasonSetModel> data) {

        Gson gson = new Gson();

        // postData(rootModelList);
        String rootModel1 = gson.toJson(data);
        Log.e("rootModel1", rootModel1);
        enableLoadingBar(true);
        RootModel rootModel = new RootModel();
//
        //  HashMap<String, Object> params = new HashMap<>();
        rootModel.setState_id(Integer.parseInt(state_val));
        rootModel.setUnit_id(Integer.parseInt(unit_val));
        rootModel.setCity_id(Integer.parseInt(dist_val));
        // rootModel.setLocation_id(Integer.parseInt(location_val));
        rootModel.setLocation_id(Integer.parseInt(town_val));
        rootModel.setTown_id(Integer.parseInt(town_val));
        rootModel.setCluster_id(Integer.parseInt(tvClustter_val));
        rootModel.setSalesdist_id(Integer.parseInt(tvSaleDistrict_val));
        rootModel.setPincode(demoBinding.pinMain.getText().toString());
        rootModel.setPopulation(demoBinding.population.getText().toString());
        rootModel.setTot_copies(Integer.parseInt(demoBinding.noofcopyAc.getText().toString()));

        if (demoBinding.main.getText().toString().length() == 0) {

            rootModel.setMain_copies(0);
        } else {
            rootModel.setMain_copies(Integer.parseInt(demoBinding.main.getText().toString()));

        }


        if (demoBinding.mainSaleOffice.getText().toString().length() == 0) {

            rootModel.setSalesoff_main_id("");
        } else {
            rootModel.setSalesoff_main_id(salesoff_main_id);

        }
        if (demoBinding.janJagriti.getText().toString().length() == 0) {

            rootModel.setJj_copies(0);
        } else {
            rootModel.setJj_copies(Integer.parseInt(demoBinding.janJagriti.getText().toString()));

        }

        if (demoBinding.janjagritiSaleOffice.getText().toString().length() == 0) {

            rootModel.setSalesoff_jj_id("");
        } else {
            rootModel.setSalesoff_jj_id(salesoff_jj_id);

        }
        rootModel.setCity_upc(city_upc);
        rootModel.setReason(reason_new_agency);
        rootModel.setProposed_count(0);
        rootModel.setStatus("1");
        rootModel.setCreate_user(PreferenceManager.getAgentId(context));
        rootModel.setUpdate_user(PreferenceManager.getAgentId(context));


        if (reason_new_agency.equalsIgnoreCase("Expansion")) {
            data.clear();
            rootModel.setAgencyRequestReason_set(data);
        } else {
            rootModel.setAgencyRequestReason_set(data);
        }

        // params.put("AgencyRequestReason_set",data);
//
//        params.put("create_user",PreferenceManager.getAgentId(context));
//        params.put("update_user",PreferenceManager.getAgentId(context));

        Call<String> call = SamriddhiApplication.getmInstance().getApiService().addAgencyRequest(rootModel);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;
                    //Log.e("dataShow",response.toString());
                    try {


                        // String message = jsonObject.getString("message");
                        arrayList = new ArrayList<>();

                        NewAgencyModel objData = new NewAgencyModel();
                        JSONObject str = new JSONObject(response.body());
                        Log.e("swesdgdsgsgsdg", str.toString());

                        objData.setId(str.getString("ag_req_id"));
                        objData.setState(str.getString("StateName"));
                        objData.setUnit(str.getString("UnitName"));

                        objData.setState_id(str.getString("state_id"));
                        objData.setUnitId(str.getString("unit_id"));

                        objData.setTown(str.getString("TownName"));

                        objData.setLocation(str.getString("LocationName"));
                        objData.setPopulation(str.getString("population"));
                        objData.setDistrict(str.getString("CityName"));
                        objData.setPincode(str.getString("pincode"));


                        objData.setMain(str.getString("main_copies"));
                        objData.setJanJagrati(str.getString("jj_copies"));


                        objData.setNoMainCopy(str.getString("tot_copies"));

                        objData.setSaleCluster(str.getString("ClusterName"));
                        objData.setCity_upc(str.getString("city_upc"));
                        objData.setReason(str.getString("reason"));
                        objData.setStatus(str.getString("status"));
                        objData.setSalesOfficeNameJJ(str.getString("SalesOfficeNameJJ"));

                        objData.setSalesOfficeNameMain(str.getString("SalesOfficeNameMain"));
                        objData.setSalesDistName(str.getString("SalesDistName"));
                        objData.setReasonArray(str.getString("AgencyRequestReason_set"));

                        objData.setUOHApproval(str.getString("UOHApproval"));
                        objData.setSOHApproval(str.getString("SOHApproval"));
                        objData.setHOApproval(str.getString("HOApproval"));


                        objData.setUHFApproval(str.getString("UHFApproval"));
                        objData.setCBRApproval(str.getString("CBRApproval"));
                        objData.setBPClosure(str.getString("BPClosure"));
                        arrayList.add(objData);

                       /* Intent intent=new Intent(newRquestActivity.this, ShowRequestListActivity.class);
                        startActivity(intent);
                        finish();*/

                        // NewAgencyModel objData2=arrayList.get(0);
                        //Log.e("sdgsgssgds",arrayList.size()+"");

                        Toast.makeText(newRquestActivity.this, "Your Request Order SuccessFully generated  ", Toast.LENGTH_SHORT).show();
                        Intent mv = new Intent(newRquestActivity.this, ViewRequestData.class);
                        mv.putExtra("MyData", objData);
                        getIntent().getSerializableExtra("MyData");
                        startActivity(mv);
                        //finish();

                        //  enableLoadingBar(false);
                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    enableLoadingBar(false);
                } else {
                    try {
                        Toast.makeText(newRquestActivity.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(newRquestActivity.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                    enableLoadingBar(false);
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(newRquestActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void postData(ArrayList<RootModel> rootModelList) {

        Gson gson = new Gson();
        String data1 = gson.toJson(rootModelList);
        Log.e("agency", data1);
        // enableLoadingBar(true);
        Call<RootModel> call = SamriddhiApplication.getmInstance().getApiService().postAgencyData(rootModelList);
        call.enqueue(new Callback<RootModel>() {
            @Override
            public void onResponse(@NonNull Call<RootModel> call, @NonNull Response<RootModel> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;


                    Log.e("dataShow", response.toString());
                    try {
                        if (response.code() == 201) {
                            Toast.makeText(newRquestActivity.this, "Success" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                        } else {

                        }


                        //  enableLoadingBar(false);
                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Log.e("dfgsds", "1127");
                        //  Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        Log.e("dfgsds", "1130");
                        // Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<RootModel> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(newRquestActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void getAgencyDetails(String agencyCode) {
        enableLoadingBar(true);
        Call<String> call;
        if (agencyCodeType.equalsIgnoreCase("subPromoSubAgent")) {

            call = SamriddhiApplication.getmInstance().getApiService().getAgencyDetails(PreferenceManager.getAgentId(context), MainAgentCode, unitCode + "/" + agencyCode);
        } else {
            MainAgentCode = "00" + agencyCode;
            call = SamriddhiApplication.getmInstance().getApiService().getAgencyDetails(PreferenceManager.getAgentId(context), "00" + agencyCode, unitCode);
        }


        //  Call<String> call = SamriddhiApplication.getmInstance().getApiService().getAgencyDetails(PreferenceManager.getAgentId(context),"00"+agencyCode,unitCode);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;
                    Log.e("agecnyData", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());


                        String status = jsonObject.getString("status");
                        Log.e("status", jsonObject.getString("status"));
                        if (status.equalsIgnoreCase("Success")) {
                            JSONObject objData = jsonObject.getJSONObject("data");
                            AgencyRequestReasonSetModel model1 = new AgencyRequestReasonSetModel();

                            model1.setAg_code(agencyCode);
                            model1.setAg_copies(Integer.parseInt(objData.getString("AVGCOPY7Days")));
                            model1.setAg_reason("");
                            model1.setReason("");
                            model1.setAg_name(objData.getString("AGNAME"));
                            agency_name = objData.getString("AGNAME");
                            AGNAME = objData.getString("AGNAME");
                            // demoBinding.replacedAgencyCode.setText(objData.getString("AGNAME"));

                            if (agencyCodeType.equalsIgnoreCase("replace")) {
                                demoBinding.replacedAgencyCode.setText(agencyCode);
                                demoBinding.tvReplacedAgencyName.setText(objData.getString("AGNAME"));
                                demoBinding.tvReplacedAsd.setText(objData.getString("asd"));
                                demoBinding.tvReplacedCurrentOutstanding.setText(objData.getString("outstanding"));
                                demoBinding.tvReplacedUnbilledAmount.setText(objData.getString("unbilled_amount"));
                                demoBinding.tvReplacedAgencyLocation.setText(objData.getString("agency_location"));
                                demoBinding.tvReplacedTotalOutstanding.setText(objData.getString("total_outstanding"));
                                demoBinding.replacedCopies.setText(objData.getString("AVGCOPY7Days"));

                            } else if (agencyCodeType.equalsIgnoreCase("subPromoSubAgent")) {

                                // demoBinding.subAgentCode.setText(agencyCode);
                                demoBinding.subAgentCode.setText(objData.getString("subagcode"));
                                // demoBinding.subAgentCopies.setText(objData.getString("AVGCOPY7Days"));
                                demoBinding.subAgentCopies.setText(objData.getString("subag_copy"));

                            } else if (agencyCodeType.equalsIgnoreCase("subPromoMainAgent")) {
                                demoBinding.subagentMainAgencyCode.setText(agencyCode);
                                demoBinding.subAgentMainCopies.setText(objData.getString("AVGCOPY7Days"));
                            }

                            alertDialog.cancel();
                            alertDialog.dismiss();
                        }

                        if (status.equalsIgnoreCase("Failed")) {
                            alertDialog.cancel();
                            alertDialog.dismiss();
                            alertMessage(jsonObject.getString("msg"));
                            // Toast.makeText(newRquestActivity.this,"agency code is not map with this location please enter different agency code",Toast.LENGTH_LONG).show();


                        }

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }
                    enableLoadingBar(false);
                } else {
                    try {
                        Log.e("dfgsds", "1214");
                        //Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        Log.e("dfgsds", "1217");
                        // Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                    enableLoadingBar(false);
                }

            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                //  enableLoadingBar(false);
                enableLoadingBar(false);
                Toast.makeText(newRquestActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });

    }


    public void getSaleDistrict(String unitId) {

        Log.e("dsgsdgsdg", unitId);
        enableLoadingBar(true);

        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getSalesDist(PreferenceManager.getAgentId(context), unitId);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("newsdata", response.body());
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {
                            g.SalesDistArray.clear();
                            for (int i = 0; i < objData.length(); i++) {

                                JSONObject objStr = objData.getJSONObject(i);
                                DropDownModel dropDownModel = new DropDownModel();
                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("sales_dist_name"));
                                g.SalesDistArray.add(dropDownModel);
                            }
                        }
                        enableLoadingBar(false);
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Log.e("dfgsds", "1276");
                        // Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        Log.e("dfgsds", "1279");
                        //  Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(newRquestActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void getNewsPaper(String name) {
        Log.e("sdfsdfsdf", name);
        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getNewsPaper(PreferenceManager.getAgentId(context) + "/" + name);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;
                    Log.e("newsdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {
                            g.newsPaperNameMasterArray.clear();

                            for (int i = 0; i < objData.length(); i++) {

                                JSONObject objStr = objData.getJSONObject(i);
                                NewsPaperModel dropDownModel = new NewsPaperModel();
                                dropDownModel.setPk(objStr.getString("pk"));
                                dropDownModel.setPaperName(objStr.getJSONObject("fields").getString("newspaper_name"));
                                g.newsPaperNameMasterArray.add(dropDownModel);
                            }
                        }
                        enableLoadingBar(false);
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Log.e("dfgsds", "1331");
                        // Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        Log.e("dfgsds", "1334");
                        //Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(newRquestActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void getMaritialStatus() {
        g.getMaritialArray.clear();
        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getMaritial(PreferenceManager.getAgentId(context));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;
                    Log.e("getMaritial", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            for (int i = 0; i < objData.length(); i++) {

                                JSONObject objStr = objData.getJSONObject(i);
                                DropDownModel dropDownModel = new DropDownModel();
                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("maritial_status"));
                                g.getMaritialArray.add(dropDownModel);
                            }
                        }
                        enableLoadingBar(false);
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Log.e("dfgsds", "1387");
                        //  Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        Log.e("dfgsds", "1391");
                        //Log.e("dfgsds","1693");
                        //  Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(newRquestActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }


    public void getChildEducation() {
        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getEducation(PreferenceManager.getAgentId(context));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;
                    Log.e("newsdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            for (int i = 0; i < objData.length(); i++) {

                                JSONObject objStr = objData.getJSONObject(i);
                                DropDownModel dropDownModel = new DropDownModel();
                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("newspaper_name"));
                                g.educationMasterArray.add(dropDownModel);
                            }
                        }
                        enableLoadingBar(false);
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Log.e("dfgsds", "1443");
                        // Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        Log.e("dfgsds", "1447");
                        // Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(newRquestActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void getCluster(String unitId) {
        salesClusterList.clear();
        enableLoadingBar(true);

        Log.e("uid", unitId);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getSalesCluster(PreferenceManager.getAgentId(context), unitId);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;
                    Log.e("sgsdgsgdsgsdgsg", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            for (int i = 0; i < objData.length(); i++) {

                                JSONObject objStr = objData.getJSONObject(i);
                                DropDownModel dropDownModel = new DropDownModel();
                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("cluster_name"));
                                salesClusterList.add(dropDownModel);
                            }
                        }
                        enableLoadingBar(false);
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Log.e("dfgsds", "1501");
                        //Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        Log.e("dfgsds", "1504");
                        // Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(newRquestActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void getSalesOffice(String unitId) {
        salesOfficeListJJ.clear();
        salesOfficeListMain.clear();
        enableLoadingBar(true);
        Log.e("id", unitId);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getSalesOffice(PreferenceManager.getAgentId(context), unitId);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;
                    Log.e("newsdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            for (int i = 0; i < objData.length(); i++) {

                                JSONObject objStr = objData.getJSONObject(i);
                                DropDownModel dropDownModel = new DropDownModel();
                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("sales_office"));

                                if (objStr.getJSONObject("fields").getInt("jj_flag") == 0) {
                                    salesOfficeListMain.add(dropDownModel);
                                } else {
                                    salesOfficeListJJ.add(dropDownModel);
                                }

                            }
                        }
                        enableLoadingBar(false);
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Log.e("dfgsds", "1563");
                        // Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        Log.e("dfgsds", "1567");
                        // Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(newRquestActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void getDesignation() {
        // Calling JSON
        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getDesignation(PreferenceManager.getAgentId(context));
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                enableLoadingBar(false);
                if (response.isSuccessful()) {


                    assert response.body() != null;
                    Log.e("designationMasterArray", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            g.designationMasterArray.clear();
                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("designation_name"));
                                dropDownModel.setId(objStr.getString("pk"));
                                g.designationMasterArray.add(dropDownModel);
                            }
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Log.e("dfgsds", "1625");
                        // Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        Log.e("dfgsds", "1628");
                        // Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Log.e("dfgsds", "1638");
                //  Toast.makeText(newRquestActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });

    }

    public void getDepartment() {
        // Calling JSON
        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getDepartment(PreferenceManager.getAgentId(context));
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                enableLoadingBar(false);
                if (response.isSuccessful()) {


                    assert response.body() != null;
                    Log.e("departmentMasterArray", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            g.departmentMasterArray.clear();
                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("department_name"));
                                dropDownModel.setId(objStr.getString("pk"));
                                g.departmentMasterArray.add(dropDownModel);
                            }
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Log.e("dfgsds", "1689");
                        //Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        Log.e("dfgsds", "1693");
                        // Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Log.e("dfgsds", "1704");
                // Toast.makeText(newRquestActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });

    }

    private void alertMessage(String message) {

        AlertUtility.showFormAlert(newRquestActivity.this, message, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onItemClick(View view, BankListModel object) {

    }

    @Override
    public void onSuccess(List<BankListModel> bankData) {

    }

    @Override
    public Context getContext() {
        return null;
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void onOptionClick(DropDownModel dataList) {

        if (dropSelectType.equalsIgnoreCase("state")) {
            Util.hideDropDown();
            //Toast.makeText(context,dataList.getId(),Toast.LENGTH_SHORT).show();
            demoBinding.tvState.setText(dataList.getDescription());
            getUnit(dataList.getId());
            getCityDistrict(dataList.getId(), dataList.getName());
            g.state_main = dataList.getId();
            state_val = dataList.getId();

            AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
            editor.putString(AppsContants.DetailStatus, "1");
            editor.putString(AppsContants.stateCode, dataList.getName());
            editor.commit();

            getNewsPaper(dataList.getName());
            Log.e("sdfdsfsdf", dataList.getName());

        } else if (dropSelectType.equalsIgnoreCase("unit")) {
            Util.hideDropDown();
            // Toast.makeText(context,dataList.getId(),Toast.LENGTH_SHORT).show();
            demoBinding.tvUnit.setText(dataList.getDescription());
            unitCode = dataList.getCode();
            g.uniteCode = dataList.getCode();
            if (dataList.getName().equalsIgnoreCase("1")) {
                demoBinding.jjLayoutView.setVisibility(View.VISIBLE);
                jjStatus = true;
            } else {
                demoBinding.jjLayoutView.setVisibility(View.GONE);
                jjStatus = false;
            }
            demoBinding.janJagriti.setText("");
            demoBinding.janjagritiSaleOffice.setText("");
            demoBinding.mainSaleOffice.setText("");
            demoBinding.main.setText("");
            g.unit_main = dataList.getId();
            unit_val = dataList.getId();
            getSalesOffice(dataList.getId());
            getCluster(dataList.getId());

            getSaleDistrict(dataList.getId());


        } else if (dropSelectType.equalsIgnoreCase("District")) {

            Util.hideDropDown();
            //Toast.makeText(context,dataList.getId(),Toast.LENGTH_SHORT).show();
            demoBinding.tvDistrict.setText(dataList.getDescription());
            dist_val = dataList.getId();
            getLocation(dataList.getName());
            //getTown(dataList.getName());
            getTown(dataList.getId());
        } else if (dropSelectType.equalsIgnoreCase("town")) {

            Util.hideDropDown();
            //  Toast.makeText(context,dataList.getId(),Toast.LENGTH_SHORT).show();
            demoBinding.tvTown.setText(dataList.getDescription());
            town_val = dataList.getId();

        } else if (dropSelectType.equalsIgnoreCase("location")) {

            Util.hideDropDown();
            // Toast.makeText(context,dataList.getId(),Toast.LENGTH_SHORT).show();
            demoBinding.tvLocation.setText(dataList.getDescription());
            location_val = dataList.getId();

        } else if (dropSelectType.equalsIgnoreCase("tvCluster")) {
            Util.hideDropDown();
            demoBinding.tvCluster.setText(dataList.getDescription());

            // location_val=dataList.getId();

            tvClustter_val = dataList.getId();

        } else if (dropSelectType.equalsIgnoreCase("tvSaleDistrict")) {
            Util.hideDropDown();

            demoBinding.tvSaleDistrict.setText(dataList.getDescription());
            // location_val=dataList.getId();
            tvSaleDistrict_val = dataList.getId();

        } else if (dropSelectType.equalsIgnoreCase("mainSaleOffice")) {
            salesoff_main_id = dataList.getId();
            demoBinding.mainSaleOffice.setText(dataList.getDescription());
            Util.hideDropDown();
        } else if (dropSelectType.equalsIgnoreCase("janjagriti_sale_office")) {
            demoBinding.janjagritiSaleOffice.setText(dataList.getDescription());
            salesoff_jj_id = dataList.getId();
            Util.hideDropDown();
        }
    }

    public void getMasterState() {
        // Calling JSON
        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getStateMaster(PreferenceManager.getAgentId(context));
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());
                    enableLoadingBar(false);
                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            for (int i = 0; i < objData.length(); i++) {

                                JSONObject objStr = objData.getJSONObject(i);
                                DropDownModel dropDownModel = new DropDownModel();
                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setName(objStr.getJSONObject("fields").getString("state_code"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("state_name"));

                                stateMasterArray.add(dropDownModel);

                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {

                        Log.e("dfgsds", "1892");
                        //  Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        Log.e("dfgsds", "1896");
                        // Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Log.e("dfgsds", "1906");
                //Toast.makeText(newRquestActivity.this, R.string.err_ifsc_code_wrong, Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void getUnit(String getUnit) {
        unitMaster.clear();
        // Calling JSON10011137MP
        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getUnit(PreferenceManager.getAgentId(context), getUnit);
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("unit_name"));

                                dropDownModel.setName(objStr.getJSONObject("fields").getString("jj_flag"));
                                dropDownModel.setCode(objStr.getJSONObject("fields").getString("unit_code"));

                                unitMaster.add(dropDownModel);

                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    enableLoadingBar(false);
                } else {
                    try {
                        Log.e("dfgsds", "1956");
                        // Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        Log.e("dfgsds", "1960");
                        // Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Log.e("dfgsds", "1970");
                // Toast.makeText(newRquestActivity.this, R.string.err_ifsc_code_wrong, Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void getCityDistrict(String getUnit, String stateCode) {
        cityMaster.clear();
        // Calling JSON10011137MP
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getDistrict(PreferenceManager.getAgentId(context), stateCode);
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("getCityDistrict", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("city_name"));
                                dropDownModel.setName(objStr.getJSONObject("fields").getString("city_code"));

                                cityMaster.add(dropDownModel);

                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        Log.e("dfgsds", "2019");
                        //  Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        Log.e("dfgsds", "2023");
                        // Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Log.e("dfgsds", "2033");
                // Toast.makeText(newRquestActivity.this, R.string.err_ifsc_code_wrong, Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void getLocation(String districtName) {


        Log.e("sjdsdgsdg", districtName);
        enableLoadingBar(true);
        locationMaster.clear();
        // Calling JSON10011137MP
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getLocationCity(PreferenceManager.getAgentId(context), districtName);
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("location_name"));

                                locationMaster.add(dropDownModel);

                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    enableLoadingBar(false);
                } else {
                    try {
                        Log.e("dfgsds", "2093");
                        //  Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        Log.e("dfgsds", "2097");
                        // Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Log.e("dfgsds", "2107");
                // Toast.makeText(newRquestActivity.this, R.string.err_ifsc_code_wrong, Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void getTown(String town) {
        tvTownMaster.clear();
        Log.e("asfasfa", town);
        // Calling JSON10011137MP
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getTownCity(PreferenceManager.getAgentId(context), town);
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            for (int i = 0; i < objData.length(); i++) {

                                JSONObject objStr = objData.getJSONObject(i);
                                DropDownModel dropDownModel = new DropDownModel();
                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("town_name"));
                                tvTownMaster.add(dropDownModel);
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        Log.e("dfgsds", "2149");
                        // Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        Log.e("dfgsds", "2153");
                        //Toast.makeText(newRquestActivity.this, "error message"+response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Log.e("dfgsds", "2163");
                // Toast.makeText(newRquestActivity.this, R.string.err_ifsc_code_wrong, Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    public void onItemClick(String formType, ProposedAgencyModel proposedAgencyModel) {


    }


}
