package com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule;

public class BothEditionModal {

    public String edition_id;
    public String edition_copies;

    public String getEdition_id() {
        return edition_id;
    }

    public void setEdition_id(String edition_id) {
        this.edition_id = edition_id;
    }

    public String getEdition_copies() {
        return edition_copies;
    }

    public void setEdition_copies(String edition_copies) {
        this.edition_copies = edition_copies;
    }



}
