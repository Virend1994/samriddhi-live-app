package com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.R;
import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.Approval_Agency_Adapter;
import com.app.samriddhi.base.adapter.DropdownMenuAdapter;
import com.app.samriddhi.base.adapter.ShowRequestNewAgencyListAdapter;
import com.app.samriddhi.databinding.ActivityShowFormRequestBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AppsContants;
import com.app.samriddhi.ui.model.AgencyRequestReasonSetModel;
import com.app.samriddhi.ui.model.DropDownModel;
import com.app.samriddhi.ui.model.NewAgencyModel;
import com.app.samriddhi.ui.model.ProposedAgencyModel;
import com.app.samriddhi.util.Constant;
import com.app.samriddhi.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

import okhttp3.Credentials;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowRequestListActivity extends BaseActivity implements DropdownMenuAdapter.OnMeneuClickListnser, ShowRequestNewAgencyListAdapter.OnItemClickListnser {


    ActivityShowFormRequestBinding mbinding;

    private ShowRequestListActivity context;
    private ArrayList<DropDownModel> unitMaster;
    private ArrayList<DropDownModel> locationMaster;
    private ArrayList<DropDownModel> stateMaster;
    private ArrayList<NewAgencyModel> arrayList;
    private ShowRequestNewAgencyListAdapter adapter2;
    private String dropSelectType = "";
    private String unitId = "", locationId = "", stateId = "";
    ShowRequestNewAgencyListAdapter showRequestNewAgencyListAdapter;
    String strJJCopies = "";
    int MainCir = 0;
    int Kybp = 0;
    int Survey = 0;
    int JJCir = 0;
    int OTP = 0;
    int agreementStatus = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.context = this;
        //setContentView(R.layout.activity_show_form_request);
        arrayList = new ArrayList<>();
        unitMaster = new ArrayList<>();
        stateMaster = new ArrayList<>();
        locationMaster = new ArrayList<>();
        mbinding = DataBindingUtil.setContentView(this, R.layout.activity_show_form_request);

        mbinding.toolbar.setTitle("Request List Data");
        setSupportActionBar(mbinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mbinding.allnewagencyRec.setHasFixedSize(true);
        mbinding.allnewagencyRec.setLayoutManager(new LinearLayoutManager(ShowRequestListActivity.this, LinearLayoutManager.VERTICAL, false));


        mbinding.tvState.setOnClickListener(v -> {
            dropSelectType = "state";
            Util.showDropDown(stateMaster, "Select State", context, this::onOptionClick);
        });


        mbinding.tvUnit.setOnClickListener(v -> {
            dropSelectType = "unit";
            Util.showDropDown(unitMaster, "Select unit", context, this::onOptionClick);
        });
        mbinding.tvLocation.setOnClickListener(v -> {
            dropSelectType = "location";
            Util.showDropDown(locationMaster, "Select Location", context, this::onOptionClick);
        });
        // getUnit(stateId);
        getState();
        // getLocation();
        //getShowRequest();
        mbinding.editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                String text = mbinding.editSearch.getText().toString().toLowerCase(Locale.getDefault());
                Log.e("bhs==>>", text);

                if (cs.length() > 0) {
                    adapter2.getFilter().filter(cs);
                    adapter2.notifyDataSetChanged();
                }

            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }


            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });


        Log.e("dgdgdgfdg", PreferenceManager.getMenu_TYPE(ShowRequestListActivity.this));
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }


    @Override
    protected void onResume() {
        super.onResume();
        // getShowRequest();
    }

    public void getShowRequest() {

        enableLoadingBar(true);
        Call<String> call = null;

//        if (unitId.equalsIgnoreCase("") && locationId.equalsIgnoreCase("")) {
//            Log.e("sdfsdfsd", "1");
//            call = SamriddhiApplication.getmInstance().getApiService().getShowAgencyRequest();
//        }
        if (!unitId.equalsIgnoreCase("")) {
            call = SamriddhiApplication.getmInstance().getApiService().getShowAgencyRequestUnit("-ag_req_id", unitId);
            Log.e("sdfsdfsd", "3");
        }

        if (!locationId.equalsIgnoreCase("") && !unitId.equalsIgnoreCase("")) {
            call = SamriddhiApplication.getmInstance().getApiService().getShowAgencyRequest("-ag_req_id", unitId, locationId);
            Log.e("sdfsdfsd", "4");
        }


        Log.e("sgksdksdg" + " " + "Unit", unitId);
        Log.e("sgksdksdg" + " " + "location>", locationId);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;

                    try {

//                        JSONObject jsonObject = new JSONObject(response.body());
//                        boolean status = jsonObject.getBoolean("status");
                        //    Toast.makeText(ShowFormRequestActivity.this, "Success"+response.errorBody().string(), Toast.LENGTH_SHORT).show();


                        arrayList.clear();
                        Log.e("sdfsdfsd", response.toString());
                        JSONArray jsonArray = new JSONArray(response.body());

                        Log.e("jsonArray", jsonArray.toString());
                        for (int i = 0; i < jsonArray.length(); i++) {

                            NewAgencyModel objData = new NewAgencyModel();
                            JSONObject str = jsonArray.getJSONObject(i);

                            if (PreferenceManager.getMenu_TYPE(ShowRequestListActivity.this).equals("Proposed")) {
                                if (PreferenceManager.getPrefUserType(ShowRequestListActivity.this).equals("UH") || PreferenceManager.getPrefUserType(ShowRequestListActivity.this).equals("SH")) {
                                    if (str.getString("UOHApproval").equals("1")) {
                                        objData.setUOHApproval(str.getString("UOHApproval"));
                                        objData.setSOHApproval(str.getString("SOHApproval"));
                                        objData.setHOApproval(str.getString("HOApproval"));
                                        objData.setUHFApproval(str.getString("UHFApproval"));
                                        objData.setCBRApproval(str.getString("CBRApproval"));
                                        objData.setBPClosure(str.getString("BPClosure"));


                                        objData.setId(str.getString("ag_req_id"));
                                        objData.setState(str.getString("StateName"));
                                        objData.setState_id(str.getString("state_id"));
                                        objData.setState_code(str.getString("StateCode"));
                                        objData.setUnit(str.getString("UnitName"));

                                        objData.setUnitId(str.getString("unit_id"));
                                        objData.setTown(str.getString("TownName"));
                                        objData.setLocation(str.getString("LocationName"));
                                        objData.setPopulation(str.getString("population"));
                                        objData.setDistrict(str.getString("CityName"));
                                        objData.setPincode(str.getString("pincode"));

                                        objData.setMain(str.getString("main_copies"));
                                        objData.setJanJagrati(str.getString("jj_copies"));
                                        objData.setNoMainCopy(str.getString("tot_copies"));
                                        objData.setSaleCluster(str.getString("ClusterName"));
                                        objData.setCity_upc(str.getString("city_upc"));
                                        objData.setReason(str.getString("reason"));
                                        objData.setStatus(str.getString("status"));
                                        objData.setSalesOfficeNameJJ(str.getString("SalesOfficeNameJJ"));

                                        objData.setSalesOfficeNameMain(str.getString("SalesOfficeNameMain"));
                                        objData.setSalesDistName(str.getString("SalesDistName"));
                                        objData.setReasonArray(str.getString("AgencyRequestReason_set"));
                                        arrayList.add(objData);


                                    }
                                } else {

                                    if (PreferenceManager.getPrefUserType(ShowRequestListActivity.this).equals("HO")) {

                                        if (str.getString("SOHApproval").equals("1")) {

                                            objData.setUOHApproval(str.getString("UOHApproval"));
                                            objData.setSOHApproval(str.getString("SOHApproval"));
                                            objData.setHOApproval(str.getString("HOApproval"));
                                            objData.setUHFApproval(str.getString("UHFApproval"));
                                            objData.setCBRApproval(str.getString("CBRApproval"));
                                            objData.setBPClosure(str.getString("BPClosure"));


                                            objData.setId(str.getString("ag_req_id"));
                                            objData.setState(str.getString("StateName"));
                                            objData.setState_id(str.getString("state_id"));
                                            objData.setState_code(str.getString("StateCode"));
                                            objData.setUnit(str.getString("UnitName"));

                                            objData.setUnitId(str.getString("unit_id"));
                                            objData.setTown(str.getString("TownName"));
                                            objData.setLocation(str.getString("LocationName"));
                                            objData.setPopulation(str.getString("population"));
                                            objData.setDistrict(str.getString("CityName"));
                                            objData.setPincode(str.getString("pincode"));

                                            objData.setMain(str.getString("main_copies"));
                                            objData.setJanJagrati(str.getString("jj_copies"));
                                            objData.setNoMainCopy(str.getString("tot_copies"));
                                            objData.setSaleCluster(str.getString("ClusterName"));
                                            objData.setCity_upc(str.getString("city_upc"));
                                            objData.setReason(str.getString("reason"));
                                            objData.setStatus(str.getString("status"));
                                            objData.setSalesOfficeNameJJ(str.getString("SalesOfficeNameJJ"));

                                            objData.setSalesOfficeNameMain(str.getString("SalesOfficeNameMain"));
                                            objData.setSalesDistName(str.getString("SalesDistName"));
                                            objData.setReasonArray(str.getString("AgencyRequestReason_set"));
                                            arrayList.add(objData);


                                        }
                                    }
                                }


                            } else {
                                Log.e("weetwetwet", "Else");
                                if (str.getString("UOHApproval").equals("0")) {

                                    objData.setUOHApproval(str.getString("UOHApproval"));
                                    objData.setSOHApproval(str.getString("SOHApproval"));
                                    objData.setHOApproval(str.getString("HOApproval"));
                                    objData.setUHFApproval(str.getString("UHFApproval"));
                                    objData.setCBRApproval(str.getString("CBRApproval"));
                                    objData.setBPClosure(str.getString("BPClosure"));

                                    objData.setId(str.getString("ag_req_id"));
                                    objData.setState_id(str.getString("state_id"));
                                    objData.setState(str.getString("StateName"));
                                    objData.setState_code(str.getString("StateCode"));
                                    objData.setUnit(str.getString("UnitName"));
                                    objData.setUnitId(str.getString("unit_id"));
                                    objData.setTown(str.getString("TownName"));
                                    objData.setLocation(str.getString("LocationName"));
                                    objData.setPopulation(str.getString("population"));
                                    objData.setDistrict(str.getString("CityName"));
                                    objData.setPincode(str.getString("pincode"));

                                    objData.setMain(str.getString("main_copies"));
                                    objData.setJanJagrati(str.getString("jj_copies"));
                                    objData.setNoMainCopy(str.getString("tot_copies"));
                                    objData.setSaleCluster(str.getString("ClusterName"));
                                    objData.setCity_upc(str.getString("city_upc"));
                                    objData.setReason(str.getString("reason"));
                                    objData.setStatus(str.getString("status"));
                                    objData.setSalesOfficeNameJJ(str.getString("SalesOfficeNameJJ"));
                                    objData.setSalesOfficeNameMain(str.getString("SalesOfficeNameMain"));
                                    objData.setSalesDistName(str.getString("SalesDistName"));
                                    objData.setReasonArray(str.getString("AgencyRequestReason_set"));
                                    arrayList.add(objData);
                                }
                            }
                        }

                        adapter2 = new ShowRequestNewAgencyListAdapter(ShowRequestListActivity.this, arrayList, context);
                        mbinding.allnewagencyRec.setAdapter(adapter2);
                        adapter2.notifyDataSetChanged();
                        enableLoadingBar(false);
                        if (arrayList.size() > 0) {
                            mbinding.TvAppointmentCount.setText("Agency Appointment :" + arrayList.size());
                        } else {
                            mbinding.TvAppointmentCount.setText("Agency Appointment : 0");
                        }

                        //  enableLoadingBar(false);
                    } catch (Exception e) {

                        Log.e("asfasfasfsa", e.getMessage());
                    }

                } else {
                    try {
//                        enableLoadingBar(false);
                        Toast.makeText(ShowRequestListActivity.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        enableLoadingBar(false);
                        Toast.makeText(ShowRequestListActivity.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(ShowRequestListActivity.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });

    }

    @Override
    public void onOptionClick(NewAgencyModel liveTest) {

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
        editor.putString(AppsContants.UOHApproveStatus, liveTest.getUOHApproval());
        editor.putString(AppsContants.SOHApproveStatus, liveTest.getSOHApproval());
        editor.putString(AppsContants.HoApproveStatus, liveTest.getHOApproval());
        editor.commit();

        Intent mv = new Intent(ShowRequestListActivity.this, ViewRequestData.class);
        mv.putExtra("MyData", liveTest);
        getIntent().getSerializableExtra("MyData");
        startActivity(mv);

    }

    @Override
    public void onApproveOnClick(NewAgencyModel liveTest, String accessType) {

        accessType = PreferenceManager.getPrefUserType(ShowRequestListActivity.this);


        Log.e("sdgsgsdsgs", "User type " + PreferenceManager.getParentUserType(ShowRequestListActivity.this));
        Log.e("sdgsgsdsgs", accessType);
        Log.e("sdfsdfsdfsf", "HO Approval" + liveTest.getHOApproval());
        getFormStatus(liveTest, accessType);
       /* // if (accessType.equalsIgnoreCase("UOH")) {
        if (accessType.equalsIgnoreCase("UH")) {
            Intent mv = new Intent(ShowRequestListActivity.this, UohApproval.class);
            mv.putExtra("MyData", liveTest);
            getIntent().getSerializableExtra("MyData");
            startActivity(mv);
        } else if (accessType.equalsIgnoreCase("HO")) {
            Intent mv = new Intent(ShowRequestListActivity.this, HOApproval.class);
            mv.putExtra("MyData", liveTest);
            getIntent().getSerializableExtra("MyData");
            startActivity(mv);
        } else if (accessType.equalsIgnoreCase("SH")) {
            Intent mv = new Intent(ShowRequestListActivity.this, SOHApproval.class);
            mv.putExtra("MyData", liveTest);
            getIntent().getSerializableExtra("MyData");
            startActivity(mv);
        }*/
    }


    public void getFormStatus(NewAgencyModel liveTest, String accessType) {
        enableLoadingBar(true);
        Log.e("dataShow", liveTest.getId());

        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getRequestData(liveTest.getId());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    enableLoadingBar(false);
                    assert response.body() != null;
                    try {
                        JSONObject requestData = new JSONObject(response.body());
                        Log.e("dhdfhdfhd", requestData.toString());
                        JSONArray jsonArray = requestData.getJSONArray("AgencyRequestAGDetail_set");
                        if (jsonArray.length() > 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject str = jsonArray.getJSONObject(i);
                                strJJCopies = requestData.getString("jj_copies");
                                MainCir = Integer.parseInt(str.getString("AgencyRequestCIRReq_count"));
                                Kybp = Integer.parseInt(str.getString("AgencyRequestKYBP_count"));
                                Survey = Integer.parseInt(str.getString("AgencyRequestSurvey_count"));
                                JJCir = Integer.parseInt(str.getString("AgencyRequestCIRReqJJ_count"));
                                agreementStatus = Integer.parseInt(str.getString("agreement_form_yn"));
                                OTP = Integer.parseInt(str.getString("otp_validation"));
                            }

                            enableLoadingBar(false);

                            if (Kybp == 1) {
                                if (Survey == 1) {
                                    if (MainCir == 1) {
                                        if (agreementStatus == 1) {
                                            if (strJJCopies.equals("0")) {   /* No JJ copies*/
                                                // if (accessType.equalsIgnoreCase("UOH")) {
                                                if (accessType.equalsIgnoreCase("UH")) {
                                                    Intent mv = new Intent(ShowRequestListActivity.this, UohApproval.class);
                                                    mv.putExtra("MyData", liveTest);
                                                    getIntent().getSerializableExtra("MyData");
                                                    startActivity(mv);
                                                }
                                                else if(accessType.equalsIgnoreCase("HO")) {

                                                    if (liveTest.getUOHApproval().equals("1")) {
                                                        if (liveTest.getSOHApproval().equals("1")) {
                                                            Intent mv = new Intent(ShowRequestListActivity.this, HOApproval.class);
                                                            mv.putExtra("MyData", liveTest);
                                                            getIntent().getSerializableExtra("MyData");
                                                            startActivity(mv);
                                                        } else {
                                                            alertMessage("SOH Approval Pending");
                                                            return;
                                                            //Toast.makeText(ShowRequestListActivity.this, "SOH Approval Pending", Toast.LENGTH_SHORT).show();
                                                        }
                                                    } else {
                                                        alertMessage("UOH Approval Pending");
                                                        return;
                                                        //Toast.makeText(ShowRequestListActivity.this, "UOH Approval Pending", Toast.LENGTH_SHORT).show();
                                                    }
                                                } else if (accessType.equalsIgnoreCase("SH")) {

                                                    if (liveTest.getUOHApproval().equals("1")) {
                                                        Intent mv = new Intent(ShowRequestListActivity.this, SOHApproval.class);
                                                        mv.putExtra("MyData", liveTest);
                                                        getIntent().getSerializableExtra("MyData");
                                                        startActivity(mv);

                                                    } else {
                                                        alertMessage("UOH Approval Pending");
                                                        return;
                                                        //Toast.makeText(ShowRequestListActivity.this, "UOH Approval Pending", Toast.LENGTH_SHORT).show();
                                                    }


                                                } else {

                                                    alertMessage("Unauthorized Access Type");
                                                    return;
                                                    //Toast.makeText(ShowRequestListActivity.this, "Unauthorized Access Type", Toast.LENGTH_SHORT).show();
                                                }


                                            } else {

                                                if (JJCir == 1) {
                                                    // if (accessType.equalsIgnoreCase("UOH")) {
                                                    if (accessType.equalsIgnoreCase("UH")) {
                                                        Intent mv = new Intent(ShowRequestListActivity.this, UohApproval.class);
                                                        mv.putExtra("MyData", liveTest);
                                                        getIntent().getSerializableExtra("MyData");
                                                        startActivity(mv);
                                                    } else if (accessType.equalsIgnoreCase("HO")) {
                                                        if (liveTest.getUOHApproval().equals("1")) {
                                                            if (liveTest.getSOHApproval().equals("1")) {
                                                                Intent mv = new Intent(ShowRequestListActivity.this, HOApproval.class);
                                                                mv.putExtra("MyData", liveTest);
                                                                getIntent().getSerializableExtra("MyData");
                                                                startActivity(mv);
                                                            } else {

                                                                alertMessage("SOH Approval Pending");
                                                                return;
                                                                //Toast.makeText(ShowRequestListActivity.this, "SOH Approval Pending", Toast.LENGTH_SHORT).show();
                                                            }
                                                        } else {
                                                            alertMessage("UOH Approval Pending");
                                                            return;
                                                            // Toast.makeText(ShowRequestListActivity.this, "UOH Approval Pending", Toast.LENGTH_SHORT).show();
                                                        }
                                                    } else if (accessType.equalsIgnoreCase("SH")) {
                                                        if (liveTest.getUOHApproval().equals("1")) {
                                                            Intent mv = new Intent(ShowRequestListActivity.this, SOHApproval.class);
                                                            mv.putExtra("MyData", liveTest);
                                                            getIntent().getSerializableExtra("MyData");
                                                            startActivity(mv);

                                                        } else {
                                                            alertMessage("UOH Approval Pending");
                                                            return;
                                                            //Toast.makeText(ShowRequestListActivity.this, "UOH Approval Pending", Toast.LENGTH_SHORT).show();
                                                        }
                                                    } else {
                                                        alertMessage("Unauthorized Access Type");
                                                        return;
                                                        //Toast.makeText(ShowRequestListActivity.this, "Unauthorized Access Type", Toast.LENGTH_SHORT).show();
                                                    }

                                                } else {
                                                    alertMessage("Please fill JJ form");
                                                    return;
                                                    //Toast.makeText(ShowRequestListActivity.this, "Please fill JJ form", Toast.LENGTH_SHORT).show();
                                                }
                                            }

                                        /*if (OTP == 1) {
                                            if (strJJCopies.equals("0")) {   *//* No JJ copies*//*
                                                // if (accessType.equalsIgnoreCase("UOH")) {
                                                if (accessType.equalsIgnoreCase("UH")) {
                                                    Intent mv = new Intent(ShowRequestListActivity.this, UohApproval.class);
                                                    mv.putExtra("MyData", liveTest);
                                                    getIntent().getSerializableExtra("MyData");
                                                    startActivity(mv);
                                                } else if (accessType.equalsIgnoreCase("HO")) {

                                                    if (liveTest.getUOHApproval().equals("1")) {
                                                        if (liveTest.getSOHApproval().equals("1")) {
                                                            Intent mv = new Intent(ShowRequestListActivity.this, HOApproval.class);
                                                            mv.putExtra("MyData", liveTest);
                                                            getIntent().getSerializableExtra("MyData");
                                                            startActivity(mv);
                                                        } else {
                                                            alertMessage("SOH Approval Pending");
                                                            return;
                                                            //Toast.makeText(ShowRequestListActivity.this, "SOH Approval Pending", Toast.LENGTH_SHORT).show();
                                                        }
                                                    } else {
                                                        alertMessage("UOH Approval Pending");
                                                        return;
                                                        //Toast.makeText(ShowRequestListActivity.this, "UOH Approval Pending", Toast.LENGTH_SHORT).show();
                                                    }
                                                } else if (accessType.equalsIgnoreCase("SH")) {

                                                    if (liveTest.getUOHApproval().equals("1")) {
                                                        Intent mv = new Intent(ShowRequestListActivity.this, SOHApproval.class);
                                                        mv.putExtra("MyData", liveTest);
                                                        getIntent().getSerializableExtra("MyData");
                                                        startActivity(mv);

                                                    } else {
                                                        alertMessage("UOH Approval Pending");
                                                        return;
                                                        //Toast.makeText(ShowRequestListActivity.this, "UOH Approval Pending", Toast.LENGTH_SHORT).show();
                                                    }


                                                } else {

                                                    alertMessage("Unauthorized Access Type");
                                                    return;
                                                    //Toast.makeText(ShowRequestListActivity.this, "Unauthorized Access Type", Toast.LENGTH_SHORT).show();
                                                }


                                            }
                                            else {

                                                if (JJCir == 1) {
                                                    // if (accessType.equalsIgnoreCase("UOH")) {
                                                    if (accessType.equalsIgnoreCase("UH")) {
                                                        Intent mv = new Intent(ShowRequestListActivity.this, UohApproval.class);
                                                        mv.putExtra("MyData", liveTest);
                                                        getIntent().getSerializableExtra("MyData");
                                                        startActivity(mv);
                                                    } else if (accessType.equalsIgnoreCase("HO")) {
                                                        if (liveTest.getUOHApproval().equals("1")) {
                                                            if (liveTest.getSOHApproval().equals("1")) {
                                                                Intent mv = new Intent(ShowRequestListActivity.this, HOApproval.class);
                                                                mv.putExtra("MyData", liveTest);
                                                                getIntent().getSerializableExtra("MyData");
                                                                startActivity(mv);
                                                            } else {

                                                                alertMessage("SOH Approval Pending");
                                                                return;
                                                                //Toast.makeText(ShowRequestListActivity.this, "SOH Approval Pending", Toast.LENGTH_SHORT).show();
                                                            }
                                                        } else {
                                                            alertMessage("UOH Approval Pending");
                                                            return;
                                                            // Toast.makeText(ShowRequestListActivity.this, "UOH Approval Pending", Toast.LENGTH_SHORT).show();
                                                        }
                                                    } else if (accessType.equalsIgnoreCase("SH")) {
                                                        if (liveTest.getUOHApproval().equals("1")) {
                                                            Intent mv = new Intent(ShowRequestListActivity.this, SOHApproval.class);
                                                            mv.putExtra("MyData", liveTest);
                                                            getIntent().getSerializableExtra("MyData");
                                                            startActivity(mv);

                                                        } else {
                                                            alertMessage("UOH Approval Pending");
                                                            return;
                                                            //Toast.makeText(ShowRequestListActivity.this, "UOH Approval Pending", Toast.LENGTH_SHORT).show();
                                                        }
                                                    } else {
                                                        alertMessage("Unauthorized Access Type");
                                                        return;
                                                        //Toast.makeText(ShowRequestListActivity.this, "Unauthorized Access Type", Toast.LENGTH_SHORT).show();
                                                    }

                                                } else {
                                                    alertMessage("Please fill JJ form");
                                                    return;
                                                    //Toast.makeText(ShowRequestListActivity.this, "Please fill JJ form", Toast.LENGTH_SHORT).show();
                                                }
                                            }
                                        }
                                        else {
                                            alertMessage("Please Verify OTP");
                                            return;
                                            //Toast.makeText(ShowRequestListActivity.this, "Please Verify OTP", Toast.LENGTH_SHORT).show();
                                        }*/
                                        } else {
                                            alertMessage("Please fill Agreement form");
                                            return;
                                        }
                                    } else {
                                        alertMessage("Please fill Main Circulation form");
                                        return;
                                        //Toast.makeText(ShowRequestListActivity.this, "Please fill Main Circulation form", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    alertMessage("Please fill Survey form");
                                    return;
                                    // Toast.makeText(ShowRequestListActivity.this, "Please fill Survey form", Toast.LENGTH_SHORT).show();
                                }

                            } else {
                                alertMessage("Please fill Kybp form");
                                return;
                                //Toast.makeText(ShowRequestListActivity.this, "Please fill Kybp form", Toast.LENGTH_SHORT).show();

                            }

                        } else {

                            alertMessage("You didn't create agency for this request");
                            try {
                                Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();
                            } catch (IOException e) {
                                Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                                e.printStackTrace();
                            }

                        }
                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }


    private void alertMessage(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })

                .show();
    }


    public void getLocation() {
        locationMaster.clear();
        // Calling JSON10011137MP
        //Call<String> call = SamriddhiApplication.getmInstance().getApiService().getLocation(PreferenceManager.getAgentId(context));
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getLocationCity(PreferenceManager.getAgentId(context), unitId);
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            for (int i = 0; i < objData.length(); i++) {
                                JSONObject objStr = objData.getJSONObject(i);
                                DropDownModel dropDownModel = new DropDownModel();
                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("location_name"));
                                locationMaster.add(dropDownModel);
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Toast.makeText(context, R.string.err_ifsc_code_wrong, Toast.LENGTH_SHORT).show();
            }

        });
    }


    public void getState() {
        stateMaster.clear();
        // Calling JSON10011137MP
        //enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getStateMaster(PreferenceManager.getAgentId(context));
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;


                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        Log.e("sgfsdgsdgs", jsonObject.toString());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("state_name"));

                                // dropDownModel.setName(objStr.getJSONObject("fields").getString("jj_flag"));

                                stateMaster.add(dropDownModel);

                            }
                        }

                    } catch (JSONException e) {
                        Log.e("sdgsdgsdg", e.getMessage());
                    }
                    //   enableLoadingBar(false);
                } else {

                    try {
                        Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {

                Toast.makeText(context, R.string.err_ifsc_code_wrong, Toast.LENGTH_SHORT).show();
            }

        });
    }


    public void getUnit(String stateId) {
        unitMaster.clear();
        Log.e("wetwetwt", "Unit");
      /*  AndroidNetworking.post("http://dev.dbsamriddhi.in/agency/getUnit/"+PreferenceManager.getAgentId(this)+"/"+stateId)
                .addHeaders("Authorization", Credentials.basic(Constant.PORTAL_USER_NAME, Constant.PORTAL_P))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("wetwetwt", response.toString());
                            JSONArray jsonArray = new JSONArray(response.getString("data"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject objStr = jsonArray.getJSONObject(i);
                                JSONObject jsonObject=new JSONObject(objStr.getString("fields"));
                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(jsonObject.getString("unit_name"));
                                dropDownModel.setName(jsonObject.getString("jj_flag"));
                                unitMaster.add(dropDownModel);

                            }
                        } catch (Exception ex) {
                            Log.e("wetwetwt", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        Log.e("wetwetwt", anError.getMessage());
                    }
                });*/

        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getUnit(PreferenceManager.getAgentId(context), stateId);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("unit_name"));

                                dropDownModel.setName(objStr.getJSONObject("fields").getString("jj_flag"));

                                unitMaster.add(dropDownModel);

                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //   enableLoadingBar(false);
                } else {

                    try {
                        Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {

                Toast.makeText(context, R.string.err_ifsc_code_wrong, Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Override
    public void onOptionClick(DropDownModel data) {
        if (dropSelectType.equalsIgnoreCase("state")) {

            stateId = data.getId();
            mbinding.tvState.setText(data.getDescription());
            Log.e("Vuduwehr", locationId);
            getUnit(stateId);
            //  getShowRequest();

        }

        if (dropSelectType.equalsIgnoreCase("unit")) {

            unitId = data.getId();
            mbinding.tvUnit.setText(data.getDescription());
            Log.e("Vuduwehr", unitId);
            getLocation();
            getShowRequest();
        }


        if (dropSelectType.equalsIgnoreCase("location")) {

            locationId = data.getId();
            mbinding.tvLocation.setText(data.getDescription());
            Log.e("Vuduwehr", locationId);
            getShowRequest();
        }


        //  getShowRequest();
        Util.hideDropDown();
    }
}
