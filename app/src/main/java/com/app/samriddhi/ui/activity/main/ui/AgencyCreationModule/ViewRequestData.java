package com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.app.samriddhi.R;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.response.JsonObjectResponse;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.Perposed_Agency_Adapter;
import com.app.samriddhi.base.adapter.Reason_agency_adapter;

import com.app.samriddhi.databinding.ActivityViewRequestDataBinding;
import com.app.samriddhi.prefernces.PreferenceManager;

import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AppsContants;
import com.app.samriddhi.ui.activity.main.ui.InVoiceActivity;
import com.app.samriddhi.ui.model.DropDownModel;
import com.app.samriddhi.ui.model.NewAgencyModel;
import com.app.samriddhi.ui.model.NewsPaperModel;
import com.app.samriddhi.ui.model.ProposedAgencyModel;
import com.app.samriddhi.ui.model.ReasonAdapterModel;
import com.app.samriddhi.ui.model.UserModel;
import com.app.samriddhi.util.AlertUtility;
import com.app.samriddhi.util.Globals;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.aspectj.lang.reflect.CatchClauseSignature;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewRequestData extends BaseActivity implements Perposed_Agency_Adapter.OnclickListener {

    NewAgencyModel dataView;
    Intent i;
    Perposed_Agency_Adapter adapter;
    private static AlertDialog alertDialog;
    ArrayList<ProposedAgencyModel> proposedAgencyModels;
    ArrayList<ReasonAdapterModel> reasonAdapterModels;
    Context context;

    ViewRequestData viewRequestData;

    Globals g = Globals.getInstance(context);
    EditText ed1, ed2, ed3, ed4;
    String getOtp = "";

    PopupWindow popupWindow;
    Reason_agency_adapter reason_agency_adapter;
    ActivityViewRequestDataBinding mbinding;
    String strAgentCode="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strAgentCode = AppsContants.sharedpreferences.getString(AppsContants.stateCode, "");

        mbinding = DataBindingUtil.setContentView(this, R.layout.activity_view_request_data);
        mbinding.toolbar.setTitle("Request List Data");
        setSupportActionBar(mbinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        i = getIntent();
        this.context = this;
        this.viewRequestData = this;
        reasonAdapterModels = new ArrayList<>();

        getMaritialStatus();
        getDepartment();
        getDesignation();
        dataView = (NewAgencyModel) i.getSerializableExtra("MyData");
        proposedAgencyModels = new ArrayList<>();
        mbinding.tvTown.setText(dataView.getTown());
        getNewsPaper(dataView.getState_code());

        Log.e("dfgdfgdf","State_id"+" "+dataView.getState_id());
        Log.e("dfgdfgdf","Unit_id"+" "+dataView.getUnitId());
        AppsContants.sharedpreferences=getSharedPreferences(AppsContants.MyPREFERENCES,MODE_PRIVATE);
        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
        editor.putString(AppsContants.StateId, dataView.getState_id());
        editor.putString(AppsContants.UnitId, dataView.getUnitId());
        editor.putString(AppsContants.SalesMain, dataView.getSalesOfficeNameMain());
        editor.putString(AppsContants.SalesJJ, dataView.getSalesOfficeNameJJ());
        editor.putString(AppsContants.Reason, dataView.getReason());
        editor.commit();

        mbinding.population.setText(dataView.getPopulation());
        mbinding.noofcopyAc.setText(dataView.getNoMainCopy());
        mbinding.main.setText(dataView.getMain());

        g.jjCirNoOfCopy = Double.parseDouble(dataView.getJanJagrati());
        g.mainCirNoOfCopy = Double.parseDouble(dataView.getMain());
        mbinding.janJagriti.setText(dataView.getJanJagrati());
        mbinding.tvDistrict.setText(dataView.getDistrict());
        mbinding.pinMain.setText(dataView.getPincode());


        mbinding.tvLocation.setText(dataView.getLocation());
        mbinding.tvState.setText(dataView.getState());
        mbinding.tvUnit.setText(dataView.getUnit());
        mbinding.tvSaleDistrict.setText(dataView.getSalesDistName());
        Log.e("ddsdadadad", dataView.getSalesDistName());

        mbinding.janjagritiSaleOffice.setText(dataView.getSalesOfficeNameJJ());
        mbinding.mainSaleOffice.setText(dataView.getSalesOfficeNameMain());
        mbinding.tvCluster.setText(dataView.getSaleCluster());
        mbinding.tvReason.setText(new StringBuilder().append(dataView.getReason().substring(0, 1).toUpperCase()).append(dataView.getReason().substring(1)).toString());


        g.unit_main = dataView.getUnitId();

        if (dataView.getCity_upc().equals("City")) {
            mbinding.cityRb.setChecked(true);
            mbinding.cityRb.setVisibility(View.VISIBLE);
            g.mainSaleGroup = "CRL";
        } else {
            mbinding.upcRb.setChecked(true);
            mbinding.upcRb.setVisibility(View.VISIBLE);
            g.mainSaleGroup = "CRU";
        }
        g.unitId = dataView.getUnitId();
        if (dataView.getSalesOfficeNameMain().length() == 0) {
            g.mainSaleOffice = "";
        } else {
            g.mainSaleOffice = dataView.getSalesOfficeNameMain();
        }


        if (dataView.getSalesOfficeNameJJ().length() == 0) {
            g.jjSaleOffice = "";
        } else {
            g.jjSaleOffice = dataView.getSalesOfficeNameJJ();
        }

        if (dataView.getJanJagrati().length() == 0 || dataView.getJanJagrati().equalsIgnoreCase("0")) {
            g.jjSaleOfficeCopy = "0";
        } else {
            g.jjSaleOfficeCopy = dataView.getJanJagrati();
        }


        if (dataView.getMain().length() == 0 || dataView.getMain().equalsIgnoreCase("0")) {
            g.mainSaleOfficeCopy = "0";
        } else {
            Log.e("dataMain", dataView.getMain());
            g.mainSaleOfficeCopy = dataView.getMain();
        }


        mbinding.addNewAgency.setOnClickListener(v -> {
            AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
            builder2.setCancelable(false);
            View view2 = LayoutInflater.from(context).inflate(R.layout.agency_add_layout, null);
            EditText name = view2.findViewById(R.id.agencyName);
            EditText agentName = view2.findViewById(R.id.agentName);

            EditText agent_mobile = view2.findViewById(R.id.agent_mobile);
            EditText otp = view2.findViewById(R.id.otp);

            Button btnSubmit = view2.findViewById(R.id.getAgencyBtn);
            ImageView closeImg = view2.findViewById(R.id.closeImg);
            btnSubmit.setOnClickListener(v1 -> {

                if (name.getText().toString().length() == 0) {
                    alertMessage("Please Enter The Name");
                    return;
                }
                if (agentName.getText().toString().length() == 0) {
                    alertMessage("Please Enter The Agent Name");
                    return;
                }
                if (agent_mobile.getText().toString().length() == 0 || agent_mobile.getText().toString().length() < 10) {
                    alertMessage("Please Enter The Valid Agent Mobile");
                    return;
                }
                addAgency(name.getText().toString(), agentName.getText().toString(), agent_mobile.getText().toString());

            });
            builder2.setView(view2);
            closeImg.setOnClickListener(v1 -> {
                alertDialog.cancel();
                alertDialog.dismiss();
            });

            alertDialog = builder2.create();
            alertDialog.show();
        });
        // agency recycler view
        mbinding.agencyDetailsRec.setNestedScrollingEnabled(true);
        mbinding.agencyDetailsRec.setHasFixedSize(true);
        mbinding.agencyDetailsRec.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        // reason recycler view
        mbinding.agencyReasonRec.setNestedScrollingEnabled(true);
        mbinding.agencyReasonRec.setHasFixedSize(true);
        mbinding.agencyReasonRec.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        g.mainNoOfCopy = Double.parseDouble(dataView.getNoMainCopy());
        g.reasonTypeName = dataView.getReason();
        try {

            Log.e("datag", dataView.getReasonArray());
            JSONArray array = new JSONArray(dataView.getReasonArray());
            g.reasonAdapterList.clear();
            for (int i = 0; i < array.length(); i++) {

                JSONObject str = array.getJSONObject(i);
                ReasonAdapterModel obj = new ReasonAdapterModel();
                obj.setAg_code(str.getString("ag_code"));
                obj.setAg_copies(str.getString("ag_copies"));
                obj.setAg_name(str.getString("ag_name"));
                obj.setAgency_location(str.getString("agency_location"));
                obj.setAgency_location(str.getString("agency_location"));


                obj.setSubag_code(str.getString("subag_code"));
                obj.setSubag_copies(str.getString("subag_copies"));

                obj.setAg_reason(str.getString("ag_reason"));
                obj.setAsd(str.getString("asd"));
                obj.setUnbilled_amount(str.getString("unbilled_amount"));
                obj.setOutstanding(str.getString("outstanding"));
                obj.setTotal_outstanding(str.getString("total_outstanding"));

                Log.e("data" + i, str.getString("ag_code"));
                g.reasonAdapterList.add(obj);
                reasonAdapterModels.add(obj);

            }

            reason_agency_adapter = new Reason_agency_adapter(context, reasonAdapterModels);
            mbinding.agencyReasonRec.setAdapter(reason_agency_adapter);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        //     getAgentData();
        // Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();


    }



    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    private void alertMessage(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })

                .show();
    }

    public void addAgency(String name, String agency, String mobile) {


        enableLoadingBar(true);
        Map<String, Object> params = new HashMap<>();
        params.put("ag_req_id", dataView.getId());
        params.put("agenct_name", agency);
        params.put("agency_name", name);
        params.put("ag_mono_otp", mobile);


        params.put("otp_validation", null);
        params.put("cluster_id", null);
        params.put("salesdist_id", null);
        params.put("salesoff_id", null);

        params.put("price_group_id", null);
        params.put("custgrp", null);
        params.put("final_status", 0);
        params.put("create_user", PreferenceManager.getAgentId(context));
        params.put("update_user", PreferenceManager.getAgentId(context));

        Call<String> call = SamriddhiApplication.getmInstance().getApiService().agDetail(params);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;


                    try {


                        JSONObject jsonObject = new JSONObject(response.body());


                        alertDialog.dismiss();
                        alertDialog.cancel();
                        AlertUtility.showAlertWithListener(ViewRequestData.this, "You have created new agency please scroll", null);


                        getAgentData();

                    } catch (Exception e) {

                        e.printStackTrace();
                    }
                    enableLoadingBar(false);
                } else {
                    try {
                        Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });

    }

    @Override
    protected void onResume() {
        //  Toast.makeText(context,"Bhupesh sen",Toast.LENGTH_SHORT).show();
        getAgentData();
        super.onResume();
    }

    public void getAgentData() {

        proposedAgencyModels.clear();
        Log.e("eyeryeryery", dataView.getId());
        // enableLoadingBar(true);
        Map<String, String> params = new HashMap<>();
        params.put("requestId", dataView.getId());
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getAgDetail(dataView.getId());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    enableLoadingBar(false);

                    Log.e("sdsdgdsgsdgs","Response"+" "+response.body());


                    assert response.body() != null;


                    try {
                        JSONArray jsonArray = new JSONArray(response.body());

                        if (jsonArray.length() > 0) {
                            Log.e("eyeryeryery", jsonArray.toString());
                            for (int i = 0; i < jsonArray.length(); i++) {

                                ProposedAgencyModel objData = new ProposedAgencyModel();
                                JSONObject str = jsonArray.getJSONObject(i);
                                objData.setId(str.getString("ag_detail_id"));
                                objData.setAgency_name(str.getString("agency_name"));

                                objData.setAgenct_name(str.getString("agenct_name"));

                                objData.setMobile(str.getString("ag_mono_otp"));
                                objData.setRequestId(str.getString("ag_req_id"));
                                objData.setOtp_validation(str.getString("otp_validation"));
                                objData.setAgencyRequestCIRReq_count(str.getInt("AgencyRequestCIRReq_count"));

                                objData.setAgencyRequestKYBP_count(str.getInt("AgencyRequestKYBP_count"));

                                objData.setAgencyRequestSurvey_count(str.getInt("AgencyRequestSurvey_count"));

                                objData.setAgencyRequestCIRReqJJ_count(str.getInt("AgencyRequestCIRReqJJ_count"));
                                objData.setAgreement_form_yn(str.getInt("agreement_form_yn"));

                                proposedAgencyModels.add(objData);
                                // g.reasonAdapterList.add(obj);
                            }

                            adapter = new Perposed_Agency_Adapter(context, proposedAgencyModels, viewRequestData);
                            mbinding.agencyDetailsRec.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
//                            adapter = new Perposed_Agency_Adapterbhs(context,proposedAgencyModels,viewRequestData);
//                            mbinding.agencyDetailsRec.setAdapter(adapter);
//                            adapter.notifyDataSetChanged();
                        } else {

                        }

                        enableLoadingBar(false);

                        //  enableLoadingBar(false);
                    } catch (Exception e) {

                        Log.e("dfgdgdfgfd",e.getMessage());
                    }

                } else {
                    try {
                        Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void getDepartment() {
        // Calling JSON
        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getDepartment(PreferenceManager.getAgentId(context));
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                enableLoadingBar(false);
                if (response.isSuccessful()) {

                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            g.departmentMasterArray.clear();
                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("department_name"));
                                dropDownModel.setId(objStr.getString("pk"));
                                g.departmentMasterArray.add(dropDownModel);
                            }
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });

    }


    @Override
    public void onItemClick(String formType, ProposedAgencyModel proposedAgencyModel) {


        if (formType.equalsIgnoreCase("optMobile")) {
            popupOtpWindow(proposedAgencyModel);
        }
        if (formType.equalsIgnoreCase("survey")) {


            if (proposedAgencyModel.getAgencyRequestSurvey_count() > 0) {
                g.SURVEYStatus = 1;
            } else {
                g.SURVEYStatus = 0;
            }

            g.ActionType = 1;
            Intent intent = new Intent(context, SurveyFormActivity.class);
            g.agentId = proposedAgencyModel.getId();

            g.agentNameStr = proposedAgencyModel.getAgenct_name();
            g.agencyNameStr = proposedAgencyModel.getAgency_name();
            g.agentMobileStr = proposedAgencyModel.getMobile();
            g.requestId = dataView.getId();
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);


        }
        if (formType.equalsIgnoreCase("kybp")) {

            if (proposedAgencyModel.getAgencyRequestKYBP_count() > 0) {
                g.KYBPStatus = 1;
            } else {
                g.KYBPStatus = 0;
            }
            g.ActionType = 1;

            Intent intent = new Intent(context, KYBPActivity.class);
            g.agentId = proposedAgencyModel.getId();
            g.agentMobileStr = proposedAgencyModel.getMobile();
            g.agentNameStr = proposedAgencyModel.getAgenct_name();
            g.agencyNameStr = proposedAgencyModel.getAgency_name();
            g.requestId = dataView.getId();
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        }

        if (formType.equalsIgnoreCase("cir_rpt")) {

            if (proposedAgencyModel.getAgencyRequestCIRReq_count() > 0) {
                g.CIRStatus = 1;
            } else {
                g.CIRStatus = 0;
            }
            g.ActionType = 1;

            Intent intent = new Intent(context, Circulation_MainForm.class);
            g.agentId = proposedAgencyModel.getId();
            g.agentNameStr = proposedAgencyModel.getAgenct_name();
            g.agencyNameStr = proposedAgencyModel.getAgency_name();
            g.agentMobileStr = proposedAgencyModel.getMobile();
            g.requestId = dataView.getId();
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        }

        if (formType.equalsIgnoreCase("jj")) {

            if (proposedAgencyModel.getAgencyRequestCIRReqJJ_count() > 0) {
                g.JJCIRStatus = 1;
            } else {
                g.JJCIRStatus = 0;
            }
            g.ActionType = 1;

            Intent intent = new Intent(context, Circulation_JJForm.class);
            g.agentId = proposedAgencyModel.getId();
            g.agentNameStr = proposedAgencyModel.getAgenct_name();
            g.agencyNameStr = proposedAgencyModel.getAgency_name();
            g.agentMobileStr = proposedAgencyModel.getMobile();
            g.requestId = dataView.getId();
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        }

    }

    public void getMaritialStatus() {
        g.getMaritialArray.clear();
        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getMaritial(PreferenceManager.getAgentId(context));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;
                    Log.e("getMaritial", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            for (int i = 0; i < objData.length(); i++) {

                                JSONObject objStr = objData.getJSONObject(i);
                                DropDownModel dropDownModel = new DropDownModel();
                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("maritial_status"));
                                g.getMaritialArray.add(dropDownModel);
                            }
                        }
                        enableLoadingBar(false);
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void getDesignation() {
        // Calling JSON
        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getDesignation(PreferenceManager.getAgentId(context));
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                enableLoadingBar(false);
                if (response.isSuccessful()) {


                    assert response.body() != null;
                    Log.e("designationMasterArray", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            g.designationMasterArray.clear();
                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("designation_name"));
                                dropDownModel.setId(objStr.getString("pk"));
                                g.designationMasterArray.add(dropDownModel);
                            }
                        }

                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(ViewRequestData.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(ViewRequestData.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(ViewRequestData.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });

    }

    public void getNewsPaper(String name) {
       // Log.e("sdfsdfsdf",name);
        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getNewsPaper(PreferenceManager.getAgentId(context)+"/"+name);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;
                    Log.e("newsdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {
                            g.newsPaperNameMasterArray.clear();

                            for (int i = 0; i < objData.length(); i++) {

                                JSONObject objStr = objData.getJSONObject(i);
                                NewsPaperModel dropDownModel = new NewsPaperModel();
                                dropDownModel.setPk(objStr.getString("pk"));
                                dropDownModel.setStatus(false);
                                dropDownModel.setPaperName(objStr.getJSONObject("fields").getString("newspaper_name"));
                                g.newsPaperNameMasterArray.add(dropDownModel);
                            }
                        }
                        enableLoadingBar(false);
                    } catch (JSONException e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Log.e("dfgsds","764");
                       // Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (Exception e) {
                        Log.e("dfgsds","768");
                       // Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void popupOtpWindow(ProposedAgencyModel proposedAgencyModel) {
        View popupView = null;


        popupView = LayoutInflater.from(ViewRequestData.this).inflate(R.layout.popup_otp, null);


        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0);


        TextView sendOtp = popupView.findViewById(R.id.sendOtp);
        TextView sunmit_button = popupView.findViewById(R.id.otp_submit);
        ed1 = popupView.findViewById(R.id.et1);
        ed1.requestFocus();
        ed1.setFocusable(true);


        ed2 = popupView.findViewById(R.id.et2);
        ed3 = popupView.findViewById(R.id.et3);
        ed4 = popupView.findViewById(R.id.et4);
        ImageView img = popupView.findViewById(R.id.close);

        Log.e("dfjkjgdkgd", proposedAgencyModel.getMobile());

        optVerifyByMobile(proposedAgencyModel.getMobile());

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });

        ed1.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {


            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

                if (s != null) {
                    ed2.requestFocus();
                }
            }
        });

        ed2.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {


            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

                if (s != null) {
                    ed3.requestFocus();
                }
            }
        });

        ed3.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {


            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            public void afterTextChanged(Editable s) {

                if (s != null) {
                    ed4.requestFocus();
                }
            }
        });


        sendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                optVerifyByMobile(proposedAgencyModel.getMobile());
            }
        });
        sunmit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ed1.getText().toString().length() == 0) {
                    alertMessage("Please enter otp number ");
                    return;
                }
                if (ed2.getText().toString().length() == 0) {
                    alertMessage("Please enter otp number ");
                    return;
                }
                if (ed3.getText().toString().length() == 0) {
                    alertMessage("Please enter otp number ");
                    return;
                }
                if (ed4.getText().toString().length() == 0) {
                    alertMessage("Please enter otp number ");
                    return;
                }
                String enterOtp = ed1.getText().toString() + ed2.getText().toString() + ed3.getText().toString() + ed4.getText().toString();

                if (enterOtp.equalsIgnoreCase(getOtp)) {
                    Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
                    updateAgencyOtpStatus(proposedAgencyModel);


                } else {
                    Toast.makeText(context, "Not Success", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    //OTP VERIFY

    public void optVerifyByMobile(String mobile) {

        Log.e("mobile no", mobile);
        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().agencyOtpVerify(mobile);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;
                    Log.e("newsdata", response.body());
                    try {

                        JSONObject jsonObject = new JSONObject(response.body());
                        JSONObject objData = jsonObject.getJSONObject("data");
                        String status = objData.getString("mobileotpsent");
                        if (status.equalsIgnoreCase("T")) {
                            Toast.makeText(ViewRequestData.this, "We send otp on your mobile please check your message box ", Toast.LENGTH_SHORT).show();

                            String otp = objData.getString("otp");
                            getOtp = otp;

                        }
                        enableLoadingBar(false);
                    } catch (JSONException e) {
                        enableLoadingBar(false);
                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                    enableLoadingBar(false);
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }

    //Agency otp verify
    public void updateAgencyOtpStatus(ProposedAgencyModel proposedAgencyModel) {
        enableLoadingBar(true);


        Map<String, Object> data = new HashMap<>();


        data.put("ag_detail_id", Integer.parseInt(proposedAgencyModel.getId()));
        data.put("ag_req_id", Integer.parseInt(proposedAgencyModel.getRequestId()));
        data.put("agenct_name", proposedAgencyModel.getAgenct_name());
        data.put("agency_name", proposedAgencyModel.getAgency_name());
        data.put("ag_mono_otp", proposedAgencyModel.getMobile());
        data.put("otp_validation", "1");
        Gson gson = new Gson();
        String data1 = gson.toJson(proposedAgencyModel);
        Log.e("proposedAgencyModel", data1);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().agencyVerifyOtp(proposedAgencyModel.getId(), data);
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                enableLoadingBar(false);
                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("responseAgency", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                        popupWindow.dismiss();
                        Toast.makeText(ViewRequestData.this, "Successfully Verify ", Toast.LENGTH_SHORT).show();
                        getAgentData();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    enableLoadingBar(false);
                } else {
                    try {
                        Toast.makeText(ViewRequestData.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(ViewRequestData.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                    enableLoadingBar(false);
                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(ViewRequestData.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }
        });

    }

}
