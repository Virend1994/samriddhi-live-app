package com.app.samriddhi.ui.view;

import com.app.samriddhi.ui.model.DashBoardData;
import com.app.samriddhi.ui.model.LogoutResultModel;

public interface IDashboardView extends IView {
    void onSuccess(DashBoardData dashBoardModel, String lastMonthBillAmt, String hintCount, Integer openCount, Integer closeCount);
    void onLogoutSuccess(LogoutResultModel logoutResultModel);


}
