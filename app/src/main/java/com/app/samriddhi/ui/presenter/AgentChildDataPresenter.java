package com.app.samriddhi.ui.presenter;

import com.app.samriddhi.R;
import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.response.JsonObjectResponse;
import com.app.samriddhi.base.BasePresenter;
import com.app.samriddhi.ui.model.AgentChildResultModel;
import com.app.samriddhi.ui.view.IAgentChildView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AgentChildDataPresenter extends BasePresenter<IAgentChildView> {
    /**
     * from this get the agnets child data
     * @param bpCode Agent id
     */

    public void getAgentChildData(String bpCode) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getAgentChildData(bpCode).enqueue(new Callback<JsonObjectResponse<AgentChildResultModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<AgentChildResultModel>> call, Response<JsonObjectResponse<AgentChildResultModel>> response) {
                getView().enableLoadingBar(false);
                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onSuccess(response.body().body);
                        else
                            getView().onError(getView().getContext().getResources().getString(R.string.str_no_result_found));

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<AgentChildResultModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }
}
