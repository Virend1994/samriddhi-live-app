package com.app.samriddhi.ui.presenter;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.base.BasePresenter;
import com.app.samriddhi.ui.model.DashBoardData;
import com.app.samriddhi.ui.model.LogoutReqModel;
import com.app.samriddhi.ui.model.LogoutResultModel;
import com.app.samriddhi.ui.view.IDashboardView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardPresenter extends BasePresenter<IDashboardView> {


    /**
     * get the copies data fro chart
     * @param agentId logged in user id
     */
    public void getNoOfCopies(String agentId) {
       getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getDashboardCopiesData(agentId).enqueue(new Callback<DashBoardData>() {
            @Override
            public void onResponse(Call<DashBoardData> call, Response<DashBoardData> response) {
                getView().enableLoadingBar(false);

                if (response.isSuccessful() && response.code() == 200) {
                    if (response.body() != null)
                        getView().onSuccess(response.body(), response.body().getLastmonthYearBillAmt(), response.body().getHintCount(), response.body().getOpenCount(), response.body().getClosedCount());

                }

            }

            @Override
            public void onFailure(Call<DashBoardData> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });

    }







    /**
     * get the dashboard data fro internal users with no cache feature.
     * @param agentId logged in user id
     */
    public void getInternalUsersDashBoardData(String agentId) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getDashboardDataForInternalUsers(agentId).enqueue(new Callback<DashBoardData>() {
            @Override
            public void onResponse(Call<DashBoardData> call, Response<DashBoardData> response) {
                getView().enableLoadingBar(false);

                if (response.isSuccessful() && response.code() == 200) {
                    if (response.body() != null)
                        getView().onSuccess(response.body(), response.body().getLastmonthYearBillAmt(), response.body().getHintCount(), response.body().getOpenCount(), response.body().getClosedCount());

                }

            }

            @Override
            public void onFailure(Call<DashBoardData> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });

    }
    public void getLogoutStatus(LogoutReqModel logoutReqModel) {
        getView().enableLoadingBar(true);

        SamriddhiApplication.getmInstance().getApiService().getLogoutStatus(logoutReqModel).enqueue(new Callback<LogoutResultModel>() {
            @Override
            public void onResponse(Call<LogoutResultModel> call, Response<LogoutResultModel> response) {
                getView().enableLoadingBar(false);


                if (response.isSuccessful() && response.code() == 200) {
                    if (response.body() != null)
                        getView().onLogoutSuccess(response.body());

                }

            }

            @Override
            public void onFailure(Call<LogoutResultModel> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }
}
