package com.app.samriddhi.ui.activity.main.ui.OrderSupply;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.samriddhi.R;
import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.DropdownMenuAdapter;
import com.app.samriddhi.base.adapter.OrderSupplyAdapter;
import com.app.samriddhi.databinding.ActivityOrderSupplyBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.model.CIR_Order_Update;
import com.app.samriddhi.ui.model.DropDownModel;
import com.app.samriddhi.ui.model.OrderSupplyModel;
import com.app.samriddhi.util.Util;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderSupply extends BaseActivity implements OrderSupplyAdapter.OnClickEditListener, DropdownMenuAdapter.OnMeneuClickListnser {
    private ActivityOrderSupplyBinding mBinding;
    private OrderSupply orderSupply;
    public ArrayList<DropDownModel> unitMaster;
    ArrayList<DropDownModel> EditionMasterArray;
    private String editionVal, saleOrgVal;

    public ArrayList<CIR_Order_Update> cirListData;
    int PoApprovalCount = 0;

    private LinearLayout dateLayout;
    private TextView saleOrg, tvEdition;
    private int lower_limit = 0, uper_limit = 0, base_copy = 0;
    private boolean reasonStatus = false, lowerUpper = false;
    private TextView tvReason;
    private ArrayList<DropDownModel> dpReasonViewSetList;
    private ArrayList<OrderSupplyModel> orderSupplyModelArrayList;
    private OrderSupplyAdapter orderSupplyAdapte;
    private String dropSelectType;
    private Context context;
    private int currentMainCount = 0, currentJJCount = 0, tomorrowMainCount = 0, tomorrowJJCount = 0;

    private void alertMessage(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })

                .show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.context = this;
        this.orderSupply = this;

        dpReasonViewSetList = new ArrayList<>();
        cirListData = new ArrayList<>();
        unitMaster = new ArrayList<>();
        orderSupplyModelArrayList = new ArrayList<>();
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_order_supply);
        mBinding.toolbar.setTitle("Center Executive");


        mBinding.recycleList.setHasFixedSize(true);
        mBinding.recycleList.setNestedScrollingEnabled(true);
        mBinding.recycleList.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mBinding.filter.setVisibility(View.VISIBLE);
        EditionMasterArray = new ArrayList<>();
        mBinding.filter.setOnClickListener(v -> {
            viewFilter();
        });
        //  mBinding.tvUnit.setText(PreferenceManager.getVkorg(context));
        getCurrentDta();
        
        
        getNextDayMasterState();
        getEdition();
        getUnit();


        mBinding.editSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                String text = mBinding.editSearch.getText().toString().toLowerCase(Locale.getDefault());
                Log.e("bhs==>>", text);


                orderSupplyAdapte.getFilter().filter(cs);
                orderSupplyAdapte.notifyDataSetChanged();


            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
        mBinding.btnSubmit.setOnClickListener(v -> {

            Gson gson = new Gson();
            String dataCIRobj = gson.toJson(cirListData);
            genOrderPopupWindow(cirListData);
            //postOrderData(cirListData);
            Log.e("cirListData", dataCIRobj);
        });


    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    public void viewFilter() {


        View popupView = null;


        popupView = LayoutInflater.from(OrderSupply.this).inflate(R.layout.ex_ordersupply_filtter_layout, null);


        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0);


        ImageView close = popupView.findViewById(R.id.close_sampling);
        MaterialButton saveBtn = popupView.findViewById(R.id.btnSave);

        saleOrg = popupView.findViewById(R.id.saleOrg);
        tvEdition = popupView.findViewById(R.id.tvEdition);

        saleOrg.setOnClickListener(v -> {
            dropSelectType = "unit";
            Util.showDropDown(unitMaster, "Select Sale Org", OrderSupply.this, this::onOptionClick);

        });


        tvEdition.setOnClickListener(v -> {
            dropSelectType = "edition";
            Util.showDropDown(EditionMasterArray, "Select Edition", OrderSupply.this, this::onOptionClick);

        });
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                popupWindow.dismiss();


            }
        });


    }

    public void getNextDayMasterState() {
        orderSupplyModelArrayList.clear();
        // Calling JSON


        enableLoadingBar(true);

        mBinding.progressBar.setVisibility(View.VISIBLE);
        Map<String, String> data = new HashMap<>();
        //Util.getCurrentDate()
      //  data.put("ord_date", Util.getCurrentDate());
        data.put("ord_date", Util.getTomorrowDate());
        data.put("vkorg", PreferenceManager.getVkorg(context));
        data.put("vkgrp", PreferenceManager.getvkgrp(context));
        data.put("pstyv", "JTAP");

        Log.e("param", data.toString());
        currentMainCount = 0;
        currentJJCount = 0;
        cirListData.clear();
        //  Call<JsonArrayResponse<OrderSupplyModel>> call =SamriddhiApplication.getmInstance().getApiService().getOrderSupply(data);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getOrderSupplyString(data);
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("master", response.body());

                    try {

                        //;


                        JSONArray obj = new JSONArray(response.body());

                        if (obj.length() > 0) {

                            for (int i = 0; i < obj.length(); i++) {
                                JSONObject strData = obj.getJSONObject(i);
                                OrderSupplyModel dataStr = new OrderSupplyModel();
                                dataStr.setCustName(strData.getString("cust_name"));
                                dataStr.setPva(strData.getString("pva"));
                                dataStr.setSoldToParty(strData.getString("sold_to_party"));
                                dataStr.setShipToParty(strData.getString("ship_to_party"));
                                dataStr.setProposedQty(Integer.valueOf(strData.getString("proposed_qty")));


                                dataStr.setGrossCopy(Integer.valueOf(strData.getString("gross_copy")));

                                if (strData.getString("base_copy").equalsIgnoreCase("")) {
                                    //  Log.e("not ava",strData.getString("zvt_portal_cir_id"));

                                } else {
                                    dataStr.setBaseCopy(strData.getJSONArray("base_copy"));


                                    //  dataStr.setBaseCopy(strData.getJSONArray("base_copy"));
                                }
//
//
                                dataStr.setVbeln(strData.getString("vbeln"));
                                dataStr.setPosnr(strData.getString("posnr"));
                                dataStr.setGjahr(strData.getString("gjahr"));
                                dataStr.setMonat(strData.getString("monat"));
                                dataStr.setVkorg(strData.getString("vkorg"));
                                dataStr.setVtweg(strData.getString("vtweg"));
                                dataStr.setSpart(strData.getString("spart"));
                                dataStr.setPstyv(strData.getString("pstyv"));


                                dataStr.setOrdDate(strData.getString("ord_date"));
                                dataStr.setVgbel(strData.getString("vgbel"));
                                dataStr.setBezei(strData.getString("bezei"));
                                dataStr.setDrerz(strData.getString("drerz"));
                                dataStr.setPva(strData.getString("pva"));
                                dataStr.setMatnr(strData.getString("matnr"));
                                dataStr.setPubName(strData.getString("pub_name"));
                                dataStr.setSoffName(strData.getString("soff_name"));
                                dataStr.setCgName(strData.getString("cg_name"));
                                dataStr.setSdistName(strData.getString("sdist_name"));
                                dataStr.setEditionName(strData.getString("edition_name"));
                                dataStr.setCustName(strData.getString("cust_name"));
                                dataStr.setCityName(strData.getString("city_name"));
                                dataStr.setVkgrp(strData.getString("vkgrp"));
                                dataStr.setSgrpName(strData.getString("sgrp_name"));
                                dataStr.setVkbur(strData.getString("vkbur"));
                                dataStr.setBzirk(strData.getString("bzirk"));
                                dataStr.setRoute(strData.getString("route"));
                                dataStr.setOrdDateF(strData.getString("ord_date_f"));
                                dataStr.setCurrent_copy("");
                                dataStr.setFrom_date("");
                                dataStr.setTo_date("");
                                dataStr.setRemark("");
                                dataStr.setReason("");

                                if (strData.getJSONArray("CIR_Order_Update").length() > 0) {

                                    JSONArray objd = strData.getJSONArray("CIR_Order_Update");
                                    JSONObject dataStr1 = objd.getJSONObject(0);
                                    CIR_Order_Update CIRobj = new CIR_Order_Update();
                                    CIRobj.setCIR_Order_Update_id(dataStr1.getString("pk"));
                                    CIRobj.setVbeln(dataStr1.getJSONObject("fields").getString("vbeln"));
                                    CIRobj.setPosnr(dataStr1.getJSONObject("fields").getString("posnr"));
                                    CIRobj.setGjahr(dataStr1.getJSONObject("fields").getString("gjahr"));
                                    CIRobj.setMonat(dataStr1.getJSONObject("fields").getString("monat"));
                                    CIRobj.setVkorg(dataStr1.getJSONObject("fields").getString("vkorg"));
                                    CIRobj.setVtweg(dataStr1.getJSONObject("fields").getString("vtweg"));
                                    CIRobj.setSpart(dataStr1.getJSONObject("fields").getString("spart"));
                                    CIRobj.setPstyv(dataStr1.getJSONObject("fields").getString("pstyv"));
                                    CIRobj.setSoldToParty(dataStr1.getJSONObject("fields").getString("sold_to_party"));
                                    CIRobj.setShipToParty(dataStr1.getJSONObject("fields").getString("ship_to_party"));
                                    CIRobj.setOrdDate(dataStr1.getJSONObject("fields").getString("ord_date"));
                                    CIRobj.setVgbel(dataStr1.getJSONObject("fields").getString("vgbel"));
                                    CIRobj.setBezei(dataStr1.getJSONObject("fields").getString("bezei"));
                                    CIRobj.setDrerz(dataStr1.getJSONObject("fields").getString("drerz"));
                                    CIRobj.setPva(dataStr1.getJSONObject("fields").getString("pva"));
                                    CIRobj.setMatnr(dataStr1.getJSONObject("fields").getString("matnr"));
                                    CIRobj.setPubName(dataStr1.getJSONObject("fields").getString("pub_name"));
                                    CIRobj.setSoffName(dataStr1.getJSONObject("fields").getString("soff_name"));
                                    CIRobj.setCgName(dataStr1.getJSONObject("fields").getString("cg_name"));
                                    CIRobj.setSdistName(dataStr1.getJSONObject("fields").getString("sdist_name"));
                                    CIRobj.setEditionName(dataStr1.getJSONObject("fields").getString("edition_name"));
                                    CIRobj.setCustName(dataStr1.getJSONObject("fields").getString("cust_name"));
                                    CIRobj.setCityName(dataStr1.getJSONObject("fields").getString("city_name"));
                                    CIRobj.setVkgrp(dataStr1.getJSONObject("fields").getString("vkgrp"));
                                    CIRobj.setSgrpName(dataStr1.getJSONObject("fields").getString("sgrp_name"));
                                    CIRobj.setVkbur(dataStr1.getJSONObject("fields").getString("vkbur"));
                                    CIRobj.setBzirk(dataStr1.getJSONObject("fields").getString("bzirk"));
                                    CIRobj.setRoute(dataStr1.getJSONObject("fields").getString("route"));
                                    CIRobj.setOrdDateF(dataStr1.getJSONObject("fields").getString("ord_date_f"));
                                    CIRobj.setProposedQty(dataStr1.getJSONObject("fields").getInt("proposed_qty"));
                                    CIRobj.setSaleQty(dataStr1.getJSONObject("fields").getInt("sale_qty"));
                                    CIRobj.setSamplingQty(dataStr1.getJSONObject("fields").getInt("sampling_qty"));
                                    CIRobj.setReasonQtyChange(dataStr1.getJSONObject("fields").getString("reason_qty_change"));
                                    CIRobj.setPerIncDec(dataStr1.getJSONObject("fields").getInt("per_inc_dec"));
                                    CIRobj.setCopiesIncDec(dataStr1.getJSONObject("fields").getInt("copies_inc_dec"));
                                    CIRobj.setRemark(dataStr1.getJSONObject("fields").getString("remark"));
                                    CIRobj.setIsApproval(dataStr1.getJSONObject("fields").getInt("is_approval"));


                                    CIRobj.setApproval_status(dataStr1.getJSONObject("fields").getInt("approval_status"));
                                    CIRobj.setApproved_by("");
                                    CIRobj.setIsApproval(dataStr1.getJSONObject("fields").getInt("is_approval"));


                                    CIRobj.setBaseCopy(dataStr1.getJSONObject("fields").getInt("base_copy"));
                                    CIRobj.setLowerLimit(dataStr1.getJSONObject("fields").getInt("lower_limit"));
                                    CIRobj.setUperLimit(dataStr1.getJSONObject("fields").getInt("uper_limit"));

                                    CIRobj.setHoldDateFrom(dataStr1.getJSONObject("fields").getString("hold_date_from"));
                                    CIRobj.setHoldDateTo(dataStr1.getJSONObject("fields").getString("hold_date_to"));
                                    CIRobj.setIsHold(dataStr1.getJSONObject("fields").getInt("is_hold"));
                                    cirListData.add(CIRobj);
                                } else {
                                    CIR_Order_Update CIRobj = new CIR_Order_Update();
                                    CIRobj.setVbeln(dataStr.getVbeln());
                                    CIRobj.setPosnr(dataStr.getPosnr());
                                    CIRobj.setGjahr(dataStr.getGjahr());
                                    CIRobj.setMonat(dataStr.getMonat());
                                    CIRobj.setVkorg(dataStr.getVkorg());
                                    CIRobj.setVtweg(dataStr.getVtweg());
                                    CIRobj.setSpart(dataStr.getSpart());
                                    CIRobj.setPstyv(dataStr.getPstyv());
                                    CIRobj.setSoldToParty(dataStr.getSoldToParty());
                                    CIRobj.setShipToParty(dataStr.getShipToParty());
                                    CIRobj.setOrdDate(dataStr.getOrdDate());
                                    CIRobj.setVgbel(dataStr.getVgbel());
                                    CIRobj.setBezei(dataStr.getBezei());
                                    CIRobj.setDrerz(dataStr.getDrerz());
                                    CIRobj.setPva(dataStr.getPva());
                                    CIRobj.setMatnr(dataStr.getMatnr());
                                    CIRobj.setApproval_status(0);
                                    CIRobj.setApproved_by("");
                                    CIRobj.setPubName(dataStr.getPubName());
                                    CIRobj.setSoffName(dataStr.getSoffName());
                                    CIRobj.setCgName(dataStr.getCgName());
                                    CIRobj.setSdistName(dataStr.getSdistName());
                                    CIRobj.setEditionName(dataStr.getEditionName());
                                    CIRobj.setCustName(dataStr.getCustName());
                                    CIRobj.setCityName(dataStr.getCityName());
                                    CIRobj.setVkgrp(dataStr.getVkgrp());
                                    CIRobj.setSgrpName(dataStr.getSgrpName());
                                    CIRobj.setVkbur(dataStr.getVkbur());
                                    CIRobj.setBzirk(dataStr.getBzirk());
                                    CIRobj.setRoute(dataStr.getRoute());
                                    CIRobj.setOrdDateF(dataStr.getOrdDateF());
                                    CIRobj.setProposedQty(0);
                                    CIRobj.setSaleQty(dataStr.getProposedQty());
                                    CIRobj.setSamplingQty(0);
                                    CIRobj.setReasonQtyChange("");
                                    CIRobj.setApproval_status(0);

                                    CIRobj.setPerIncDec(0);
                                    CIRobj.setCopiesIncDec(0);
                                    CIRobj.setRemark("");
                                    CIRobj.setIsApproval(0);


                                    CIRobj.setBaseCopy(0);
                                    CIRobj.setLowerLimit(0);
                                    CIRobj.setUperLimit(0);

                                    CIRobj.setHoldDateFrom("9999-12-31T00:00:00");
                                    CIRobj.setHoldDateTo("9999-12-31T00:00:00");
                                    CIRobj.setIsHold(1);
                                    cirListData.add(CIRobj);
                                }


                                if (strData.getString("mainjj").equalsIgnoreCase("MAIN")) {
                                    currentMainCount += Integer.valueOf(strData.getString("proposed_qty"));
                                } else if (strData.getString("mainjj").equalsIgnoreCase("JJ")) {
                                    currentJJCount += Integer.valueOf(strData.getString("proposed_qty"));
                                }


                                dataStr.setCIROrderUpdate(strData.getJSONArray("CIR_Order_Update"));

                                orderSupplyModelArrayList.add(dataStr);

                            }


                            mBinding.tvcurrentJJPO.setText(String.valueOf(currentJJCount));
                            mBinding.tvcurrentMainPO.setText(String.valueOf(currentMainCount));

                            // ArrayList<OrderSupplyModel> list;

                            orderSupplyAdapte = new OrderSupplyAdapter(context, orderSupplyModelArrayList, orderSupply, cirListData, "Ex");
                            mBinding.recycleList.setAdapter(orderSupplyAdapte);
                            mBinding.progressBar.setVisibility(View.GONE);
                            getReason();
                        } else {
                            enableLoadingBar(false);
                            Toast.makeText(context, "No data available for today date", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception e) {

                        Log.e("data", e.getMessage());
                        e.printStackTrace();
                    }

                    enableLoadingBar(false);

                } else {
                    try {
                        enableLoadingBar(false);
                        Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(context, R.string.label_something_went_wrong, Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void getCurrentDta() {

        // Calling JSON
        enableLoadingBar(true);
        Map<String, String> data = new HashMap<>();
        //Util.getCurrentDate()

        data.put("ord_date", Util.getCurrentDate());
        data.put("vkorg", PreferenceManager.getVkorg(context));
        data.put("vkgrp", PreferenceManager.getvkgrp(context));
        data.put("pstyv", "JTAP");

        Log.e("param", data.toString());
        tomorrowMainCount = 0;
        tomorrowJJCount = 0;

        //  Call<JsonArrayResponse<OrderSupplyModel>> call =SamriddhiApplication.getmInstance().getApiService().getOrderSupply(data);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getOrderSupplyString(data);
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {

                        //;

                        
                        JSONArray obj = new JSONArray(response.body());
                        if (obj.length() > 0) {
                            for (int i = 0; i < obj.length(); i++) {
                                JSONObject strData = obj.getJSONObject(i);
                                OrderSupplyModel dataStr = new OrderSupplyModel();

                                if (strData.getString("mainjj").equalsIgnoreCase("MAIN")) {
                                    tomorrowMainCount += Integer.valueOf(strData.getString("proposed_qty"));
                                } else if (strData.getString("mainjj").equalsIgnoreCase("JJ")) {
                                    tomorrowJJCount += Integer.valueOf(strData.getString("proposed_qty"));
                                }


                            }

                            mBinding.tvnextDayJJPo.setText(String.valueOf(tomorrowJJCount));
                            mBinding.tvnextDayMainPo.setText(String.valueOf(tomorrowMainCount));

                            int diffMain = currentMainCount - tomorrowMainCount;
                            int diffJJ = currentJJCount - tomorrowJJCount;

                            mBinding.tvDifferentMainCopy.setText(String.valueOf(diffMain));
                            mBinding.tvDifferentJJCopy.setText(String.valueOf(diffJJ));

                        } else {
                            enableLoadingBar(false);
                            Toast.makeText(OrderSupply.this, "No data available for the next date", Toast.LENGTH_LONG).show();
                        }

                    } catch (Exception e) {
                        enableLoadingBar(false);
                        e.printStackTrace();
                    }

                } else {
                    try {
                        enableLoadingBar(false);
                        Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(context, R.string.label_something_went_wrong, Toast.LENGTH_SHORT).show();
            }

        });
    }


    public void sohFiltter() {


        View popupView = null;


        popupView = LayoutInflater.from(OrderSupply.this).inflate(R.layout.uoh_fillter_order, null);


        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0);


        ImageView close = popupView.findViewById(R.id.close_sampling);
        MaterialButton saveBtn = popupView.findViewById(R.id.btnSave);


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                popupWindow.dismiss();


            }
        });


        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();


            }

        });


    }

    public void genOrderPopupWindow(ArrayList<CIR_Order_Update> cirListData) {


        View popupView = null;


        popupView = LayoutInflater.from(OrderSupply.this).inflate(R.layout.generate_order, null);


        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0);


        ImageView close = popupView.findViewById(R.id.close_sampling);
        MaterialButton saveBtn = popupView.findViewById(R.id.btnSave);
        TextView totalAgency = popupView.findViewById(R.id.totalAgency);
        TextView poGen = popupView.findViewById(R.id.poGen);
        TextView approvalCount = popupView.findViewById(R.id.appCount);

        int totalAgencyCount = cirListData.size();
        int PoGenCount = 0;
        PoApprovalCount = 0;
        ArrayList<CIR_Order_Update> cirListDataNew = new ArrayList<>();
        for (int i = 0; i < cirListData.size(); i++) {

            if (cirListData.get(i).getRemark().length() == 0 && cirListData.get(i).getApproval_status() == 0) {
                PoGenCount++;

            } else if (cirListData.get(i).getApproval_status() == 4) {
                //cirListData.get(i).setApproval_status(0);
                CIR_Order_Update obj = cirListData.get(i);
                cirListDataNew.add(obj);
                PoApprovalCount++;
            }

        }
        totalAgency.setText(String.valueOf(totalAgencyCount));
        poGen.setText(String.valueOf(PoGenCount));
        approvalCount.setText(String.valueOf(PoApprovalCount));
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                popupWindow.dismiss();


            }
        });


        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                popupWindow.dismiss();

                if (PoApprovalCount > 0) {
                    postOrderData(cirListDataNew);
                } else {

                    alertMessage("Edit at least one data");
                    return;
                }


            }

        });


    }

    public void exPopupWindow1(int CIRLength, OrderSupplyModel data, ArrayList<OrderSupplyModel> orderSupplyModels, int pos) {


        View popupView = null;


        popupView = LayoutInflater.from(OrderSupply.this).inflate(R.layout.edit_order_supply, null);


        int width = LinearLayout.LayoutParams.MATCH_PARENT;
        int height = LinearLayout.LayoutParams.MATCH_PARENT;
        boolean focusable = true;
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);
        popupWindow.showAtLocation(popupView, Gravity.BOTTOM, 0, 0);


        ImageView close = popupView.findViewById(R.id.close_sampling);
        MaterialButton saveBtn = popupView.findViewById(R.id.btnSave);
        EditText remark_cr = popupView.findViewById(R.id.remark_cr);
        EditText tvCurrentCopy = popupView.findViewById(R.id.tvCurrentCopy);

        TextView tvBaseCopy = popupView.findViewById(R.id.tvBaseCopy);
        RelativeLayout reasonDrop = popupView.findViewById(R.id.reasonDrop);

        dateLayout = popupView.findViewById(R.id.dateLayout);
        TextView from_date = popupView.findViewById(R.id.from_date);
        TextView to_date = popupView.findViewById(R.id.to_date);
        TextView tvDifferentCopy = popupView.findViewById(R.id.tvDifferentCopy);
        tvReason = popupView.findViewById(R.id.tvReason);
        tvCurrentCopy.setText(data.getProposedQty().toString());

        to_date.setOnClickListener(v -> {
            int mYear, mMonth, mDay;
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(OrderSupply.this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {


                            String monthYear = "", DayOfMonth = "";
                            if (monthOfYear < 9) {
                                monthYear = "0";
                            } else {
                                monthYear = "";
                            }
                            if (dayOfMonth < 9) {
                                DayOfMonth = "0";
                            } else {
                                DayOfMonth = "";
                            }

                            to_date.setText(year + "-" + monthYear + (monthOfYear + 1) + "-" + DayOfMonth + dayOfMonth);
                            //  ppr_rcv_date_value = String.format("%02d/%02d/%02d", dayOfMonth,monthOfYear + 1,year);


                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();

        });
        from_date.setOnClickListener(v -> {
            int mYear, mMonth, mDay;
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog datePickerDialog = new DatePickerDialog(OrderSupply.this,
                    new DatePickerDialog.OnDateSetListener() {

                        @Override
                        public void onDateSet(DatePicker view, int year,
                                              int monthOfYear, int dayOfMonth) {


                            String monthYear = "", DayOfMonth = "";
                            if (monthOfYear < 9) {
                                monthYear = "0";
                            } else {
                                monthYear = "";
                            }
                            if (dayOfMonth < 9) {
                                DayOfMonth = "0";
                            } else {
                                DayOfMonth = "";
                            }

                            from_date.setText(year + "-" + monthYear + (monthOfYear + 1) + "-" + DayOfMonth + dayOfMonth);
                            //  ppr_rcv_date_value = String.format("%02d/%02d/%02d", dayOfMonth,monthOfYear + 1,year);


                        }
                    }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();

        });
        tvReason.setOnClickListener(v -> {
            dropSelectType = "edition";
            Util.showDropDown(dpReasonViewSetList, "Select Reason", OrderSupply.this, this::onOptionClick);

        });


        try {


            JSONObject strData = data.getBaseCopy().getJSONObject(0);
            lower_limit = Integer.parseInt(strData.getJSONObject("fields").getString("lower_limit"));
            uper_limit = Integer.parseInt(strData.getJSONObject("fields").getString("uper_limit"));
            base_copy = Integer.parseInt(strData.getJSONObject("fields").getString("base_copy"));
            tvBaseCopy.setText(strData.getJSONObject("fields").getString("base_copy"));


            tvCurrentCopy.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {

                    if (s.length() > 0) {
                        int currentCopy = Integer.parseInt(tvCurrentCopy.getText().toString());

                        if (currentCopy < lower_limit || currentCopy > uper_limit) {
                            reasonDrop.setVisibility(View.VISIBLE);
                            lowerUpper = true;
                        } else {
                            lowerUpper = false;
                            reasonDrop.setVisibility(View.GONE);
                        }
                        int baseCopy = Integer.parseInt(tvBaseCopy.getText().toString());
                        int difference = baseCopy - currentCopy;
                        tvDifferentCopy.setText(String.valueOf(difference));

                    }

                }
            });


        } catch (JSONException e) {
            e.printStackTrace();
        }


        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                popupWindow.dismiss();


            }
        });


        if (cirListData.get(pos).getRemark().length() > 0) {
            remark_cr.setText(cirListData.get(pos).getRemark());
        }
//        if(cirListData.get(pos).getProposedQty().toString().length()>0){
//            tvCurrentCopy.setText(cirListData.get(pos).getProposedQty().toString());
//        }


        if (cirListData.get(pos).getReasonQtyChange().length() > 0) {
            if (cirListData.get(pos).getReasonQtyChange().equalsIgnoreCase("Hold")) {
                reasonStatus = true;
                dateLayout.setVisibility(View.VISIBLE);

                from_date.setText(cirListData.get(pos).getHoldDateFrom().toString().replace("T00:00:00", ""));
                to_date.setText(cirListData.get(pos).getHoldDateTo().toString().replace("T00:00:00", ""));
                cirListData.get(pos).setHoldDateTo(cirListData.get(pos).getHoldDateTo());

            } else {
                reasonStatus = false;
                dateLayout.setVisibility(View.GONE);
            }
            tvReason.setText(cirListData.get(pos).getReasonQtyChange());
        }


        if (CIRLength > 0) {
            saveBtn.setText("Update");
        } else {
            saveBtn.setText("Submit");
        }

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String changeCopy = "", fromDate = "", toDate = "";
                if (tvCurrentCopy.getText().length() == 0) {
                    alertMessage("Please enter current copy");
                    return;
                }
                if (remark_cr.getText().length() == 0) {
                    alertMessage("Please enter remark");
                    return;
                }

                if (reasonStatus) {

                    if (from_date.getText().length() == 0) {
                        alertMessage("Please select the Form Date");
                        return;
                    }

                    if (to_date.getText().length() == 0) {
                        alertMessage("Please Select The To Date");
                        return;
                    }
                    fromDate = from_date.getText().toString() + "T00:00:00";
                    toDate = to_date.getText().toString() + "T00:00:00";

                } else {
                    fromDate = "9999-12-31T00:00:00";
                    toDate = "9999-12-31T00:00:00";
                }

//if(lowerUpper){
//
//    alertMessage();
//
//}else{
//
//}


                if (tvCurrentCopy.getText().length() == 0) {
                    alertMessage("Please enter current copy");
                    return;
                }
                if (lowerUpper) {
                    if (tvReason.getText().length() == 0) {
                        alertMessage("Please Select Reason");
                        return;
                    }
                }

                //   postOrderData(data, tvCurrentCopy.getText().toString(), fromDate, toDate, remark_cr.getText().toString());

                if (CIRLength > 0) {
                    cirListData.get(pos).setUperLimit(uper_limit);
                    cirListData.get(pos).setLowerLimit(lower_limit);
                    cirListData.get(pos).setBaseCopy(base_copy);
                    cirListData.get(pos).setHoldDateFrom(fromDate);
                    cirListData.get(pos).setRemark(remark_cr.getText().toString());
                    cirListData.get(pos).setHoldDateTo(toDate);
                    cirListData.get(pos).setReasonQtyChange(tvReason.getText().toString());
                    cirListData.get(pos).setProposedQty(Integer.valueOf(tvCurrentCopy.getText().toString()));
                    Log.e("data", cirListData.toString());
                    popupWindow.dismiss();

                    orderSupplyAdapte = new OrderSupplyAdapter(context, orderSupplyModels, orderSupply, cirListData, "Uoh");
                    mBinding.recycleList.setAdapter(orderSupplyAdapte);
                    orderSupplyAdapte.notifyDataSetChanged();

                    CIR_Order_Update cirListDataUpdate = cirListData.get(pos);
                    //cirListDataUpdate.add(cirListData.get(pos));
                    postOrderUpdateData(cirListDataUpdate, cirListData.get(pos).getCIR_Order_Update_id());

                } else {
                    cirListData.get(pos).setUperLimit(uper_limit);
                    cirListData.get(pos).setLowerLimit(lower_limit);
                    cirListData.get(pos).setBaseCopy(base_copy);
                    cirListData.get(pos).setHoldDateFrom(fromDate);
                    cirListData.get(pos).setRemark(remark_cr.getText().toString());
                    cirListData.get(pos).setHoldDateTo(toDate);
                    cirListData.get(pos).setApproval_status(4);
                    cirListData.get(pos).setReasonQtyChange(tvReason.getText().toString());
                    cirListData.get(pos).setProposedQty(Integer.valueOf(tvCurrentCopy.getText().toString()));
                    Log.e("data", cirListData.toString());
                    popupWindow.dismiss();

                    orderSupplyAdapte = new OrderSupplyAdapter(context, orderSupplyModels, orderSupply, cirListData, "Ex");
                    mBinding.recycleList.setAdapter(orderSupplyAdapte);
                    orderSupplyAdapte.notifyDataSetChanged();
                }


            }

        });


    }

    public void postOrderUpdateData(CIR_Order_Update cirListData, String id) {


        Gson gson = new Gson();
        String dataCIRobj = gson.toJson(cirListData);
        Log.e("agencyRequest", dataCIRobj);


        Call<String> call = SamriddhiApplication.getmInstance().getApiService().UpdateAddCIR_Order_Update(id, cirListData);


        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;


                    Log.e("dataShow", response.toString());


                    try {
                        //  JSONObject jsonObject = new JSONObject(response.body());
                        // Toast.makeText(UohOrderSupply.this, jsonObject.toString(), Toast.LENGTH_SHORT).show();

                        // String message = jsonObject.getString("message");


                        Toast.makeText(OrderSupply.this, "Your Data is Save", Toast.LENGTH_SHORT).show();

                        // getCurrentDta();


                        //  enableLoadingBar(false);
                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(OrderSupply.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(OrderSupply.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(OrderSupply.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });


    }

    public void postOrderData(ArrayList<CIR_Order_Update> cirListData) {


        Gson gson = new Gson();
        String dataCIRobj = gson.toJson(cirListData);
        Log.e("agencyRequest", dataCIRobj);


        Call<String> call = SamriddhiApplication.getmInstance().getApiService().addCIR_Order_Update(cirListData);


        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;


                    Log.e("dataShow", response.toString());

                    Toast.makeText(OrderSupply.this, "Your Data is Save", Toast.LENGTH_SHORT).show();
                    getCurrentDta();


                    //  enableLoadingBar(false);


                } else {
                    try {
                        Toast.makeText(OrderSupply.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(OrderSupply.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(OrderSupply.this, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });


    }


    public void getReason() {
        dpReasonViewSetList.clear();
        // Calling JSON
        //enableLoadingBar(true);


        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getOrderChangeReasonViewSet();
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                enableLoadingBar(false);
                if (response.isSuccessful()) {

                    Log.e("responseData", response.body());

                    assert response.body() != null;


                    try {


                        JSONArray reasonArray = new JSONArray(response.body());

                        for (int i = 0; i < reasonArray.length(); i++) {
                            JSONObject jsonObject = reasonArray.getJSONObject(i);
                            DropDownModel obj = new DropDownModel();
                            obj.setId(jsonObject.getString("reason_id"));
                            obj.setDescription(jsonObject.getString("reason"));
                            dpReasonViewSetList.add(obj);

                        }


                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(OrderSupply.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(OrderSupply.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {

                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });


    }

    @Override
    public void onEdit(int CIRLength, OrderSupplyModel data, ArrayList<OrderSupplyModel> orderSupplyModels, int pos) {

        // Toast.makeText(this,data.getProposedQty().toString(),Toast.LENGTH_LONG).show();
        exPopupWindow1(CIRLength, data, orderSupplyModels, pos);

    }

    @Override
    public void onOptionClick(DropDownModel listData) {


        try {
            if (listData.getDescription().equalsIgnoreCase("Hold")) {
                reasonStatus = true;
                dateLayout.setVisibility(View.VISIBLE);
            } else {
                reasonStatus = false;
                dateLayout.setVisibility(View.GONE);
            }
            tvReason.setText(listData.getDescription());


            if (dropSelectType.equalsIgnoreCase("unit")) {
                saleOrgVal = listData.getId();
                saleOrg.setText(listData.getDescription());

            } else if (dropSelectType.equalsIgnoreCase("edition")) {
                editionVal = listData.getId();
                tvEdition.setText(listData.getDescription());
            }
        } catch (Exception e) {

        }
        Util.hideDropDown();
    }


    private void getEdition() {
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getEdition(PreferenceManager.getAgentId(context));

        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            EditionMasterArray.clear();
                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("edition_name"));
                                EditionMasterArray.add(dropDownModel);

                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }

    public void getUnit() {
        unitMaster.clear();
        // Calling JSON10011137MP
        enableLoadingBar(true);
        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getUnit(PreferenceManager.getAgentId(context), "MP");
        // Enqueue Callback will be call when get response...
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    assert response.body() != null;
                    Log.e("VECHILELISTdata", response.body());

                    try {
                        JSONObject jsonObject = new JSONObject(response.body());

                        JSONArray objData = jsonObject.getJSONArray("data");
                        String status = jsonObject.getString("status");
                        if (status.equalsIgnoreCase("Success")) {

                            for (int i = 0; i < objData.length(); i++) {


                                JSONObject objStr = objData.getJSONObject(i);

                                DropDownModel dropDownModel = new DropDownModel();

                                dropDownModel.setId(objStr.getString("pk"));
                                dropDownModel.setDescription(objStr.getJSONObject("fields").getString("unit_name"));

                                dropDownModel.setName(objStr.getJSONObject("fields").getString("jj_flag"));
                                dropDownModel.setCode(objStr.getJSONObject("fields").getString("unit_code"));

                                unitMaster.add(dropDownModel);

                            }


                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    enableLoadingBar(false);
                } else {
                    try {
                        Toast.makeText(OrderSupply.this, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(OrderSupply.this, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {

                Toast.makeText(OrderSupply.this, R.string.err_ifsc_code_wrong, Toast.LENGTH_SHORT).show();
            }

        });
    }
}
