package com.app.samriddhi.ui.presenter;

import com.app.samriddhi.R;
import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.response.JsonArrayResponse;
import com.app.samriddhi.base.BasePresenter;
import com.app.samriddhi.ui.model.HintModel;
import com.app.samriddhi.ui.view.IAppHintView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppHintsPresenter extends BasePresenter<IAppHintView> {


    /**
     *  get the apps hints
     * @param language app language code
     */
    public void getAppHints(String language) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getAppHints(language).enqueue(new Callback<JsonArrayResponse<HintModel>>() {
            @Override
            public void onResponse(Call<JsonArrayResponse<HintModel>> call, Response<JsonArrayResponse<HintModel>> response) {
                getView().enableLoadingBar(false);
                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().list != null)
                            getView().onSuccess(response.body().list);
                        else
                            getView().onError(getView().getContext().getResources().getString(R.string.str_no_result_found));

                    }
                }
            }

            @Override
            public void onFailure(Call<JsonArrayResponse<HintModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

}
