package com.app.samriddhi.ui.activity.main.ui.MisEntry;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.Adapter.PoAdapter;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.DailyPoActivity;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.POModal.DailyPOModal;
import com.google.android.material.card.MaterialCardView;

import java.util.ArrayList;
import java.util.List;

public class MisAgencyAdapter extends RecyclerView.Adapter<MisAgencyAdapter.ViewHolder> implements Filterable {

    Context context;
    List<MisAgencyModals> dataAdapters;
    private final List<MisAgencyModals> searchArray;
    int row_index = -1;
    public MisAgencyAdapter(List<MisAgencyModals> getDataAdapter, Context context) {

        super();
        this.dataAdapters = getDataAdapter;
        this.searchArray = new ArrayList<>();
        this.searchArray.addAll(dataAdapters);
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.mis_agency_adapter, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, @SuppressLint("RecyclerView") int position) {

        MisAgencyModals dataAdapterOBJ = dataAdapters.get(position);
        Viewholder.setIsRecyclable(false);

        Viewholder.txtAgencyCode.setText(dataAdapterOBJ.getAgency_code());
        Viewholder.txtName.setText(dataAdapterOBJ.getAgency_name());
        Viewholder.txtEdition.setText(dataAdapterOBJ.getEdition());
        //Viewholder.txtEdition.setText(dataAdapterOBJ.getCenter());
        Viewholder.txtNPS.setText(dataAdapterOBJ.getNps());
        Viewholder.txtPO.setText(dataAdapterOBJ.getPo());
        Viewholder.editUnsold.setText(dataAdapterOBJ.getUnsold());
        Viewholder.editFreshUnsold.setText(dataAdapterOBJ.getFresh_unsold());
        Viewholder.editOldUnsold.setText(dataAdapterOBJ.getOld_unsold());


        if(dataAdapterOBJ.getFreshOldStatus().equals("1")){

            Viewholder.viewFresh.setVisibility(View.VISIBLE);
            Viewholder.editOldUnsold.setVisibility(View.VISIBLE);
            Viewholder.editFreshUnsold.setVisibility(View.VISIBLE);  /*Show only in bihar and jharkhand*/

        }
        else {

            Viewholder.viewFresh.setVisibility(View.GONE);
            Viewholder.editOldUnsold.setVisibility(View.GONE);
            Viewholder.editFreshUnsold.setVisibility(View.GONE);
        }

        if (dataAdapterOBJ.getPstyv().equals("ZFOR")) {

            Viewholder.editUnsold.setBackground(context.getResources().getDrawable(R.drawable.non_editable_input));
            Viewholder.editUnsold.setEnabled(false);

            Viewholder.editOldUnsold.setBackground(context.getResources().getDrawable(R.drawable.non_editable_input));
            Viewholder.editOldUnsold.setEnabled(false);

            Viewholder.editFreshUnsold.setBackground(context.getResources().getDrawable(R.drawable.non_editable_input));
            Viewholder.editFreshUnsold.setEnabled(false);
        }
        else {


           if(dataAdapterOBJ.getFreshOldStatus().equals("1")){

               Viewholder.editOldUnsold.setBackground(context.getResources().getDrawable(R.drawable.border));
               Viewholder.editOldUnsold.setEnabled(true);

               Viewholder.editFreshUnsold.setBackground(context.getResources().getDrawable(R.drawable.border));
               Viewholder.editFreshUnsold.setEnabled(true);

               Viewholder.editUnsold.setBackground(context.getResources().getDrawable(R.drawable.non_editable_input));
               Viewholder.editUnsold.setEnabled(false);
           }

           else {
               Viewholder.editFreshUnsold.setBackground(context.getResources().getDrawable(R.drawable.non_editable_input));
               Viewholder.editFreshUnsold.setEnabled(false);

               Viewholder.editOldUnsold.setBackground(context.getResources().getDrawable(R.drawable.non_editable_input));
               Viewholder.editOldUnsold.setEnabled(false);

               Viewholder.editUnsold.setEnabled(true);
               Viewholder.editUnsold.setBackground(context.getResources().getDrawable(R.drawable.border));

           }

        }

        Viewholder.editUnsold.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override

            public boolean onEditorAction(TextView vvvvv, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //do here your stuff f

                    if (vvvvv != null) {
                        InputMethodManager imm =
                                (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(vvvvv.getWindowToken(), 0);
                    }

                    String strData = Viewholder.editUnsold.getText().toString();

                    if(strData.equals("")){
                        strData="0";
                        Viewholder.editUnsold.setText(strData);
                    }
                    int total = Integer.parseInt(strData);

                    dataAdapters.get(position).setUnsold(total + "");

                    int totalPO=Integer.parseInt(dataAdapters.get(position).getPo());
                    int totaNPS=totalPO-total;
                    dataAdapters.get(position).setNps(totaNPS + "");
                    Viewholder.txtNPS.setText(totaNPS+"");
                    dataAdapters.get(position).setEditStatus("1");

                    if (context instanceof MisEntryActivity) {
                        ((MisEntryActivity)context).UpdateNpsUnsold();
                    }
                    return true;
                }
                return false;
            }
        });



        Viewholder.editFreshUnsold.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override

            public boolean onEditorAction(TextView vvvvv, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //do here your stuff f

                    if (vvvvv != null) {
                        InputMethodManager imm =
                                (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(vvvvv.getWindowToken(), 0);
                    }

                    String strData = Viewholder.editFreshUnsold.getText().toString();
                    if(strData.equals("")){
                        strData="0";
                        Viewholder.editFreshUnsold.setText(strData);
                    }
                    int freshUnsold = Integer.parseInt(strData);

                    int OldUnsold = Integer.parseInt(dataAdapters.get(position).getOld_unsold());

                    int TotalUnsold=freshUnsold+OldUnsold;


                    dataAdapters.get(position).setFresh_unsold(freshUnsold + "");
                    dataAdapters.get(position).setUnsold(TotalUnsold + "");
                    Viewholder.editUnsold.setText(TotalUnsold+"");

                    int totalPO=Integer.parseInt(dataAdapters.get(position).getPo());
                    int totaNPS=totalPO-TotalUnsold;
                    dataAdapters.get(position).setNps(totaNPS + "");
                    Viewholder.txtNPS.setText(totaNPS+"");
                    dataAdapters.get(position).setEditStatus("1");

                    if (context instanceof MisEntryActivity) {
                        ((MisEntryActivity)context).UpdateNpsUnsold();
                    }
                    return true;
                }
                return false;
            }
        });


        Viewholder.editOldUnsold.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override

            public boolean onEditorAction(TextView vvvvv, int actionId, KeyEvent event) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //do here your stuff f

                    if (vvvvv != null) {
                        InputMethodManager imm =
                                (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(vvvvv.getWindowToken(), 0);
                    }

                    String strData = Viewholder.editOldUnsold.getText().toString();
                    if(strData.equals("")){
                         strData="0";
                        Viewholder.editOldUnsold.setText(strData);
                    }

                    int OldUnsold = Integer.parseInt(strData);
                    int freshUnsold = Integer.parseInt(dataAdapters.get(position).getFresh_unsold());
                    int TotalUnsold=OldUnsold+freshUnsold;

                    dataAdapters.get(position).setOld_unsold(OldUnsold + "");
                    Log.e("fghfghfghf",OldUnsold+"");
                    dataAdapters.get(position).setUnsold(TotalUnsold + "");
                    Viewholder.editUnsold.setText(TotalUnsold+"");

                    int totalPO=Integer.parseInt(dataAdapters.get(position).getPo());
                    int totaNPS=totalPO-TotalUnsold;
                    dataAdapters.get(position).setNps(totaNPS + "");
                    Viewholder.txtNPS.setText(totaNPS+"");
                    dataAdapters.get(position).setEditStatus("1");

                    if (context instanceof MisEntryActivity) {
                        ((MisEntryActivity)context).UpdateNpsUnsold();
                    }
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public ArrayList<MisAgencyModals> getArrayData() {
        return (ArrayList<MisAgencyModals>) dataAdapters;
    }


    class ViewHolder extends RecyclerView.ViewHolder {



        TextView txtAgencyCode,txtName,txtEdition,txtPO,txtNPS;
        EditText editUnsold,editFreshUnsold,editOldUnsold;
        View viewFresh;

        public ViewHolder(View itemView) {
            super(itemView);
            viewFresh = itemView.findViewById(R.id.viewFresh);
            txtName = itemView.findViewById(R.id.txtName);
            txtAgencyCode = itemView.findViewById(R.id.txtAgencyCode);
            txtEdition = itemView.findViewById(R.id.txtEdition);
            txtPO = itemView.findViewById(R.id.txtPO);
            txtNPS = itemView.findViewById(R.id.txtNPS);
            editUnsold = itemView.findViewById(R.id.editUnsold);
            editFreshUnsold = itemView.findViewById(R.id.editFreshUnsold);
            editOldUnsold = itemView.findViewById(R.id.editOldUnsold);

        }
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String filterString = charSequence.toString().toLowerCase();
                //arraylist1.clear();
                FilterResults results = new FilterResults();
                int count = searchArray.size();
                final List<MisAgencyModals> list = new ArrayList<>();

                MisAgencyModals filterableString;

                for (int i = 0; i < count; i++) {
                    filterableString = searchArray.get(i);

                    if (searchArray.get(i).getEdition().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                    } else if (searchArray.get(i).getCenter().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                    } else if (searchArray.get(i).getAgency_code().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                    } else if (searchArray.get(i).getAgency_name().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                    }
                    else if (searchArray.get(i).getMainJJ().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                    }
                    else if (searchArray.get(i).getExecutive_name().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                    }
                    else if (searchArray.get(i).getExecutive_Code().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                    }

                    else if (searchArray.get(i).getUnsold().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                    }

                    else if (searchArray.get(i).getPo().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                    }
                    else {

                    }
                }

                results.values = list;
                results.count = list.size();
                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                dataAdapters = (List<MisAgencyModals>) filterResults.values;
                notifyDataSetChanged();

            }
        };

    }
}