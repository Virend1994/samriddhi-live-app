package com.app.samriddhi.ui.view;

import com.app.samriddhi.ui.model.BankListModel;

import java.util.List;

public interface IBankListView extends IView {
    void onSuccess(List<BankListModel> bankData);
}
