package com.app.samriddhi.ui.presenter;

import android.util.Log;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.response.BaseResponse;
import com.app.samriddhi.base.BasePresenter;
import com.app.samriddhi.ui.model.LanguageModel;
import com.app.samriddhi.ui.view.IView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LanguagePresenter extends BasePresenter<IView> {

    public void submitLanguageChange(LanguageModel mData){
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().languageChange(mData).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                getView().enableLoadingBar(false);
                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body() != null)
                            getView().onInfo(response.body().replyMsg );
                        else
                            getView().onInfo(response.body().replyMsg);
                    }
                }

                else {

                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }
}
