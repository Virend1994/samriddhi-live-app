package com.app.samriddhi.ui.activity.main.ui.Ledger;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;
import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AppsContants;
import com.google.android.material.card.MaterialCardView;

import java.util.List;

public class CashCreditAdapter extends RecyclerView.Adapter<CashCreditAdapter.ViewHolder> {
    Context context;
    List<LedgerModal> dataAdapters;

    public CashCreditAdapter(List<LedgerModal> getDataAdapter, Context context) {
        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cash_credit_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, @SuppressLint("RecyclerView") int position) {

        LedgerModal dataAdapterOBJ = dataAdapters.get(position);
        Viewholder.setIsRecyclable(false);
        Viewholder.txtCityName.setText(dataAdapterOBJ.getCityupc());
        Viewholder.txtSerial.setText(dataAdapterOBJ.getSerial_num());
        Viewholder.txtAmount.setText(dataAdapterOBJ.getCity_amt());
        Viewholder.cardViewMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (context instanceof CashCreditActivity) {

                    AppsContants.sharedpreferences=context.getSharedPreferences(AppsContants.MyPREFERENCES,Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor=AppsContants.sharedpreferences.edit();
                    editor.putString(AppsContants.cityupc,dataAdapterOBJ.getCityupc());
                    editor.putString(AppsContants.MarketCode,dataAdapterOBJ.getMarket_code());
                    editor.putString(AppsContants.CityCode,dataAdapterOBJ.getCity_name());
                    editor.commit();
                    ((CashCreditActivity) context).startActivityAnimation(context, ShowAgentListActivity.class, false);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataAdapters.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtSerial, txtCityName, txtAmount;
        MaterialCardView cardViewMain;

        public ViewHolder(View itemView) {
            super(itemView);
            txtSerial = itemView.findViewById(R.id.txtSerial);
            txtCityName = itemView.findViewById(R.id.txtStateName);
            txtAmount = itemView.findViewById(R.id.txtAmount);
            cardViewMain = itemView.findViewById(R.id.cardViewMain);
        }
    }
}