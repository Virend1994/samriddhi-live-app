package com.app.samriddhi.ui.activity.main.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.RecyclerViewArrayAdapter;
import com.app.samriddhi.databinding.ActivityBankListBinding;
import com.app.samriddhi.ui.model.BankListModel;
import com.app.samriddhi.ui.presenter.BankListPresenter;
import com.app.samriddhi.ui.view.IBankListView;
import com.app.samriddhi.util.AlertUtility;

import java.util.ArrayList;
import java.util.List;

public class BankListActivity extends BaseActivity implements IBankListView, RecyclerViewArrayAdapter.OnItemClickListener<BankListModel>, TextWatcher {
    ActivityBankListBinding mBinding;
    BankListPresenter mPresenter;
    List<BankListModel> arrBankData = new ArrayList<>();
    String bankUrl = "";
    boolean isSearchOption = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_bank_list);
        initViews();

    }

    /**
     * initialize views
     */

    private void initViews() {
        mBinding.toolbar.txtTittle.setText(getResources().getString(R.string.str_bank_tittle));
        mBinding.toolbar.rlNotification.setVisibility(View.GONE);
        mBinding.toolbar.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mPresenter = new BankListPresenter();
        mPresenter.setView(this);
        mPresenter.getBankList();
        mBinding.editSearch.addTextChangedListener(this);
        mBinding.recycleBanks.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    public void onSuccess(List<BankListModel> bankData) {
        if (bankData != null && bankData.size() > 0) {
            for (int i = 0; i < bankData.size(); i++)
                arrBankData.add(bankData.get(i));
            mBinding.recycleBanks.setAdapter(new RecyclerViewArrayAdapter(arrBankData, this));
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    public void onItemClick(View view, BankListModel object) {
        for (BankListModel data : arrBankData) {
            if (object.getBankName().equalsIgnoreCase(data.getBankName())) {
                data.setSelected(true);
                bankUrl = object.getBankURL();
            } else
                data.setSelected(false);
        }

    }

    /**
     * Handles the buttons click event
     * @param view item view
     */
    public void onclick(View view) {
        switch (view.getId()) {
            case R.id.btn_pay_later:
                mBinding.toolbar.imgBack.performClick();
                break;
            case R.id.btn_pay_now:
                if (!bankUrl.isEmpty()) {
                    if (!bankUrl.startsWith("https://") && !bankUrl.startsWith("http://")) {
                        bankUrl = "http://" + bankUrl;
                    }
                    Intent openUrlIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(bankUrl));
                    if (openUrlIntent.resolveActivity(getPackageManager()) != null) {
                        startActivity(openUrlIntent);
                    }
                } else
                    AlertUtility.showAlertDialog(this, getResources().getString(R.string.str_select_bank));
                break;
            case R.id.img_close:
                if (!isSearchOption) {
                    mBinding.editSearch.setText("");
                    hideKeyBoard();
                    filter("");
                }
                break;
            default:
                break;
        }
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
          // Do something
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        // Do something
    }

    @Override
    public void afterTextChanged(Editable s) {
        if (s.length() == 0) {
            mBinding.imgClose.setImageResource(R.drawable.ic_search);
            isSearchOption = true;
        } else {
            mBinding.imgClose.setImageResource(R.drawable.close);
            isSearchOption = false;
        }
        filter(s.toString());
    }

    /**
     * search the filter text from list of items
     * @param text filtered text
     */
    void filter(String text) {
        List<BankListModel> temp = new ArrayList();
        for (BankListModel d : arrBankData) {
            if (d.getBankName().toLowerCase().contains(text.toLowerCase())) {
                temp.add(d);
            }
        }
        //update recyclerview
        (((RecyclerViewArrayAdapter) mBinding.recycleBanks.getAdapter())).updateList(temp);
    }
}
