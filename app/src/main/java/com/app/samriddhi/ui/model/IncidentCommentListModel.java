package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IncidentCommentListModel extends BaseModel {
    @SerializedName("Incident_Number")
    @Expose
    private Integer incidentNumber;
    @SerializedName("IncidentComment_Text")
    @Expose
    private String incidentCommentText;
    @SerializedName("Incident_Status")
    @Expose
    private String incidentStatus;
    @SerializedName("IncidentComment_By")
    @Expose
    private String incidentCommentBy;
    @SerializedName("IncidentComment_On")
    @Expose
    private String incidentCommentOn;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("create_date")
    @Expose
    private String createDate;
    @SerializedName("update_date")
    @Expose
    private String updateDate;
    @SerializedName("create_user")
    @Expose
    private Object createUser;
    @SerializedName("update_user")
    @Expose
    private Object updateUser;


    @SerializedName("IncidentComment_By_Name")
    @Expose
    private String incidentCommentByName;
    @SerializedName("IncidentComment_By_Level")
    @Expose
    private String incidentCommentByLevel;

    public String getIncidentCommentByName() {
        return incidentCommentByName;
    }

    public void setIncidentCommentByName(String incidentCommentByName) {
        this.incidentCommentByName = incidentCommentByName;
    }

    public String getIncidentCommentByLevel() {
        return incidentCommentByLevel;
    }

    public void setIncidentCommentByLevel(String incidentCommentByLevel) {
        this.incidentCommentByLevel = incidentCommentByLevel;
    }

    public Integer getIncidentNumber() {
        return incidentNumber;
    }

    public void setIncidentNumber(Integer incidentNumber) {
        this.incidentNumber = incidentNumber;
    }

    public String getIncidentCommentText() {
        return incidentCommentText;
    }

    public void setIncidentCommentText(String incidentCommentText) {
        this.incidentCommentText = incidentCommentText;
    }

    public String getIncidentStatus() {
        return incidentStatus;
    }

    public void setIncidentStatus(String incidentStatus) {
        this.incidentStatus = incidentStatus;
    }

    public String getIncidentCommentBy() {
        return incidentCommentBy;
    }

    public void setIncidentCommentBy(String incidentCommentBy) {
        this.incidentCommentBy = incidentCommentBy;
    }

    public String getIncidentCommentOn() {
        return incidentCommentOn;
    }

    public void setIncidentCommentOn(String incidentCommentOn) {
        this.incidentCommentOn = incidentCommentOn;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public Object getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Object createUser) {
        this.createUser = createUser;
    }

    public Object getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Object updateUser) {
        this.updateUser = updateUser;
    }
}
