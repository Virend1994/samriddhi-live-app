package com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.app.samriddhi.R;
import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.base.adapter.Approval_Agency_Adapter;
import com.app.samriddhi.databinding.AgencyApprovalSohViewLayoutBinding;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AppsContants;
import com.app.samriddhi.ui.model.NewAgencyModel;
import com.app.samriddhi.ui.model.ProposedAgencyModel;
import com.app.samriddhi.util.Globals;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SOHApproval extends BaseActivity implements Approval_Agency_Adapter.OnclickListener {
    NewAgencyModel dataView;

    AgencyApprovalSohViewLayoutBinding mBinding;
    AlertDialog alertDialog;
    Context context;
    SOHApproval contextListener;
    ProposedAgencyModel data;
    Approval_Agency_Adapter adapter;
    boolean remarkUohStatus = true;
    int count = 0;
    private ArrayList<ProposedAgencyModel> srList;
    private ArrayList<ProposedAgencyModel> newSrList;
    Intent i;
    Globals g = Globals.getInstance(context);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setContentView(R.layout.agency_approval_soh_view_layout);
        data = new ProposedAgencyModel();
        mBinding = DataBindingUtil.setContentView(this, R.layout.agency_approval_soh_view_layout);
        mBinding.toolbar.setTitle("SOH- Approval Matrix ");
        srList = new ArrayList<>();
        setSupportActionBar(mBinding.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);



        initView();

        getAgentData();

    }


    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    public void initView() {
        i = getIntent();
        context = this;
        contextListener = this;
        dataView = (NewAgencyModel) i.getSerializableExtra("MyData");
        mBinding.tvUnit.setText(dataView.getUnit());

        Log.e("hgdfjgdjfgs",dataView.getId());/*635*/


        if (dataView.getUOHApproval().equalsIgnoreCase("1")) {
            mBinding.uohStatus.setText("Approved");
            mBinding.uohStatus.setTextColor(Color.parseColor("#00c851"));
        } else {
            mBinding.uohStatus.setText("Not Approved");
        }
        if (dataView.getHOApproval().equalsIgnoreCase("1")) {
            mBinding.tvHo.setTextColor(Color.parseColor("#00c851"));
            mBinding.tvHo.setText("Approved");
        } else {
            mBinding.tvHo.setText("Not Approved");
        }

        if (dataView.getSOHApproval().equalsIgnoreCase("1")) {
            mBinding.tvSoh.setTextColor(Color.parseColor("#00c851"));
            mBinding.tvSoh.setText("Approved");
            mBinding.tvSoh.setTextColor(Color.parseColor("#00c851"));
        } else {
            mBinding.tvSoh.setText("Not Approved");
        }

        if (dataView.getUHFApproval().equalsIgnoreCase("1")) {
            mBinding.tvUfh.setTextColor(Color.parseColor("#00c851"));
            mBinding.tvUfh.setText("Approved");
        } else {
            mBinding.tvUfh.setText("Not Approved");
        }

        if (dataView.getCBRApproval().equalsIgnoreCase("1")) {
            mBinding.tvCbr.setTextColor(Color.parseColor("#00c851"));
            mBinding.tvCbr.setText("Approved");
        } else {
            mBinding.tvCbr.setText("Not Approved");
        }

        if (dataView.getBPClosure().equalsIgnoreCase("1")) {
            mBinding.tvBo.setText("Approved");
        } else {
            mBinding.tvBo.setText("Not Approved");
        }

        if(dataView.getSOHApproval().equals("1")){

            mBinding.btnlayout.setVisibility(View.GONE);
        }
        else {
            mBinding.btnlayout.setVisibility(View.VISIBLE);

        }
        mBinding.tvPopulation.setText(dataView.getPopulation());
        mBinding.tvTotalCopies.setText(dataView.getNoMainCopy());
        mBinding.tvReason.setText(dataView.getReason());
        mBinding.tvState.setText(dataView.getState());
        mBinding.tvUnit.setText(dataView.getUnit());
        mBinding.unitName.setText(dataView.getUnit());
        mBinding.tvCity.setText(dataView.getDistrict());
        mBinding.tvUpcCity.setText(dataView.getCity_upc());

       // mBinding.tvlocationName.setText(dataView.getLocation());
        mBinding.tvlocationName.setText(dataView.getTown());

        mBinding.agencyDetailsRec.setHasFixedSize(true);
        mBinding.agencyDetailsRec.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        mBinding.btnReject.setOnClickListener(v -> {

            if (mBinding.remarkCr.getText().toString().length() == 0) {
                alertMessage("Please enter the remark");
                return;
            }

            newSrList.size();


            if(newSrList.size()==0){
                alertMessage("Please Select Agency");
                return;

            }
            enableLoadingBar(true);

            count = 0;
            for (int i = 0; i < newSrList.size(); i++) {

                if (newSrList.get(i).isStatusCheck()) {
                    ProposedAgencyModel objData = new ProposedAgencyModel();

                    objData.setAg_detail_id(newSrList.get(i).getId());
                    objData.setAg_req_id(newSrList.get(i).getRequestId());

                    objData.setAgency_name(newSrList.get(i).getAgency_name());

                    objData.setAgenct_name(newSrList.get(i).getAgenct_name());
                    objData.setSOHApproval("2");

                    objData.setSOHRemark(mBinding.remarkCr.getText().toString());
                   // objData.setSOHUpdateBy(PreferenceManager.getAgentId(context));
                    objData.setSOHUpdateBy(PreferenceManager.getPrefAgentName(context));

                    objData.setAgencyRequestCIRReq_count(newSrList.get(i).getAgencyRequestCIRReq_count());

                    objData.setAgencyRequestKYBP_count(newSrList.get(i).getAgencyRequestKYBP_count());

                    objData.setAgencyRequestSurvey_count(newSrList.get(i).getAgencyRequestSurvey_count());

                    objData.setAgencyRequestCIRReqJJ_count(newSrList.get(i).getAgencyRequestCIRReqJJ_count());
                    addRemarkData(objData, newSrList.get(i).getId());
                    count++;

                    Log.e("isStatusCheck" + i, String.valueOf(newSrList.get(i).isStatusCheck()));
                } else {
                    Log.e("isStatusCheck" + i, String.valueOf(newSrList.get(i).isStatusCheck()));
                    ProposedAgencyModel objData = new ProposedAgencyModel();

                    objData.setAg_detail_id(newSrList.get(i).getId());
                    objData.setAg_req_id(newSrList.get(i).getRequestId());

                    objData.setAgency_name(newSrList.get(i).getAgency_name());

                    objData.setAgenct_name(newSrList.get(i).getAgenct_name());
                    objData.setSOHApproval("0");

                    objData.setSOHRemark(mBinding.remarkCr.getText().toString());

                   // objData.setSOHUpdateBy(PreferenceManager.getAgentId(context));

                    objData.setSOHUpdateBy(PreferenceManager.getPrefAgentName(context));
                    objData.setAgencyRequestCIRReq_count(newSrList.get(i).getAgencyRequestCIRReq_count());

                    objData.setAgencyRequestKYBP_count(newSrList.get(i).getAgencyRequestKYBP_count());
                    objData.setAgencyRequestSurvey_count(newSrList.get(i).getAgencyRequestSurvey_count());
                    objData.setAgencyRequestCIRReqJJ_count(newSrList.get(i).getAgencyRequestCIRReqJJ_count());
                    addRemarkData(objData, newSrList.get(i).getId());
                    count++;
                }

            }
            if (count == newSrList.size()) {
                enableLoadingBar(false);
                finish();
            }

        });


        mBinding.btnApprove.setOnClickListener(v -> {

            if (mBinding.remarkCr.getText().toString().length() == 0) {
                alertMessage("Please enter the remark");
                return;
            }

            try {
                newSrList.size();
            } catch (Exception e) {
                e.printStackTrace();
            }



            if(newSrList.size()==0){
                alertMessage("Please Select Agency");
                return;

            }
            enableLoadingBar(true);


            count = 0;
            for (int i = 0; i < newSrList.size(); i++) {

                Log.e("sdsdfsdfs","Clicked"+" "+newSrList.get(i).isStatusCheck()+"");



                if (newSrList.get(i).isStatusCheck()) {
                    ProposedAgencyModel objData = new ProposedAgencyModel();

                    objData.setAg_detail_id(newSrList.get(i).getId());
                    objData.setAg_req_id(newSrList.get(i).getRequestId());

                    objData.setAgency_name(newSrList.get(i).getAgency_name());

                    objData.setAgenct_name(newSrList.get(i).getAgenct_name());
                    objData.setSOHApproval("1");

                    objData.setSOHRemark(mBinding.remarkCr.getText().toString());
                   // objData.setSOHUpdateBy(PreferenceManager.getAgentId(context));
                    objData.setSOHUpdateBy(PreferenceManager.getPrefAgentName(context));

                    objData.setAgencyRequestCIRReq_count(newSrList.get(i).getAgencyRequestCIRReq_count());

                    objData.setAgencyRequestKYBP_count(newSrList.get(i).getAgencyRequestKYBP_count());

                    objData.setAgencyRequestSurvey_count(newSrList.get(i).getAgencyRequestSurvey_count());

                    objData.setAgencyRequestCIRReqJJ_count(newSrList.get(i).getAgencyRequestCIRReqJJ_count());

                    Date c = Calendar.getInstance().getTime();
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                    String formattedDate = df.format(c);
                    objData.setSOHUpdateOn(formattedDate+" 00:00:00.000000");
                    objData.setUser_id(PreferenceManager.getAgentId(this));
                    addRemarkData(objData, newSrList.get(i).getId());
                    count++;

                    Log.e("isStatusCheck" + i, String.valueOf(newSrList.get(i).isStatusCheck()));
                } else {
                    Log.e("isStatusCheck" + i, String.valueOf(newSrList.get(i).isStatusCheck()));
                    ProposedAgencyModel objData = new ProposedAgencyModel();

                    objData.setAg_detail_id(newSrList.get(i).getId());
                    objData.setAg_req_id(newSrList.get(i).getRequestId());

                    objData.setAgency_name(newSrList.get(i).getAgency_name());

                    objData.setAgenct_name(newSrList.get(i).getAgenct_name());

                    objData.setSOHApproval("0");

                    objData.setSOHRemark(mBinding.remarkCr.getText().toString());
                   // objData.setSOHUpdateBy(PreferenceManager.getAgentId(context));
                    objData.setSOHUpdateBy(PreferenceManager.getPrefAgentName(context));

                    objData.setAgencyRequestCIRReq_count(newSrList.get(i).getAgencyRequestCIRReq_count());

                    objData.setAgencyRequestKYBP_count(newSrList.get(i).getAgencyRequestKYBP_count());

                    objData.setAgencyRequestSurvey_count(newSrList.get(i).getAgencyRequestSurvey_count());

                    objData.setAgencyRequestCIRReqJJ_count(newSrList.get(i).getAgencyRequestCIRReqJJ_count());



                    Date c = Calendar.getInstance().getTime();
                    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                    String formattedDate = df.format(c);

                    objData.setSOHUpdateOn(formattedDate+" 00:00:00.000000");
                    objData.setUser_id(PreferenceManager.getAgentId(this));
                    addRemarkData(objData, newSrList.get(i).getId());

                    count++;
                }

            }
            if (count == newSrList.size()) {
                enableLoadingBar(false);

                finish();
            }
        });





    }

    private void alertMessage(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })

                .show();
    }

    public void getAgentData() {


        // enableLoadingBar(true);


        Log.e("sdfsdfsfsdf",dataView.getId());

        Call<String> call = SamriddhiApplication.getmInstance().getApiService().getRequestData(dataView.getId());
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {
                    enableLoadingBar(false);


                    assert response.body() != null;


                    try {
                        double totalAsd=0;
                        double totalAsdCollected=0;
                        double totalPDC=0;
                        double difference=0;

                        JSONObject requestData = new JSONObject(response.body());
                        Log.e("requestData", requestData.toString());
                        JSONArray jsonArray = requestData.getJSONArray("AgencyRequestAGDetail_set");


                        mBinding.agencyCount.setText(String.valueOf(jsonArray.length()));


                        if (jsonArray.length() > 0) {


                            for (int i = 0; i < jsonArray.length(); i++) {

                                ProposedAgencyModel objData = new ProposedAgencyModel();
                                JSONObject str = jsonArray.getJSONObject(i);

                                if(str.getString("status").equals("1")) {


                                    objData.setId(str.getString("ag_detail_id"));
                                    objData.setAgency_name(str.getString("agency_name"));
                                    objData.setAgenct_name(str.getString("agenct_name"));
                                    objData.setCreatedUser(requestData.getString("create_user"));
                                    objData.setTotal_copies(requestData.getString("tot_copies"));

                                    objData.setMobile(str.getString("ag_mono_otp"));
                                    objData.setRequestId(str.getString("ag_req_id"));
                                    objData.setJj_copies(requestData.getString("jj_copies"));
                                    objData.setMain_copies(requestData.getString("main_copies"));
                                    objData.setSOHRemark(str.getString("SOHRemark"));
                                    objData.setUOHRemark(str.getString("UOHRemark"));
                                    objData.setHORemark(str.getString("HORemark"));

                                    Log.e("sdfsdsgds",str.getString("UOHApproval"));
                                    if (str.getString("UOHApproval").equalsIgnoreCase("1")) {
                                        mBinding.linearUOH.setVisibility(View.VISIBLE);
                                        objData.setStatusCheck(true);
                                        mBinding.tvUOHRemark.setText(str.getString("UOHRemark"));
                                        Log.e("sdfsdsgds","If");
                                    }

                                    if (str.getString("SOHApproval").equalsIgnoreCase("1")) {
                                        mBinding.linearSOH.setVisibility(View.VISIBLE);
                                        objData.setStatusCheck(true);
                                        mBinding.tvSohRemark.setText(str.getString("SOHRemark"));
                                    }



                                    if (str.getString("HOApproval").equalsIgnoreCase("1")) {
                                        mBinding.linearHO.setVisibility(View.VISIBLE);
                                        mBinding.tvHoRemark.setText(str.getString("HORemark"));
                                    }

                                    /*if (str.getString("UOHRemark").equalsIgnoreCase("null") || str.getString("UOHRemark").equalsIgnoreCase(null) || str.getString("UOHRemark").equalsIgnoreCase("")) {

                                    } else {
                                        if (remarkUohStatus) {
                                            remarkUohStatus = false;
                                            mBinding.tvUOHRemark.setText(str.getString("UOHRemark"));
                                        }
                                    }
*/

                                   // JSONArray AgencyRequestCIRReq_set = str.getJSONArray("AgencyRequestCIRReq_set");
                                    JSONArray AgencyRequestCIRReq_set = str.getJSONArray("AgencyRequestCIRReq_set");
                                    objData.setAgencyRequestCIRReq_set(AgencyRequestCIRReq_set);
                                    //JSONArray AgencyRequestKYBP_count=str.getJSONArray("AgencyRequestKYBP_set");

                                    // JSONArray AgencyRequestSurvey_set=str.getJSONArray("AgencyRequestSurvey_set");

                                    objData.setAgencyRequestCIRReq_count(Integer.parseInt(str.getString("AgencyRequestCIRReq_count")));
                                    objData.setAgencyRequestKYBP_count(Integer.parseInt(str.getString("AgencyRequestKYBP_count")));
                                    objData.setAgencyRequestSurvey_count(Integer.parseInt(str.getString("AgencyRequestSurvey_count")));
                                    objData.setAgencyRequestCIRReqJJ_count(Integer.parseInt(str.getString("AgencyRequestCIRReqJJ_count")));

                                    objData.setUOHApproval(str.getString("UOHApproval"));
                                    objData.setHOApproval(str.getString("HOApproval"));
                                    objData.setSOHApproval(str.getString("SOHApproval"));

                                    for (int k = 0; k < AgencyRequestCIRReq_set.length(); k++) {
                                        JSONObject ar = AgencyRequestCIRReq_set.getJSONObject(k);
                                        objData.setCommission_amt(ar.getString("commission_amt"));
                                        objData.setCommission_per(ar.getString("commission_per"));
                                        objData.setAsd_collected(ar.getString("asd_collected"));
                                        objData.setAsd_as_per_norms(ar.getString("asd_as_per_norms"));

                                        totalAsd+=Double.parseDouble(ar.getString("asd_as_per_norms"));
                                        totalAsdCollected+=Double.parseDouble(ar.getString("asd_collected"));
                                    }

                                    for (int j = 0; j <AgencyRequestCIRReq_set.length() ; j++) {
                                        JSONObject obj = AgencyRequestCIRReq_set.getJSONObject(j);
                                        JSONArray jsonArray1=obj.getJSONArray("CirReqCheque_set");
                                        for (int k = 0; k <jsonArray1.length() ; k++) {
                                            JSONObject jsonObjects=jsonArray1.getJSONObject(k);
                                            totalPDC+=Double.parseDouble(jsonObjects.getString("cheque_amt"));
                                        }
                                    }
                                    srList.add(objData);
                                }
                                else {
                                    Log.e("sdfsfsf","else");
                                }

                            }


                            Log.e("sdfsdfsdfssdfs",totalAsd+"");
                            Log.e("sdfsdfsdfssdfs",totalAsdCollected+"");
                            mBinding.txtTotal.setText(totalAsd+"");
                            mBinding.txtCollected.setText(totalAsdCollected+"");

                            mBinding.txtPDC.setText(totalPDC+"");
                            BigDecimal c = BigDecimal.valueOf(totalAsd).subtract(BigDecimal.valueOf(totalAsdCollected));
                            BigDecimal total=c.subtract(BigDecimal.valueOf(totalPDC));

                            mBinding.txtDifference.setText(total+"");


                            adapter = new Approval_Agency_Adapter(context, srList, contextListener);
                            mBinding.agencyDetailsRec.setAdapter(adapter);
                            adapter.notifyDataSetChanged();

                        } else {

                        }

                        enableLoadingBar(false);

                        //  enableLoadingBar(false);
                    } catch (Exception e) {
                      Log.e("sdsdfsdsfdsfs",e.getMessage());
                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }


    @Override
    public void onItemClick(ProposedAgencyModel proposedAgencyModel, ArrayList<ProposedAgencyModel> list) {


        if (dataView.getHOApproval().equalsIgnoreCase("1") || dataView.getHOApproval().equalsIgnoreCase("2")) {
            alertMessage("You Can not edit Now ");
        } else {
            if (proposedAgencyModel.isStatusCheck()) {
                //  mBinding.btnlayout.setVisibility(View.VISIBLE);
            } else {
                //  mBinding.btnlayout.setVisibility(View.GONE);
            }
        }

        data = proposedAgencyModel;
        newSrList = list;
        // addRemarkData(proposedAgencyModel);

    }

    @Override
    public void onEditClick(ProposedAgencyModel proposedAgencyModel) {

        Intent intentttt = new Intent(context, ViewRequestData.class);
        intentttt.putExtra("MyData",dataView);
        getIntent().getSerializableExtra("MyData");
        startActivity(intentttt);

       /* AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view1 = LayoutInflater.from(context).inflate(R.layout.edit_agency_approval_matrix, null);
        //View view1 = LayoutInflater.from(context).inflate(R.layout.perposed_agency_rec_list, null);

        TextView otp_veri = view1.findViewById(R.id.otp_veri);
        TextView tvAgentName = view1.findViewById(R.id.tvAgentName);

        TextView kybp_form = view1.findViewById(R.id.kybp_form);
        TextView survey_form = view1.findViewById(R.id.survey_form);
        TextView cir_recipt = view1.findViewById(R.id.cir_recipt);
        TextView jj_cir = view1.findViewById(R.id.jj_cir);

        ImageView imgCross = view1.findViewById(R.id.open);
        otp_veri.setVisibility(View.GONE);
        imgCross.setImageResource(R.drawable.cross);
        tvAgentName.setText(proposedAgencyModel.getAgency_name());




        if(proposedAgencyModel.getJj_copies().equals("0")){
            jj_cir.setVisibility(View.GONE);
        }
        else {
            jj_cir.setVisibility(View.VISIBLE);
        }

        survey_form.setOnClickListener(v -> {

            if (proposedAgencyModel.getAgencyRequestSurvey_count() > 0) {
                g.SURVEYStatus = 1;
            } else {
                g.SURVEYStatus = 0;
            }

            g.ActionType = 2;
            Intent intent = new Intent(context, SurveyFormActivity.class);
            g.agentId = proposedAgencyModel.getId();

            g.agentNameStr = proposedAgencyModel.getAgenct_name();
            g.agencyNameStr = proposedAgencyModel.getAgency_name();
            g.agentMobileStr = proposedAgencyModel.getMobile();
            g.requestId = dataView.getId();
            startActivity(intent);

        });

        kybp_form.setOnClickListener(v -> {
            if (proposedAgencyModel.getAgencyRequestKYBP_count() > 0) {
                g.KYBPStatus = 1;
            } else {
                g.KYBPStatus = 0;
            }
            g.ActionType = 2;

            Intent intent = new Intent(context, KYBPActivity.class);
            g.agentId = proposedAgencyModel.getId();
            g.agentMobileStr = proposedAgencyModel.getMobile();
            g.agentNameStr = proposedAgencyModel.getAgenct_name();
            g.agencyNameStr = proposedAgencyModel.getAgency_name();
            g.requestId = dataView.getId();
            startActivity(intent);

        });
        cir_recipt.setOnClickListener(v -> {
            if (proposedAgencyModel.getAgencyRequestCIRReq_count() > 0) {
                g.CIRStatus = 1;
            } else {
                g.CIRStatus = 0;
            }
            g.ActionType = 2;

            Intent intent = new Intent(context, Circulation_MainForm.class);
            g.agentId = proposedAgencyModel.getId();
            g.agentNameStr = proposedAgencyModel.getAgenct_name();
            g.agencyNameStr = proposedAgencyModel.getAgency_name();
            g.agentMobileStr = proposedAgencyModel.getMobile();
            g.mainSaleOfficeCopy = proposedAgencyModel.getMain_copies();
            g.requestId = dataView.getId();
            startActivity(intent);

        });


        jj_cir.setOnClickListener(v -> {

            if (proposedAgencyModel.getAgencyRequestCIRReqJJ_count() > 0) {
                g.JJCIRStatus = 1;
            } else {
                g.JJCIRStatus = 0;
            }
            g.ActionType = 2;

            Intent intent = new Intent(context, Circulation_JJForm.class);
            g.agentId = proposedAgencyModel.getId();
            g.agentNameStr = proposedAgencyModel.getAgenct_name();
            g.agencyNameStr = proposedAgencyModel.getAgency_name();
            g.agentMobileStr = proposedAgencyModel.getMobile();
            //g.jjSaleOfficeCopy = proposedAgencyModel.getJj_copies();
            g.requestId = dataView.getId();
            startActivity(intent);

        });


        imgCross.setOnClickListener(v -> {
            alertDialog.dismiss();
            alertDialog.cancel();
        });
        builder.setView(view1);
        alertDialog = builder.create();
        alertDialog.show();*/




        /////


       /* AlertDialog.Builder builder2 = new AlertDialog.Builder(context);
        builder2.setCancelable(false);
        View view2 = LayoutInflater.from(context).inflate(R.layout.agency_add_layout, null);

        EditText name = view2.findViewById(R.id.agencyName);
        name.setText(proposedAgencyModel.getAgency_name());

        EditText agentName = view2.findViewById(R.id.agentName);
        agentName.setText(proposedAgencyModel.getAgenct_name());

        EditText agent_mobile = view2.findViewById(R.id.agent_mobile);
        agent_mobile.setText(proposedAgencyModel.getMobile());

        EditText otp = view2.findViewById(R.id.otp);

        Button btnSubmit = view2.findViewById(R.id.getAgencyBtn);
        ImageView closeImg = view2.findViewById(R.id.closeImg);


        btnSubmit.setOnClickListener(v1 -> {

            if (name.getText().toString().length() == 0) {
                alertMessage("Please Enter The Name");
                return;
            }
            if (agentName.getText().toString().length() == 0) {
                alertMessage("Please Enter The Agent Name");
                return;
            }
            if (agent_mobile.getText().toString().length() == 0 || agent_mobile.getText().toString().length() < 10) {
                alertMessage("Please Enter The Valid Agent Mobile");
                return;
            }
          //  addAgency(name.getText().toString(), agentName.getText().toString(), agent_mobile.getText().toString());
            proposedAgencyModel.setAgency_name(name.getText().toString());
            proposedAgencyModel.setAgenct_name(agentName.getText().toString());
            proposedAgencyModel.setMobile(agent_mobile.getText().toString());
            addRemarkData(proposedAgencyModel,proposedAgencyModel.getAg_detail_id());
            alertDialog.cancel();
            alertDialog.dismiss();

        });
        builder2.setView(view2);
        closeImg.setOnClickListener(v1 -> {
            alertDialog.cancel();
            alertDialog.dismiss();
        });

        alertDialog = builder2.create();
        alertDialog.show();*/

    }

    @Override
    public void onDeleteClick(ProposedAgencyModel proposedAgencyModel) {

    }

    public void addRemarkData(ProposedAgencyModel objData, String agId) {
        //  http://103.96.220.236/agency/agdetail/64/


        Gson gson = new Gson();
        String data1 = gson.toJson(objData).toString();
        Log.e("asfafafaffs", agId);
        Log.e("asfafafaffs", data1);

        Call<String> call = SamriddhiApplication.getmInstance().getApiService().addUOHRemark(agId, objData);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {

                if (response.isSuccessful()) {

                    assert response.body() != null;


                    Log.e("dataShow", response.toString());


                    try {
                        JSONObject jsonObject = new JSONObject(response.body());
                      //  Toast.makeText(context, jsonObject.toString(), Toast.LENGTH_SHORT).show();

                        // String message = jsonObject.getString("message");

                        Toast.makeText(context, "Your Request Order SuccessFully generated ", Toast.LENGTH_SHORT).show();

                        finish();


                        //  enableLoadingBar(false);
                    } catch (Exception e) {

                        e.printStackTrace();
                    }

                } else {
                    try {
                        Toast.makeText(context, "error message" + response.errorBody().string(), Toast.LENGTH_SHORT).show();

                    } catch (IOException e) {
                        Toast.makeText(context, "error message" + response.errorBody().toString(), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                enableLoadingBar(false);
                Toast.makeText(context, "Something Went Wrong", Toast.LENGTH_SHORT).show();
            }

        });
    }
}
