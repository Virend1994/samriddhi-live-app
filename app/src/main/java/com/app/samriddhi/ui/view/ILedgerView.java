package com.app.samriddhi.ui.view;

import com.app.samriddhi.ui.model.LedgerModel;

public interface ILedgerView extends IView {
    void onSuccess(LedgerModel mLedgerData, String agencyName);
}
