package com.app.samriddhi.ui.activity.main.ui;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import androidx.databinding.DataBindingUtil;

import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseActivity;
import com.app.samriddhi.databinding.ActivityContactUsBinding;
import com.app.samriddhi.ui.model.ContactUsData;
import com.app.samriddhi.ui.presenter.ContactUsPresenter;
import com.app.samriddhi.ui.view.IContactUsView;

import java.util.List;

public class ContactUsActivity extends BaseActivity implements IContactUsView {
    ActivityContactUsBinding mBinding;
    ContactUsPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_contact_us);
        setViews();

    }

    /**
     * initialize nad bind the views
     */

    private void setViews() {
        mPresenter = new ContactUsPresenter();
        mPresenter.setView(this);
        mPresenter.getContactData();
        mBinding.toolbarContact.txtTittle.setText(getResources().getString(R.string.str_contact_us_tittle));

        mBinding.toolbarContact.imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        mBinding.toolbarContact.rlNotification.setOnClickListener(v -> {
            startActivityAnimation(this, NotificationActivity.class, false);
        });
    }

    @Override
    public void onSuccess(List<ContactUsData> mData) {
        if (mData.size() > 0) {
            mBinding.setItem(mData.get(0).getFields());
        }

    }

    @Override
    public Context getContext() {
        return this;
    }

    /**
     * handle the click events on screen
     * @param view item view
     */

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_phone:
                startActivity(new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", mBinding.txtPhone.getText().toString(), null)));
                break;
            case R.id.txt_email:
                Intent i = new Intent(Intent.ACTION_SEND);
                i.setType("message/rfc822");
                i.putExtra(Intent.EXTRA_EMAIL, new String[]{mBinding.txtEmail.getText().toString()});
                i.putExtra(Intent.EXTRA_SUBJECT, "subject of email");
                i.putExtra(Intent.EXTRA_TEXT, "body of email");
                try {
                    startActivity(Intent.createChooser(i, "Send mail..."));
                } catch (android.content.ActivityNotFoundException ex) {
                    Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
                }
                break;
            default:
                break;

        }

    }
}
