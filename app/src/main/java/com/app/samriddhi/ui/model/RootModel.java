package com.app.samriddhi.ui.model;

import java.util.List;

public class RootModel {
    public int state_id;
    public int unit_id;
    public int ag_req_id;
    public int city_id;
    public String city_upc;
    public String salesoff_main_id;
    public int location_id;
    public int salesdist_id;
    public String salesoff_jj_id;
    public int town_id;
    public List<AgencyRequestReasonSetModel> AgencyRequestReason_set;
    public List<AgencyRequestAGDetailSetModel> AgencyRequestAGDetail_set;
    public String pincode;
    public String population;
    public int tot_copies;
    public int main_copies;
    public int jj_copies;
    public int cluster_id;

    public String reason;
    public int proposed_count;
    public Object status;

    public String getSalesoff_jj_id() {
        return salesoff_jj_id;
    }

    public void setSalesoff_jj_id(String salesoff_jj_id) {
        this.salesoff_jj_id = salesoff_jj_id;
    }

    public String getSalesoff_main_id() {
        return salesoff_main_id;
    }

    public void setSalesoff_main_id(String salesoff_main_id) {
        this.salesoff_main_id = salesoff_main_id;
    }

    public int getSalesdist_id() {
        return salesdist_id;
    }

    public void setSalesdist_id(int salesdist_id) {
        this.salesdist_id = salesdist_id;
    }

    public int getCluster_id() {
        return cluster_id;
    }

    public void setCluster_id(int cluster_id) {
        this.cluster_id = cluster_id;
    }

    public int getAg_req_id() {
        return ag_req_id;
    }

    public void setAg_req_id(int ag_req_id) {
        this.ag_req_id = ag_req_id;
    }

    public Object getStatus() {
        return status;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public Object getCreate_user() {
        return create_user;
    }

    public void setCreate_user(Object create_user) {
        this.create_user = create_user;
    }

    public Object getUpdate_user() {
        return update_user;
    }

    public void setUpdate_user(Object update_user) {
        this.update_user = update_user;
    }

    public Object create_user;
    public Object update_user;

    public int getState_id() {
        return state_id;
    }

    public void setState_id(int state_id) {
        this.state_id = state_id;
    }

    public int getUnit_id() {
        return unit_id;
    }

    public void setUnit_id(int unit_id) {
        this.unit_id = unit_id;
    }

    public int getCity_id() {
        return city_id;
    }

    public void setCity_id(int city_id) {
        this.city_id = city_id;
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public int getTown_id() {
        return town_id;
    }

    public void setTown_id(int town_id) {
        this.town_id = town_id;
    }

    public List<AgencyRequestReasonSetModel> getAgencyRequestReason_set() {
        return AgencyRequestReason_set;
    }

    public void setAgencyRequestReason_set(List<AgencyRequestReasonSetModel> agencyRequestReason_set) {
        this.AgencyRequestReason_set = agencyRequestReason_set;
    }

    public List<AgencyRequestAGDetailSetModel> getAgencyRequestAGDetail_set() {
        return AgencyRequestAGDetail_set;
    }

    public void setAgencyRequestAGDetail_set(List<AgencyRequestAGDetailSetModel> agencyRequestAGDetail_set) {
        this.AgencyRequestAGDetail_set = agencyRequestAGDetail_set;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    public int getTot_copies() {
        return tot_copies;
    }

    public void setTot_copies(int tot_copies) {
        this.tot_copies = tot_copies;
    }

    public int getMain_copies() {
        return main_copies;
    }

    public void setMain_copies(int main_copies) {
        this.main_copies = main_copies;
    }

    public int getJj_copies() {
        return jj_copies;
    }

    public void setJj_copies(int jj_copies) {
        this.jj_copies = jj_copies;
    }

    public String getCity_upc() {
        return city_upc;
    }

    public void setCity_upc(String city_upc) {
        this.city_upc = city_upc;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public int getProposed_count() {
        return proposed_count;
    }

    public void setProposed_count(int proposed_count) {
        this.proposed_count = proposed_count;
    }
}
