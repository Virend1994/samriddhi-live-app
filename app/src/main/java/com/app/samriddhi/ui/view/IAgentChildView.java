package com.app.samriddhi.ui.view;

import com.app.samriddhi.ui.model.AgentChildResultModel;

public interface IAgentChildView extends IView {
    void onSuccess(AgentChildResultModel agentChildData);
}
