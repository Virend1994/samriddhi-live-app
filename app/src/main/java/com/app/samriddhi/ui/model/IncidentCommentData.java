package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IncidentCommentData extends BaseModel {
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("pk")
    @Expose
    private Integer pk;
    @SerializedName("fields")
    @Expose
    private IncidentCommentListModel fields;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Integer getPk() {
        return pk;
    }

    public void setPk(Integer pk) {
        this.pk = pk;
    }

    public IncidentCommentListModel getFields() {
        return fields;
    }

    public void setFields(IncidentCommentListModel fields) {
        this.fields = fields;
    }

}
