package com.app.samriddhi.ui.view;

import com.app.samriddhi.ui.model.ContactUsData;

import java.util.List;

public interface IContactUsView extends IView {
    void onSuccess(List<ContactUsData> mData);
}
