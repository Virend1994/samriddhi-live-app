package com.app.samriddhi.ui.activity.main.ui.Grievance;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;

import androidx.appcompat.view.ContextThemeWrapper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.app.samriddhi.R;
import com.app.samriddhi.base.BaseFragment;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.Grievance.multipleimage.GiftAdapter;
import com.app.samriddhi.ui.activity.main.ui.Grievance.multipleimage.ScalingUtilities;
import com.app.samriddhi.ui.activity.main.ui.Grievance.multipleimage.MultipleImageSelectActivity;
import com.app.samriddhi.ui.view.IView;
import com.app.samriddhi.util.AlertUtility;
import com.app.samriddhi.util.Constant;

import com.google.android.material.button.MaterialButton;
import com.vlk.multimager.utils.Constants;
import com.vlk.multimager.utils.Image;
import com.vlk.multimager.utils.Params;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;

public class ComplaintFragment extends BaseFragment {

    private static final String IMAGE_DIRECTORY = "/directory";
    private final int GALLERY = 1;
    private final int CAMERA = 2;
    List<Uri> mSelected;
    File ImageFile;
    File file2;
    File file3;
    File file4;
    File file5;
    String strImage = "";

    String strImage1 = "";
    String strImage2 = "";
    String strImage3 = "";
    String strImage4 = "";
    String strImage5 = "";
    private static final int INTENT_REQUEST_GET_IMAGES = 13;
    public ArrayList<String> catMaster;

    public ArrayList<String> catId;

    public ArrayList<String> subCatMaster;

    public ArrayList<String> subCatId;

    BetterSpinner tvCategory, tvSubCat;

    boolean flagCat = false, flagSubCat = false;

    String strCatId = "", strSubCatId = "";

    String strDate = "";
    EditText editDescription;
    TextView txtAddImages;
    TextView txtUploadHint;
    MaterialButton save_cr;
    private Uri uriFilePath;

    /* Multiple images work*/
    public static RecyclerView image_recycle;
    public static List<String> mDetials123 = new ArrayList<String>();
    public static GiftAdapter giftadapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_complaint, container, false);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        mDetials123 = new ArrayList<>();
        txtAddImages = view.findViewById(R.id.txtAddImages);
        image_recycle = view.findViewById(R.id.recyclerview);
        txtUploadHint = view.findViewById(R.id.txtUploadHint);
        editDescription = view.findViewById(R.id.editDescription);
        txtAddImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPictureDialog();

            }
        });


        tvCategory = view.findViewById(R.id.tvCategory);
        tvSubCat = view.findViewById(R.id.tvSubCat);
        getCategory();

        save_cr = view.findViewById(R.id.save_cr);
        save_cr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String strDescription = editDescription.getText().toString();

                if (strCatId.equals("")) {

                    Toast.makeText(getActivity(), R.string.select_category, Toast.LENGTH_SHORT).show();
                } else if (strSubCatId.equals("")) {

                    Toast.makeText(getActivity(), R.string.select_sub_category, Toast.LENGTH_SHORT).show();
                } else if (strDescription.equals("")) {

                    Toast.makeText(getActivity(), R.string.enter_description, Toast.LENGTH_SHORT).show();
                    editDescription.setText("");
                    strDescription = "";
                } else {

                    SubmitComplaint(strDescription);
                }
            }
        });


        TextView txtCat = view.findViewById(R.id.txtCat);
        TextView txtSubCat = view.findViewById(R.id.txtSubCat);
        TextView txtDesc = view.findViewById(R.id.txtDesc);

        if (PreferenceManager.getAppLang(getActivity()).equals("hi")) {

            txtCat.setText("शिकायत का प्रकार चुनें");
            txtSubCat.setText("संबंधित शिकायत का चयन करें");
            txtDesc.setText("शिकायत विवरण");
            txtAddImages.setText("इमेज अपलोड करे");
            save_cr.setText("सबमिट");

        }

        return view;
    }


    private void alertMessage(String message) {

        AlertDialog alertDialog = new AlertDialog.Builder(getActivity())
                .setMessage(message)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                })

                .show();
    }

  /* @Override
    public void onResume() {
        super.onResume();


        Log.e("dsgsdgsgsdg", mDetials123.size() + "");

        if (mDetials123.size() != 0) {
            image_recycle.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.HORIZONTAL));
            giftadapter = new GiftAdapter(getActivity(), mDetials123, "0");
            image_recycle.setAdapter(giftadapter);
            image_recycle.setVisibility(View.VISIBLE);
            txtUploadHint.setVisibility(View.VISIBLE);

        }
        else {
            image_recycle.setVisibility(View.GONE);
            txtUploadHint.setVisibility(View.GONE);
        }

    }*/

    public void SubmitComplaint(String strDescription) {

        Log.e("afsafafafa", "array" + " " + mDetials123.size() + "");

        for (int i = 0; i < mDetials123.size(); i++) {

            if (i == 0) {

                ImageFile = new File(mDetials123.get(0));
                Log.e("dasfasfasf", i + " " + ImageFile.toString());

                int maxFileSize = 1 * 1024 * 1024;
                Long l = ImageFile.length();
                String fileSize = l.toString();
                int finalFileSize = Integer.parseInt(fileSize);

                Log.e("djhksdjhkfjs", i + "image size" + finalFileSize + "");
                Log.e("djhksdjhkfjs", i + "total mb" + maxFileSize + "");
                if (finalFileSize > maxFileSize) {
                    alertMessage("Image 1 size is greater than 1 mb");
                    return;

                }
            }

            if (i == 1) {

                file2 = new File(mDetials123.get(1));
                Log.e("dasfasfasf", i + " " + file2.toString());

                int maxFileSize = 1 * 1024 * 1024;
                Long l = ImageFile.length();
                String fileSize = l.toString();
                int finalFileSize = Integer.parseInt(fileSize);

                Log.e("djhksdjhkfjs", i + "image size" + finalFileSize + "");
                Log.e("djhksdjhkfjs", i + "total mb" + maxFileSize + "");
                if (finalFileSize > maxFileSize) {
                    alertMessage("Image 2 size is greater than 1 mb");
                    return;

                }
            }
            if (i == 2) {
                file3 = new File(mDetials123.get(2));
                Log.e("dasfasfasf", i + " " + file3.toString());

                int maxFileSize = 1 * 1024 * 1024;
                Long l = ImageFile.length();
                String fileSize = l.toString();
                int finalFileSize = Integer.parseInt(fileSize);

                Log.e("djhksdjhkfjs", i + "image size" + finalFileSize + "");
                Log.e("djhksdjhkfjs", i + "total mb" + maxFileSize + "");
                if (finalFileSize > maxFileSize) {
                    alertMessage("Image 3 size is greater than 1 mb");
                    return;

                }
            }


            if (i == 3) {
                file4 = new File(mDetials123.get(3));
                Log.e("dasfasfasf", i + " " + file4.toString());

                int maxFileSize = 1 * 1024 * 1024;
                Long l = ImageFile.length();
                String fileSize = l.toString();
                int finalFileSize = Integer.parseInt(fileSize);

                Log.e("djhksdjhkfjs", i + "image size" + finalFileSize + "");
                Log.e("djhksdjhkfjs", i + "total mb" + maxFileSize + "");
                if (finalFileSize > maxFileSize) {
                    alertMessage("Image 4 size is greater than 1 mb");
                    return;

                }
            }
            if (i == 4) {
                file5 = new File(mDetials123.get(4));
                Log.e("dasfasfasf", i + " " + file4.toString());

                int maxFileSize = 1 * 1024 * 1024;
                Long l = ImageFile.length();
                String fileSize = l.toString();
                int finalFileSize = Integer.parseInt(fileSize);

                Log.e("djhksdjhkfjs", i + "image size" + finalFileSize + "");
                Log.e("djhksdjhkfjs", i + "total mb" + maxFileSize + "");
                if (finalFileSize > maxFileSize) {
                    alertMessage("Image 5 size is greater than 1 mb");
                    return;

                }
            }
        }

        enableLoadingBar(true);
        if (mDetials123.size() == 5) {
            Log.e("dfgdfgdfg", "5");
            AndroidNetworking.upload(Constant.BASE_URL_Grievance + "agentservices/insertCqrs")
                    .addMultipartParameter("cat_id", strCatId)
                    .addMultipartParameter("sub_cat_id", strSubCatId)
                    .addMultipartParameter("agent_id", PreferenceManager.getAgentId(getActivity()))
                    .addMultipartParameter("description", strDescription)
                    .addMultipartParameter("complaint_type", "c")
                    .addMultipartFile("image_path", ImageFile)
                    .addMultipartFile("image_path2", file2)
                    .addMultipartFile("image_path3", file3)
                    .addMultipartFile("image_path4", file4)
                    .addMultipartFile("image_path5", file5)
                    .setTag("Upload")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            dismissProgressBar();
                            Log.e("dfgdfgdfg", response.toString());

                            try {

                                if (response.getString("status").equals("success")) {
                                    PreferenceManager.setComplaint_number(getActivity(), response.getString("request_id"));
                                    PreferenceManager.setComplaint_Type(getActivity(), "c");
                                    startActivity(new Intent(getActivity(), SubmitRequestPopup.class));
                                    getActivity().finish();
                                } else {

                                    Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception ex) {
                                // Log.e("safafaf", anError.getMessage());
                                dismissProgressBar();
                                Log.e("safafasfasf", ex.getMessage());
                            }


                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("safafaf", anError.getMessage());
                            dismissProgressBar();
                            // Log.e("dfgdfgdfg", anError.getMessage());

                        }
                    });

        }

        if (mDetials123.size() == 4) {
            Log.e("dfgdfgdfg", "4");
            AndroidNetworking.upload(Constant.BASE_URL_Grievance + "agentservices/insertCqrs")
                    .addMultipartParameter("cat_id", strCatId)
                    .addMultipartParameter("sub_cat_id", strSubCatId)
                    .addMultipartParameter("agent_id", PreferenceManager.getAgentId(getActivity()))
                    .addMultipartParameter("description", strDescription)
                    .addMultipartParameter("complaint_type", "c")
                    .addMultipartFile("image_path", ImageFile)
                    .addMultipartFile("image_path2", file2)
                    .addMultipartFile("image_path3", file3)
                    .addMultipartFile("image_path4", file4)
                    .setTag("Upload")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            dismissProgressBar();
                            Log.e("dfgdfgdfg", response.toString());

                            try {

                                if (response.getString("status").equals("success")) {
                                    PreferenceManager.setComplaint_number(getActivity(), response.getString("request_id"));
                                    PreferenceManager.setComplaint_Type(getActivity(), "c");
                                    startActivity(new Intent(getActivity(), SubmitRequestPopup.class));
                                    getActivity().finish();
                                } else {

                                    Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception ex) {
                                // Log.e("safafaf", anError.getMessage());
                                dismissProgressBar();
                                Log.e("safafasfasf", ex.getMessage());
                            }


                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("safafaf", anError.getMessage());
                            dismissProgressBar();
                            // Log.e("dfgdfgdfg", anError.getMessage());

                        }
                    });

        }


        if (mDetials123.size() == 3) {

            Log.e("dfgdfgdfg", "3");
            AndroidNetworking.upload(Constant.BASE_URL_Grievance + "agentservices/insertCqrs")
                    .addMultipartParameter("cat_id", strCatId)
                    .addMultipartParameter("sub_cat_id", strSubCatId)
                    .addMultipartParameter("agent_id", PreferenceManager.getAgentId(getActivity()))
                    .addMultipartParameter("description", strDescription)
                    .addMultipartParameter("complaint_type", "c")
                    .addMultipartFile("image_path", ImageFile)
                    .addMultipartFile("image_path2", file2)
                    .addMultipartFile("image_path3", file3)
                    .setTag("Upload")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            dismissProgressBar();
                            Log.e("dfgdfgdfg", response.toString());

                            try {

                                if (response.getString("status").equals("success")) {
                                    PreferenceManager.setComplaint_number(getActivity(), response.getString("request_id"));
                                    PreferenceManager.setComplaint_Type(getActivity(), "c");
                                    startActivity(new Intent(getActivity(), SubmitRequestPopup.class));
                                    getActivity().finish();
                                } else {

                                    Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception ex) {
                                // Log.e("safafaf", anError.getMessage());
                                dismissProgressBar();
                                Log.e("safafasfasf", ex.getMessage());
                            }


                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("safafaf", anError.getMessage());
                            dismissProgressBar();
                            // Log.e("dfgdfgdfg", anError.getMessage());

                        }
                    });

        }
        if (mDetials123.size() == 2) {
            Log.e("dfgdfgdfg", "2");
            AndroidNetworking.upload(Constant.BASE_URL_Grievance + "agentservices/insertCqrs")
                    .addMultipartParameter("cat_id", strCatId)
                    .addMultipartParameter("sub_cat_id", strSubCatId)
                    .addMultipartParameter("agent_id", PreferenceManager.getAgentId(getActivity()))
                    .addMultipartParameter("description", strDescription)
                    .addMultipartParameter("complaint_type", "c")
                    .addMultipartFile("image_path", ImageFile)
                    .addMultipartFile("image_path2", file2)
                    .setTag("Upload")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            dismissProgressBar();
                            Log.e("dfgdfgdfg", response.toString());

                            try {

                                if (response.getString("status").equals("success")) {
                                    PreferenceManager.setComplaint_number(getActivity(), response.getString("request_id"));
                                    PreferenceManager.setComplaint_Type(getActivity(), "c");
                                    startActivity(new Intent(getActivity(), SubmitRequestPopup.class));
                                    getActivity().finish();
                                } else {

                                    Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception ex) {
                                // Log.e("safafaf", anError.getMessage());
                                dismissProgressBar();
                                Log.e("safafasfasf", ex.getMessage());
                            }


                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("safafaf", anError.getMessage());
                            dismissProgressBar();
                            // Log.e("dfgdfgdfg", anError.getMessage());

                        }
                    });

        }

        if (mDetials123.size() == 1) {
            Log.e("dfgdfgdfg", "1");
            AndroidNetworking.upload(Constant.BASE_URL_Grievance + "agentservices/insertCqrs")
                    .addMultipartParameter("cat_id", strCatId)
                    .addMultipartParameter("sub_cat_id", strSubCatId)
                    .addMultipartParameter("agent_id", PreferenceManager.getAgentId(getActivity()))
                    .addMultipartParameter("description", strDescription)
                    .addMultipartParameter("complaint_type", "c")
                    .addMultipartFile("image_path", ImageFile)
                    .setTag("Upload")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            dismissProgressBar();
                            Log.e("dfgdfgdfg", response.toString());

                            try {

                                if (response.getString("status").equals("success")) {
                                    PreferenceManager.setComplaint_number(getActivity(), response.getString("request_id"));
                                    PreferenceManager.setComplaint_Type(getActivity(), "c");
                                    startActivity(new Intent(getActivity(), SubmitRequestPopup.class));
                                    getActivity().finish();
                                } else {

                                    Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception ex) {
                                // Log.e("safafaf", anError.getMessage());
                                dismissProgressBar();
                                Log.e("safafasfasf", ex.getMessage());
                            }


                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("safafaf", anError.getMessage());
                            dismissProgressBar();
                            // Log.e("dfgdfgdfg", anError.getMessage());

                        }
                    });

        }


        if (mDetials123.size() == 0) {
            Log.e("dfgdfgdfg", "0");

            AndroidNetworking.upload(Constant.BASE_URL_Grievance + "agentservices/insertCqrs")
                    .addMultipartParameter("cat_id", strCatId)
                    .addMultipartParameter("sub_cat_id", strSubCatId)
                    .addMultipartParameter("agent_id", PreferenceManager.getAgentId(getActivity()))
                    .addMultipartParameter("description", strDescription)
                    .addMultipartParameter("complaint_type", "c")
                    .setTag("Upload")
                    .setPriority(Priority.HIGH)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            dismissProgressBar();
                            Log.e("dfgdfgdfg", response.toString());
                            try {

                                if (response.getString("status").equals("success")) {
                                    PreferenceManager.setComplaint_number(getActivity(), response.getString("request_id"));
                                    PreferenceManager.setComplaint_Type(getActivity(), "c");
                                    startActivity(new Intent(getActivity(), SubmitRequestPopup.class));
                                    getActivity().finish();
                                } else {

                                    Toast.makeText(getActivity(), response.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            } catch (Exception ex) {
                                // Log.e("safafaf", anError.getMessage());
                                dismissProgressBar();
                                Log.e("safafasfasf", ex.getMessage());
                            }


                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.e("safafaf", anError.getMessage());
                            dismissProgressBar();
                            // Log.e("dfgdfgdfg", anError.getMessage());

                        }
                    });
        }

    }


    public void getCategory() {
        flagCat = false;
        AndroidNetworking.post(Constant.BASE_URL_Grievance + "agentservices/fetchCatSubcat")
                .addBodyParameter("type", "c")/*Complaint*/
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            catMaster = new ArrayList<>();
                            catId = new ArrayList<>();
                            Log.e("sfsdfsfsf", response.toString());
                            JSONArray objData = new JSONArray(response.getString("categories"));

                            for (int i = 0; i < objData.length(); i++) {

                                JSONObject objStr = objData.getJSONObject(i);
                                catMaster.add(objStr.getString("name"));
                                catId.add(objStr.getString("id"));
                            }

                            flagCat = true;
                            ArrayAdapter<String> taxiTypeAdapter = new ArrayAdapter<String>(getContext(), R.layout.item_spinner_view, catMaster);
                            taxiTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            tvCategory.setAdapter(taxiTypeAdapter);

                            if (flagCat == true) {

                                tvCategory.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                        strCatId = catId.get(position);
                                        Log.e("dgsdgsdgs", strCatId);
                                        getSubCategory(strCatId);
                                    }
                                });
                            } else {
                                Toast.makeText(getActivity(), "Loading categories.", Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            Log.e("sjdfhskfsd", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        Log.e("sjdfhskfsd", anError.getMessage());
                    }
                });
    }

    public void getSubCategory(String strCatId) {

        flagSubCat = false;
        AndroidNetworking.post(Constant.BASE_URL_Grievance + "agentservices/fetchCatSubcat")
                .addBodyParameter("type", "c")/*Complaint*/
                .addBodyParameter("parentId", strCatId)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            subCatMaster = new ArrayList<>();
                            subCatId = new ArrayList<>();
                            Log.e("sfsdfsfsf", response.toString());
                            JSONArray objData = new JSONArray(response.getString("categories"));
                            for (int i = 0; i < objData.length(); i++) {
                                JSONObject objStr = objData.getJSONObject(i);
                                subCatMaster.add(objStr.getString("name"));
                                subCatId.add(objStr.getString("id"));
                            }

                            flagSubCat = true;

                            ArrayAdapter<String> taxiTypeAdapter = new ArrayAdapter<String>(getContext(), R.layout.item_spinner_view, subCatMaster);
                            taxiTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            tvSubCat.setAdapter(taxiTypeAdapter);

                            if (flagSubCat == true) {

                                tvSubCat.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                    @Override
                                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                                        strSubCatId = subCatId.get(position);
                                        Log.e("dgsdgsdgs", strSubCatId);
                                    }
                                });
                            } else {
                                Toast.makeText(getActivity(), "Loading sub categories.", Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            Log.e("sjdfhskfsd", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        Log.e("sjdfhskfsd", anError.getMessage());
                    }
                });
    }

    public void showPictureDialog() {

        AlertUtility.showAlert(getActivity(), "Select photo from gallery or camera", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(getActivity(), MultipleImageSelectActivity.class);
                Params params = new Params();
                params.setCaptureLimit(5);
                params.setPickerLimit(5);
                params.setToolbarColor(R.color.yellow_button_color);
                params.setActionButtonColor(R.color.yellow_button_color);
                params.setButtonTextColor(R.color.yellow_button_color);
                intent.putExtra(Constants.KEY_PARAMS, params);
                startActivityForResult(intent, Constants.TYPE_MULTI_PICKER);
            }
        });

//        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(new ContextThemeWrapper(getActivity(),R.style.AppTheme_AlertDialog));
//        builder .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//
//                        Intent intent = new Intent(getActivity(), MultipleImageSelectActivity.class);
//                        Params params = new Params();
//                        params.setCaptureLimit(5);
//                        params.setPickerLimit(5);
//                        params.setToolbarColor(R.color.yellow_button_color);
//                        params.setActionButtonColor(R.color.yellow_button_color);
//                        params.setButtonTextColor(R.color.yellow_button_color);
//                        intent.putExtra(Constants.KEY_PARAMS, params);
//                        startActivityForResult(intent, Constants.TYPE_MULTI_PICKER);
//
//
//                       /* AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
//                        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
//                        editor.putString(AppsContants.ImageStatus, "c");
//                        editor.commit();
//                        startActivity(new Intent(getActivity(), MultiPhotoSelectActivity.class));
//                        break;*/
//
//
//            }
//        });
//        builder.setTitle("Select photo from gallery or camera");
//        builder.show();
    }


    private String decodeFile(String path, int DESIREDWIDTH, int DESIREDHEIGHT) {
        String strMyImagePath = null;
        Bitmap scaledBitmap = null;

        try {
            // Part 1: Decode image
            Bitmap unscaledBitmap = ScalingUtilities.decodeFile(path, DESIREDWIDTH, DESIREDHEIGHT, ScalingUtilities.ScalingLogic.FIT);

            if (!(unscaledBitmap.getWidth() <= DESIREDWIDTH && unscaledBitmap.getHeight() <= DESIREDHEIGHT)) {
                // Part 2: Scale image
                scaledBitmap = ScalingUtilities.createScaledBitmap(unscaledBitmap, DESIREDWIDTH, DESIREDHEIGHT, ScalingUtilities.ScalingLogic.FIT);
            } else {
                unscaledBitmap.recycle();
                return path;
            }

            // Store to tmp file

            String extr = Environment.getExternalStorageDirectory().toString();
            File mFolder = new File(extr + "/TMMFOLDER");
            if (!mFolder.exists()) {
                mFolder.mkdir();
            }
            Random r = new Random();
            int randomNumber = r.nextInt(10000000);
            String s = randomNumber + "tmp.png";

            File f = new File(mFolder.getAbsolutePath(), s);

            strMyImagePath = f.getAbsolutePath();
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(f);
                scaledBitmap.compress(Bitmap.CompressFormat.JPEG, 500, fos);
                fos.flush();
                fos.close();
            } catch (FileNotFoundException e) {

                e.printStackTrace();
            } catch (Exception e) {

                e.printStackTrace();
            }

            scaledBitmap.recycle();
        } catch (Throwable e) {
        }

        if (strMyImagePath == null) {
            return path;
        }
        Log.e("ghjihjoihgtj", strMyImagePath);
        return strMyImagePath;

    }

    

   /*   public void showPictureDialog() {
        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(getActivity());
        builder.setTitle("Select Action");
        String[] pictureDialogItems = {"Select photo from gallery", "Capture image from camera"};

        builder.setItems(pictureDialogItems, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                switch (which) {

                    case 0:
                        // choosePhotoFromGallery();
                        AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                        editor.putString(AppsContants.ImageStatus, "c");
                        editor.commit();
                        startActivity(new Intent(getActivity(), MultiPhotoSelectActivity.class));
                        break;

                    case 1:
                        captureFromCamera();
                        break;
                }

            }
        });

        builder.show();
    }*/


    public void choosePhotoFromGallery() {

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, GALLERY);
    }


    public void captureFromCamera() {
        PackageManager packageManager = getActivity().getPackageManager();
        if (packageManager.hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            File mainDirectory = new File(Environment.getExternalStorageDirectory(), "MyFolder/tmp");
            if (!mainDirectory.exists())
                mainDirectory.mkdirs();

            Calendar calendar = Calendar.getInstance();
            uriFilePath = Uri.fromFile(new File(mainDirectory, "IMG_" + calendar.getTimeInMillis()));
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, uriFilePath);
            startActivityForResult(intent, 1);
        }
    }

/*
    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (uriFilePath != null)
            outState.putString("uri_file_path", uriFilePath.toString());
        super.onSaveInstanceState(outState);
    }*/


  /*  @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }


        if (requestCode == 1) {
            String filePath = uriFilePath.getPath();
            mDetials123.add(filePath);
            Log.e("sdfsdf", "" + filePath);
        }

    }
*/

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == getActivity().RESULT_CANCELED) {
            return;
        }
        mDetials123 = new ArrayList<>();
        switch (requestCode) {
            case Constants.TYPE_MULTI_PICKER:
                ArrayList<Image> imagesList = data.getParcelableArrayListExtra(Constants.KEY_BUNDLE_LIST);
                for (int i = 0; i < imagesList.size(); i++) {


                    // ComplaintFragment.mDetials123.add(decodeFile(selectedItems.get(k), 150, 150));
                    // mDetials123.add(decodeFile(imagesList.get(i).imagePath, 500, 500));
                    mDetials123.add(imagesList.get(i).imagePath);
                    Log.e("SDFsfsfsdf", imagesList.get(i).imagePath);

                }

                if (mDetials123.size() != 0) {

                    image_recycle.setLayoutManager(new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.HORIZONTAL));
                    giftadapter = new GiftAdapter(getActivity(), mDetials123, "0");
                    image_recycle.setAdapter(giftadapter);
                    image_recycle.setVisibility(View.VISIBLE);
                    txtUploadHint.setVisibility(View.VISIBLE);

                } else {
                    image_recycle.setVisibility(View.GONE);
                    txtUploadHint.setVisibility(View.GONE);
                }
                break;
        }
       /* if (requestCode == 1) {
            String filePath = uriFilePath.getPath();
            mDetials123.add(filePath);
            Log.e("sdfsdf", "" + filePath);
        }*/
    }
}