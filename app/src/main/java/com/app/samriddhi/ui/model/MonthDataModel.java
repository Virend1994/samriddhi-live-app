package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MonthDataModel extends BaseModel {
    @SerializedName("MTDdata")
    @Expose
    private ArrayList<MonthListModel> mTDdata = null;
    @SerializedName("todate")
    @Expose
    private String todate;
    @SerializedName("frmdate")
    @Expose
    private String frmdate;
    @SerializedName("reply")
    @Expose
    private String reply;

    public ArrayList<MonthListModel> getMTDdata() {
        return mTDdata;
    }

    public void setMTDdata(ArrayList<MonthListModel> mTDdata) {
        this.mTDdata = mTDdata;
    }

    public String getTodate() {
        return todate;
    }

    public void setTodate(String todate) {
        this.todate = todate;
    }

    public String getFrmdate() {
        return frmdate;
    }

    public void setFrmdate(String frmdate) {
        this.frmdate = frmdate;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }
}
