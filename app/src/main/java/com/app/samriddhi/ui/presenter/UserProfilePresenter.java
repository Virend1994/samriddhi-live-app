package com.app.samriddhi.ui.presenter;

import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.response.BaseResponse;
import com.app.samriddhi.api.response.JsonObjectResponse;
import com.app.samriddhi.base.BasePresenter;
import com.app.samriddhi.ui.model.UserProfileModel;
import com.app.samriddhi.ui.view.IUserProfileView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserProfilePresenter extends BasePresenter<IUserProfileView> {

    /**
     * get the agent profiles data
     * @param bp_code logged in user id
     */

    public void getUserProfile(String bp_code) {
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().getUserProfile(bp_code).enqueue(new Callback<JsonObjectResponse<UserProfileModel>>() {
            @Override
            public void onResponse(Call<JsonObjectResponse<UserProfileModel>> call, Response<JsonObjectResponse<UserProfileModel>> response) {
                getView().enableLoadingBar(false);
                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        if (response.body().body != null)
                            getView().onProfileSuccess(response.body().body, response.body().getIsedit());
                        else
                            getView().onError(response.body().replyMsg);
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObjectResponse<UserProfileModel>> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }

    /**
     * submit the users profile updated data
     * @param mData users prodile data
     */
    public void updateProfile(UserProfileModel mData) {
        getView().enableLoadingBar(true);

        SamriddhiApplication.getmInstance().getApiService().submitProfileData(mData).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                getView().enableLoadingBar(false);
                if (!handleError(response)) {
                    if (response.isSuccessful() && response.code() == 200) {
                        getView().onInfo(response.body().replyMsg);
                    }
                }

            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);

            }
        });
    }
}
