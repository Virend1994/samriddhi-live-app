package com.app.samriddhi.ui.activity.main.ui.OrderSupply.Adapter;

import static android.content.Context.MODE_PRIVATE;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.opengl.Visibility;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.samriddhi.R;
import com.app.samriddhi.prefernces.PreferenceManager;
import com.app.samriddhi.ui.activity.main.ui.AgencyClose.AppsContants;
import com.app.samriddhi.ui.activity.main.ui.AgencyCreationModule.ShowRequestListActivity;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.DailyPoActivity;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.POModal.DailyPOModal;
import com.app.samriddhi.ui.activity.main.ui.OrderSupply.PendingOrdersActivity;
import com.app.samriddhi.ui.model.DropDownModel;

import java.util.ArrayList;
import java.util.List;

public class PendingOrderAdapter extends RecyclerView.Adapter<PendingOrderAdapter.ViewHolder> implements Filterable {

    Context context;
    List<DailyPOModal> dataAdapters;
    private final List<DailyPOModal> searchArray;
    private final List<String> rName;
    private final List<String> rId;
    private final int allstatus;
    int total = 0;

    public PendingOrderAdapter(List<DailyPOModal> getDataAdapter, Context context, List<String> reasonName, List<String> reasonId, int allstatus) {


        super();
        this.dataAdapters = getDataAdapter;
        this.rName = reasonName;
        this.allstatus = allstatus;
        this.rId = reasonId;
        this.searchArray = new ArrayList<>();
        this.searchArray.addAll(dataAdapters);
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.pending_order_items, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, @SuppressLint("RecyclerView") final int position) {

        DailyPOModal dataAdapterOBJ = dataAdapters.get(position);

        // Viewholder.txtRouteCode.setText(dataAdapterOBJ.getRouteCode());

        Viewholder.txtMainEditionName.setText(dataAdapterOBJ.getCust_name() + " " + "(" + dataAdapterOBJ.getSold_to_party() + ")");
        // Viewholder.txtMainEditionName.setText(dataAdapterOBJ.getCust_name());
        Viewholder.tvEdtion.setText(dataAdapterOBJ.getEdi_name());
        Viewholder.tvCopyType.setText(dataAdapterOBJ.getCopies_type());
        Viewholder.tvSoldToParty.setText(dataAdapterOBJ.getSold_to_party());
        Viewholder.tvShipToParty.setText(dataAdapterOBJ.getShip_to_party());
        Viewholder.tvItemCat.setText(dataAdapterOBJ.getItem_cat());
        Viewholder.tvRefQtd.setText(dataAdapterOBJ.getRef_qtd());
        Viewholder.editSaleQTD.setText(dataAdapterOBJ.getProposed_qty());
        Viewholder.txtEditSale.setText(dataAdapterOBJ.getProposed_qty());
        // Viewholder.editSaleQTD.setText("0");
        Viewholder.tvINCDEC.setText(dataAdapterOBJ.getInc_dsc());
        Viewholder.txtSaleQTD.setText(dataAdapterOBJ.getSale_qtd());


        Viewholder.editRemark.setText(dataAdapterOBJ.getRemark());

        if (dataAdapterOBJ.getOrderApproval().equals("0")&&dataAdapterOBJ.getRj_sh_flag().equals("1")) {

            if (allstatus == 1) {

                Viewholder.checkItem.setChecked(true);
                dataAdapters.get(position).setCheckedItem("1");
            } else {
                Viewholder.checkItem.setChecked(false);
                dataAdapters.get(position).setCheckedItem("0");
            }

            if(PreferenceManager.getPrefUserType(context).equals("SH")) {
                Viewholder.txtStatus.setText("Pending");
                Viewholder.txtStatus.setTextColor(context.getResources().getColor(R.color.yellow_button_color));
            }
            else {
                Viewholder.txtStatus.setText("Submitted for SH");
                Viewholder.txtStatus.setTextColor(context.getResources().getColor(R.color.red));
            }


           // Viewholder.txtStatus.setText("Pending");
            Viewholder.editSaleQTD.setVisibility(View.VISIBLE);
            Viewholder.txtEditSale.setVisibility(View.GONE);
        }

        else if (dataAdapterOBJ.getOrderApproval().equals("0")&&dataAdapterOBJ.getRj_sh_flag().equals("0")) {

            if (allstatus == 1) {

                Viewholder.checkItem.setChecked(true);
                dataAdapters.get(position).setCheckedItem("1");
            } else {
                Viewholder.checkItem.setChecked(false);
                dataAdapters.get(position).setCheckedItem("0");
            }

            Viewholder.txtStatus.setText("Pending");
            Viewholder.txtStatus.setTextColor(context.getResources().getColor(R.color.red));
            Viewholder.editSaleQTD.setVisibility(View.VISIBLE);
            Viewholder.txtEditSale.setVisibility(View.GONE);
        }
         else if (dataAdapterOBJ.getOrderApproval().equals("3")) {
            if(PreferenceManager.getPrefUserType(context).equals("SH")) {  /* For Rajasthan's SH only */
                AppsContants.sharedpreferences = context.getSharedPreferences(AppsContants.MyPREFERENCES, MODE_PRIVATE);
                String strStateFlag = AppsContants.sharedpreferences.getString(AppsContants.StateFlag, "");
                if (strStateFlag.equals("1")) {
                    if (allstatus == 1) {

                        Viewholder.checkItem.setChecked(true);
                        dataAdapters.get(position).setCheckedItem("1");
                    } else {
                        Viewholder.checkItem.setChecked(false);
                        dataAdapters.get(position).setCheckedItem("0");
                    }

                    Viewholder.txtStatus.setText("Pending");
                    Viewholder.txtStatus.setTextColor(context.getResources().getColor(R.color.red));
                    Viewholder.editSaleQTD.setVisibility(View.VISIBLE);
                    Viewholder.txtEditSale.setVisibility(View.GONE);

                }
            }
            else {

                 if (allstatus == 1) {

                        Viewholder.checkItem.setChecked(true);
                        dataAdapters.get(position).setCheckedItem("1");
                    } else {
                        Viewholder.checkItem.setChecked(false);
                        dataAdapters.get(position).setCheckedItem("0");
                    }

                    Viewholder.txtStatus.setText("Submitted");
                    Viewholder.txtStatus.setTextColor(context.getResources().getColor(R.color.red));
                    Viewholder.editSaleQTD.setVisibility(View.VISIBLE);
                    Viewholder.txtEditSale.setVisibility(View.GONE);
            }


        }

        else if (dataAdapterOBJ.getOrderApproval().equals("1")) {

            Viewholder.txtStatus.setText("Approved");
            Viewholder.txtStatus.setTextColor(context.getResources().getColor(R.color.primary_green));
            Viewholder.checkItem.setVisibility(View.GONE);
            Viewholder.editSaleQTD.setVisibility(View.GONE);
            Viewholder.txtEditSale.setVisibility(View.VISIBLE);
            if (allstatus == 1) {

                Viewholder.checkItem.setChecked(true);
                dataAdapters.get(position).setCheckedItem("1");
            } else {
                Viewholder.checkItem.setChecked(false);
                dataAdapters.get(position).setCheckedItem("0");
            }


        } else {

            if (allstatus == 1) {

                Viewholder.checkItem.setChecked(true);
                dataAdapters.get(position).setCheckedItem("1");
            } else {
                Viewholder.checkItem.setChecked(false);
                dataAdapters.get(position).setCheckedItem("0");
            }
            Viewholder.checkItem.setVisibility(View.GONE);
            Viewholder.editSaleQTD.setVisibility(View.GONE);
            Viewholder.txtEditSale.setVisibility(View.VISIBLE);
            Viewholder.txtStatus.setText("Rejected");
            Viewholder.txtStatus.setTextColor(context.getResources().getColor(R.color.red));


        }

        Viewholder.imgUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Viewholder.imgUp.setVisibility(View.GONE);
                Viewholder.imgDrop.setVisibility(View.VISIBLE);
                Viewholder.linDetail.setVisibility(View.GONE);
                if (context instanceof DailyPoActivity) {
                    ((DailyPoActivity) context).SetAdapter();
                }

            }
        });
        Viewholder.imgDrop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DailyPOModal dataAdapterOBJ = dataAdapters.get(position);
                Viewholder.imgDrop.setVisibility(View.GONE);
                Viewholder.imgUp.setVisibility(View.VISIBLE);
                Viewholder.linDetail.setVisibility(View.VISIBLE);
            }
        });

       /* if (dataAdapterOBJ.getItem_cat().equals("ZFOR")) {

            Viewholder.editSaleQTD.setEnabled(false);
            Viewholder.editSaleQTD.setText(dataAdapterOBJ.getSale_qtd());
            Viewholder.editSaleQTD.setBackground(context.getResources().getDrawable(R.drawable.edit_not_editable));
            Viewholder.spinReason.setBackground(context.getResources().getDrawable(R.drawable.edit_not_editable));
            Viewholder.tvINCDEC.setText("0");

            dataAdapters.get(position).setIs_approval("1");
            dataAdapters.get(position).setRemark("");
            dataAdapters.get(position).setProposed_qty("");


        } else {
            Viewholder.editSaleQTD.setEnabled(true);
            int actualCopies = Integer.parseInt(dataAdapterOBJ.getSale_qtd());
            int actualIncreasedCopies = Integer.parseInt(dataAdapterOBJ.getInc_dsc());

            total = actualCopies + actualIncreasedCopies;
            Viewholder.editSaleQTD.setText(total + "");

            int baseMin = Integer.parseInt(dataAdapterOBJ.getBase_min());
            int baseMax = Integer.parseInt(dataAdapterOBJ.getBase_max());
            int baseCopy = Integer.parseInt(dataAdapterOBJ.getBase_copy());

            if (total >= baseCopy) { *//* total 105 : baseCopy 100*//*
                if (total > baseMax) { *//* total 105 : baseMax 103*//*

                    Viewholder.linearWarning.setVisibility(View.VISIBLE);
                    Viewholder.txtBaseCopy.setText("Total base copies are:" + " " + baseCopy + "");
                    Viewholder.txtBaseMax.setText("Upper limit:" + " " + baseMax + "");
                    Viewholder.txtBaseMin.setText("Lower limit:" + " " + baseMin + "");

                    dataAdapters.get(position).setIs_approval("0");
                    dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                    dataAdapters.get(position).setProposed_qty(total + "");

                } else {
                    Viewholder.linearWarning.setVisibility(View.GONE);
                    dataAdapters.get(position).setIs_approval("1");
                    dataAdapters.get(position).setRemark("");
                    dataAdapters.get(position).setProposed_qty("");

                }

            } else {*//* total 98 : baseCopy 100*//*

                if (total < baseMin) { *//* total 94 : baseMin 95*//*
                    Viewholder.linearWarning.setVisibility(View.VISIBLE);
                    Viewholder.txtBaseCopy.setText("Total base copies are:" + " " + baseCopy + "");
                    Viewholder.txtBaseMax.setText("Upper limit:" + " " + baseMax + "");
                    Viewholder.txtBaseMin.setText("Lower limit:" + " " + baseMin + "");
                    dataAdapters.get(position).setIs_approval("0");
                    dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                    dataAdapters.get(position).setProposed_qty(total + "");

                } else {
                    Viewholder.linearWarning.setVisibility(View.GONE);
                    dataAdapters.get(position).setIs_approval("1");
                    dataAdapters.get(position).setRemark("");
                    dataAdapters.get(position).setProposed_qty("");

                }
            }
        }*/

        Viewholder.btnSaveProposedQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Viewholder.btnSaveProposedQty.setVisibility(View.GONE);

                //do here your stuff f

                int baseMin = 0;
                int baseMax = 0;
                int baseCopy = 0;

                if (dataAdapterOBJ.getBase_min() != null && !dataAdapterOBJ.getBase_min().isEmpty() && !dataAdapterOBJ.getBase_min().equals("null")) {
                    baseMin = Integer.parseInt(dataAdapters.get(position).getBase_min());
                    baseMax = Integer.parseInt(dataAdapters.get(position).getBase_max());
                    baseCopy = Integer.parseInt(dataAdapters.get(position).getBase_copy());
                }


                if (view != null) {
                    InputMethodManager imm =
                            (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }


                String strData = Viewholder.editSaleQTD.getText().toString();
                if (strData.length() > 0) {
                    total = Integer.parseInt(strData);
                    int refQTD = 0;
                    int totalINC = 0;
                    int setMainval = 0;

                    if (dataAdapterOBJ.getProposed_qty() != null && !dataAdapterOBJ.getProposed_qty().isEmpty() && !dataAdapterOBJ.getProposed_qty().equals("null")) {


                        if (dataAdapterOBJ.getCopies_type().equals("JJ")) {

                            int getProposed = Integer.parseInt(dataAdapters.get(position).getStoreProposed());

                            if (total < getProposed) {
                                int getTotalSaleJJ = Integer.parseInt(dataAdapterOBJ.getTotalSaleJJ());
                                int toalDiff = getProposed - total;
                                Log.e("sdfsfasfa", toalDiff + "");
                                int getDecreaseValue = getTotalSaleJJ - toalDiff;
                                PendingOrdersActivity.txtSalesJJ.setText(getDecreaseValue + "");
                            } else if (total == getProposed) {
                                PendingOrdersActivity.txtSalesJJ.setText(dataAdapterOBJ.getTotalSaleJJ());
                            } else {
                                int getTotalSaleJJ = Integer.parseInt(dataAdapterOBJ.getTotalSaleJJ());
                                // int toalDiff = getProposed + total;
                                int toalDiff = total - getProposed;
                                Log.e("sdfsfasfa", toalDiff + "");
                                int getDecreaseValue = getTotalSaleJJ + toalDiff;
                                PendingOrdersActivity.txtSalesJJ.setText(getDecreaseValue + "");
                            }

                        } else {

                            int getProposed = Integer.parseInt(dataAdapters.get(position).getStoreProposed());

                            if (total < getProposed) {
                                // int getTotalSaleJJ = Integer.parseInt(dataAdapterOBJ.getTotalSaleJJ());
                                int getTotalSaleJJ = Integer.parseInt(dataAdapterOBJ.getTotalSaleMain());
                                int toalDiff = getProposed - total;
                                Log.e("sdfsfasfa", toalDiff + "");
                                int getDecreaseValue = getTotalSaleJJ - toalDiff;
                                PendingOrdersActivity.txtSalesMain.setText(getDecreaseValue + "");
                            } else if (total == getProposed) {
                                PendingOrdersActivity.txtSalesMain.setText(dataAdapterOBJ.getTotalSaleMain());
                            } else {
                                int getTotalSaleJJ = Integer.parseInt(dataAdapterOBJ.getTotalSaleMain());
                                // int toalDiff = getProposed + total;
                                int toalDiff = total - getProposed;
                                Log.e("sdfsfasfa", toalDiff + "");
                                int getDecreaseValue = getTotalSaleJJ + toalDiff;
                                PendingOrdersActivity.txtSalesMain.setText(getDecreaseValue + "");
                            }

                        }

                        refQTD = Integer.parseInt(dataAdapterOBJ.getSale_qtd());
                        totalINC = total - refQTD;
                        dataAdapters.get(position).setProposed_qty(total + "");
                        dataAdapters.get(position).setIncreament(totalINC + "");
                        dataAdapters.get(position).setApprovalINCDEC(totalINC + "");
                        //  Viewholder.tvINCDEC.setText(totalINC + "");



                           /* if(dataAdapterOBJ.getDiffStatus().equals("1")){

                                int tot= Integer.parseInt(dataAdapterOBJ.getMainSaleTotal());
                                int finalTotal=0;
                                for (int i = 0; i <dataAdapters.size() ; i++) {
                                    String var=dataAdapters.get(i).getIncreament();
                                    int getVal= Integer.parseInt(var);
                                    finalTotal+=getVal;
                                }

                                int sum=tot+finalTotal;
                                int txtRefMain = Integer.parseInt(PendingOrdersActivity.txtRefMain.getText().toString());
                                int txtRefTotal = Integer.parseInt(PendingOrdersActivity.txtRefTotal.getText().toString());
                                PendingOrdersActivity.txtSalesMain.setText(sum+"");
                                PendingOrdersActivity.txtSaleTotal.setText(sum+"");

                                int mainDiff=sum-txtRefMain;
                                int CopiesDiff=sum-txtRefTotal;
                                PendingOrdersActivity.txtMainDiff.setText(mainDiff+"");
                                PendingOrdersActivity.txtTotalCopiesDiff.setText(CopiesDiff+"");

                            }*/


                    }

                    if (total >= baseCopy) {
                        if (total > baseMax) {

                            Viewholder.linearWarning.setVisibility(View.VISIBLE);
                            Viewholder.txtBaseCopy.setText("Total base copies are:" + " " + baseCopy + "");
                            Viewholder.txtBaseMax.setText("Upper limit:" + " " + baseMax + "");
                            Viewholder.txtBaseMin.setText("Lower limit:" + " " + baseMin + "");
                            dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                            dataAdapters.get(position).setProposed_qty(total + "");


                        } else {
                            Viewholder.linearWarning.setVisibility(View.GONE);
                            dataAdapters.get(position).setIs_approval("1");
                            dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                            dataAdapters.get(position).setProposed_qty(total + "");

                        }

                    } else {

                        if (total < baseMin) {
                            Viewholder.linearWarning.setVisibility(View.VISIBLE);
                            Viewholder.txtBaseCopy.setText("Total base copies are:" + " " + baseCopy + "");
                            Viewholder.txtBaseMax.setText("Upper limit:" + " " + baseMax + "");
                            Viewholder.txtBaseMin.setText("Lower limit:" + " " + baseMin + "");
                            dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                            dataAdapters.get(position).setProposed_qty(total + "");


                        } else {
                            Viewholder.linearWarning.setVisibility(View.GONE);
                            dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                            dataAdapters.get(position).setProposed_qty(total + "");
                        }
                    }

                } else {
                    total = 0;
                    if (total >= baseCopy) {
                        if (total > baseMax) {

                            Viewholder.linearWarning.setVisibility(View.VISIBLE);
                            Viewholder.txtBaseCopy.setText("Total base copies are:" + " " + baseCopy + "");
                            Viewholder.txtBaseMax.setText("Upper limit:" + " " + baseMax + "");
                            Viewholder.txtBaseMin.setText("Lower limit:" + " " + baseMin + "");
                            dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                            dataAdapters.get(position).setProposed_qty(total + "");


                        } else {

                            Viewholder.linearWarning.setVisibility(View.GONE);
                            dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                            dataAdapters.get(position).setProposed_qty(total + "");


                        }

                    } else {

                        if (total < baseMin) {
                            Viewholder.linearWarning.setVisibility(View.VISIBLE);
                            Viewholder.txtBaseCopy.setText("Total base copies are:" + " " + baseCopy + "");
                            Viewholder.txtBaseMax.setText("Upper limit:" + " " + baseMax + "");
                            Viewholder.txtBaseMin.setText("Lower limit:" + " " + baseMin + "");
                            dataAdapters.get(position).setIs_approval("0");
                            dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                            dataAdapters.get(position).setProposed_qty(total + "");


                        } else {
                            Viewholder.linearWarning.setVisibility(View.GONE);
                            dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                            dataAdapters.get(position).setProposed_qty(total + "");
                        }
                    }
                }

                dataAdapters.get(position).setProposed_qty(total + "");


                if (context instanceof PendingOrdersActivity) {

                    dataAdapters.get(position).setChangeStatus("1");

                    ((PendingOrdersActivity) context).GetINCDECValues();
                }

                if (context instanceof PendingOrdersActivity) {
                    ((PendingOrdersActivity) context).UpldatePoDiff();
                }


            }
        });
//        Viewholder.editSaleQTD.setOnEditorActionListener(new EditText.OnEditorActionListener() {
//            @Override
//
//            public boolean onEditorAction(TextView vvvvv, int actionId, KeyEvent event) {
//                if (actionId == EditorInfo.IME_ACTION_DONE) {
//                    //do here your stuff f
//
//                    int baseMin = 0;
//                    int baseMax = 0;
//                    int baseCopy = 0;
//
//                    if (dataAdapterOBJ.getBase_min() != null && !dataAdapterOBJ.getBase_min().isEmpty() && !dataAdapterOBJ.getBase_min().equals("null")) {
//                        baseMin = Integer.parseInt(dataAdapters.get(position).getBase_min());
//                        baseMax = Integer.parseInt(dataAdapters.get(position).getBase_max());
//                        baseCopy = Integer.parseInt(dataAdapters.get(position).getBase_copy());
//                    }
//
//                    if (vvvvv != null) {
//                        InputMethodManager imm =
//                                (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
//                        imm.hideSoftInputFromWindow(vvvvv.getWindowToken(), 0);
//                    }
//
//
//                    String strData = Viewholder.editSaleQTD.getText().toString();
//                    if (strData.length() > 0) {
//
//                        total = Integer.parseInt(strData);
//                        int refQTD = 0;
//                        int totalINC = 0;
//                        int setMainval = 0;
//
//                        if (dataAdapterOBJ.getProposed_qty() != null && !dataAdapterOBJ.getProposed_qty().isEmpty() && !dataAdapterOBJ.getProposed_qty().equals("null")) {
//
//
//                            if (dataAdapterOBJ.getCopies_type().equals("JJ")) {
//
//                                int getProposed = Integer.parseInt(dataAdapters.get(position).getStoreProposed());
//
//                                if (total < getProposed) {
//                                    int getTotalSaleJJ = Integer.parseInt(dataAdapterOBJ.getTotalSaleJJ());
//                                    int toalDiff = getProposed - total;
//                                    Log.e("sdfsfasfa",toalDiff+"");
//                                    int getDecreaseValue = getTotalSaleJJ - toalDiff;
//                                    PendingOrdersActivity.txtSalesJJ.setText(getDecreaseValue + "");
//                                } else if (total == getProposed) {
//                                    PendingOrdersActivity.txtSalesJJ.setText(dataAdapterOBJ.getTotalSaleJJ());
//                                } else {
//                                    int getTotalSaleJJ = Integer.parseInt(dataAdapterOBJ.getTotalSaleJJ());
//                                   // int toalDiff = getProposed + total;
//                                    int toalDiff = total - getProposed;
//                                    Log.e("sdfsfasfa",toalDiff+"");
//                                    int getDecreaseValue = getTotalSaleJJ + toalDiff;
//                                    PendingOrdersActivity.txtSalesJJ.setText(getDecreaseValue + "");
//                                }
//
//                            }
//                            else {
//
//                                int getProposed = Integer.parseInt(dataAdapters.get(position).getStoreProposed());
//
//                                if (total < getProposed) {
//                                   // int getTotalSaleJJ = Integer.parseInt(dataAdapterOBJ.getTotalSaleJJ());
//                                    int getTotalSaleJJ = Integer.parseInt(dataAdapterOBJ.getTotalSaleMain());
//                                    int toalDiff = getProposed - total;
//                                    Log.e("sdfsfasfa",toalDiff+"");
//                                    int getDecreaseValue = getTotalSaleJJ - toalDiff;
//                                    PendingOrdersActivity.txtSalesMain.setText(getDecreaseValue + "");
//                                } else if (total == getProposed) {
//                                    PendingOrdersActivity.txtSalesMain.setText(dataAdapterOBJ.getTotalSaleMain());
//                                } else {
//                                    int getTotalSaleJJ = Integer.parseInt(dataAdapterOBJ.getTotalSaleMain());
//                                    // int toalDiff = getProposed + total;
//                                    int toalDiff = total - getProposed;
//                                    Log.e("sdfsfasfa",toalDiff+"");
//                                    int getDecreaseValue = getTotalSaleJJ + toalDiff;
//                                    PendingOrdersActivity.txtSalesMain.setText(getDecreaseValue + "");
//                                }
//
//                            }
//
//                            refQTD = Integer.parseInt(dataAdapterOBJ.getSale_qtd());
//                            totalINC = total - refQTD;
//                            dataAdapters.get(position).setProposed_qty(total + "");
//                            dataAdapters.get(position).setIncreament(totalINC + "");
//                            dataAdapters.get(position).setApprovalINCDEC(totalINC + "");
//                          //  Viewholder.tvINCDEC.setText(totalINC + "");
//
//
//
//                           /* if(dataAdapterOBJ.getDiffStatus().equals("1")){
//
//                                int tot= Integer.parseInt(dataAdapterOBJ.getMainSaleTotal());
//                                int finalTotal=0;
//                                for (int i = 0; i <dataAdapters.size() ; i++) {
//                                    String var=dataAdapters.get(i).getIncreament();
//                                    int getVal= Integer.parseInt(var);
//                                    finalTotal+=getVal;
//                                }
//
//                                int sum=tot+finalTotal;
//                                int txtRefMain = Integer.parseInt(PendingOrdersActivity.txtRefMain.getText().toString());
//                                int txtRefTotal = Integer.parseInt(PendingOrdersActivity.txtRefTotal.getText().toString());
//                                PendingOrdersActivity.txtSalesMain.setText(sum+"");
//                                PendingOrdersActivity.txtSaleTotal.setText(sum+"");
//
//                                int mainDiff=sum-txtRefMain;
//                                int CopiesDiff=sum-txtRefTotal;
//                                PendingOrdersActivity.txtMainDiff.setText(mainDiff+"");
//                                PendingOrdersActivity.txtTotalCopiesDiff.setText(CopiesDiff+"");
//
//                            }*/
//
//
//                        }
//
//                        if (total >= baseCopy) {
//                            if (total > baseMax) {
//
//                                Viewholder.linearWarning.setVisibility(View.VISIBLE);
//                                Viewholder.txtBaseCopy.setText("Total base copies are:" + " " + baseCopy + "");
//                                Viewholder.txtBaseMax.setText("Upper limit:" + " " + baseMax + "");
//                                Viewholder.txtBaseMin.setText("Lower limit:" + " " + baseMin + "");
//                                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
//                                dataAdapters.get(position).setProposed_qty(total + "");
//
//
//                            } else {
//                                Viewholder.linearWarning.setVisibility(View.GONE);
//                                dataAdapters.get(position).setIs_approval("1");
//                                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
//                                dataAdapters.get(position).setProposed_qty(total + "");
//
//                            }
//
//                        } else {
//
//                            if (total < baseMin) {
//                                Viewholder.linearWarning.setVisibility(View.VISIBLE);
//                                Viewholder.txtBaseCopy.setText("Total base copies are:" + " " + baseCopy + "");
//                                Viewholder.txtBaseMax.setText("Upper limit:" + " " + baseMax + "");
//                                Viewholder.txtBaseMin.setText("Lower limit:" + " " + baseMin + "");
//                                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
//                                dataAdapters.get(position).setProposed_qty(total + "");
//
//
//                            } else {
//                                Viewholder.linearWarning.setVisibility(View.GONE);
//                                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
//                                dataAdapters.get(position).setProposed_qty(total + "");
//                            }
//                        }
//
//                    } else {
//                        total = 0;
//                        if (total >= baseCopy) {
//                            if (total > baseMax) {
//
//                                Viewholder.linearWarning.setVisibility(View.VISIBLE);
//                                Viewholder.txtBaseCopy.setText("Total base copies are:" + " " + baseCopy + "");
//                                Viewholder.txtBaseMax.setText("Upper limit:" + " " + baseMax + "");
//                                Viewholder.txtBaseMin.setText("Lower limit:" + " " + baseMin + "");
//                                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
//                                dataAdapters.get(position).setProposed_qty(total + "");
//
//
//                            } else {
//
//                                Viewholder.linearWarning.setVisibility(View.GONE);
//                                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
//                                dataAdapters.get(position).setProposed_qty(total + "");
//
//
//                            }
//
//                        } else {
//
//                            if (total < baseMin) {
//                                Viewholder.linearWarning.setVisibility(View.VISIBLE);
//                                Viewholder.txtBaseCopy.setText("Total base copies are:" + " " + baseCopy + "");
//                                Viewholder.txtBaseMax.setText("Upper limit:" + " " + baseMax + "");
//                                Viewholder.txtBaseMin.setText("Lower limit:" + " " + baseMin + "");
//                                dataAdapters.get(position).setIs_approval("0");
//                                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
//                                dataAdapters.get(position).setProposed_qty(total + "");
//
//
//                            } else {
//                                Viewholder.linearWarning.setVisibility(View.GONE);
//                                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
//                                dataAdapters.get(position).setProposed_qty(total + "");
//                            }
//                        }
//                    }
//
//                    dataAdapters.get(position).setProposed_qty(total + "");
//
//
//                    if (context instanceof PendingOrdersActivity) {
//
//                        dataAdapters.get(position).setChangeStatus("1");
//
//                        ((PendingOrdersActivity) context).GetINCDECValues();
//                    }
//
//                    if (context instanceof PendingOrdersActivity) {
//                        ((PendingOrdersActivity)context).UpldatePoDiff();
//                    }
//                    return true;
//                }
//                return false;
//            }
//        });


//        int baseMin = Integer.parseInt(dataAdapterOBJ.getBase_min());
//        int baseMax = Integer.parseInt(dataAdapterOBJ.getBase_max());
//        int baseCopy = Integer.parseInt(dataAdapterOBJ.getBase_copy());

       /* Viewholder.editSaleQTD.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    //do here your stuff f
                    String strData = Viewholder.editSaleQTD.getText().toString();
                    Log.e("dfhdhd", strData);

                    if (strData.length() > 0) {

                        total = Integer.parseInt(strData);
                        if (total >= baseCopy) { *//* total 105 : baseCopy 100*//*
                            if (total > baseMax) { *//* total 105 : baseMax 103*//*

                                Viewholder.linearWarning.setVisibility(View.VISIBLE);
                                Viewholder.txtBaseCopy.setText("Total base copies are:" + " " + baseCopy + "");
                                Viewholder.txtBaseMax.setText("Upper limit:" + " " + baseMax + "");
                                Viewholder.txtBaseMin.setText("Lower limit:" + " " + baseMin + "");
                                dataAdapters.get(position).setIs_approval("0");
                                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                                dataAdapters.get(position).setProposed_qty(total + "");

                            } else {
                                Viewholder.linearWarning.setVisibility(View.GONE);
                                dataAdapters.get(position).setIs_approval("1");
                                dataAdapters.get(position).setRemark("");
                                dataAdapters.get(position).setProposed_qty("");

                            }

                        } else {*//* total 98 : baseCopy 100*//*

                            if (total < baseMin) { *//* total 94 : baseMin 95*//*
                                Viewholder.linearWarning.setVisibility(View.VISIBLE);
                                Viewholder.txtBaseCopy.setText("Total base copies are:" + " " + baseCopy + "");
                                Viewholder.txtBaseMax.setText("Upper limit:" + " " + baseMax + "");
                                Viewholder.txtBaseMin.setText("Lower limit:" + " " + baseMin + "");
                                dataAdapters.get(position).setIs_approval("0");
                                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                                dataAdapters.get(position).setProposed_qty(total + "");

                            } else {
                                Viewholder.linearWarning.setVisibility(View.GONE);
                                dataAdapters.get(position).setIs_approval("1");
                                dataAdapters.get(position).setRemark("");
                                dataAdapters.get(position).setProposed_qty("");

                            }
                        }

                    } else {
                        total = 0;
                        if (total >= baseCopy) { *//* total 105 : baseCopy 100*//*
                            if (total > baseMax) { *//* total 105 : baseMax 103*//*

                                Viewholder.linearWarning.setVisibility(View.VISIBLE);
                                Viewholder.txtBaseCopy.setText("Total base copies are:" + " " + baseCopy + "");
                                Viewholder.txtBaseMax.setText("Upper limit:" + " " + baseMax + "");
                                Viewholder.txtBaseMin.setText("Lower limit:" + " " + baseMin + "");
                                dataAdapters.get(position).setIs_approval("0");
                                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                                dataAdapters.get(position).setProposed_qty(total + "");

                            } else {
                                Viewholder.linearWarning.setVisibility(View.GONE);
                                dataAdapters.get(position).setIs_approval("1");
                                dataAdapters.get(position).setRemark("");
                                dataAdapters.get(position).setProposed_qty("");

                            }

                        } else {*//* total 98 : baseCopy 100*//*

                            if (total < baseMin) { *//* total 94 : baseMin 95*//*
                                Viewholder.linearWarning.setVisibility(View.VISIBLE);
                                Viewholder.txtBaseCopy.setText("Total base copies are:" + " " + baseCopy + "");
                                Viewholder.txtBaseMax.setText("Upper limit:" + " " + baseMax + "");
                                Viewholder.txtBaseMin.setText("Lower limit:" + " " + baseMin + "");
                                dataAdapters.get(position).setIs_approval("0");
                                dataAdapters.get(position).setRemark(Viewholder.editRemark.getText().toString());
                                dataAdapters.get(position).setProposed_qty(total + "");

                            } else {
                                Viewholder.linearWarning.setVisibility(View.GONE);
                                dataAdapters.get(position).setIs_approval("1");
                                dataAdapters.get(position).setRemark("");
                                dataAdapters.get(position).setProposed_qty("");

                            }
                        }
                    }
                    return true;
                }
                return false;
            }
        });*/

        if (!dataAdapterOBJ.getInc_dsc().equals("0")) {


            Viewholder.editSaleQTD.setBackground(context.getResources().getDrawable(R.drawable.border_green));

        } else {

            Viewholder.editSaleQTD.setBackground(context.getResources().getDrawable(R.drawable.border));
        }

        Viewholder.spinReason.setBackground(context.getResources().getDrawable(R.drawable.border));


        Viewholder.checkItem.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {
                    Log.e("sfasfasa", "Checked");
                    dataAdapters.get(position).setCheckedItem("1");
                } else {
                    Log.e("sfasfasa", "unchecked");
                    dataAdapters.get(position).setCheckedItem("0");
                }
            }
        });

        ArrayAdapter aa = new ArrayAdapter(context, android.R.layout.simple_spinner_item, rName);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        Viewholder.spinReason.setAdapter(aa);
        int setPos = 0;

        if (!dataAdapterOBJ.getReason_qty_change().equals("false")) {
            try {
                setPos = Integer.parseInt(dataAdapterOBJ.getReason_qty_change());
                Log.e("dgsdgsd", setPos + "");

                if (setPos == 23) {

                    Viewholder.spinReason.setSelection(setPos - 1);
                    Viewholder.txtReason.setText(rName.get(setPos - 1));
                } else {
                    Viewholder.spinReason.setSelection(setPos);
                    Viewholder.txtReason.setText(rName.get(setPos));

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Viewholder.spinReason.setSelection(0);
            Viewholder.txtReason.setText(rName.get(0));
        }

        // Viewholder.spinReason.setSelection(setPos-1);

        try {
            dataAdapters.get(position).setReason_qty_change(rId.get(setPos));
        } catch (Exception e) {
            e.printStackTrace();
        }


        Viewholder.editSaleQTD.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                Viewholder.btnSaveProposedQty.setVisibility(View.VISIBLE);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {

                // TODO Auto-generated method stub
            }
        });
       /* Viewholder.spinReason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int positions, long id) {

                Log.e("sdgsdg", rId.get(positions));
                Viewholder.txtReason.setText(rName.get(position));

                try {
                    dataAdapters.get(position).setReason_qty_change(rId.get(positions));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/


    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    public ArrayList<DailyPOModal> getArrayData() {
        return (ArrayList<DailyPOModal>) dataAdapters;
    }


    class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout linDetail, linearWarning;
        ImageView imgUp;
        ImageView imgDrop;
        EditText editSaleQTD;
        Button btnSaveProposedQty;
        TextView tvEdtion, txtReason, tvCopyType, txtStatus, tvSoldToParty, tvShipToParty, tvItemCat, tvRefQtd, tvINCDEC, txtMainEditionName;
        TextView txtBaseCopy, txtSaleQTD, txtBaseMax, txtBaseMin, txtEditSale;
        EditText editRemark;
        Spinner spinReason;
        CheckBox checkItem;

        public ViewHolder(View itemView) {
            super(itemView);
            imgUp = itemView.findViewById(R.id.imgUp);
            imgDrop = itemView.findViewById(R.id.imgDrop);
            linDetail = itemView.findViewById(R.id.linDetail);
            checkItem = itemView.findViewById(R.id.checkItem);

            tvEdtion = itemView.findViewById(R.id.tvEdtion);
            tvCopyType = itemView.findViewById(R.id.tvCopyType);
            tvSoldToParty = itemView.findViewById(R.id.tvSoldToParty);
            tvShipToParty = itemView.findViewById(R.id.tvShipToParty);
            tvItemCat = itemView.findViewById(R.id.tvItemCat);
            tvRefQtd = itemView.findViewById(R.id.tvRefQtd);
            txtStatus = itemView.findViewById(R.id.txtStatus);
            editSaleQTD = itemView.findViewById(R.id.editSaleQTD);
            btnSaveProposedQty = itemView.findViewById(R.id.btn_save_proposed_qty);
            tvINCDEC = itemView.findViewById(R.id.tvINCDEC);
            editRemark = itemView.findViewById(R.id.editRemark);
            spinReason = itemView.findViewById(R.id.spinReason);
            txtBaseCopy = itemView.findViewById(R.id.txtBaseCopy);
            txtBaseMin = itemView.findViewById(R.id.txtBaseMin);
            txtBaseMax = itemView.findViewById(R.id.txtBaseMax);
            linearWarning = itemView.findViewById(R.id.linearWarning);
            txtMainEditionName = itemView.findViewById(R.id.txtMainEditionName);
            txtSaleQTD = itemView.findViewById(R.id.txtSaleQTD);
            txtReason = itemView.findViewById(R.id.txtReason);
            txtEditSale = itemView.findViewById(R.id.txtEditSale);
        }
    }


    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String filterString = charSequence.toString().toLowerCase();
                //arraylist1.clear();
                FilterResults results = new FilterResults();
                int count = searchArray.size();
                final List<DailyPOModal> list = new ArrayList<>();

                DailyPOModal filterableString;

                for (int i = 0; i < count; i++) {
                    filterableString = searchArray.get(i);
                    if (searchArray.get(i).getEdi_name().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                    } else if (searchArray.get(i).getSold_to_party().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                    } else if (searchArray.get(i).getShip_to_party().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                    } else if (searchArray.get(i).getCust_name().toLowerCase().contains(filterString)) {
                        list.add(filterableString);
                    } else {


                    }
                }

                results.values = list;
                results.count = list.size();

                return results;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                dataAdapters = (List<DailyPOModal>) filterResults.values;
                notifyDataSetChanged();
            }
        };


    }

}
