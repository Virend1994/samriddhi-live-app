package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GrievanceCreateIncidentModel extends BaseModel {

    public boolean isReply;
    @SerializedName("Bp_code")
    @Expose
    private String bpCode;
    @SerializedName("device_id")
    @Expose
    private String deviceId;
    @SerializedName("Incident_Cat_Code")
    @Expose
    private String incidentCatCode;
    @SerializedName("Incident_Status_Code")
    @Expose
    private String incidentStatusCode;
    @SerializedName("incident_feedback_text")
    @Expose
    private String incidentFeedbackText;
    @SerializedName("Incident_Text")
    @Expose
    private String incidentText;
    @SerializedName("Incident_Number")
    @Expose
    private String incidentNumber;
    @SerializedName("incident_rating")
    @Expose
    private String incidentRating;
    @SerializedName("incident_comment_text")
    @Expose
    private String incidentCommentText;


    /////Incident reply constructor body for internal users case
    public GrievanceCreateIncidentModel(String bpCode, String deviceId, String incidentStatusCode, String incidentNumber, String incidentCommentText, boolean isReply) {
        this.bpCode = bpCode;
        this.deviceId = deviceId;
        this.incidentStatusCode = incidentStatusCode;
        this.incidentNumber = incidentNumber;
        this.incidentCommentText = incidentCommentText;
        this.isReply = isReply;
    }

    ///Incident close item update by users
    public GrievanceCreateIncidentModel(String bpCode, String deviceId, String incidentStatusCode, String incidentFeedbackText, String incidentNumber, String incidentRating) {
        this.bpCode = bpCode;
        this.deviceId = deviceId;
        this.incidentStatusCode = incidentStatusCode;
        this.incidentFeedbackText = incidentFeedbackText;
        this.incidentNumber = incidentNumber;
        this.incidentRating = incidentRating;
    }


    //// Craete a new incident by the users
    public GrievanceCreateIncidentModel(String bpCode, String deviceId, String incidentCatCode, String incidentStatusCode, String incidentText) {
        this.bpCode = bpCode;
        this.deviceId = deviceId;
        this.incidentCatCode = incidentCatCode;
        this.incidentStatusCode = incidentStatusCode;
        this.incidentText = incidentText;
    }

    public String getBpCode() {
        return bpCode;
    }

    public void setBpCode(String bpCode) {
        this.bpCode = bpCode;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getIncidentCatCode() {
        return incidentCatCode;
    }

    public void setIncidentCatCode(String incidentCatCode) {
        this.incidentCatCode = incidentCatCode;
    }

    public String getIncidentStatusCode() {
        return incidentStatusCode;
    }

    public void setIncidentStatusCode(String incidentStatusCode) {
        this.incidentStatusCode = incidentStatusCode;
    }

    public String getIncidentText() {
        return incidentText;
    }

    public void setIncidentText(String incidentText) {
        this.incidentText = incidentText;
    }
}
