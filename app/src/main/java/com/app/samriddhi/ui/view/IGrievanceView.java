package com.app.samriddhi.ui.view;

import com.app.samriddhi.ui.model.GrievanceCategoryModel;
import com.app.samriddhi.ui.model.GrievanceModel;
import com.app.samriddhi.ui.model.GrievanceOpenItemListModel;
import com.app.samriddhi.ui.model.IncidentCommentData;
import com.app.samriddhi.ui.model.IncidentDataModel;

import java.util.List;

public interface IGrievanceView extends IView {
    void onCategoryGetSuccess(List<GrievanceModel> mGrievData);

    void onGetIncidentStatus(List<GrievanceCategoryModel> mGrievData);

    void onOpenListItemSuccess(List<GrievanceOpenItemListModel> arrData, String opencomplaint);

    void onIncidentSubItemsList(IncidentDataModel body, String complaintType);

    void onIncidentCommentsSuccess(List<IncidentCommentData> list);

    void onDataSubmitSuccess(String message, String task);

}
