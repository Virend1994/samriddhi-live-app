package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NavEdtnSet extends BaseModel {
    @SerializedName("results")
    @Expose
    private List<NavEditionList> results = null;

    public List<NavEditionList> getResults() {
        return results;
    }

    public void setResults(List<NavEditionList> results) {
        this.results = results;
    }
}
