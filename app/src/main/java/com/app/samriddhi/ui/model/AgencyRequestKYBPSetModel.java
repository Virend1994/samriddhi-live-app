package com.app.samriddhi.ui.model;

import java.util.List;

public class AgencyRequestKYBPSetModel {
    public int resi_city;
    public int resi_state;
    public int busi_city;
    public int busi_state;
    public String ag_maritial_id;
    public Object db_dept;
    public Object db_desig;
    public Object db_relation;
    public Object db_city;
    public String add_proof_type;
    public List<AgencyChildSet> Agency_Child_set;
    public List<AgencyOtherNewsPaperSet> Agency_OtherNewsPaper_set;
    public String ag_gender;
    public String ag_title;
    public String first_name;
    public String middle_name;
    public String last_name;

    public int ag_detail_id;
    public String agency_name;
    public String resi_houseno;
    public String resi_building;
    public String resi_street;
    public String resi_pincode;
    public String resi_mono;
    public String resi_watsup;
    public String resi_email;
    public String busi_houseno;
    public String busi_building;
    public String busi_street;
    public String busi_pincode;
    public String busi_mono;
    public String busi_watsup;
    public String busi_email;
    public String ag_religion;
    public Object ag_dob;
    public String spouse_name;
    public Object spouse_dob;
    public Object ag_marrige_ani;
    public int no_of_children;
    public String father_name;
    public Object father_dob;
    public String mother_name;
    public Object mother_dob;
    public String brother_name;
    public Object brother_dob;
    public String sister_name;
    public Object sister_dob;
    public String nominee_name;
    public String nominee_relation;
    public int other_nespaper_count;
    public String bank_holder_name;
    public String bank_acc_no;
    public String bank_name_branch;
    public String bank_address;
    public String bank_IFSC;
    public String GSTIN;
    public String ag_db_relative;
    public String db_gender;
    public String db_fullname;
    public Object db_empid;
    public String pan_no;
    public Object pan_copy;
    public String add_proof_no;
    public Object add_proof_copy;
    public String bank_copy_type;
    public Object bank_copy;
    public Object resi_location;

    public String getAg_title() {
        return ag_title;
    }

    public void setAg_title(String ag_title) {
        this.ag_title = ag_title;
    }

    public Object getResi_location() {
        return resi_location;
    }

    public void setResi_location(Object resi_location) {
        this.resi_location = resi_location;
    }

    public Object getDb_empid() {
        return db_empid;
    }

    public void setDb_empid(Object db_empid) {
        this.db_empid = db_empid;
    }

    public Object getDb_dept() {
        return db_dept;
    }

    public void setDb_dept(Object db_dept) {
        this.db_dept = db_dept;
    }

    public Object getDb_desig() {
        return db_desig;
    }

    public void setDb_desig(Object db_desig) {
        this.db_desig = db_desig;
    }

    public Object getDb_relation() {
        return db_relation;
    }

    public void setDb_relation(Object db_relation) {
        this.db_relation = db_relation;
    }

    public Object getDb_city() {
        return db_city;
    }

    public void setDb_city(Object db_city) {
        this.db_city = db_city;
    }

    public int getAg_detail_id() {
        return ag_detail_id;
    }

    public void setAg_detail_id(int ag_detail_id) {
        this.ag_detail_id = ag_detail_id;
    }

    public String getGSTIN() {
        return GSTIN;
    }

    public void setGSTIN(String GSTIN) {
        this.GSTIN = GSTIN;
    }

    public int getResi_city() {
        return resi_city;
    }

    public void setResi_city(int resi_city) {
        this.resi_city = resi_city;
    }

    public int getResi_state() {
        return resi_state;
    }

    public void setResi_state(int resi_state) {
        this.resi_state = resi_state;
    }

    public int getBusi_city() {
        return busi_city;
    }

    public void setBusi_city(int busi_city) {
        this.busi_city = busi_city;
    }

    public int getBusi_state() {
        return busi_state;
    }

    public void setBusi_state(int busi_state) {
        this.busi_state = busi_state;
    }

    public String getAg_maritial_id() {
        return ag_maritial_id;
    }

    public void setAg_maritial_id(String ag_maritial_id) {
        this.ag_maritial_id = ag_maritial_id;
    }





    public String getAdd_proof_type() {
        return add_proof_type;
    }

    public void setAdd_proof_type(String add_proof_type) {
        this.add_proof_type = add_proof_type;
    }

    public List<AgencyChildSet> getAgency_Child_set() {
        return Agency_Child_set;
    }

    public void setAgency_Child_set(List<AgencyChildSet> agency_Child_set) {
        this.Agency_Child_set = agency_Child_set;
    }

    public List<AgencyOtherNewsPaperSet> getAgency_OtherNewsPaper_set() {
        return Agency_OtherNewsPaper_set;
    }

    public void setAgency_OtherNewsPaper_set(List<AgencyOtherNewsPaperSet> agency_OtherNewsPaper_set) {
        this.Agency_OtherNewsPaper_set = agency_OtherNewsPaper_set;
    }

    public String getAg_gender() {
        return ag_gender;
    }

    public void setAg_gender(String ag_gender) {
        this.ag_gender = ag_gender;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getAgency_name() {
        return agency_name;
    }

    public void setAgency_name(String agency_name) {
        this.agency_name = agency_name;
    }

    public String getResi_houseno() {
        return resi_houseno;
    }

    public void setResi_houseno(String resi_houseno) {
        this.resi_houseno = resi_houseno;
    }

    public String getResi_building() {
        return resi_building;
    }

    public void setResi_building(String resi_building) {
        this.resi_building = resi_building;
    }

    public String getResi_street() {
        return resi_street;
    }

    public void setResi_street(String resi_street) {
        this.resi_street = resi_street;
    }

    public String getResi_pincode() {
        return resi_pincode;
    }

    public void setResi_pincode(String resi_pincode) {
        this.resi_pincode = resi_pincode;
    }

    public String getResi_mono() {
        return resi_mono;
    }

    public void setResi_mono(String resi_mono) {
        this.resi_mono = resi_mono;
    }

    public String getResi_watsup() {
        return resi_watsup;
    }

    public void setResi_watsup(String resi_watsup) {
        this.resi_watsup = resi_watsup;
    }

    public String getResi_email() {
        return resi_email;
    }

    public void setResi_email(String resi_email) {
        this.resi_email = resi_email;
    }

    public String getBusi_houseno() {
        return busi_houseno;
    }

    public void setBusi_houseno(String busi_houseno) {
        this.busi_houseno = busi_houseno;
    }

    public String getBusi_building() {
        return busi_building;
    }

    public void setBusi_building(String busi_building) {
        this.busi_building = busi_building;
    }

    public String getBusi_street() {
        return busi_street;
    }

    public void setBusi_street(String busi_street) {
        this.busi_street = busi_street;
    }

    public String getBusi_pincode() {
        return busi_pincode;
    }

    public void setBusi_pincode(String busi_pincode) {
        this.busi_pincode = busi_pincode;
    }

    public String getBusi_mono() {
        return busi_mono;
    }

    public void setBusi_mono(String busi_mono) {
        this.busi_mono = busi_mono;
    }

    public String getBusi_watsup() {
        return busi_watsup;
    }

    public void setBusi_watsup(String busi_watsup) {
        this.busi_watsup = busi_watsup;
    }

    public String getBusi_email() {
        return busi_email;
    }

    public void setBusi_email(String busi_email) {
        this.busi_email = busi_email;
    }

    public String getAg_religion() {
        return ag_religion;
    }

    public void setAg_religion(String ag_religion) {
        this.ag_religion = ag_religion;
    }

    public Object getAg_dob() {
        return ag_dob;
    }

    public void setAg_dob(Object ag_dob) {
        this.ag_dob = ag_dob;
    }

    public String getSpouse_name() {
        return spouse_name;
    }

    public void setSpouse_name(String spouse_name) {
        this.spouse_name = spouse_name;
    }

    public Object getSpouse_dob() {
        return spouse_dob;
    }

    public void setSpouse_dob(Object spouse_dob) {
        this.spouse_dob = spouse_dob;
    }

    public Object getAg_marrige_ani() {
        return ag_marrige_ani;
    }

    public void setAg_marrige_ani(Object ag_marrige_ani) {
        this.ag_marrige_ani = ag_marrige_ani;
    }

    public int getNo_of_children() {
        return no_of_children;
    }

    public void setNo_of_children(int no_of_children) {
        this.no_of_children = no_of_children;
    }

    public String getFather_name() {
        return father_name;
    }

    public void setFather_name(String father_name) {
        this.father_name = father_name;
    }

    public Object getFather_dob() {
        return father_dob;
    }

    public void setFather_dob(Object father_dob) {
        this.father_dob = father_dob;
    }

    public String getMother_name() {
        return mother_name;
    }

    public void setMother_name(String mother_name) {
        this.mother_name = mother_name;
    }

    public Object getMother_dob() {
        return mother_dob;
    }

    public void setMother_dob(Object mother_dob) {
        this.mother_dob = mother_dob;
    }

    public String getBrother_name() {
        return brother_name;
    }

    public void setBrother_name(String brother_name) {
        this.brother_name = brother_name;
    }

    public Object getBrother_dob() {
        return brother_dob;
    }

    public void setBrother_dob(Object brother_dob) {
        this.brother_dob = brother_dob;
    }

    public String getSister_name() {
        return sister_name;
    }

    public void setSister_name(String sister_name) {
        this.sister_name = sister_name;
    }

    public Object getSister_dob() {
        return sister_dob;
    }

    public void setSister_dob(Object sister_dob) {
        this.sister_dob = sister_dob;
    }

    public String getNominee_name() {
        return nominee_name;
    }

    public void setNominee_name(String nominee_name) {
        this.nominee_name = nominee_name;
    }

    public String getNominee_relation() {
        return nominee_relation;
    }

    public void setNominee_relation(String nominee_relation) {
        this.nominee_relation = nominee_relation;
    }

    public int getOther_nespaper_count() {
        return other_nespaper_count;
    }

    public void setOther_nespaper_count(int other_nespaper_count) {
        this.other_nespaper_count = other_nespaper_count;
    }

    public String getBank_holder_name() {
        return bank_holder_name;
    }

    public void setBank_holder_name(String bank_holder_name) {
        this.bank_holder_name = bank_holder_name;
    }

    public String getBank_acc_no() {
        return bank_acc_no;
    }

    public void setBank_acc_no(String bank_acc_no) {
        this.bank_acc_no = bank_acc_no;
    }

    public String getBank_name_branch() {
        return bank_name_branch;
    }

    public void setBank_name_branch(String bank_name_branch) {
        this.bank_name_branch = bank_name_branch;
    }

    public String getBank_address() {
        return bank_address;
    }

    public void setBank_address(String bank_address) {
        this.bank_address = bank_address;
    }

    public String getBank_IFSC() {
        return bank_IFSC;
    }

    public void setBank_IFSC(String bank_IFSC) {
        this.bank_IFSC = bank_IFSC;
    }

    public String getgSTIN() {
        return GSTIN;
    }

    public void setgSTIN(String gSTIN) {
        this.GSTIN = gSTIN;
    }

    public String getAg_db_relative() {
        return ag_db_relative;
    }

    public void setAg_db_relative(String ag_db_relative) {
        this.ag_db_relative = ag_db_relative;
    }

    public String getDb_gender() {
        return db_gender;
    }

    public void setDb_gender(String db_gender) {
        this.db_gender = db_gender;
    }

    public String getDb_fullname() {
        return db_fullname;
    }

    public void setDb_fullname(String db_fullname) {
        this.db_fullname = db_fullname;
    }



    public String getPan_no() {
        return pan_no;
    }

    public void setPan_no(String pan_no) {
        this.pan_no = pan_no;
    }

    public Object getPan_copy() {
        return pan_copy;
    }

    public void setPan_copy(Object pan_copy) {
        this.pan_copy = pan_copy;
    }

    public String getAdd_proof_no() {
        return add_proof_no;
    }

    public void setAdd_proof_no(String add_proof_no) {
        this.add_proof_no = add_proof_no;
    }

    public Object getAdd_proof_copy() {
        return add_proof_copy;
    }

    public void setAdd_proof_copy(Object add_proof_copy) {
        this.add_proof_copy = add_proof_copy;
    }

    public String getBank_copy_type() {
        return bank_copy_type;
    }

    public void setBank_copy_type(String bank_copy_type) {
        this.bank_copy_type = bank_copy_type;
    }

    public Object getBank_copy() {
        return bank_copy;
    }

    public void setBank_copy(Object bank_copy) {
        this.bank_copy = bank_copy;
    }
}
