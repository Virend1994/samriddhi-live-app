package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MonthListModel extends BaseModel {
    @SerializedName("BillNo")
    @Expose
    private String billNo;
    @SerializedName("CloseBal")
    @Expose
    private String closeBal;
    @SerializedName("Fkdat")
    @Expose
    private String fkdat;
    @SerializedName("SoldToPty")
    @Expose
    private String soldToPty;
    @SerializedName("Monat")
    @Expose
    private String monat;
    @SerializedName("Gjahr")
    @Expose
    private String gjahr;
    @SerializedName("SoldCopies")
    @Expose
    private String soldCopies;
    @SerializedName("NetBilling")
    @Expose
    private String netBilling;
    @SerializedName("OpenBal")
    @Expose
    private String openBal;
    @SerializedName("PayAmt")
    @Expose
    private String payAmt;
    @SerializedName("Payment")
    @Expose
    private String payment;

    public String getBillNo() {
        return billNo;
    }

    public void setBillNo(String billNo) {
        this.billNo = billNo;
    }

    public String getCloseBal() {
        return closeBal;
    }

    public void setCloseBal(String closeBal) {
        this.closeBal = closeBal;
    }

    public String getFkdat() {
        return fkdat;
    }

    public void setFkdat(String fkdat) {
        this.fkdat = fkdat;
    }

    public String getSoldToPty() {
        return soldToPty;
    }

    public void setSoldToPty(String soldToPty) {
        this.soldToPty = soldToPty;
    }

    public String getMonat() {
        return monat;
    }

    public void setMonat(String monat) {
        this.monat = monat;
    }

    public String getGjahr() {
        if (gjahr.isEmpty())
            return "";
        else {
            return gjahr = gjahr.substring(gjahr.length() - 2);
        }
    }

    public void setGjahr(String gjahr) {
        this.gjahr = gjahr;
    }

    public String getSoldCopies() {
        return soldCopies;
    }

    public void setSoldCopies(String soldCopies) {
        this.soldCopies = soldCopies;
    }

    public String getNetBilling() {
        return netBilling;
    }

    public void setNetBilling(String netBilling) {
        this.netBilling = netBilling;
    }

    public String getOpenBal() {
        return openBal;
    }

    public void setOpenBal(String openBal) {
        this.openBal = openBal;
    }

    public String getPayAmt() {
        return payAmt;
    }

    public void setPayAmt(String payAmt) {
        this.payAmt = payAmt;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    /**
     * returns the formatted month
     * @return
     */
    public String getFormattedMonth() {
        if (monat.equalsIgnoreCase("01")) {
            return "Jan" + getGjahr();
        } else if (monat.equalsIgnoreCase("02")) {
            return "Feb" + getGjahr();
        } else if (monat.equalsIgnoreCase("03")) {
            return "Mar" + getGjahr();
        } else if (monat.equalsIgnoreCase("04")) {
            return "Apr" + getGjahr();
        } else if (monat.equalsIgnoreCase("05")) {
            return "May" + getGjahr();
        } else if (monat.equalsIgnoreCase("06")) {
            return "Jun" + getGjahr();
        } else if (monat.equalsIgnoreCase("07")) {
            return "Jul" + getGjahr();
        } else if (monat.equalsIgnoreCase("08")) {
            return "Aug" + getGjahr();
        } else if (monat.equalsIgnoreCase("09")) {
            return "Sep" + getGjahr();
        } else if (monat.equalsIgnoreCase("10")) {
            return "Oct" + getGjahr();
        } else if (monat.equalsIgnoreCase("11")) {
            return "Nov" + getGjahr();
        } else if (monat.equalsIgnoreCase("12")) {
            return "Dec" + getGjahr();
        } else return "";
    }
}
