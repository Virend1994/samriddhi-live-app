package com.app.samriddhi.ui.activity.main.ui.Grievance;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

public class RequestDetailTabAdapter  extends FragmentStatePagerAdapter {
    int mNumOfTabs;



    public RequestDetailTabAdapter(@NonNull FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs=NumOfTabs;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment=null;
        switch (position){
            case 0:

                OpenFragment openFragment=new OpenFragment();
                return openFragment;
            case 1:
                CloseFragment closeFragment=new CloseFragment();
                return closeFragment;
        }

        return null;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
