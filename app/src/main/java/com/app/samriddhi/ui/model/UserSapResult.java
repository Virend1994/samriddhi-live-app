package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class UserSapResult extends BaseModel {

    @SerializedName("results")
    @Expose
    private List<LoginSapResultModel> results = null;

    public List<LoginSapResultModel> getResults() {
        return results;
    }

    public void setResults(List<LoginSapResultModel> results) {
        this.results = results;
    }
}
