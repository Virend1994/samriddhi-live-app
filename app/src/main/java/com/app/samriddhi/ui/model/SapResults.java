package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SapResults extends BaseModel {

    @SerializedName("mandt")
    @Expose
    private String mandt;
    @SerializedName("partner")
    @Expose
    private String partner;
    @SerializedName("vkorg")
    @Expose
    private String vkorg;
    @SerializedName("vkbur")
    @Expose
    private String vkbur;
    @SerializedName("vkgrp")
    @Expose
    private String vkgrp;
    @SerializedName("name1")
    @Expose
    private String name1;
    @SerializedName("name2")
    @Expose
    private String name2;
    @SerializedName("kdgrp")
    @Expose
    private String kdgrp;
    @SerializedName("city1")
    @Expose
    private String city1;

    @SerializedName("Mobile")
    @Expose
    private String mobile;

    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("reqsent_flag")
    @Expose
    private String reqsentFlag;
    @SerializedName("request_send")
    @Expose
    private String requestSend;
    @SerializedName("req_user")
    @Expose
    private String reqUser;
    @SerializedName("active_flag")
    @Expose
    private String activeFlag;
    @SerializedName("active_date")
    @Expose
    private String activeDate;
    @SerializedName("deviceid")
    @Expose
    private String deviceid;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("request_time")
    @Expose
    private String requestTime;
    @SerializedName("active_time")
    @Expose
    private String activeTime;
    @SerializedName("ParentFlag")
    @Expose
    private String parentFlag;
    @SerializedName("Parent")
    @Expose
    private String parent;
    @SerializedName("vtweg")
    @Expose
    private String vtweg;
    @SerializedName("bzirk")
    @Expose
    private String bzirk;


    public String getMandt() {
        return mandt;
    }

    public void setMandt(String mandt) {
        this.mandt = mandt;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public String getVkorg() {
        return vkorg;
    }

    public void setVkorg(String vkorg) {
        this.vkorg = vkorg;
    }

    public String getVkbur() {
        return vkbur;
    }

    public void setVkbur(String vkbur) {
        this.vkbur = vkbur;
    }

    public String getVkgrp() {
        return vkgrp;
    }

    public void setVkgrp(String vkgrp) {
        this.vkgrp = vkgrp;
    }

    public String getName1() {
        return name1;
    }

    public void setName1(String name1) {
        this.name1 = name1;
    }

    public String getName2() {
        return name2;
    }

    public void setName2(String name2) {
        this.name2 = name2;
    }

    public String getKdgrp() {
        return kdgrp;
    }

    public void setKdgrp(String kdgrp) {
        this.kdgrp = kdgrp;
    }

    public String getCity1() {
        return city1;
    }

    public void setCity1(String city1) {
        this.city1 = city1;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getReqsentFlag() {
        return reqsentFlag;
    }

    public void setReqsentFlag(String reqsentFlag) {
        this.reqsentFlag = reqsentFlag;
    }

    public String getRequestSend() {
        return requestSend;
    }

    public void setRequestSend(String requestSend) {
        this.requestSend = requestSend;
    }

    public String getReqUser() {
        return reqUser;
    }

    public void setReqUser(String reqUser) {
        this.reqUser = reqUser;
    }

    public String getActiveFlag() {
        return activeFlag;
    }

    public void setActiveFlag(String activeFlag) {
        this.activeFlag = activeFlag;
    }

    public String getActiveDate() {
        return activeDate;
    }

    public void setActiveDate(String activeDate) {
        this.activeDate = activeDate;
    }

    public String getDeviceid() {
        return deviceid;
    }

    public void setDeviceid(String deviceid) {
        this.deviceid = deviceid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRequestTime() {
        return requestTime;
    }

    public void setRequestTime(String requestTime) {
        this.requestTime = requestTime;
    }

    public String getActiveTime() {
        return activeTime;
    }

    public void setActiveTime(String activeTime) {
        this.activeTime = activeTime;
    }

    public String getParentFlag() {
        return parentFlag;
    }

    public void setParentFlag(String parentFlag) {
        this.parentFlag = parentFlag;
    }

    public String getParent() {
        return parent;
    }

    public void setParent(String parent) {
        this.parent = parent;
    }

    public String getVtweg() {
        return vtweg;
    }

    public void setVtweg(String vtweg) {
        this.vtweg = vtweg;
    }

    public String getBzirk() {
        return bzirk;
    }

    public void setBzirk(String bzirk) {
        this.bzirk = bzirk;
    }
}
