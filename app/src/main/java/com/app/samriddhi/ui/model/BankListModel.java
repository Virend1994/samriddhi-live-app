package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BankListModel extends BaseModel {
    @SerializedName("Bank_Name")
    @Expose
    private String bankName;
    @SerializedName("Bank_Type")
    @Expose
    private Object bankType;
    @SerializedName("Bank_URL")
    @Expose
    private String bankURL;
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("create_date")
    @Expose
    private String createDate;
    @SerializedName("update_date")
    @Expose
    private String updateDate;
    @SerializedName("create_user")
    @Expose
    private Object createUser;
    @SerializedName("update_user")
    @Expose
    private Object updateUser;

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public Object getBankType() {
        return bankType;
    }

    public void setBankType(Object bankType) {
        this.bankType = bankType;
    }

    public String getBankURL() {
        return bankURL;
    }

    public void setBankURL(String bankURL) {
        this.bankURL = bankURL;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public Object getCreateUser() {
        return createUser;
    }

    public void setCreateUser(Object createUser) {
        this.createUser = createUser;
    }

    public Object getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(Object updateUser) {
        this.updateUser = updateUser;
    }
}
