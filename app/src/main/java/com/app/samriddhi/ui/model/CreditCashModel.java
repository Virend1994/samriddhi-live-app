package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CreditCashModel extends BaseModel {

    @SerializedName("Dch")
    @Expose
    private String dch;
    @SerializedName("SdistName")
    @Expose
    private String sdistName;
    @SerializedName("Vtweg")
    @Expose
    private String vtweg;
    @SerializedName("EditionName")
    @Expose
    private String editionName;
    @SerializedName("Sgrp")
    @Expose
    private String sgrp;
    @SerializedName("Vkgrp")
    @Expose
    private String vkgrp;
    @SerializedName("Fltr")
    @Expose
    private String fltr;
    @SerializedName("Bzirk")
    @Expose
    private String bzirk;
    @SerializedName("State")
    @Expose
    private String state;
    @SerializedName("Vkorg")
    @Expose
    private String vkorg;
    @SerializedName("Location")
    @Expose
    private String location;
    @SerializedName("Adid")
    @Expose
    private String adid;
    @SerializedName("Pva")
    @Expose
    private String pva;
    @SerializedName("OrdDate")
    @Expose
    private String ordDate;
    @SerializedName("Partner")
    @Expose
    private String partner;
    @SerializedName("SoldToParty")
    @Expose
    private String soldToParty;
    @SerializedName("CustName")
    @Expose
    private String custName;
    @SerializedName("GrossCopy")
    @Expose
    private String grossCopy;
    @SerializedName("FreeCopy")
    @Expose
    private String freeCopy;
    @SerializedName("PaidCopy")
    @Expose
    private String paidCopy;
    @SerializedName("Zlao")
    @Expose
    private String zlao;
    @SerializedName("Zcoo")
    @Expose
    private String zcoo;

    public String getDch() {
        return dch;
    }

    public void setDch(String dch) {
        this.dch = dch;
    }

    public String getSdistName() {
        return sdistName;
    }

    public void setSdistName(String sdistName) {
        this.sdistName = sdistName;
    }

    public String getVtweg() {
        return vtweg;
    }

    public void setVtweg(String vtweg) {
        this.vtweg = vtweg;
    }

    public String getEditionName() {
        return editionName;
    }

    public void setEditionName(String editionName) {
        this.editionName = editionName;
    }

    public String getSgrp() {
        return sgrp;
    }

    public void setSgrp(String sgrp) {
        this.sgrp = sgrp;
    }

    public String getVkgrp() {
        return vkgrp;
    }

    public void setVkgrp(String vkgrp) {
        this.vkgrp = vkgrp;
    }

    public String getFltr() {
        return fltr;
    }

    public void setFltr(String fltr) {
        this.fltr = fltr;
    }

    public String getBzirk() {
        return bzirk;
    }

    public void setBzirk(String bzirk) {
        this.bzirk = bzirk;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getVkorg() {
        return vkorg;
    }

    public void setVkorg(String vkorg) {
        this.vkorg = vkorg;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAdid() {
        return adid;
    }

    public void setAdid(String adid) {
        this.adid = adid;
    }

    public String getPva() {
        return pva;
    }

    public void setPva(String pva) {
        this.pva = pva;
    }

    public String getOrdDate() {
        return ordDate;
    }

    public void setOrdDate(String ordDate) {
        this.ordDate = ordDate;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public String getSoldToParty() {
        return soldToParty;
    }

    public void setSoldToParty(String soldToParty) {
        this.soldToParty = soldToParty;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getGrossCopy() {
        return grossCopy;
    }

    public void setGrossCopy(String grossCopy) {
        this.grossCopy = grossCopy;
    }

    public String getFreeCopy() {
        return freeCopy;
    }

    public void setFreeCopy(String freeCopy) {
        this.freeCopy = freeCopy;
    }

    public String getPaidCopy() {
        return paidCopy;
    }

    public void setPaidCopy(String paidCopy) {
        this.paidCopy = paidCopy;
    }

    public String getZlao() {
        return zlao;
    }

    public void setZlao(String zlao) {
        this.zlao = zlao;
    }

    public String getZcoo() {
        return zcoo;
    }

    public void setZcoo(String zcoo) {
        this.zcoo = zcoo;
    }
}
