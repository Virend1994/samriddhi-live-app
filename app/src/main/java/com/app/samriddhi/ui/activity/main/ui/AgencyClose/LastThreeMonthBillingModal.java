package com.app.samriddhi.ui.activity.main.ui.AgencyClose;

public class LastThreeMonthBillingModal {

    public String id;
    public String month;
    public String opening_balance;
    public String billing_amount;
    public String unbilling_amount;
    public String net_amount;
    public String payment;
    public String closing_balance;
    public String copies;


    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getOpening_balance() {
        return opening_balance;
    }

    public void setOpening_balance(String opening_balance) {
        this.opening_balance = opening_balance;
    }

    public String getBilling_amount() {
        return billing_amount;
    }

    public void setBilling_amount(String billing_amount) {
        this.billing_amount = billing_amount;
    }

    public String getUnbilling_amount() {
        return unbilling_amount;
    }

    public void setUnbilling_amount(String unbilling_amount) {
        this.unbilling_amount = unbilling_amount;
    }

    public String getNet_amount() {
        return net_amount;
    }

    public void setNet_amount(String net_amount) {
        this.net_amount = net_amount;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getClosing_balance() {
        return closing_balance;
    }

    public void setClosing_balance(String closing_balance) {
        this.closing_balance = closing_balance;
    }

    public String getCopies() {
        return copies;
    }

    public void setCopies(String copies) {
        this.copies = copies;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
