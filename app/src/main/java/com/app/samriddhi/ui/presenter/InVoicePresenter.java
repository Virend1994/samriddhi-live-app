package com.app.samriddhi.ui.presenter;

import android.util.Log;

import com.app.samriddhi.R;
import com.app.samriddhi.SamriddhiApplication;
import com.app.samriddhi.api.request.InvoiceRequest;
import com.app.samriddhi.api.response.BaseResponse;
import com.app.samriddhi.base.BasePresenter;
import com.app.samriddhi.ui.view.IView;
import com.google.gson.Gson;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InVoicePresenter extends BasePresenter<IView> {

    /**
     * confirm the bill viewed invoice by agent
     * @param invoiceData Agnets invoice data
     */



    public void
    confirmInvoice(InvoiceRequest invoiceData) {

        Gson gson = new Gson();
        String data = gson.toJson(invoiceData);
        Log.e("asfdfasfa", data);
        getView().enableLoadingBar(true);
        SamriddhiApplication.getmInstance().getApiService().confirmBill(invoiceData).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                getView().enableLoadingBar(false);

                Log.e("asfdfasfa",response.body().toString());
                if (!handleError(response)) {
                    if(response.isSuccessful() && response.code() == 200) {
                        if (response.body() != null) {
                            getView().onInfo(response.body().replyMsg);
                        } else {
                            getView().onError(getView().getContext().getString(R.string.str_no_record_found));

                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                getView().enableLoadingBar(false);
                try {
                    t.printStackTrace();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                getView().onError(null);
            }
        });
    }
}
