package com.app.samriddhi.ui.model;

import com.app.samriddhi.base.model.BaseModel;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SalesDistrictWiseResultModel extends BaseModel {

    @SerializedName("results")
    @Expose
    private List<SalesDistrictWiseListModel> salesOrgCityList = null;

    public List<SalesDistrictWiseListModel> getSalesOrgCityList() {
        return salesOrgCityList;
    }

    public void setSalesOrgCityList(List<SalesDistrictWiseListModel> salesOrgCityList) {
        this.salesOrgCityList = salesOrgCityList;
    }
}
